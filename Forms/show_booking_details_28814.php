<?php
session_start();
require_once('../init.php');
include(''.INCLUDE_PATH.'config.php');
include(''.INCLUDE_PATH.'settings.php');
include(''.INCLUDE_PATH.'auth.php');
require_once(CLASSES_PATH . "job.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "chargeAccount.php");
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "jobReference.php");
require_once(INCLUDE_PATH . "functions_date_time.php");

$database = new database();
$job = new Job();
$job_reference = new JobReference();
$user = new user();
$charge_account = new chargeAccount();
$job_id = $_GET['job_id'];

$this_job_details = $job->getJobDetails($job_id);
$job_ref_details = $job_reference->getJobReferenceDetails($this_job_details['job_reference_id']);
if($job_id == $job_ref_details['j1_id'])
	{
		$make_white_background = 1;
	}
else
	{
		$make_white_background = 2;
	}
$j1_details = $job->getJobDetails($job_ref_details['j1_id']);

if($job_ref_details['job_type'] == '2')
	{
		$j2_details = $job->getJobDetails($job_ref_details['j2_id']);
	}
$entered_by_details = $user->getUserDetails($job_ref_details['entered_by']);
$charge_account_details = $charge_account->getChargeAccountDetails($job_ref_details['charge_acc_id']);
echo'
<link href="../Css/minified/jquery-ui-1.10.3.custom.min.css" rel="stylesheet"  />
	<link href="../Css/jtable/themes/lightcolor/blue/jtable.css" rel="stylesheet"  type="text/css" />
	<link href="../Css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
	<link href="../Css/style.css" rel="stylesheet"  />
	<link href="../Css/menu.css" rel="stylesheet"  />
	<link href="../Css/chosen.min.css" rel="stylesheet"  />
	<link href="../Css/autocomplete.css" rel="stylesheet"  />
	<link href="../Css/jquery.timeentry.css" rel="stylesheet"  />

	
	<script src="../Scripts/jquery-1.10.2.min.js"></script>
	<script src="../Scripts/jquery-ui.min.js"></script>
	<script src="../Scripts/jquery.timeentry.min.js"></script>
	<script src="../Scripts/jquery.jtable.min.js"></script>
	<script src="../Scripts/jquery.validate.js"></script>
	<script src="../Scripts/jquery.validationEngine.js"></script>
	<script src="../Scripts/jquery.validationEngine-en.js"></script>
	<script src="../Scripts/chosen.jquery.min.js"></script>
	<script src="../Scripts/show_booking_details.js"></script>
	
	
	<div id="dialog_confirm_job_1" style="display:none;"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><b>Do you want to CONFIRM this booking?</b><br/><br/>This will send a confirmation email to the customer.</p></div>
	<div id="dialog_decline_job_1" style="display:none;"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><b>Do you want to DECLINE this booking?</b><br/><br/>This will send a declined email to the customer.</p></div>
	<div id="dialog_cancel_job_1" style="display:none;"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><b>Do you want to CANCEL this booking?</b><br/><br/>This will make the status of the booking cancelled.</p></div>
	<div id="dialog_clone_job_1" style="display:none;"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><b>Do you want to CLONE this booking?</b><br/><br/>This will clone only this booking and will redirect you to the new cloned booking page</p></div>
	<div id="dialog_rev_clone_job_1" style="display:none;"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><b>Do you want to REVERSE CLONE this booking?</b><br/><br/>This will clone this booking with addresses in reverse order and redirect you to the new cloned page.</p></div>
	<div id="dialog_reactivate_job_1" style="display:none;"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><b>Do you want to RE-ACTIVATE this booking?</b><br/><br/>This will change the status of this cancelled boking to CONFIRMED.</p></div>
	<div id="dialog_delete_job_1" style="display:none;"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><b>Do you want to DELETE this booking?</b><br/><br/>This is an irreversible process. Data will be lost forever.</p></div>
	
	<div id="dialog_resend_details_job_1" style="display:none;"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><b>Do you want to resend details of this job via email to the driver?</b></div>
	<div id="dialog_resend_details_job_2" style="display:none;"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><b>Do you want to resend details of this job via email to the driver?</b></div>
	<div id="dialog_pay_job_1" style="display:none;"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><b>Do you want to PAY for this booking now?</b></div>
	<div id="dialog_pay_job_2" style="display:none;"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><b>Do you want to PAY for this booking now?</b></div>
	<div id="dialog_j1_tear_off" style="display:none;"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><b>This will delete all Driver Logs for this job?</b><br/><br/>This is an irreversible process. Data will be lost forever.</p></div>
	<div id="dialog_j2_tear_off" style="display:none;"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><b>This will delete all Driver Logs for this job?</b><br/><br/>This is an irreversible process. Data will be lost forever.</p></div>
	<div id="dialog_j1_pay_extra" style="display:none;"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><b>You are about to pay extra to the driver?</b><br/><br/>This will make accounting entry in driver accounts.</p></div>
	<div id="dialog_j2_pay_extra" style="display:none;"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><b>You are about to pay extra to the driver?</b><br/><br/>This will make accounting entry in driver accounts.</p></div>
	
	<div id="dialog_confirm_job_2" style="display:none;"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><b>Do you want to CONFIRM this booking?</b><br/><br/>This will send a confirmation email to the customer.</p></div>
	<div id="dialog_decline_job_2" style="display:none;"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><b>Do you want to DECLINE this booking?</b><br/><br/>This will send a declined email to the customer.</p></div>
	<div id="dialog_cancel_job_2" style="display:none;"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><b>Do you want to CANCEL this booking?</b><br/><br/>This will make the status of the booking cancelled.</p></div>
	<div id="dialog_clone_job_2" style="display:none;"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><b>Do you want to CLONE this booking?</b><br/><br/>This will clone only this booking and will redirect you to the new cloned booking page</p></div>
	<div id="dialog_rev_clone_job_2" style="display:none;"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><b>Do you want to REVERSE CLONE this booking?</b><br/><br/>This will clone this booking with addresses in reverse order and redirect you to the new cloned page.</p></div>
	<div id="dialog_reactivate_job_2" style="display:none;"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><b>Do you want to RE-ACTIVATE this booking?</b><br/><br/>This will change the status of this cancelled boking to CONFIRMED.</p></div>
	<div id="dialog_delete_job_2" style="display:none;"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><b>Do you want to DELETE this booking?</b><br/><br/>This is an irreversible process. Data will be lost forever.</p></div>
	
	<div id="dialog_1" style="display:none;"></div>
	<div id="dialog_2" style="display:none;"></div>
	<div id="dialog_3" style="display:none;"></div>
	<div id="dialog_4" style="display:none;"></div>
	<div id="dialog_5" style="display:none;"></div>
	<div id="dialog_6" style="display:none;"></div>
	<div id="dialog_7" style="display:none;"></div>
	<div id="dialog_8" style="display:none;"></div>
	<div id="dialog1" style="display:none;"></div>
	<div id="dialog2" style="display:none;"></div>
	<div id="dialog3" style="display:none;"></div>
	<div id="dialog4" style="display:none;"></div>
	<div id="dialog5" style="display:none;"></div>
	<div id="dialog6" style="display:none;"></div>
	<div id="dialog7" style="display:none;"></div>
	<div id="dialog8" style="display:none;"></div>
	
	
	<div id="dialog_submit" style="display:none;"></div>
	
	<div id="dialog_confirm_jobs" title="CONFIRM?"  style="display:none;">
	  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This will CONFIRM this booking and confirmation email will be sent to client. Are you sure?</p>
	</div>
	<div id="dialog_decline_jobs" title="CONFIRM?"  style="display:none;">
	  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This will DECLINE this booking and apology email will be sent to client. Are you sure?</p>
	</div>
	<div id="dialog_clone_jobs" title="CONFIRM?"  style="display:none;">
	  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This will clone this booking. Are you sure?</p>
	</div>
	<div id="dialog_rev_clone_jobs" title="CONFIRM?"  style="display:none;">
	  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This will reverse clone this booking. Are you sure?</p>
	</div>
	<div id="dialog_cancel_jobs" title="CONFIRM?"  style="display:none;">
	  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Do you really want to CANCEL bookings? If this is return booking, this will cancel both the bookings.</p>
	</div>
	<div id="dialog_delete_jobs" title="CONFIRM?"  style="display:none;">
	  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Do you really want to DELETE bookings? This will delete both the bookings completely.</p>
	</div>
	<div id="dialog_reactivate_jobs" title="CONFIRM?"  style="display:none;">
	  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Do you really want to RE-ACTIVATE bookings? This will change the status of both the bookings to CONFIRMED.</p>
	</div>
<table>
	<tr>
		<td valign="top">
			<form name="form_update_booking" id="form_update_booking" action="" method="POST">
			<table id="bkg_table">
				<tr>
					<td colspan="3" style="background-image: url(../Images/bg1.jpg);" valign="top">
						<span id="save_booking"><img src="../Images/save.png" alt="Save/Update" height="50" width="50" style="vertical-align:top;"></span>';
						if($_SESSION['ROLE_ID'] == '1') // Admin
							{
								if($job_ref_details['job_type'] == '2')
									{
										if($j1_details['job_status'] == '10' && $j2_details['job_status'] == '10')
											{
												echo'<span id="confirm_jobs"><img src="../Images/confirm.png" alt="Confirm Bookings" height="50" width="50" style="vertical-align:top;"></span>';
												echo'<span id="decline_jobs"><img src="../Images/decline.png" alt="Decline Bookings" height="50" width="50" style="vertical-align:top;"></span>';
											}
										echo'<span id="clone_jobs"><img src="../Images/clone.png" alt="Clone Bookings" height="50" width="50" style="vertical-align:top;"></span>';
										echo'<span id="rev_clone_jobs"><img src="../Images/rev_clone.png" alt="Reverse Clone Bookings" height="50" width="50" style="vertical-align:top;"></span>';
										
										if(($j1_details['job_status'] == '90' && $j2_details['job_status'] == '90') || ($j1_details['job_status'] == '100' && $j2_details['job_status'] == '100'))
											{
												echo'<span id="reactivate_jobs"><img src="../Images/reactivate.png" alt="Reactivate Bookings" height="50" width="50" style="vertical-align:top;"></span>';
												echo'<span id="delete_jobs"><img src="../Images/delete.png" alt="Delete Bookings" height="50" width="50" style="vertical-align:top;"></span>';
												
											}
										else
											{
												echo'<span id="cancel_jobs"><img src="../Images/cancel.png" alt="Cancel Bookings" height="50" width="50" style="vertical-align:top;"></span>';
											}
									}
							}
						echo'
						<input type="hidden" name="job_ref_id" value="'.$job_ref_details['id'].'"/>
					</td>
				</tr>
				<tr>
					<td valign="top">
						<table>
							<tr>
								<td>
									<table style="border: 1px solid #418594;" width="100%">
										<tr>
											<td>Entered By</td>
											<td><input class="readonly" size="30%" type="text" name="entered_by" id="entered_by" value="'.$entered_by_details['title'].' '.$entered_by_details['fname'].' '.$entered_by_details['lname'].'" readonly/>';
									echo '</td>
										</tr>
										<tr>
											<td>Recorded On</td>
											<td><input class="readonly" size="30%" type="text" name="record_date" id="record_date" value="'.date( 'm/d/y g:i A', strtotime($job_ref_details['created_on'])).'" readonly/></td>
										</tr>';
										if($_SESSION['ROLE_ID'] == '1')
											{
												echo'
												<tr>                                                                               
													<td><span id="text_blue">Booking Source</span></td>
													<td>
														<input type="radio" name="job_src" value="Email"';if($job_ref_details['job_src']=='Email') {echo' checked';} echo'>Email
														<input type="radio" name="job_src" value="Phone"';if($job_ref_details['job_src']=='Phone') {echo' checked';} echo'>Phone
														<input type="radio" name="job_src" value="Website"';if($job_ref_details['job_src']=='Website') {echo' checked';} echo'>Website
														<input type="radio" name="job_src" value="Others"';if($job_ref_details['job_src']=='Others') {echo' checked';} echo'>Others
													</td>
												</tr> ';
											}
										if($_SESSION['ROLE_ID'] == '1' || ($_SESSION['ROLE_ID'] == '3' && $charge_account_details['acc_type'] != '3'))
											{
												echo'
												<tr>                                                                               
													<td valign="top">Send Confirmation To</td>
													<td>
														<input type="checkbox" name="conf_to_client" id="conf_to_client" value="1">To Client
														<input type="checkbox" name="conf_to_pax" id="conf_to_pax" value="1">To Pax<br/>
													</td>
												</tr>
												<tr>
													<td>Order Reference</td>
													<td><input type="text" size="30%"  name="order_ref" id="order_ref" value="'.$job_ref_details['order_ref'].'"></td>
												</tr>';
											}
										if($_SESSION['ROLE_ID'] == '1') // Admin
											{
												echo'
												<tr>
													<td>Booking For</td>
													<td><select name="acc_type" id="acc_type" style="width:47%;">';
														$query = "SELECT * from variable__charge_acc_type order by order_id ASC";
														$result = $database->query($query);
														while ($row = mysql_fetch_array($result))
															{
																echo '<option value="'.$row['id'].'"'; if($job_ref_details['acc_type'] == $row['id']) {echo 'selected';} echo'>'.$row['details'].'</option>';
															}
														echo '
													</td>
												</tr>';
											}
										else // Customer
											{
												echo'<input type="hidden" name="acc_type" value="'.$job_ref_details['acc_type'].'">';
											}
										if($job_ref_details['acc_type'] == '3')
											{
												echo'
												<tr class="for_student">
													<td>Student ID</td>
													<td><input type="text" name="std_id" id="std_id" value="'.$job_ref_details['std_id'].'"></td>
												</tr>
												<tr class="for_student">
													<td>Student Name</td>
													<td><select name="std_title" id="std_title">
															<option value="Mr"'; if($job_ref_details['std_title']== 'Mr') {echo 'selected';} echo'>Mr</option>
															<option value="Ms"'; if($job_ref_details['std_title']== 'Ms') {echo 'selected';} echo'>Ms</option>
															<option value="Mrs"'; if($job_ref_details['std_title']== 'Mrs') {echo 'selected';} echo'>Mrs</option>
														</select>
														<input  type="text" name="std_fname" id="std_fname" value="'.$job_ref_details['std_fname'].'">
														<input  type="text" name="std_lname" id="std_lname" value="'.$job_ref_details['std_lname'].'">
													</td>
												</tr>';
											}
										if($_SESSION['ROLE_ID'] == '1') // Admin
											{
												echo'
												<tr>
													<td>Charge Account</td>
													<td>
														<select name="charge_acc" id="charge_acc" class="chosen-select" style="width:350px;">
														<option value="">Please Select</option>';
														$query = "SELECT * from charge_acc where acc_type='".$job_ref_details['acc_type']."' order by account_name ASC";
														$result = $database->query($query);
														while ($row = mysql_fetch_array($result))
															{
																echo '<option value="'.$row['id'].'"'; if($job_ref_details['charge_acc_id'] == $row['id']) {echo 'selected';} echo'>'.$row['account_name'].'</option>';
															}
														echo '
														</select>';
														echo'
														<input type="button" class="sml_btn" name="show_charge_acc" id="show_charge_acc" value="Add">
														<span id="span_charge_acc_history"></span>
													</td>
												</tr>
												<tr id="row_new_charge_account">
													<td></td>
													<td>
														<table id="sub_table">
															<tr>
																<th>New Account Name
																</th>
															</tr>
															<tr>
																<td>
																	<input type="text" size="27" name="account_name" id="account_name" placeholder="Account Name" value="'.$_POST['account_name'].'"><br/>
																	<select name="account_type">
																		<option value="1"'; if($_POST['account_type'] == '1') {echo 'selected';} echo'>Corporate</option>
																		<option value="2"'; if($_POST['account_type'] == '2') {echo 'selected';} echo'>Individual</option>
																		<option value="3"'; if($_POST['account_type'] == '3') {echo 'selected';} echo'>University</option>
																	</select><br/>
																	<input type="text" size="27" name="myob_no" id="myob_no" placeholder="MYOB No" value="'.$_POST['myob_no'].'"><br/>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td>Booking Made by</td>
													<td>
														<select name="acc_contact" id="acc_contact" class="chosen-select" style="width:350px;">
														<option value="">Please Select</option>';
														$query = "SELECT * from
																	charge_acc__contacts, user
																	where charge_acc__contacts.user_id = user.id
																	AND charge_acc__contacts.charge_acc_id='".$job_ref_details['charge_acc_id']."' 
																	AND (charge_acc__contacts.type_id = '1' OR charge_acc__contacts.type_id = '3')
																	order by user.fname";
														$result = $database->query($query);
														while ($row = mysql_fetch_array($result))
															{
																echo '<option value="'.$row['id'].'"'; if($job_ref_details['bkg_by_id'] == $row['id']) {echo 'selected';} echo'>'.$row['fname'].' '.$row['lname'].'</option>';
															}
														echo '
														</select>';
														echo'
														<input type="button" class="sml_btn" name="bkg_made_by" id="bkg_made_by" value="Add">
														<span id="span_bkg_made_by_history"></span>';
														$query = "SELECT * from user where id='".$job_ref_details['bkg_by_id']."'";
														$result = $database->query($query);
														$row = mysql_fetch_array($result);
														echo '<div id="show_acc_contact_preference" class="small_red">'.$row['preference'].'</div>';
													echo'
													</td>
												</tr>
												<tr id="row_new_contact">
													<td></td>
													<td>
														<table id="sub_table">
															<tr>
																<th colspan="3">New Booking Contact:</th>
															</tr>
															<tr>
																<td>Contact Type</td>
																<td colspan="2"><select name="bkg_by_type" id="bkg_by_type" style="width:47%;">';
																	$query = "SELECT * from variable__conatct_type where id='1' OR id='3' order by order_id ASC";
																	$result = $database->query($query);
																	while ($row = mysql_fetch_array($result))
																		{
																			echo "<option value='".$row['id']."'>".$row['details']."</option>";
																		}
																	echo '
																</td>
															</tr>
															<tr>
																<td>Password</td>
																<td colspan="2"><input type="text" name="bkg_by_password" id="bkg_by_password" placeholder="Password" value="'.$_POST['bkg_by_password'].'"></td>
															</tr>
															<tr>
																<td valign="top">Name</td>
																<td colspan="2">
																	<select name="bkg_by_title" id="bkg_by_title">
																		<option value=""></option>
																		<option value="Mr"'; if($_POST['bkg_by_title'] == 'Mr') {echo 'selected';} echo'>Mr</option>
																		<option value="Mrs"'; if($_POST['bkg_by_title'] == 'Mrs') {echo 'selected';} echo'>Mrs</option>
																		<option value="Ms"'; if($_POST['bkg_by_title'] == 'Ms') {echo 'selected';} echo'>Ms</option>
																		<option value="Dr"'; if($_POST['bkg_by_title'] == 'Dr') {echo 'selected';} echo'>Dr</option>
																		<option value="Prof"'; if($_POST['bkg_by_title'] == 'Prof') {echo 'selected';} echo'>Prof</option>
																	</select>
																	<input type="text" name="bkg_by_fname" id="bkg_by_fname" placeholder="First Name" value="'.$_POST['bkg_by_fname'].'">
																	<input type="text" name="bkg_by_lname" id="bkg_by_lname" placeholder="Last Name" value="'.$_POST['bkg_by_lname'].'"><br/>
																	<input type="text" name="bkg_by_mobile" id="bkg_by_mobile" placeholder="Mobile" value="'.$_POST['bkg_by_mobile'].'">
																	<input type="text" name="bkg_by_phone" id="bkg_by_phone" placeholder="Phone" value="'.$_POST['bkg_by_phone'].'"><br/>
																	<input type="text" name="bkg_by_email" id="bkg_by_email" placeholder="Email" value="'.$_POST['bkg_by_email'].'">
																</td>
															</tr>
															<tr>
																<td>Preference:<br/>(If any)</td>
																<td colspan="2">
																	<textarea name="bkg_by_preference" id="bkg_by_preference" cols="30" rows="2">'; echo $_POST['new_pax_preference']; echo'</textarea>
																</td>
															</tr>
														</table>
													</td>
												</tr>';
											}
										if($_SESSION['ROLE_ID'] == '1' || ($_SESSION['ROLE_ID'] == '3' && $charge_account_details['acc_type'] != '3'))
											{
												echo'
												<tr>
													<td>Select Passenger</td>
													<td>
														<select name="acc_passenger" id="acc_passenger" class="chosen-select" style="width:350px;">
														<option value="">Please Select</option>';
														$query = "SELECT * from
																	charge_acc__contacts, user
																	where charge_acc__contacts.user_id = user.id
																	AND charge_acc__contacts.charge_acc_id='".$job_ref_details['charge_acc_id']."' 
																	AND (charge_acc__contacts.type_id = '1' OR charge_acc__contacts.type_id = '2')
																	order by user.fname";
														$result = $database->query($query);
														while ($row = mysql_fetch_array($result))
															{
																echo '<option value="'.$row['id'].'"'; if($job_ref_details['pax_id'] == $row['id']) {echo 'selected';} echo'>'.$row['fname'].' '.$row['lname'].'</option>';
															}
														echo '
														</select>
														<input type="button" class="sml_btn" name="add_new_pax" id="add_new_pax" value="Add">
														<span id="span_passenger_history"></span>';
														$query = "SELECT * from user where id='".$job_ref_details['pax_id']."'";
														$result = $database->query($query);
														$row = mysql_fetch_array($result);
														echo '<div id="show_pax_preference" class="small_red">'.$row['preference'].'</div>';
													echo'
													</td>
												</tr>
												<tr id="row_new_passenger">
													<td></td>
													<td>
														<table id="sub_table">
															<tr>
																<th colspan="3">
																New Pax Name:
																<span id="pax_name_toggle"><img src="../Images/return_icon.gif" alt="same as Booking Made By" height="20" width="20" style="vertical-align:top;"></span>
																</th>
															</tr>
															<tr>
																<td>Contact Type</td>
																<td colspan="2"><select name="new_pax_type" id="new_pax_type" style="width:47%;">';
																	
																	$query = "SELECT * from variable__conatct_type where id='1' OR id='2' order by order_id ASC";
																	$result = $database->query($query);
																	while ($row = mysql_fetch_array($result))
																		{
																			echo "<option value='".$row['id']."'>".$row['details']."</option>";
																		}
																	echo '
																</td>
															</tr>
															<tr>
																<td>Password</td>
																<td colspan="2"><input type="text" name="new_pax_password" id="new_pax_password" placeholder="Password"></td>
															</tr>
															<tr>
																<td valign="top">Name</td>
																<td colspan="2">
																	<select name="new_pax_title" id="new_pax_title">
																		<option value="">Mr</option>
																		<option value="Mr"'; if($_POST['new_pax_title'] == 'Mr') {echo 'selected';} echo'>Mr</option>
																		<option value="Mrs"'; if($_POST['new_pax_title'] == 'Mrs') {echo 'selected';} echo'>Mrs</option>
																		<option value="Ms"'; if($_POST['new_pax_title'] == 'Ms') {echo 'selected';} echo'>Ms</option>
																		<option value="Dr"'; if($_POST['new_pax_title'] == 'Dr') {echo 'selected';} echo'>Dr</option>
																		<option value="Prof"'; if($_POST['new_pax_title'] == 'Prof') {echo 'selected';} echo'>Prof</option>
																	</select>
																	<input type="text" name="new_pax_fname" id="new_pax_fname" placeholder="First Name" value="'.$_POST['new_pax_fname'].'">
																	<input type="text" name="new_pax_lname" id="new_pax_lname" placeholder="Last Name" value="'.$_POST['new_pax_lname'].'"><br/>
																	<input type="text" name="new_pax_mobile" id="new_pax_mobile" placeholder="Mobile" value="'.$_POST['new_pax_mobile'].'">
																	<input type="text" name="new_pax_phone" id="new_pax_phone" placeholder="Phone" value="'.$_POST['new_pax_phone'].'"><br/>
																	<input type="text" name="new_pax_email" id="new_pax_email" placeholder="Email" value="'.$_POST['new_pax_email'].'">
																</td>
															</tr>
															<tr>
																<td>Preference:<br/>(If any)</td>
																<td colspan="2">
																	<textarea name="new_pax_preference" id="new_pax_preference" cols="30" rows="2">'; echo $_POST['new_pax_preference']; echo'</textarea>
																</td>
															</tr>
														</table>
													</td>
												</tr>';
											}
										echo'
										<tr>
											<td><span id="text_blue">Journey Type</span></td>
											<td>';
												//CANNOT EDIT Journey type once it has been entered
												If($job_ref_details['job_type']=='1')
													{
														echo'<b>ONE WAY BOOKING</b>';
													}
												else
													{
														echo '<b>RETURN BOOKING</b>';
													}
											echo'
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr  width="100%">
								<td colspan="2" valign="top">
									<table '; if($make_white_background == '1') { echo 'class="white"';} echo 'style="border: 1px solid #418594;">
										<tr>
											<td colspan="3" style="background:#b3dffa; padding:3px;">
												<input type="text" size="5" name="j1_id" id="j1_id"  value="'.$j1_details['id'].'" class="readonly bold large" readonly>
												<b>JOURNEY 1</b> - ';
												$query = "SELECT * from job__status_ids where id='".$j1_details['job_status']."'";
												$result = $database->query($query);
												$row = mysql_fetch_array($result);
												if($row['details']=="RECEIVED") {echo "<span class='status_red'>".$row['details']."</span>";}
												else if($row['details']=="CONFIRMED") {echo "<span class='status_green'>".$row['details']."</span>";}
												else if($row['details']=="DECLINED") {echo "<span class='status_strikeout'>".$row['details']."</span>";}
												else if($row['details']=="CANCELLED") {echo "<span class='status_strikeout'>".$row['details']."</span>";}
												else {return "<span class='status_black_bold'>".$row['details']."</span>";}
												if($_SESSION['ROLE_ID'] == '1') // Admin
													{
														if($j1_details['job_status'] == 10) // Received - show buttons - Confirm, Decline, Cancel
															{ 
																echo '&nbsp;&nbsp;<input type="button" name="confirm_job_1" id="confirm_job_1" value="Confirm" class="sml_btn" title="Confirm Booking" />';
																echo '&nbsp; &nbsp; <input type="button" name="decline_job_1" id="decline_job_1" value="Decline"  class="sml_btn" title="Decline Booking"  />';
																echo '&nbsp; &nbsp; <input type="button" name="cancel_job_1" id="cancel_job_1" value="Cancel"  class="sml_btn" title="Cancel Booking"  />';
																echo '&nbsp; &nbsp; <input type="button" name="clone_job_1" id="clone_job_1"  value="Clone" class="sml_btn" title="Clone Booking" />';
																echo '&nbsp; &nbsp; <input type="button" name="clone_job_1" id="rev_clone_job_1"  value="Rev-Clone" class="sml_btn" title="Reverse Clone Booking" />';
															}
														if($j1_details['job_status'] == '20') // Confirmed
															{
																echo '&nbsp; &nbsp; <input type="button" name="cancel_job_1" id="cancel_job_1" value="Cancel"  class="sml_btn" title="Cancel Booking"  />';
																echo '&nbsp; &nbsp; <input type="button" name="clone_job_1" id="clone_job_1"  value="Clone" class="sml_btn" title="Clone Booking" />';
																echo '&nbsp; &nbsp; <input type="button" name="clone_job_1" id="rev_clone_job_1"  value="Rev-Clone" class="sml_btn" title="Reverse Clone Booking" />';
															}	
														if($j1_details['job_status'] == '90') // Declined
															{
																echo '&nbsp;&nbsp;<input type="button" name="reactivate_job_1" id="reactivate_job_1" value="Re-activate" class="sml_btn" title="Reactivate Booking" />';
																echo '&nbsp; &nbsp; <input type="button" name="cancel_job_1" id="cancel_job_1" value="Cancel"  class="sml_btn" title="Cancel Booking"  />';
																echo '&nbsp; &nbsp; <input type="button" name="clone_job_1" id="clone_job_1"  value="Clone" class="sml_btn" title="Clone Booking" />';
																echo '&nbsp; &nbsp; <input type="button" name="clone_job_1" id="rev_clone_job_1"  value="Rev-Clone" class="sml_btn" title="Reverse Clone Booking" />';
																echo '&nbsp; &nbsp; <input type="button" name="delete_job_1" id="delete_job_1"  value="DELETE" class="sml_btn" title="Delete this Booking" />';
															}
														if($j1_details['job_status'] == '100') // Cancelled
															{
																echo '&nbsp;&nbsp;<input type="button" name="reactivate_job_1" id="reactivate_job_1" value="Re-activate" class="sml_btn" title="Reactivate Booking" />';
																echo '&nbsp; &nbsp; <input type="button" name="clone_job_1" id="clone_job_1"  value="Clone" class="sml_btn" title="Clone Booking" />';
																echo '&nbsp; &nbsp; <input type="button" name="clone_job_1" id="rev_clone_job_1"  value="Rev-Clone" class="sml_btn" title="Reverse Clone Booking" />';
																echo '&nbsp; &nbsp; <input type="button" name="delete_job_1" id="delete_job_1"  value="DELETE" class="sml_btn" title="Delete this Booking" />';
															}
													}
											echo'
											</td>
										</tr>
										<tr>
											<td>State</td>
											<td colspan="2">
												<select name="j1_state" id="j1_state">';
												$database = new database();
												$query = "SELECT * from variable__states order by order_id ASC";
												$result = $database->query($query);
												while ($row = mysql_fetch_array($result))
													{
														echo '<option value="'.$row['details'].'"'; if($j1_details['frm_state'] == $row['details']) {echo 'selected';} echo'>'.$row['details'].'</option>';
													}
												echo '
												</select>
											</td>
										</tr>
										<tr>
											<td><b>Date & Time</b></td>
											<td colspan="2">
												<input placeholder="Date" name="j1_date" class="large_bold" id="j1_date" size="22" type="text" value="'.formatDate($j1_details['job_date'],1).'"/>
												<input name="j1_date_cap" id="j1_date_cap" type="hidden" value="'.$j1_details['job_date'].'"/>
												<input type="text" placeholder="Time" class="large_bold" size="4" name="j1_time" id="j1_time"  value="'.showTimeWithoutSeconds($j1_details['job_time']).'"/>
											</td>
										</tr>';
										if($_SESSION['ROLE_ID'] == '1') // Admin
											{
												echo'
												<tr>
													<td valign="top">Driver Status - ';
														$database = new database();
														$query = "SELECT * from job__driver_status_ids where id='".$j1_details['driver_status']."'";
														$result = $database->query($query);
														$row = mysql_fetch_array($result);
														$j1_driver_status 			= $row['id'];
														$j1_driver_status_details  	= $row['details'];
														if($j1_driver_status !='')
															{
																if($j1_driver_status_details=="ALLOCATED") {echo "<span class='status_red'>".$row['details']."</span>";}
																if($j1_driver_status_details=="OFFERED") {echo "<span class='status_blue'>".$row['details']."</span>";}
																if($j1_driver_status_details=="ACCEPTED") {echo "<span class='status_blue'>".$row['details']."</span>";}
																if($j1_driver_status_details=="ROGERED") {echo "<span class='status_green'>".$row['details']."</span>";}
																if($j1_driver_status_details=="PAID") {echo "<span class='status_strikeout'>".$row['details']."</span>";}
															}
													echo'
													</td>
													<td colspan="2">';
														if($j1_driver_status == '' || $j1_driver_status == '1' || $j1_driver_status == '2')	//means current status is either nothing or Allocated or Offered
															{
																echo '
																<select name="j1_driver_status" id="j1_driver_status">
																	<option value=""';  if($j1_details['driver_status'] == '0') {echo 'selected';} echo'></option>
																	<option value="1"'; if($j1_details['driver_status'] == '1') {echo 'selected';} echo'>Allocate</option>
																	<option value="2"'; if($j1_details['driver_status'] == '2') {echo 'selected';} echo'>Offer</option>
																	<option value="3"'; if($j1_details['driver_status'] == '3') {echo 'selected';} echo'>Accepted</option>
																</select>
																<span id="j1_driver_row">'; 
																		if($j1_driver_status == '1') // Allocated
																			{
																				//find the driver who has been allocated the job
																				$query1 = "SELECT MAX(id) as id FROM job__driver where job_id='".$j1_details['id']."' AND  allocated_to!='0'";
																				$result1 = $database->query($query1);
																				$row1 = mysql_fetch_array($result1);
																				if($row1['id'] != '')
																					{
																						$query2 = "SELECT * from job__driver where id='".$row1['id']."'";
																						$result2 = $database->query($query2);
																						$row2 = mysql_fetch_array($result2);
																						$j1_allocated_to_driver_id = $row2['allocated_to'];
																						$j1_allocated_price = $row2['allocated_amount'];
																					}
																			}
																		echo'
																		<select name="j1_driver" id="j1_driver" >
																		<option value="">Please Select</option>';
																		$query = "SELECT * FROM user where role_id='2' order by id ASC";
																		$database = new database;
																		$result = $database->query($query);
																		while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
																			{
																				echo '<option value="'.$row['id'].'"';if($row['id'] == $j1_allocated_to_driver_id) {echo 'selected';} echo'>'.$row['fname'].' '.$row['lname'].'</option>';
																			}
																		echo '</select> 
																</span>
																<span id="j1_driver_row_multiple"> 
																	<select name="j1_drivers[]" id="j1_driver2" class="chosen" multiple="true" style="width:200px;" >
																		<option value="">Please Select</option>';
																		$query = "SELECT * FROM user where role_id='2' order by id ASC";
																		$database = new database;
																		$result = $database->query($query);
																		while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
																			{
																				echo "<option value=$row[id]>$row[fname] $row[lname]</option>";
																			}
																		echo '</select>
																</span>
																<span id="j1_driver_price_span" > Price: <input type="text" name="j1_driver_price" id="j1_driver_price" value="'.$j1_allocated_price.'" placeholder="0.00" size="5"><br/></span>';
																if($j1_driver_status == '1' || $j1_driver_status == '2')
																	{
																		echo'<input type="button" class="sml_btn"  name="j1_tear_off" id="j1_tear_off" value="Tear Off">';
																	}
															}
														if($j1_driver_status == '3' || $j1_driver_status == '4')	//Accepted or rogered
															{
																$query1 = "SELECT MAX(id) as id FROM job__driver where job_id='".$j1_details['id']."' AND  accepted_by!='0'";
																$database = new database;
																$result1 = $database->query($query1);
																$row1 = mysql_fetch_array($result1);
																$query2 = "SELECT * from job__driver where id='".$row1['id']."'";
																$result2 = $database->query($query2);
																$row2 = mysql_fetch_array($result2);
																$accepted_by = $user->getUserDetails($row2['accepted_by']);
																echo '
																<b>$'.$row2['accepted_amount'].'</b> - '.$accepted_by['fname'].' '.$accepted_by['lname'].' (Ph- '.$accepted_by['mobile'].' '.$accepted_by['phone'].')
																<input type="button" class="sml_btn"  name="j1_resend_details" id="j1_resend_details" value="Re-send">
																<input type="button" class="sml_btn"  name="j1_pay_now" id="j1_pay_now" value="Pay Now">
																<input type="button" class="sml_btn"  name="j1_tear_off" id="j1_tear_off" value="Tear Off">
																<input type="button" class="sml_btn"  name="j1_extra_now" id="j1_extra_now" value="Pay Extra">';
															}	
														if($j1_driver_status == '5')	//Paid
															{
																$query1 = "SELECT MAX(id) as id FROM job__driver where job_id='".$j1_details['id']."' AND  accepted_by!='0'";
																$database = new database;
																$result1 = $database->query($query1);
																$row1 = mysql_fetch_array($result1);
																$query2 = "SELECT * from job__driver where id='".$row1['id']."'";
																$result2 = $database->query($query2);
																$row2 = mysql_fetch_array($result2);
																$accepted_by = $user->getUserDetails($row2['accepted_by']);
																echo '
																<b>$'.$row2['accepted_amount'].'</b> - '.$accepted_by['fname'].' '.$accepted_by['lname'].' (Ph- '.$accepted_by['mobile'].' '.$accepted_by['phone'].')
																<input type="button" class="sml_btn"  name="j1_extra_now" id="j1_extra_now" value="Pay Extra">';
															}	
													echo '
													</td>
												</tr>
												<tr id="j1_row_extra_payments">
													<td valign="top">Extra Payments</td>
													<td valign="top">';
														$query = "SELECT * from job__driver_extra_pay where job_id='".$j1_details['id']."' order by created_on DESC";
														$result = $database->query($query);
														while($row = mysql_fetch_array($result))
															{
																echo 'Paid on '.$row['created_on'].' $'.$row['amount'].' - '.$row['notes'].'<br/>';
															}
														echo'
														<input type="text" size="30" placeholder="Extra Driver Fee Notes" id="j1_driver_ex_notes" name="j1_driver_ex_notes" value="'.$j1_details['driver_ex_notes'].'" />
														<input type="text" name="j1_driver_ex_price" id="j1_driver_ex_price" placeholder="0.00" size="5" value="'.$j1_details['driver_ex_price'].'" >
														<input type="button" class="sml_btn"  name="j1_pay_extra" id="j1_pay_extra" value="Pay Now">
													</td>
												</tr>';
											}
										echo'
										<tr>
											<td valign="top">
												<table>
													<tr>
														<td colspan="2"><b>FROM</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
														if($_SESSION['ROLE_ID'] == '1' || ( $_SESSION['ROLE_ID'] == '3' && $charge_account_details['acc_type'] != '3')) // Customer
															{
																echo '<input type="button" class="sml_btn"  name="pax_add_1" id="pax_add_1" value="History">';
															}
														echo'
															<input type="button" class="sml_btn"  name="poi_1" id="poi_1" value="POI">
														</td>
													</tr>
													<tr>
														<td colspan="2"><input placeholder="Flight Number" class="bold_text"  size="12" name="j1_flight_no" type="text" value="'.$j1_details['frm_flight_no'].'"></td>
													</tr>
													<tr>
														<td colspan="2"><input placeholder="Address Line 1" size="30" name="j1_line_1" id="j1_line_1" type="text" value="'.$j1_details['frm_line1'].'"></td>
													<tr>
														<td colspan="2"><input placeholder="Address Line 2" size="30" name="j1_line_2" id="j1_line_2" type="text" value="'.$j1_details['frm_line2'].'"></td>
													<tr>
														<td><input placeholder="Suburb" class="bold_text"  size="18" name="j1_sub" id="j1_sub" type="text" value="'.$j1_details['frm_sub'].'"></td>
														<td><input placeholder="Postcode" size="3" name="j1_pc" id="j1_pc" type="text" value="'.$j1_details['frm_pc'].'"></td>
													</tr>
												</table>
											</td>
											<td valign="top">
												<table>
													<tr>
														<td colspan="2">
															<b>TO</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
															if($_SESSION['ROLE_ID'] == '1' || ( $_SESSION['ROLE_ID'] == '3' && $charge_account_details['acc_type'] != '3')) // Customer
																{
																	echo'<input type="button" class="sml_btn" name="pax_add_2" id="pax_add_2" value="History">';
																}
															echo'
															<input type="button" class="sml_btn" name="poi_2" id="poi_2" value="POI">
														</td>
													</tr>
													<tr>
														<td colspan="2"><input placeholder="Flight Number" class="bold_text"  size="12" name="j1_to_flight_no" type="text"  value="'.$j1_details['to_flight_no'].'">
														</td>
													</tr>
													<tr>
														<td colspan="2"><input placeholder="Address Line 1" size="30" name="j1_to_line_1" id="j1_to_line_1" type="text" value="'.$j1_details['to_line1'].'">
														</td>
													</tr>
													<tr>
														<td colspan="2"><input placeholder="Address Line 2" size="30" name="j1_to_line_2" id="j1_to_line_2" type="text" value="'.$j1_details['to_line2'].'">
														</td>
													</tr>
													<tr>
														<td><input placeholder="Suburb" class="bold_text"  size="18" name="j1_to_sub" id="j1_to_sub" type="text" value="'.$j1_details['to_sub'].'"></td>
														<td><input placeholder="Postcode" size="3" name="j1_to_pc" id="j1_to_pc" type="text" value="'.$j1_details['to_pc'].'"></td>
													</tr>
												</table>
											</td>
											<td valign="top">
												<table>
													<tr>
														<td>Car:</td>
														<td>
															<select name="j1_car_id" id="j1_car_id">';
															$query = "SELECT * FROM variable__car_type order by order_id ASC";
															$database = new database;
															$result = $database->query($query);
															while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
																{
																	echo "<option value='".$row['id']."'";if($j1_details['car_id']== $row['id']) {echo'selected';} echo ">$row[details]</option>";
																}
															echo '</select>
														</td>
													</tr>
													<tr>
														<td>Pax:</td>
														<td>
															<select name="j1_pax_nos" id="j1_pax_nos" style="width:50%;">';
															for($x = 1; $x <= $_MAX_PAX_NOS; $x++)
																{
																	echo '<option value="'.$x.'"';if($j1_details['pax_nos']== $x) {echo'selected';} echo '>'.$x.'</option>';
																}
															echo'</select>
														</td>
													</tr>
													<tr>
														<td>Luggage:</td>
														<td>
															<select name="j1_luggage" id="j1_luggage" style="width:50%;">';
															for($x = 0; $x <= $_MAX_LUGGAGE; $x++)
																{
																	echo '<option value="'.$x.'"';if($j1_details['luggage']== $x) {echo'selected';} echo '>'.$x.'</option>';
																}
															echo'</select>
														</td>
													</tr>
													<tr>
														<td>Capsule:</td>
														<td>
															<select name="j1_baby_capsules" id="j1_baby_capsules" style="width:50%;">';
															for($x = 0; $x <= $_MAX_CAPSULE; $x++)
																{
																	echo '<option value="'.$x.'"';if($j1_details['baby_capsule']== $x) {echo'selected';} echo '>'.$x.'</option>';
																}
															echo'</select>
														</td>
													</tr>
													<tr>
														<td>Baby Seat:</td>
														<td>
															<select name="j1_baby_seats" id="j1_baby_seats" style="width:50%;">';
															for($x = 0; $x <= $_MAX_BABY_SEAT; $x++)
																{
																	echo '<option value="'.$x.'"';if($j1_details['baby_seat']== $x) {echo'selected';} echo '>'.$x.'</option>';
																}
															echo'</select>
														</td>
													</tr>
													<tr>
														<td>Booster:</td>
														<td>
															<select name="j1_booster_seats" id="j1_booster_seats" style="width:50%;">';
															for($x = 0; $x <= $_MAX_BOOSTER_SEAT; $x++)
																{
																	echo '<option value="'.$x.'"';if($j1_details['booster_seat']== $x) {echo'selected';} echo '>'.$x.'</option>';
																}
															echo'</select>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td align="left"><input type="button" class="sml_btn"  name="j1_via" id="j1_via" value="VIA"></td>
											<td align="left"><input type="button" class="sml_btn"  name="j1_to_via" id="j1_to_via" value="VIA"></td>
											<td align="left"></td>
										</tr>
										</tr>
										<tr id="j1_via_row">
											<td valign="top">';
													if($j1_details['frm_via_line1'] !='')
														{
															echo '<table>';
														}
													else
														{
															echo '<table id="j1_via_table">';
														}
													echo'
													<tr>
														<td colspan="2"><b>FROM - VIA</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
														if($_SESSION['ROLE_ID'] == '1' || ( $_SESSION['ROLE_ID'] == '3' && $charge_account_details['acc_type'] != '3')) // Customer
															{
																echo'<input type="button" class="sml_btn"  name="pax_add_5" id="pax_add_5" value="History">';
															}
														echo'
															<input type="button" class="sml_btn"  name="poi_5" id="poi_5" value="POI">
														</td>
													</tr>
													<tr>
														<td colspan="2"><input placeholder="Address Line 1" size="30" name="j1_via_line_1" id="j1_via_line_1" type="text" value="'.$j1_details['frm_via_line1'].'"></td>
													<tr>
														<td colspan="2"><input placeholder="Address Line 2" size="30" name="j1_via_line_2" id="j1_via_line_2" type="text" value="'.$j1_details['frm_via_line2'].'"</td>
													</tr>
													<tr>
														<td><input placeholder="Suburb" class="bold_text"  size="18" name="j1_via_sub" id="j1_via_sub" type="text" value="'.$j1_details['frm_via_sub'].'"></td>
														<td><input placeholder="Postcode" size="3" name="j1_via_pc" id="j1_via_pc" type="text" value="'.$j1_details['frm_via_pc'].'"></td>
													</tr>
												</table>
											</td>
											<td valign="top">';
													if($j1_details['to_via_line1'] !='')
														{
															echo '<table>';
														}
													else
														{
															echo '<table id="j1_to_via_table">';
														}
													echo'
													<tr>
														<td colspan="2">
															<b>TO - VIA</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
															if($_SESSION['ROLE_ID'] == '1' || ( $_SESSION['ROLE_ID'] == '3' && $charge_account_details['acc_type'] != '3')) // Customer
																{
																	echo'<input type="button" class="sml_btn" name="pax_add_6" id="pax_add_6" value="History">';
																}
															echo'
															<input type="button" class="sml_btn" name="poi_6" id="poi_6" value="POI">
														</td>
													</tr>
													<tr>
														<td colspan="2"><input placeholder="Address Line 1" size="30" name="j1_to_via_line_1" id="j1_to_via_line_1" type="text" value="'.$j1_details['to_via_line1'].'">
														</td>
													</tr>
													<tr>
														<td colspan="2"><input placeholder="Address Line 2" size="30" name="j1_to_via_line_2" id="j1_to_via_line_2" type="text" value="'.$j1_details['to_via_line2'].'">
														</td>
													</tr>
													<tr>
														<td><input placeholder="Suburb" class="bold_text"  size="18" name="j1_to_via_sub" id="j1_to_via_sub" type="text" value="'.$j1_details['to_via_sub'].'"></td>
														<td><input placeholder="Postcode" size="3" name="j1_to_via_pc" id="j1_to_via_pc" type="text" value="'.$j1_details['to_via_pc'].'"></td>
													</tr>
												</table>
											</td>
											<td valign="top">
												
											</td>
										</tr>
									</table>
								</td>
							</tr>';
							if($job_ref_details['job_type'] =='2')
								{
									echo'
									<tr class="return_feilds">
										<td colspan="2" valign="top">
											<table '; if($make_white_background == '2') { echo 'class="white"';} echo 'style="border: 1px solid #418594;">
												<tr>
													<td colspan="3" style="background:#b3dffa; padding:3px;">
													<input type="text" size="5" name="j2_id" id="j2_id"  value="'.$j2_details['id'].'" class="readonly bold large" readonly>
													<b>JOURNEY 2</b> - ';
														$query = "SELECT * from job__status_ids where id='".$j2_details['job_status']."'";
														$result = $database->query($query);
														$row = mysql_fetch_array($result);
														if($row['details']=="RECEIVED") {echo "<span class='status_red'>".$row['details']."</span>";}
														else if($row['details']=="CONFIRMED") {echo "<span class='status_green'>".$row['details']."</span>";}
														else if($row['details']=="DECLINED") {echo "<span class='status_strikeout'>".$row['details']."</span>";}
														else if($row['details']=="CANCELLED") {echo "<span class='status_strikeout'>".$row['details']."</span>";}
														else {return "<span class='status_red'>".$row['details']."</span>";}
														if($_SESSION['ROLE_ID'] == '1') // Admin
															{
																if($j2_details['job_status'] == 10) // Received - show buttons - Confirm, Decline, Cancel
																	{ 
																		echo '&nbsp;&nbsp;<input type="button" name="confirm_job_2" id="confirm_job_2" value="Confirm" class="sml_btn" title="Confirm Booking" />';
																		echo '&nbsp; &nbsp; <input type="button" name="decline_job_2" id="decline_job_2" value="Decline"  class="sml_btn" title="Decline Booking"  />';
																		echo '&nbsp; &nbsp; <input type="button" name="cancel_job_2" id="cancel_job_2" value="Cancel"  class="sml_btn" title="Cancel Booking"  />';
																		echo '&nbsp; &nbsp; <input type="button" name="clone_job_2" id="clone_job_2"  value="Clone" class="sml_btn" title="Clone Booking" />';
																		echo '&nbsp; &nbsp; <input type="button" name="clone_job_2" id="rev_clone_job_2"  value="Rev-Clone" class="sml_btn" title="Reverse Clone Booking" />';
																	}
																if($j2_details['job_status'] == '20') // Confirmed
																	{
																		echo '&nbsp; &nbsp; <input type="button" name="cancel_job_2" id="cancel_job_2" value="Cancel"  class="sml_btn" title="Cancel Booking"  />';
																		echo '&nbsp; &nbsp; <input type="button" name="clone_job_2" id="clone_job_2"  value="Clone" class="sml_btn" title="Clone Booking" />';
																		echo '&nbsp; &nbsp; <input type="button" name="clone_job_2" id="rev_clone_job_2"  value="Rev-Clone" class="sml_btn" title="Reverse Clone Booking" />';
																	}	
																if($j2_details['job_status'] == '90') // Declined
																	{
																		echo '&nbsp;&nbsp;<input type="button" name="reactivate_job_2" id="reactivate_job_2" value="Re-activate" class="sml_btn" title="Reactivate Booking" />';
																		echo '&nbsp; &nbsp; <input type="button" name="cancel_job_2" id="cancel_job_2" value="Cancel"  class="sml_btn" title="Cancel Booking"  />';
																		echo '&nbsp; &nbsp; <input type="button" name="clone_job_2" id="clone_job_2"  value="Clone" class="sml_btn" title="Clone Booking" />';
																		echo '&nbsp; &nbsp; <input type="button" name="clone_job_2" id="rev_clone_job_2"  value="Rev-Clone" class="sml_btn" title="Reverse Clone Booking" />';
																		echo '&nbsp; &nbsp; <input type="button" name="delete_job_2" id="delete_job_2"  value="DELETE" class="sml_btn" title="Delete this Booking" />';
																	}
																if($j2_details['job_status'] == '100') // Cancelled
																	{
																		echo '&nbsp;&nbsp;<input type="button" name="reactivate_job_2" id="reactivate_job_2" value="Re-activate" class="sml_btn" title="Reactivate Booking" />';
																		echo '&nbsp; &nbsp; <input type="button" name="clone_job_2" id="clone_job_2"  value="Clone" class="sml_btn" title="Clone Booking" />';
																		echo '&nbsp; &nbsp; <input type="button" name="clone_job_2" id="rev_clone_job_2"  value="Rev-Clone" class="sml_btn" title="Reverse Clone Booking" />';
																		echo '&nbsp; &nbsp; <input type="button" name="delete_job_2" id="delete_job_2"  value="DELETE" class="sml_btn" title="Delete this Booking" />';
																	}
															}
													echo'
													</td>
												</tr>
												<tr class="return_feilds">
													<td>State</td>
													<td colspan="2">
														<select name="j2_state" id="j2_state">';
														$query = "SELECT * from variable__states order by order_id ASC";
														$result = $database->query($query);
														while ($row = mysql_fetch_array($result))
															{
																echo "<option value='".$row['details']."'";if($row['details'] == $j2_details['to_state']) {echo 'selected';} echo ">".$row['details']."</option>";
															}
														echo '
														</select>
													</td>
												</tr>
												<tr class="return_feilds">
													<td><b>Return Date & Time</b></td>
													<td colspan="2">
														<input placeholder="Date" name="j2_date" class="large_bold" id="j2_date" size="22" type="text" value="'.formatDate($j2_details['job_date'],1).'">
														<input name="j2_date_cap" id="j2_date_cap" type="hidden" value="'.$j2_details['job_date'].'">
														<input placeholder="Time" type="text" class="large_bold" size="4" name="j2_time" id="j2_time" value="'.showTimeWithoutSeconds($j2_details['job_time']).'">
													</td>
												</tr>';
												if($_SESSION['ROLE_ID'] == '1') // Admin
													{
														echo'
														<tr class="return_feilds" >
															<td valign="top">Driver Status - ';
																$database = new database();
																$query = "SELECT * from job__driver_status_ids where id='".$j2_details['driver_status']."'";
																$result = $database->query($query);
																$row = mysql_fetch_array($result);
																$j2_driver_status 			= $row['id'];
																$j2_driver_status_details  	= $row['details'];
																if($j2_driver_status !='')
																	{
																		if($j2_driver_status_details=="ALLOCATED") {echo "<span class='status_red'>".$row['details']."</span>";}
																		if($j2_driver_status_details=="OFFERED") {echo "<span class='status_blue'>".$row['details']."</span>";}
																		if($j2_driver_status_details=="ACCEPTED") {echo "<span class='status_blue'>".$row['details']."</span>";}
																		if($j2_driver_status_details=="ROGERED") {echo "<span class='status_green'>".$row['details']."</span>";}
																		if($j2_driver_status_details=="PAID") {echo "<span class='status_strikeout'>".$row['details']."</span>";}
																	}
															echo'
															</td>
															<td colspan="2">';
																if($j2_driver_status == '' || $j2_driver_status == '1' || $j2_driver_status == '2')	//means current status is either nothing or Allocated or Offered
																	{
																		echo '
																		<select name="j2_driver_status" id="j2_driver_status">
																			<option value=""';  if($j2_details['driver_status'] == '0') {echo 'selected';} echo'></option>
																			<option value="1"'; if($j2_details['driver_status'] == '1') {echo 'selected';} echo'>Allocate</option>
																			<option value="2"'; if($j2_details['driver_status'] == '2') {echo 'selected';} echo'>Offer</option>
																			<option value="3"'; if($j2_details['driver_status'] == '3') {echo 'selected';} echo'>Accepted</option>
																		</select>
																		<span id="j2_driver_row">'; 
																			if($j2_driver_status == '1') // Allocated
																				{
																					//find the driver who has been allocated the job
																					$query1 = "SELECT MAX(id) as id FROM job__driver where job_id='".$j2_details['id']."' AND  allocated_to!='0'";
																					$result1 = $database->query($query1);
																					$row1 = mysql_fetch_array($result1);
																					if($row1['id'] != '')
																						{
																							$query2 = "SELECT * from job__driver where id='".$row1['id']."'";
																							$result2 = $database->query($query2);
																							$row2 = mysql_fetch_array($result2);
																							$j2_allocated_to_driver_id = $row2['allocated_to'];
																							$j2_allocated_price = $row2['allocated_amount'];
																						}
																				}
																			echo'
																			<select name="j2_driver" id="j2_driver" >
																			<option value="">Please Select</option>';
																			$query = "SELECT * FROM user where role_id='2' order by id ASC";
																			$database = new database;
																			$result = $database->query($query);
																			$d_selected='';
																			while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
																				{
																					echo '<option value="'.$row['id'].'"';if($row['id'] == $j2_allocated_to_driver_id) {echo 'selected';} echo'>'.$row['fname'].' '.$row['lname'].'</option>';
																				}
																			echo '</select> 
																		</span>
																		<span id="j2_driver_row_multiple"> 
																			<select name="j2_drivers[]" id="j2_driver2" class="chosen" multiple="true" style="width:200px;" >
																				<option value="">Please Select</option>';
																				$query = "SELECT * FROM user where role_id='2' order by id ASC";
																				$database = new database;
																				$result = $database->query($query);
																				while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
																					{
																						echo "<option value=$row[id]>$row[fname] $row[lname]</option>";
																					}
																				echo '</select>
																		</span>
																		<span id="j2_driver_price_span" > Price: <input type="text" name="j2_driver_price" id="j2_driver_price" value="'.$j2_allocated_price.'" placeholder="0.00" size="5"><br/></span>';
																		if($j2_driver_status == '1' || $j2_driver_status == '2')
																			{
																				echo'<input type="button" class="sml_btn"  name="j2_tear_off" id="j2_tear_off" value="Tear Off">';
																			}
																	}
																if($j2_driver_status == '3' || $j2_driver_status == '4')	//Accepted or Rogered
																	{
																		$query1 = "SELECT MAX(id) as id FROM job__driver where job_id='".$j2_details['id']."' AND  accepted_by!='0'";
																		$database = new database;
																		$result1 = $database->query($query1);
																		$row1 = mysql_fetch_array($result1);
																		$query2 = "SELECT * from job__driver where id='".$row1['id']."'";
																		$result2 = $database->query($query2);
																		$row2 = mysql_fetch_array($result2);
																		$accepted_by = $user->getUserDetails($row2['accepted_by']);
																		echo '
																		<b>$'.$row2['accepted_amount'].'</b> - '.$accepted_by['fname'].' '.$accepted_by['lname'].' (Ph- '.$accepted_by['mobile'].' '.$accepted_by['phone'].')
																		<input type="button" class="sml_btn"  name="j2_resend_details" id="j2_resend_details" value="Re-send">
																		<input type="button" class="sml_btn"  name="j2_pay_now" id="j2_pay_now" value="Pay Now">
																		<input type="button" class="sml_btn"  name="j2_tear_off" id="j2_tear_off" value="Tear Off">
																		<input type="button" class="sml_btn"  name="j2_extra_now" id="j2_extra_now" value="Pay Extra">';
																	}	
																if($j2_driver_status == '5')	//Paid
																	{
																		$query1 = "SELECT MAX(id) as id FROM job__driver where job_id='".$j2_details['id']."' AND  accepted_by!='0'";
																		$database = new database;
																		$result1 = $database->query($query1);
																		$row1 = mysql_fetch_array($result1);
																		$query2 = "SELECT * from job__driver where id='".$row1['id']."'";
																		$result2 = $database->query($query2);
																		$row2 = mysql_fetch_array($result2);
																		$accepted_by = $user->getUserDetails($row2['accepted_by']);
																		echo '
																		<b>$'.$row2['accepted_amount'].'</b> - '.$accepted_by['fname'].' '.$accepted_by['lname'].' (Ph- '.$accepted_by['mobile'].' '.$accepted_by['phone'].')
																		<input type="button" class="sml_btn"  name="j2_extra_now" id="j2_extra_now" value="Pay Extra">';
																		
																	}
															echo '
															</td>
														</tr>
														<tr id="j2_row_extra_payments"  class="return_feilds">
															<td valign="top">Extra Payments</td>
															<td valign="top">';
																$query = "SELECT * from job__driver_extra_pay where job_id='".$j2_details['id']."' order by created_on DESC";
																$result = $database->query($query);
																while($row = mysql_fetch_array($result))
																	{
																		echo 'Paid on '.$row['created_on'].' $'.$row['amount'].' - '.$row['notes'].'<br/>';
																	}
																echo'
																<input type="text" size="30" placeholder="Extra Driver Fee Notes" id="j2_driver_ex_notes" name="j2_driver_ex_notes"/>
																<input type="text" name="j2_driver_ex_price" id="j2_driver_ex_price" placeholder="0.00" size="5" />
																<input type="button" class="sml_btn"  name="j2_pay_extra" id="j2_pay_extra" value="Pay Now">
															</td>
														</tr>';
													}
												echo'
												<tr class="return_feilds">
													<td>
														<table>
															<tr>
																<td colspan="2">
																	<b>FROM</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
																	if($_SESSION['ROLE_ID'] == '1' || ( $_SESSION['ROLE_ID'] == '3' && $charge_account_details['acc_type'] != '3')) // Customer
																		{
																			echo'<input type="button" class="sml_btn" name="pax_add_3" id="pax_add_3" value="History">';
																		}
																	echo'
																	<input type="button" class="sml_btn" name="poi_3" id="poi_3" value="POI">
																</td>
															</tr>
															<tr>
																<td colspan="2"><input placeholder="Flight Number" class="bold_text"  size="12" name="j2_flight_no" type="text" value="'.$j2_details['frm_flight_no'].'"></td>
															</tr>
															<tr>
																<td colspan="2"><input placeholder="Address Line 1" size="30" name="j2_line_1" id="j2_line_1" type="text" value="'.$j2_details['frm_line1'].'"></td>
															</tr>
															<tr>
																<td colspan="2"><input placeholder="Address Line 2" size="30" name="j2_line_2" id="j2_line_2" type="text" value="'.$j2_details['frm_line2'].'"></td>
															</tr>
															<tr>
																<td><input placeholder="Suburb" class="bold_text"  size="18" name="j2_sub" id="j2_sub" type="text" value="'.$j2_details['frm_sub'].'"></td>
																<td><input placeholder="Postcode" size="3" name="j2_pc" id="j2_pc" type="text" value="'.$j2_details['frm_pc'].'"></td>
															</tr>
														</table>
													</td>
													<td>
														<table>
															<tr>
																<td colspan="2">
																	<b>TO</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
																	if($_SESSION['ROLE_ID'] == '1' || ( $_SESSION['ROLE_ID'] == '3' && $charge_account_details['acc_type'] != '3')) // Customer
																		{
																			echo'<input type="button" class="sml_btn" name="pax_add_4" id="pax_add_4" value="History">';
																		}
																	echo'
																	<input type="button" class="sml_btn" name="poi_4" id="poi_4" value="POI">
																</td>
															</tr>
															<tr>
																<td colspan="2"><input placeholder="Flight Number"  class="bold_text"  size="12" name="j2_to_flight_no" id="j2_to_flight_no" type="text" value="'.$j2_details['to_flight_no'].'"></td>
															</tr>
															<tr>
																<td colspan="2"><input placeholder="Address Line 1" size="30" name="j2_to_line_1" id="j2_to_line_1" type="text" value="'.$j2_details['to_line1'].'"></td>
															</tr>
															<tr>
																<td colspan="2"><input placeholder="Address Line 2" size="30" name="j2_to_line_2" id="j2_to_line_2" type="text" value="'.$j2_details['to_line2'].'"></td>
															</tr>
															<tr>
																<td><input placeholder="Suburb" class="bold_text"  size="18" name="j2_to_sub" id="j2_to_sub" type="text" value="'.$j2_details['to_sub'].'"></td>
																<td><input placeholder="Postcode" size="3" name="j2_to_pc" id="j2_to_pc" type="text" value="'.$j2_details['to_pc'].'"></td>
															</tr>
														</table>
													</td>
													<td valign="top">
														<table>
															<tr>
																<td>Car:</td>
																<td>
																	<select name="j2_car_id" id="j2_car_id">';
																	$query = "SELECT * FROM variable__car_type order by order_id ASC";
																	$database = new database;
																	$result = $database->query($query);
																	while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
																		{
																			echo "<option value='".$row['id']."'";if($j2_details['car_id']== $row['id']) {echo'selected';} echo ">$row[details]</option>";
																		}
																	echo '</select>
																</td>
															</tr>
															<tr>
																<td>Pax:</td>
																<td>
																	<select name="j2_pax_nos" id="j2_pax_nos" style="width:50%;">';
																	for($x = 1; $x <= $_MAX_PAX_NOS; $x++)
																		{
																			echo '<option value="'.$x.'"';if($j2_details['pax_nos']== $x) {echo'selected';} echo '>'.$x.'</option>';
																		}
																	echo'</select>
																</td>
															</tr>
															<tr>
																<td>Luggage:</td>
																<td>
																	<select name="j2_luggage" id="j2_luggage" style="width:50%;">';
																	for($x = 0; $x <= $_MAX_LUGGAGE; $x++)
																		{
																			echo '<option value="'.$x.'"';if($j2_details['luggage']== $x) {echo'selected';} echo '>'.$x.'</option>';
																		}
																	echo'</select>
																</td>
															</tr>
															<tr>
																<td>Capsule:</td>
																<td>
																	<select name="j2_baby_capsules" id="j2_baby_capsules" style="width:50%;">';
																	for($x = 0; $x <= $_MAX_CAPSULE; $x++)
																		{
																			echo '<option value="'.$x.'"';if($j2_details['baby_capsule']== $x) {echo'selected';} echo '>'.$x.'</option>';
																		}
																	echo'</select>
																</td>
															</tr>
															<tr>
																<td>Baby Seat:</td>
																<td>
																	<select name="j2_baby_seats" id="j2_baby_seats" style="width:50%;">';
																	for($x = 0; $x <= $_MAX_BABY_SEAT; $x++)
																		{
																			echo '<option value="'.$x.'"';if($j2_details['baby_seat']== $x) {echo'selected';} echo '>'.$x.'</option>';
																		}
																	echo'</select>
																</td>
															</tr>
															<tr>
																<td>Booster:</td>
																<td>
																	<select name="j2_booster_seats" id="j2_booster_seats" style="width:50%;">';
																	for($x = 0; $x <= $_MAX_BOOSTER_SEAT; $x++)
																		{
																			echo '<option value="'.$x.'"';if($j2_details['booster_seat']== $x) {echo'selected';} echo '>'.$x.'</option>';
																		}
																	echo'</select>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td align="left"><input type="button" class="sml_btn"  name="j2_via" id="j2_via" value="VIA"></td>
													<td align="left"><input type="button" class="sml_btn"  name="j2_to_via" id="j2_to_via" value="VIA"></td>
													<td align="left"></td>
												</tr>
												<tr id="j2_via_row">
													<td>';
														if($j2_details['frm_via_line1'] !='')
															{
																echo '<table>';
															}
														else
															{
																echo '<table id="j2_via_table">';
															}
														echo'
															<tr>
																<td colspan="2">
																	<b>FROM - VIA</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
																if($_SESSION['ROLE_ID'] == '1' || ( $_SESSION['ROLE_ID'] == '3' && $charge_account_details['acc_type'] != '3')) // Customer
																	{
																		echo'<input type="button" class="sml_btn" name="pax_add_7" id="pax_add_7" value="History">';
																	}
																echo'
																	<input type="button" class="sml_btn" name="poi_7" id="poi_7" value="POI">
																</td>
															</tr>
															<tr>
																<td colspan="2"><input placeholder="Address Line 1" size="30" name="j2_via_line_1" id="j2_via_line_1" type="text" value="'.$j2_details['frm_via_line1'].'"></td>
															</tr>
															<tr>
																<td colspan="2"><input placeholder="Address Line 2" size="30" name="j2_via_line_2" id="j2_line_via_2" type="text" value="'.$j2_details['frm_via_line2'].'"></td>
															</tr>
															<tr>
																<td><input placeholder="Suburb" class="bold_text"  size="18" name="j2_via_sub" id="j2_via_sub" type="text" value="'.$j2_details['frm_via_sub'].'"></td>
																<td><input placeholder="Postcode" size="3" name="j2_via_pc" id="j2_via_pc" type="text" value="'.$j2_details['frm_via_pc'].'"></td>
															</tr>
														</table>
													</td>
													<td>';
														if($j2_details['to_via_line1'] !='')
															{
																echo '<table>';
															}
														else
															{
																echo '<table id="j2_to_via_table">';
															}
														echo'
															<tr>
																<td colspan="2">
																	<b>TO</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
																if($_SESSION['ROLE_ID'] == '1' || ( $_SESSION['ROLE_ID'] == '3' && $charge_account_details['acc_type'] != '3')) // Customer
																	{
																		echo'<input type="button" class="sml_btn" name="pax_add_8" id="pax_add_8" value="History">';
																	}
																echo'
																	<input type="button" class="sml_btn" name="poi_8" id="poi_8" value="POI">
																</td>
															</tr>
															<tr>
																<td colspan="2"><input placeholder="Address Line 1" size="30" name="j2_to_via_line_1" id="j2_to_via_line_1" type="text" value="'.$j2_details['to_via_line1'].'"></td>
															</tr>
															<tr>
																<td colspan="2"><input placeholder="Address Line 2" size="30" name="j2_to_via_line_2" id="j2_to_via_line_2" type="text" value="'.$j2_details['to_via_line2'].'"></td>
															</tr>
															<tr>
																<td><input placeholder="Suburb" class="bold_text"  size="18" name="j2_to_via_sub" id="j2_to_via_sub" type="text" value="'.$j2_details['to_via_sub'].'"></td>
																<td><input placeholder="Postcode" size="3" name="j2_to_via_pc" id="j2_to_via_pc" type="text" value="'.$j2_details['to_via_pc'].'"></td>
															</tr>
														</table>
													</td>
													<td valign="top">
													</td>
												</tr>
											</table>
										</td>
									</tr>';
								}
						echo'
						</table>
					</td>
					<td width="10px"></td>
					<td valign="top">
						<table style="border: 1px solid #418594;" width="100%">';
							if($_SESSION['ROLE_ID'] == '1') // Admin
								{
									echo'
									<tr>
										<td>Charge Mode</td>
										<td colspan="2">
											<select name="charge_mode" id="charge_mode">';
											$query = "SELECT * FROM variable__charge_mode order by order_id ASC";
											$database = new database;
											$result = $database->query($query);
											while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
												{
													echo "<option value='".$row['id']."'";if($row['id'] == $job_ref_details['charge_mode'] ) { echo 'selected';} echo">".$row['details']."</option>";
												}
											echo '</select>
										</td>
									</tr>
									<tr>	
										<td></td>
										<td><b>Journey 1</b></td>';
										if($job_ref_details['job_type'] =='2')
											{
												echo '<td class="return_feilds"><b>Return</b></td>';
											}
										else
											{
												echo '<td></td>';
											}
									echo'
									</tr>
									<tr>
										<td>
											Kilometers
											<span id="get_guidance_kms"><img src="../Images/questionmark.jpg" height="18" width="18" style="vertical-align:top;"></span>
										</td>
										<td><input style="text-align: right;" name="j1_kms" id="j1_kms" placeholder="00" size="5" type="text" value="'.$j1_details['kms'].'"></td>';
										if($job_ref_details['job_type'] =='2')
											{
												echo '<td class="return_feilds"><input style="text-align: right;" name="j2_kms" id="j2_kms" placeholder="00" size="5" type="text"  value="'.$j2_details['kms'].'"></td>';
											}
										else
											{
												echo '<td></td>';
											}
									echo'
									</tr>';
									
										$query = "SELECT * FROM settings__revenue_field order by id ASC";
										$database = new database;
										$result = $database->query($query);
										while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
											{
												echo '
												<tr>
													<td>';
													if($row['checkbox'] == '1')
														{
															echo'<input type="checkbox" name="chk_'.$row['id'].'" id="chk_'.$row['id'].'" "/>
																<input type="hidden" name="chk_'.$row['id'].'_value" id="chk_'.$row['id'].'_value" value="'.$row['on_check_value'].'"/>
																<input type="hidden" name="chk2_'.$row['id'].'_value" id="chk2_'.$row['id'].'_value" value="'.$row['on_check_value'].'"/>';
														}
													echo'
													&nbsp;'.$row['label'].'';
													if($row['id'] == '4')
														{
															echo'&nbsp;<input type="text" name="wait_hrs" id="wait_hrs" size="3" placeholder="Hours"/>';
														}
													echo'
													</td>
													<td><input style="text-align: right;" name="j1_'.$row['name_id'].'" id="j1_'.$row['name_id'].'"';
													if($j1_details[''.$row['name_id'].''] !='0.00')
														{
															echo 'value="'.$j1_details[''.$row['name_id'].''].'"';
														}
													else
														{
															echo 'value="0.00"';
														}
													echo'size="5" type="text"></td>';
													if($job_ref_details['job_type'] =='2')
														{
															echo'
															<td class="return_feilds"><input style="text-align: right;" name="j2_'.$row['name_id'].'" id="j2_'.$row['name_id'].'"';
															if($j2_details[''.$row['name_id'].''] !='0.00')
																{
																	echo 'value="'.$j2_details[''.$row['name_id'].''].'"';
																}
															else
																{
																	echo 'value="0.00"';
																}
															echo'size="5" type="text" ></td>';
														}
												echo'
												</tr>
												';
											}
										echo '
									<tr>
										<td><b>TOTAL FARE</b></td>
										<td><input style="text-align: right;"  class="bold_text"  name="j1_tot_fare" id="j1_tot_fare" size="5" type="text" readonly ';if($j1_details['tot_fare'] != '0.00') {echo 'value="'.$j1_details['tot_fare'].'"';} else {echo 'value="0.00"';} echo'></td>';
										if($job_ref_details['job_type'] =='2')
											{
												echo'<td class="return_feilds"><input style="text-align: right;"  class="bold_text"  name="j2_tot_fare" id="j2_tot_fare" size="5" type="text" readonly ';if($j2_details['tot_fare'] != '0.00') {echo 'value="'.$j2_details['tot_fare'].'"';} else {echo 'value="0.00"';} echo'></td>';
											}
									echo'
									</tr>
									<tr>
										<td>Driver Fee</td>
										<td><input style="text-align: right;"  name="j1_drv_fee" id="j1_drv_fee" size="5" type="text" readonly ';if($j1_details['drv_fee'] != '0.00') {echo 'value="'.$j1_details['drv_fee'].'"';} else {echo 'value="0.00"';} echo'></td>';
										if($job_ref_details['job_type'] =='2')
											{	
												echo'<td class="return_feilds"><input style="text-align: right;" name="j2_drv_fee" id="j2_drv_fee" size="5" type="text" ';if($j2_details['drv_fee'] != '0.00') {echo 'value="'.$j2_details['drv_fee'].'"';} else {echo 'value="0.00"';} echo'></td>';
											}
									echo'
									</tr>
									<tr>
										<td>Other Expenses</td>
										<td><input style="text-align: right;" name="j1_oth_exp" id="j1_oth_exp" size="5" type="text" ';if($j1_details['oth_exp'] != '0.00') {echo 'value="'.$j1_details['oth_exp'].'"';} else {echo 'value="0.00"';} echo'></td>';
										
										if($job_ref_details['job_type'] =='2')
											{
												echo'<td class="return_feilds"><input style="text-align: right;" name="j2_oth_exp" id="j2_oth_exp" size="5" type="text" ';if($j2_details['oth_exp'] != '0.00') {echo 'value="'.$j2_details['oth_exp'].'"';} else {echo 'value="0.00"';} echo'></td>';
											}
									echo'
									</tr>
									<tr>
										<td><b>NET PROFIT</b></td>
										<td><input style="text-align: right;" class="bold_text" name="j1_profit" id="j1_profit" size="5" type="text" readonly ';if($j1_details['profit'] != '0.00') {echo 'value="'.$j1_details['profit'].'"';} else {echo 'value="0.00"';} echo'></td>';
										if($job_ref_details['job_type'] =='2')
											{
												echo'<td class="return_feilds"><input style="text-align: right;"  class="bold_text" name="j2_profit" id="j2_profit" size="5" type="text" readonly ';if($j2_details['profit'] != '0.00') {echo 'value="'.$j2_details['profit'].'"';} else {echo 'value="0.00"';} echo'></td>';
											}
									echo'
									</tr>';
								}
							echo'
							<tr>
								<td colspan="3" style="background:#b3dffa; padding:3px;"><b>EXTERNAL NOTES</b></td>
							</tr>
							</tr>
								<td valign="top"><span id="text_blue">Journey 1</span></td>
								<td colspan=2"><textarea cols="16" rows="4" name="j1_ext_notes">'; echo $j1_details['ext_notes']; echo'</textarea></td>
							</tr>';
							if($job_ref_details['job_type'] =='2')
								{
									echo'
									<tr id="row_j2_ext_notes">
										<td valign="top"><span id="text_blue">Journey 2</span></td>
										<td colspan=2"><textarea cols="16" rows="4" name="j2_ext_notes">'; echo $j2_details['ext_notes']; echo'</textarea></td>
									</tr>';
								}
							if($_SESSION['ROLE_ID'] == '1') //admin
								{
									echo'
									<tr>
										<td colspan="3" style="background:#b3dffa; padding:3px;"><b>INTERNAL NOTES</b></td>
									</tr>
									<tr>
										<td valign="top"><span id="text_blue">Journey 1</span></td>
										<td colspan=2"><textarea cols="16" rows="4" name="j1_int_notes">'; echo $j1_details['int_notes']; echo'</textarea></td>
									</tr>';
									if($job_ref_details['job_type'] =='2')
										{
											echo'
											<tr id="row_j2_int_notes">
												<td valign="top"><span id="text_blue">Journey 2</span></td>
												<td colspan=2"><textarea cols="16" rows="4" name="j2_int_notes">'; echo $j2_details['int_notes']; echo'</textarea></td>
											</tr>';
										}
								}
						echo'
						</table>
					</td>
				</tr>
			</table>
			</form>
		</td>';
		if($_SESSION['ROLE_ID'] == '1') //admin
			{
				echo'
				<td valign="top">
					<div id="log_background_1"><span id="reference_log_plus"><b>+</b></span>
					<span id="reference_log_minus"><b>-<b/></span>
					Booking Reference Log</b></div>
					<div id="reference_log_table">
						<table width="100%" id="log_table">
							<tr>
								<th>Log Id</th>
								<th>Entered By</th>
								<th>Created On</th>
								<th>Message</th>
								<th>Old Value</th>
								<th>New Value</th>
							</tr>';
						$query = "SELECT * FROM log__job_reference where job_reference_id='".$job_ref_details['id']."' order by id ASC";
						$database = new database;
						$result = $database->query($query);
						$num_rows = mysql_num_rows($result);
						if($num_rows >=1)
							{
								while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
									{
										$entered_by = $user->getUserDetails($row['entered_by']);
										echo'
										<tr>
											<td class="align_right">'.$row['id'].'</td>
											<td>'.$entered_by['fname'].' '.$entered_by['lname'].'</td>
											<td>'.date('d-m-Y H:i:s', strtotime($row['created_on'])).'</td>
											<td>'.$row['message'].'</td>
											<td class="align_right">'.$row['old_value'].'</td>
											<td class="align_right">'.$row['new_value'].'</td>
										</tr>';
									
									}
							}
						else
							{
								echo '<tr><td colspan="6">No Records Found...</td></tr>';
							}
						echo'
						</table>
					</div>
					<br/>
					<div id="log_background_2"><b>Booking ID '.$j1_details['id'].' Log (Journey 1)</b></div>
					<div id="log_background_3">
					<span id="j1_log_plus"><b>+</b></span>
					<span id="j1_log_minus"><b>-<b/></span>
					Booking Log</div>
					<div id="j1_log_table">
						<table width="100%" id="log_table">
							<tr>
								<th>Log Id</th>
								<th>Entered By</th>
								<th>Created On</th>
								<th>Message</th>
								<th>Old Value</th>
								<th>New Value</th>
							</tr>';
							$query = "SELECT * FROM log__job where job_id ='".$j1_details['id']."' order by id ASC";
							$database = new database;
							$result = $database->query($query);
							$num_rows = mysql_num_rows($result);
							if($num_rows >=1)
								{
									while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
										{
											$entered_by = $user->getUserDetails($row['entered_by']);
											echo'
											<tr>
												<td class="align_right">'.$row['id'].'</td>
												<td>'.$entered_by['fname'].' '.$entered_by['lname'].'</td>
												<td>'.date('d-m-Y H:i:s', strtotime($row['created_on'])).'</td>
												<td>'.$row['message'].'</td>
												<td class="align_right">'.$row['old_value'].'</td>
												<td class="align_right">'.$row['new_value'].'</td>
											</tr>';
										
										}
								}
							else
								{
									echo '<tr><td colspan="6">No Records Found...</td></tr>';
								}
							echo'
						</table>
					</div>
					<div id="log_background_3">
					<span id="j1_driver_log_plus"><b>+</b></span>
					<span id="j1_driver_log_minus"><b>-<b/></span>
					Driver Log</div>
					<div id="j1_driver_log_table">
						<table width="100%" id="log_table">
							<tr>
								<th>Log Id</th>
								<th>Entered By</th>
								<th>Created On</th>
								<th>Message</th>
								<th>Old Value</th>
								<th>New Value</th>
							</tr>';
							$query = "SELECT * FROM log__job_driver where job_id ='".$j1_details['id']."' order by id ASC";
							$database = new database;
							$result = $database->query($query);
							$num_rows = mysql_num_rows($result);
							if($num_rows >=1)
								{
									while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
										{
											$entered_by = $user->getUserDetails($row['entered_by']);
											echo'
											<tr>
												<td class="align_right">'.$row['id'].'</td>
												<td>'.$entered_by['fname'].' '.$entered_by['lname'].'</td>
												<td>'.date('d-m-Y H:i:s', strtotime($row['created_on'])).'</td>
												<td>'.$row['message'].'</td>
												<td class="align_right">'.$row['old_value'].'</td>
												<td class="align_right">'.$row['new_value'].'</td>
											</tr>';
										
										}
								}
							else
								{
									echo '<tr><td colspan="6">No Records Found...</td></tr>';
								}
							echo'
						</table>
					</div>
					<br/>';
					if($job_ref_details['job_type'] == '2')
						{
							echo'
							<div id="log_background_2"><b>Booking ID '.$j2_details['id'].' Log (Journey 2)</b></div>
							<div id="log_background_3">
							<span id="j2_log_plus"><b>+</b></span>
							<span id="j2_log_minus"><b>-<b/></span>
							Booking Log</div>
							<div id="j2_log_table">
								<table width="100%"  id="log_table">
									<tr>
										<th>Log Id</th>
										<th>Entered By</th>
										<th>Created On</th>
										<th>Message</th>
										<th>Old Value</th>
										<th>New Value</th>
									</tr>';
								$query = "SELECT * FROM log__job where job_id='".$j2_details['id']."' order by id ASC";
								$database = new database;
								$result = $database->query($query);
								$num_rows = mysql_num_rows($result);
								if($num_rows >=1)
									{
										while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
											{
												$entered_by = $user->getUserDetails($row['entered_by']);
												echo'
												<tr>
													<td class="align_right">'.$row['id'].'</td>
													<td>'.$entered_by['fname'].' '.$entered_by['lname'].'</td>
													<td>'.date('d-m-Y H:i:s', strtotime($row['created_on'])).'</td>
													<td>'.$row['message'].'</td>
													<td class="align_right">'.$row['old_value'].'</td>
													<td class="align_right">'.$row['new_value'].'</td>
												</tr>';
											
											}
									}
								else
									{
										echo '<tr><td colspan="6">No Records Found...</td></tr>';
									}
								echo'
								</table>
							</div>
							<div id="log_background_3">
							<span id="j2_driver_log_plus"><b>+</b></span>
							<span id="j2_driver_log_minus"><b>-<b/></span>
							Driver Log</div>
							<div id="j2_driver_log_table">
								<table width="100%" id="log_table">
									<tr>
										<th>Log Id</th>
										<th>Entered By</th>
										<th>Created On</th>
										<th>Message</th>
										<th>Old Value</th>
										<th>New Value</th>
									</tr>';
									$query = "SELECT * FROM log__job_driver where job_id ='".$j2_details['id']."' order by id ASC";
									$database = new database;
									$result = $database->query($query);
									$num_rows = mysql_num_rows($result);
									if($num_rows >=1)
										{
											while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
												{
													$entered_by = $user->getUserDetails($row['entered_by']);
													echo'
													<tr>
														<td class="align_right">'.$row['id'].'</td>
														<td>'.$entered_by['fname'].' '.$entered_by['lname'].'</td>
														<td>'.date('d-m-Y H:i:s', strtotime($row['created_on'])).'</td>
														<td>'.$row['message'].'</td>
														<td class="align_right">'.$row['old_value'].'</td>
														<td class="align_right">'.$row['new_value'].'</td>
													</tr>';
												
												}
										}
									else
										{
											echo '<tr><td colspan="6">No Records Found...</td></tr>';
										}
									echo'
								</table>
							</div><br/><br/>';
						}
					if($j1_details['driver_status'] == '2') //Offered
						{
							echo'
								<table width="100%" id="log_table">
									<tr>
										<th colspan="4">Offers for Booking ID '.$j1_details['id'].'</th>
									</tr>
									<tr>
										<th>Offered At</th>
										<th>Offered To</th>
										<th>Offered Amount</th>
										<th>Delete</th>
									</tr>';
									$query = "SELECT * FROM job__driver where job_id ='".$j1_details['id']."' AND offered_to !='0' order by created_on DESC";
									$database = new database;
									$result = $database->query($query);
									$num_rows = mysql_num_rows($result);
									if($num_rows >=1)
										{
											while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
												{
													$offered_to = $user->getUserDetails($row['offered_to']);
													echo'
													<tr>
														<td>'.date('d-m-Y H:i:s', strtotime($row['created_on'])).'</td>
														<td>'.$offered_to['fname'].' '.$offered_to['lname'].'</td>
														<td class="align_right">$'.$row['offered_amount'].'</td>
														<td class="align_right"><a href="#" id="'.$row['id'].'" class="delete_job_offer_j1">Delete</a></td>
													</tr>';
												
												}
										}
									else
										{
											echo '<tr><td colspan="6">No Records Found...</td></tr>';
										}
									echo'
								</table><br/><br/>
							</div>';
						}
					if($j2_details['driver_status'] == '2') //Offered
						{
							echo'
								<table width="100%" id="log_table">
									<tr>
										<th colspan="4">Offers for Booking ID '.$j2_details['id'].'</th>
									</tr>
									<tr>
										<th>Offered At</th>
										<th>Offered To</th>
										<th>Offered Amount</th>
										<th>Delete</th>
									</tr>';
									$query = "SELECT * FROM job__driver where job_id ='".$j2_details['id']."' AND offered_to !='0' order by created_on DESC";
									$database = new database;
									$result = $database->query($query);
									$num_rows = mysql_num_rows($result);
									if($num_rows >=1)
										{
											while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
												{
													$offered_to = $user->getUserDetails($row['offered_to']);
													echo'
													<tr>
														<td>'.date('d-m-Y H:i:s', strtotime($row['created_on'])).'</td>
														<td>'.$offered_to['fname'].' '.$offered_to['lname'].'</td>
														<td class="align_right">$'.$row['offered_amount'].'</td>
														<td class="align_right"><a href="#" id="'.$row['id'].'" class="delete_job_offer_j2">Delete</a></td>
													</tr>';
												
												}
										}
									else
										{
											echo '<tr><td colspan="6">No Records Found...</td></tr>';
										}
									echo'
								</table>
							</div>';
						}
					echo'
				</td>';
			}
	echo'
	</tr>
</table>';
?>