<?php
ini_set("display_errors", 1);
session_start();
require_once('../init.php');
include(''.INCLUDE_PATH.'config.php');
include(''.INCLUDE_PATH.'settings.php');
include(''.INCLUDE_PATH.'auth.php');
include(''.INCLUDE_PATH.'functions.php');
include(''.INCLUDE_PATH.'functions_date_time.php');
include(''.CLASSES_PATH.'database.php');
include(''.CLASSES_PATH.'chargeAccount.php');
include(''.CLASSES_PATH.'job.php');
require_once(''.CLASSES_PATH ."invoice.php");


$database 	= 	new database;


if (isset($_POST['edit_invoice']))
	{
		$database 			= 	new database;
		$invoice			=	new invoice();
		$invoice_details	= 	$invoice->getInvoiceDetails( $_POST['edit_invoice_id']); //this is value posted from hidden text field
		
		if($_POST['current_status_id'] != $invoice_details['status_id'])
			{
				$invoice->updateInvoiceTable($invoice_details['id'], 'status_id', $_POST['current_status_id']); 
			}
		if($_POST['notes'] != $invoice_details['notes'])
			{
				$invoice->updateInvoiceTable($invoice_details['id'], 'notes', $_POST['notes']); 
			}
		unset($_POST);
		$message = "Invoice Edited Successfully....!!!";
	}
if (isset($_POST['add_payment']))
	{
		
		if($_POST['payment_amount'] != '' )
		{
			$result_payment_amount_check = 1;
		}
		else
		{
			$error_message .= "Check - <b>Payment Amount.......Left Blank.</b><br/>";
		}
		
	//Check 2, see if contact or first name is provided
		if($_POST['paid_date'] != '')
		{
			$result_paid_date_check = 1; 
		}
		else
		{
			$error_message .= "Check - <b>Paid Date.......Left Blank.</b><br/>";
		}
		
		if($_POST['payment_method'] != '')
		{
			$result_payment_method_check = 1; 
		}
		else
		{
			$error_message .= "Check - <b>Payment Method.......Left Blank.</b><br/>";
		}
	
		//$errors = validateFields($_POST, $rules);
		if($result_payment_amount_check == '1' && $result_paid_date_check == '1' && $result_payment_method_check == '1')
			{
				$database 			= 	new database;
				$invoice			=	new invoice();
				$invoice->addInvoicePaymentTable($_POST['invoice_id'], $_POST['payment_method'],  $_POST['paid_date'], $_POST['payment_amount']);
				unset($_POST);
				$message = "Payment Added Successfully....!!!";
			}
		else{
			
		}
	}	
if (isset($_POST['delete_invoice']))
	{
		$database 	= 	new database;
		$query 		= "DELETE from invoice where id= ".$_POST['invoice_id']."";
		$result = $database->query($query);
		$message = "Invoice Deleted Succesfully";
		header('Location: ../Forms/new_invoice.php');
		exit();
	}	
$invoice 				= 	new invoice();
$invoice_details 		= 	$invoice->getInvoiceDetails( $_GET['id']);
//Get the customer name to display at the top
$query = "Select * from charge_acc where id = '".$invoice_details['charge_acc_id']."'";
$result = $database->query($query);
$row = mysql_fetch_array($result);
echo'
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
<link href="../Css/minified/jquery-ui-1.10.3.custom.min.css" rel="stylesheet"  />
	<link href="../Css/jtable/themes/lightcolor/blue/jtable.css" rel="stylesheet"  type="text/css" />
	<link href="../Css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
	<link href="../Css/style.css" rel="stylesheet"  />
	<link href="../Css/menu.css" rel="stylesheet"  />
	<link href="../Css/chosen.min.css" rel="stylesheet"  />
	<link href="../Css/autocomplete.css" rel="stylesheet"  />
	<link href="../Css/jquery.timeentry.css" rel="stylesheet"  />
	<link rel="stylesheet" type="text/css" href="../Css/easyui/themes/default/easyui.css">
	
	<script src="../Scripts/jquery-1.10.2.min.js"></script>
	<script src="../Scripts/jquery-ui.min.js"></script>
	<script src="../Scripts/jquery.timeentry.min.js"></script>
	<script src="../Scripts/jquery.jtable.min.js"></script>
	<script src="../Scripts/jquery.validate.js"></script>
	<script src="../Scripts/jquery.validationEngine.js"></script>
	<script src="../Scripts/jquery.validationEngine-en.js"></script>
	<script src="../Scripts/chosen.jquery.min.js"></script>
	<script src="../Scripts/invoicing.js"></script>
	<script src="../Scripts/jquery.easyui.min.js"></script>
	<script src="../Scripts/tabs.js"></script>

</head>
<body>
<form action="'.$PHP_SELF.'" method="post">
<div id="box">
<div id="heading">Edit Invoice</div><br/>
<h2>Customer - '.$row['account_name'].'</h2>
<h3>
	Invoice No. - '.$_GET['id'].'<br>
	GST - $'.$invoice_details['gst'].'<br>
	Invoice Total - $'.$invoice_details['total_amount'].'<br>
	Date Created  - '.$invoice_details['c_date'].'
</h3>';
if($message != '')
		{
			echo '<div class="success">'.$message.'</div>';
			unset($message);
		}

if (!empty($error_message))
	{
	  echo "<span class='error'>$error_message</span>";
	  unset($error_message);
	}
	echo'
<table width="100%">
	<tr>
		<td valign="top" width="25%">
			<table id="invoiceTable">
				<tr>
					<th colspan="2">Edit Invoice No - '.$_GET['id'].'</th>
				</tr>
				<tr>
					<td valign="top"><label>Invoice Status</label></td>
					<td>
						<select name="current_status_id" id="current_status_id">';
							$database = new database();
							$query = "SELECT * FROM invoice__status order by id ASC";
							$result = $database->query($query);
							while ($row = mysql_fetch_array($result))
								{
									echo "<option value='".$row['id']."'";
									if($row['id'] == ''.$invoice_details['status_id'].''){echo 'selected';}
									echo ">".$row['details']."</option>";
								}
							echo'
						</select>
					</td>
				</tr>
				<tr>
					<td valign="top"><label>Invoice Notes</label></td>
					<td>
						<textarea rows="3" name="notes">';
						if(isset($_POST['notes'])){ echo $_POST['notes'];} else{echo $invoice_details['notes'];}
						echo'</textarea>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="hidden" name="edit_invoice_id" value="'.$invoice_details['id'].'" />
						<input type="submit" name="edit_invoice" value="EDIT INVOICE"/>
					</td>
				</tr>
			</table>
		</td>
		<td valign="top" width="25%">
			<table id="invoiceTable">
				<tr>
					<th colspan="2">Add Payment Information</th>
				</tr>
				<tr>
					<td valign="top"><label>Amount Paid</label></td>
					<td>
						<input type="text" name="payment_amount" value="">
					</td>
				</tr>
				<tr>
					<td valign="top"><label>Paid On</label></td>
					<td>
						<input type="text" name="paid_date" id="paid_date">
					</td>
				</tr>
				<tr>
					<td valign="top"><label>Payment Method</label></td>
					<td>
						<select name="payment_method" id="payment_method">
							<option value="">Please Select</option>';
							$database = new database();
							$query = "SELECT * FROM invoice__paid_method order by id ASC";
							$result = $database->query($query);
							while ($row = mysql_fetch_array($result))
								{
									echo "<option value='".$row['id']."'>".$row['details']."</option>";
								}
							echo'
						</select>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="hidden" name="invoice_id" value="'.$invoice_details['id'].'" />
						<input type="submit" name="add_payment" value="ADD PAYMENT"/>
					</th>
				</tr>
			</table>
		</td>
		<td valign="top" width="25%">
			<table id="invoiceTable">
				<tr>
					<th colspan="4">Historical Payment for this invoice</th>
				</tr>';
				$query = "SELECT
							invoice__paid.invoice_id as invoice_id,
							invoice__paid.paid_method_id as paid_method_id,
							invoice__paid.paid_on as paid_on,
							invoice__paid.amount_paid as amount_paid,
							DATE_FORMAT(invoice__paid.c_date,'%d-%m-%Y') as c_date,
							invoice__paid_method.details as payment_method
							FROM						
							invoice__paid
							LEFT JOIN invoice__paid_method ON invoice__paid.paid_method_id = invoice__paid_method.id
							where invoice_id='".$invoice_details['id']."' 
							order by c_date DESC";
				$result = $database->query($query);
				$no_of_results = mysql_num_rows($result);
				if($no_of_results >0)
					{
						echo'
							<tr>
								<th>Paid on</th>
								<th>Method</th>
								<th>Amount</th>
								<th>Record Date</th>';
						while ($row = mysql_fetch_array($result))
							{
								echo '
									<tr>
										<td>'.$row['paid_on'].'</td>
										<td>'.$row['payment_method'].'</td>
										<td>'.$row['amount_paid'].'</td>
										<td>'.$row['c_date'].'</td>
									</tr>';
							}
					}
				else
					{
						echo '<tr><td colspan="4">No Historical Payments found...</td><tr>';
					}
			echo'
			</table>
		</td>
		<td valign="top" width="25%">
			<table id="invoiceTable">
				<tr>
					<th colspan="4">This invoice Sent Status</th>
				</tr>';
				$query = "SELECT 
							id,
							invoice_id, 
							sent_to,
							email,
							DATE_FORMAT(c_date,'%d-%m-%Y') as c_date
							FROM
							invoice__sent
							where invoice_id='".$invoice_details['id']."' 
							order by c_date DESC";
				$result = $database->query($query);
				$no_of_results = mysql_num_rows($result);
				if($no_of_results >0)
					{
						echo'
							<tr>
								<th>Sent On</th>
								<th>Sent To</th>
								<th>Sent to Email</th>';
						while ($row = mysql_fetch_array($result))
							{
								echo '
									<tr>
										<td>'.$row['c_date'].'</td>
										<td>'.$row['sent_to'].'</td>
										<td>'.$row['email'].'</td>
									</tr>';
							}
					}
				else
					{
						echo '<tr><td colspan="4">No Sent data found......</td><tr>';
					}
			echo'
			</table>
		</td>
	</tr>
</table>
<br/><br/><br/><br/><br/><br/><br/><br/><br/>
<table id="invoiceTable">
	<tr>
		<th colspan="2">DELETE THIS INVOICE</th>
	</tr>
	<tr>
		<td>Delete Invoice No.</td>
		<td><input type="hidden" name="invoice_id" value="'.$_GET['id'].'">'.$_GET['id'].'</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<input type="submit" name="delete_invoice" value="DELETE INVOICE"/>
		</td>
	</tr>
</table>
</div>
</form>
</body>
</html>';
?>