<?php
session_start();
if (isset($_POST['add_new']))
	{
		$database = new Database;
		if(checkforDuplicates('variable__'.$_POST['variable_name'].'', 'details', $_POST['new_value']) == '0')
			{
				$query = "INSERT INTO variable__".$_POST['variable_name']." (id, order_id, details) VALUES (NULL, '', '".$_POST['new_value']."')";
				$result = $database->query($query);
				$id = mysql_insert_id();
				$query = "SELECT * from variable__".$_POST['variable_name']."";
				$result = $database->query($query);
				$no_of_rows = mysql_num_rows($result);
				$query = "UPDATE variable__".$_POST['variable_name']." SET order_id = '".$no_of_rows."' WHERE id='".$id."'";
				$result = $database->query($query);
				$message = "New ".$_POST['display_name']." Added";
				unset($_POST);
				unset($_GET);
			}
		else
			{
				$error_message = "ERROR: Duplicate ".$_POST['display_name']." value found. NOT ADDED";
			}
	}

if (isset($_POST['delete_value']))
	{
		$database = new Database;
		$query = "DELETE FROM variable__".$_POST['variable_name']." where id =".$_POST['del_variable_id']."";
		$result = $database->query($query);
		$message = "".$_POST['display_name']." Deleted";
		unset($_POST);
		unset($_GET);
	}

if (isset($_POST['update_value']))
	{
		$database = new Database;
		$query = "UPDATE variable__".$_POST['variable_name']." SET details = '".$_POST['new_variable_name']."' WHERE id='".$_POST['update_variable_id']."'";
		$result = $database->query($query);
		$message = "".$_POST['display_name']." Edited";
		unset($_POST);
		unset($_GET);
	}

if (isset($_POST['change_order']))
	{
		$database = new Database;
		//STEP 1 Get Old and new positions of the variable
		$query = "SELECT * FROM variable__".$_POST['variable_name']." WHERE id='".$_POST['order_change_for_id']."'";
		$result = $database->query($query);
		$row = mysql_fetch_array($result);
		//$old_position = $row['order_id'];
		
		$table = 'variable__'.$_POST['variable_name'].'';
		$pos = 'order_id';
		$oldpos = $row['order_id'];
		$newpos = $_POST['new_order_id'];
		$query ="UPDATE $table
				SET    $pos = CASE WHEN $pos = $oldpos THEN
									   $newpos
								  WHEN $newpos < $oldpos AND $pos < $oldpos THEN
									   $pos + 1
								  WHEN $newpos > $oldpos AND $pos > $oldpos THEN
									   $pos - 1
							 END
				WHERE  $pos BETWEEN LEAST($newpos, $oldpos) AND GREATEST($newpos, $oldpos)";
		$result = $database->query($query);
		$message = "Order Changed for ".$_POST['display_name']."";
		unset($_POST);
		unset($_GET);
	}

if(	isset($_GET['function']) && $_GET['function']  == 'add')
	{
		echo'
		<form action="" method="POST">
			<table id="settings_table">
				<tr>
					<th>New '.$_GET['display_name'].'</th>
				</tr>';
				if($error_message != '')
					{
						echo '<tr><td colspan="3"><span class="error">'.$error_message.'</span></td></tr>';
						unset($error_message);
					}
				echo'
				<tr>
					<td>
						<input type="text" name="new_value" value=""/>
						<input type="hidden" name="variable_name" value="'.$_GET['id'].'"/>
						<input type="hidden" name="variable_name" value="'.$_GET['variable_name'].'"/>
						<input type="hidden" name="display_name" value="'.$_GET['display_name'].'"/>
					</td>
				</tr>
				<tr>
					<td><input type="submit" name="add_new" value="Add new '.$_GET['display_name'].'"></td>
				</tr>
			</table>
		</form>';
	}
elseif(	isset($_GET['function']) && $_GET['function']  == 'delete')
	{
		echo'
		<form action="" method="POST">
			<table id="settings_table">
				<tr>
					<th>Delete '.$_GET['display_name'].'</th>
				</tr>
				<tr>
					<td>
						<select name="del_variable_id">';
						$database = new database();
						$query = "SELECT * from variable__".$_GET['variable_name']." order by order_id ASC";
						$result = $database->query($query);
						$no_of_rows = mysql_num_rows($result);
						while ($row = mysql_fetch_array($result))
							{
								echo "<option value='".$row['id']."'>".$row['details']."</option>";
							}
						echo'
						</select>
						<input type="hidden" name="variable_name" value="'.$_GET['variable_name'].'"/>
						<input type="hidden" name="display_name" value="'.$_GET['display_name'].'"/>
					</td>
				</tr>
				<tr>
					<td><input type="submit" name="delete_value" value="Delete '.$_GET['display_name'].'"></td>
				</tr>
			</table>
		</form>';
	}
elseif(	isset($_GET['function']) && $_GET['function']  == 'update')
	{
		echo'
		<form action="" method="POST">
			<table id="settings_table">
				<tr>
					<th colspan="2">Edit '.$_GET['display_name'].'</th>
				</tr>
				<tr>
					<td>Select '.$_GET['display_name'].' to Edit<br/>
						<select name="update_variable_id">';
						$database = new database();
						$query = "SELECT * from variable__".$_GET['variable_name']." order by order_id ASC";
						$result = $database->query($query);
						$no_of_rows = mysql_num_rows($result);
						while ($row = mysql_fetch_array($result))
							{
								echo "<option value='".$row['id']."'>".$row['details']."</option>";
							}
						echo'
					</select>
					</td>
					<td>New '.$_GET['display_name'].' Name<br/>
						<input type="text" name="new_variable_name"/>
						<input type="hidden" name="variable_name" value="'.$_GET['variable_name'].'"/>
						<input type="hidden" name="display_name" value="'.$_GET['display_name'].'"/>
					</td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" name="update_value" value="Edit '.$_GET['display_name'].'"></td>
				</tr>
			</table>
		</form>';
	}
elseif(	isset($_GET['function']) && $_GET['function']  == 'change_ord')
	{
		echo'
		<form action="" method="POST">
			<table id="settings_table">
				<tr>
					<th colspan="2">Change Order</th>
				</tr>
				<tr>
					<td>
						Current Order<br/>
						<select name="order_change_for_id">';
						$database = new database();
						$query = "SELECT * from variable__".$_GET['variable_name']." order by order_id ASC";
						$result = $database->query($query);
						$no_of_rows = mysql_num_rows($result);
						while ($row = mysql_fetch_array($result))
							{
								echo "<option value='".$row['id']."'>".$row['details']."</option>";
							}
						echo'
					</select>
					</td>
					<td>New Order<br/>
						<select name="new_order_id" id="new_order_id">';
						for($x=1; $x<=$no_of_rows; $x++)
							{
								echo "<option value='".$x."'>".$x."</option>";
							}
						echo'
						</select>
						<input type="hidden" name="variable_name" value="'.$_GET['variable_name'].'"/>
						<input type="hidden" name="display_name" value="'.$_GET['display_name'].'"/>
					</td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" name="change_order" value="Change order for '.$_GET['display_name'].'"></td>
				</tr>
			</table>
		</form>';
	}
else
	{
		$database = new database();
		$query = "SELECT * from variable__list order by id ASC";
		$result = $database->query($query);
		$no_of_rows = mysql_num_rows($result);
		echo "<table id='settings_table'>
				<tr>
					<th colspan='4'>Edit Variable Settings</th>
				</tr>
				<tr>
					<th>Variable Name</th>
					<th>Description</th>
					<th>Operations</th>
					<th>Current Values</th>
				</tr>";
		if($message != '')
			{
				echo '<tr><td colspan="3" style="color:green; font-weight:bold; text-decoration:italic;">'.$message.'</span></td></tr>';
				unset($message);
			}
		while ($row = mysql_fetch_array($result))
			{
				echo"
				<tr>
					<td><b>".$row['display_name']."</b></td>
					<td>".$row['description']."</td>
					<td>";
					if($row['allow_add'] == '1')
						{
							echo"<a href='".ROOT_ADDRESS."Admin/index.php?content_id=system_settings&function=add&variable_id=".$row['id']."&variable_name=".$row['variable_name']."&display_name=".$row['display_name']."'>Add ".$row['display_name']."</a><br/>";
						}
					if($row['allow_update'] == '1')
						{
							echo"<a href='".ROOT_ADDRESS."Admin/index.php?content_id=system_settings&function=update&variable_id=".$row['id']."&variable_name=".$row['variable_name']."&display_name=".$row['display_name']."'>Edit ".$row['display_name']."</a><br/>";
						}
					if($row['allow_delete'] == '1')
						{
							echo"<a href='".ROOT_ADDRESS."Admin/index.php?content_id=system_settings&function=delete&variable_id=".$row['id']."&variable_name=".$row['variable_name']."&display_name=".$row['display_name']."'>Delete ".$row['display_name']."</a><br/>";
						}
					if($row['allow_order_change'] == '1')
						{
							echo"<a href='".ROOT_ADDRESS."Admin/index.php?content_id=system_settings&function=change_ord&variable_id=".$row['id']."&variable_name=".$row['variable_name']."&display_name=".$row['display_name']."'>Change Order for ".$row['display_name']."</a><br/>";
						}
					echo"
					</td>
					<td>
						<select style='width:150px;'>";
							$query1 = "SELECT * from variable__".$row['variable_name']." order by order_id ASC";
							
							$result1 = $database->query($query1);
							while ($row1 = mysql_fetch_array($result1))
								{
									echo "<option value=''>".$row1['details']."</option>";
								}
							echo"
						</select>
					</td>					
				</tr>
				";
			}
		echo"</table>";
	}
?>
