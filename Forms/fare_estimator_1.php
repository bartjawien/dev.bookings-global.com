<?php
session_start();
require_once('../init.php');
include(''.INCLUDE_PATH.'config.php');
include(''.INCLUDE_PATH.'settings.php');
include(''.INCLUDE_PATH.'auth.php');
include(''.INCLUDE_PATH.'functions.php');
include(''.CLASSES_PATH.'database.php');
include(''.CLASSES_PATH.'mailer.php');


echo'
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<link href="../Css/minified/jquery-ui-1.10.3.custom.min.css" rel="stylesheet"  />
	<link href="../Css/jtable/themes/lightcolor/blue/jtable.css" rel="stylesheet"  type="text/css" />
	<link href="../Css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
	<link href="../Css/style.css" rel="stylesheet"  />
	<link href="../Css/menu.css" rel="stylesheet"  />
	<link href="../Css/chosen.min.css" rel="stylesheet"  />
	<link href="../Css/autocomplete.css" rel="stylesheet"  />
	<link href="../Css/jquery.timeentry.css" rel="stylesheet"  />

	
	<script src="../Scripts/jquery-1.10.2.min.js"></script>
	<script src="../Scripts/jquery-ui.min.js"></script>
	<script src="../Scripts/jquery.timeentry.min.js"></script>
	<script src="../Scripts/jquery.jtable.min.js"></script>
	<script src="../Scripts/jquery.validate.js"></script>
	<script src="../Scripts/jquery.validationEngine.js"></script>
	<script src="../Scripts/jquery.validationEngine-en.js"></script>
	<script src="../Scripts/chosen.jquery.min.js"></script>
	<script src="../Scripts/fare_estimator_1.js"></script>

	<script src="../Scripts/jquery.geocomplete.js"></script>
	<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>

</head>
<body  id="map_page">
<div id="dialog_submit" style="display:none;"></div> 			
<form name="form_estimate_fare" id="form_estimate_fare" action="" method="POST">
<h1>Fare Estimator - Clients</h1>
<table id="table_estimate_fare" style="border:1px solid black; width:30%;">
	<tr>
		<td colspan="2" style="background-image: url(../Images/bg1.jpg);" valign="top">
			<input type="button" name="estimate_fare" id="estimate_fare" value="1111" class="estimate_fare"  onclick="GetRoute()"/>
		</td>
	</tr>
	<tr>
		<td valign="top"> 
			<table style=" padding:20px;">
				<tr>
					<td>
						Pickup Location
					</td>
					<td>
						<input type="text" id="pickup_address" name="pickup_address" class="estimate_fare_textbox">
					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr> 
					<td>
						Destination
					</td>
					<td>
						<input type="text" id="destination_address" name="destination_address"  class="estimate_fare_textbox">
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<span class="loading"><img src="../Images/loading.gif"></img></span>
						<div id="calculated_fare"></div>
					</td>
				</tr>
			</table>
		</td>
		<td>
			<table>
				<tr>
					<td>
						<div id="dvMap" style="width: 1000px; height: 500px">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<form name="form_estimate_fare_2" id="form_estimate_fare_2" action="" method="POST">
<table id="table_estimate_fare" style="border:1px solid black; width:30%;">
	<tr>
		<td colspan="2" style="background-image: url(../Images/bg1.jpg);" valign="top">
			<input type="button" name="estimate_fare_2" id="estimate_fare_2" class="estimate_fare""/>
		</td>
	</tr>
	<tr>
		<td>
			<table  id="fareEstimatorTable" width="100%">
				<tr>
					<th colspan="4">FIXED FARE ESTIMATOR</th>
				</tr>
				<tr>
					<td>Point A</td>
					<td>
						<select name="pickup_from_area" id="pickup_from_area">
							<option value="">--------SELECT AREA----------</option>
							<option value="melb_airport">Melbourne Airport Domestic</option>
							<option value="cbd">Melbourne CBD</option>
							<option value="avalon_airport">Avalon Airport</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Point B</td>
					<td>
						<select name="destination_suburb" id="destination_suburb">
						<option value="">-------SELECT SUBURB------</option>';
						$database = new Database;
						$query2 = "SELECT * from cars__prices order by suburb ASC";
						$result2 = $database->query($query2);

						while($row2 = mysql_fetch_array($result2))
							{
								echo'<option value="'.$row2['id'].'">'.$row2['suburb'].'</option>';
									
							}
					echo'
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<span class="loading"><img src="../Images/loading.gif"></img></span>
						<div id="calculated_fare_2"></div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>';
?>
<script>
var source, destination;
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
google.maps.event.addDomListener(window, 'load', function () {
    //new google.maps.places.SearchBox(document.getElementById('pickup_address'));
    //new google.maps.places.SearchBox(document.getElementById('destination_address'));
    directionsDisplay = new google.maps.DirectionsRenderer({ 'draggable': true });
});
 
function GetRoute() {
    var melbourne = new google.maps.LatLng(-37.8131869, 144.9629796);
    var mapOptions = {
        zoom: 7,
        center: melbourne
    };
    map = new google.maps.Map(document.getElementById('dvMap'), mapOptions);
    directionsDisplay.setMap(map);
    directionsDisplay.setPanel(document.getElementById('dvPanel'));
 
    //*********DIRECTIONS AND ROUTE**********************//
    source = document.getElementById("pickup_address").value;
    destination = document.getElementById("destination_address").value;
 
    var request = {
        origin: source,
        destination: destination,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
    });
 
    //*********DISTANCE AND DURATION**********************//
    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix({
        origins: [source],
        destinations: [destination],
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false
    }, function (response, status) {
        if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS") {
            var distance = response.rows[0].elements[0].distance.text;
            var duration = response.rows[0].elements[0].duration.text;
            var dvDistance = document.getElementById("dvDistance");
           dvDistance.innerHTML = "";
            dvDistance.innerHTML += "Distance: " + distance + "<br />";
            dvDistance.innerHTML += "Duration:" + duration;
 
        } else {
            alert("Unable to find the distance via road.");
        }
    });
}
</script>
</body>
</html>