<!DOCTYPE html>
<html>
<head>
<title> Allied Chauffeured Cars</title>
<link href="../Css/minified/jquery-ui-1.10.3.custom.min.css" rel="stylesheet"  />
<link href="css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../Scripts/jquery-1.10.2.min.js"></script>
<!--<script type="text/javascript" src="Scripts/jquery_header_slide.js"></script>-->
<script src="../Scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="../Scripts/student_booking.js"></script>
</head>
<body>
<table  style="background-color:#D1D1D1; font-family:Verdana, Geneva, sans-serif;font-size:.8em; padding:5px 0px 0px 0px;" width="100%" >
	<tr align="center" >
		<td align="center" >
		<div id="dialog_submit" style="display:none;"></div>
			<form name="form_student_booking" id="form_student_booking" method="post">
				<table id="forms_table" align="center" width="80%">
					<tr>
						<th colspan="2">RMIT - Students Booking Request</th>
					</tr>
					<tr>
						<td colspan="2" height="10px"></td>
					</tr>
					<tr>
						<th class="sub">Date Time</th>
						<td></td>
					</tr>
					<tr>
						<td>Journey Date</td>
						<td>
							<input type="text" name="j1_date" id="j1_date" size="15">
							<input type="hidden" name="j1_date_cap" id="j1_date_cap" size="15">
						</td>
					</tr>
					<tr>
						<td>Journey Time</td>
						<td>
							<select name="j1_time_hrs" id="j1_time_hrs">
								<option value="">HOURS</option><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option>
							</select>
							<select name="j1_time_mins" id="j1_time_mins">
								<option value="">HOURS</option><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option>
							</select>
						</td>
					</tr>	
								<tr>
									<th class="sub">Student Information</th>
									<td></td>
								</tr>
							<tr>
								<td>Student ID Number</td>
								<td>
									<input type="text" name="std_id" id="std_id" placeholder="Student Id">
								</td>
							</tr>
							<tr>
								<td>Student Name</td>
								<td>
									<select name="std_title" id="std_title">
										<option value="">TITLE</option>
										<option value="Mr">Mr</option>
										<option value="Ms">Ms</option>
										<option value="Mrs">Mrs</option>
									</select>
									<input type="text" name="std_fname" id="std_fname" placeholder="First Name">
									<input type="text" name="std_lname" id="std_lname" placeholder="Last Name">
								</td>
							</tr>
							<tr>
								<td>Phone Number</td>
								<td><input type="text" name="std_ph" id="std_ph" placeholder="Phone Number"></td>
							</tr>
							<tr>
								<td>Email</td>
								<td><input type="text" name="std_email" id="std_email" placeholder="Email"></td>
							</tr>
							<tr>
								<td>Are you under 18</td>
								<td>
									No<input type="radio" name="under_18" id="under_18" value="0" checked >
									Yes<input type="radio" name="under_18" id="under_18" value="1" >
								</td>
							</tr>
					<tr>
						<th class="sub">Arrival Information</th>
						<td></td>
					</tr>				
					<tr>
						<td>Journey From</td>
						<td>
							<select name="j1_from" id="j1_from">
								<option value="">SELECT</option>
								<option value="1">Airports</option>
								<option value="2">Suburbs</option>
							</select>
						</td>
					</tr>
					<tr id="j1_airport">
						<td>Select Airport</td>
						<td>
							<select name="j1_airport" id="j1_airport">
								<option value="">SELECT</option>
								<option value="1">Melbourne International Airport</option>
								<option value="2">Melbourne Domestic Airport</option>
								<option value="3">Essendon Airport</option>
							</select><br/>
							Terminal : <input type="text" name="j1_terminal" id="j1_terminal" placeholder="Terminal"><br/>
							Flight No : <input type="text" name="j1_flight_no" id="j1_flight_no" placeholder="Flight Number"><br/>
						</td>
					</tr>
					<tr id="j1_suburb">
						<td>Address</td>
						<td>
							<input type="text" name="j1_line_1" id="j1_line_1" placeholder="Address Line 1"><br/>
							<input type="text" name="j1_line_2" id="j1_line_2" placeholder="Address Line 2"><br/>
							<input type="text" name="j1_sub" id="j1_sub" placeholder="Suburb">
							<input type="text" name="j1_pc" id="j1_pc" placeholder="Postcode"><br/>
							<select name="j1_state" id="j1_state">
								<option value="">SELECT</option>
								<option value="VIC">VIC</option>
								<option value="NSW">NSW</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Journey To</td>
						<td>
							<select name="j1_to" id="j1_to">
								<option value="">SELECT</option>
								<option value="1">Airports</option>
								<option value="2">Suburbs</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Select Airport</td>
						<td>
							<select name="j1_to_airport" id="j1_to_airport">
								<option value="">SELECT</option>
								<option value="1">Melbourne International Airport</option>
								<option value="2">Melbourne Domestic Airport</option>
								<option value="3">Essendon Airport</option>
							</select><br/>
							Terminal : <input type="text" name="j1_to_terminal" id="j1_terminal" placeholder="Terminal"><br/>
							Flight No : <input type="text" name="j1_to_flight_no" id="j1_flight_no" placeholder="Flight Number"><br/>
						</td>
					</tr>
					<tr>
						<td>Address</td>
						<td>
							<input type="text" name="j1_to_line_1" id="j1_to_line_1" placeholder="Address Line 1"><br/>
							<input type="text" name="j1_to_line_2" id="j1_to_line_2" placeholder="Address Line 2"><br/>
							<input type="text" name="j1_to_sub" id="j1_to_sub" placeholder="Suburb">
							<input type="text" name="j1_to_pc" id="j1_to_pc" placeholder="Postcode"><br/>
							<select name="j1_to_state" id="j1_to_state">
								<option value="">SELECT</option>
								<option value="VIC">VIC</option>
								<option value="NSW">NSW</option>
							</select>
						</td>
					</tr>
					<tr>
						<th class="sub">Additional Information</th>
						<td></td>
					</tr>
					<tr>
						<td>Vehicle Required</td>
						<td>
							<select name="car_type" id="car_type">
								<option value="">SELECT</option>
								<option value="1">Sedan (Seats 4)</option>
								<option value="2">European Sedan (Seats 4)</option>
								<option value="3">VAN (Seats 7)</option>
								<option value="4">Bus (Seats 12)</option>
								<option value="5">Bus (Seats 24)</option>
								<option value="6">Bus (Seats 32)</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Number of Passengers</td>
						<td>
							<select name="pax_nos" id="pax_nos">
								<option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Baby Capsules</td>
						<td>
							<select name="capsule" id="capsule">
								<option value="">0</option><option value="1">1</option><option value="2">2</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Baby Seats</td>
						<td>
							<select name="baby_seat" id="baby_seat">
								<option value="">0</option><option value="1">1</option><option value="2">2</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Booster Seats</td>
						<td>
							<select name="booster" id="booster">
								<option value="">0</option><option value="1">1</option><option value="2">2</option>
							</select>
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<input type="button" name="make_booking" id="make_booking" value="MAKE BOOKING">
							<input type="hidden" name="job_src" id="job_src" value="Others">
							<input type="hidden" name="acc_type" id="acc_type" value="3">
							<input type="hidden" name="charge_acc" id="charge_acc" value="15">
							<input type="hidden" name="acc_contact" id="acc_contact" value="295">
							<input type="hidden" name="acc_passenger" id="acc_passenger" value="90">
							<input type="hidden" name="job_type" id="job_type" value="1">
							<input type="hidden" name="entered_by" id="entered_by" value="90">
							<input type="hidden" name="charge_mode" id="charge_mode" value="1">
						</td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
</table>

</body>
</html>