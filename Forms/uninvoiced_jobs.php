<?php
ini_set("display_errors", 0);
session_start();
require_once('../init.php');
//include(''.INCLUDE_PATH.'auth.php');
include(''.INCLUDE_PATH.'config.php');
include(''.INCLUDE_PATH.'settings.php');
include(''.INCLUDE_PATH.'auth.php');
include(''.INCLUDE_PATH.'functions.php');
include(''.INCLUDE_PATH.'functions_date_time.php');
include(''.CLASSES_PATH.'database.php');
include(''.CLASSES_PATH.'chargeAccount.php');
include(''.CLASSES_PATH.'job.php');
include(''.CLASSES_PATH.'mailer.php');
include(''.CLASSES_PATH.'validation.php');

$database 	= 	new database;
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
<link href="../Css/minified/jquery-ui-1.10.3.custom.min.css" rel="stylesheet"  />
	<link href="../Css/jtable/themes/lightcolor/blue/jtable.css" rel="stylesheet"  type="text/css" />
	<link href="../Css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
	<link href="../Css/style.css" rel="stylesheet"  />
	<link href="../Css/menu.css" rel="stylesheet"  />
	<link href="../Css/chosen.min.css" rel="stylesheet"  />
	<link href="../Css/autocomplete.css" rel="stylesheet"  />
	<link href="../Css/jquery.timeentry.css" rel="stylesheet"  />

	
	<script src="../Scripts/jquery-1.10.2.min.js"></script>
	<script src="../Scripts/jquery-ui.min.js"></script>
	<script src="../Scripts/jquery.timeentry.min.js"></script>
	<script src="../Scripts/jquery.jtable.min.js"></script>
	<script src="../Scripts/jquery.validate.js"></script>
	<script src="../Scripts/jquery.validationEngine.js"></script>
	<script src="../Scripts/jquery.validationEngine-en.js"></script>
	<script src="../Scripts/chosen.jquery.min.js"></script>
	<script src="../Scripts/invoicing.js"></script>

</head>
<body>
	<form action="" method="post" name="uninvoiced_form" id="uninvoiced_form">
	<div id="box">
		<div id="heading">Uninvoiced Jobs</div><br/>
	<?php	if($message != '')
			{
				echo '<div class="success">'.$message.'</div>';
				unset($message);
			}
		
		if (!empty($error_message))
			{
			  echo "<span class='error'>$error_message</span>";
			  unset($error_message);
			} ?>
		
		<h2>STEP 1 - SELECT CUSTOMER</h2>
			<table>
				<tr>
					<td valign="top">
						<label>Charge account Type</label>
							<select name="acc_type_un" id="acc_type_un">
								<option value="">Please Select</option>
						<?php	$database = new database();
								$query = "SELECT * from variable__charge_acc_type order by order_id ASC";
											$result = $database->query($query);
											while ($row = mysql_fetch_array($result))
												{
													echo "<option value='".$row['id']."'>".$row['details']."</option>";
												}
						?>
							</select>
					<div style="padding:10px;" id="date_range">
						<label>Select Date Range</label>
						<input type="text" name="date_from" id="date_from" size="20" placeholder="Date From" value="<?php echo $_POST['date_from']; ?>" />
						<input type="hidden" name="cap_date_from" id="cap_date_from" value="<?php echo $_POST['cap_date_from']; ?>" />
						<input type="text" name="date_to" id="date_to" size="20" placeholder="Date To" value="<?php echo $_POST['date_to']; ?>" />
						<input type="hidden" name="cap_date_to" id="cap_date_to" value="<?php echo $_POST['cap_date_to']; ?>" />
					</div>
							<div id="row_customer_un"></div>
							<div class="loading" style="display: none"><br><label>Please Wait..</label><img src="../Images/loading.gif" /></div>
							<br>
					</td>
				</tr>
			</table>
		
		
		<div style=" padding-top:50px;"><input type="submit" class="big_btn" id="search" name="search" value="Search" disabled></div>
			
	</div>
	</form>

<?php
if(isset($_POST['search'])) 
{
	$query1 = "SELECT
				*,
				job.id as id,
				job.cur_pay_status_id as cur_pay_status_id,
				CONCAT(a.fname) as entry_by,
				CONCAT(b.fname,'<br/>',b.phone) as bkg_by,
				CONCAT(c.title, ' ', c.fname, ' ', c.lname) as pax_name,
				CONCAT(d.fname,'<br/>',d.mobile) as driver,
				CONCAT(job__reference.std_title, ' ', job__reference.std_fname, ' ', job__reference.std_lname) as student_name,
				CONCAT(job.frm_line1,' ',job.frm_line2,'<br/><b>',job.frm_sub,'</b>') as from_address,
				CONCAT(job.frm_via_line1,' ',job.frm_via_line2,'<br/><b>',job.frm_via_sub,'</b>') as from_via_address,
				CONCAT(job.to_flight_no,' ',job.to_line1,' ',job.to_line2,'<br/><b>',job.to_sub,'</b>') as to_address,
				CONCAT(job.to_via_line1,' ',job.to_via_line2,'<br/><b>',job.to_via_sub,'</b>') as to_via_address,
				CONCAT('<b>',TIME_FORMAT(job.job_time,'%H:%i'),'</b><br/>',DATE_FORMAT(job.job_date,'%d/%m/%Y')) as date_time,
				variable__car_type.details as vehicle_type,
				job__status_ids.details as current_booking_status,
				job__driver_status_ids.details as driver_status,
				job__reference.std_title as std_title,
				job__reference.std_fname as std_fname,
				job__reference.std_lname as std_lname,
				job__reference.under_18 as under_18,
				job__reference.acc_type as acc_type,
				job.driver_notes as driver_notes, 
				job__driver.accepted_amount as driver_price,
				job__driver_extra_pay.amount as extra_pay,
				job__payment_status.details as job_current_payment_status_id,
				job.fare as base_fare,
				job.tot_fare as total_fare,
				job.pax_nos as pax_nos,
				job.luggage as luggage,
				job.baby_seat as baby_seat,
				job.booster_seat as booster_seat,
				job.baby_capsule as baby_capsule,
				job.job_time as job_time,
				job.job_date as job_date,
				job.wait as waiting_time,
				job.inter as int_arrival,
				job.park as parking,
				job.ah as after_hour,
				job.tolls as tolls,
				job.oth as other_ex,
				job.ext_notes as ext_notes
				from 
				job
				LEFT JOIN job__reference ON job.job_reference_id = job__reference.id
				LEFT JOIN job__status_ids ON job.job_status = job__status_ids.id
				LEFT JOIN variable__car_type ON job.car_id = variable__car_type.id
				LEFT JOIN job__driver_status_ids ON job.driver_status = job__driver_status_ids.id
				LEFT JOIN user AS a ON job__reference.entered_by = a.id
				LEFT JOIN user AS b ON job__reference.bkg_by_id = b.id
				LEFT JOIN user AS c ON job__reference.pax_id = c.id
				LEFT JOIN user AS d ON job.driver_id = d.id
				LEFT JOIN charge_acc ON job__reference.charge_acc_id = charge_acc.id
				LEFT JOIN job__driver ON job.id = job__driver.job_id AND job__driver.id=(SELECT MAX(id) FROM job__driver WHERE job_id = job.id)  
				LEFT JOIN job__driver_extra_pay ON job.id = job__driver_extra_pay.job_id
				LEFT JOIN job__payment_status ON job.cur_pay_status_id = job__payment_status.id
				WHERE job.cur_pay_status_id = '6' AND job__reference.charge_acc_id=".$_POST['charge_acc_un'];
			
			if($_POST['date_from']!='' && $_POST['date_to']!='')
			{
				$query1 .= " AND job.job_date BETWEEN '".$_POST['date_from']."' AND '".$_POST['date_to']."'";
			}
			
				$query1 .= " order by job.job_date desc ,job.job_time desc";
	
		$result = $database->query($query1);
	$no_of_bookings = mysql_num_rows($result);
	echo '
	<table width="100%" id="listBookings">
		<thead>
			<tr>
				<th colspan="15" style="font-size: 18px; font-weight:bold; text-align:left; padding:10px 10px 10px 10px;">ALL UNINVOICED JOBS</th>
			</tr>';
			if($no_of_bookings > 0)
				{
					echo'
					<tr>
						<th width="10%">Booked By</th>
						<th width="4%" style="text-align:left;">ID</th>
						<th width="7%">Status</th>
						<th width="6%">Time</th>
						<th width="6%">Extras</th>
						<th width="6%">Pax Details</th>
						<th width="10%">Pickup</th>
						<th width="10%">Desitnation</th>
						<th width="11%">Payment</th>
						<th width="8%">Fare</th>
						<th width="6%">DP</th>
						<th width="6%">Driver</th>
						<th width="10%">Notes</th>
					</tr>';
					while($row = mysql_fetch_array($result))
						{
							//$vehicle_class = ''.$row['vehicle_class_details'].'<br>';
							
							if($row['pax_nos'] > 1)
								{$no_of_adult = 'Pax Nos - '.$row['pax_nos'].'<br>';}
							else{$no_of_adult = '';}
							
							if($row['booster_seat'] > 0)
								{$booster_seats = 'Booster Seats - '.$row['booster_seat'].'<br>';}
							else{$booster_seats = '';}
							
							if($row['baby_seat'] > 0)
								{$baby_seats = 'Baby Seats - '.$row['baby_seat'].'<br>';}
							else{$baby_seats = '';}
							
												
							$extras = ''.$no_of_adult.''.$booster_seats.''.$baby_seats;
					
							$today  	= 	date('d-m-Y', mktime(0,0,0,date("m"),date("d"),date("Y")));
							$tomorrow  	= 	date('d-m-Y', mktime(0,0,0,date("m"),date("d")+1,date("Y")));
							
							echo'<tr>
								<td valign="top"><div id="big_txt_bold">'.$row['bkg_by'].'</div></td>
								<td valign="top">
									<div id="big_txt_bold">'.$row['id'].'</div>
								</td>
								<td valign="top">';
									if ($row['job_status_ids'] == '1' || $row['job_status_ids'] == '7')//1- received
										{
											$fontcolor = 'red';
										}
									else if($row['job_status_ids'] == '2' || $row['job_status_ids'] == '3')
										{
											$fontcolor = 'blue';
										}
									else if($row['job_status_ids'] == '4' || $row['job_status_ids'] == '8')
										{
											$fontcolor = 'green';
										}
									echo'
									<div id="big_txt_bold"><font color="'.$fontcolor.'">'.$row['current_booking_status'].'</font></div>';
									
									echo'
								</td>
								<td valign="top"><span id="big_txt_bold"  valign="bottom">'.$row['job_time'].'</span><br><span id="smaller_txt">'.$row['job_date'].'</span></td>
								<td valign="top">
									<div id="small_bold_txt">'.$row['vehicle_type'].'</div>';
									if($extras !='')
										{
											echo'<span id="small_highlighted_txt" valign="bottom">'.$extras.'</span>';
										}
								echo'
								</td>
								<td valign="top"><span id="small_bold_txt"  valign="bottom"><font color="red">'.$row['pax_name'].'</font><br>'.$row['std_title'].' '.$row['std_fname'].' '.$row['std_lname'].'</span></td>
								<td valign="top"><span id="small_bold_txt">';
									
									echo ''.$row['from_address'].'</span>
								</td>
								<td valign="top"><span id="small_bold_txt">'.$row['to_address'].'</span></td>
								<td valign="top">
									<span id="smallest_txt">'.$row['job_current_payment_status_id'].'</span>
								</td>
								<td valign="top" style="text-align:right;">
									<span id="big_txt_bold">$'.$row['total_fare'].'<span><br>';
									if($row['base_fare'] !== '0.00')
										{
											echo '<span id="smallest_txt">Base Fare - '.$row['base_fare'].'<span><br>';
										}
									if($row['waiting'] !== '0.00')
										{
											echo '<span id="smallest_txt">Waiting - '.$row['waiting_time'].'<span><br>';
										}
									if($row['after_hour'] !== '0.00')
										{
											echo '<span id="smallest_txt">After Hour - '.$row['after_hour'].'<span><br>';
										}
									if($row['parking'] !== '0.00')
										{
											echo '<span id="smallest_txt">Parking - '.$row['parking'].'<span><br>';
										}
									if($row['tolls'] !== '0.00')
										{
											echo '<span id="smallest_txt">Tolls - '.$row['tolls'].'<span><br>';
										}
									if($row['other_ex'] !== '0.00')
										{
											echo '<span id="smallest_txt">Oth Ex - '.$row['other_ex'].'<span><br>';
										}
									
								echo'
								</td>
								<td valign="top" style="text-align:right;"><span id="big_txt_bold">$'.$row['driver_price'].'</span></td>
								<td valign="top"><span id="small_bold_txt">'.$row['driver'].' '.'</span></td>
								<td valign="top"><span id="small_txt">'.$row['ext_notes'].'</span></td>
							</tr>';	
						}
				}
			else
				{
					echo "<tr><td colspan='15' >NO RECORDS FOUND</td></tr>";
				}
	echo'
	</tbody></table>';

}
