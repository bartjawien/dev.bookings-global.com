<?php
ini_set("memory_limit","1024M");

session_start();
require_once('../init.php');
//include(''.INCLUDE_PATH.'auth.php');
include(''.INCLUDE_PATH.'config.php');
include(''.INCLUDE_PATH.'settings.php');
include(''.INCLUDE_PATH.'auth.php');
include(''.INCLUDE_PATH.'functions.php');
include(''.INCLUDE_PATH.'functions_date_time.php');
include(''.CLASSES_PATH.'database.php');
include(''.CLASSES_PATH.'chargeAccount.php');
include(''.CLASSES_PATH.'job.php');
include(''.CLASSES_PATH.'mailer.php');
include(''.CLASSES_PATH.'validation.php');
include(''.CLASSES_PATH.'invoice.php');
require_once('../html2pdf/html2pdf.class.php');


$database 	= 	new database;
//First see if the customer is setup with a surcharge
$query1 = "select * from charge_acc where id = '".$_POST['charge_acc']."'";
$result1 = $database->query($query1);
$row1 = mysql_fetch_array($result1);
$surcharge_percentage = $row1['surcharge'];
$add_gst = $row1['include_gst'];
$surcharge_percentage = 0.00;
$add_gst = 0;
$discount = 0;
//Get the business details of this business

$invoice = new Invoice();

$query2 = "SELECT * from settings__business";
$result2 = $database->query($query2);
$row2 = mysql_fetch_array($result2);	


if(isset($_POST['job_id']))
{
	foreach($_POST['job_id'] as $job_id)
	{
				$job = new Job();
				$job_details = $job->getJobDetails($job_id);
				
				
						//$base_fare 			= 	(($job_details['base_fare'] * 10) / 11);
						$base_fare 			= 	$job_details['fare'];
						$extras				= 	$job_details['tot_fare'] - $job_details['fare'];
						if($surcharge_percentage != '0.00')
							{
								//$surcharge 	= 	((($base_fare + $extras) * $surcharge_percentage) * 10) / 11;
								$surcharge 	= 	($base_fare + $extras) * $surcharge_percentage;
							}	
						else
							{
								$surcharge 	= 0;
							}
											
						$total_base_fare	=	(($base_fare + $extras + $surcharge)*10)/11; // this is the total fare ex gst for this job
						$gst 				= 	($base_fare + $extras + $surcharge)/11; // this is total gst for this job
						$total_fare			=   $total_base_fare + $gst;
						
						$GTT_base_fare 		= 	$GTT_base_fare + $base_fare;
						$GTT_extras 		= 	$GTT_extras + $extras;
						$GTT_surcharge 		= 	$GTT_surcharge + $surcharge;
						//$GTT_ex_gst		=	$GTT_ex_gst + $total_base_fare;
						$GTT_ex_gst			=	$GTT_ex_gst + $total_base_fare ;
						//$GTT_gst			= 	$GTT_gst + $gst;
						$GTT_gst			= 	$GTT_gst + $gst;
						//$GTT_fare			=   $GTT_fare + $total_fare;
						$GTT_fare			=   $GTT_fare + $total_fare;
					
	}
		/*if($row1['discount'] !='0')
			{
				$GTT_fare = $GTT_fare - ($GTT_fare * $row1['discount']);
			}*/
}	

if (isset($_POST['save_only']))
{
	
		
			
			// INSERT THESE VALUES IN INVOICE TABLE	
			
			if($row1['payment_status_id'] == '1' || $row1['payment_status_id'] == '2') //customer is a collect or charge customer
				{
					if($row1['discount'] != '0.00')
						{
							$query7 = "INSERT INTO invoice 
										(charge_acc_id, status_id, sent_status_id, base_fare, extras, surcharge, total_ex_gst, gst, total_amount, notes) VALUES
										('".$_POST['charge_acc_id']."','2', '1','".$_POST['GT_base_fare']."', '".$_POST['GT_extras']."', '".$_POST['GT_surcharge']."', '".$_POST['GT_ex_gst']."', '".$_POST['GST_after_discount']."', '".$_POST['GT_fare_after_discount']."', '".$_POST['discount_in_percentage_string discount']." applied on invoice')";
						}
					else
						{
							$query7 = "INSERT INTO invoice 
										(charge_acc_id, status_id, sent_status_id, base_fare, extras, surcharge, total_ex_gst, gst, total_amount) VALUES
										('".$_POST['charge_acc_id']."','2', '1','".$_POST['GT_base_fare']."', '".$_POST['GT_extras']."', '".$_POST['GT_surcharge']."', '".$_POST['GT_ex_gst']."', '".$_POST['GT_gst']."', '".$_POST['GT_fare']."')";
						}
				}
			else
				{
					if($row1['discount'] != '0.00')
						{
							$query7 = "INSERT INTO invoice 
										(charge_acc_id, status_id, sent_status_id, base_fare, extras, surcharge, total_ex_gst, gst, total_amount, notes) VALUES
										('".$_POST['charge_acc_id']."','1', '1','".$_POST['GT_base_fare']."', '".$_POST['GT_extras']."', '".$_POST['GT_surcharge']."', '".$_POST['GT_ex_gst']."', '".$_POST['GST_after_discount']."', '".$_POST['GT_fare_after_discount']."', '".$_POST['discount_in_percentage_string']." discount applied on invoice')";
						}
					else
						{
							$query7 = "INSERT INTO invoice 
										(charge_acc_id, status_id, sent_status_id, base_fare, extras, surcharge, total_ex_gst, gst, total_amount) VALUES
										('".$_POST['charge_acc_id']."','1', '1','".$_POST['GT_base_fare']."', '".$_POST['GT_extras']."', '".$_POST['GT_surcharge']."', '".$_POST['GT_ex_gst']."', '".$_POST['GT_gst']."', '".$_POST['GT_fare']."')";
						}
				}
			
			$result7 = $database->query($query7);
			$this_invoice_id = mysql_insert_id();
			
			$jobs_arr = explode(',',$_POST['jobs']);
			foreach($jobs_arr as $job_id)
				{
					$job = new Job();
					$job->updateJob($job_id, 'cur_pay_status_id', '7'); 
					//$job->updateJob($job_id, 'invoice_id', $this_invoice_id);
					$job->addJobPaymentStatusTable( $job_id, '7', $_SESSION['EMAIL']);
					
					$query = "INSERT INTO invoice__items
								(invoice_id, job_id) VALUES
								('$this_invoice_id','$job_id')";
					$result = $database->query($query);
				}
				
			$html= gzuncompress($_SESSION['html_data']); 
			$html= str_replace('###NEXT_INVOICE_ID###', $this_invoice_id, $html);
			
			$myFile = "../invoices/invoice_".$this_invoice_id.".html";
			$fh = fopen($myFile, 'w') or die("can't open file");
			fwrite($fh, $html);
			fclose($fh);

			ob_start();
			include('../invoices/invoice_'.$this_invoice_id.'.html');
			$content = ob_get_clean();
			
			$html2pdf = new HTML2PDF('P', 'A2', 'en');
			//$html2pdf->setDefaultFont('arial');
			//$html2pdf->pdf->SetMargins(20,18);
			//$html2pdf->pdf->SetTopMargin(100);
			//$html2pdf->pdf->SetDisplayMode('fullpage');
			$html2pdf->writeHTML($content);
			$pdfdoc = $html2pdf->Output('../invoices/invoice_'.$this_invoice_id.'.pdf' , 'F');
			
			$message = "Invoice Successfully Saved";
			unset($_POST);
			unset($_SESSION['html_data']);
			echo '<script>window.open("http://dev.bookings-global.com.au/invoices/invoice_'.$this_invoice_id.'.html");</script>';
			
		
}
	
if (isset($_POST['save_send']))
	{

		// INSERT THESE VALUES IN INVOICE TABLE	
		
		if($row1['payment_status_id'] == '1' || $row1['payment_status_id'] == '2') //customer is a collect or charge customer
			{
				if($row1['discount'] != '0.00')
					{
						$query7 = "INSERT INTO invoice 
									(charge_acc_id, status_id, sent_status_id, base_fare, extras, surcharge, total_ex_gst, gst, total_amount, notes) VALUES
									('".$_POST['charge_acc_id']."','2', '1','".$_POST['GT_base_fare']."', '".$_POST['GT_extras']."', '".$_POST['GT_surcharge']."', '".$_POST['GT_ex_gst']."', '".$_POST['GST_after_discount']."', '".$_POST['GT_fare_after_discount']."', '".$_POST['discount_in_percentage_string discount']." applied on invoice')";
					}
				else
					{
						$query7 = "INSERT INTO invoice 
									(charge_acc_id, status_id, sent_status_id, base_fare, extras, surcharge, total_ex_gst, gst, total_amount) VALUES
									('".$_POST['charge_acc_id']."','2', '1','".$_POST['GT_base_fare']."', '".$_POST['GT_extras']."', '".$_POST['GT_surcharge']."', '".$_POST['GT_ex_gst']."', '".$_POST['GT_gst']."', '".$_POST['GT_fare']."')";
					}
			}
		else
			{
				if($row1['discount'] != '0.00')
					{
						$query7 = "INSERT INTO invoice 
									(charge_acc_id, status_id, sent_status_id, base_fare, extras, surcharge, total_ex_gst, gst, total_amount, notes) VALUES
									('".$_POST['charge_acc_id']."','1', '1','".$_POST['GT_base_fare']."', '".$_POST['GT_extras']."', '".$_POST['GT_surcharge']."', '".$_POST['GT_ex_gst']."', '".$_POST['GST_after_discount']."', '".$_POST['GT_fare_after_discount']."', '".$_POST['discount_in_percentage_string']." discount applied on invoice')";
					}
				else
					{
						$query7 = "INSERT INTO invoice 
									(charge_acc_id, status_id, sent_status_id, base_fare, extras, surcharge, total_ex_gst, gst, total_amount) VALUES
									('".$_POST['charge_acc_id']."','1', '1','".$_POST['GT_base_fare']."', '".$_POST['GT_extras']."', '".$_POST['GT_surcharge']."', '".$_POST['GT_ex_gst']."', '".$_POST['GT_gst']."', '".$_POST['GT_fare']."')";
					}
			}	
			
			$result7 = $database->query($query7);
			$this_invoice_id = mysql_insert_id();
			
			$jobs_arr = explode(',',$_POST['jobs']);
			foreach($jobs_arr as $job_id)
				{
					$job = new Job();
					$job->updateJob($job_id, 'cur_pay_status_id', '7'); 
					//$job->updateJob($job_id, 'invoice_id', $this_invoice_id);
					$job->addJobPaymentStatusTable( $job_id, '7', $_SESSION['EMAIL']);
					
					$query = "INSERT INTO invoice__items
								(invoice_id, job_id) VALUES
								('$this_invoice_id','$job_id')";
					$result = $database->query($query);
				}
				
			$html= gzuncompress($_SESSION['html_data']); 
			$html= str_replace('###NEXT_INVOICE_ID###', $this_invoice_id, $html);
			
			$myFile = "../invoices/invoice_".$this_invoice_id.".html";
			$fh = fopen($myFile, 'w') or die("can't open file");
			fwrite($fh, $html);
			fclose($fh);

			ob_start();
			include('../invoices/invoice_'.$this_invoice_id.'.html');
			$content = ob_get_clean();
			
			$html2pdf = new HTML2PDF('P', 'A2', 'en');
			$html2pdf->setDefaultFont('arial');
			//$html2pdf->pdf->SetMargins(20,18);
			//$html2pdf->pdf->SetTopMargin(100);
			//$html2pdf->pdf->SetDisplayMode('fullpage');
			$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
			$pdfdoc = $html2pdf->Output('../invoices/invoice_'.$this_invoice_id.'.pdf' , 'F');
			
			//-----------------INSERT HERE INTO INVOICE SENT TABLE, sent to all customers-----------------------------
			if($_POST['send_id'] !='')
			{
				$send_id_arr = explode(',',$_POST['send_id']);
				foreach($send_id_arr  as $send_to)
					{
						
						$query8 = "Select * from user where id='".$send_to."'";
						$result8 = $database->query($query8);
						$row8 = mysql_fetch_array($result8);
						$sent_to_name = "".$row8['title']." ".$row8['fname']." ".$row8['lname']."";
						
						
						$query9 = "INSERT INTO invoice__sent
									(invoice_id, sent_to, email) VALUES
									('$this_invoice_id','$sent_to_name', '".$row8['email']."')";
						$result9 = $database->query($query9);
						$from = $row2['email'];
						$fromname = $row2['bus_name'];
						$to_email = $row8['email'];
						$to_title = $row8['title'];
						$to_fname = $row8['fname'];
						$to_lname = $row8['lname'];
						$mail_body = $_POST['mail_body_inv'];
						$mail_body= str_replace('###TITLE###', $row8['title'], $mail_body);
						$mail_body= str_replace('###FNAME###', $row8['fname'], $mail_body);
						$mail_body= str_replace('###LNAME###', $row8['lname'], $mail_body);
						$mail_body= str_replace('###ADMIN_EMAIL###', $row2['email'], $mail_body);
						$mail_body= str_replace('###ADMIN_PHONE###', $row2['ph_1'], $mail_body);
						$mail_body= str_replace('###ADMIN_BUS###', $row2['bus_name'], $mail_body);
						
						$mailer = new mailer();
						$mailer->sendInvoice($from, $fromname, $to_email, $to_title, $to_fname, $to_lname,$this_invoice_id, $mail_body);
						$message = "Invoice Successfully Saved and sent";
						
						//now change the status 'invoice sent status'  to 2 (means SAVED AND SENT)
						$query3 = "UPDATE invoice SET sent_status_id='2' where id='".$this_invoice_id."'";
						$result3 = $database->query($query3);
					}
			}
			else{
					$message = "Invoice Successfully Saved. No recipients found for sending the Invoice";
			}
			
			unset($_POST);
			unset($_SESSION['html_data']);
			echo '<script>window.open("http://dev.bookings-global.com.au/invoices/invoice_'.$this_invoice_id.'.html");</script>';
	}

//for previewing
if (isset($_POST['preview_send']))
{
	
	if(isset($_POST['job_id']) && isset($_POST['send_to']))
	{
		echo '<link href="../Css/style.css" rel="stylesheet"  />';
		echo '<script src="../Scripts/jquery-1.10.2.min.js"></script>
			<script src="../Scripts/jquery-ui.min.js"></script>
			<script src="../Scripts/jquery.timeentry.min.js"></script>
			<script src="../Scripts/jquery.jtable.min.js"></script>
			<script src="../Scripts/jquery.validate.js"></script>
			<script src="../Scripts/jquery.validationEngine.js"></script>
			<script src="../Scripts/jquery.validationEngine-en.js"></script>
			<script src="../Scripts/chosen.jquery.min.js"></script>
			<script src="../Scripts/invoicing.js"></script>';
		echo '<script src="../Scripts/tinymce/tinymce.min.js" type="text/javascript"></script>';
		echo '<script src="../Scripts/tinymce/tinymce_init.js" type="text/javascript"></script>';
		
		//Get the details of this customer

		$query3 = "SELECT * from charge_acc where id=".$_POST['charge_acc'];
		$result3 = $database->query($query3);
		$row3 = mysql_fetch_array($result3);
		
		if($_POST['acc_type'] == '3')
		{
			$invoice->InvoiceForUniversities($_POST['charge_acc'],$GTT_fare,$_POST['job_id'], $_POST['send_to']);
		}
		else
		{
			$invoice->InvoiceForCorporates($_POST['charge_acc'],$GTT_fare,$_POST['job_id'],$_POST['send_to'],$_POST['is_credit_card_surcharge'], $_POST['credit_card_surcharge']);
		}
		
		$html = gzuncompress($_SESSION['html_data']);
		
				echo '<div id="box">';
				echo '<form name="preview_form" method="post" >';
				echo '<div id="heading">Preview of the Invoice</div><br/>';
				echo $html;
				
				echo '<div id="heading">Mail Body</div><br/>';
				echo '<div align="center" style="color:red; font-weight:bold" >Please do not change text inside #\'s </div> ';
				
				echo '<textarea name="mail_body_inv" >Dear ###TITLE### ###FNAME### ###LNAME###<br/><br/>
				
				
Please find attached your current tax invoice for our services.<br/><br/>
				  
					  
Kindly ensure if you are paying by EFT, you have correct and up to date account details as listed at the bottom of your recent invoice <b>and that you include the invoice number you are paying</b>.<br/>
If you have pre- arranged with our office to have your credit card debited, please treat this as your receipt.</br>
					  
Thank you for your business, and should you have any enquiries regarding your account please do not hesitate to contact us via e-mail : ###ADMIN_EMAIL### , or telephone on: ###ADMIN_PHONE###.<br/><br/>
					  
					  
Kind Regards<br/>
Admin<br/>
###ADMIN_BUS### 
					  </textarea><br/><br/>';
			$_SESSION['html_data'] =  gzcompress($html);
				echo '<input type="hidden" name="charge_acc_id" value="'.$_POST['charge_acc'].'">
					  <input type="hidden" name="jobs" value="'.(($_POST['job_id']!='')?implode(',',$_POST['job_id']):'').'">
					  <input type="hidden" name="send_id" value="'.(($_POST['send_to']!='')?implode(',',$_POST['send_to']):'').'">
					  <input type="hidden" name="GT_base_fare" value="'.$GT_base_fare.'">
					  <input type="hidden" name="GT_extras" value="'.$GT_extras.'">
					  <input type="hidden" name="GT_surcharge" value="'.$GT_surcharge.'">
					  <input type="hidden" name="GT_ex_gst" value="'.$GT_ex_gst.'">
					  <input type="hidden" name="GST_after_discount" value="'.$GST_after_discount.'">
					  <input type="hidden" name="GT_fare_after_discount" value="'.$GT_fare_after_discount.'">
					  <input type="hidden" name="discount_in_percentage_string" value="'.$discount_in_percentage_string.'">
					  <input type="hidden" name="GT_gst" value="'.$GT_gst.'">
					  <input type="hidden" name="GT_fare" value="'.$GT_fare.'">
					  <input type="submit" name="cancel" value="Back" class="sml_btn" />
					  <input type="submit" name="save_only" value="Save Only" class="big_btn" >
					  <input type="submit" name="save_send" value="Save and Send" class="big_btn" >
					  <input type="button" name="reload" value="Refresh" class="big_btn" id="reload" >'; 
						  
				echo '</form>';
				echo '</div>';
	}	
	else{
		if( !(isset($_POST['job_id'])) )
			$error_message = 'Please Select Some Jobs';
		elseif( !(isset($_POST['send_to'])))
			$error_message = 'Please Select Some Receivers of the invoice';
			
		unset($_POST);
	}
			
}
	
if(!isset($_POST['preview_send']) || $_POST['preview_send']=='')
{
	if(isset($_SESSION['html_data']))
		unset($_SESSION['html_data']);
 	/*echo '<style> .ui-autocomplete .ui-menu .ui-widget .ui-widget-content .ui-corner-all {
			margin-top:-35px !important;
			}</style>';*/
	echo '
	<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
<link href="../Css/minified/jquery-ui-1.10.3.custom.min.css" rel="stylesheet"  />
	<link href="../Css/jtable/themes/lightcolor/blue/jtable.css" rel="stylesheet"  type="text/css" />
	<link href="../Css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
	<link href="../Css/style.css" rel="stylesheet"  />
	<link href="../Css/menu.css" rel="stylesheet"  />
	<link href="../Css/chosen.min.css" rel="stylesheet"  />
	<link href="../Css/autocomplete.css" rel="stylesheet"  />
	<link href="../Css/jquery.timeentry.css" rel="stylesheet"  />

	
	<script src="../Scripts/jquery-1.10.2.min.js"></script>
	<script src="../Scripts/jquery-ui.min.js"></script>
	<script src="../Scripts/jquery.timeentry.min.js"></script>
	<script src="../Scripts/jquery.jtable.min.js"></script>
	<script src="../Scripts/jquery.validate.js"></script>
	<script src="../Scripts/jquery.validationEngine.js"></script>
	<script src="../Scripts/jquery.validationEngine-en.js"></script>
	<script src="../Scripts/chosen.jquery.min.js"></script>
	<script src="../Scripts/invoicing.js"></script>

</head>
<body>
	<form action="'.$PHP_SELF.'" method="post" name="make_invoice_form" id="make_invoice_form">
	<div id="box">
		<div id="heading">Make New Invoice</div><br/>';
		if($message != '')
			{
				echo '<div class="success">'.$message.'</div>';
				unset($message);
			}
		
		if (!empty($error_message))
			{
			  echo "<span class='error'>$error_message</span>";
			  unset($error_message);
			}
		echo'
		<h2>STEP 1 - SELECT CUSTOMER</h2>
			<table>
				<tr>
					<td valign="top">
						<label>Charge account Type</label>
							<select name="acc_type" id="acc_type">
								<option value="">Please Select</option>';
								$database = new database();
								$query = "SELECT * from variable__charge_acc_type order by order_id ASC";
											$result = $database->query($query);
											while ($row = mysql_fetch_array($result))
												{
													echo "<option value='".$row['id']."'>".$row['details']."</option>";
												}
											echo '
							</select>
					<br/>
					<div style="padding:10px;" id="date_range">
						<div style="padding:10px; border: 1px solid #BFC1C2; margin:10px; ">
							<input type="radio" name="select_range" class="select_range" checked="checked" value="1" />
							<label>Select Date Range</label><br />
							<input type="text" name="date_from" id="date_from" size="20" placeholder="Date From" value="'.$_POST['date_from'].'" />
							<input type="hidden" name="cap_date_from" id="cap_date_from" value="'.$_POST['cap_date_from'].'" />
							<input type="text" name="date_to" id="date_to" size="20" placeholder="Date To" value="'. $_POST['date_to'].'" />
							<input type="hidden" name="cap_date_to" id="cap_date_to" value="'.$_POST['cap_date_to'].'" />
							
							<br /><strong>OR</strong><br />
							<input type="radio" name="select_range" class="select_range" value="2" />
							<label>Preset Dates</label><br />
							<input type="radio" name="preset_dates" class="preset_dates_cls" value="1" disabled /><img src="../Images/lastmon.PNG" height="50">
							<input type="radio" name="preset_dates" class="preset_dates_cls" value="2" disabled /><img src="../Images/lastfortnight.PNG" height="50">
							<input type="radio" name="preset_dates" class="preset_dates_cls" value="3" disabled /><img src="../Images/previous_week.png" height="50">
							<input type="radio" name="preset_dates" class="preset_dates_cls" value="6" disabled /><img src="../Images/thismon.PNG" height="50">
							<input type="radio" name="preset_dates" class="preset_dates_cls" value="4" disabled /><img src="../Images/this_week.png" height="50">
							<input type="radio" name="preset_dates" class="preset_dates_cls" value="5" disabled /><img src="../Images/today.png" height="50">
						</div><br /><br />
						<input type="checkbox" name="cancelled_job" id="cancelled_job" checked="checked" value="1" />
						&nbsp; &nbsp; Show Cancelled Job<br />
						<br /><br />
						
						<input type="checkbox" name="is_credit_card_surcharge" id="is_credit_card_surcharge" value="1" />
						&nbsp; &nbsp; Credit Card Surcharge <input type="text" name="credit_card_surcharge" id="credit_card_surcharge" value="'.$row2['credit_card_surcharge'].'" />  <br />
						The Credit card surcharge can be changed either in the settings or manually be inputted.<br /><br />
					</div><br />
					
							<div id="row_customer"></div>
							<div class="loading" style="display: none"><br><label>Please Wait..</label><img src="../Images/loading.gif" /></div>
							<br>
					</td>
				</tr>
			</table>';
			
		echo '<h2>STEP 2 - SELECT JOBS</h2><br>
			<div id="uninvoiced_jobs"></div>
			<br>';
		
		echo'
			<div ><input type="checkbox" name="include_message" checked value="1"><b>Include following message in this Invoice</b> (This value can be auto set in business settings)<br/>';
			//<input type="text" size="100" name="invoice_message" id="inv_message" value=""><br/><br/>';
		echo '<input id="inv_message" name="invoice_message" size="100" value="'.$row2['invoice_message'].'" /></div>';
		
		echo '<div style=" padding-top:50px;"><input type="submit" class="big_btn" name="preview_send" id="preview_send" value="Preview of the Invoice"></div>
			
	</div>
	</form>';

	
	
}


?>
