<?php
session_start();
require_once('../init.php');
include(''.INCLUDE_PATH.'config.php');
include(''.INCLUDE_PATH.'settings.php');
include(''.INCLUDE_PATH.'auth.php');
include(''.INCLUDE_PATH.'functions.php');
include(''.INCLUDE_PATH.'functions_date_time.php');
include(''.CLASSES_PATH.'database.php');
include(''.CLASSES_PATH.'chargeAccount.php');
include(''.CLASSES_PATH.'job.php');
include(''.CLASSES_PATH.'mailer.php');
include(''.CLASSES_PATH.'validation.php');

$database 	= 	new database;


$query = "SELECT * from customer order by cust_name ASC";
$result = $database->query($query);
$GT_PAID = 0;
while($row = mysql_fetch_array($result))
	{
		$query1 = "SELECT 
				invoice.id as id,
				invoice.customer_id as customer_id,
				invoice.status_id as status_id,
				invoice.base_fare as base_fare,
				invoice.extras as extras,
				invoice.surcharge as surcharge,
				invoice.total_ex_gst as total_ex_gst,
				invoice.gst as gst,
				invoice.total_amount as total_amount,
				invoice.notes as notes,
				DATE_FORMAT(invoice.c_date,'%d-%m-%Y') as c_date,
				invoice__status.details as invoice_status_id,
				invoice__sent_status.details as invoice_sent_status,
				customer.id as cust_id,
				customer.cust_name
				from $db_name.invoice
				LEFT JOIN $db_name.invoice__status ON $db_name.invoice.status_id = $db_name.invoice__status.id
				LEFT JOIN $db_name.invoice__sent_status ON $db_name.invoice.sent_status_id = $db_name.invoice__sent_status.id
				LEFT JOIN $db_name.customer ON $db_name.invoice.customer_id = $db_name.customer.id
				where invoice.status_id = '2'
				AND invoice.customer_id = '".$row['id']."'
				ORDER BY customer.cust_name, invoice.id ASC";
		$result1 = $database->query($query1);
		$no_of_invoices = mysql_num_rows($result1);
		$total_paid = 0;
		if($no_of_invoices > 0)
			{
				echo '<h3>'.$row['cust_name'].' -('.$no_of_invoices.')</h3><div><table width="100%" id="listBookings">
					<tr>
						<th colspan="12">PAID INVOICES OF - '.$row['cust_name'].'</th>
					</tr>';
				while ($row1 = mysql_fetch_array($result1))
					{
						echo '
						<tr id="hide_rows">
							<td>
								<a href="http://limotransfer.com.au/'.$_SESSION['FOLDERNAME'].'/invoices/invoice_'.$row1['id'].'.html" target="_blank">HTML</a>
								<a href="http://limotransfer.com.au/'.$_SESSION['FOLDERNAME'].'/invoices/invoice_'.$row1['id'].'.pdf" target="_blank">PDF</a>
							</td>
							<td>'.$row1['id'].'</td>	
							<td>'.$row1['invoice_status_id'].'</td>
							<td>'.$row1['invoice_sent_status'].'</td>
							<td>'.$row1['c_date'].'</td>
							<td style="text-align:right;">'.$row1['base_fare'].'</td>
							<td style="text-align:right;">'.$row1['extras'].'</td>
							<td style="text-align:right;">'.$row1['surcharge'].'</td>
							<td style="text-align:right;">'.$row1['total_ex_gst'].'</td>
							<td style="text-align:right;">'.$row1['gst'].'</td>
							<td style="text-align:right;"><b>$'.$row1['total_amount'].'</b></td>
							<td>'.$row1['notes'].'</td>
						</tr>';	
						$total_paid = $total_paid + $row1['total_amount'];
						$GT_PAID = $GT_PAID + $row1['total_amount'];
					}
					echo '
					<tr>
						<td  style="text-align:right;" colspan="10"><b>TOTAL PAID</b></td>
						<td style="text-align:right;"><b>$'.number_format($total_paid,2).'</b></td>
						<td></td>
					</tr>';
				echo '</table></div>';
			}
	}
echo'<b>TOTAL PAID = $'.number_format($GT_PAID,2).'';
?>