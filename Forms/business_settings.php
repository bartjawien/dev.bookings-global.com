<?php
session_start();
require_once('../init.php');
//include(''.INCLUDE_PATH.'auth.php');
include(''.INCLUDE_PATH.'config.php');
include(''.INCLUDE_PATH.'settings.php');
include(''.INCLUDE_PATH.'auth.php');
include(''.INCLUDE_PATH.'functions.php');
include(''.INCLUDE_PATH.'functions_date_time.php');
include(''.CLASSES_PATH.'database.php');


$database 	= 	new database;

if($_POST['update'])
{
	
	$query1 = "Update settings__business SET bus_name = '".$_POST['bus_name']."',
													abn = '".$_POST['abn']."',
													ph_1 = '".$_POST['ph_1']."',
													ph_2 = '".$_POST['ph_2']."',
													email = '".$_POST['email']."',
													website = '".$_POST['website']."',
													address = '".$_POST['address']."',
													bank_name = '".$_POST['bank_name']."',
													account_name = '".$_POST['account_name']."',
													bsb = '".$_POST['bsb']."',
													account_number = '".$_POST['account_number']."',
													cheque_payable_to = '".$_POST['cheque_payable_to']."',
													paypal_email = '".$_POST['paypal_email']."',
													invoice_message = '".$_POST['invoice_message']."',
													credit_card_surcharge = '".$_POST['credit_card_surcharge']."'";
													
	$result1 = $database->query($query1);
	if($result1)
	{
		$message='Data Updated successfully';
	}
	else
	{
		$error_message = 'Sorry Unable to update';
	}
}

//First see if the customer is setup with a surcharge
$query1 = "select * from settings__business";
$result1 = $database->query($query1);
$row1 = mysql_fetch_array($result1);


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<link href="../Css/minified/jquery-ui-1.10.3.custom.min.css" rel="stylesheet"  />
	<link href="../Css/jtable/themes/lightcolor/blue/jtable.css" rel="stylesheet"  type="text/css" />
	<link href="../Css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
	<link href="../Css/style.css" rel="stylesheet"  />
	<link href="../Css/menu.css" rel="stylesheet"  />
	<link href="../Css/chosen.min.css" rel="stylesheet"  />
	<link href="../Css/autocomplete.css" rel="stylesheet"  />
	<link href="../Css/jquery.timeentry.css" rel="stylesheet"  />

	
	<script src="../Scripts/jquery-1.10.2.min.js"></script>
	<script src="../Scripts/jquery-ui.min.js"></script>
	<script src="../Scripts/jquery.timeentry.min.js"></script>
	<script src="../Scripts/jquery.jtable.min.js"></script>
	<script src="../Scripts/jquery.validate.js"></script>
	<script src="../Scripts/jquery.validationEngine.js"></script>
	<script src="../Scripts/jquery.validationEngine-en.js"></script>
	<script src="../Scripts/chosen.jquery.min.js"></script>
	
</head>
<body>
			
<form name="bus_settings" id="bus_settings" action="" method="POST">
	<h1> Update Business Settings</h1>
	<div class="success"><?php echo $message;
								unset($message); ?>
	</div>
	<div class="error"><?php echo $error_message;
								unset($error_message); ?>
	</div>
	<table>
		<tr> 
			<td>Business Name</td>
			<td><input type="text" id="bus_name" name="bus_name" value="<?php echo $row1['bus_name']; ?>" /></td>
		<tr>
		<tr> 
			<td>ABN</td>
			<td><input type="text" id="abn" name="abn" value="<?php echo $row1['abn']; ?>" /></td>
		<tr>
		<tr> 
			<td>Phone 1</td>
			<td><input type="text" id="ph_1" name="ph_1" value="<?php echo $row1['ph_1']; ?>" /></td>
		<tr>
		<tr> 
			<td>Phone 2</td>
			<td><input type="text" id="ph_2" name="ph_2" value="<?php echo $row1['ph_2']; ?>" /></td>
		<tr>
		<tr> 
			<td>Email</td>
			<td><input type="text" id="email" name="email" value="<?php echo $row1['email']; ?>" /></td>
		<tr>
		<tr> 
			<td>Website</td>
			<td><input type="text" id="website" name="website" value="<?php echo $row1['website']; ?>" /></td>
		<tr>
		<tr> 
			<td>Address</td>
			<td><textarea id="address" name="address" rows="2" cols="50" ><?php echo $row1['address']; ?></textarea></td>
		<tr>
		<tr> 
			<td>Bank Name</td>
			<td><input type="text" id="bank_name" name="bank_name" value="<?php echo $row1['bank_name']; ?>" /></td>
		<tr>
		<tr> 
			<td>Account Name</td>
			<td><input type="text" id="account_name" name="account_name" value="<?php echo $row1['account_name']; ?>" /></td>
		<tr>
		<tr> 
			<td>BSB</td>
			<td><input type="text" id="bsb" name="bsb" value="<?php echo $row1['bsb']; ?>" /></td>
		<tr>
		<tr> 
			<td>Account Number</td>
			<td><input type="text" id="account_number" name="account_number" value="<?php echo $row1['account_number']; ?>" /></td>
		<tr>
		<tr> 
			<td>Cheque Payable to</td>
			<td><input type="text" id="cheque_payable_to" name="cheque_payable_to" value="<?php echo $row1['cheque_payable_to']; ?>" /></td>
		<tr>
		<tr> 
			<td>Paypal Email</td>
			<td><input type="text" id="paypal_email" name="paypal_email" value="<?php echo $row1['paypal_email']; ?>" /></td>
		<tr>
		<tr> 
			<td>Invoice Message</td>
			<td><input type="text" id="invoice_message" name="invoice_message" value="<?php echo $row1['invoice_message']; ?>" /></td>
		<tr>
		<tr> 
			<td>Credit Card Surcharge</td>
			<td><input type="text" id="credit_card_surcharge" name="credit_card_surcharge" value="<?php echo $row1['credit_card_surcharge']; ?>" /></td>
		<tr>
		<tr> 
			<td colspan="2"><input type="submit" id="update" name="update" value="Update" class="sml_btn" /></td>
		<tr>
	</table>
</form>
</body>
</html>

