<?php
ini_set("memory_limit","1024M");
session_start();
require_once('../init.php');
include(''.INCLUDE_PATH.'config.php');
include(''.INCLUDE_PATH.'settings.php');
include(''.INCLUDE_PATH.'auth.php');
//include(''.INCLUDE_PATH.'functions.php');
//include(''.INCLUDE_PATH.'functions_date_time.php');
include(''.CLASSES_PATH.'database.php');
include(''.CLASSES_PATH.'chargeAccount.php');
include(''.CLASSES_PATH.'job.php');
include(''.CLASSES_PATH.'mailer.php');
include(''.CLASSES_PATH.'validation.php');
include(''.CLASSES_PATH.'invoice.php');
require_once('../html2pdf/html2pdf.class.php');

$database 	= 	new database;
echo'
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
<link href="../Css/minified/jquery-ui-1.10.3.custom.min.css" rel="stylesheet"  />
	<link href="../Css/jtable/themes/lightcolor/blue/jtable.css" rel="stylesheet"  type="text/css" />
	<link href="../Css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
	<link href="../Css/style.css" rel="stylesheet"  />
	<link href="../Css/menu.css" rel="stylesheet"  />
	<link href="../Css/chosen.min.css" rel="stylesheet"  />
	<link href="../Css/autocomplete.css" rel="stylesheet"  />
	<link href="../Css/jquery.timeentry.css" rel="stylesheet"  />
	
	<script src="../Scripts/jquery-1.10.2.min.js"></script>
	<script src="../Scripts/jquery-ui.min.js"></script>
	<script src="../Scripts/jquery.timeentry.min.js"></script>
	<script src="../Scripts/jquery.jtable.min.js"></script>
	<script src="../Scripts/jquery.validate.js"></script>
	<script src="../Scripts/jquery.validationEngine.js"></script>
	<script src="../Scripts/jquery.validationEngine-en.js"></script>
	<script src="../Scripts/chosen.jquery.min.js"></script>
	<script src="../Scripts/make_new_invoice_university.js"></script>

</head>
<br/><br/>
<div id="dialog_1" style="display:none;"></div>
<table width="100%">
	<tr>
		<td>
			<table>
				<tr>
					<td>Select Customer</td>
					<td>
						<select name="charge_account_id" id="charge_account_id">
							<option value="">PLEASE SELECT A CHARGE ACCOUNT</option>
							<option value="99999">MONASH COMBINED</option>';
							$query = "select * from charge_acc where acc_type = '3' order by account_name ASC";
							$result = $database->query($query);
							while($row = mysql_fetch_array($result))
								{
									echo'<option value="'.$row['id'].'">'.$row['account_name'].'</option>';
								}
						echo'
						</select>
					</td>
				</tr>
				<tr>
					<td>From Date:</td>
					<td>
						<input type="text" name="from_date" id="from_date">
						<input type="hidden" name="hidden_from_date" id="hidden_from_date">
					</td>
				</tr>
				<tr>
					<td>To Date:</td>
					<td>
						<input type="text" name="to_date" id="to_date">
						<input type="hidden" name="hidden_to_date" id="hidden_to_date">
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="button" name="search_jobs" id="search_jobs" class="approve_button" value="SEARCH FOR JOBS">
						<span id="ajaxBusy"><img src="http://dev.bookings-global.com.au/Images/loading_small.gif"></span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="padding:10px; background-color:#f4eefe; text-align:center;">
			<div id="result_message"></div>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<br/><br/>
			<div id="list_uninvoiced_jobs_for_this_account"></div>
		</td>
	</tr>
</table>';
?>
