<?php
error_reporting('0');

/**------------------------DO NOT CHANGE THE SETTINGS BELOW AS THEY MAY EFFECT THE FUNCTIONALITY OF SOFTWARE -----------------------------------**/
	//APPLICATION PATH SETTINGS
	define('APP_PATH', realpath(dirname(__FILE__)) . '/');
	if (!defined('ADMIN_PATH')) 		{	define('ADMIN_PATH', APP_PATH . 'Admin/');	}
	if (!defined('CLASSES_PATH')) 		{	define('CLASSES_PATH', APP_PATH . 'Classes/');	}
	if (!defined('CRONS_PATH')) 		{	define('CRONS_PATH', APP_PATH . 'Crons/');	}
	if (!defined('INCLUDE_PATH')) 		{	define('INCLUDE_PATH', APP_PATH . 'Include/');	}
	if (!defined('MEMBERS_PATH')) 		{	define('MEMBERS_PATH', APP_PATH . 'Members/');	}
	if (!defined('FORMS_PATH')) 		{	define('FORMS_PATH', APP_PATH . 'Forms/');	}
	if (!defined('DATA_TABLES_PATH')) 	{	define('DATA_TABLES_PATH', APP_PATH . 'Data_tables/');	}

	//ROOT ADDRESS SETTINGS (change in production mode to http://alliedcars.com.au)
	if (!defined('ROOT_ADDRESS'))		{ 	define('ROOT_ADDRESS', 'http://dev.bookings-global.com.au/');	}
	if (!defined('CSS_PATH'))			{ 	define('CSS_PATH', ROOT_ADDRESS . 'Css/');	}
	if (!defined('SCRIPTS_PATH')) 		{	define('SCRIPTS_PATH', ROOT_ADDRESS . 'Scripts/');}
	if (!defined('IMAGES_PATH')) 		{	define('IMAGES_PATH', ROOT_ADDRESS . 'Images/');}

	//COOKIE SETTINGS
	if (!defined('COOKIE_NAME'))		{ 	define('COOKIE_NAME', 'ALLIED');	}

	//DOMAIN SETTINGS
	if (!defined('DOMAIN_NAME'))		{ 	define('DOMAIN_NAME', 'http://dev.bookings-global.com.au/');	}
/**------------------------------------------------------------------------------------------------------------------------------------------------**/	
/**------------------------------------------------------------------------------------------------------------------------------------------------**/	
/**------------------------------------------------------------------------------------------------------------------------------------------------**/	
/**------------------------------------------------------------------------------------------------------------------------------------------------**/	
/**------------------------CHANGE THE SETTINGS BELOW AS PER YOUR REQUIREMENTS----------------------------------------------------------------------**/

	//VERSION SETTINGS
	if (!defined('CURRENT_VERSION'))	{ 	define('CURRENT_VERSION', '1.1.0');	}
	
	//DEVELOPER INFOORMATION
	if (!defined('DEVELOPED_BY'))		{ 	define('DEVELOPED_BY', 'Allied Chauffeured Cars Australia Pty Ltd');	}
	if (!defined('DEVELOPER_NAME'))		{ 	define('DEVELOPER_NAME', 'AM');	}
	if (!defined('DEVELOPER_CONTACT'))	{ 	define('DEVELOPER_CONTACT', '03 8383 9999');	}
	if (!defined('DEVELOPER_EMAIL'))	{ 	define('DEVELOPER_EMAIL', 'juan@alliedcars.com.au');	}

	//BUSINESS SETTINGS
	if (!defined('BUSINESS_NAME'))		{ 	define('BUSINESS_NAME', 'ALLIED CARS');	}
	if (!defined('BUSINESS_ADDRESS'))	{ 	define('BUSINESS_ADDRESS', '5-7 Mcintosh Street, Airport West, VIC 3042');	}
	if (!defined('COPYRIGHT_NOTICE'))	{ 	define('COPYRIGHT_NOTICE', 'Copyright Allied Cars &#169;2013');	}
	if (!defined('BUSINESS_NO_REPLY'))	{ 	define('BUSINESS_NO_REPLY', 'noreply@alliedcars.com.au');	}
	if (!defined('BUSINESS_PHONE'))		{ 	define('BUSINESS_PHONE', '(03) 8383 9999');	}

	//ERROR REPORT EMAIL
	if (!defined('ERROR_REPORT_EMAIL'))	{ 	define('ERROR_REPORT_EMAIL', 'juan@alliedcars.com.au');	} 	//Some important system logs will be emailed here to track the performance of the software
/**--------------------------------------------------------------------------------------------------------------------------------------------------**/
?>