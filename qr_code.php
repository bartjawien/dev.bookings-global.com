<?php
/**
 * QR Code + Logo Generator
 *
 * http://labs.nticompassinc.com
 */
 
 // http://dev.bookings-global.com.au/qr_code.php?data=ABC%0AAlliedCars
//$data = isset($_GET['data']) ? $_GET['data'] : 'http://alliedcars.com.au';
/*
$data = 'Monash Connect : Job ID - 123456, Student - Ms. Ching Wong (ID 868895), Destination: Monash College, Clayton VIC 3080';
$size = '200x200';
$logo = FALSE;
header('Content-type: image/png');
// Get QR Code image from Google Chart API
// http://code.google.com/apis/chart/infographics/docs/qr_codes.html
$QR = imagecreatefrompng('https://chart.googleapis.com/chart?cht=qr&chld=H|1&chs='.$size.'&chl='.urlencode($data));
if($logo !== FALSE)
	{
		$logo = imagecreatefromstring(file_get_contents($logo));
		$QR_width = imagesx($QR);
		$QR_height = imagesy($QR);
		
		$logo_width = imagesx($logo);
		$logo_height = imagesy($logo);
		
		// Scale logo to fit in the QR Code
		$logo_qr_width = $QR_width/3;
		$scale = $logo_width/$logo_qr_width;
		$logo_qr_height = $logo_height/$scale;
		
		imagecopyresampled($QR, $logo, $QR_width/3, $QR_height/3, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
	}
imagepng($QR);
imagedestroy($QR);
*/
$qr = file_get_contents("https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=Example");

//$mail->addStringAttachment($qr, "qr.png");
?>