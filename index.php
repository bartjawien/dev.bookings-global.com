<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
error_reporting('0');
require_once('init.php');
include(''.INCLUDE_PATH.'config.php');
include(''.CLASSES_PATH.'database.php');
include(''.CLASSES_PATH.'login.php');
include(''.CLASSES_PATH.'mailer.php');
//print_r($_COOKIE);

/*if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARTDED_FOR'] != '') 
	{
		$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} 
else 
	{
		$ip_address = $_SERVER['REMOTE_ADDR'];
	}*/
	function getIP() {
    if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown")) {
        $ip = getenv("HTTP_CLIENT_IP");
    } else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown")) {
        $ip = getenv("HTTP_X_FORWARDED_FOR");
    } else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown")) {
        $ip = getenv("REMOTE_ADDR");
    } else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown")) {
        $ip = $_SERVER['REMOTE_ADDR'];
    } else {
        $ip = "unknown";
    }
    return($ip);
}
$ip_address = getIP();
if(isset($_GET['login_reminder']))
	{
		if($_GET['login_reminder'] == '1' && $_GET['email'] && $_GET['password'])
			{
				$login = new Login();
				$email 		= mysql_real_escape_string($_GET['email']);
				$password	= mysql_real_escape_string($_GET['password']);
				$login = new Login();
				$login_result = $login->check_login($email, $password, 1, $ip_address);
				if($login_result['result'] == '1')
					{
						header('Location: Admin/index.php');
					}
			}
	}
if(isset($_GET['send_password']))
	{
		if($_GET['send_password'] == '1' && $_GET['email'])
			{
				$login = new Login();
				$email = mysql_real_escape_string($_GET['email']);
				$reminder_result = $login->send_password_reminder($email);
				$message = $reminder_result['message'];
			}
	}

if(isset($_COOKIE[''.COOKIE_NAME.''])) // this means the cookie is found for login
	{	
		$email 		=	$_COOKIE[''.COOKIE_NAME.'']['EMAIL'];
		$password 	=	$_COOKIE[''.COOKIE_NAME.'']['PASSWORD'];
		$login = new Login();
		$login_result = $login->check_login($email, $password, 1, $ip_address);
		if($login_result['result'] == '1')
			{
				header('Location: Admin/index.php');
			}
	}
if(isset($_POST['login']))
	{
		$login = new Login();
		$login_result = $login->check_login($_POST['email'], $_POST['password'], $_POST['remember'], $ip_address);
		if($login_result['result'] == '1')
			{
				header('Location: Admin/index.php');
			}
		else
			{
				$message = $login_result['message'];
			}
	}

?>
<!DOCTYPE html> 
<html> 
	<head> 
		 <meta charset="utf-8">
		<title>Allied Cars - Admin Home</title>
		<link href="Css/style.css" rel="stylesheet"  />
		
	</head> 
	<body> 
	<form name="login_form" id="login_form" action="" method="POST">
	<table id="login_table">
		<tr>
			<th colspan="2">Members Login</th>
		</tr>
		<?php
		if($message != '')
			{
				echo'<tr><td colspan="2"><span class="error">'.$message.'</span></td></tr>';
			}
		?>
		<tr>
			<td>Email</td>
			<td><input type="email" name="email" id="email" value="<?php echo $_POST['email']; ?>" size="25px"></td>
		</tr>
		<tr>
			<td>Password</td>
			<td><input type="password" name="password" id="password"  value="<?php echo $_POST['password']; ?>" size="25px"></td>
		</tr>
		<tr>
			<td><input type="checkbox" name="remember" id="remember" value="1" checked />Remember Me</td>
			<td><input type="submit" name="login" id="login" value="LOGIN" /></td>
		</tr>
	</table>
	</form>
	</body>
</html>