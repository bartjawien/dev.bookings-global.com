<?php
error_reporting('0');

session_start();
require_once('../init.php');
//include(''.INCLUDE_PATH.'auth.php');
include(''.INCLUDE_PATH.'config.php');
include(''.INCLUDE_PATH.'settings.php');
include(''.INCLUDE_PATH.'functions.php');
include(''.CLASSES_PATH.'database.php');
include(''.CLASSES_PATH.'chargeAccount.php');

//ROLE IDS
//	1 - ADMIN
//	2 - University
// 	3 - Charge Account

$database = new database;
$charge_account = new ChargeAccount();

if($_SESSION['ROLE_ID'] == '3')
	{
		$query = "SELECT * from charge_acc__contacts where user_id = '".$_SESSION['USER_ID']."'";
		$result = $database->query($query);
		$row = mysql_fetch_assoc($result);
		$charge_account_details = $charge_account->getChargeAccountDetails($row['charge_acc_id']);
		$charge_acc_id = $row['charge_acc_id'];
	}
echo'
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Allied Cars - Admin Home</title>
	<link href="../Css/minified/jquery-ui-1.10.3.custom.min.css" rel="stylesheet"  />
	<link href="../Css/jtable/themes/lightcolor/blue/jtable.css" rel="stylesheet"  type="text/css" />
	<link href="../Css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
	<link href="../Css/style.css" rel="stylesheet"  />
	<link href="../Css/menu.css" rel="stylesheet"  />
	<link href="../Css/chosen.min.css" rel="stylesheet"  />
	<link href="../Css/autocomplete.css" rel="stylesheet"  />
	<link href="../Css/jquery.timeentry.css" rel="stylesheet"  />
	<link rel="stylesheet" type="text/css" href="../Css/easyui/themes/default/easyui.css">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="../Scripts/jquery-ui.min.js"></script>
	<script src="../Scripts/jquery.timeentry.min.js"></script>
	<script src="../Scripts/jquery.jtable.min.js"></script>
	<script src="../Scripts/jquery.validate.js"></script>
	<script src="../Scripts/jquery.validationEngine.js"></script>
	<script src="../Scripts/jquery.validationEngine-en.js"></script>
	<script src="../Scripts/chosen.jquery.min.js"></script>
	<script src="../Scripts/browsing_menu.js"></script>
	<script src="../Scripts/start_browsing_menu.js"></script>
	<script src="../Scripts/list_all_bookings.js"></script>
	<script src="../Scripts/jquery.easyui.min.js"></script>
	<script src="../Scripts/tabs.js"></script>
</head>
<body>
	<table style="margin:0px; padding:0px;" width="100%"; border:0px;>
		<tr>
			<td>
				<table width="100%" style="background:#4781a5; color:#fff;  border:1px solid #0b0296; margin:0px; padding:0px;">
					<tr>
						<td style="border:0px solid #000;">
							<img src="../Images/logo.png">&nbsp;&nbsp;&nbsp;&nbsp;Allied Chauffeured Cars Australia Pty Ltd';
							if($_SESSION['ROLE_ID'] == '3') // Customer
								{
									echo ' - <b>'.$charge_account_details['account_name'],' Online Booking System</b>';
								}
							else
								{
									echo' - Version 2.0.1';
								}
						echo'
						</td>
						<td style="text-align:right;">
							Welcome, you are logged in as '.$_SESSION['TITLE'].' '.$_SESSION['FNAME'].' '.$_SESSION['LNAME'].'
						</td>
					</tr>
				</table>
				
				<table  style="background:#0b0296;" width="100%">
					<tr>
						<td>
							<div id="smoothmenu1" class="ddsmoothmenu">
								<ul>';
								if($_SESSION['ROLE_ID'] == '3') // customer
									{	
										echo'<li><a href="#" onclick="addTab(\'New Booking\',\'../Forms/form_new_booking.php\')"><img src="../Images/New_Booking.png" border="0px"  height="50px" width="67px"></a></li>';
										echo'<li><a href="#" onclick="addTab(\'Todays Bookings\',\'http://dev.bookings-global.com.au/Data_tables/list_all_bookings.php?content_id=list_all_bookings&dt_from=today&dt_to=today\')"><img src="../Images/Today.png" border="0px" height"50px" width="67px"></a></li>';
											// <li><a href="#" onclick="addTab(\'Search Bookings\',\'../Forms/search_bookings.php\')"><img src="../Images/search.png" border="0px"  height="50px" width="67px"></a></li>';
										if( $charge_acc_id == '514' || $charge_acc_id == '513' || $charge_acc_id == '513' )
											{
												echo '<li><a href="#" onclick="addTab(\'Report \',\'../Data_tables/report_jobs_allotted_to_drivers.php\')"><img src="../Images/report.png" border="0px" height"50px" width="67px"></a></li>';
											}
									}
								if($_SESSION['ROLE_ID'] == '2') // driver
									{
										echo'<li><a href="#"onclick="addTab(\'Todays Bookings\',\'http://dev.bookings-global.com.au/Data_tables/list_all_bookings.php?content_id=list_all_bookings&dt_from=today&dt_to=today\')"><img src="../Images/Today.png" border="0px" height"45px" width="62px"></a></li>';
										echo '<li><a href="#" onclick="addTab(\'Report \',\'../Data_tables/report_jobs_allotted_to_drivers.php\')"><img src="../Images/report.png" border="0px" height"50px" width="67px"></a></li>';
									}
								if($_SESSION['ROLE_ID'] == '1') // admin
									{
										echo'<li><a href="#" onclick="addTab(\'New Booking\',\'../Forms/form_new_booking.php\')"><img src="../Images/New_Booking.png" border="0px"  height="45px" width="62px"></a></li>';
										echo'<li><a href="#"onclick="addTab(\'Todays Bookings\',\'http://dev.bookings-global.com.au/Data_tables/list_all_bookings.php?content_id=list_all_bookings&dt_from=today&dt_to=today\')"><img src="../Images/Today.png" border="0px" height"45px" width="62px"></a></li>';
										echo'<li><a href="#"><img src="../Images/Table.png" border="0px" height"45px" width="62px"></a>
												<ul>
													<li><a href="#" onclick="addTab(\'Charge Accounts\',\'../Data_tables/list_charge_accounts.php\')">List Charge Accounts</a></li>
													<li><a href="#" onclick="addTab(\'Partners\',\'../Data_tables/list_partners.php\')">List Partners</a></li>
													<li><a href="#" onclick="addTab(\'Drivers\',\'../Data_tables/list_drivers.php\')">List Drivers</a></li>
													<li><a href="#" onclick="addTab(\'Admins\',\'../Data_tables/list_admins.php\')">List Admins</a></li>
													<li><a href="#" onclick="addTab(\'Driver Groups\',\'../Data_tables/list_driver_groups.php\')">List Driver Groups</a></li>
												</ul>
											</li>';	
										echo'<li><a href="#"><img src="../Images/report.png" border="0px" height"45px" width="62px"></a>
												<ul>
													
													<li><a href="#" onclick="addTab(\'Report - Drivers\',\'../Data_tables/report_jobs_allotted_to_drivers.php\')">Drivers</a></li>
												</ul>
											</li>';
										if($_SESSION['ROLE_ID'] == '1') // ADMIN
											{
												echo'<li><a href="#"><img src="../Images/invoices.png" border="0px" height"45px" width="62px"></a>
														<ul>
															<li><a href="#" onclick="addTab(\'New Invoice\',\'../Forms/new_invoice.php\')">New Invoice</a></li>
															<li><a href="#" onclick="addTab(\'New Uni Invoice\',\'../Forms/new_invoice_university.php\')">New Uni Invoice</a></li>
															<li><a href="#" onclick="addTab(\'List Invoices\',\'../Forms/list_invoices.php\')">List Invoices</a></li>
														</ul>
													</li>';
											}
										echo'<li><a href="#"><img src="../Images/accounts.png" border="0px" height"45px" width="62px"></a>
												<ul>
													<li><a href="#">New Entry</a>
														<ul>
															<li><a href="#" onclick="addTab(\'Cash in entry\',\'../Forms/cash_in.php\')">Cash in entry</a></li>
															<li><a href="#" onclick="addTab(\'Cash out entry\',\'../Forms/cash_out.php\')">Cash out entry</a></li>
															<li><a href="#" onclick="addTab(\'Credit entry\',\'../Forms/credit_entry.php\')">Credit entry</a></li>
															<li><a href="#" onclick="addTab(\'Debit entry\',\'../Forms/debit_entry.php\')">Debit entry</a></li>
														</ul>
													</li>
													<li><a href="#" onclick="addTab(\'Driver Payments\',\'../Forms/driver_payments.php\')">Driver Payments</a></li>
												</ul>
											</li>';
										
										echo'<li><a href="#"><img src="../Images/settings.png" border="0px" height"45px" width="62px"></a>
												<ul>
													<li><a href="#" onclick="addTab(\'Business Settings\',\'../Forms/business_settings.php\')">Business Settings</a></li>
													<li><a href="#" onclick="addTab(\'Fare Estimator\',\'../Forms/fare_estimator_1.php\')">Fare Estimator - Clients</a></li>
													<li><a href="#" onclick="addTab(\'Fare Estimator\',\'../Forms/fare_estimator_2.php\')">Fare Estimator - Admin</a></li>
													<li><a href="#" onclick="addTab(\'POIs\',\'../Data_tables/list_pois.php\')">Point of Interests</a></li>
													<li><a href="#" onclick="addTab(\'Cars\',\'../Data_tables/list_car_types.php\')">Car Types</a></li>
													<li><a href="#" onclick="addTab(\'Var Estimator\',\'../Data_tables/list_variable_estimator_settings.php\')">Fare Estimator Settings - Variable</a></li>
													<li><a href="#" onclick="addTab(\'Fix Estimator\',\'../Data_tables/list_fixed_estimator_settings.php\')">Fare Estimator Settings - Fixed</a></li>
													
												</ul>
											</li>';
										echo'<li><a href="#"><img src="../Images/Log.png" border="0px" height"45px" width="62px"></a>
												<ul>
													<li><a href="#" onclick="addTab(\'Booking Log\',\'../Data_tables/log_bookings.php\')">Bookings</a></li>
													<li><a href="#" onclick="addTab(\'Driver Log\',\'../Data_tables/log_driver.php\')">Drivers</a></li>
												</ul>
											</li>';
											/*
											<li><a href="#" onclick="addTab(\'Search Bookings\',\'../Forms/search_bookings.php\')"><img src="../Images/search.png" border="0px"  height="50px" width="67px"></a></li>
											*/
									}
								echo'
								</ul>
							<br style="clear: left" />
							</div>
						</td>
						<td style="text-align:right;">
							<a href="#"><img src="../Images/Help.png" border="0px" height"45px" width="62px"></a>
							<a href="../Admin/logout.php"><img src="../Images/logout.png" border="0px" height"45px" width="62px"></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<div id="tt" class="easyui-tabs"></div>
</body>
</html>';
?>