<?php
require_once('../init.php');
include(''.INCLUDE_PATH.'config.php');
include(''.CLASSES_PATH.'database.php');
echo'
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>NEW BOOKING FORM</title>
	<link href="../Css/minified/jquery-ui-1.10.3.custom.min.css" rel="stylesheet"  />
	<link href="../Css/style.css" rel="stylesheet"  />
	<link href="../Css/menu.css" rel="stylesheet"  />
	<link href="../Css/chosen.min.css" rel="stylesheet"  />
	<link href="../Css/autocomplete.css" rel="stylesheet"  />
</head>
<body>
	<table style="margin:0px; padding:0px;" width="100%"; border:0px;>
		<tr>
			<td align="center">';
				include(''.FORMS_PATH.'form_new_booking.php');
			echo'
			</td>
		</tr>
	</table>
</body>
<script src="../Scripts/jquery-1.9.1.js"></script>
<script src="../Scripts/jquery-ui.min.js"></script>
<script src="../Scripts/jquery.validate.min"></script>
<script src="../Scripts/chosen.jquery.min.js"></script>
<script src="../Scripts/browsing_menu.js"></script>
<script src="../Scripts/start_browsing_menu.js"></script>
<script src="../Scripts/form_new_booking.js"></script>
</html>';
?>