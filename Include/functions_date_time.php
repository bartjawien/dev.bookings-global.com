<?php
/*---------------------------------------------------------------------------------------------------------------------*/
function getTimeDiff($t1,$t2) 
		{
			$x1	= explode(" ",$t1); 
			$x2	= explode(" ",$t2); 
			$d1 = explode("-",$x1[0]); 
			$d2 = explode("-",$x2[0]);
			$a1 = explode(":",$x1[1]); 
			$a2 = explode(":",$x2[1]); 
			$time1 = (($d1[0]*365*24*60*60)+ ($d1[1]*30*24*60*60) + ($d1[2]*24*60*60) + ($a1[0]*60*60)+ ($a1[1]*60) +($a1[2])); 
			$time2 = (($d2[0]*365*24*60*60)+ ($d2[1]*30*24*60*60) + ($d2[2]*24*60*60) + ($a2[0]*60*60)+ ($a2[1]*60)+ ($a2[2])); 
			$diff = ($time1-$time2);
			return $diff;
		}

		
/*---------------------------------------------------------------------------------------------------------------------*/
function microtime_float () 
	{ 
		list ($msec, $sec) = explode(' ', microtime()); 
		$microtime = (float)$msec + (float)$sec; 
		return $microtime; 
	} 
	
/*---------------------------------------------------------------------------------------------------------------------*/

//This function can take date in the following formats
//10-12-2013 OR 10/12/2012
//Returns date in YY-mm-dd (2012-12-10) format suited to be inserted as date field in mysql database. 
function calculateDateInYYmmddFormat($date)
	{
		if ((strpos($date,'-') !== false))
			{
				//means date is in dd-mm-yyyy format
				$date_array 	= 	explode("-",$date);
				$job_date 		= 	date('d-m-Y', mktime(0,0,0,$date_array[1],$date_array[0],$date_array[2]));
				return $job_date;
			}
		else if ((strpos($date,'/') !== false))
			{
				//means date is in dd-mm-yyyy format
				$date_array 	= 	explode("/",$date);
				$job_date 		= 	date('d-m-Y', mktime(0,0,0,$date_array[1],$date_array[0],$date_array[2]));
				return $job_date;
			}
	}
$date 	= 	"17/01/2013";
	
/*---------------------------------------------------------------------------------------------------------------------*/
//Takes input in mysql date format 2013-12-31
//Returns date in following formats as per selections
// Selection 1 	= 	dd-mm-yyyy
// Selection 2 	= 	dd/mm/yyyy
// Selection 3 	= 	31-Dec-2013
// Selection 4 	= 	31/Dec/2013
// Selection 5 	= 	Sun, 31-12-2013
// Selection 6 	= 	Sun, 31/12/2013
// Selection 7 	= 	Sun, 31-Dec-2013
// Selection 8 	= 	Sun, 31/Dec/2013
// Selection 9 	= 	31st Dec 2013
// Selection 10 = 	31st December 2013
// Selection 11 = 	31st Dec 2013, Sun
// Selection 12 = 	31st December 2013, Sunday


function formatDate($date, $selection_type)
	{
		if($selection_type == '1')
			{
				$date_array 	= 	explode("-",$date);
				$new_date		=   date('d-m-Y', mktime(0,0,0,$date_array[1],$date_array[2],$date_array[0]));
				return $new_date;  //dd-mm-yyyy
			}
		if($selection_type == '2')
			{
				$date_array 	= 	explode("-",$date);
				$new_date		=   date('d/m/Y', mktime(0,0,0,$date_array[1],$date_array[2],$date_array[0]));
				return $new_date;   //dd/mm/yyyy
			}
		if($selection_type == '3')
			{
				$date_array 	= 	explode("-",$date);
				$new_date		=   date('d-M-Y', mktime(0,0,0,$date_array[1],$date_array[2],$date_array[0]));
				return $new_date;  //31-Dec-2013
			}
		if($selection_type == '4')
			{
				$date_array 	= 	explode("-",$date);
				$new_date		=   date('d/M/Y', mktime(0,0,0,$date_array[1],$date_array[2],$date_array[0]));
				return $new_date;   //31/Dec/2013
			}
		if($selection_type == '5')
			{
				$date_array 	= 	explode("-",$date);
				$new_date		=   date('D, d-m-Y', mktime(0,0,0,$date_array[1],$date_array[2],$date_array[0]));
				return $new_date;   //Sun, 31-12-2013
			}
		if($selection_type == '6')
			{
				$date_array 	= 	explode("-",$date);
				$new_date		=   date('D, d/m/Y', mktime(0,0,0,$date_array[1],$date_array[2],$date_array[0]));
				return $new_date;   //Sun, 31/12/2013
			}
		if($selection_type == '7')
			{
				$date_array 	= 	explode("-",$date);
				$new_date		=   date('D, d-M-Y', mktime(0,0,0,$date_array[1],$date_array[2],$date_array[0]));
				return $new_date;   //Sun, 31-Dec-2013
			}
		if($selection_type == '8')
			{
				$date_array 	= 	explode("-",$date);
				$new_date		=   date('D, d/M/Y', mktime(0,0,0,$date_array[1],$date_array[2],$date_array[0]));
				return $new_date;   //Sun, 31/Dec/2013
			}
		if($selection_type == '9')
			{
				$date_array 	= 	explode("-",$date);
				$new_date		=   date('jS M Y', mktime(0,0,0,$date_array[1],$date_array[2],$date_array[0]));
				return $new_date;   //31st Dec 2013
			}
		if($selection_type == '10')
			{
				$date_array 	= 	explode("-",$date);
				$new_date		=   date('jS F Y', mktime(0,0,0,$date_array[1],$date_array[2],$date_array[0]));
				return $new_date;   //31st December 2013
			}
		if($selection_type == '11')
			{
				$date_array 	= 	explode("-",$date);
				$new_date		=   date('jS M Y, D', mktime(0,0,0,$date_array[1],$date_array[2],$date_array[0]));
				return $new_date;   //31st Dec 2013, Sun
			}
		if($selection_type == '12')
			{
				$date_array 	= 	explode("-",$date);
				$new_date		=   date('jS F Y, l', mktime(0,0,0,$date_array[1],$date_array[2],$date_array[0]));
				return $new_date;   //31st December 2013, Sunday
			}
	}
/*---------------------------------------------------------------------------------------------------------------------*/ 
//This function can take time in the following inputs
//22:30 or 10:30 PM
//Returns time in 24 hour format with trailing seconds in the fomat HH:ii:ss
function calculateTimeInHisFormat($time)
	{
		if ((strpos($time,'AM') !== false) || (strpos($time,'PM') !== false))
			{
				$time_array_1 	= 	explode(" ",$time);
				$time_array_2	=	explode(":",$time_array_1[0]);
				if($time_array_1[1] == 'AM')
					{
						$new_hrs 	= 	$time_array_2[0];
						$new_mins	=	$time_array_2[1];
						$job_time 		= 	date('H:i:s', mktime($new_hrs,$new_mins,0));
						return $job_time;
					}
				else if($time_array_1[1] == 'PM')
					{
						$new_hrs 	= 	$time_array_2[0] + 12;
						$new_mins	=	$time_array_2[1];
						$job_time 		= 	date('H:i:s', mktime($new_hrs,$new_mins,0));
						return $job_time;
					}
			}
		else
			{
				$time_array 	= 	explode(":",$time);
				$job_time 		= 	date('H:i:s', mktime($time_array[0],$time_array[1],0));
				return $job_time;
			}
	} 
/*---------------------------------------------------------------------------------------------------------------------*/

//Takes input as time in mysql time format i.e. HH:ii:ss
//Returns time in AM PM format like 12:39 PM without seconds
function calculateTimeInAmPmFormatWithoutSeconds($time)
	{
		$time_array 	= 	explode(":",$time);
		$new_time = date('g:i A', mktime($time_array[0],$time_array[1],0));
		return $new_time;
	}
	
/*---------------------------------------------------------------------------------------------------------------------*/
//Takes input as time in mysql time format i.e. HH:ii:ss
//Returns time in AM PM format like 12:39:05 PM with seconds
function calculateTimeInAmPmFormatWithSeconds($time)
	{
		$time_array 	= 	explode(":",$time);
		$new_time = date('g:i:s A', mktime($time_array[0],$time_array[1],$time_array[2]));
		return $new_time;
	}
/*---------------------------------------------------------------------------------------------------------------------*/
//Takes input as time in mysql time format i.e. HH:ii:ss
//Returns time in HH:ii format
function showTimeWithoutSeconds($time)
	{
		$time_array 	= 	explode(":",$time);
		$new_time = "".$time_array[0].":".$time_array[1]."";
		return $new_time;
	}
?>