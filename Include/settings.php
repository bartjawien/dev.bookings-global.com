<?php
/*
PLEASE BE CAREFUL IN CHAGNING THE VALUES IN THIS PAGE
All the values in this page define the system settings and any changes will be reflected in the system all over.
To change your individual settings, please go to preferences section. No individual preferences can be changed from here.

Note any changes made in this page will reflect on prospective basis rather than retrospective basis.
*/
$_CHARGE_ACCOUNT_TYPE 	= array(
									"1" => "Corporate",
									"2" => "Personal",
									"3" => "University",
									"4" => "Other",
								);
$_CONTACT_TYPE 	= array(
									"1" => "Passenger",
									"2" => "Booking",
									"3" => "Billing",
								);
								
$_BOOKING_STATES 		= array(
									"VIC" => "VIC",
									"NSW" => "NSW",
									"QLD" => "QLD",
									"ACT" => "ACT",
									"SA" => "SA",
									"WA" => "WA",
									"TAS" => "TAS",
									"NT" => "NT",
									"NZ" => "NZ",
								);
								
$_BOOKING_CAR_TYPES 	= array(
									"1" => "Sedan",
									"2" => "Sedan NO GAS",
									"3" => "BMW",
									"4" => "Mercedes",
									"5" => "Audi",
									"6" => "Viano",
									"7" => "VAN 7 Seater",
									"8" => "Van 12 Seater",
									"9" => "Minibus",
									"10" => "Coach",
									"11" => "Stretch",
								);

$_USER_ROLE 			= array(
									""  => "Select",
									"1" => "Admin",
									"2" => "Driver",
									"3" => "Charge Acc Pax",
									"4" => "Charge Acc Bookings",
									"5" => "Charge Acc Billings",
								);
								
$_USER_PHONE_TYPE 		= array(
									"1" => "Mobile",
									"2" => "Work",
									"3" => "Home",
									"4" => "Others",
								);
								
$_USER_EMAIL_TYPE 		= array(
									"1" => "Work",
									"2" => "Personal",
								);
								
$_USER_ADDRESS_TYPE 	= array(
									"1" => "Work",
									"2" => "Home",
									"3" => "Others",
								);
								
$_FLEET_CAR_LICENCES 	= array(
									""  => "Select",
									"1" => "Hire Car Full",
									"2" => "Hire Car Restricted",
									"3" => "Bus",
									"4" => "Un Licenced",
									"5" => "Not Known",
									"6" => "Others",
								);
$_FLEET_CAR_SHAPE 		= array(
									"1" => "Current",
									"2" => "Non Current",
								);
$_FLEET_CAR_FUEL_TYPE 	= array(
									"1" => "Petrol",
									"2" => "Gas",
									"3" => "Diesel",
									"4" => "Hybrid",
									"5" => "Electric",
								);
$_FLEET_CAR_COLOR 		= array(
									"" => "",
									"1" => "White",
									"2" => "Grey",
									"3" => "Black",
									"4" => "Silver",
									"5" => "Blue",
									"6" => "Others",
								);
$_FLEET_CAR_MAKE 		= array(
									""  => "",
									"1" => "Holden",
									"2" => "Mercedes",
									"3" => "BMW",
									"4" => "Audi",
									"5" => "Ford",
									"6" => "Toyota",
								);
$_FLEET_CAR_MODEL 		= array(
									""  => "",
									"1" => "Caprice",
									"2" => "SL",
									"3" => "LI",
									"4" => "Others",
								);
								
$_MAX_PAX_NOS 	= 		57;
$_MAX_LUGGAGE 	= 		57;
$_MAX_BABY_SEAT 	= 	2;
$_MAX_BOOSTER_SEAT 	= 	2;
$_MAX_CAPSULE 	= 		2;


?>