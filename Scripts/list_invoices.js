$(document).ready(function()
	{
		$("#charge_account_id").chosen({disable_search_threshold: 10});
		$('#ajaxBusy').hide();
		var dateToday = new Date();
		$( "#from_date" ).datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'dd-mm-yy',
			altField: "#hidden_from_date",
			altFormat: "yy-mm-dd"
		});
		$("#to_date" ).datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'dd-mm-yy',
			altField: "#hidden_to_date",
			altFormat: "yy-mm-dd"
		});
		
		
	
		$(document).on('click', '#search_jobs', function(event) {
			
			$('#ajaxBusy').show();
			var charge_account_id 	= 	$("#charge_account_id").val();
			var date_type 			= 	$("#date_type").val();
			var from_date 			= 	$("#hidden_from_date").val();
			var to_date 			= 	$("#hidden_to_date").val();
			var invoice_status 		= 	$("#invoice_status").val();
			var invoice_sent_status = 	$("#invoice_sent_status").val();
			
			$.getJSON("../Ajax/list_invoices.php?charge_account_id="+charge_account_id+"&date_type="+date_type+"&from_date="+from_date+"&to_date="+to_date+"&invoice_status="+invoice_status+"&invoice_sent_status="+invoice_sent_status, function(data) {
				$('#list_invoices').html(data.data);
				$('#ajaxBusy').hide();
			});
		});
		
		$(document).on('click', '.show_pdf', function(event) {
			var invoice_id = $(this).prop('id');
			$.getJSON("../Ajax/list_invoices.php?show_pdf=1&invoice_id="+invoice_id, function(data) {
				$('#dialog_show_pdf').html(data.data);
				$("#dialog_show_pdf").dialog({
					title: 'Invoice ID - '+invoice_id+'.pdf',
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 800,
					height: 800,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui) {}
				});
			});
		});
	
		$(document).on('click', '.show_html', function(event) {
			var invoice_id = $(this).prop('id');
			$.getJSON("../Ajax/list_invoices.php?show_html=1&invoice_id="+invoice_id, function(data) {
				$('#dialog_show_html').html(data.data);
				$("#dialog_show_html").dialog({
					title: 'Invoice ID - '+invoice_id+'.html',
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 800,
					height: 800,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui) {}
					
				});
			});
		});

		$(document).on('click', '.add_payment', function(event) {
			var invoice_id = $(this).prop('id');
			$.getJSON("../Ajax/list_invoices.php?add_payment=1&invoice_id="+invoice_id, function(data) {
				$('#dialog_add_payment').html(data.data);
				$("#payment_date" ).datepicker({
					changeMonth: true,
					changeYear: true,
					dateFormat: 'dd-mm-yy',
					altField: "#hidden_payment_date",
					altFormat: "yy-mm-dd"
				});
				$("#dialog_add_payment").dialog({
					title: 'Invoice ID - '+invoice_id,
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 400,
					height: 400,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui)
						{
							$("#add_payment").click(function() {
									$('#ajaxBusy1').show();
									$("#add_payment").prop("disabled", true);
									var invoice_id 	= 	$("#invoice_id").val();
									var invoice_new_status 	= 	$("#invoice_new_status").val();
									var payment_date 		= 	$("#payment_date").val();
									var amount_paid 		= 	$("#amount_paid").val();
									var payment_method 		= 	$("#payment_method").val();
									var payment_notes 		= 	$("#payment_notes").val();
									$.getJSON("../Ajax/list_invoices.php?add_payment_exec=1&invoice_id="+invoice_id+"&invoice_new_status="+invoice_new_status+"&payment_date="+payment_date+"&amount_paid="+amount_paid+"&payment_method="+payment_method+"&payment_notes="+payment_notes, function(data) {
									$('#ajaxBusy1').hide();
									$("#dialog_add_payment").html(data.data);
								});
							});
						}
				});
			});
		});
		
		$(document).on('click', '#check_all_receivers', function(event) {
			if(this.checked) 
				{ // check select status
					$('.to_receivers').each(function() {
						this.checked = true;
					});
				}
			else{
					$('.to_receivers').each(function() {
						this.checked = false;                 
					});         
				}
		});
		
		$(document).on('click', '.send_invoice', function(event) {
			var invoice_id = $(this).prop('id');
			$.getJSON("../Ajax/list_invoices.php?send_invoice=1&invoice_id="+invoice_id, function(data) {
				$('#dialog_send_invoice').html(data.data);
				$("#dialog_send_invoice").dialog({
					title: 'Invoice ID - '+invoice_id,
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 800,
					height: 400,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui)
						{
							$("#send_invoice_button").click(function() {
									$('#ajaxBusy1').show();
									var receivers = [];
									$('.to_receivers:checked').each(function() 
										{
											receivers.push($(this).val());
										});
									var invoice_id 				= 	$("#invoice_id").val();
									var add_receiver_1_email 	= 	$("#add_receiver_1_email").val();
									var add_receiver_1_name 	= 	$("#add_receiver_1_name").val();
									var add_receiver_2_email 	= 	$("#add_receiver_2_email").val();
									var add_receiver_2_name 	= 	$("#add_receiver_2_name").val();
									$.getJSON("../Ajax/list_invoices.php?send_invoice_exec=1&invoice_id="+invoice_id+"&receivers="+receivers+"&add_receiver_1_name="+add_receiver_1_name+"&add_receiver_1_email="+add_receiver_1_email+"&add_receiver_2_name="+add_receiver_2_name+"&add_receiver_2_email="+add_receiver_2_email, function(data) {
									$('#ajaxBusy1').hide();
									$("#dialog_send_invoice").html(data.data);
								});
							});
						}
				});
			});
		});
		$(document).on('click', '.resend_invoice', function(event) {
			var invoice_id = $(this).prop('id');
			$.getJSON("../Ajax/list_invoices.php?resend_invoice=1&invoice_id="+invoice_id, function(data) {
				$('#dialog_resend_invoice').html(data.data);
				$("#dialog_resend_invoice").dialog({
					title: 'Resend Invoice ID - '+invoice_id,
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 500,
					height: 300,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui)
						{
							$("#resend_invoice_button").click(function() {
									$('#ajaxBusy1').show();
									var invoice_id 				= 	$("#invoice_id").val();
									$.getJSON("../Ajax/list_invoices.php?resend_invoice_exec=1&invoice_id="+invoice_id, function(data) {
									$('#ajaxBusy1').hide();
									$("#dialog_resend_invoice").html(data.data);
								});
							});
							$("#cancel_resend_button").click(function() {
								$("#dialog_resend_invoice").dialog('close');
							});
						}
				});
			});
		});
		
		$(document).on('click', '.show_log', function(event) {
			var invoice_id = $(this).prop('id');
			$.getJSON("../Ajax/list_invoices.php?show_invoice_log=1&invoice_id="+invoice_id, function(data) {
				$('#dialog_show_log').html(data.data);
				$("#dialog_show_log").dialog({
					title: 'Resend Invoice ID - '+invoice_id,
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 800,
					height: 400,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui){}
				});
			});
		});
		$(document).on('click', '.cancel_invoice', function(event) {
			var invoice_id = $(this).prop('id');
			$.getJSON("../Ajax/list_invoices.php?cancel_invoice=1&invoice_id="+invoice_id, function(data) {
				$('#dialog_cancel_invoice').html(data.data);
				$("#dialog_cancel_invoice").dialog({
					title: 'Cancel Invoice ID - '+invoice_id,
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 300,
					height: 300,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui)
						{
							$("#cancel_invoice_button").click(function() {
									$('#ajaxBusy1').show();
									var invoice_id 				= 	$("#invoice_id").val();
									$.getJSON("../Ajax/list_invoices.php?cancel_invoice_exec=1&invoice_id="+invoice_id, function(data) {
									$('#ajaxBusy1').hide();
									$("#dialog_cancel_invoice").html(data.data);
								});
							});
							$("#do_not_cancel_invoice_button").click(function() {
								$("#dialog_cancel_invoice").dialog('close');
							});
						}
				});
			});
		});
	});