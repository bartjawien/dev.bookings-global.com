$(document).ready(function()
{ 
	$("#j1_driver").data("placeholder","Select Driver").chosen();
	$(document).on('change', '#j1_driver', function(event) {
			var id=$("#j1_driver").val();
			$.ajax({
						method: "post",
						url: "../Ajax/accounting.php",
						data: "acc_bal=1&driver_id="+id,
						success: function(result){
							$("#driver_acc_balance").html("<b> Total available balance: "+result+" </b>");
							$("#driver_account_balance").val(result);
						}
					});
			
		});
	$(document).on('click', '#cancel_entry', function(event) {
		location.reload();
		
	});

	$(document).on('click', '#save_booking', function(event) {
			$('.save_booking').prop("disabled", true); // To avoid any accidental resumbit
			var id = $("#j1_driver").val();
			var cash_in_amt = $("#cash_in_amt").val();
			var notes = $("#cash_in_notes").val();
			var error='';
			error = (id=='')?'No Driver selected<br />':'';
			error = (cash_in_amt=='')?error+'Cash in Amount is blank <br />':error;
			if(error=='')
			{
				$.ajax({
							method: "post",
							url: "../Ajax/accounting.php",
							data: "save_cash_in_entry=1&driver_id="+id+"&cash_in_amt="+cash_in_amt+"&notes="+notes,
							success: function(result){
								$('.save_booking').prop("disabled", false);
								$("#message").html('Transaction done successfully');
								$("#message").css('color','green');
								$('#j1_driver').val('').trigger('chosen:updated');
								$("#driver_acc_balance").html('');
								$("#cash_in_amt").val('');
								$("#cash_in_notes").val('');
								$("#driver_account_balance").val('');
							}
						});
			}
			else{
					$('.save_booking').prop("disabled", false); // enable the submit button
					$('#dialog_submit').html(error);
					$("#dialog_submit").dialog({
						title: 'SUBMISSION ERROR',
						resizable: true,
						draggable: true,
						modal: true,
						position: 'center',
						width: 300,
						height: 200,
						buttons: {
									Ok: function() 
										{
											$( this ).dialog( "close" );
									}
							},
						show: { effect:'fade', duration:500 },
						hide: { effect:'fade', duration:400},			
						close: function (event, ui) {
							$( this ).dialog( "close" );
						},
						open: function (event, ui) {}
					})
			}
		});	

	$(document).on('click', '#save_cash_out_entry', function(event) {
			$('.save_cash_out_entry').prop("disabled", true); // enable the submit button
			var id = $("#j1_driver").val();
			var cash_out_amt = $("#cash_out_amt").val();
			var notes = $("#cash_out_notes").val();
			var driver_bal = $("#driver_account_balance").val();
			//console.log(parseFloat(driver_bal)+' '+parseFloat(cash_out_amt));
			var error='';
			error = (id=='')?'No Driver selected<br />':'';
			error = (cash_out_amt=='')?error+'Cash out Amount is blank <br />':error;
			if(error=='')
			{
				if(parseFloat(driver_bal) >= parseFloat(cash_out_amt))
				{
					$.ajax({
								method: "post",
								url: "../Ajax/accounting.php",
								data: "save_cash_out_entry=1&driver_id="+id+"&cash_out_amt="+cash_out_amt+"&notes="+notes,
								success: function(result){
									$("#message").html('Transaction done successfully');
									$("#message").css('color','green');
									$('#j1_driver').val('').trigger('chosen:updated');
									$("#driver_acc_balance").html('');
									$("#cash_out_amt").val('');
									$("#cash_out_notes").val('');
									$("#driver_account_balance").val('');
									$('.save_cash_out_entry').prop("disabled", false); // enable the submit button
								}
							});
				}
				else
				{
					$("#message").html('Sorry unable to process your request as the drivers account has insufficient amount.');
					$("#message").css('color','red');
					$("#cash_out_amt").val('');
					$("#cash_out_amt").focus();
					$('.save_cash_out_entry').prop("disabled", false); // enable the submit button
					
				}
			}
			else{
					$('.save_cash_out_entry').prop("disabled", false); // enable the submit button
					$('#dialog_submit').html(error);
					$("#dialog_submit").dialog({
						title: 'SUBMISSION ERROR',
						resizable: true,
						draggable: true,
						modal: true,
						position: 'center',
						width: 300,
						height: 200,
						buttons: {
									Ok: function() 
										{
											$( this ).dialog( "close" );
									}
							},
						show: { effect:'fade', duration:500 },
						hide: { effect:'fade', duration:400},			
						close: function (event, ui) {
							$( this ).dialog( "close" );
						},
						open: function (event, ui) {}
					})
			}
			
		});	
		
		$(document).on('click', '#save_credit_entry', function(event) {
			$('.save_credit_entry').prop("disabled", true); // enable the submit button
			var id = $("#j1_driver").val();
			var credit_amt = $("#credit_amt").val();
			var notes = $("#credit_notes").val();
			var driver_bal = $("#driver_account_balance").val();
			//console.log(parseFloat(driver_bal)+' '+parseFloat(cash_out_amt));
			var error='';
			error = (id=='')?'No Driver selected<br />':'';
			error = (credit_amt=='')?error+'Amount to be credited is blank <br />':error;
			if(error=='')
			{
				$.ajax({
						method: "post",
						url: "../Ajax/accounting.php",
						data: "save_credit_entry=1&driver_id="+id+"&credit_amt="+credit_amt+"&notes="+notes,
						success: function(result){
							$("#message").html('Transaction done successfully');
							$("#message").css('color','green');
							$('#j1_driver').val('').trigger('chosen:updated');
							$("#driver_acc_balance").html('');
							$("#credit_amt").val('');
							$("#credit_notes").val('');
							$("#driver_account_balance").val('');
							$('.save_credit_entry').prop("disabled", false); // enable the submit button
						}
					});
			}
			else{
					$('.save_credit_entry').prop("disabled", false); // enable the submit button
					$('#dialog_submit').html(error);
					$("#dialog_submit").dialog({
						title: 'SUBMISSION ERROR',
						resizable: true,
						draggable: true,
						modal: true,
						position: 'center',
						width: 300,
						height: 200,
						buttons: {
									Ok: function() 
										{
											$( this ).dialog( "close" );
									}
							},
						show: { effect:'fade', duration:500 },
						hide: { effect:'fade', duration:400},			
						close: function (event, ui) {
							$( this ).dialog( "close" );
						},
						open: function (event, ui) {}
					})
			}
			
			
		});	
	
		$(document).on('click', '#save_debit_entry', function(event) {
			$('.save_debit_entry').prop("disabled", false); // enable the submit button
			var id = $("#j1_driver").val();
			var debit_amt = $("#debit_amt").val();
			var notes = $("#debit_notes").val();
			var driver_bal = $("#driver_account_balance").val();
			
			var error='';
			error = (id=='')?'No Driver selected<br />':'';
			error = (debit_amt=='')?error+'Amount to be debited is blank <br />':error;
			if(error=='')
			{
				if(parseFloat(driver_bal) >= parseFloat(debit_amt))
				{
					$.ajax({
							method: "post",
							url: "../Ajax/accounting.php",
							data: "save_debit_entry=1&driver_id="+id+"&debit_amt="+debit_amt+"&notes="+notes,
							success: function(result){
								$("#message").html('Transaction done successfully');
								$("#message").css('color','green');
								$('#j1_driver').val('').trigger('chosen:updated');
								$("#driver_acc_balance").html('');
								$("#debit_amt").val('');
								$("#debit_notes").val('');
								$("#driver_account_balance").val('');
								$('.save_debit_entry').prop("disabled", false); // enable the submit button
							}
						});
				}
				else
				{
					$("#message").html('Sorry unable to process your request as the drivers account has insufficient amount.');
					$("#message").css('color','red');
					$("#debit_amt").val('');
					$("#debit_amt").focus();
					$('.save_debit_entry').prop("disabled", false); // enable the submit button
				}
			
			}
			else{
					$('.save_debit_entry').prop("disabled", false); // enable the submit button
					$('#dialog_submit').html(error);
					$("#dialog_submit").dialog({
						title: 'SUBMISSION ERROR',
						resizable: true,
						draggable: true,
						modal: true,
						position: 'center',
						width: 300,
						height: 200,
						buttons: {
									Ok: function() 
										{
											$( this ).dialog( "close" );
									}
							},
						show: { effect:'fade', duration:500 },
						hide: { effect:'fade', duration:400},			
						close: function (event, ui) {
							$( this ).dialog( "close" );
						},
						open: function (event, ui) {}
					})
			}
			
		});	
});

