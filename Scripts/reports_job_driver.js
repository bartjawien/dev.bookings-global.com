$(document).ready(function()
	{
		var dateToday = new Date();
		
		$( "#date_from" ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd-mm-yy',
		altField: "#cap_date_from",
		altFormat: "yy-mm-dd"
		});
		$("#date_to" ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd-mm-yy',
		altField: "#cap_date_to",
		altFormat: "yy-mm-dd"
		});
		$("#date_from_1" ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd-mm-yy',
		altField: "#cap_date_from_1",
		altFormat: "yy-mm-dd"
		});
		$("#date_to_1" ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd-mm-yy',
		altField: "#cap_date_to_1",
		altFormat: "yy-mm-dd"
		});
		
		 
			
		$('#search_by_drivers').autocomplete({
				source:'../Ajax/autocomplete_drivers.php',
				minLength:2,
				select: function(event, ui){
						$("#search_by_drivers").val(ui.item.label);
						$("#search_by_drivers_id").val(ui.item.val);
						//$("#j1_pc").val(ui.item.postcode);
					}
			});
			
		$('#search_by_pax').autocomplete({
				source:'../Ajax/autocomplete_drivers.php?pax=1',
				minLength:2,
				select: function(event, ui){
						$("#search_by_pax").val(ui.item.label);
						$("#search_by_pax_id").val(ui.item.val);
						
					}
			});
		
		$('#search_by_car').autocomplete({
				source:'../Ajax/autocomplete_drivers.php?car=1',
				minLength:2,
				select: function(event, ui){
						$("#search_by_car").val(ui.item.label);
						$("#search_by_car_id").val(ui.item.val);
						//$("#j1_pc").val(ui.item.postcode);
					}
			});
			
		$('#search_by_charge_account').autocomplete({
				source:'../Ajax/autocomplete_drivers.php?charge_acc=1',
				minLength:2,
				select: function(event, ui){
						$("#search_by_charge_account").val(ui.item.label);
						$("#search_by_charge_account_id").val(ui.item.val);
						
					}
			});
		
		$( "#tabs" ).tabs({
			beforeLoad: function( event, ui ) 
				{
					if($("#pressed_button").val() == 'search')
						{
							$('#allBookingsTable').jtable('destroy');
						}
					ui.jqXHR.error(function() 
						{
							ui.panel.html("Couldn't load this tab. We'll try to fix this as soon as possible. ");
						});
				}
		}); 
		
		/*$("a[href=#tabs-1]").click(function() {
			//console.log($("#advanced_pressed_button").val()+'CL');
			if($("#advanced_pressed_button").val() == 'search')
			{
				$('#allBookingsTable').jtable('destroy');
			}
		});*/
		
		$('.all_booking_radio_cls').click(function() 
			{
				$('.preset_dates_cls').removeAttr("checked");
				if($(this).val() == '1') 
					{
						$( "#date_from" ).val('');
						$( "#date_to" ).val('');
						$( "#cap_date_from" ).val('');
						$( "#cap_date_to" ).val('');
						$( "#date_from" ).attr('disabled','disabled');
						$( "#date_to" ).attr('disabled','disabled');
					}
				else
					{
						$("#date_from").removeAttr('disabled');
						$("#date_to").removeAttr('disabled');					
					}
			});
		
		$('.preset_dates_cls').click(function() 
			{
				$('.all_booking_radio_cls').removeAttr("checked");
				$( "#date_from" ).val('');
				$( "#date_to" ).val('');
				$( "#cap_date_from" ).val('');
				$( "#cap_date_to" ).val('');
				$( "#date_from" ).attr('disabled','disabled');
				$( "#date_to" ).attr('disabled','disabled');
				$( "#wk_from" ).val('');
				$( "#wk_to" ).val('');
				$( "#wk_counter" ).val('');
				$( "#dt_from" ).val('');
				$( "#dt_to" ).val('');
				$( "#counter" ).val('');
				$( "#yr_from" ).val('');
				$( "#yr_to" ).val('');
				$( "#mn_from" ).val('');
				$( "#mn_to" ).val('');

				switch($(this).val()) 
					{
						case '1': 
							$( "#wk_from" ).val('-1');
							$( "#wk_to" ).val('-1');
							$( "#wk_counter" ).val('0');
							break;
						case '2':
							$( "#dt_from" ).val('-1');
							$( "#dt_to" ).val('-1');
							$( "#counter" ).val('0');
							break;
						case '3':
							$( "#dt_from" ).val('today');
							$( "#dt_to" ).val('today');
							
							break;
						case '4': 
							$( "#wk_from" ).val('0');
							$( "#wk_to" ).val('0');
							$( "#wk_counter" ).val('0');
							break;
						case '5':
							$( "#dt_from" ).val('1');
							$( "#dt_to" ).val('1');
							$( "#counter" ).val('0');
							break;
						case '6': 
							$( "#wk_from" ).val('1');
							$( "#wk_to" ).val('1');
							$( "#wk_counter" ).val('0');
							break;
						case '7':
							$( "#yr_from" ).val('-1');
							$( "#yr_to" ).val('-1');
							
							break;
						case '8': 
							$( "#yr_from" ).val('-2');
							$( "#yr_to" ).val('-2');
							
							break;
						case '9':
							$( "#yr_from" ).val('1');
							$( "#yr_to" ).val('1');
							
							break;
						case '10': 
							$( "#yr_from" ).val('2');
							$( "#yr_to" ).val('2');
							
							break;
						case '11':
							$( "#mn_from" ).val('-1');
							$( "#mn_to" ).val('-1');
							
							break;
						case '12': 
							$( "#mn_from" ).val('1');
							$( "#mn_to" ).val('1');
							
							break;
					}
			});
		
		
		$('#search_specific').click(function() 
			{
				var hidden_role_id = $("#hidden_role_id").val();
		
				if($("#search_by_charge_account").val() == '')
					{
						$("#search_by_charge_account_id").val('');
					}
			
				if($("#pressed_button").val() != '')
					{
						$('#allBookingsTable').jtable('destroy');
					}
				$("#pressed_button").val('search');
				
				if(hidden_role_id == '1')
					{ 
						$('#allBookingsTable').jtable(
							{
								title: 'Bookings List ',
								paging: true, //Enable paging
								pageSize: 1500,
								sorting: true, //Enable sorting
								defaultSorting: 'job_date ASC, job_time ASC',
								selecting: true, //Enable selecting
								multiselect: true, //Allow multiple selecting
								selectingCheckboxes: true, //Show checkboxes on first column
								//selecting: true, //Enable selecting
								//multiselect: true, //Allow multiple selecting
								//selectingCheckboxes: true, //Show checkboxes on first column
								//openChildAsAccordion: true, //Enable this line to show child tabes as accordion style
								columnResizable:true,
								ajaxSettings: 
									{
										type: 'POST',
										dataType: 'json',
										data:$("#search_dates").serialize(),
										cache: false
									},
							
								cache: false,
								actions: 
									{
										listAction: '../Ajax/report_jobs_allotted_to_drivers.php'
									},
								fields: 
									{
										job_id: 			
											{
												title: 'Job ID',
												key: true,
												display: function (data) 
													{
														if(data.record.job_id != 'Driver Total' && data.record.job_id > 100 )
															{
																var $link = $('<a href="#">'+data.record.job_id+'</a>');
																$link.on('click', function(event) 
																	{
																		$.getScript("http://dev.bookings-global.com.au/Scripts/jquery.easyui.min.js");
																		$.getScript("http://dev.bookings-global.com.au/Scripts/tabs.js");
																		var url = 'http://dev.bookings-global.com.au/Forms/show_booking_details.php?job_id='+data.record.job_id+'';
																		var title = 'JOB ID - '+data.record.job_id+'';
																		var content = '<iframe  scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:1200px;"></iframe>';
																		window.parent.$('#tt').tabs('add',
																			{
																				title:title,
																				content:content,
																				closable:true
																			});
																	
																		return false;
																	});
																			
																return $link;
															}
														else 
															{
																return data.record.job_id;
															}
													}
												},
										date_time: 
											{
												title: 'Job Date Time'
											},
										current_booking_status: {title: 'Bkg Status'},
										driver_status: {title: "Driver Status"},
										driver: 
											{	
												title: 'Driver Name',
												display: function (data) 
													{
														if(data.record.job_id != 'Driver Total' && data.record.job_id < 100 && data.record.driver != '')
															{
																return "<div class='status_red'><strong>"+data.record.driver+"</strong></div>";
															}
														else
															{
																return data.record.driver;
															}
													}
											},
										vehicle_type: {title: 'Vehicle Type'},
										pax: {title: 'Pax Name'},
										pax_nos: {title: 'Pax #'},
										luggage: {title: 'Lug'},
										from_address:			{title: 'FROM'},
										from_via_address:		{title: 'VIA'},
										to_via_address:			{title: 'VIA'},
										to_address:				{title: 'TO'},
										driver_notes:			{title: 'Drivers Notes'},
										driver_price:			
											{
												title: 'Driver Price',
												display: function (data) 
													{
														if(data.record.job_id != 'Driver Total' && data.record.job_id < 100  && data.record.driver_price != '')
															{
																return "<div class='status_red'><strong>"+data.record.driver_price+"</strong></div>";
															}
														else
															{
																return data.record.driver_price;
															}
													}
											},
										extra_pay:				
											{
												title: 'Extra Pay',
												display: function (data) 
													{
														if(data.record.job_id != 'Driver Total' && data.record.job_id < 100 && data.record.extra_pay != '' )
															{
																return "<div class='status_red'><strong>"+data.record.extra_pay+"</strong></div>";
															}
														else
															{
																return data.record.extra_pay;
															}
													}
											},
										driver_fee:				
											{
												title: 'Driver Fee',
												display: function (data) 
													{
														if(data.record.job_id != 'Driver Total' && data.record.job_id < 100 && data.record.driver_fee != '')
															{
																return "<div class='status_red'><strong>"+data.record.driver_fee+"</strong></div>";
															}
														else
															{
																return data.record.driver_fee;
															}
													}
											}
									}
					
							});
					}
				else if( hidden_role_id == '3' )
					{
						$('#allBookingsTable').jtable(
							{
								title: 'Bookings List ',
								paging: true, //Enable paging
								pageSize: 1500,
								sorting: true, //Enable sorting
								defaultSorting: 'job_date ASC, job_time ASC',
								columnResizable:true,
								ajaxSettings: 
									{
										type: 'POST',
										dataType: 'json',
										data:$("#search_dates").serialize(),
										cache: false
									},
								cache: false,
								actions: 
									{
										listAction: '../Ajax/report_jobs_allotted_to_drivers.php'
									},
								fields: 
									{
										job_id: 			
											{
												title: 'Job ID',
												key: true,
												display: function (data) 
													{
														if(data.record.job_id != 'Driver Total')
															{
																var $link = $('<a href="#">'+data.record.job_id+'</a>');
																$link.on('click', function(event) 
																	{
																		$.getScript("http://dev.bookings-global.com.au/Scripts/jquery.easyui.min.js");
																		$.getScript("http://dev.bookings-global.com.au/Scripts/tabs.js");
																		var url = 'http://dev.bookings-global.com.au/Forms/show_booking_details.php?job_id='+data.record.job_id+'';
																		var title = 'JOB ID - '+data.record.job_id+'';
																		var content = '<iframe  scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:1200px;"></iframe>';
																		window.parent.$('#tt').tabs('add',
																			{
																				title:title,
																				content:content,
																				closable:true
																			});
																
																		return false;
																	});
														
																return $link;
															}
														else 
															return data.record.job_id;
													}
											},
										date_time: 
											{
												title: 'Job Date Time'
											},
										current_booking_status: {title: 'Bkg Status'},
										vehicle_type: {title: 'Vehicle'},
										pax: {title: 'Pax'},
										from_address:			{title: 'FROM'},
										from_via_address:		{title: 'VIA'},
										to_address:				{title: 'TO'}
									}
						
							});
					}
				else if( hidden_role_id == '2' )
					{
						$('#allBookingsTable').jtable(
							{
								title: 'Bookings List ',
								paging: true, //Enable paging
								pageSize: 1500,
								sorting: true, //Enable sorting
								defaultSorting: 'job_date ASC, job_time ASC',
								columnResizable:true,
								ajaxSettings: 
									{
										type: 'POST',
										dataType: 'json',
										data:$("#search_dates").serialize(),
										cache: false
									},
							
								cache: false,
								actions: 
									{
										listAction: '../Ajax/report_jobs_allotted_to_drivers.php'
									},
								fields: 
									{
										job_id: 			
											{
												title: 'Job ID',
												key: true,
												display: function (data) 
												{
													return data.record.job_id;
												}
											},
										date_time: 				{title: 'Job Date Time'},
										pax: 					{title: 'Pax'},
										from_address:			{title: 'FROM'},
										from_via_address:		{title: 'VIA'},
										to_address:				{title: 'TO'},
										to_via_address:			{title: 'VIA'},
										driver_status: 			{title: "Driver Status"},
										driver: 				{title: 'Driver'},
										vehicle_type: 			{title: 'Vehicle'},
										driver_notes:			{title: 'Driver notes'},
										driver_price:			{title: 'Driver price'},
										extra_pay:				{title: 'Extra Pay'},
										driver_fee:				{title: 'Driver Fee'}
									}
						
							});
					}
			 
				//Load student list from server
				$('#allBookingsTable').jtable('load');
				return false; 
			
			});
		$('#download_html').click(function() 
			{
				$("#pressed_button").val('');
				$("#advanced_pressed_button").val('');
				return true;
			});
		$('#download_pdf').click(function() 
			{
				$("#pressed_button").val('');
				$("#advanced_pressed_button").val('');
				return true;
			});
		$('#download_excel').click(function() 
			{
				$("#pressed_button").val('');
				$("#advanced_pressed_button").val('');
				return true;
			});
});