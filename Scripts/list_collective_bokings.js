$(document).ready(function()
	{
		var dateToday = new Date();
		$( "#date_from" ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd-mm-yy',
		altField: "#cap_date_from",
		altFormat: "yy-mm-dd"
		});
		$("#date_to" ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd-mm-yy',
		altField: "#cap_date_to",
		altFormat: "yy-mm-dd"
		});
		$("#date_from_1" ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd-mm-yy',
		altField: "#cap_date_from_1",
		altFormat: "yy-mm-dd"
		});
		$("#date_to_1" ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd-mm-yy',
		altField: "#cap_date_to_1",
		altFormat: "yy-mm-dd"
		});
		
		$('#search_by_charge_account').autocomplete({
				source:'../Ajax/autocomplete_drivers.php?charge_acc=1',
				minLength:2,
				select: function(event, ui){
						$("#search_by_charge_account").val(ui.item.label);
						$("#search_by_charge_account_id").val(ui.item.val);
						
					}
			});
		
		if($('#booking_status').val() =='')
		{
			$( "#search_by_charge_account" ).attr('disabled','disabled');
			$( "#search_by_charge_account_id" ).attr('disabled','disabled');
		}
		
		$('#booking_status').change(function() {
			if($('#booking_status').val() !='')
			{
				$("#search_by_charge_account").removeAttr('disabled');
				$("#search_by_charge_account_id").removeAttr('disabled');
			}
			else
			{
				$( "#search_by_charge_account" ).attr('disabled','disabled');
				$( "#search_by_charge_account_id" ).attr('disabled','disabled');
			}
		});
		
		
	
		
	});