$(document).ready(function()
	{
		$("#driver_id").chosen({disable_search_threshold: 10});
		
		$('#ajaxBusy').hide();
		var dateToday = new Date();
		$( "#from_date" ).datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'dd-mm-yy',
			altField: "#hidden_from_date",
			altFormat: "yy-mm-dd"
		});
		$("#to_date" ).datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'dd-mm-yy',
			altField: "#hidden_to_date",
			altFormat: "yy-mm-dd"
		});

		$(document).on('click', '#search_jobs', function(event) {
			
			$('#ajaxBusy').show();
			var driver_id 	= 	$("#driver_id").val();
			var from_date 			= 	$("#hidden_from_date").val();
			var to_date 			= 	$("#hidden_to_date").val();
			var driver_status 		= 	$("#driver_status").val();
			
			$.getJSON("../Ajax/driver_payments_exec.php?driver_id="+driver_id+"&from_date="+from_date+"&to_date="+to_date+"&driver_status="+driver_status, function(data) {
				$('#driver_jobs').html(data.data);
				$('#ajaxBusy').hide();
			});
		});
		
		$(document).on('click', '.job_id_link', function(event) {
			var job_id = $(this).prop('id');
			$.getScript("http://dev.bookings-global.com.au/Scripts/jquery.easyui.min.js");
			$.getScript("http://dev.bookings-global.com.au/Scripts/tabs.js");
			var url = 'http://dev.bookings-global.com.au/Forms/show_booking_details.php?job_id='+job_id+'';
			var title = 'JOB ID - '+job_id+'';
			var content = '<iframe  scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:1200px;"></iframe>';
			window.parent.$('#tt').tabs('add',
				{
					title:title,
					content:content,
					closable:true
				});
			return false;
		});
		
		
		$(document).on('click', '#check_all_jobs', function(event) {
			if(this.checked) 
				{ // check select status
					$('.to_pay_jobs').each(function() {
						this.checked = true;
					});
				}
			else{
					$('.to_pay_jobs').each(function() {
						this.checked = false;                 
					});         
				}
		});
		
		$(document).on('click', '#pay_now', function(event) {
			var pay_jobs = [];
			var driver_id 	= 	$("#driver_id").val();
			$('.to_pay_jobs:checked').each(function() 
				{
					pay_jobs.push($(this).val());
				});
			$.getJSON("../Ajax/driver_payments_exec.php?pay_driver=1&pay_jobs="+pay_jobs+"&driver_id="+driver_id, function(data) {
				$('#dialog_1').html(data.data);
				$("#dialog_1").dialog({
					title: 'Payment summary',
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 400,
					height: 600,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},	
					buttons: {
								Ok: function() {
								location.reload(true);
								$( this ).dialog( "close" );
							}
						},
					close: function (event, ui) {},
					open: function (event, ui) {}
				});
			});
		});
	});
	