
	function checkAll(field)
		{
			for (i = 0; i < field.length; i++)
			field[i].checked = true ;
		}
	function uncheckAll(field)
		{
			for (i = 0; i < field.length; i++)
				field[i].checked = false ;
		}


$(document).ready(function()
	{
		$(".loading").ajaxStart(function () {
			$(this).show();
		});

		$(".loading").ajaxStop(function () {
				$(this).hide();
		});

		$( "#date_from" ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd-mm-yy',
		altField: "#cap_date_from",
		altFormat: "yy-mm-dd"
		});
		$("#date_to" ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd-mm-yy',
		altField: "#cap_date_to",
		altFormat: "yy-mm-dd"
		});
		$("#date_range").hide();
		$(document).on('change', '#acc_type', function(event) {
			var id=$(this).val();
			$.getJSON("../Ajax/get_options.php?option_type=invoice&acc_type="+id, function(data) {
				$("#row_customer").html(data.data);
				$('#uninvoiced_jobs').html('');
				$("#date_range").show();
				$( "#date_from" ).val('');
				$( "#date_to" ).val('');
				$( "#cap_date_from" ).val('');
				$( "#cap_date_to" ).val('');
				var config = {
				  '.chosen-select'           : {},
				  '.chosen-select-deselect'  : {allow_single_deselect:true},
				  '.chosen-select-no-single' : {disable_search_threshold:10},
				  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
				  '.chosen-select-width'     : {width:"95%"}
				}
				for (var selector in config) {
				  $(selector).chosen(config[selector]);
				}
			});
		});
		
		$('.select_range').click(function() {
			var select_range_val = $(this).val();
			switch(select_range_val)
			{
				
				case '1':
					$("#date_from").removeAttr('disabled');
					$("#date_to").removeAttr('disabled');	
					$( ".preset_dates_cls" ).attr('disabled','disabled');
					$('.preset_dates_cls').removeAttr("checked");
					break;
					
				case '2':
					$('.preset_dates_cls').removeAttr("disabled");
					$( "#date_from" ).val('');
					$( "#date_to" ).val('');
					$( "#cap_date_from" ).val('');
					$( "#cap_date_to" ).val('');
					$( "#date_from" ).attr('disabled','disabled');
					$( "#date_to" ).attr('disabled','disabled');
					break;
					
			}
		});
		
		$(document).on('click', '#show_result', function(event) {
			var id = $('#charge_acc').val();
			
			if($("#cancelled_job").is(':checked'))
			{
				var cancel_job = '1';
			}
			else{
				var cancel_job = '0';
			}
			
			var select_range_val = $("input[name='select_range']:checked").val();
			//console.log(select_range_val);
			if(select_range_val == '1')
			{
				var c_date_from= $('#cap_date_from').val();
				var c_date_to= $('#cap_date_to').val();
				var charge_acc = 'charge_acc='+ id+'&date_from='+c_date_from+'&date_to='+c_date_to+'&cancel_job='+cancel_job;
			}
			else
			{
				var preset_dates=$("input[name='preset_dates']:checked").val();
				var charge_acc = 'charge_acc='+ id+'&preset_dates='+preset_dates+'&cancel_job='+cancel_job;
			}
			$.ajax
				({
					type: "POST",
					url: "../Ajax/invoice_form_options.php",
					//dataType: 'html',
					data: charge_acc,
					cache: false,
					success: function(html)
						{
							$('#uninvoiced_jobs').html(html);
							
						} 
				});
		});
		
		$(document).on('change', '#acc_type_for_listing', function(event) {
			var id=$(this).val();
			$.getJSON("../Ajax/get_options.php?option_type=invoice_listing&acc_type="+id, function(data) {
				$("#row_customer_for_listing").html(data.data);
				$('#list_invoices').html('');
				
				var config = {
				  '.chosen-select'           : {},
				  '.chosen-select-deselect'  : {allow_single_deselect:true},
				  '.chosen-select-no-single' : {disable_search_threshold:10},
				  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
				  '.chosen-select-width'     : {width:"95%"}
				}
				for (var selector in config) {
				  $(selector).chosen(config[selector]);
				}
			});
		});
		
		$(document).on('change', '#charge_acc_for_listing', function(event) {
			var id=$(this).val();
			var charge_acc = 'customer_id_for_listing='+ id;
			$.ajax
				({
					type: "POST",
					url: "../Ajax/invoice_form_options.php",
					//dataType: 'html',
					data: charge_acc,
					cache: false,
					success: function(html)
						{
							$('#list_invoices').html(html);
						} 
				});
		});
		
		$( "#paid_date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd-mm-yy'
		});
		
		$(document).on('change', '#acc_type_un', function(event) {
			var id=$(this).val();
			$.getJSON("../Ajax/get_options.php?option_type=invoice_un&acc_type="+id, function(data) {
				$("#row_customer_un").html(data.data);
				$('#uninvoiced_jobs').html('');
				$("#date_range").show();
				$( "#date_from" ).val('');
				$( "#date_to" ).val('');
				$( "#cap_date_from" ).val('');
				$( "#cap_date_to" ).val('');
				$( "#search" ).prop('disabled', false);
				var config = {
				  '.chosen-select'           : {},
				  '.chosen-select-deselect'  : {allow_single_deselect:true},
				  '.chosen-select-no-single' : {disable_search_threshold:10},
				  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
				  '.chosen-select-width'     : {width:"95%"}
				}
				for (var selector in config) {
				  $(selector).chosen(config[selector]);
				}
			});
		});
		
		$(document).on('click', '#preview_send', function(event) {
			var checkedNum = $('input[name="job_id[]"]:checked').length;
			if (!checkedNum) {
				alert('Please select jobs');
				return false;
			}
			else
			{
				checkedNum = $('input[name="send_to[]"]:checked').length;
				if (!checkedNum) {
					alert('Please select receiver');
					return false;
				}
			}
			return true;
		});
		
		$(document).on('click', '#reload', function(event) {
			window.location.reload(true);
			
		});
});