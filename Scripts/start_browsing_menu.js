ddsmoothmenu.init({
	mainmenuid: "smoothmenu1", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	customtheme: ["#3f3992", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})