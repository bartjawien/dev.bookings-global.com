$(document).ready(function()
{ 
	$( ".loading" ).hide();
	$(function()
		{
			$("#pickup_address, #destination_address").geocomplete({
					componentRestrictions: { country: 'au' }
				});
				
			$("#find").click(function()
				{
					$("#pickup_address, #destination_address").trigger("geocode");
				});
		});
	$(document).on('click', '#estimate_fare', function(event) {
		
			var pickup_address		=	$("#pickup_address").val();
			var destination_address = 	$("#destination_address").val();
			
			//new google.maps.places.SearchBox(document.getElementById('txtSource'));
			//new google.maps.places.SearchBox(document.getElementById('txtDestination'));
			//directionsDisplay = new google.maps.DirectionsRenderer({ 'draggable': true });
			
			var error='';
			error = (pickup_address=='')?'Pickup address required<br />':'';
			error = (destination_address=='')?error+'Destination address required<br />':error;
			$( ".loading" ).show();
			if(error=='')
				{
					$.ajax({
								method: 	"post",
								url: 		"../Ajax/fare_estimator_2.php",
								dataType: 	"json",
								data: 		"pickup_address="+pickup_address+"&destination_address="+destination_address,
								success: function(data)
									{
										$( ".loading" ).hide();
										$('.estimate_fare').prop("disabled", false);
										
										var myString = "<table width='100%' id='fareEstimatorTable'>";
										
										myString += "<tr>";
										myString += "<th class='left'>Distance</th><th colspan='3' class='left'>"+data['data1']['distance']+" Kms</th>";
										myString += "</tr>";
										
										myString += "<tr>";
										myString += "<th class='left'>Travel Time</th><th colspan='3' class='left'>"+data['data1']['time_hrs']+" hrs: "+data['data1']['time_min']+" mins</th>";
										myString += "</tr>";
										
										myString += "<tr>";
										myString += "<th  class='left'>Car Type</th>";
										myString += "<th  class='right'>Estimated Fare</th>";
										myString += "<th  class='right'>Discount Criteria</th>";
										myString += "<th  class='right'>Driver Fare</th>";
										myString += "</tr>";
										//alert(data.length);
										for (var i = 0; i < data['data2'].length; i++)
											{
												myString += "<tr>";
												myString += "<td class='left'>"+data['data2'][i]['car_type']+"</td>";
												myString += "<td class='right'>$"+data['data2'][i]['est_fare']+"</td>";
												myString += "<td class='right'>"+data['data2'][i]['amt_percentage']+"</td>";
												myString += "<td class='right'><b>$"+data['data2'][i]['driver_fare']+"</b></td>";
												myString += "</tr>";
											}
										myString += "</table>";
										$("#calculated_fare").html(myString);
									}
							});
				}
			else
				{
					$( ".loading" ).hide();
					$('.estimate_fare').prop("disabled", false); // enable the submit button
					$('#dialog_submit').html(error);
					$("#dialog_submit").dialog({
						title: 'SUBMISSION ERROR',
						resizable: true,
						draggable: true,
						modal: true,
						position: 'center',
						width: 300,
						height: 200,
						buttons: {
									Ok: function() 
										{
											$( this ).dialog( "close" );
									}
							},
						show: { effect:'fade', duration:500 },
						hide: { effect:'fade', duration:400},			
						close: function (event, ui) {
							$( this ).dialog( "close" );
						},
						open: function (event, ui) {}
					})
				}
		});	
	
	$(document).on('click', '#estimate_fare_2', function(event) {
		
			var pickup_from_area 		= 	$("#pickup_from_area").val();
			var destination_suburb 		= 	$("#destination_suburb").val();
			
			//new google.maps.places.SearchBox(document.getElementById('txtSource'));
			//new google.maps.places.SearchBox(document.getElementById('txtDestination'));
			//directionsDisplay = new google.maps.DirectionsRenderer({ 'draggable': true });
			
			var error='';
			error = (pickup_from_area=='')?'Point A required<br />':'';
			error = (destination_suburb=='')?error+'Point B required<br />':error;
			$( ".loading" ).show();
			if(error=='')
				{
					$.ajax({
								method: 	"post",
								url: 		"../Ajax/fare_estimator_2.php",
								dataType: 	"json",
								data: 		"pickup_from_area="+pickup_from_area+"&destination_suburb="+destination_suburb,
								success: function(data)
									{
										$( ".loading" ).hide();
										$('.estimate_fare').prop("disabled", false);
										
										var myString = "<table width='100%' id='fareEstimatorTable'>";
									
										myString += "<tr>";
										myString += "<th  class='left'>Car Type</th>";
										myString += "<th  class='right'>Estimated Fare</th>";
										myString += "<th  class='right'>Discount Criteria</th>";
										myString += "<th  class='right'>Driver Fare</th>";
										myString += "</tr>";
										//alert(data.length);
										for (var i = 0; i < data.length; i++)
											{
												myString += "<tr>";
												myString += "<td class='left'>"+data[i]['car_type']+"</td>";
												myString += "<td class='right'>$"+data[i]['est_fare']+"</td>";
												myString += "<td class='right'>"+data[i]['amt_percentage']+"</td>";
												myString += "<td class='right'><b>$"+data[i]['driver_fare']+"</b></td>";
												myString += "</tr>";
											}
										myString += "</table>";
										$("#calculated_fare_2").html(myString);
									}
							});
				}
			else
				{
					$( ".loading" ).hide();
					$('.estimate_fare').prop("disabled", false); // enable the submit button
					$('#dialog_submit').html(error);
					$("#dialog_submit").dialog({
						title: 'SUBMISSION ERROR',
						resizable: true,
						draggable: true,
						modal: true,
						position: 'center',
						width: 300,
						height: 200,
						buttons: {
									Ok: function() 
										{
											$( this ).dialog( "close" );
									}
							},
						show: { effect:'fade', duration:500 },
						hide: { effect:'fade', duration:400},			
						close: function (event, ui) {
							$( this ).dialog( "close" );
						},
						open: function (event, ui) {}
					})
				}
		});	


});