$(document).ajaxStart(function(){ 
	$('#ajaxBusy').show(); 
	}).ajaxStop(function(){ 
	$('#ajaxBusy').hide();
});

$(document).ready(function()
	{
		//TO ADD A GROUP
		
		$(document).on('click', '#make_new_group', function(event) {
			
			var new_group_name = $("#new_group_name").val();
			$('.make_new_group').prop("disabled", true); // To avoid any accidental resumbit
			$.ajax
			({
				type: "POST",
				url: "../Ajax/driver_groups.php",
				data: "new_group_name="+new_group_name,
				cache: false,
				success: function(data)
					{
						
						$('.make_new_group').prop("disabled", false);
						location.reload(true);
						$('#message').html(data.message);	
						$("#message").css('color','green');
						window.setTimeout
							(
								function()
									{
										location.reload(true)
									},
								4000
							);
					} 
			});
		});
		
		// TO DELETE A GROUP
		
		$(document).on('click', '#delete_groups', function(event) {
			
			var gr_id = [];
			$('.group_ids:checked').each(function() 
				{
					gr_id.push($(this).val());
				});

			$('.delete_groups').prop("disabled", true); // To avoid any accidental resumbit
			$.ajax
			({
				type: "POST",
				url: "../Ajax/driver_groups.php",
				data: "group_id="+gr_id,
				cache: false,
				success: function(data)
					{
						$('#message').html(data.message);	
						$("#message").css('color','green');
						$('.delete_groups').prop("disabled", false);
						window.setTimeout
							(
								function()
									{
										location.reload(true)
									},
								4000
							);
					} 
			});
		});
		
		// TO SHOW GROUP MEMBERS
		
		$(document).on('click', '.group_names', function(event) {
			
			var group_id = $(this).attr("id");
			//alert(group_id);
			//$('.group_names').empty();
			$.ajax
			({
				type: "POST",
				url: "../Ajax/driver_groups.php",
				data: "this_group_id="+group_id,
				cache: false,
				success: function(data)
					{
						$('#show_group_member_details').html(data.table);
					} 
			});
		});
		
		// TO DELETE A DRIVER FROM A GROUP
		
		$(document).on('click', '.in_group', function(event) {
			
			var user_id 	= 	$(this).attr("id");
			var group_id 	= 	$("#clicked_gr_id").attr('value');
			$.ajax
			({
				type: "POST",
				url: "../Ajax/driver_groups.php",
				data: "delete_from_group_id="+group_id+"&delete_user_id="+user_id,
				cache: false,
				success: function(data)
					{
						$('#show_message').html(data.message);
						$("#show_message").css('color','red');
					} 
			});
		});
		
		// TO ADD A DRIVER TO A GROUP
		
		$(document).on('click', '.out_group', function(event) {
			
			var user_id 	= 	$(this).attr("id");
			var group_id 	= 	$("#clicked_gr_id").attr('value');
			
			$.ajax
			({
				type: "POST",
				url: "../Ajax/driver_groups.php",
				data: "add_to_group_id="+group_id+"&add_user_id="+user_id,
				cache: false,
				success: function(data)
					{
						$('#show_message').html(data.message);
						$("#show_message").css('color','red');
					} 
			});
		});

	});