$(document).ajaxStart(function(){ 
	$('#ajaxBusy').show(); 
	}).ajaxStop(function(){ 
	$('#ajaxBusy').hide();
});

$(document).ready(function()
	{
		$('#j1_time').timeEntry({show24Hours: true, noSeparatorEntry: true});
		$('#j2_time').timeEntry({show24Hours: true, noSeparatorEntry: true});
		
		
		var dateToday = new Date();
		$( "#j1_date" ).datepicker({
		numberOfMonths: 4,
		changeMonth: true,
		changeYear: true,
		dateFormat: 'D, d M, yy',
		altField: "#j1_date_cap",
		altFormat: "yy-mm-dd"
		});
		$("#j2_date" ).datepicker({
		numberOfMonths: 4,
		changeMonth: true,
		changeYear: true,
		dateFormat: 'D, d M, yy',
		altField: "#j2_date_cap",
		altFormat: "yy-mm-dd"
		});
		$('body').append('<div id="ajaxBusy"><p><img src="../Images/loading.gif"></p></div>');
 
		$('#ajaxBusy').css({
			display:"none",
			margin:"0px",
			paddingLeft:"0px",
			paddingRight:"0px",
			paddingTop:"0px",
			paddingBottom:"0px",
			position: "fixed",
			top: "50%",
			left: "50%",
			width:"auto"
		});
		$("#row_std_id").hide();
		$("#row_std_name").hide();
		$("#guardian_details").hide();
		$("#agent_details").hide();
		$("#destination_contact_details").hide();
		$("#homestay_details").hide();
		$("#row_new_charge_account").hide();
		$("#row_new_contact").hide();
		$("#row_new_passenger").hide();
		$("#edit_acc_contact_details_span").hide();
		$("#j1_driver_row").hide();
		$("#j1_driver_price_span").hide();
		//$("#j1_driver_notes_span").hide();
		$("#j1_driver_row_multiple").hide();
		$("#j1_driver_group").hide();
		$("#j1_driver_ex_notes").hide();
		$("#j1_driver_ex_price").hide();
		$("#j2_driver_row").hide();
		$("#j2_driver_row_multiple").hide();
		$("#j2_driver_group").hide();
		$("#j2_driver_price_span").hide();
		//$("#j2_driver_notes_span").hide();
		$("#j2_driver_ex_notes").hide();
		$("#j2_driver_ex_price").hide();
		$(".for_student").hide();
		$(".return_feilds").hide();
		$("#j1_via_table").hide();
		$("#j1_to_via_table").hide();
		$("#j2_via_table").hide();
		$("#j2_to_via_table").hide();
		
		$('#guardian_info').on('click', function(event) {
			$("#guardian_details").toggle('fast');
		}); 
		
		$('.under_18').click(function() 
			{ 
				var x = $("input[name=under_18]:checked").val();
				if(x == '1')  //YES
					{
						$( "#guardian_details" ).show();
					}
				else
					{
						$( "#guardian_details" ).hide();
					}
			});
		
		$('.has_agent').click(function() 
			{ 
				var x = $("input[name=has_agent]:checked").val();
				if(x == '1')  //YES
					{
						$( "#agent_details" ).show();
					}
				else
					{
						$( "#agent_details" ).hide();
					}
			});
		$('.has_destination_contact').click(function() 
			{ 
				var x = $("input[name=has_destination_contact]:checked").val();
				if(x == '1')  //YES
					{
						$( "#destination_contact_details" ).show();
					}
				else
					{
						$( "#destination_contact_details" ).hide();
					}
			});
		$('.is_homestay').click(function() 
			{ 
				var x = $("input[name=is_homestay]:checked").val();
				if(x == '1')  //YES
					{
						$( "#homestay_details" ).show();
					}
				else
					{
						$( "#homestay_details" ).hide();
					}
			});
		
		$('#j1_via').on('click', function(event) {
			$("#j1_via_table").toggle('fast');
		}); 
		$('#j1_to_via').on('click', function(event) {
			$("#j1_to_via_table").toggle('fast');
		}); 
		$('#j2_via').on('click', function(event) {
			$("#j2_via_table").toggle('fast');
		}); 
		$('#j2_to_via').on('click', function(event) {
			$("#j2_to_via_table").toggle('fast');
		}); 
		$('#j1_sub').autocomplete({
				source:'../Ajax/autocomplete_suburbs.php',
				minLength:2,
				select: function(event, ui){
						$("#j1_sub").val(ui.item.suburb);
						$("#j1_pc").val(ui.item.postcode);
					}
			});
		$('#j1_to_sub').autocomplete({
				source:'../Ajax/autocomplete_suburbs.php',
				minLength:2,
				select: function(event, ui){
						$("#j1_to_sub").val(ui.item.suburb);
						$("#j1_to_pc").val(ui.item.postcode);
					}
			});
		$('#j1_via_sub').autocomplete({
				source:'../Ajax/autocomplete_suburbs.php',
				minLength:2,
				select: function(event, ui){
						$("#j1_via_sub").val(ui.item.suburb);
						$("#j1_via_pc").val(ui.item.postcode);
					}
			});
		$('#j1_to_via_sub').autocomplete({
				source:'../Ajax/autocomplete_suburbs.php',
				minLength:2,
				select: function(event, ui){
						$("#j1_to_via_sub").val(ui.item.suburb);
						$("#j1_to_via_pc").val(ui.item.postcode);
					}
			});
		$('#j2_sub').autocomplete({
				source:'../Ajax/autocomplete_suburbs.php',
				minLength:2,
				select: function(event, ui){
						$("#j2_sub").val(ui.item.suburb);
						$("#j2_pc").val(ui.item.postcode);
					}
			});
		$('#j2_to_sub').autocomplete({
				source:'../Ajax/autocomplete_suburbs.php',
				minLength:2,
				select: function(event, ui){
						$("#j2_to_sub").val(ui.item.suburb);
						$("#j2_to_pc").val(ui.item.postcode);
					}
			});	
		$('#j2_via_sub').autocomplete({
				source:'../Ajax/autocomplete_suburbs.php',
				minLength:2,
				select: function(event, ui){
						$("#j2_via_sub").val(ui.item.suburb);
						$("#j2_via_pc").val(ui.item.postcode);
					}
			});
		$('#j2_to_via_sub').autocomplete({
				source:'../Ajax/autocomplete_suburbs.php',
				minLength:2,
				select: function(event, ui){
						$("#j2_to_via_sub").val(ui.item.suburb);
						$("#j2_to_via_pc").val(ui.item.postcode);
					}
			});
		
		$('#acc_type').change(function() {
				var id=$(this).val();
				if(id=='3') // when booking type not selected
					{
						$( ".for_student" ).show();
					}
				else
					{
						$( ".for_student" ).hide();
					}
			});
		$('#job_type').change(function() {
				$(".return_feilds").toggle('fast');
			});
		$(document).on('click', '#show_charge_acc', function(event) {
		//$('#show_charge_acc').click(function() {
				$('#row_new_charge_account').toggle('fast');
				$("#charge_acc").val('').trigger("chosen:updated");
				$("#acc_contact").val('').trigger("chosen:updated");
				$("#acc_passenger").val('').trigger("chosen:updated");
			});
		$('#bkg_made_by').click(function() {
				$('#row_new_contact').toggle('fast');
				$("#acc_contact").val('').trigger("chosen:updated");
				$("#edit_acc_contact_id").val('');
				$('#edit_acc_contact_details_span').hide();
				$('#show_acc_contact_preference').hide();
				$('#edit_acc_contact_button_span').hide();
			});
		$('#add_new_pax').click(function() {
				$('#row_new_passenger').toggle('fast');
				$("#acc_passenger").val('').trigger("chosen:updated");
			});
			
		$(document).on('click', '#charge_acc_history', function(event) {
				alert("Function not available yet");
			});
		$(document).on('click', '#bkg_made_by_history', function(event) {
				alert("Function not available yet");
			});
		$(document).on('click', '#passenger_history', function(event) {
				alert("Function not available yet");
			});
		//The following code will automatically choose account type as corporate on page load and get the list of charge accounts under corporate booking for
		var initial_id = 1;
		$.getJSON("../Ajax/get_options.php?acc_type="+initial_id, function(data) {
			$("#span_charge_acc").html(data.data);
			
			var config = {
			  '.chosen-select'           : {},
			  '.chosen-select-deselect'  : {allow_single_deselect:true},
			  '.chosen-select-no-single' : {disable_search_threshold:10},
			  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
			  '.chosen-select-width'     : {width:"95%"}
			}
			for (var selector in config) {
			  $(selector).chosen(config[selector]);
			}
		});
		$(document).on('change', '#acc_type', function(event) {
			var id=$(this).val();
			$.getJSON("../Ajax/get_options.php?acc_type="+id, function(data) {
				$("#span_charge_acc").html(data.data);
				
				var config = {
				  '.chosen-select'           : {},
				  '.chosen-select-deselect'  : {allow_single_deselect:true},
				  '.chosen-select-no-single' : {disable_search_threshold:10},
				  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
				  '.chosen-select-width'     : {width:"95%"}
				}
				for (var selector in config) {
				  $(selector).chosen(config[selector]);
				}
			});
		}); 
		
		$(document).on('change', '#charge_acc', function(event) {
			
			$("#edit_acc_contact_id").val('');
			$('#edit_acc_contact_details_span').hide();
			$('#show_acc_contact_preference').hide();
			$('#edit_acc_contact_button_span').hide();
			
			$("#edit_pax_contact_id").val('');
			$('#edit_pax_contact_details_span').hide();
			$('#show_pax_preference').hide();
			$('#edit_pax_contact_button_span').hide();
			
			var id=$(this).val();
			if($('#acc_type').val() == '3')
			{
				$.ajax
				({
					type: "POST",
					url: "../Ajax/get_specific_value.php",
					data: 'charge_acc='+id,
					cache: false,
					success: function(data)
						{

						} 
				});
			}
			$.getJSON("../Ajax/get_options.php?acc_contacts="+id, function(data) {
				$('#span_acc_contacts').html(data.data1);
				$('#span_acc_passengers').html(data.data2);
				$("#add_under_18_email_input").html(data.data3);
				$("#add_homestay_email_input").html(data.data4);
				$("#span_charge_acc_history").html('<input type="button" class="sml_btn" name="charge_acc_history" id="charge_acc_history" value="History">');
				var config = {
				  '.chosen-select'           : {},
				  '.chosen-select-deselect'  : {allow_single_deselect:true},
				  '.chosen-select-no-single' : {disable_search_threshold:10},
				  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
				  '.chosen-select-width'     : {width:"95%"}
				}
				for (var selector in config) {
				  $(selector).chosen(config[selector]);
				}
			});
		}); 
		
		$(document).on('change', '#acc_contact', function(event) {
			$("#span_bkg_made_by_history").html('<input type="button" class="sml_btn" name="bkg_made_by_history" id="bkg_made_by_history" value="History">');
			$('#row_new_contact').hide();
			var id=$(this).val();
			if(id != '')
				{
					$.getJSON("../Ajax/get_options.php?acc_contact_id="+id, function(data) {
						$('#show_acc_contact_preference').show();
						$('#edit_acc_contact_button_span').show();
						$('#show_acc_contact_preference').html(data.data1);
						$('#edit_acc_contact_button_span').html(
						'<input type="button" class="sml_btn" name="edit_acc_contact_button" id="edit_acc_contact_button" value="Edit">'+
						'<input type="hidden" name="edit_acc_contact_id" id="edit_acc_contact_id" value="'+id+'">'); 
					$('#edit_acc_contact_details_span').hide();
					});
				}
			else
				{
					$("#edit_acc_contact_id").val('');
					$('#edit_acc_contact_details_span').hide();
					$('#show_acc_contact_preference').hide();
					$('#edit_acc_contact_button_span').hide();
				}
		}); 
		$(document).on('click', '#edit_acc_contact_button', function(event) {
			
			var acc_cont_id= $("#edit_acc_contact_id").val();
			if(acc_cont_id != '')
			{
				$.ajax
				({
					type: "POST",
					url: "../Ajax/get_options.php",
					/*dataType: 'json',*/
					data: "acc_cont_id="+acc_cont_id,
					cache: false,
					success: function(data)
						{
							$('#edit_acc_contact_details_span').html(data);	
							$('#edit_acc_contact_details_span').toggle('fast');							
						} 
				});
			}
		});
		
		$(document).on('click', '#update_acc', function(event) {
			var acc_cont_id= $("#edit_acc_contact_id").val(),
			bkgtitle =$("#edit_acc_title").val(),
			bkgfname =$("#edit_acc_fname").val(),
			bkglname =$("#edit_acc_lname").val(),
			bkgphone =$("#edit_acc_phone").val(),
			bkgmobile =$("#edit_acc_mobile").val(),
			bkgemail =$("#edit_acc_email").val(),
			bkgpreference =$("#edit_acc_preference").val() ;
			$.ajax
			({
				type: "POST",
				url: "../Ajax/get_options.php",
				/*dataType: 'json',*/
				data: "update_acc=1&acc_id="+acc_cont_id+"&title="+bkgtitle+"&fname="+bkgfname+"&lname="+bkglname+"&phone="+bkgphone+"&mobile="+bkgmobile+"&email="+bkgemail+"&preference="+bkgpreference,
				cache: false,
				success: function(data)
					{
						$('#edit_acc_contact_details_span').html(data);	
						$("#edit_acc_contact_id").val('');
						$('#show_acc_contact_preference').hide();
						$('#edit_acc_contact_button_span').hide();
						////update bkg_made_by and passenger list
						var ch_acc_id=$('#charge_acc').val();
						$.getJSON("../Ajax/get_options.php?acc_contacts_after_edit="+ch_acc_id+"&acc_id="+acc_cont_id, 
						function(data) {
							$('#span_acc_contacts').html(data.data);
							
							$("#span_charge_acc_history").html('<input type="button" class="sml_btn" name="charge_acc_history" id="charge_acc_history" value="History">');
							var config = {
							  '.chosen-select'           : {},
							  '.chosen-select-deselect'  : {allow_single_deselect:true},
							  '.chosen-select-no-single' : {disable_search_threshold:10},
							  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
							  '.chosen-select-width'     : {width:"95%"}
							}
							for (var selector in config) {
							  $(selector).chosen(config[selector]);
							}
						});
						
					} 
			});
		});
		
		$(document).on('change', '#acc_passenger', function(event) {
			$("#span_passenger_history").html('<input type="button" class="sml_btn" name="passenger_history" id="passenger_history" value="History">');
			var id=$(this).val();
			if(id!='')
			{
				$.getJSON("../Ajax/get_options.php?acc_pax_id="+id, function(data) {
					$('#show_pax_preference').html(data.data1);
					$('#edit_pax_contact_button_span').show();
					$('#edit_pax_contact_button_span').html(
					'<input type="button" class="sml_btn" name="edit_pax_contact_button" id="edit_pax_contact_button" value="Edit">'+
					'<input type="hidden" name="edit_pax_contact_id" id="edit_pax_contact_id" value="'+id+'">'); 
					$('#edit_pax_contact_details_span').hide();
				});
			}
			else
			{
				$("#edit_pax_contact_id").val('');
				$('#edit_pax_contact_details_span').hide();
				$('#show_pax_preference').hide();
				$('#edit_pax_contact_button_span').hide();
			}
		});
		$(document).on('click', '#edit_pax_contact_button', function(event) {
			
			var pax_contact_id= $("#edit_pax_contact_id").val();
			if(pax_contact_id != '')
			{
				$.ajax
				({
					type: "POST",
					url: "../Ajax/get_options.php",
					/*dataType: 'json',*/
					data: "pax_cont_id="+pax_contact_id,
					cache: false,
					success: function(data)
						{
							$('#edit_pax_contact_details_span').html(data);	
							$('#edit_pax_contact_details_span').toggle('fast');							
						} 
				});
			}
		});
		
		$(document).on('click', '#update_pax', function(event) {
			var pax_cont_id= $("#edit_pax_id").val(),
			paxtitle =$("#edit_pax_title").val(),
			paxfname =$("#edit_pax_fname").val(),
			paxlname =$("#edit_pax_lname").val(),
			paxphone =$("#edit_pax_phone").val(),
			paxmobile =$("#edit_pax_mobile").val(),
			paxemail =$("#edit_pax_email").val(),
			paxpreference =$("#edit_pax_preference").val() ;
			$.ajax
			({
				type: "POST",
				url: "../Ajax/get_options.php",
				/*dataType: 'json',*/
				data: "update_pax=1&pax_id="+pax_cont_id+"&title="+paxtitle+"&fname="+paxfname+"&lname="+paxlname+"&phone="+paxphone+"&mobile="+paxmobile+"&email="+paxemail+"&preference="+paxpreference,
				cache: false,
				success: function(data)
					{
						$('#edit_pax_contact_details_span').html(data);	
						$("#edit_pax_contact_id").val('');
						$('#show_pax_preference').hide();
						$('#edit_pax_contact_button_span').hide();
						////update passenger list
						var ch_acc_id=$('#charge_acc').val();
						$.getJSON("../Ajax/get_options.php?pax_contacts_after_edit="+ch_acc_id+"&pax_id="+pax_cont_id, 
						function(data) {
							$('#span_acc_passengers').html(data.data);
							var config = {
							  '.chosen-select'           : {},
							  '.chosen-select-deselect'  : {allow_single_deselect:true},
							  '.chosen-select-no-single' : {disable_search_threshold:10},
							  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
							  '.chosen-select-width'     : {width:"95%"}
							}
							for (var selector in config) {
							  $(selector).chosen(config[selector]);
							}
						});
						
					} 
			});
		});
		//Add or remove new booking made by phone numbers and email
		$(function() 
			{
				var addDiv = $('#bkg_by_add_ph_email');
				var i = $('#bkg_by_add_ph_email p').size() + 1;
				$(document).on('click', '#add_ph_email_bkg_made_by', function() 
					{
						$('<p><select name="bkg_by_ph_type[]"><option value="1">Mobile</option><option value="2">Work</option><option value="3">Home</option></select><input type="text" name="bkg_by_ph[]" placeholder="Phone#"/><select name="bkg_by_email_type[]"><option value="1">Work</option><option value="2">Personal</option></select><input type="text" name="bkg_by_email[]" placeholder="Email"/><span id="remove_ph_email_bkg_made_by"><img src="../Images/minus.png" height="16px"></img></span></p>').appendTo(addDiv);
						i++;
						return false;
					});
				 $(document).on('click', '#remove_ph_email_bkg_made_by', function() 
					{ 
						if( i > 2 ) 
							{
								$(this).parents('p').remove();
								i--;
							}
						return false;
					});
			});
		//Add or remove new Passenger phone numbers and email
		$(function() 
			{
				var addDiv = $('#passenger_add_ph_email');
				var i = $('#passenger_add_ph_email p').size() + 1;
				$(document).on('click', '#add_ph_email_passenger', function() 
					{
						$('<p><select name="new_pax_ph_type[]"><option value="1">Mobile</option><option value="2">Work</option><option value="3">Home</option></select><input type="text" name="new_pax_ph[]" placeholder="Phone#"/><select name="new_pax_email_type[]"><option value="1">Work</option><option value="2">Personal</option></select><input type="text" name="new_pax_email[]" placeholder="Email"/><span id="remove_ph_email_passenger"><img src="../Images/minus.png" height="16px"></img></span></p>').appendTo(addDiv);
						i++;
						return false;
					});
				 $(document).on('click', '#remove_ph_email_passenger', function() 
					{ 
						if( i > 2 ) 
							{
								$(this).parents('p').remove();
								i--;
							}
						return false;
					});
			});


		$('#pax_name_toggle').click(function() {
				$('#new_pax_password').val($('#new_pax_password').val());
				var x  = $('#bkg_by_title').val();
				$("select#new_pax_title option").each(function() { this.selected = (this.text == x); });
				$('#new_pax_fname').val($('#bkg_by_fname').val());
				$('#new_pax_lname').val($('#bkg_by_lname').val());

				//works only for one of the fields
				$("input[name='bkg_by_ph[]']").map(function() 
					{
						$("input[name='new_pax_ph[]']").val(this.value);
					}).get();
				$("input[name='bkg_by_email[]']").map(function() 
					{
						$("input[name='new_pax_email[]']").val(this.value);
					}).get();
			});
		$('#address_toggle').click(function() {
				$('#j2_to_line_1').val($('#j1_line_1').val());
				$('#j2_to_line_2').val($('#j1_line_2').val());
				$('#j2_to_sub').val($('#j1_sub').val());
				$('#j2_to_pc').val($('#j1_pc').val());
				
				$('#j2_line_1').val($('#j1_to_line_1').val());
				$('#j2_line_2').val($('#j1_to_line_2').val());
				$('#j2_sub').val($('#j1_to_sub').val());
				$('#j2_pc').val($('#j1_to_pc').val());
				
			});
		$('#j1_driver_status').change(function() {
				var id=$(this).val();
				$("#j1_driver_row").hide();
				
				if(id == '')
					{
						$("#j1_driver_row").hide();
						$("#j1_driver_price_span").hide();
						//$("#j1_driver_notes_span").hide();
						$("#j1_driver_ex_notes").hide();
						$("#j1_driver_ex_price").hide();
						$("#j1_driver_row_multiple").hide();
						$("#j1_driver_group").hide();
					}
				else if(id == '2')
					{
						$("#j1_driver_row_multiple").show();
						$("#j1_driver2").data("placeholder","Select Driver").chosen();
						$("#j1_driver_group").show();
						$("#j1_driver_price_span").show();
						//$("#j1_driver_notes_span").show();
						$("#j1_driver_ex_notes").show();
						$("#j1_driver_ex_price").show();
						 
					}
				else
					{
						$("#j1_driver_row").show();
						$("#j1_driver_price_span").show();
						//$("#j1_driver_notes_span").show();
						$("#j1_driver_ex_notes").show();
						$("#j1_driver_ex_price").show(); 
						$("#j1_driver_row_multiple").hide();
						$("#j1_driver_group").hide();
					}
			});	
		$('#j2_driver_status').change(function() {
				var id=$(this).val();
				$("#j2_driver_row").hide();
				
				if(id == '')
					{
						$("#j2_driver_row").hide();
						$("#j2_driver_price_span").hide();
						//$("#j2_driver_notes_span").hide();
						$("#j2_driver_ex_notes").hide();
						$("#j2_driver_ex_price").hide();
						$("#j2_driver_row_multiple").hide();
						$("#j2_driver_group").hide();
					}
				else if(id == '2')
					{
						$("#j2_driver_row_multiple").show();
						$("#j2_driver2").data("placeholder","Select Driver").chosen();
						$("#j2_driver_group").hide();
						$("#j2_driver_price_span").show();
						//$("#j2_driver_notes_span").show();
						$("#j2_driver_ex_notes").show();
						$("#j2_driver_ex_price").show();
						 
					}
				else
					{
						$("#j2_driver_row").show();
						$("#j2_driver_price_span").show();
						//$("#j2_driver_notes_span").show();
						$("#j2_driver_ex_notes").show();
						$("#j2_driver_ex_price").show();
						$("#j2_driver_row_multiple").hide();
						$("#j2_driver_group").hide();
					}
			});	
			
		$('.under_18').click(function() {
			if($(this).val() == '1')
			{
				var j1_dr_notes = $("#j1_driver_notes").val();
				var j2_dr_notes = $("#j2_driver_notes").val();
				
				if(j1_dr_notes != '')
				{
					$("#j1_driver_notes").val(j1_dr_notes+' , Under 18 ');
				}
				
				else
				{
					$("#j1_driver_notes").val(j1_dr_notes+' Under 18 ');
				}
				
				if(j2_dr_notes != '')
				{
					$("#j2_driver_notes").val(j2_dr_notes+' , Under 18 ');
				}
				
				else
				{
					$("#j2_driver_notes").val(j2_dr_notes+' Under 18 ');
				}
				
			}
			
		});
		
		$('#chk_1').change(function(e){if($('#chk_1').is(':checked')){var a = parseFloat($('#chk_1_value').val());$('#j1_fare').val(a.toFixed(2));} if($('#chk_1').is(':unchecked')){var d= 0.00;$('#j1_fare').val(d.toFixed(2));}});
		$('#chk_2').change(function(e){if($('#chk_2').is(':checked')){var a = parseFloat($('#chk_2_value').val());$('#j1_inter').val(a.toFixed(2));} if($('#chk_2').is(':unchecked')){var d= 0.00;$('#j1_inter').val(d.toFixed(2));}});
		$('#chk_3').change(function(e){if($('#chk_3').is(':checked')){var a = parseFloat($('#chk_3_value').val());$('#j1_ed').val(a.toFixed(2));} if($('#chk_3').is(':unchecked')){var d= 0.00;$('#j1_ed').val(d.toFixed(2));}});
		$('#chk_4').change(function(e){if($('#chk_4').is(':checked')){var a = parseFloat($('#chk_4_value').val());$('#j1_wait').val(a.toFixed(2));} if($('#chk_4').is(':unchecked')){var d= 0.00;$('#j1_wait').val(d.toFixed(2));}});
		$('#chk_5').change(function(e){if($('#chk_5').is(':checked')){var a = parseFloat($('#chk_5_value').val());$('#j1_park').val(a.toFixed(2));} if($('#chk_5').is(':unchecked')){var d= 0.00;$('#j1_park').val(d.toFixed(2));}});
		$('#chk_6').change(function(e){if($('#chk_6').is(':checked')){var a = parseFloat($('#chk_6_value').val());$('#j1_tolls').val(a.toFixed(2));} if($('#chk_6').is(':unchecked')){var d= 0.00;$('#j1_tolls').val(d.toFixed(2));}});
		$('#chk_7').change(function(e){if($('#chk_7').is(':checked')){var a = parseFloat($('#chk_7_value').val());$('#j1_bs').val(a.toFixed(2));} if($('#chk_7').is(':unchecked')){var d= 0.00;$('#j1_bs').val(d.toFixed(2));}});
		$('#chk_8').change(function(e){if($('#chk_8').is(':checked')){var a = parseFloat($('#chk_8_value').val());$('#j1_ah').val(a.toFixed(2));} if($('#chk_8').is(':unchecked')){var d= 0.00;$('#j1_ah').val(d.toFixed(2));}});
		$('#chk_9').change(function(e){if($('#chk_9').is(':checked')){var a = parseFloat($('#chk_9_value').val());$('#j1_me').val(a.toFixed(2));} if($('#chk_9').is(':unchecked')){var d= 0.00;$('#j1_me').val(d.toFixed(2));}});
		$('#chk_10').change(function(e){if($('#chk_10').is(':checked')){var a = parseFloat($('#chk_10_value').val());$('#j1_alc').val(a.toFixed(2));} if($('#chk_10').is(':unchecked')){var d= 0.00;$('#j1_alc').val(d.toFixed(2));}});
		$('#chk_11').change(function(e){if($('#chk_11').is(':checked')){var a = parseFloat($('#chk_11_value').val());$('#j1_fc').val(a.toFixed(2));} if($('#chk_11').is(':unchecked')){var d= 0.00;$('#j1_fc').val(d.toFixed(2));}});
		$('#chk_12').change(function(e){if($('#chk_12').is(':checked')){var a = parseFloat($('#chk_12_value').val());$('#j1_oth').val(a.toFixed(2));} if($('#chk_12').is(':unchecked')){var d= 0.00;$('#j1_oth').val(d.toFixed(2));}});
		//change the value of the driver price
		$('#j1_driver_price').change(function(e){
			var j1_drv_fee = parseFloat($('#j1_driver_price').val());
			$('#j1_driver_price').val(j1_drv_fee.toFixed(2));
			$('#j1_drv_fee').val(j1_drv_fee.toFixed(2));
		});
		$('#j2_driver_price').change(function(e){
			var j2_drv_fee = parseFloat($('#j2_driver_price').val());
			$('#j2_driver_price').val(j2_drv_fee.toFixed(2));
			$('#j2_drv_fee').val(j2_drv_fee.toFixed(2));
		});
		
		$('input').change(function(e)  {
			var j1_tot_fare = 0;
			var j2_tot_fare = 0;

			var j1_fare = parseFloat($('#j1_fare').val());
			var j1_inter = parseFloat($('#j1_inter').val());
			var j1_ed = parseFloat($('#j1_ed').val());
			var j1_wait = parseFloat($('#j1_wait').val());
			var j1_tolls = parseFloat($('#j1_tolls').val());
			var j1_bs = parseFloat($('#j1_bs').val());
			var j1_park = parseFloat($('#j1_park').val());
			var j1_ah = parseFloat($('#j1_ah').val());
			var j1_me = parseFloat($('#j1_me').val());
			var j1_alc = parseFloat($('#j1_alc').val());
			var j1_fc = parseFloat($('#j1_fc').val());
			var j1_oth = parseFloat($('#j1_oth').val());
			var j1_tot_fare = parseFloat($('#j1_tot_fare').val());
			var j1_drv_fee = parseFloat($('#j1_drv_fee').val());
			var j1_oth_exp = parseFloat($('#j1_oth_exp').val());
			var j1_profit = parseFloat($('#j1_profit').val());
			
			var j2_fare = parseFloat($('#j2_fare').val());
			var j2_inter = parseFloat($('#j2_inter').val());
			var j2_ed = parseFloat($('#j2_ed').val());
			var j2_wait = parseFloat($('#j2_wait').val());
			var j2_tolls = parseFloat($('#j2_tolls').val());
			var j2_bs = parseFloat($('#j2_bs').val());
			var j2_park = parseFloat($('#j2_park').val());
			var j2_ah = parseFloat($('#j2_ah').val());
			var j2_me = parseFloat($('#j2_me').val());
			var j2_alc = parseFloat($('#j2_alc').val());
			var j2_fc = parseFloat($('#j2_fc').val());
			var j2_oth = parseFloat($('#j2_oth').val());
			var j2_tot_fare = parseFloat($('#j2_tot_fare').val());
			var j2_drv_fee = parseFloat($('#j2_drv_fee').val());
			var j2_oth_exp = parseFloat($('#j2_oth_exp').val());
			var j2_profit = parseFloat($('#j2_profit').val());
			
			j1_tot_fare = j1_fare + j1_inter + j1_ed + j1_wait + j1_tolls + j1_bs + j1_park + j1_ah + j1_me + j1_alc + j1_fc + j1_oth ;
			j2_tot_fare = j2_fare + j2_inter + j2_ed + j2_wait + j2_tolls + j2_bs + j2_park + j2_ah + j2_me + j2_alc + j2_fc +j2_oth ;
			
			j1_tot_exp = j1_drv_fee + j1_oth_exp;
			j1_profit = j1_tot_fare - j1_tot_exp;
			
			j2_tot_exp = j2_drv_fee + j2_oth_exp;
			j2_profit = j2_tot_fare - j2_tot_exp;
			
			$('#j1_tot_fare').val(j1_tot_fare.toFixed(2));
			$('#j1_profit').val(j1_profit.toFixed(2));
			
			$('#j2_tot_fare').val(j2_tot_fare.toFixed(2));
			$('#j2_profit').val(j2_profit.toFixed(2));
			
			$('#j1_fare').val(j1_fare.toFixed(2));
			$('#j1_inter').val(j1_inter.toFixed(2));
			$('#j1_ed').val(j1_ed.toFixed(2));
			$('#j1_wait').val(j1_wait.toFixed(2));
			$('#j1_tolls').val(j1_tolls.toFixed(2));
			$('#j1_bs').val(j1_bs.toFixed(2));
			$('#j1_park').val(j1_park.toFixed(2));
			$('#j1_ah').val(j1_ah.toFixed(2));
			$('#j1_me').val(j1_me.toFixed(2));
			$('#j1_alc').val(j1_alc.toFixed(2));
			$('#j1_fc').val(j1_fc.toFixed(2));
			$('#j1_oth').val(j1_oth.toFixed(2));
			$('#j1_drv_fee').val(j1_drv_fee.toFixed(2));
			$('#j1_oth_exp').val(j1_oth_exp.toFixed(2));
			
			$('#j2_fare').val(j2_fare.toFixed(2));
			$('#j2_inter').val(j2_inter.toFixed(2));
			$('#j2_ed').val(j2_ed.toFixed(2));
			$('#j2_wait').val(j2_wait.toFixed(2));
			$('#j2_tolls').val(j2_tolls.toFixed(2));
			$('#j2_bs').val(j2_bs.toFixed(2));
			$('#j2_park').val(j2_park.toFixed(2));
			$('#j2_ah').val(j2_ah.toFixed(2));
			$('#j2_me').val(j2_me.toFixed(2));
			$('#j2_alc').val(j2_alc.toFixed(2));
			$('#j2_fc').val(j2_fc.toFixed(2));
			$('#j2_oth').val(j2_oth.toFixed(2));
			$('#j2_drv_fee').val(j2_drv_fee.toFixed(2));
			$('#j2_oth_exp').val(j2_oth_exp.toFixed(2));
			
		});

		

		$("#get_guidance_kms").click(function(){
			
			var job_type		= 	$("#job_type").val();
			var pk_state		= 	$("#pk_state").val();
			var j1_sub			= 	$("#j1_sub").val();
			var j1_pc			= 	$("#j1_pc").val();
			var j1_to_sub		= 	$("#j1_to_sub").val();
			var j1_to_pc		= 	$("#j1_to_pc").val();
			var ret_state		= 	$("#ret_state").val();
			var j2_sub			= 	$("#j2_sub").val();
			var j2_pc			= 	$("#j2_pc").val();
			var j2_to_sub		= 	$("#j2_to_sub").val();
			var j2_to_pc		= 	$("#j2_to_pc").val();
			var from 		= ""+j1_sub+" "+pk_state+" "+j1_pc+" Australia";
			var to 			= ""+j1_to_sub+" "+pk_state+" "+j1_to_pc+" Australia";
			var ret_from 	= ""+j2_sub+" "+ret_state+" "+j2_pc+" Australia";
			var ret_to 		= ""+j2_to_sub+" "+ret_state+" "+j2_to_pc+" Australia";
			$.ajax
				({
					type: "POST",
					url: "../Ajax/get_options.php",
					dataType: 'json',
					data: {job_type: job_type, from: from, to: to, ret_from: ret_from, ret_to: ret_to},
					cache: false,
					success: function(data)
						{
							//Then add the values
							$("#j1_kms").val(data.data1);
							$("#j2_kms").val(data.data2);
						} 
				});
		});
		
		$('#poi_1').on('click', function(event) {
			$.getJSON("../Ajax/get_options.php?all_pois", function(data) {
				$('#dialog_1').html(data.data);
				$("#dialog_1").dialog({
					title: 'Points of Interest',
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 530,
					height: 300,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui) 
						{
							$("#dialog_1").click(function() {
									var id = $('input:radio[name=add_sel_add_1]:checked').val();
									$.getJSON("../Ajax/get_options.php?a_pois="+id, function(data) {
									$('#j1_line_1').val(data.data1);
									$('#j1_line_2').val(data.data2);
									$('#j1_sub').val(data.data3);
									$('#j1_pc').val(data.data4);
									$('#dialog_1').dialog('close');
									return false;
								});
							});
						}
				});
			});
		}); 
		
		$('#poi_2').on('click', function(event) {
			$.getJSON("../Ajax/get_options.php?all_pois", function(data) {
				$('#dialog_2').html(data.data);
				$("#dialog_2").dialog({
					title: 'Points of Interest',
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 530,
					height: 300,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui) 
						{
							$("#dialog_2").click(function() {
									var id = $('input:radio[name=add_sel_add_1]:checked').val();
									$.getJSON("../Ajax/get_options.php?a_pois="+id, function(data) {
									$('#j1_to_line_1').val(data.data1);
									$('#j1_to_line_2').val(data.data2);
									$('#j1_to_sub').val(data.data3);
									$('#j1_to_pc').val(data.data4);
									$('#dialog_2').dialog('close');
									return false;
								});
							});
						}
				});
			});
		}); 
		
		$('#poi_3').on('click', function(event) {
			$.getJSON("../Ajax/get_options.php?all_pois", function(data) {
				$('#dialog_3').html(data.data);
				$("#dialog_3").dialog({
					title: 'Points of Interest',
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 530,
					height: 300,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui) 
						{
							$("#dialog_3").click(function() {
									var id = $('input:radio[name=add_sel_add_1]:checked').val();
									$.getJSON("../Ajax/get_options.php?a_pois="+id, function(data) {
										$('#j2_line_1').val(data.data1);
										$('#j2_line_2').val(data.data2);
										$('#j2_sub').val(data.data3);
										$('#j2_pc').val(data.data4);
										$('#dialog_3').dialog('close');
										return false;
								});
							});
						}
				});
			});
		}); 
		
		$('#poi_4').on('click', function(event) {
			$.getJSON("../Ajax/get_options.php?all_pois", function(data) {
				$('#dialog_4').html(data.data);
				$("#dialog_4").dialog({
					title: 'Points of Interest',
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 530,
					height: 300,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui) 
						{
							$("#dialog_4").click(function() {
									var id = $('input:radio[name=add_sel_add_1]:checked').val();
									$.getJSON("../Ajax/get_options.php?a_pois="+id, function(data) {
									$('#j2_to_line_1').val(data.data1);
									$('#j2_to_line_2').val(data.data2);
									$('#j2_to_sub').val(data.data3);
									$('#j2_to_pc').val(data.data4);
									$('#dialog_4').dialog('close');
									return false;
								});
							});
						}
				});
			});
		});
		
		$('#poi_5').on('click', function(event) {
			$.getJSON("../Ajax/get_options.php?all_pois", function(data) {
				$('#dialog_5').html(data.data);
				$("#dialog_5").dialog({
					title: 'Points of Interest',
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 530,
					height: 300,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui) 
						{
							$("#dialog_5").click(function() {
									var id = $('input:radio[name=add_sel_add_1]:checked').val();
									$.getJSON("../Ajax/get_options.php?a_pois="+id, function(data) {
									$('#j1_via_line_1').val(data.data1);
									$('#j1_via_line_2').val(data.data2);
									$('#j1_via_sub').val(data.data3);
									$('#j1_via_pc').val(data.data4);
									$('#dialog_5').dialog('close');
									return false;
								});
							});
						}
				});
			});
		});	
			$('#poi_6').on('click', function(event) {
			$.getJSON("../Ajax/get_options.php?all_pois", function(data) {
				$('#dialog_6').html(data.data);
				$("#dialog_6").dialog({
					title: 'Points of Interest',
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 530,
					height: 300,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui) 
						{
							$("#dialog_6").click(function() {
									var id = $('input:radio[name=add_sel_add_1]:checked').val();
									$.getJSON("../Ajax/get_options.php?a_pois="+id, function(data) {
									$('#j1_to_via_line_1').val(data.data1);
									$('#j1_to_via_line_2').val(data.data2);
									$('#j1_to_via_sub').val(data.data3);
									$('#j1_to_via_pc').val(data.data4);
									$('#dialog_6').dialog('close');
									return false;
								});
							});
						}
				});
			});
		});	
			$('#poi_7').on('click', function(event) {
			$.getJSON("../Ajax/get_options.php?all_pois", function(data) {
				$('#dialog_7').html(data.data);
				$("#dialog_7").dialog({
					title: 'Points of Interest',
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 530,
					height: 300,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui) 
						{
							$("#dialog_7").click(function() {
									var id = $('input:radio[name=add_sel_add_1]:checked').val();
									$.getJSON("../Ajax/get_options.php?a_pois="+id, function(data) {
									$('#j2_via_line_1').val(data.data1);
									$('#j2_via_line_2').val(data.data2);
									$('#j2_via_sub').val(data.data3);
									$('#j2_via_pc').val(data.data4);
									$('#dialog_7').dialog('close');
									return false;
								});
							});
						}
				});
			});
		});	
			$('#poi_8').on('click', function(event) {
			$.getJSON("../Ajax/get_options.php?all_pois", function(data) {
				$('#dialog_8').html(data.data);
				$("#dialog_8").dialog({
					title: 'Points of Interest',
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 530,
					height: 300,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui) 
						{
							$("#dialog_8").click(function() {
									var id = $('input:radio[name=add_sel_add_1]:checked').val();
									$.getJSON("../Ajax/get_options.php?a_pois="+id, function(data) {
									$('#j2_to_via_line_1').val(data.data1);
									$('#j2_to_via_line_2').val(data.data2);
									$('#j2_to_via_sub').val(data.data3);
									$('#j2_to_via_pc').val(data.data4);
									$('#dialog_8').dialog('close');
									return false;
								});
							});
						}
				});
			});
		}); 
			
		$('#pax_add_1').on('click', function(event) {
			var pax_id = $('#acc_passenger').val();
			$.getJSON("../Ajax/get_options.php?all_pax_add="+pax_id, function(data) {
				$('#dialog1').html(data.data);
				$("#dialog1").dialog({
					title: 'Passenger Addresses',
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 530,
					height: 300,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui) 
						{
							$("#dialog1").click(function() {
								var id = $('input:radio[name=add_sel_add_1]:checked').val();
								$.getJSON("../Ajax/get_options.php?an_add="+id, function(data) {
								$('#j1_line_1').val(data.data1);
								$('#j1_line_2').val(data.data2);
								$('#j1_sub').val(data.data3);
								$('#j1_pc').val(data.data4);
								$('#dialog1').dialog('close');
								return false;
								});
							});
						}
				});
			});
		}); 
		
		$('#pax_add_2').on('click', function(event) {
			var pax_id = $('#acc_passenger').val();
			$.getJSON("../Ajax/get_options.php?all_pax_add="+pax_id, function(data) {
				$('#dialog2').html(data.data);
				$("#dialog2").dialog({
					title: 'Passenger Addresses',
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 530,
					height: 300,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui) 
						{
							$("#dialog2").click(function() {
								var id = $('input:radio[name=add_sel_add_1]:checked').val();
								$.getJSON("../Ajax/get_options.php?an_add="+id, function(data) {
								$('#j1_to_line_1').val(data.data1);
								$('#j1_to_line_2').val(data.data2);
								$('#j1_to_sub').val(data.data3);
								$('#j1_to_pc').val(data.data4);
								$('#dialog2').dialog('close');
								return false;
								});
							});
						}
				});
			});
		}); 
		
		$('#pax_add_3').on('click', function(event) {
			var pax_id = $('#acc_passenger').val();
			$.getJSON("../Ajax/get_options.php?all_pax_add="+pax_id, function(data) {
				$('#dialog3').html(data.data);
				$("#dialog3").dialog({
					title: 'Passenger Addresses',
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 530,
					height: 300,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui) 
						{
							$("#dialog3").click(function() {
								var id = $('input:radio[name=add_sel_add_1]:checked').val();
								$.getJSON("../Ajax/get_options.php?an_add="+id, function(data) {
								$('#j2_line_1').val(data.data1);
								$('#j2_line_2').val(data.data2);
								$('#j2_sub').val(data.data3);
								$('#j2_pc').val(data.data4);
								$('#dialog3').dialog('close');
								return false;
								});
							});
						}
				});
			});
		}); 
		
		$('#pax_add_4').on('click', function(event) {
			var pax_id = $('#acc_passenger').val();
			$.getJSON("../Ajax/get_options.php?all_pax_add="+pax_id, function(data) {
				$('#dialog4').html(data.data);
				$("#dialog4").dialog({
					title: 'Passenger Addresses',
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 530,
					height: 300,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui) 
						{
							$("#dialog4").click(function() {
								var id = $('input:radio[name=add_sel_add_1]:checked').val();
								$.getJSON("../Ajax/get_options.php?an_add="+id, function(data) {
								$('#j2_to_line_1').val(data.data1);
								$('#j2_to_line_2').val(data.data2);
								$('#j2_to_sub').val(data.data3);
								$('#j2_to_pc').val(data.data4);
								$('#dialog4').dialog('close');
								return false;
								});
							});
						}
				});
			});
		}); 
		
		$('#pax_add_5').on('click', function(event) {
			var pax_id = $('#acc_passenger').val();
			$.getJSON("../Ajax/get_options.php?all_pax_add="+pax_id, function(data) {
				$('#dialog5').html(data.data);
				$("#dialog5").dialog({
					title: 'Passenger Addresses',
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 530,
					height: 300,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui) 
						{
							$("#dialog5").click(function() {
								var id = $('input:radio[name=add_sel_add_1]:checked').val();
								$.getJSON("../Ajax/get_options.php?an_add="+id, function(data) {
								$('#j1_via_line_1').val(data.data1);
								$('#j1_via_line_2').val(data.data2);
								$('#j1_via_sub').val(data.data3);
								$('#j1_via_pc').val(data.data4);
								$('#dialog5').dialog('close');
								return false;
								});
							});
						}
				});
			});
		}); 
		
		$('#pax_add_6').on('click', function(event) {
			var pax_id = $('#acc_passenger').val();
			$.getJSON("../Ajax/get_options.php?all_pax_add="+pax_id, function(data) {
				$('#dialog6').html(data.data);
				$("#dialog6").dialog({
					title: 'Passenger Addresses',
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 530,
					height: 300,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui) 
						{
							$("#dialog6").click(function() {
								var id = $('input:radio[name=add_sel_add_1]:checked').val();
								$.getJSON("../Ajax/get_options.php?an_add="+id, function(data) {
								$('#j1_to_via_line_1').val(data.data1);
								$('#j1_to_via_line_2').val(data.data2);
								$('#j1_to_via_sub').val(data.data3);
								$('#j1_to_via_pc').val(data.data4);
								$('#dialog6').dialog('close');
								return false;
								});
							});
						}
				});
			});
		}); 
		
		$('#pax_add_7').on('click', function(event) {
			var pax_id = $('#acc_passenger').val();
			$.getJSON("../Ajax/get_options.php?all_pax_add="+pax_id, function(data) {
				$('#dialog7').html(data.data);
				$("#dialog7").dialog({
					title: 'Passenger Addresses',
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 530,
					height: 300,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui) 
						{
							$("#dialog7").click(function() {
								var id = $('input:radio[name=add_sel_add_1]:checked').val();
								$.getJSON("../Ajax/get_options.php?an_add="+id, function(data) {
								$('#j2_via_line_1').val(data.data1);
								$('#j2_via_line_2').val(data.data2);
								$('#j2_via_sub').val(data.data3);
								$('#j2_via_pc').val(data.data4);
								$('#dialog7').dialog('close');
								return false;
								});
							});
						}
				});
			});
		}); 
		
		$('#pax_add_8').on('click', function(event) {
			var pax_id = $('#acc_passenger').val();
			$.getJSON("../Ajax/get_options.php?all_pax_add="+pax_id, function(data) {
				$('#dialog8').html(data.data);
				$("#dialog8").dialog({
					title: 'Passenger Addresses',
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 530,
					height: 300,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400},			
					close: function (event, ui) {},
					open: function (event, ui) 
						{
							$("#dialog8").click(function() {
								var id = $('input:radio[name=add_sel_add_1]:checked').val();
								$.getJSON("../Ajax/get_options.php?an_add="+id, function(data) {
								$('#j2_to_via_line_1').val(data.data1);
								$('#j2_to_via_line_2').val(data.data2);
								$('#j2_to_via_sub').val(data.data3);
								$('#j2_to_via_pc').val(data.data4);
								$('#dialog8').dialog('close');
								return false;
								});
							});
						}
				});
			});
		}); 
		
		$('.cancel_booking').click(function() {
		window.location.reload(true);
		});

		$('.save_booking').click(function() {
			$('#loading').show();
			$('.save_booking').prop("disabled", true); // To avoid any accidental resubmit
			var person_role_id = $('#person_role_id').val();
			if(person_role_id == '3')
			{
				if($('#acc_type').val() == '3')
				{
					var charge_acc_id = $('#charge_acc').val();
					$.ajax
					({
						type: "POST",
						url: "../Ajax/get_specific_value.php",
						data: 'charge_acc='+charge_acc_id,
						cache: false,
						success: function(data)
							{
								//Then add the values
								$("#j1_driver_notes").val(data);
								$("#j2_driver_notes").val(data);
							} 
					});
				}
			}
			$.ajax
				({
					type: "POST",
					dataType: 'json',
					url: "../Ajax/new_booking_exec.php",
					data: $("#form_new_booking").serialize(),
					cache: false,
					success: function(data)
						{
							var result = data.data1;
							if(result == 'OK')
								{
									$('#dialog_submit').html(data.data2);
									$("#dialog_submit").dialog({
										title: 'Booking Result',
										resizable: true,
										draggable: true,
										modal: true,
										position: 'center',
										width: 580,
										height: 650,
										/*
										buttons: {
													Ok: function() 
														{
															$('.save_booking').prop("disabled", true); // To avoid any accidental resubmit
															$( this ).dialog( "close" );
															window.location.reload(true);
													}
											},
										*/
										buttons: {
										
													Ok: function() 
														{
															//$('.save_booking').prop("disabled", true); // To avoid any accidental resubmit
															$( this ).dialog( "close" );
															//$( this ).window( "close" );
															window.location.reload(true);
															$.getScript("http://dev.bookings-global.com.au/Scripts/jquery.easyui.min.js");
															$.getScript("http://dev.bookings-global.com.au/Scripts/tabs.js");
															var url = 'http://dev.bookings-global.com.au/Forms/show_booking_details.php?job_id='+data.data3+'';
															var title = 'JOB ID - '+data.data3+'';
															var content = '<iframe  scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:1200px;"></iframe>';
															window.parent.$('#tt').tabs('add',{
																title:title,
																content:content,
																closable:true
															});
															return false;
													}
											},
										show: { effect:'fade', duration:500 },
										hide: { effect:'fade', duration:400},			
										close: function (event, ui) {
											$('.save_booking').prop("disabled", true); // To avoid any accidental resubmit
											$( this ).dialog( "close" );
											window.location.reload(true);
										},
										open: function (event, ui) {}
									});
								}
							else if(result == 'ERROR')
								{
									$('.save_booking').prop("disabled", false); // enable the submit button
									$('#dialog_submit').html(data.data2);
									$("#dialog_submit").dialog({
										title: 'SUBMISSION ERROR',
										resizable: true,
										draggable: true,
										modal: true,
										position: 'center',
										width: 500,
										height: 400,
										buttons: {
													Ok: function() 
														{
															$( this ).dialog( "close" );
															if(data.data3 == 'timeout')
															{
																window.location.reload(true);
															}
													}
											},
										show: { effect:'fade', duration:500 },
										hide: { effect:'fade', duration:400},			
										close: function (event, ui) {
											$( this ).dialog( "close" );
										},
										open: function (event, ui) {}
									});
								}
							else
								{
									window.location.reload(true);
								}
						} 
				});
			return false; // avoid to execute the actual submit of the form.
		}); 
		
		$("#std_id").keyup(function(){
			
			var idval = $('#std_id').val(),
				idlength = idval.length;
			
			if(idlength <=4 )
			{
				if(isNaN(idval))
				{
					alert("Please enter only digits. First four digits should be integer");
					$('#std_id').val(idval.substr(0,idlength-1));
					$('#std_id').focus(); 
				}
				if ( idlength ==4 ) {
					idval += " ";
					$('#std_id').val(idval);
				}
			}
			
			
		});
		
		$("#std_id").blur(function(){
			var idval = $('#std_id').val(),
				charge_acc_id = $('#charge_acc').val(),
				person_role_id = $('#person_role_id').val(),
				idflag=0;
				console.log(charge_acc_id);
			if(idval.length >4)
			{
				switch (charge_acc_id)
				{
					case '15': //rmit
						if(idval[0] == '3' && idval[4] == ' ' && idval.length == '8')
							idflag='1';
						else
						{
							$('#dialog_submit').html('Enter correct student id. Format is xxxx xxx');
							$("#dialog_submit").dialog({
								title: 'Warning',
								resizable: true,
								draggable: true,
								modal: true,
								position: 'center',
								width: 400, 
								height: 200,
								buttons: {
											Ok: function() 
												{
													$( this ).dialog( "close" );
												}
									},
								show: { effect:'fade', duration:500 },
								hide: { effect:'fade', duration:400},			
								close: function (event, ui) {
									$( this ).dialog( "close" );
								},
								open: function (event, ui) {}
							});
							$('#std_id').val('');
							$('#std_id').focus(); 
						}
						break;
						
					case '19': //monash
						if(idval[0] == '2' && idval[4] == ' ' && idval.length == '9')
							idflag='1';
						else
						{
							$('#dialog_submit').html('Enter correct student id. Format is xxxx xxxx');
							$("#dialog_submit").dialog({
								title: 'Warning',
								resizable: true,
								draggable: true,
								modal: true,
								position: 'center',
								width: 400, 
								height: 200,
								buttons: {
											Ok: function() 
												{
													$( this ).dialog( "close" );
												}
									},
								show: { effect:'fade', duration:500 },
								hide: { effect:'fade', duration:400},			
								close: function (event, ui) {
									$( this ).dialog( "close" );
								},
								open: function (event, ui) {}
							});
							$('#std_id').val('');
							$('#std_id').focus(); 
						}
						break;

					default: //monash clayton and monash college
						if(idval[4] == ' ' && idval.length == '9')
							idflag='1';
						else
						{
							$('#dialog_submit').html('Enter correct student id. Format is xxxx xxxx');
							$("#dialog_submit").dialog({
								title: 'Warning',
								resizable: true,
								draggable: true,
								modal: true,
								position: 'center',
								width: 400, 
								height: 200,
								buttons: {
											Ok: function() 
												{
													$( this ).dialog( "close" );
												}
									},
								show: { effect:'fade', duration:500 },
								hide: { effect:'fade', duration:400},			
								close: function (event, ui) {
									$( this ).dialog( "close" );
								},
								open: function (event, ui) {}
							});
							$('#std_id').val('');
							$('#std_id').focus(); 
						}
						
				}
			}
			if(idval != '' && idflag == '1')
			{				
						
				$.ajax
					({
						type: "POST",
						url: "../Ajax/duplicate_stdid_check.php",
						data: 'std_id='+idval+'&charge_acc_id='+charge_acc_id,
						cache: false, 
						success: function(data)
						{
							if(data!=0)
							{ 
								if(person_role_id == '1')
								{
									$('#dialog_submit').html('This student ID has already been used, double check before continuing');
									$("#dialog_submit").dialog({
										title: 'Warning',
										resizable: true,
										draggable: true,
										modal: true,
										position: 'center',
										width: 600, 
										height: 200,
										buttons: {
													Ok: function() 
														{
															$( this ).dialog( "close" );
														}
											},
										show: { effect:'fade', duration:500 },
										hide: { effect:'fade', duration:400},			
										close: function (event, ui) {
											$( this ).dialog( "close" );
										},
										open: function (event, ui) {}
									});
								}
								else if( person_role_id =='3' )
								{
									if(!confirm('This student ID has already been used, Do you wish to continue? Please be aware that extra charges might apply.'))
									{
										$('#std_id').val('');
										$('#std_id').focus();
									}
								}
							}
						}
					});
			}
		});
	});