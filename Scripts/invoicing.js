
	function checkAll(field)
		{
			for (i = 0; i < field.length; i++)
			field[i].checked = true ;
		}
	function uncheckAll(field)
		{
			for (i = 0; i < field.length; i++)
				field[i].checked = false ;
		}


$(document).ready(function()
	{
		$(".loading").ajaxStart(function () {
			$(this).show();
		});

		$(".loading").ajaxStop(function () {
				$(this).hide();
		});

		$( "#date_from" ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd-mm-yy',
		altField: "#cap_date_from",
		altFormat: "yy-mm-dd"
		});
		$("#date_to" ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd-mm-yy',
		altField: "#cap_date_to",
		altFormat: "yy-mm-dd"
		});
		
		
		$('#search_by_entered_by').autocomplete({
				source:'../Ajax/autocomplete_drivers.php?entered_by=1',
				minLength:2,
				select: function(event, ui){
						$("#search_by_entered_by").val(ui.item.label);
						$("#search_by_entered_by_id").val(ui.item.val);
						
					}
			});
		
		$('#search_by_pax').autocomplete({
				source:'../Ajax/autocomplete_drivers.php?pax=1',
				minLength:2,
				select: function(event, ui){
						$("#search_by_pax").val(ui.item.label);
						$("#search_by_pax_id").val(ui.item.val);
						
					}
			});
		
		$('#search_by_car').autocomplete({
				source:'../Ajax/autocomplete_drivers.php?car=1',
				minLength:2,
				select: function(event, ui){
						$("#search_by_car").val(ui.item.label);
						$("#search_by_car_id").val(ui.item.val);
						//$("#j1_pc").val(ui.item.postcode);
					}
			});
			
		$('#search_by_charge_account').autocomplete({
				source:'../Ajax/autocomplete_drivers.php?charge_acc=1',
				minLength:2,
				select: function(event, ui){
						$("#search_by_charge_account").val(ui.item.label);
						$("#charge_acc").val(ui.item.val);
						
					}
			});
		
			$('.all_booking_radio_cls').click(function() {
				$('.preset_dates_cls').removeAttr("checked");
				if($(this).val() == '1') 
					{
						$( "#date_from" ).val('');
						$( "#date_to" ).val('');
						$( "#cap_date_from" ).val('');
						$( "#cap_date_to" ).val('');
						$( "#date_from" ).attr('disabled','disabled');
						$( "#date_to" ).attr('disabled','disabled');
					}
				else
					{
						$("#date_from").removeAttr('disabled');
						$("#date_to").removeAttr('disabled');					
					}
			});		
			
		$('.preset_dates_cls').click(function() {
			$('.all_booking_radio_cls').removeAttr("checked");
		});
			
		$(document).on('click', '#show_result', function(event) {
			
			if($("#search_by_charge_account").val() == '')
			{
				$("#charge_acc").val('');
			}
			
			if($("#search_by_entered_by").val() == '')
			{
				$("#search_by_entered_by_id").val('');
			}
			
			if($("#search_by_pax").val() == '')
			{
				$("#search_by_pax_id").val('');
			}
			
			if($("#search_by_car").val() == '')
			{
				$("#search_by_car_id").val('');
			}
			
			var charge_acc_id = $('#charge_acc').val();
			var pax_id = $('#search_by_pax_id').val();
			var entered_by = $('#search_by_entered_by_id').val();
			var car_id = $('#search_by_car_id').val();
			var swt5 = $('#swt5').val();
			var swt10 = $('#swt10').val();
			var swt20 = $('#swt20').val();
			var swt30 = $('#swt30').val();
			var swt80 = $('#swt80').val();
			var swt90 = $('#swt90').val();
			var swt100 = $('#swt100').val();
			
			if($("#cancelled_job").is(':checked'))
			{
				var cancel_job = '1';
			}
			else{
				var cancel_job = '0';
			}
			
			var all_booking_radio_val = $("input[name='all_booking_radio']:checked").val();
			
			//console.log(all_booking_radio_val);
			
			if(all_booking_radio_val == '1')
			{				
				var charge_acc = 'charge_acc='+ charge_acc_id+'&all_booking_radio=1&cancel_job='+cancel_job+'&pax_id='+pax_id+'&entered_by='+entered_by+'&car_id='+car_id+'&swt5='+swt5+'&swt10='+swt10+'&swt20='+swt20+'&swt30='+swt30+'&swt80='+swt80+'&swt90='+swt90+'&swt100='+swt100;
			}
			else if(all_booking_radio_val == '2')
			{
				var c_date_from= $('#cap_date_from').val();
				var c_date_to= $('#cap_date_to').val();
				var charge_acc = 'charge_acc='+ charge_acc_id+'&date_from='+c_date_from+'&date_to='+c_date_to+'&cancel_job='+cancel_job+'&pax_id='+pax_id+'&entered_by='+entered_by+'&car_id='+car_id+'&swt5='+swt5+'&swt10='+swt10+'&swt20='+swt20+'&swt30='+swt30+'&swt80='+swt80+'&swt90='+swt90+'&swt100='+swt100;
			}
			else
			{
				var preset_dates=$("input[name='preset_dates']:checked").val();
				var charge_acc = 'charge_acc='+ charge_acc_id+'&preset_dates='+preset_dates+'&cancel_job='+cancel_job+'&pax_id='+pax_id+'&entered_by='+entered_by+'&car_id='+car_id+'&swt5='+swt5+'&swt10='+swt10+'&swt20='+swt20+'&swt30='+swt30+'&swt80='+swt80+'&swt90='+swt90+'&swt100='+swt100;
			}
			
			$.ajax
				({
					type: "POST",
					url: "../Ajax/invoice_form_options.php",
					//dataType: 'html',
					data: charge_acc+'&show_data=1',
					cache: false,
					success: function(html)
						{
							$('#uninvoiced_jobs').html(html);
							
						} 
				});
		});
		
		$(document).on('change', '#acc_type_for_listing', function(event) {
			var id=$(this).val();
			$.getJSON("../Ajax/get_options.php?option_type=invoice_listing&acc_type="+id, function(data) {
				$("#row_customer_for_listing").html(data.data);
				$('#list_invoices').html('');
				
				var config = {
				  '.chosen-select'           : {},
				  '.chosen-select-deselect'  : {allow_single_deselect:true},
				  '.chosen-select-no-single' : {disable_search_threshold:10},
				  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
				  '.chosen-select-width'     : {width:"95%"}
				}
				for (var selector in config) {
				  $(selector).chosen(config[selector]);
				}
			});
		});
		
		$(document).on('change', '#charge_acc_for_listing', function(event) {
			var id=$(this).val();
			var charge_acc = 'customer_id_for_listing='+ id;
			$.ajax
				({
					type: "POST",
					url: "../Ajax/invoice_form_options.php",
					//dataType: 'html',
					data: charge_acc,
					cache: false,
					success: function(html)
						{
							$('#list_invoices').html(html);
						} 
				});
		});
		
		$( "#paid_date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd-mm-yy'
		});
		
		$(document).on('change', '#acc_type_un', function(event) {
			var id=$(this).val();
			$.getJSON("../Ajax/get_options.php?option_type=invoice_un&acc_type="+id, function(data) {
				$("#row_customer_un").html(data.data);
				$('#uninvoiced_jobs').html('');
				$("#date_range").show();
				$( "#date_from" ).val('');
				$( "#date_to" ).val('');
				$( "#cap_date_from" ).val('');
				$( "#cap_date_to" ).val('');
				$( "#search" ).prop('disabled', false);
				var config = {
				  '.chosen-select'           : {},
				  '.chosen-select-deselect'  : {allow_single_deselect:true},
				  '.chosen-select-no-single' : {disable_search_threshold:10},
				  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
				  '.chosen-select-width'     : {width:"95%"}
				}
				for (var selector in config) {
				  $(selector).chosen(config[selector]);
				}
			});
		});
		
		$(document).on('click', '#preview_send', function(event) {
			var checkedNum = $('input[name="job_id[]"]:checked').length;
			if (!checkedNum) {
				alert('Please select jobs');
				return false;
			}
			else
			{
				checkedNum = $('input[name="send_to[]"]:checked').length;
				if (!checkedNum) {
					alert('Please select receiver');
					return false;
				}
			}
			return true;
		});
		
		$(document).on('click', '#reload', function(event) {
			window.location.reload(true);
			
		});
});