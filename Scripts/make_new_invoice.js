$(document).ready(function()
	{
		$("#charge_account_id").chosen({disable_search_threshold: 10});
		
		$('#ajaxBusy').hide();
		var dateToday = new Date();
		$( "#from_date" ).datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'dd-mm-yy',
			altField: "#hidden_from_date",
			altFormat: "yy-mm-dd"
		});
		$("#to_date" ).datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'dd-mm-yy',
			altField: "#hidden_to_date",
			altFormat: "yy-mm-dd"
		});

		$('#search_jobs').on('click', function(event) {
			
			$('#ajaxBusy').show();
			var charge_account_id 	= 	$("#charge_account_id").val();
			var from_date 			= 	$("#hidden_from_date").val();
			var to_date 			= 	$("#hidden_to_date").val();
			
			$.getJSON("../Ajax/make_new_invoice.php?charge_account_id="+charge_account_id+"&from_date="+from_date+"&to_date="+to_date, function(data) {
				$('#list_uninvoiced_jobs_for_this_account').html(data.data);
				$('#ajaxBusy').hide();
			});
		});
		
		$(document).on('click', '.job_id_link', function(event) {
			
			var job_id = $(this).prop('id');
			
			$.getScript("http://dev.bookings-global.com.au/Scripts/jquery.easyui.min.js");
			$.getScript("http://dev.bookings-global.com.au/Scripts/tabs.js");
			var url = 'http://dev.bookings-global.com.au/Forms/show_booking_details.php?job_id='+job_id+'';
			var title = 'JOB ID - '+job_id+'';
			var content = '<iframe  scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:1200px;"></iframe>';
			window.parent.$('#tt').tabs('add',
				{
					title:title,
					content:content,
					closable:true
				});
			return false;
		});
		
		
		$(document).on('click', '#check_all_jobs', function(event) {
			if(this.checked) 
				{ // check select status
					$('.to_invoice_jobs').each(function() {
						this.checked = true;
					});
				}
			else{
					$('.to_invoice_jobs').each(function() {
						this.checked = false;                 
					});         
				}
		});
		
		$(document).on('click', '#check_all_receivers', function(event) {
			if(this.checked) 
				{ // check select status
					$('.to_receivers').each(function() {
						this.checked = true;
					});
				}
			else{
					$('.to_receivers').each(function() {
						this.checked = false;                 
					});         
				}
		});
		
		$(document).on('click', '#preview_invoice', function(event) {
			$('.loader').append('<span id="ajaxBusy"><img src="../Images/loading_small.gif"></span>');
			
 			var to_inv_jobs = [];
			var to_rec = [];
			
			$('.to_invoice_jobs:checked').each(function() 
				{
					to_inv_jobs.push($(this).val());
				});
				
			$('.to_receivers:checked').each(function() 
				{
					to_rec.push($(this).val());
				});
			if(to_inv_jobs.length <=0)
				{
					$('#ajaxBusy').hide();
					alert('PLEASE SELECT A JOB GROUP TO CONTINUE'); // message when no jobs selected - invoices
				}
			var add_receiver_1_email 			= 	$("#add_receiver_1_email").val();
			var add_receiver_1_name 			= 	$("#add_receiver_1_name").val();
			var add_receiver_2_email 			= 	$("#add_receiver_2_email").val();
			var add_receiver_2_name 			= 	$("#add_receiver_2_name").val();
			var charge_account_id 				= 	$("#charge_account_id").val();
			var charge_account_name 			= 	$("#charge_account_name").val();
			var invoice_status 					= 	$("#invoice_status").val();
			var invoice_date_today				=  	$("#invoice_date_today").val();
			var manual_invoice_date				=  	$("#manual_invoice_date").val();
			var hidden_manual_invoice_date		=  	$("#hidden_manual_invoice_date").val();
			var invoice_payable 				= 	$("#invoice_payable").val();
			var manual_invoice_due_date			=  	$("#manual_invoice_due_date").val();
			var hidden_manual_invoice_due_date	=  	$("#hidden_manual_invoice_due_date").val();
			var card_surcharge 					= 	$("#card_surcharge").val();
			var embed_message 					= 	$("#embed_message").val();
			var email_message 					= 	$("#email_message").val();
			var po_number 						= 	$("#po_number").val();
			
			$.getJSON("http://dev.bookings-global.com.au/Ajax/make_new_invoice.php?to_rec="+to_rec+"&to_inv_jobs="+to_inv_jobs+"&charge_account_id="+charge_account_id+"&charge_account_name="+charge_account_name+"&invoice_status="+invoice_status+"&invoice_date_today="+invoice_date_today+"&manual_invoice_date="+manual_invoice_date+"&hidden_manual_invoice_date="+hidden_manual_invoice_date+"&invoice_payable="+invoice_payable+"&manual_invoice_due_date="+manual_invoice_due_date+"&hidden_manual_invoice_due_date="+hidden_manual_invoice_due_date+"&card_surcharge="+card_surcharge+"&add_receiver_1_email="+add_receiver_1_email+"&add_receiver_1_name="+add_receiver_1_name+"&add_receiver_2_email="+add_receiver_2_email+"&add_receiver_2_name="+add_receiver_2_name+"&embed_message="+embed_message+"&po_number="+po_number,
			 function(data) {
				var charge_account_id		=	data.charge_account_id;
				var invoice_status			=	data.invoice_status;
				var invoice_date			=  	data.invoice_date;
				var invoice_due_date		=  	data.invoice_due_date;
				var total_base_fare			=  	data.total_base_fare;
				var total_extras			=  	data.total_extras;
				var total_before_surcharge	=  	data.total_before_surcharge;
				var card_surcharge_percent 	=	data.card_surcharge_percent;
				var card_surcharge			=  	data.card_surcharge;
				var total_after_surcharge	=  	data.total_after_surcharge;
				var gst						=  	data.gst;
				var notes					=  	data.notes;
				var jobs					=  	data.jobs;
				var receivers				=  	data.receivers;
				var add_receiver_1_name		=  	data.add_receiver_1_name;
				var add_receiver_1_email	=  	data.add_receiver_1_email;
				var add_receiver_2_name		=  	data.add_receiver_2_name;
				var add_receiver_2_email	=  	data.add_receiver_2_email;
				var embed_message			=  	data.embed_message;
				var email_message			=  	data.email_message;
				var po_number				=  	data.po_number;
				
				
				$('#dialog_100').html(data.data);
				$('<input type="button" id="send_now" name="send_now" class="approve_button" value="SEND NOW"/><span id="ajaxBusy1"><img src="http://dev.bookings-global.com.au/Images/loading_small.gif"></span>').appendTo('#dialog_100');
				$('<input type="button" id="save_only" name="save_only"  class="approve_button" value="SAVE ONLY"/><span id="ajaxBusy2"><img src="http://dev.bookings-global.com.au/Images/loading_small.gif"></span>').appendTo('#dialog_100');
				
				$('#ajaxBusy1').hide();
				$('#ajaxBusy2').hide();	
				
				$("#dialog_100").dialog({
					title: 'Invoice Preview',
					resizable: true,
					draggable: true,
					modal: true,
					position: 'center',
					width: 900,
					height: 1200,
					show: { effect:'fade', duration:500 },
					hide: { effect:'fade', duration:400 },			
					close: function (event, ui) {},
					open: function (event, ui) 
						{
							// $(this).html(data.data); // hiden
							// $('<input type="button" id="send_now" name="send_now" class="approve_button" value="SEND NOW"/><span id="ajaxBusy1"><img src="http://dev.bookings-global.com.au/Images/loading_small.gif"></span>').appendTo('#dialog_100'); // hiden
							// $('<input type="button" id="save_only" name="save_only"  class="approve_button" value="SAVE ONLY"/><span id="ajaxBusy2"><img src="http://dev.bookings-global.com.au/Images/loading_small.gif"></span>').appendTo('#dialog_100'); //hiden
				
							// $('#ajaxBusy1').hide(); //hiden
							// $('#ajaxBusy2').hide();  //hiden	
							
							$("#send_now").click(function() {
									$('#ajaxBusy1').show();
									$("#send_now").prop("disabled", true);
									$("#save_only").prop("disabled", true);
									$.getJSON("../Ajax/make_new_invoice.php?send_now=1&charge_account_id="+charge_account_id+"&invoice_status="+invoice_status+"&invoice_date="+invoice_date+"&invoice_due_date="+invoice_due_date+"&total_base_fare="+total_base_fare+"&total_extras="+total_extras+"&total_before_surcharge="+total_before_surcharge+"&card_surcharge_percent="+card_surcharge_percent+"&card_surcharge="+card_surcharge+"&total_after_surcharge="+total_after_surcharge+"&gst="+gst+"&jobs="+jobs+"&receivers="+receivers+"&add_receiver_1_name="+add_receiver_1_name+"&add_receiver_1_email="+add_receiver_1_email+"&add_receiver_2_name="+add_receiver_2_name+"&add_receiver_2_email="+add_receiver_2_email+"&embed_message="+embed_message+"&email_message="+email_message+"&po_number="+po_number, function(data) {
									$('#ajaxBusy1').hide();
									$("#dialog_100").html(data.data);
								});
							});
							
							$("#save_only").click(function() {
									$('#ajaxBusy2').show();
									$("#send_now").prop("disabled", true);
									$("#save_only").prop("disabled", true);
									$.getJSON("../Ajax/make_new_invoice.php?save_only=1&charge_account_id="+charge_account_id+"&invoice_status="+invoice_status+"&invoice_date="+invoice_date+"&invoice_due_date="+invoice_due_date+"&total_base_fare="+total_base_fare+"&total_extras="+total_extras+"&total_before_surcharge="+total_before_surcharge+"&card_surcharge_percent="+card_surcharge_percent+"&card_surcharge="+card_surcharge+"&total_after_surcharge="+total_after_surcharge+"&gst="+gst+"&jobs="+jobs+"&receivers="+receivers+"&add_receiver_1_name="+add_receiver_1_name+"&add_receiver_1_email="+add_receiver_1_email+"&add_receiver_2_name="+add_receiver_2_name+"&add_receiver_2_email="+add_receiver_2_email+"&embed_message="+embed_message+"&email_message="+email_message+"&po_number="+po_number, function(data) {
									$('#ajaxBusy2').hide();
									$("#dialog_100").html(data.data);
								});
							});
						}
				});
			});
		});
	});
	