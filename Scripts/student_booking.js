$(document).ajaxStart(function(){ 
	$('#ajaxBusy').show(); 
	}).ajaxStop(function(){ 
	$('#ajaxBusy').hide();
});
$(document).ready(function()
	{
		$( "#j1_date" ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'D, d M, yy',
		altField: "#j1_date_cap",
		altFormat: "yy-mm-dd"
		});

		/*$('#j1_from').change(function() {
				var id=$(this).val();
				if(id=='3') // when booking type not selected
					{
						$( ".for_student" ).show();
					}
				else
					{
						$( ".for_student" ).hide();
					}
			});*/
		
		$('#make_booking').click(function() {
			
			$('#make_booking').prop("disabled", true); // To avoid any accidental resubmit
			$.ajax
				({
					type: "POST",
					dataType: 'json',
					url: "../Ajax/student_booking_exec.php",
					data: $("#form_student_booking").serialize(),
					cache: false,
					success: function(data)
						{
							var result = data.data1;
							if(result == 'OK')
								{
									$('#dialog_submit').html(data.data2);
									$("#dialog_submit").dialog({
										title: 'Booking Result',
										resizable: true,
										draggable: true,
										modal: true,
										position: 'center',
										width: 500,
										height: 400,
										buttons: {
													Ok: function() 
														{
															$('#make_booking').prop("disabled", true); // To avoid any accidental resubmit
															$( this ).dialog( "close" );
															window.location.reload(true);
													}
											},
										show: { effect:'fade', duration:500 },
										hide: { effect:'fade', duration:400},			
										close: function (event, ui) {
											$('#make_booking').prop("disabled", true); // To avoid any accidental resubmit
											$( this ).dialog( "close" );
											window.location.reload(true);
										},
										open: function (event, ui) {}
									});
								}
							else if(result == 'ERROR')
								{
									$('#make_booking').prop("disabled", false); // enable the submit button
									$('#dialog_submit').html(data.data2);
									$("#dialog_submit").dialog({
										title: 'SUBMISSION ERROR',
										resizable: true,
										draggable: true,
										modal: true,
										position: 'center',
										width: 500,
										height: 400,
										buttons: {
													Ok: function() 
														{
															$( this ).dialog( "close" );
													}
											},
										show: { effect:'fade', duration:500 },
										hide: { effect:'fade', duration:400},			
										close: function (event, ui) {
											$( this ).dialog( "close" );
										},
										open: function (event, ui) {}
									});
								}
							else
								{
									window.location.reload(true);
								}
						} 
				});
			return false; // avoid to execute the actual submit of the form.
		}); 

	});