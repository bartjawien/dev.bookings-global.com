<?php 
session_start();
require_once('../init.php');
include(''.INCLUDE_PATH.'config.php');
include(''.INCLUDE_PATH.'settings.php');
include(''.INCLUDE_PATH.'functions.php');
include(''.CLASSES_PATH.'database.php');
echo'
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<link href="../Css/minified/jquery-ui-1.10.3.custom.min.css" rel="stylesheet"  />
	<link href="../Css/jtable/themes/metro/blue/jtable.css" rel="stylesheet"  type="text/css" />
	<link href="../Css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
	<link href="../Css/style.css" rel="stylesheet"  />
	<link href="../Css/menu.css" rel="stylesheet"  />
	<link href="../Css/chosen.min.css" rel="stylesheet"  />
	<link href="../Css/autocomplete.css" rel="stylesheet"  />
	<link href="../Css/jquery.timeentry.css" rel="stylesheet"  />
	<link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/icon.css">
	
	<script src="../Scripts/jquery-1.10.2.min.js"></script>
	<script src="../Scripts/jquery-ui.min.js"></script>
	<script src="../Scripts/jquery.timeentry.min.js"></script>
	<script src="../Scripts/jquery.jtable.min.js"></script>
	<script src="../Scripts/jquery.validate.js"></script>
	<script src="../Scripts/jquery.validationEngine.js"></script>
	<script src="../Scripts/jquery.validationEngine-en.js"></script>
	<script src="../Scripts/chosen.jquery.min.js"></script>
	<script src="../Scripts/form_new_booking.js"></script>
	
	<style> 
       .redCell
			{
			} 
		.jtable-child-table-container
			{
				margin-left:5%;
			}
    </style>

</head>
<body>'; ?>
<div id="dialog1" style="display:none;"></div>
<div class="filtering" style="text-align:left;">
	<form>
		<input type="text" name="search_string" id="search_string" />
		<button type="submit" id="LoadSearchButton">Search For</button>
	</form>
</div>
<div id="driversTable" style="width: 100%;"></div>
	<script type="text/javascript">
		$(document).ready(function () {
		$('#driversTable').jtable({
			title: 'Drivers List',
			paging: true, //Enable paging
			//pageSize: 2,
			sorting: true, //Enable sorting
			defaultSorting: 'fname ASC',
			selecting: true, //Enable selecting
			multiselect: true, //Allow multiple selecting
			selectingCheckboxes: true, //Show checkboxes on first column
			
			toolbar: {
				items: [
					{
						icon: '../../Images/delete.png',
						text: 'Delete Selected Rows',
						click: function () {
							var selectedRows = $('#driversTable').jtable('selectedRows');
							//alert(selectedRows);
							$('#driversTable').jtable('deleteRows', selectedRows);
						}
					},
					{	
						icon: '../../Images/excel.png',
						text: 'Export to Excel',
						click: function () {
							window.location = '../../Ajax/list_drivers.php?action=export_excel';
						}
					},
					{	
						icon: '../../Images/htmlicon.png',
						text: 'Export to HTML',
						click: function () {
							window.location = '../../Ajax/list_drivers.php?action=export_html';
						}
					},
					{	
						icon: '../../Images/pdficon.png',
						text: 'Export to PDF',
						click: function () {
							window.location = '../../Ajax/list_drivers.php?action=export_pdf';
						}
					}
				]
			},
			deleteConfirmation: function(data) { 
				data.deleteConfirmMessage = 'Are you sure you want to delete this Driver<br/><b>' + data.record.fname +'?</b><br/><br/>This action is irreversible..';
			},
			//openChildAsAccordion: true, //Enable this line to show child tabes as accordion style
			actions: {
						listAction: '../../Ajax/list_drivers.php?action=list_drivers',
						createAction: '../../Ajax/list_drivers.php?action=create_driver', 
						updateAction: '../../Ajax/list_drivers.php?action=update_driver',
						deleteAction: '../../Ajax/list_drivers.php?action=delete_driver'
					},
			fields: {
						id: {
							key: true,
							create: false,
							edit: false,
							list: false
						},
					
						Address: {
									title: '',
									width: '1%',
									sorting: false,
									edit: false,
									create: false,
									display: function (userData) 
										{
											//Create an image that will be used to open child table
											var $img = $('<img src="../../Images/address.png" title="View Addresses" />');
											//Open child table when user clicks the image
											$img.click(function () {
												$('#driversTable').jtable('openChildTable',
												$img.closest('tr'),
													{
														title:  'Address for ' +userData.record.title +' '+ userData.record.fname +' '+ userData.record.lname,
														actions: {
																	listAction: '../../Ajax/list_drivers.php?action=list_addresses&id='+userData.record.id,
																	deleteAction: '../../Ajax/list_drivers.php?action=delete_addresses&id='+userData.record.id,
																	updateAction: '../../Ajax/list_drivers.php?action=update_addresses&id='+userData.record.id,
																	createAction: '../../Ajax/list_drivers.php?action=create_addresses&id='+userData.record.id
														},
														fields: {
																	user_id: {
																		type: 'hidden',
																		defaultValue: userData.record.id
																	},
																	id: {
																			title: 'ID',
																			key: true,
																			list: false
																	},
																	type_id: {
																		title: 'Type',
																		width: '10%',
																		create: true,
																		edit: true,
																		options: 
																				{ 
																					<?php 
																						$query_address_type	= "Select * from variable__address_type order by order_id ASC";
																						$database = new database;
																						$address_type_result = $database->query($query_address_type);
																						while($address_type = mysql_fetch_array($address_type_result))
																							{
																								$address_type_text[] = "'".$address_type['id']."':'".$address_type['details']."'";
																							}
																						if (!empty($address_type_text))
																							{ 
																								$address_type_string = implode(",",$address_type_text);
																							} 
																						echo $address_type_string;
																					?>
																				}
																				
																	},
																	line1: {
																			title: 'Line 1'
																	},
																	line2: {
																			title: 'Line 2'
																	},
																	sub: {
																			title: 'Suburb'
																	},
																	postcode: {
																			title: 'Postcode'
																	},
																	state: {
																			title: 'State',
																			options: {
																					<?php 
																							$query_states 	= "Select * from variable__states order by order_id ASC";
																							$database = new database;
																							$result_states = $database->query($query_states);
																							while($states = mysql_fetch_array($result_states))
																								{
																									$text_states[] = "'".$states['id']."':'".$states['details']."'";
																								}
																							if (!empty($text_states))
																								{ 
																									$string_states = implode(",",$text_states);
																								} 
																							echo $string_states;
																						?>
																					}
																	},
																	created_on: {
																			title: 'Record Date',
																			create: false,
																			edit: false,
																			list: false
																	}
																},
														//Initialize validation logic when a form is created
														formCreated: function (event, data) 
															{
																data.form.css('width','400px');
																data.form.find('input[name=line1]').css('width','250px').addClass('validate[required,minSize[1]]');
																data.form.find('input[name=line2]').css('width','250px');
																data.form.find('input[name=sub]').css('width','250px').addClass('validate[required, minSize[2]]');
																data.form.find('input[name=postcode]').css('width','250px');
																data.form.find('input[name="state"]').css('width','250px');
																data.form.validationEngine();
															},
														//Validate form when it is being submitted
														formSubmitting: function (event, data) 
															{
																return data.form.validationEngine('validate');
															},
														//Dispose validation logic when form is closed
														formClosed: function (event, data) 
															{
																data.form.validationEngine('hide');
																data.form.validationEngine('detach');
															}
													}, 
												function (data) 
													{ //opened handler
														data.childTable.jtable('load');
													});
												});
										//Return image to show on the person row
										return $img;
									}
								},
						Car: {
									title: '',
									width: '1%',
									sorting: false,
									edit: false,
									create: false,
									display: function (userData) 
										{
											//Create an image that will be used to open child table
											var $img = $('<img src="../../Images/car.png" title="View car" />');
											$img.click(function () {

											$('#driversTable').jtable('openChildTable',
											$img.closest('tr'),
													{
														title:  'Cars of ' +userData.record.title +' '+ userData.record.fname +' '+ userData.record.lname,
														actions: {
															listAction: '../../Ajax/list_drivers.php?action=list_cars&id='+userData.record.id,
															deleteAction: '../../Ajax/list_drivers.php?action=delete_cars&id='+userData.record.id,
															updateAction: '../../Ajax/list_drivers.php?action=update_cars&id='+userData.record.id,
															createAction: '../../Ajax/list_drivers.php?action=create_cars&id='+userData.record.id
														},
														fields: {
																	user_id: {
																		type: 'hidden',
																		defaultValue: userData.record.id
																	},
																	id: {
																			title: 'ID',
																			key: true,
																			list: false
																	},
																	plate_no: {
																			title: 'Plate No'
																	},
																	car_type: {
																		title: 'Car Type',
																		create: true,
																		edit: true,
																		options: 
																				{ 
																					<?php 
																						$query_car_type	= "Select * from variable__car_type order by order_id ASC";
																						$database = new database;
																						$car_type_res = $database->query($query_car_type);
																						while($car_type = mysql_fetch_array($car_type_res))
																							{
																								$car_type_text[] = "'".$car_type['id']."':'".$car_type['details']."'";
																							}
																						if (!empty($car_type_text))
																							{ 
																								$car_type_string = implode(",",$car_type_text);
																							} 
																						echo $car_type_string;
																					?>
																				}
																				
																	},
																	licence_type: {
																		title: 'License Type',
																		create: true,
																		edit: true,
																		options: 
																				{ 
																					<?php 
																						$query_car_licence_type	= "Select * from variable__car_licence_type order by order_id ASC";
																						$database = new database;
																						$car_licence_type_res = $database->query($query_car_licence_type);
																						while($car_licence_type = mysql_fetch_array($car_licence_type_res))
																							{
																								$car_licence_type_text[] = "'".$car_licence_type['id']."':'".$car_licence_type['details']."'";
																							}
																						if (!empty($car_licence_type_text))
																							{ 
																								$car_licence_type_string = implode(",",$car_licence_type_text);
																							} 
																						echo $car_licence_type_string;
																					?>
																				}
																				
																	},
																	shape_type: {
																		title: 'Shape Type',
																		create: true,
																		edit: true,
																		options: 
																				{ 
																					<?php 
																						$query_car_shape	= "Select * from variable__car_shape order by order_id ASC";
																						$database = new database;
																						$car_shape_res = $database->query($query_car_shape);
																						while($car_shape = mysql_fetch_array($car_shape_res))
																							{
																								$car_shape_text[] = "'".$car_shape['id']."':'".$car_shape['details']."'";
																							}
																						if (!empty($car_shape_text))
																							{ 
																								$car_shape_string = implode(",",$car_shape_text);
																							} 
																						echo $car_shape_string;
																					?>
																				}
																				
																	},
																	fuel_type: {
																		title: 'Fuel Type',
																		create: true,
																		edit: true,
																		options: 
																				{ 
																					<?php 
																						$query_car_fuel_type	= "Select * from variable__car_fuel_type order by order_id ASC";
																						$database = new database;
																						$car_fuel_type_res = $database->query($query_car_fuel_type);
																						while($car_fuel_type = mysql_fetch_array($car_fuel_type_res))
																							{
																								$car_fuel_type_text[] = "'".$car_fuel_type['id']."':'".$car_fuel_type['details']."'";
																							}
																						if (!empty($car_fuel_type_text))
																							{ 
																								$car_fuel_type_string = implode(",",$car_fuel_type_text);
																							} 
																						echo $car_fuel_type_string;
																					?>
																				}
																				
																	},
																	color_type: {
																		title: 'Color Type',
																		create: true,
																		edit: true,
																		options: 
																				{ 
																					<?php 
																						$query_car_color	= "Select * from variable__car_color order by order_id ASC";
																						$database = new database;
																						$car_color_res = $database->query($query_car_color);
																						while($car_color = mysql_fetch_array($car_color_res))
																							{
																								$car_color_text[] = "'".$car_color['id']."':'".$car_color['details']."'";
																							}
																						if (!empty($car_color_text))
																							{ 
																								$car_color_string = implode(",",$car_color_text);
																							} 
																						echo $car_color_string;
																					?>
																				}
																				
																	},
																	make_type: {
																		title: 'Make Type',
																		create: true,
																		edit: true,
																		options: 
																				{ 
																					<?php 
																						$query_car_make	= "Select * from variable__car_make order by order_id ASC";
																						$database = new database;
																						$car_make_res = $database->query($query_car_make);
																						while($car_make = mysql_fetch_array($car_make_res))
																							{
																								$car_make_text[] = "'".$car_make['id']."':'".$car_make['details']."'";
																							}
																						if (!empty($car_make_text))
																							{ 
																								$car_make_string = implode(",",$car_make_text);
																							} 
																						echo $car_make_string;
																					?>
																				}
																				
																	},
																	model_type: {
																		title: 'Model Type',
																		create: true,
																		edit: true,
																		options: 
																				{ 
																					<?php 
																						$query_car_model	= "Select * from variable__car_model order by order_id ASC";
																						$database = new database;
																						$car_model_res = $database->query($query_car_model);
																						while($car_model = mysql_fetch_array($car_model_res))
																							{
																								$car_model_text[] = "'".$car_model['id']."':'".$car_model['details']."'";
																							}
																						if (!empty($car_model_text))
																							{ 
																								$car_model_string = implode(",",$car_model_text);
																							} 
																						echo $car_model_string;
																					?>
																				}
																				
																	}
																},
														//Initialize validation logic when a form is created
														formCreated: function (event, data) 
															{
																data.form.css('width','400px');
																data.form.find('input[name=plate_no]').css('width','250px').addClass('validate[required,minSize[2]]');
																data.form.validationEngine();
															},
														//Validate form when it is being submitted
														formSubmitting: function (event, data) 
															{
																return data.form.validationEngine('validate');
															},
														//Dispose validation logic when form is closed
														formClosed: function (event, data) 
															{
																data.form.validationEngine('hide');
																data.form.validationEngine('detach');
															}
													}, 
												function (data) 
													{ //opened handler
														data.childTable.jtable('load');
												});
										
											});
										//Return image to show on the person row
										return $img;
								}
						},
						Account: {
									title: '',
									width: '1%',
									sorting: false,
									edit: false,
									create: false,
									display: function (userData) 
										{
											//Create an image that will be used to open child table
											var $img = $('<img src="../../Images/money_bag.png" title="View Accounts" />');
											$img.click(function () {
										//console.log(userData);
											$('#driversTable').jtable('openChildTable',
											$img.closest('tr'),
													{
														title:  'ACCOUNT of  ' +userData.record.title +' '+ userData.record.fname +' '+ userData.record.lname,
														actions: {
															listAction: '../../Ajax/list_drivers.php?action=list_accounts&id='+userData.record.id
														},
														fields: 
															{
																user_id: {
																	type: 'hidden',
																	defaultValue: userData.record.id
																},
																id: {
																		title: 'ID',
																		width: '5%',
																		key: true,
																		list: false
																},
																trx_id: {
																	title: 'Trx ID',
																	width: '5%'
																},
																trx_date: {
																	title: 'Trx Date',
																	width: '10%'
																},
																entered_by: {
																	title: 'Entered By',
																	width: '10%'
																},
																description: {
																	title: 'Description',
																	width: '30%'
																},
																acc_dr: {
																		title: 'Acc Debit',
																		width: '13%'
																},
																acc_cr: {
																		title: 'Acc Credit',
																		width: '13%'
																},
																amt_dr: {
																	title: 'Amt Debit',
																	width: '7%'
																},
																amt_cr: {
																	title: 'Amt Credit',
																	width: '7%'
																}
															}
														
													}, 
												function (data) 
													{ //opened handler
														data.childTable.jtable('load');
												});
										
											});
										//Return image to show on the person row
										return $img;
								}
						},
						Bank_account: {
									title: '',
									width: '1%',
									sorting: false,
									edit: false,
									create: false,
									display: function (userData) 
										{
											//Create an image that will be used to open child table
											var $img = $('<img src="../../Images/bank_accounts.png" title="View bank accounts" />');
											$img.click(function () {

											$('#driversTable').jtable('openChildTable',
											$img.closest('tr'),
													{
														title:  'Bank Accounts of ' +userData.record.title +' '+ userData.record.fname +' '+ userData.record.lname,
														actions: {
															listAction: '../../Ajax/list_drivers.php?action=list_bank_accounts&id='+userData.record.id,
															deleteAction: '../../Ajax/list_drivers.php?action=delete_bank_accounts&id='+userData.record.id,
															updateAction: '../../Ajax/list_drivers.php?action=update_bank_accounts&id='+userData.record.id,
															createAction: '../../Ajax/list_drivers.php?action=create_bank_accounts&id='+userData.record.id
														},
														fields: {
																	user_id: {
																		type: 'hidden',
																		defaultValue: userData.record.id
																	},
																	id: {
																			title: 'ID',
																			key: true,
																			list: false
																	},
																	bank_name: {
																		title: 'Bank Name',
																		create: true,
																		edit: true,
																																					
																	},
																	account_name: {
																		title: 'Account Name',
																		create: true,
																		edit: true,
																				
																	},
																	bsb_number: {
																		title: 'BSB',
																		create: true,
																		edit: true,
																																						
																	},
																	account_number: {
																		title: 'Account Number',
																		create: true,
																		edit: true,
																				
																	}
																},
														//Initialize validation logic when a form is created
														formCreated: function (event, data) 
															{
																data.form.css('width','400px');
																data.form.find('input[name=bank_name]').css('width','250px').addClass('validate[required,minSize[2]]');
																data.form.find('input[name=account_name]').css('width','250px');
																data.form.find('input[name=bsb_number]').css('width','250px');
																data.form.find('input[name=account_number]').css('width','250px');
																data.form.validationEngine();
															},
														//Validate form when it is being submitted
														formSubmitting: function (event, data) 
															{
																return data.form.validationEngine('validate');
															},
														//Dispose validation logic when form is closed
														formClosed: function (event, data) 
															{
																data.form.validationEngine('hide');
																data.form.validationEngine('detach');
															}
													}, 
												function (data) 
													{ //opened handler
														data.childTable.jtable('load');
												});
										
											});
										//Return image to show on the person row
										return $img;
								}
						},
						title: {
							width: '3%',
							title: 'Title',
							options: {'':'','Mr':'Mr','Ms':'Ms','Mrs':'Mrs'}
						},
						fname: {
							title: 'Fname'
						},
						lname: {
							title: 'Lname'
						},
						password: {
							title: 'Password'
						},
						email: {
							title: 'Email'
						},
						mobile: {
							title: 'Mobile'
						},
						phone: {
							title: 'Phone'
						},
						bus_name: {
							title: 'Business Name',
							create: true,
							edit: true,
							list: false
						},
						abn: {
							width: '5%',
							title: 'ABN',
							create: true,
							edit: true,
							list: false
						},
						dc: {
							width: '5%',
							title: 'DC',
							create: true,
							edit: true,
							list: false
						},
						dc_exp: {
							title: 'DC Exp.',
							type: 'date',
							create: true,
							edit: true,
							list: false
						},
						lic: {
							title: 'Lic. No',
							create: true,
							edit: true,
							list: false
						},
						lic_exp: {
							title: 'Lic. Expiry',
							type: 'date',
							create: true,
							edit: true,
							list: false
						},
						notes: {
							width: '10%',
							title: 'Notes'
						},
						acc_bal: {
							width: '5%',
							title: 'Acc. Bal.',
							create: false,
							edit: false
						},
						hidden: {
							title: 'Visibility',
							options: {'0':'Current','1':'LEFT'}
						} ,
						mass_email: 
						{
							title: 'Mass Email',
							width: '40%',
							display: function(data) 
								{
									var thisRegex = new RegExp('[A-Z] 1');
									var textFname = data.record.fname;
									if(!thisRegex.test(textFname)){
										return 'NA';
									}
									else 
									{
										return '<button type="button" class="my_button" id="'+data.record.fname+'" >Send Email</button> ';
									}
									
								}
						}
					},
					recordsLoaded: function(event, data) 
									{
										$('.my_button').click(function() 
											{
												var fname = $(this).attr('id');
										
												$.ajax
													({
														type: "POST",
														url: "../Ajax/mass_email.php",
														data: "show_form=1&fname="+fname,
														cache: false,
														success: function(data)
															{
																//alert(data);
																$('#dialog1').html(data);
																$("#dialog1").dialog({
																	title: 'Mass Email',
																	resizable: true,
																	draggable: true,
																	modal: true,
																	position: 'center',
																	width: 530,
																	height: 300,
																	show: { effect:'fade', duration:500 },
																	hide: { effect:'fade', duration:400},			
																	close: function (event, ui) {},
																	open: function (event, ui) 
																		{
																		
																			$("#send_mass_email").click(function() {
																				$.ajax
																				({
																					type: "POST",
																					url: "../Ajax/mass_email.php",
																					data: $("#mass_email_form").serialize(),
																					cache: false,
																					success: function(data)
																					{
																						$('#dialog1').html(data);	
																						//$('#dialog1').dialog('close');
																						return false;
																					}

																				});
																		
																				
																			});
																		}
																});
																
															}
													});
									
										
											});
									},
				//Initialize validation logic when a form is created
				formCreated: function (event, data) 
					{
						data.form.css('width','400px');
						data.form.find('input[name=fname]').css('width','250px').addClass('validate[required,minSize[2]]');
						data.form.find('input[name=lname]').css('width','250px');
						data.form.find('input[name=password]').css('width','250px');
						data.form.find('input[name=email]').css('width','250px');
						data.form.find('input[name=mobile]').css('width','250px');
						data.form.find('input[name=phone]').css('width','250px');
						data.form.find('input[name=bus_name]').css('width','250px');
						data.form.find('input[name=abn]').css('width','250px');
						data.form.find('input[name=dc]').css('width','250px');
						data.form.find('input[name=dc_exp]').css('width','250px');
						data.form.find('input[name=lic]').css('width','250px');
						data.form.find('input[name=lic_exp]').css('width','250px');
						data.form.find('input[name=lic]').css('width','250px');
						data.form.find('input[name=notes]').css('width','250px');
						data.form.validationEngine();
					},
				//Validate form when it is being submitted
				formSubmitting: function (event, data) 
					{
						return data.form.validationEngine('validate');
					},
				//Dispose validation logic when form is closed
				formClosed: function (event, data) 
					{
						data.form.validationEngine('hide');
						data.form.validationEngine('detach');
					}

		});
		 //Re-load records when user click 'load records' button.
        $('#LoadSearchButton').click(function (e) 
			{
				e.preventDefault();
				$('#driversTable').jtable('load', 
					{
						search_string: $('#search_string').val()
					});
			});
		//Load student list from server
		$('#driversTable').jtable('load');
	});
</script>
</body>
<html>