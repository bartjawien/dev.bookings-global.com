<?php 
session_start();
require_once('../init.php');
include(''.INCLUDE_PATH.'config.php');
include(''.INCLUDE_PATH.'settings.php');
include(''.INCLUDE_PATH.'functions.php');
include(''.CLASSES_PATH.'database.php');
echo'
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<link href="../Css/minified/jquery-ui-1.10.3.custom.min.css" rel="stylesheet"  />
	<link href="../Css/jtable/themes/metro/blue/jtable.css" rel="stylesheet"  type="text/css" />
	<link href="../Css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
	<link href="../Css/style.css" rel="stylesheet"  />
	<link href="../Css/menu.css" rel="stylesheet"  />
	<link href="../Css/chosen.min.css" rel="stylesheet"  />
	<link href="../Css/autocomplete.css" rel="stylesheet"  />
	<link href="../Css/jquery.timeentry.css" rel="stylesheet"  />
	<link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/icon.css">
	
	<script src="../Scripts/jquery-1.10.2.min.js"></script>
	<script src="../Scripts/jquery-ui.min.js"></script>
	<script src="../Scripts/jquery.timeentry.min.js"></script>
	<script src="../Scripts/jquery.jtable.min.js"></script>
	<script src="../Scripts/jquery.validate.js"></script>
	<script src="../Scripts/jquery.validationEngine.js"></script>
	<script src="../Scripts/jquery.validationEngine-en.js"></script>
	<script src="../Scripts/chosen.jquery.min.js"></script>
	<script src="../Scripts/form_new_booking.js"></script>
	<style> 
       .redCell
			{
			} 
		.jtable-child-table-container
			{
				margin-left:5%;
			}
    </style>

</head>
<body>'; ?>
<div class="filtering" style="text-align:left;">
	<form>
		<input type="text" name="search_string" id="search_string" />
		<button type="submit" id="LoadSearchButton">Search For</button>
	</form>
</div>
<div id="carTypesTable" style="width: 100%;"></div>
	<script type="text/javascript">
		$(document).ready(function () {
		$('#carTypesTable').jtable({
			title: 'Car Types',
			paging: true, //Enable paging
			//pageSize: 2,
			sorting: true, //Enable sorting
			defaultSorting: 'id ASC',
			selecting: true, //Enable selecting
			multiselect: true, //Allow multiple selecting
			selectingCheckboxes: true, //Show checkboxes on first column
			
			toolbar: {
				items: [
					{
						icon: '../../Images/delete.png',
						text: 'Delete Selected Rows',
						click: function () {
							var $selectedRows = $('#carTypesTable').jtable('selectedRows');
							$('#carTypesTable').jtable('deleteRows', $selectedRows);
						}
					},
					{	
						icon: '../../Images/excel.png',
						text: 'Export to Excel',
						click: function () {
							window.location = '../../Ajax/list_car_types.php?action=export_excel';
						}
					}
				]
			},
			deleteConfirmation: function(data) {
				data.deleteConfirmMessage = 'Are you sure to delete Car Type<br/><b>' + data.record.details +'?</b><br/><br/>This action is irreversible..';
			},
			//openChildAsAccordion: true, //Enable this line to show child tabes as accordion style
			actions: {
						listAction: '../../Ajax/list_car_types.php?action=list_car_types',
						createAction: '../../Ajax/list_car_types.php?action=create_car_types',
						updateAction: '../../Ajax/list_car_types.php?action=update_car_types',
						deleteAction: '../../Ajax/list_car_types.php?action=delete_car_types'
					},
			fields: {
						id: {
							key: true,
							create: false,
							edit: false,
							list: false
						},
						order_id: {
							title: 'Order ID'
						},
						details: {
							title: 'Car Type'
						}
					},
				//Initialize validation logic when a form is created
				formCreated: function (event, data) 
					{
						data.form.css('width','400px');
						data.form.find('input[name=order_id]').css('width','250px').addClass('validate[required,minSize[1]]');
						data.form.find('input[name=details]').css('width','250px').addClass('validate[required,minSize[2]]');
						data.form.validationEngine();
					},
				//Validate form when it is being submitted
				formSubmitting: function (event, data) 
					{
						return data.form.validationEngine('validate');
					},
				//Dispose validation logic when form is closed
				formClosed: function (event, data) 
					{
						data.form.validationEngine('hide');
						data.form.validationEngine('detach');
					}

		});
		 //Re-load records when user click 'load records' button.
        $('#LoadSearchButton').click(function (e) 
			{
				e.preventDefault();
				$('#carTypesTable').jtable('load', 
					{
						search_string: $('#search_string').val()
					});
			});
		//Load student list from server
		$('#carTypesTable').jtable('load');
	});
</script>
</body>
<html>