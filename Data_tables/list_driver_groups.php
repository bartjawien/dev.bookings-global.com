<?php 
session_start();
require_once('../init.php');
include(''.INCLUDE_PATH.'config.php');
include(''.INCLUDE_PATH.'settings.php');
include(''.INCLUDE_PATH.'functions.php');
include(''.CLASSES_PATH.'database.php');
$database = new Database;
echo'
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<link href="../Css/minified/jquery-ui-1.10.3.custom.min.css" rel="stylesheet"  />
	<link href="../Css/jtable/themes/metro/blue/jtable.css" rel="stylesheet"  type="text/css" />
	<link href="../Css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
	<link href="../Css/style.css" rel="stylesheet"  />
	<link href="../Css/menu.css" rel="stylesheet"  />
	<link href="../Css/chosen.min.css" rel="stylesheet"  />
	<link href="../Css/autocomplete.css" rel="stylesheet"  />
	<link href="../Css/jquery.timeentry.css" rel="stylesheet"  />
	<link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/icon.css">
	
	<script src="../Scripts/jquery-1.10.2.min.js"></script>
	<script src="../Scripts/jquery-ui.min.js"></script>
	<script src="../Scripts/jquery.timeentry.min.js"></script>
	<script src="../Scripts/jquery.jtable.min.js"></script>
	<script src="../Scripts/jquery.validate.js"></script>
	<script src="../Scripts/jquery.validationEngine.js"></script>
	<script src="../Scripts/jquery.validationEngine-en.js"></script>
	<script src="../Scripts/chosen.jquery.min.js"></script>
	<script src="../Scripts/driver_groups.js"></script>
</head>
<body>';
$query = "SELECT * from drivers__group_names order by group_name ASC";
$result = $database->query($query);
echo'
<table>
	<tr>
		<td valign="top">
			<form name="make_new_driver_group" id="make_new_driver_group" action="" method="POST">

				<table id="log_table">
					<tr>
						<th><b>Select</b></th>
						<th><b>Group Name</b></th>
					</tr>
					<tr>
						<td colspan="2">
							<div id="message"></div>
						</td>
					</tr>';
					while($row = mysql_fetch_array($result))
						{
							echo'<tr>
									<td valign="top"><input type="checkbox" name="group_id[]" class="group_ids" value="'.$row['id'].'"/></td>
									<td>
										<span class="group_names" id="'.$row['id'].'"><a href="#">[ Details] </a></span>&nbsp;&nbsp;&nbsp;&nbsp;
										'.$row['group_name'].' 
										<div id="show_group_members_'.$row['id'].'"></div>
									</td>
								</tr>';
						}
					echo'
					<tr>
						<td valign="bottom"><input type="button" name="delete_groups" id="delete_groups" class="delete_groups" value="Delete"></td>
						<td>
							<input type="text" name="new_group_name" id="new_group_name" placeholder="New Group Name"><br/>
							<input type="button" name="make_new_group" id="make_new_group" class="make_new_group" value="Create New Group">
						</td>
					</tr>
				</table>
			</form>
		</td>
		<td valign="top">
			<div id="show_group_member_details"></div>
		</td>
	</tr>
</table>
</body>
</html>';
?>