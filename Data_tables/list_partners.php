<?php 
session_start();
require_once('../init.php');
include(''.INCLUDE_PATH.'config.php');
include(''.INCLUDE_PATH.'settings.php');
include(''.INCLUDE_PATH.'functions.php');
include(''.CLASSES_PATH.'database.php');
echo'
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<link href="../Css/minified/jquery-ui-1.10.3.custom.min.css" rel="stylesheet"  />
	<link href="../Css/jtable/themes/metro/blue/jtable.css" rel="stylesheet"  type="text/css" />
	<link href="../Css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
	<link href="../Css/style.css" rel="stylesheet"  />
	<link href="../Css/menu.css" rel="stylesheet"  />
	<link href="../Css/chosen.min.css" rel="stylesheet"  />
	<link href="../Css/autocomplete.css" rel="stylesheet"  />
	<link href="../Css/jquery.timeentry.css" rel="stylesheet"  />
	<link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/icon.css">
	
	<script src="../Scripts/jquery-1.10.2.min.js"></script>
	<script src="../Scripts/jquery-ui.min.js"></script>
	<script src="../Scripts/jquery.timeentry.min.js"></script>
	<script src="../Scripts/jquery.jtable.min.js"></script>
	<script src="../Scripts/jquery.validate.js"></script>
	<script src="../Scripts/jquery.validationEngine.js"></script>
	<script src="../Scripts/jquery.validationEngine-en.js"></script>
	<script src="../Scripts/chosen.jquery.min.js"></script>
	<script src="../Scripts/form_new_booking.js"></script>
	
	<style> 
       .redCell
			{
			} 
		.jtable-child-table-container
			{
				margin-left:5%;
			}
    </style>

</head>
<body>'; ?>
<div class="filtering" style="text-align:left;">
	<form>
		<input type="text" name="search_string" id="search_string" />
		<button type="submit" id="LoadSearchButton">Search For</button>
	</form>
</div>
<div id="chargeAccountsTable" style="width: 100%;"></div>
	<script type="text/javascript">
		$(document).ready(function () {
		$('#chargeAccountsTable').jtable({
			title: 'Charge Accounts List',
			paging: true, //Enable paging
			//pageSize: 2,
			sorting: true, //Enable sorting
			defaultSorting: 'account_name ASC',
			selecting: true, //Enable selecting
			multiselect: true, //Allow multiple selecting
			selectingCheckboxes: true, //Show checkboxes on first column
			
			toolbar: {
				items: [
					{
						icon: '../../Images/delete.png',
						text: 'Delete Selected Rows',
						click: function () {
							var $selectedRows = $('#chargeAccountsTable').jtable('selectedRows');
							$('#chargeAccountsTable').jtable('deleteRows', $selectedRows);
						}
					},
					{	
						icon: '../../Images/excel.png',
						text: 'Export to Excel',
						click: function () {
							window.location = '../../Ajax/list_charge_accounts.php?action=export_excel';
						}
					},
					{	
						icon: '../../Images/htmlicon.png',
						text: 'Export to HTML',
						click: function () {
							window.location = '../../Ajax/list_charge_accounts.php?action=export_html';
						}
					},
					{	
						icon: '../../Images/pdficon.png',
						text: 'Export to PDF',
						click: function () {
							window.location = '../../Ajax/list_charge_accounts.php?action=export_pdf';
						}
					}
				]
			},
			deleteConfirmation: function(data) {
				data.deleteConfirmMessage = 'Are you sure to delete this charge Account<br/><b>' + data.record.account_name +'?</b><br/><br/>This action is irreversible..';
			},
			//openChildAsAccordion: true, //Enable this line to show child tabes as accordion style
			actions: {
						listAction: '../../Ajax/list_charge_accounts.php?action=list_charge_accounts',
						createAction: '../../Ajax/list_charge_accounts.php?action=create_charge_accounts',
						updateAction: '../../Ajax/list_charge_accounts.php?action=update_charge_accounts',
						deleteAction: '../../Ajax/list_charge_accounts.php?action=delete_charge_accounts'
					},
			fields: {
						id: {
							key: true,
							create: false,
							edit: false,
							list: false
						},
						Contacts: {
									title: '',
									width: '1%',
									sorting: true, //Enable sorting
									edit: false,
									create: false,
									display: function (userData) 
										{
											//Create an image that will be used to open child table
											var $img = $('<img src="../../Images/address.png" title="View Contacts" />');
											//Open child table when user clicks the image
											$img.click(function () {
												$('#chargeAccountsTable').jtable('openChildTable',
												$img.closest('tr'),
													{
														title:  '<img src="../../Images/address.png" title="View Contacts" />Contacts for ' +userData.record.account_name,
														actions: {
																	listAction: '../../Ajax/list_charge_accounts.php?action=list_charge_accounts_contacts&id='+userData.record.id,
																	deleteAction: '../../Ajax/list_charge_accounts.php?action=delete_charge_accounts_contact&id='+userData.record.id,
																	updateAction: '../../Ajax/list_charge_accounts.php?action=update_charge_accounts_contact&id='+userData.record.id,
																	createAction: '../../Ajax/list_charge_accounts.php?action=create_charge_account_contact&id='+userData.record.id
														},
														fields: {
																	user_id: {
																			title: 'User ID',
																			key: true,
																			list: false,
																			create: false,
																			edit: false
																	},
																	contact_id: {
																			title: 'Contact ID',
																			list: false,
																			create: false,
																			edit: false
																	},
																	//CHILD TABLE DEFINITION FOR "PHONE NUMBERS"
																	Address: {
																				title: '',
																				width: '1%',
																				sorting: false,
																				edit: false,
																				create: false,
																				display: function (userData) 
																					{
																						//Create an image that will be used to open child table
																						var $img = $('<img src="../../Images/address.png" title="View Addresses" />');
																						//Open child table when user clicks the image
																						$img.click(function () {
																							$('#chargeAccountsTable').jtable('openChildTable',
																							$img.closest('tr'),
																								{
																									title:  'Address for ' +userData.record.title +' '+ userData.record.fname +' '+ userData.record.lname,
																									actions: {
																												listAction: '../../Ajax/list_charge_accounts.php?action=list_addresses&id='+userData.record.user_id,
																												deleteAction: '../../Ajax/list_charge_accounts.php?action=delete_addresses&id='+userData.record.user_id,
																												updateAction: '../../Ajax/list_charge_accounts.php?action=update_addresses&id='+userData.record.id,
																												createAction: '../../Ajax/list_charge_accounts.php?action=create_addresses&id='+userData.record.id
																									},
																									fields: {
																												user_id: {
																													type: 'hidden',
																													defaultValue: userData.record.user_id
																												},
																												id: {
																														title: 'ID',
																														key: true,
																														list: false
																												},
																												type_id: {
																													title: 'Type',
																													width: '10%',
																													create: true,
																													edit: true,
																													options: 
																															{ 
																																<?php 
																																	$query_address_type 	= "Select * from variable__address_type order by order_id ASC";
																																	$database = new database;
																																	$result_address_type = $database->query($query_address_type);
																																	while($address_type = mysql_fetch_array($result_address_type))
																																		{
																																			$text_address_type[] = "'".$address_type['id']."':'".$address_type['details']."'";
																																		}
																																	if (!empty($text_address_type))
																																		{ 
																																			$string_address_type = implode(",",$text_address_type);
																																		} 
																																	echo $string_address_type;
																																?>
																															}
																															
																												},
																												line1: {
																														title: 'Line 1'
																												},
																												line2: {
																														title: 'Line 2'
																												},
																												sub: {
																														title: 'Suburb'
																												},
																												postcode: {
																														title: 'Postcode'
																												},
																												state: {
																														title: 'State',
																														options: {
																																	<?php 
																																			$query_states 	= "Select * from variable__states order by order_id ASC";
																																			$database = new database;
																																			$result_states = $database->query($query_states);
																																			while($states = mysql_fetch_array($result_states))
																																				{
																																					$text_states[] = "'".$states['id']."':'".$states['details']."'";
																																				}
																																			if (!empty($text_states))
																																				{ 
																																					$string_states = implode(",",$text_states);
																																				} 
																																			echo $string_states;
																																		?>
																																	}
																												},
																												created_on: {
																														title: 'Record Date',
																														create: false,
																														edit: false,
																														list: false
																												}
																											},
																									//Initialize validation logic when a form is created
																									formCreated: function (event, data) 
																										{
																											data.form.css('width','400px');
																											data.form.find('input[name=line1]').css('width','250px').addClass('validate[required,minSize[1]]');
																											data.form.find('input[name=line2]').css('width','250px');
																											data.form.find('input[name=sub]').css('width','250px').addClass('validate[required, minSize[2]]');
																											data.form.find('input[name=postcode]').css('width','250px');
																											data.form.find('input[name="state"]').css('width','250px');
																											data.form.validationEngine();
																										},
																									//Validate form when it is being submitted
																									formSubmitting: function (event, data) 
																										{
																											return data.form.validationEngine('validate');
																										},
																									//Dispose validation logic when form is closed
																									formClosed: function (event, data) 
																										{
																											data.form.validationEngine('hide');
																											data.form.validationEngine('detach');
																										}
																								}, 
																							function (data) 
																								{ //opened handler
																									data.childTable.jtable('load');
																								});
																							});
																					//Return image to show on the person row
																					return $img;
																				}
																			},
																	type_id: {
																		title: 'Type',
																		width: '10%',
																		create: true,
																		edit: true,
																		options: 
																				{ 
																					<?php 
																						$query_contact_type 	= "Select * from variable__conatct_type order by order_id ASC";
																						$database = new database;
																						$result_contact_type = $database->query($query_contact_type);
																						while($contact_type = mysql_fetch_array($result_contact_type))
																							{
																								$text_contact_type[] = "'".$contact_type['id']."':'".$contact_type['details']."'";
																							}
																						if (!empty($text_contact_type))
																							{ 
																								$string_contact_type = implode(",",$text_contact_type);
																							} 
																						echo $string_contact_type;
																					?>
																				}
																				
																	},
																	title: {
																		title: 'Title',
																		options: {'':'','Mr':'Mr','Ms':'Ms','Mrs':'Mrs','Dr':'Dr','Prof':'Prof','Sir':'Sir'}
																	},
																	fname: {
																		title: 'First Name'
																	},
																	lname: {
																		title: 'Last Name'
																	},
																	password: {
																		title: 'Password'
																	},
																	email: {
																		title: 'Email'
																	},
																	mobile: {
																		title: 'Mobile'
																	},
																	phone: {
																		title: 'phone'
																	},
																	preference: {
																		title: 'Preference'
																	},
																	hidden: {
																		title: 'Visibility',
																		options: {'0':'Current','1':'LEFT'}
																	}
																},
														//Initialize validation logic when a form is created
														formCreated: function (event, data) 
															{
																data.form.css('width','400px');
																data.form.find('input[name=fname]').css('width','250px').addClass('validate[required,minSize[2]]');
																data.form.find('input[name=lname]').css('width','250px');
																data.form.find('input[name=password]').css('width','250px');
																data.form.find('input[name=email]').css('width','250px');
																data.form.find('input[name=mobile]').css('width','250px');
																data.form.find('input[name=phone]').css('width','250px');
																data.form.validationEngine();
															},
														//Validate form when it is being submitted
														formSubmitting: function (event, data) 
															{
																return data.form.validationEngine('validate');
															},
														//Dispose validation logic when form is closed
														formClosed: function (event, data) 
															{
																data.form.validationEngine('hide');
																data.form.validationEngine('detach');
															}
													}, 
												function (data) 
													{ //opened handler
														data.childTable.jtable('load');
													});
												});
										//Return image to show on the person row
										return $img;
									}
								},
						//CHILD TABLE DEFINITION FOR "PHONE NUMBERS"
						poi: {
									title: '',
									width: '1%',
									sorting: false,
									edit: false,
									create: false,
									display: function (userData) 
										{
											//Create an image that will be used to open child table
											var $img = $('<img src="../../Images/history.png" title="POIS" height="16px" width="16px"/>');
											//Open child table when user clicks the image
											$img.click(function () {
												$('#chargeAccountsTable').jtable('openChildTable',
												
												$img.closest('tr'),
													{
														title:  'POIs for ' +userData.record.account_name,
														actions: {
																	listAction: '../../Ajax/list_charge_accounts.php?action=list_pois&id='+userData.record.id,
																	deleteAction: '../../Ajax/list_charge_accounts.php?action=delete_pois&id='+userData.record.id,
																	updateAction: '../../Ajax/list_charge_accounts.php?action=update_pois&id='+userData.record.id,
																	createAction: '../../Ajax/list_charge_accounts.php?action=create_pois&id='+userData.record.id
														},
														fields: {
																	id: {
																			title: 'ID',
																			key: true,
																			list: false
																	},
																	line1: {
																			title: 'Line 1'
																	},
																	line2: {
																			title: 'Line 2'
																	},
																	sub: {
																			title: 'Suburb'
																	},
																	postcode: {
																			title: 'Postcode'
																	},
																	state: {
																			title: 'State',
																			options: {
																						<?php 
																								$query_states1 	= "Select * from variable__states order by order_id ASC";
																								$database = new database;
																								$result_states1 = $database->query($query_states1);
																								while($states1 = mysql_fetch_array($result_states1))
																									{
																										$text_states1[] = "'".$states1['id']."':'".$states1['details']."'";
																									}
																								if (!empty($text_states1))
																									{ 
																										$string_states1 = implode(",",$text_states1);
																									} 
																								echo $string_states1;
																							?>
																						}
																	},
																	country: {
																		title: 'Country',
																		options: {'Australia':'Australia','New Zealand':'New Zealand'}
																	}
																},
														//Initialize validation logic when a form is created
														formCreated: function (event, data) 
															{
																data.form.css('width','400px');
																data.form.find('input[name=line1]').css('width','250px').addClass('validate[required,minSize[1]]');
																data.form.find('input[name=line2]').css('width','250px');
																data.form.find('input[name=sub]').css('width','250px').addClass('validate[required, minSize[2]]');
																data.form.find('input[name=postcode]').css('width','250px');
																data.form.find('input[name="state"]').css('width','250px');
																data.form.validationEngine();
															},
														//Validate form when it is being submitted
														formSubmitting: function (event, data) 
															{
																return data.form.validationEngine('validate');
															},
														//Dispose validation logic when form is closed
														formClosed: function (event, data) 
															{
																data.form.validationEngine('hide');
																data.form.validationEngine('detach');
															}
													}, 
												function (data) 
													{ //opened handler
														data.childTable.jtable('load');
													});
												});
										//Return image to show on the person row
										return $img;
									}
								},
						acc_type: {
							title: 'Account Type',
							width: '10%',
							create: true,
							edit: true,
							options: 
									{ 
										<?php 
										$query_charge_acc_type 	= "Select * from variable__charge_acc_type order by order_id ASC";
										$database = new database;
										$result_charge_acc_type = $database->query($query_charge_acc_type);
										while($charge_acc_type = mysql_fetch_array($result_charge_acc_type))
											{
												$text_charge_acc_type[] = "'".$charge_acc_type['id']."':'".$charge_acc_type['details']."'";
											}
										if (!empty($text_charge_acc_type))
											{ 
												$string_charge_acc_type = implode(",",$text_charge_acc_type);
											} 
										echo $string_charge_acc_type;
										?>
									}
						},
						account_name: {
							title: 'Account Name'
						},
						myob_no: {
							title: 'MYOB Number'
						},
						created_by: {
							title: 'Created By',
							create: false,
							edit: false,
							list: true
						},
						created_on: {
							title: 'Record Date',
							create: false,
							edit: false,
							list: false
						}
					},
				//Initialize validation logic when a form is created
				formCreated: function (event, data) 
					{
						data.form.css('width','400px');
						data.form.find('input[name=account_name]').css('width','250px').addClass('validate[required,minSize[2]]');
						data.form.find('input[name=myob_no]').css('width','250px');
						data.form.validationEngine();
					},
				//Validate form when it is being submitted
				formSubmitting: function (event, data) 
					{
						return data.form.validationEngine('validate');
					},
				//Dispose validation logic when form is closed
				formClosed: function (event, data) 
					{
						data.form.validationEngine('hide');
						data.form.validationEngine('detach');
					}

		});
		 //Re-load records when user click 'load records' button.
        $('#LoadSearchButton').click(function (e) 
			{
				e.preventDefault();
				$('#chargeAccountsTable').jtable('load', 
					{
						search_string: $('#search_string').val()
					});
			});
		//Load student list from server
		$('#chargeAccountsTable').jtable('load');
	});
</script>
</body>
<html>
