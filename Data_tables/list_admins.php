<?php 
session_start();
require_once('../init.php');
include(''.INCLUDE_PATH.'config.php');
include(''.INCLUDE_PATH.'settings.php');
include(''.INCLUDE_PATH.'functions.php');
include(''.CLASSES_PATH.'database.php');
echo'
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<link href="../Css/minified/jquery-ui-1.10.3.custom.min.css" rel="stylesheet"  />
	<link href="../Css/jtable/themes/metro/blue/jtable.css" rel="stylesheet"  type="text/css" />
	<link href="../Css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
	<link href="../Css/style.css" rel="stylesheet"  />
	<link href="../Css/menu.css" rel="stylesheet"  />
	<link href="../Css/chosen.min.css" rel="stylesheet"  />
	<link href="../Css/autocomplete.css" rel="stylesheet"  />
	<link href="../Css/jquery.timeentry.css" rel="stylesheet"  />
	<link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/icon.css">
	
	<script src="../Scripts/jquery-1.10.2.min.js"></script>
	<script src="../Scripts/jquery-ui.min.js"></script>
	<script src="../Scripts/jquery.timeentry.min.js"></script>
	<script src="../Scripts/jquery.jtable.min.js"></script>
	<script src="../Scripts/jquery.validate.js"></script>
	<script src="../Scripts/jquery.validationEngine.js"></script>
	<script src="../Scripts/jquery.validationEngine-en.js"></script>
	<script src="../Scripts/chosen.jquery.min.js"></script>
	<script src="../Scripts/form_new_booking.js"></script>
	<style> 
       .redCell
			{
			} 
		.jtable-child-table-container
			{
				margin-left:5%;
			}
    </style>

</head>
<body>'; ?>
<div class="filtering" style="text-align:left;">
	<form>
		<input type="text" name="search_string" id="search_string" />
		<button type="submit" id="LoadSearchButton">Search For</button>
	</form>
</div>
<div id="adminsTable" style="width: 100%;"></div>
	<script type="text/javascript">
		$(document).ready(function () {
		$('#adminsTable').jtable({
			title: 'Users List',
			paging: true, //Enable paging
			//pageSize: 2,
			sorting: true, //Enable sorting
			defaultSorting: 'id ASC',
			selecting: true, //Enable selecting
			multiselect: true, //Allow multiple selecting
			selectingCheckboxes: true, //Show checkboxes on first column
			
			toolbar: {
				items: [
					{
						icon: '../../Images/delete.png',
						text: 'Delete Selected Rows',
						click: function () {
							var $selectedRows = $('#adminsTable').jtable('selectedRows');
							$('#adminsTable').jtable('deleteRows', $selectedRows);
						}
					},
					{	
						icon: '../../Images/excel.png',
						text: 'Export to Excel',
						click: function () {
							window.location = '../../Ajax/list_admins.php?action=export_excel';
						}
					},
					{	
						icon: '../../Images/htmlicon.png',
						text: 'Export to HTML',
						click: function () {
							window.location = '../../Ajax/list_admins.php?action=export_html';
						}
					},
					{	
						icon: '../../Images/pdficon.png',
						text: 'Export to PDF',
						click: function () {
							window.location = '../../Ajax/list_admins.php?action=export_pdf';
						}
					}
				]
			},
			deleteConfirmation: function(data) {
				data.deleteConfirmMessage = 'Are you sure to delete user<br/><b>' + data.record.title +' '+ data.record.fname +' '+ data.record.lname +'?</b><br/><br/>This action is irreversible..';
			},
			//openChildAsAccordion: true, //Enable this line to show child tabes as accordion style
			actions: {
						listAction: '../../Ajax/list_admins.php?action=list_user',
						createAction: '../../Ajax/list_admins.php?action=create_user',
						updateAction: '../../Ajax/list_admins.php?action=update_user',
						deleteAction: '../../Ajax/list_admins.php?action=delete_user'
					},
			fields: {
						id: {
							key: true,
							create: false,
							edit: false,
							list: false
						},
						//CHILD TABLE DEFINITION FOR "PHONE NUMBERS"
						Address: {
									title: '',
									width: '1%',
									sorting: false,
									edit: false,
									create: false,
									display: function (userData) 
										{
											//Create an image that will be used to open child table
											var $img = $('<img src="../../Images/address.png" title="View Addresses" />');
											//Open child table when user clicks the image
											$img.click(function () {
												$('#adminsTable').jtable('openChildTable',
												$img.closest('tr'),
													{
														title:  'Address for ' +userData.record.title +' '+ userData.record.fname +' '+ userData.record.lname,
														actions: {
																	listAction: '../../Ajax/list_admins.php?action=list_addresses&id='+userData.record.id,
																	deleteAction: '../../Ajax/list_admins.php?action=delete_addresses&id='+userData.record.id,
																	updateAction: '../../Ajax/list_admins.php?action=update_addresses&id='+userData.record.id,
																	createAction: '../../Ajax/list_admins.php?action=create_addresses&id='+userData.record.id
														},
														fields: {
																	user_id: {
																		type: 'hidden',
																		defaultValue: userData.record.id
																	},
																	id: {
																			title: 'ID',
																			key: true,
																			list: false
																	},
																	type_id: {
																		title: 'Type',
																		width: '10%',
																		create: true,
																		edit: true,
																		options: 
																				{ 
																					<?php 
																						$query_address_type	= "Select * from variable__address_type order by order_id ASC";
																						$database = new database;
																						$address_type_res = $database->query($query_address_type);
																						while($address_type = mysql_fetch_array($address_type_res))
																							{
																								$address_type_text[] = "'".$address_type['id']."':'".$address_type['details']."'";
																							}
																						if (!empty($address_type_text))
																							{ 
																								$address_type_string = implode(",",$address_type_text);
																							} 
																						echo $address_type_string;
																					?>
																				}
																				
																	},
																	line1: {
																			title: 'Line 1'
																	},
																	line2: {
																			title: 'Line 2'
																	},
																	sub: {
																			title: 'Suburb'
																	},
																	postcode: {
																			title: 'Postcode'
																	},
																	state: {
																			title: 'State',
																			options: {
																					<?php 
																							$query_states 	= "Select * from variable__states order by order_id ASC";
																							$database = new database;
																							$result_states = $database->query($query_states);
																							while($states = mysql_fetch_array($result_states))
																								{
																									$text_states[] = "'".$states['id']."':'".$states['details']."'";
																								}
																							if (!empty($text_states))
																								{ 
																									$string_states = implode(",",$text_states);
																								} 
																							echo $string_states;
																						?>
																					}
																	},
																	created_on: {
																			title: 'Record Date',
																			create: false,
																			edit: false,
																			list: false
																	}
																},
														//Initialize validation logic when a form is created
														formCreated: function (event, data) 
															{
																data.form.css('width','400px');
																data.form.find('input[name=line1]').css('width','250px').addClass('validate[required,minSize[2]]');
																data.form.find('input[name=line2]').css('width','250px');
																data.form.find('input[name=sub]').css('width','250px').addClass('validate[required, minSize[2]]');
																data.form.find('input[name=postcode]').css('width','250px');
																data.form.find('input[name="state"]').css('width','250px');
																data.form.validationEngine();
															},
														//Validate form when it is being submitted
														formSubmitting: function (event, data) 
															{
																return data.form.validationEngine('validate');
															},
														//Dispose validation logic when form is closed
														formClosed: function (event, data) 
															{
																data.form.validationEngine('hide');
																data.form.validationEngine('detach');
															}
													}, 
												function (data) 
													{ //opened handler
														data.childTable.jtable('load');
													});
												});
										//Return image to show on the person row
										return $img;
									}
								},
						role_id: {
							title: 'Role',
							width: '10%',
							create: true,
							edit: true,
							options: 
									{ 
										<?php 
											$query_user_role 	= "Select * from variable__user_role order by order_id ASC";
											$database = new database;
											$result_user_role = $database->query($query_user_role);
											while($user_role = mysql_fetch_array($result_user_role))
												{
													$text_user_role[] = "'".$user_role['id']."':'".$user_role['details']."'";
												}
											if (!empty($text_user_role))
												{ 
													$string_user_role = implode(",",$text_user_role);
												} 
											echo $string_user_role;
										?>
									}
									
						},
						title: {
							title: 'Title',
							options: {'':'','Mr':'Mr','Ms':'Ms','Mrs':'Mrs','Dr':'Dr','Prof':'Prof'}
						},
						fname: {
							title: 'Fname'
						},
						lname: {
							title: 'Lname'
						},
						password: {
							title: 'Password'
						},
						email: {
									title: 'Email',
									width: '15%',
									sorting: true,
									edit: true,
									create: true
									
						},
						mobile: {
									title: 'mobile',
									width: '10%',
									sorting: true,
									edit: true,
									create: true
						},
						phone: {
									title: 'Phone',
									width: '10%',
									sorting: true,
									edit: true,
									create: true
						},
						created_on: {
							title: 'Record Date',
							create: false,
							edit: false,
							list: false
						},
						hidden: {
							title: 'Visibility',
							options: {'0':'Current','1':'LEFT'}
						}
					},
				//Initialize validation logic when a form is created
				formCreated: function (event, data) 
					{
						data.form.css('width','400px');
						data.form.find('input[name=fname]').css('width','250px').addClass('validate[required,minSize[2]]');
						data.form.find('input[name=lname]').css('width','250px');
						data.form.find('input[name=password]').css('width','250px');
						data.form.find('input[name=email]').css('width','250px');
						data.form.find('input[name=mobile]').css('width','250px');
						data.form.find('input[name=phone]').css('width','250px');
						data.form.validationEngine();
					},
				//Validate form when it is being submitted
				formSubmitting: function (event, data) 
					{
						return data.form.validationEngine('validate');
					},
				//Dispose validation logic when form is closed
				formClosed: function (event, data) 
					{
						data.form.validationEngine('hide');
						data.form.validationEngine('detach');
					}

		});
		 //Re-load records when user click 'load records' button.
        $('#LoadSearchButton').click(function (e) 
			{
				e.preventDefault();
				$('#adminsTable').jtable('load', 
					{
						search_string: $('#search_string').val()
					});
			});
		//Load student list from server
		$('#adminsTable').jtable('load');
	});
</script>
</body>
<html>