<?php 
session_start();
require_once('../init.php');
include(''.INCLUDE_PATH.'config.php');
include(''.INCLUDE_PATH.'settings.php');
include(''.INCLUDE_PATH.'functions.php');
include(''.CLASSES_PATH.'database.php');
echo'
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<link href="../Css/minified/jquery-ui-1.10.3.custom.min.css" rel="stylesheet"  />
	<link href="../Css/jtable/themes/metro/blue/jtable.css" rel="stylesheet"  type="text/css" />
	<link href="../Css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
	<link href="../Css/style.css" rel="stylesheet"  />
	<link href="../Css/menu.css" rel="stylesheet"  />
	<link href="../Css/chosen.min.css" rel="stylesheet"  />
	<link href="../Css/autocomplete.css" rel="stylesheet"  />
	<link href="../Css/jquery.timeentry.css" rel="stylesheet"  />
	<link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/icon.css">
	
	<script src="../Scripts/jquery-1.10.2.min.js"></script>
	<script src="../Scripts/jquery-ui.min.js"></script>
	<script src="../Scripts/jquery.timeentry.min.js"></script>
	<script src="../Scripts/jquery.jtable.min.js"></script>
	<script src="../Scripts/jquery.validate.js"></script>
	<script src="../Scripts/jquery.validationEngine.js"></script>
	<script src="../Scripts/jquery.validationEngine-en.js"></script>
	<script src="../Scripts/chosen.jquery.min.js"></script>
	
	<style> 
       .redCell
			{
			} 
		.jtable-child-table-container
			{
				margin-left:5%;
			}
    </style>

</head>
<body>'; ?>
<!--<div class="filtering" style="text-align:left;">
	<form>
		<input type="text" name="search_string" id="search_string" />
		<button type="submit" id="LoadSearchButton">Search For</button>
	</form>-->
</div>
<div id="logBookingsTable" style="width: 100%;"></div>
	<script type="text/javascript">
		$(document).ready(function () {
		$('#logBookingsTable').jtable({
			title: 'Booking Log List',
			paging: true, //Enable paging
			//pageSize: 2,
			sorting: true, //Enable sorting
			defaultSorting: 'id DESC',
			selecting: true, //Enable selecting
			multiselect: true, //Allow multiple selecting
			selectingCheckboxes: true, //Show checkboxes on first column
			
			toolbar: {
				items: [
					{
						icon: '../../Images/delete.png',
						text: 'Delete Selected Rows',
						click: function () {
							var $selectedRows = $('#logBookingsTable').jtable('selectedRows');
							$('#logBookingsTable').jtable('deleteRows', $selectedRows);
						}
					}
				]
			},
			deleteConfirmation: function(data) {
				data.deleteConfirmMessage = 'Are you sure to delete log<br/><b>' + data.record.job_id +' ?</b><br/><br/>This action is irreversible..';
			},
			//openChildAsAccordion: true, //Enable this line to show child tabes as accordion style
			actions: {
						listAction: '../../Ajax/log_bookings.php?action=list_booking_log',
						deleteAction: '../../Ajax/log_bookings.php?action=delete_booking_log'
					},
			fields: {
						id: {
							key: true,
							create: false,
							edit: false,
							list: false
						},
						job_id: {
							title: 'Job Id'
						},
						entered_by: {
							title: 'Entered By'							
						},
						created_on: {
							title: 'Record Date'
						},
						message: {
							title: 'Message'
						},
						new_value: {
							title: 'New Value'
						},
						old_value: {
							title: 'Old Value'
						}
					}

		});
		 //Re-load records when user click 'load records' button.
       /* $('#LoadSearchButton').click(function (e) 
			{
				e.preventDefault();
				$('#logBookingsTable').jtable('load', 
					{
						search_string: $('#search_string').val()
					});
			});*/
		//Load student list from server
		$('#logBookingsTable').jtable('load');
	});
</script>
</body>
<html>