<?php 
session_start();
require_once('../init.php');
include(''.INCLUDE_PATH.'auth.php');
include(''.INCLUDE_PATH.'config.php');
include(''.INCLUDE_PATH.'settings.php');
include(''.INCLUDE_PATH.'functions.php');
include(''.CLASSES_PATH.'database.php');
include(''.CLASSES_PATH.'chargeAccount.php');
echo'
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
<link href="../Css/minified/jquery-ui-1.10.3.custom.min.css" rel="stylesheet"  />
	<link href="../Css/jtable/themes/lightcolor/blue/jtable.css" rel="stylesheet"  type="text/css" />
	<link href="../Css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
	<link href="../Css/style.css" rel="stylesheet"  />
	<link href="../Css/menu.css" rel="stylesheet"  />
	<link href="../Css/chosen.min.css" rel="stylesheet"  />
	<link href="../Css/autocomplete.css" rel="stylesheet"  />
	<link href="../Css/jquery.timeentry.css" rel="stylesheet"  />
	<link rel="stylesheet" type="text/css" href="../Css/easyui/themes/default/easyui.css">
	
	<script src="../Scripts/jquery-1.10.2.min.js"></script>
	<script src="../Scripts/jquery-ui.min.js"></script>
	<script src="../Scripts/jquery.timeentry.min.js"></script>
	<script src="../Scripts/jquery.jtable.min.js"></script>
	<script src="../Scripts/jquery.validate.js"></script>
	<script src="../Scripts/jquery.validationEngine.js"></script>
	<script src="../Scripts/jquery.validationEngine-en.js"></script>
	<script src="../Scripts/chosen.jquery.min.js"></script>
	<script src="../Scripts/reports_job_driver.js"></script>';?>
 
	<style> 
       .redCell
        {
        } 
      </style>
	  

</head>
<body>
<div id="tabs">
<ul>
	<li><a href="#tabs-1" >Reports <br>1. Select a date range for your report, by using "Preset Dates" or "Select a Date Range"  <br>2. Click on "Search" 
	<br>3. You can download the report using "Download as HTML, PDF or Excel" 
	<br>4. For a more refined report, you can also use either or both "Extra Search Options"  & "More Option Switches"
	<br>5. Click on "Search" to refresh report </a></li>
	<!--<li><a href="../Ajax/advanced_report_driver_form.php">Advanced</a></li>-->

</ul>
<div id="tabs-1">

<form name="search_dates" id="search_dates" method="post" action="../Ajax/report_jobs_allotted_to_drivers.php">
	<div style="width:100%">
		<div style="float:left; padding:10px; border:1px solid black; width:50%;">
			<h1> Preset Dates </h1>
			<input type="radio" name="preset_dates" id="preset_dates" class="preset_dates_cls" value="1" /><img src="../Images/previous_week.png" height="50">
			<input type="radio" name="preset_dates" id="preset_dates" class="preset_dates_cls" value="2" /><img src="../Images/previous_day.png" height="50">
			<input type="radio" name="preset_dates" id="preset_dates" class="preset_dates_cls" value="3" /><img src="../Images/today.png" height="50">
			<input type="radio" name="preset_dates" id="preset_dates" class="preset_dates_cls" value="4" /><img src="../Images/this_week.png" height="50">
			<input type="radio" name="preset_dates" id="preset_dates" class="preset_dates_cls" value="5" /><img src="../Images/next_day.png" height="50">
			<input type="radio" name="preset_dates" id="preset_dates" class="preset_dates_cls" value="6" /><img src="../Images/next_week.png" height="50"><br/>
			<input type="radio" name="preset_dates" id="preset_dates" class="preset_dates_cls" value="11" /><img src="../Images/lastmon.PNG" height="50">
			<input type="radio" name="preset_dates" id="preset_dates" class="preset_dates_cls" value="12" /><img src="../Images/thismon.PNG" height="50">
			<input type="radio" name="preset_dates" id="preset_dates" class="preset_dates_cls" value="7" /><img src="../Images/lastcal.PNG" height="50">
			<input type="radio" name="preset_dates" id="preset_dates" class="preset_dates_cls" value="8" /><img src="../Images/lastfin.PNG" height="50">
			<input type="radio" name="preset_dates" id="preset_dates" class="preset_dates_cls" value="9" /><img src="../Images/thiscal.PNG" height="50">
			<input type="radio" name="preset_dates" id="preset_dates" class="preset_dates_cls" value="10" /><img src="../Images/thisfin.PNG" height="50">
			
			<input type="hidden" name="wk_from" id="wk_from" value="" />
			<input type="hidden" name="wk_to" id="wk_to" value="" />
			<input type="hidden" name="dt_from" id="dt_from" value="" />
			<input type="hidden" name="dt_to" id="dt_to" value="" />
			<input type="hidden" name="wk_counter" id="wk_counter" value="" />
			<input type="hidden" name="counter" id="dt_counter" value="" />
			<input type="hidden" name="yr_from" id="yr_from" value="" />
			<input type="hidden" name="yr_to" id="yr_to" value="" />
			<input type="hidden" name="mn_from" id="mn_from" value="" />
			<input type="hidden" name="mn_to" id="mn_to" value="" />
		</div>
		<div style="float:left; padding:10px; width:40%; ">
			<div style="padding:10px; border:1px solid black;">
				<h1> Select a Date Range </h1>
				<div>
					<input type="radio" name="all_booking_radio" id="all_booking_radio" class="all_booking_radio_cls" value="1" /> <strong>ALL</strong> Bookings (Please <strong>Only</strong> Select this Option if <strong>Absolutely</strong> Necessary)<br />
					<input type="radio" name="all_booking_radio" id="all_booking_radio" class="all_booking_radio_cls" value="2" /> Select Date range<br />
				</div>
				<div style="padding:10px;">
					<input type="text" name="date_from" id="date_from" size="15" disabled placeholder="Date From" value="<?php echo $_POST['date_from'];?>" />
					<input type="hidden" name="cap_date_from" id="cap_date_from" value="<?php echo $_POST['cap_date_from'];?>" />
					<input type="text" name="date_to" id="date_to" size="15" disabled placeholder="Date To" value="<?php echo $_POST['date_to'];?>" />
					<input type="hidden" name="cap_date_to" id="cap_date_to" value="<?php echo $_POST['cap_date_to'];?>" />
				</div>
			</div>
		</div>
	</div>
	<div style="width:100%;">
		
		<div style="float:left; margin:10px 0 0 0 ; padding:10px; border:1px solid black; width:50%;">
			<h1> Extra Search Options</h1>
			<?php if($_SESSION['ROLE_ID'] == '1')
			{ ?>
			<div class="mst_contain">
				<label> Driver &nbsp;&nbsp; </label>
				<input type="text" name="search_by_drivers" id="search_by_drivers" value="" placeholder="Driver's Name" />
				<input type="hidden" name="search_by_drivers_id" id="search_by_drivers_id" value=""  />
			</div>
			<?php } ?>
			<div class="mst_contain">
				<label> <?php echo ($_SESSION['ROLE_ID'] == '3')? 'Passenger / Student': 'Passenger';  ?>&nbsp;&nbsp; </label>
				<input type="text" name="search_by_pax" id="search_by_pax" value="" placeholder="Passenger's Name" />
				<input type="hidden" name="search_by_pax_id" id="search_by_pax_id" value=""  />
			</div>
			<?php if($_SESSION['ROLE_ID'] != '2')
			{ ?>
			<div class="mst_contain">
				<label> Car &nbsp;&nbsp; </label>
				<input type="text" name="search_by_car" id="search_by_car" value="" placeholder="Car Type" />
				<input type="hidden" name="search_by_car_id" id="search_by_car_id" value=""  />
			</div>
			
			<?php 
			}
			if($_SESSION['ROLE_ID'] == '1')
			{ ?>
			<div class="mst_contain">
				<label> Charge To &nbsp;&nbsp; </label>
				<input type="text" name="search_by_charge_account" id="search_by_charge_account" value="" placeholder="Charge Account Name" />
				<input type="hidden" name="search_by_charge_account_id" id="search_by_charge_account_id" value=""  />
			</div>
			<div class="mst_contain">
				<label> Booking Source &nbsp;&nbsp; </label>
				<select name="search_by_job_src" id="search_by_job_src" >
					<option value=""> Select Job Source</option>
					<option value="Email"> Email</option>
					<option value="Website"> Website</option>
					<option value="Phone"> Phone</option>
					<option value="Others"> Others</option>
				</select>
			</div>
			<div class="mst_contain">
				<label> Driver status &nbsp;&nbsp; </label>
				<select name="search_by_driver_status" id="search_by_driver_status" >
					<option value=""> Select Driver Status</option>
					<option value="1">Allocated</option>
					<option value="2">Sent</option>
					<option value="3">Accepted</option>
					<option value="4">Rogered</option>
					<option value="5">Paid</option>
				</select>
			</div>
			<?php } ?>
			<div class="mst_contain">
				<label> Flight Number &nbsp;&nbsp; </label>
				<input type="text" name="search_by_flight_no" id="search_by_flight_no" value="" placeholder="Flight No."  >
			</div>
			<div class="mst_contain">
				<label> Time &nbsp;&nbsp; </label>
				<select name="search_by_time_hrs" id="search_by_time_hrs" >
						<option value="">Hours</option>
						<?php for($x=0; $x<24; $x++)
							{
								if($x<10)
									{
										$x = '0'.$x.'';
									}
								echo '<option value="'.$x.'" >'.$x.'</option>';
							}
					?>
					</select>
					<select name="search_by_time_mins" id="search_by_time_mins" >
						<option value="">Minutes</option>
					<?php	for($y=0; $y<60; $y++)
							{
								if($y<10)
									{
										$y = '0'.$y.'';
									}
								echo '<option value="'.$y.'"> '.$y.'</option>';
							}
					?>
					</select> 
			</div>
		</div>
		
		<?php 
		if($_SESSION['ROLE_ID'] != '2')
			{ ?>
			<div style="float:left; padding:10px;  width:40%;">
				<div style="padding:10px; border:1px solid black; margin:10px;">
					<h1> More Options Switches</h1>
					<div class="swt_head">
						<label>
							Job Statuses
						</label>
						<span>
							Must Be
						</span>
						<span>
							Must Not Be
						</span>
						<span>
							Does Not Matter
						</span>
					</div>
					
					<div class="swt_row">
						<label>
							Waiting
						</label>
						<span>
							<input type="radio" name="swt5" id="swt5" value="1" />
						</span>
						<span>
							<input type="radio" name="swt5" id="swt5" value="-1" />
						</span>
						<span>
							<input type="radio" name="swt5" id="swt5" value="0" checked />
						</span>
					</div>

					<div class="swt_row">
						<label>
							Received
						</label>
						<span>
							<input type="radio" name="swt10" id="swt10" value="1" />
						</span>
						<span>
							<input type="radio" name="swt10" id="swt10" value="-1" />
						</span>
						<span>
							<input type="radio" name="swt10" id="swt10" value="0" checked />
						</span>
					</div>
					
					<div class="swt_row">
						<label>
							Unconfirmed
						</label>
						<span>
							<input type="radio" name="swt10" id="swt10" value="1" />
						</span>
						<span>
							<input type="radio" name="swt10" id="swt10" value="-1" />
						</span>
						<span>
							<input type="radio" name="swt30" id="swt30" value="0" checked />
						</span>
					</div>
					
					<div class="swt_row">
						<label>
							Confirmed
						</label>
						<span>
							<input type="radio" name="swt20" id="swt20" value="1" />
						</span>
						<span>
							<input type="radio" name="swt20" id="swt20" value="-1" />
						</span>
						<span>
							<input type="radio" name="swt20" id="swt20" value="0" checked />
						</span>
					</div>
					
					<div class="swt_row">
						<label>
							No Show
						</label>
						<span>
							<input type="radio" name="swt80" id="swt80" value="1" />
						</span>
						<span>
							<input type="radio" name="swt80" id="swt80" value="-1" />
						</span>
						<span>
							<input type="radio" name="swt80" id="swt80" value="0" checked />
						</span>
					</div>
					
					<div class="swt_row">
						<label>
							Declined
						</label>
						<span>
							<input type="radio" name="swt90" id="swt90" value="1" />
						</span>
						<span>
							<input type="radio" name="swt90" id="swt90" value="-1" />
						</span>
						<span>
							<input type="radio" name="swt90" id="swt90" value="0" checked />
						</span>
					</div>
					
					<div class="swt_row">
						<label>
							Cancelled
						</label>
						<span>
							<input type="radio" name="swt100" id="swt100" value="1" />
						</span>
						<span>
							<input type="radio" name="swt100" id="swt100" value="-1" />
						</span>
						<span>
							<input type="radio" name="swt100" id="swt100" value="0" checked />
						</span>
					</div>
					
				</div>
				<?php if($_SESSION['ROLE_ID'] == '1')
				{ ?>
				<div style="padding:10px 10px 20px 10px; border:1px solid black; margin:10px; ">
					<h1> Report type </h1>
					<input type="checkbox" name="driver_by_price" id="driver_by_price" value="1" />&nbsp;&nbsp;
					<strong>Total Payable - Driver's report</strong>
				</div>
				<?php } ?>
			</div>
			<?php } ?>
	</div>
	<div style="padding:10px; width:100%; float:right;">
		<input type="hidden" name="pressed_button" id="pressed_button" value="" />
		<input type="hidden" name="hidden_role_id" id="hidden_role_id" value="<?php echo $_SESSION['ROLE_ID']; ?>" />
		
		<input type="submit" class="sml_btn" name="search_specific" id="search_specific" value="Search">
		<?php if($_SESSION['ROLE_ID'] == '1')
		{ ?>
		<input type="submit" class="big_btn"  name="download_html" id="download_html" value="Download as Html" /> &nbsp;&nbsp;
		<?php } ?>
		<input type="submit" class="big_btn"  name="download_pdf" id="download_pdf" value="Download as Pdf" />
		<input type="submit" class="big_btn"  name="download_excel" id="download_excel" value="Download as Excel" />
	</div>
</form> 

	
	<div style="clear:both"></div>
	
</div>
</div>
<div id="allBookingsTable" style="width: 100%;"></div>
</body>
</html>