<?php
// INCLUDE THE REQUIRED FILES.
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(INCLUDE_PATH . "functions.php");
require_once(INCLUDE_PATH . "config.php");

// ADD HEADERS FOR THE WEBSERVICE.
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: POST, GET');
header('Content-type: application/json');
header('Access-Control-Allow-Headers:Content-Type');

// INITIALIZE DIFFERENT CLASS OBJECTS.
$database = new Database;

// RECEIVE THE DATA IN JSON FORMAT.
$inputJSON = file_get_contents('php://input');
$obj = json_decode($inputJSON,TRUE);

// ---------- ACCOUNTING LISTING STARTS HERE. ---------- //

if( $obj["action"] == "list_driver_accounts" )
{
	$query = "SELECT * FROM drivers__accounts_".$obj["id"]." ORDER BY id DESC" ;
	$result = $database->query($query);
	$recordCount = mysql_num_rows($result);
	$rows = array();
	while($row = mysql_fetch_array($result))
	{
		$row['txn_type'] = ( $row['amt_cr'] > 0 )?'credit_list':'debit_list';
		//$row['txn_type'] = ( $row['amt_dr'] > 0 )?'dr':'';
		$rows[] = $row;
	}
	
	$query1 = "SELECT SUM(amt_cr) as credit_amt, SUM(amt_dr) as debit_amt, SUM(amt_cr)-SUM(amt_dr) as balance FROM drivers__accounts_".$obj["id"]." " ;
	$result1 = $database->query($query1);
	$row1 = mysql_fetch_array($result1);

	$return_data = array();
	$return_data['status'] = 200;
	$return_data['record_count'] = $recordCount;
	$return_data['records'] = $rows;
	$return_data['credit_amt'] = $row1['credit_amt'];
	$return_data['debit_amt'] = $row1['debit_amt'];
	$return_data['balance'] = $row1['balance'];
	
	$json = json_encode($return_data); 
	echo $json;
}