<?php
// INCLUDE THE REQUIRED FILES.
require_once('../init.php');
include(''.INCLUDE_PATH.'config.php');
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "chargeAccount.php");
require_once(CLASSES_PATH . "job.php");
require_once(CLASSES_PATH . "jobReference.php");
require_once(CLASSES_PATH . "address.php");
require_once(CLASSES_PATH . "mailer.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "vehicle.php");
// ADD HEADERS FOR THE WEBSERVICE.
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: POST, GET');
header('Content-type: application/json');
header('Access-Control-Allow-Headers:Content-Type');
// INITIALIZE DIFFERENT CLASS OBJECTS.
$database = new database;
$charge_acc = new chargeAccount();
$user = new user();
$job = new Job();
$job_ref = new jobReference();
$address = new Address();
// RECEIVE THE DATA IN JSON FORMAT.
$inputJSON = file_get_contents('php://input');
$obj = json_decode($inputJSON,TRUE);

// ---------- NEW BOOKING STARTS HERE. ---------- //

// ADD NEW CHARGE ACC, CONTACT AND PASSENGER IF REQUIRED.
if( $obj['charge_acc'] != '' )  // means charge account was selected from drop down list
{
	$charge_acc_id = $obj['charge_acc'];
}
else  // means new charge account entered
{
	$new_account_name = mysql_real_escape_string($obj['account_name']);
	if( $new_account_name != '' )
	{
		$charge_acc_id = $charge_acc->addChargeAccount($obj['user_id'], $obj['account_type'], $new_account_name, $obj['myob_no']);
	}
}
if($obj['acc_contact'] !='')  // means booking made by selected from drop down list
{
	$contact_id = $obj['acc_contact'];
}
else  // means new booking made by entered
{
	$new_bkg_by_fname = mysql_real_escape_string($obj['bkg_by_fname']);
	$new_bkg_by_lname = mysql_real_escape_string($obj['bkg_by_lname']);
	$new_bkg_by_password = mysql_real_escape_string($obj['bkg_by_password']);
	if( $new_bkg_by_fname != '' )
	{
		$contact_id = $charge_acc->addChargeAccountContact($obj['bkg_by_title'], $new_bkg_by_fname, $new_bkg_by_lname, $new_bkg_by_password, $obj['bkg_by_email'], $obj['bkg_by_mobile'], $obj['bkg_by_phone'], $obj['bkg_by_preference'], $charge_acc_id, $obj['bkg_by_type']);
	}
}
if($obj['acc_passenger']!='' || $obj['acc_passenger']!=NULL)  // means pax selected from drop down list
{
	$pax_id = $obj['acc_passenger'];
}
else  // means new passenger entered
{
	if( ($obj['bkg_by_fname'] != $obj['new_pax_fname']) && ($obj['bkg_by_lname'] != $obj['new_pax_lname']) )
	{
		$new_pax_fname = mysql_real_escape_string($obj['new_pax_fname']);
		$new_pax_lname = mysql_real_escape_string($obj['new_pax_lname']);
		$new_pax_password = mysql_real_escape_string($obj['new_pax_password']);
		if( $new_pax_fname != '' )
		{
			$pax_id = $charge_acc->addChargeAccountContact($obj['new_pax_title'], $new_pax_fname, $new_pax_lname, $new_pax_password, $obj['new_pax_email'],$obj['new_pax_mobile'],$obj['new_pax_phone'], $obj['new_pax_preference'], $charge_acc_id, $obj['new_pax_type']);
		}
	}
	else
	{
		$pax_id = $contact_id;
	}
}

if( $obj['role_id'] == '1' )  // admin
{
	$job_status_id = '20';  // confirmed
}
if( $obj['role_id'] == '3' ) // client or pax
{
	$job_status_id = '10';  // received
}

$from_state = $address->getStateName($obj['j1_state']);
$to_state = $from_state;
if( $obj['j1_via_sub'] != '' )  // if suburb not blank
{
	$from_via_state = $from_state;
}
if( $obj['j1_to_via_sub'] != '' )  // if suburb not blank
{
	$to_via_state = $from_state;
}
$conf_to_client = ($obj['conf_to_bkg_made_by']=='1')? 1:(($obj['conf_to_client']==1)? 1:0);
$conf_to_pax = $obj['conf_to_pax'];



// ADD VALUES TO JOB REFERENCE TABLE.
$job_reference_id = $job_ref->addNewJobReference(
													$obj['user_id'],
													$obj['job_src'],
													$obj['order_ref'],
													$obj['acc_type'],
													$obj['std_id'],
													$obj['std_title'],
													mysql_real_escape_string($obj['std_fname']),
													mysql_real_escape_string($obj['std_lname']),
													'',
													$obj['std_email'],
													$charge_acc_id,
													$contact_id,
													$pax_id,
													$obj['job_type'],
													$obj['charge_mode'],
													$obj['under_18'],
													'',
													'0',
													$conf_to_client,
													$conf_to_pax
												);
												
// ADD REFERENCE LOG.
$job_ref->addJobReferenceLog($job_reference_id, $obj['user_id'], 'New Job Reference Created', $job_reference_id, 'NIL');

// IF ON BEHALF IS THERE.

// IF STUDENT GUARDIAN IS THERE.
if( $obj['under_18'] == '1' )
{
	$job_ref->updateJobReferenceTable($job_reference_id, 'under_18', '1');
	$job_ref->updateJobReferenceTable($job_reference_id, 'std_guar_title', $obj['std_guar_title']);
	$job_ref->updateJobReferenceTable($job_reference_id, 'std_guar_fname', $obj['std_guar_fname']);
	$job_ref->updateJobReferenceTable($job_reference_id, 'std_guar_lname', $obj['std_guar_lname']);
	$job_ref->updateJobReferenceTable($job_reference_id, 'std_guar_email', $obj['std_guar_email']);
	$job_ref->updateJobReferenceTable($job_reference_id, 'std_guar_phone', $obj['std_guar_phone']);
	$job_ref->updateJobReferenceTable($job_reference_id, 'under_18_email', $obj['under_18_email']);
}
// IF THERE IS AN AGENT.
if( $obj['has_agent'] == '1' )
{
	$job_ref->updateJobReferenceTable($job_reference_id, 'has_agent', '1');
	$job_ref->updateJobReferenceTable($job_reference_id, 'agents_name', $obj['agents_name']);
	$job_ref->updateJobReferenceTable($job_reference_id, 'agents_email', $obj['agents_email']);
}
if( $obj['has_destination_contact'] == '1' )
{
	$job_ref->updateJobReferenceTable($job_reference_id, 'has_destination_contact', '1');
	$job_ref->updateJobReferenceTable($job_reference_id, 'destination_contact_name', $obj['destination_contact_name']);
	$job_ref->updateJobReferenceTable($job_reference_id, 'destination_contact_phone', $obj['destination_contact_phone']);
	$job_ref->updateJobReferenceTable($job_reference_id, 'destination_contact_email', $obj['destination_contact_email']);
}	
// IF HOMESTAY, ADD THIS VALUE TO HOMESTAY.
if( $obj['is_homestay'] == '1' )
{
	$job_ref->updateJobReferenceTable($job_reference_id, 'is_homestay', '1');
	$job_ref->updateJobReferenceTable($job_reference_id, 'homestay_email', $obj['homestay_email']);
}

// ADD THE VALUES TO JOB TABLE.
$j1_id = $job->addNewJob
						(
							$job_reference_id,
							$job_status_id,
							$obj['j1_date_cap'],
							$obj['j1_time'],
							mysql_real_escape_string($obj['j1_flight_no']),
							mysql_real_escape_string($obj['j1_line_1']),
							mysql_real_escape_string($obj['j1_line_2']),
							mysql_real_escape_string($obj['j1_sub']),
							mysql_real_escape_string($obj['j1_pc']),
							$from_state,
							mysql_real_escape_string($obj['j1_to_flight_no']),
							mysql_real_escape_string($obj['j1_to_line_1']),
							mysql_real_escape_string($obj['j1_to_line_2']),
							mysql_real_escape_string($obj['j1_to_sub']),
							mysql_real_escape_string($obj['j1_to_pc']),
							$to_state,
							mysql_real_escape_string($obj['j1_via_line_1']),
							mysql_real_escape_string($obj['j1_via_line_2']),
							mysql_real_escape_string($obj['j1_via_sub']),
							mysql_real_escape_string($obj['j1_via_pc']),
							$from_via_state,
							mysql_real_escape_string($obj['j1_to_via_line_1']),
							mysql_real_escape_string($obj['j1_to_via_line_2']),
							mysql_real_escape_string($obj['j1_to_via_sub']),
							mysql_real_escape_string($obj['j1_to_via_pc']),
							$to_via_state,
							$obj['j1_car_id'],
							$obj['j1_pax_nos'],
							$obj['j1_luggage'],
							$obj['j1_baby_seats'],
							$obj['j1_booster_seats'],
							$obj['j1_baby_capsules'],
							'',											// this is driver_status_field (leave blank)
							$obj['j1_kms'],
							$obj['j1_fare'],
							$obj['j1_inter'],
							$obj['j1_ed'],
							$obj['j1_wait'],
							$obj['j1_tolls'],
							$obj['j1_bs'],
							$obj['j1_park'],
							$obj['j1_ah'],
							$obj['j1_me'],
							$obj['j1_alc'],
							$obj['j1_fc'],
							$obj['j1_oth'],
							$obj['j1_tot_fare'],
							$obj['j1_drv_fee'],
							$obj['j1_oth_exp'],
							$obj['j1_profit'],
							mysql_real_escape_string($obj['j1_ext_notes']),
							mysql_real_escape_string($obj['j1_int_notes']),
							mysql_real_escape_string($obj['j1_driver_notes']),
							$obj['j1_lat'],
							$obj['j1_long']
						);
if($j1_id != '')
{
	$return_message .= "<b>Your Booking has been successful</b><br/><br/>";
	$return_message .= "Following bookings has been generated<br/><br/>";
	$return_message .= "<b>BOOKING ID -  ".$j1_id."</b><br/>";
}
$push_message = 'A new job (Job Id : '.$j1_id.') has been created.';
$push_title = 'New job '.$j1_id;
$job->commonGooglePushNotification($obj['user_id'],$push_message,$push_title);
$query56 = "SELECT * from user where role_id='1'";
		$result56 = $database->query($query56);
		while($row9 = mysql_fetch_array($result56))
		{
			if($row9['device_token']!='' || $row9['device_token']!=NULL)
			{
				$push_title = 'New job '.$j1_id;
				$push_message = 'A new job (Job Id : '.$j1_id.') has been created.';
				$job->commonGooglePushNotification($row9['id'],$push_message,$push_title);
			}
		}
						
// ADD THE VALUES TO JOB LOGS TABLES.
$job->addJobLog($j1_id, $obj['user_id'], 'New job created', $j1_id, '');

// ADD THIS IN JOB REFERENCE TABLE.
$job_ref->updateJobReferenceTable($job_reference_id, 'j1_id', $j1_id);

// ADD THIS IN JOB REFERENCE LOG TABLE.
$job_ref->addJobReferenceLog($job_reference_id, $obj['user_id'], 'Job 1 Added', $j1_id, 'NIL');

// ALLOCATE, SEND OF ACCEPT DRIVER.
if( $obj['j1_driver_status'] == '1' && $obj['j1_driver'] !='' )  // allocate
{
	$job->updateJob($j1_id, 'driver_status', '1');  // change the driver status to Allocate
	$job->addNewDriverToJob($j1_id, $obj['j1_driver'], $obj['j1_driver_price'], '', '', '', '', '', '', '', '', '','');  // add this to job__driver table
	$driver_details = $user->getUserDetails($obj['j1_driver']);  // get details of this driver
	$job->addJobDriverLog($j1_id, $obj['user_id'], 'Job Allocated', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$obj['j1_driver_price'].'', '');
	
	$push_message = 'Job Id : '.$j1_id.' has been allocated to you.';
	$push_title = 'New job '.$j1_id;
	$job->commonGooglePushNotification($obj['j1_driver'],$push_message,$push_title);
	
}

// FOR DRIVER/S OFFER.
if( $obj['j1_driver_status'] == '2' && !empty($obj['j1_driver']) && $obj['j1_driver_price'] !='' )  // send
{
	$mailer = new mailer();
	$job->updateJob($j1_id, 'driver_status', '2');  // change the driver status to Sent
	
		$driver_details = $user->getUserDetails($obj['j1_driver']);  // get details of this driver
		$job->addNewDriverToJob($j1_id, '', '', $obj['j1_driver'], $obj['j1_driver_price'], '', '', '', '', '', '', '','');  // add this to job__driver table
		$job->addJobDriverLog($j1_id, $obj['user_id'], 'Job Offered', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$obj['j1_driver_price'].'', '');
		$mailer->sendJobOfferToDriver($obj['j1_driver'], $j1_id, $obj['j1_driver_price'],$obj['j1_driver_notes']);  // send job offer to the driver
		$push_title = 'New job '.$j1_id;
		$push_message = 'Job Id : '.$j1_id.' has been offered to you.';
		$job->commonGooglePushNotification($obj['j1_driver'],$push_message,$push_title);
}

// FOR GROUP DRIVERS OFFER.
if( $obj['j1_driver_status'] == '2' && !empty($obj['j1_driver_group']) && $obj['j1_driver_group'] !='' )  // send
{
	$mailer = new mailer();
	$job->updateJob($j1_id, 'driver_status', '2');  // change the driver status to Sent
	
	$query = "SELECT * from drivers__group_drivers where group_id='".$obj['j1_driver_group']."'";
	$result = $database->query($query);
	while($row = mysql_fetch_array($result))
	{
		$driver_details = $user->getUserDetails($row['user_id']);  // get details of this driver
		$job->addNewDriverToJob($j1_id, '', '', $row['user_id'], $obj['j1_driver_price'], '', '', '', '', '', '', '','');  // add this to job__driver table
		$job->addJobDriverLog($j1_id, $obj['user_id'], 'Job Offered', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$obj['j1_driver_price'].'', '');
		$mailer->sendJobOfferToDriver($row['user_id'], $j1_id, $obj['j1_driver_price'],$obj['j1_driver_notes']);  // send job offer to the driver
		$push_title = 'New job '.$j1_id;
		$push_message = 'Job Id : '.$j1_id.' has been offered to you.';
		$job->commonGooglePushNotification($row['user_id'],$push_message,$push_title);
	}
}
if(($obj['j1_driver_status'] == '3' || $obj['j1_driver_status'] == '4' || $obj['j1_driver_status'] == '5') && !empty($obj['j1_driver']) && $obj['j1_driver_price'] !='' )  // accepted
{
	$mailer = new mailer();
	$job->updateJob($j1_id, 'driver_status',$obj['j1_driver_status']);  // change the driver status to Sent
	$job->addNewDriverToJob($j1_id, '', '', '', '', $obj['j1_driver'], $obj['j1_driver_price'], '', '', '', '', '','');  // add this to job__driver table
	$q1=$database->query("UPDATE job SET driver_id='".$obj['j1_driver']."' WHERE id='".$j1_id."'");
	$driver_details = $user->getUserDetails($obj['j1_driver']);  // get details of this driver
	$job->addJobDriverLog($j1_id, $obj['user_id'], 'Job Accepted', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$obj['j1_driver_price'].'', '');
	$mailer->sendJobDetailsToDriver($j1_id, $obj['j1_driver'], $obj['j1_driver_price'],$obj['j1_driver_notes']);  // send job acceptance email with job details to the driver
}

// UPDATE ADDRESSES.
$add_id_1 = $address->checkDuplicateOrEnter($pax_id, $obj['j1_line_1'], $obj['j1_line_2'], $obj['j1_sub'], $obj['j1_pc'], $obj['j1_state']);
$add_id_2 = $address->checkDuplicateOrEnter($pax_id, $obj['j1_to_line_1'], $obj['j1_to_line_2'], $obj['j1_to_sub'], $obj['j1_to_pc'], $obj['j1_state']);
$add_id_3 = $address->checkDuplicateOrEnter($pax_id, $obj['j1_via_line_1'], $obj['j1_via_line_2'], $obj['j1_via_sub'], $obj['j1_via_pc'], $obj['j1_state']);
$add_id_4 = $address->checkDuplicateOrEnter($pax_id, $obj['j1_to_via_line_1'], $obj['j1_to_via_line_2'], $obj['j1_to_via_sub'], $obj['j1_to_via_pc'], $obj['j1_state']);

if( $obj['job_type'] == '2' )
{
	$from_state = $address->getStateName($obj['j2_state']);
	$to_state = $from_state;
	if( $obj['j1_via_sub'] != '' )  // if suburb not blank
	{
		$from_via_state = $from_state;
	}
	if( $obj['j1_to_via_sub'] != '' )  // if suburb not blank
	{
		$to_via_state = $from_state;
	}
	$j2_id = $job->addNewJob
							(
								$job_reference_id,
								$job_status_id,
								$obj['j2_date_cap'],
								$obj['j2_time'],
								mysql_real_escape_string($obj['j2_flight_no']),
								mysql_real_escape_string($obj['j2_line_1']),
								mysql_real_escape_string($obj['j2_line_2']),
								mysql_real_escape_string($obj['j2_sub']),
								mysql_real_escape_string($obj['j2_pc']),
								$from_state,
								mysql_real_escape_string($obj['j2_to_flight_no']),
								mysql_real_escape_string($obj['j2_to_line_1']),
								mysql_real_escape_string($obj['j2_to_line_2']),
								mysql_real_escape_string($obj['j2_to_sub']),
								mysql_real_escape_string($obj['j2_to_pc']),
								$to_state,
								mysql_real_escape_string($obj['j2_via_line_1']),
								mysql_real_escape_string($obj['j2_via_line_2']),
								mysql_real_escape_string($obj['j2_via_sub']),
								mysql_real_escape_string($obj['j2_via_pc']),
								$from_via_state,
								mysql_real_escape_string($obj['j2_to_via_line_1']),
								mysql_real_escape_string($obj['j2_to_via_line_2']),
								mysql_real_escape_string($obj['j2_to_via_sub']),
								mysql_real_escape_string($obj['j2_to_via_pc']),
								$to_via_state,
								$obj['j2_car_id'],
								$obj['j2_pax_nos'],
								$obj['j2_luggage'],
								$obj['j2_baby_seats'],
								$obj['j2_booster_seats'],
								$obj['j2_baby_capsules'],
								'',											//this is driver_status_field (leave blank)
								$obj['j2_kms'],
								$obj['j2_fare'],
								$obj['j2_inter'],
								$obj['j2_ed'],
								$obj['j2_wait'],
								$obj['j2_tolls'],
								$obj['j2_bs'],
								$obj['j2_park'],
								$obj['j2_ah'],
								$obj['j2_me'],
								$obj['j2_alc'],
								$obj['j2_fc'],
								$obj['j2_oth'],
								$obj['j2_tot_fare'],
								$obj['j2_drv_fee'],
								$obj['j2_oth_exp'],
								$obj['j2_profit'],
								mysql_real_escape_string($obj['j2_ext_notes']),
								mysql_real_escape_string($obj['j2_int_notes']),
								mysql_real_escape_string($obj['j2_driver_notes']),
								$obj['j2_lat'],
								$obj['j2_long']
							);
	if($j2_id != '')
	{
		$return_message .= "<b>BOOKING ID -  ".$j2_id."</b><br/>";
	}
	$push_message = 'A new job (Job Id : '.$j2_id.') has been created.';
	$push_title = 'New job '.$j2_id;
	$job->commonGooglePushNotification($obj['user_id'],$push_message,$push_title);
	$query = "SELECT * from user where role_id='1'";
		$result = $database->query($query);
		while($row = mysql_fetch_array($result))
		{
			if($row['device_token']!='' || $row['device_token']!=NULL)
			{
				$push_message = 'A new job (Job Id : '.$j2_id.') has been created.';
				$job->commonGooglePushNotification($row['id'],$push_message);
			}
		}
	
	
	// ADD IN JOB LOG TABLE.
	$job->addJobLog($j2_id, $obj['user_id'], 'New job created', $j2_id, '');
	$job_ref->updateJobReferenceTable($job_reference_id, 'j2_id', $j2_id);
	// ADD IN JOB REFERENCE LOG TABLE.
	$job_ref->addJobReferenceLog($job_reference_id, $obj['user_id'], 'Job 2 Added', $j2_id, 'NIL');
	
	// ALLOCATE, SEND OF ACCEPT DRIVER.
	if( $obj['j2_driver_status'] == '1' && $obj['j2_driver'] !='' )  // allocate
	{
		$job->updateJob($j2_id, 'driver_status', '1');  // change the driver status to Allocate
		$job->updateJob($j2_id, 'driver_id', $obj['j2_driver']);  // put this driver in job table
		$job->updateJob($j2_id, 'driver_price', $obj['j2_driver_price']);  // put the driver price in the job table
		$job->addNewDriverToJob($j2_id, $obj['j2_driver'], $obj['j2_driver_price'], '', '', '', '', '', '', '', '', '','');  // add this to job__driver table
		$driver_details = $user->getUserDetails($obj['j2_driver']);  // get details of this driver
		$job->addJobDriverLog($j2_id, $obj['user_id'], 'Job Allocated', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$obj['j2_driver_price'].'', '');
		
		$push_message = 'Job Id : '.$j2_id.' has been allocated to you.';
		$push_title = 'Job '.$j2_id;
		$job->commonGooglePushNotification($obj['j2_driver'],$push_message);
	}
	if( $obj['j2_driver_status'] == '2' && !empty($obj['j2_driver']) && $obj['j2_driver_price'] !='' )  // send/offer
	{
		$mailer = new mailer();
		$job->updateJob($j2_id, 'driver_status', '2');  // change the driver status to Sent
		
			$user->getUserDetails($obj['j2_driver']);  // get details of this driver
			$job->addNewDriverToJob($j2_id, '', '', $obj['j2_driver'], $obj['j2_driver_price'], '', '', '', '', '', '', '','');  // add this to job__driver table
			$driver_details = $user->getUserDetails($obj['j2_driver']); // get details of this driver
			$job->addJobDriverLog($j2_id, $obj['user_id'], 'Job Offered', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$obj['j2_driver_price'].'', '');
			$mailer->sendJobOfferToDriver($obj['j2_driver'], $j2_id, $obj['j2_driver_price'],$obj['j2_driver_notes']);  // send job offer to the driver
		$push_title = 'Job '.$j2_id;
		$push_message = 'Job Id : '.$j2_id.' has been offered to you.';
		$job->commonGooglePushNotification($obj['j2_driver'],$push_message,$push_title);
	}
	
	// FOR GROUP DRIVER OFFER.	
	if( $obj['j2_driver_status'] == '2' && !empty($obj['j2_driver_group']) && $obj['j2_driver_group'] !='' )  // send
	{
		$mailer = new mailer();
		$job->updateJob($j2_id, 'driver_status', '2');  // change the driver status to sent
		
		$query = "SELECT * from drivers__group_drivers where group_id='".$obj['j2_driver_group']."'";
		$result = $database->query($query);
		while($row = mysql_fetch_array($result))
		{
			$user->getUserDetails($row['user_id']);  // get details of this driver
			$job->addNewDriverToJob($j2_id, '', '', $row['user_id'], $obj['j2_driver_price'], '', '', '', '', '', '', '','');  // add this to job__driver table
			$driver_details = $user->getUserDetails($row['user_id']);  // get details of this driver
			$job->addJobDriverLog($j2_id, $obj['user_id'], 'Job Offered', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$obj['j2_driver_price'].'', '');
			$mailer->sendJobOfferToDriver($row['user_id'], $j2_id, $obj['j2_driver_price'],$obj['j2_driver_notes']);  // send job offer to the driver
			if($driver_details['device_token']!='' || $driver_details['device_token']!=NULL)
			{
				$push_title = 'Job '.$j2_id;
				$push_message = 'Job Id : '.$j2_id.' has been offered to you.';
				$job->commonGooglePushNotification($row['user_id'],$push_message,$push_title);
			}
		}
	}
	if(($obj['j2_driver_status'] == '3' || $obj['j2_driver_status'] == '4' || $obj['j2_driver_status'] == '5') && !empty($obj['j2_driver']) && $obj['j2_driver_price'] !='' )  // accepted
	{
		$mailer = new mailer();
		$job->updateJob($j2_id, 'driver_status', $obj['j2_driver_status']);  // change the driver status to Sent/Accepted
		$job->updateJob($j2_id, 'driver_id', $obj['j2_driver']);  // put this driver in job table
		$job->updateJob($j2_id, 'driver_price', $obj['j2_driver_price']);  // put the driver price in the job table
		$job->addNewDriverToJob($j2_id, '', '', '', '', $obj['j2_driver'], $obj['j2_driver_price'], '', '', '', '', '','');  // add this to job__driver table
		$q1=$database->query("UPDATE job SET driver_id='".$obj['j2_driver']."' WHERE id='".$j2_id."'");
		$driver_details = $user->getUserDetails($obj['j2_driver']);  // get details of this driver
		$job->addJobDriverLog($j2_id, $obj['user_id'], 'Job Accepted', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$obj['j2_driver_price'].'', '');
		$mailer->sendJobDetailsToDriver($j2_id, $obj['j2_driver'], $obj['j2_driver_price'],$obj['j2_driver_notes']);  // send job acceptance email with job details to the driver
	}
	
	// UPDATE ADDRESSES
	$add_id_5 = $address->checkDuplicateOrEnter($pax_id, $obj['j2_line_1'], $obj['j2_line_2'], $obj['j2_sub'], $obj['j2_pc'], $obj['j2_state']);
	$add_id_6 = $address->checkDuplicateOrEnter($pax_id, $obj['j2_to_line_1'], $obj['j2_to_line_2'], $obj['j2_to_sub'], $obj['j2_to_pc'], $obj['j2_state']);
	$add_id_7 = $address->checkDuplicateOrEnter($pax_id, $obj['j2_via_line_1'], $obj['j2_via_line_2'], $obj['j2_via_sub'], $obj['j2_via_pc'], $obj['j2_state']);
	$add_id_8 = $address->checkDuplicateOrEnter($pax_id, $obj['j2_to_via_line_1'], $obj['j2_to_via_line_2'], $obj['j2_to_via_sub'], $obj['j2_to_via_pc'], $obj['j2_state']);
}

// SEND MAIL TO bkg_by_id WHEN bkg_src IS WEBSITE.
// IF BOOKING WAS MADE BY ADMIN STAFF, SEND EMAILS TO CLIENTS AS BOOKING CONFIRMED.
if( $obj['role_id'] == '1' )  // admin
{
	$mailer = new Mailer();
	$job_ref_details = $job_ref->getJobReferenceDetails($job_reference_id);
	if( $job_ref_details['acc_type'] == '3' )  // if this is booked by university or student
	{
		$mailer->sendUniversityMail($job_reference_id, '6');
	}
	else  // booking made for other types of accounts
	{
		if( $contact_id == $pax_id )  // send this booking to 
		{
			if( $obj['conf_to_pax'] == '1' || $obj['conf_to_bkg_made_by'] == '1' )  // send this booking pax
			{
				$user = new User();
				$user_details = $user->getUserDetails($pax_id);
				if( $user_details['email'] != '' )
				{
					$mailer->sendMailToClientOnBooking($job_reference_id, '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
					$additional_message .= 'Booking confirmation sent to passenger.<br/>';
				}
			else
				{
					$additional_message .= 'Passenger Email - Not Found. Email not sent to passenger.<br/>';
				}
			}
		}
		else
		{
			if( $obj['conf_to_bkg_made_by'] == '1' )  // send this booking to booking made corporates only and not universities
			{
				$user = new User();
				$user_details = $user->getUserDetails($contact_id);
				if( $user_details['email'] != '' )
				{
					$mailer->sendMailToClientOnBooking($job_reference_id, '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
					$additional_message .= 'Booking confirmation sent to client.<br/>';
				}
			else
				{
					$additional_message .= 'Booking by Email - Not Found. Email not sent to booking made by.<br/>';
				}
			}
			if( $obj['conf_to_pax'] == '1' )  // send this booking pax
			{
				$user = new User();
				$user_details = $user->getUserDetails($pax_id);
				if( $user_details['email'] != '' )
				{
					$mailer->sendMailToClientOnBooking($job_reference_id, '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
				}
			}
		}
		if( $job_ref_details['agent_email'] != '' )  // send an email to the agent
		{
			$mailer->sendMailToClientOnBooking($job_reference_id, '1', $job_ref_details['agent_email'], 'Agent', '','');
		}
	}
}

// IF BOOKING WAS MADE BY CUSTOMER SEND EMAIL AS RECEIVED AND WAITING FOR CONFIRMATION
if( $obj['role_id'] == '2' )  // driver
{
	$mailer = new Mailer();
	if( $obj['conf_to_bkg_made_by'] == '1' )  // send this booking to booking made by
	{
		$user = new User();
		$user_details = $user->getUserDetails($contact_id);
		if( $user_details['email'] != '' )
		{
			$mailer->sendMailToClientOnBooking($job_reference_id, '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
		}
	}
	if( $obj['conf_to_pax'] == '1' )  // send this booking pax
	{
		$user = new User();
		$user_details = $user->getUserDetails($pax_id);
		if( $user_details['email'] != '' )
		{
			$mailer->sendMailToClientOnBooking($job_reference_id, '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
		}
	}
	$mailer->sendMailToAdminOnBooking($job_reference_id);
}

if( $obj['role_id'] == '3' )  // charge account making the booking using BMS
{
	$mailer = new Mailer();
	$user = new User();
	$user_details = $user->getUserDetails($contact_id);
	$job_ref_details = $job_ref->getJobReferenceDetails($job_reference_id);
	if( $job_ref_details['acc_type'] == '3' )  // if this is booked by university or student
	{
		$mailer->sendUniversityMail($job_reference_id, '2');
	}
	else
	{
		if( $user_details['email'] != '' )
		{
			$mailer->sendMailToClientOnBooking($job_reference_id, '2', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
		}
		
		if( $pax_id != $contact_id )  // so that one email is not sent to same email id twice, incase pax and contact are same.
		{
			$user_details = $user->getUserDetails($pax_id); 
			if( $user_details['email'] != '' )
			{
				$mailer->sendMailToClientOnBooking($job_reference_id, '2', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
			}
		}
		$mailer->sendMailToAdminOnBooking($job_reference_id);
	}
}
if($obj['role_id'] == '1')
{
	$message = ''.$return_message.'<br/>'.$additional_message.'';
}
else if($obj['role_id'] == '2' || $obj['role_id'] == '3')
{
	$user = new User();
	$user_details = $user->getUserDetails($contact_id);
	$booking_txt = ($j2_id != '')? $j1_id.' & '.$j2_id : $j1_id;  
	$html_message = "<div class='outer_box'>
		<div class='outer_inner_box'>
		Dear \"".$user_details['fname']."\".<br /><br />
		Thank you for your Online Booking.<br /><br />
		This is to let you know, that our Online Booking system has received your Booking.<br /><br /> 
		This booking ID ".$booking_txt." is ONLY VALID IF booked according to our terms and conditions as listed below:<br /><br /><br /> 
		TERMS & CONDITIONS FOR THIS ONLINE BOOKING<br /><br /> 
		1... During office hours, this booking MUST be booked with a 3 hour lead* time.<br /><br /> 
		2... Outside office hours, this booking MUST be booked with a 14 hour lead* time.<br /><br /> 
		3... Weekend and public holidays, this booking MUST be booked with a 36 hour lead* time.<br /><br />
		* \"Lead Time\" is the time before the booking time.<br /><br /> 
		New bookings or bookings cancellations are valid only on receipt of our confirmation.<br /><br /><br />
		Our service is 24 hours a day,<br />
		Our office hours are<br />
		Monday to Friday 8am to 7pm<br />
		Saturday 1pm to 5pm<br />
		Sunday   1pm to 8pm<br />
		Please also be aware that our office closes on public holidays<br /><br />
		Kind regards,<br />
		ALLIED CHAUFFEURED CARS AUSTRALIA<br />
		ABN: 38 076 977 136 <br />
		</div>
		<div class=\"footer_div\">
		Copyright &copy; 2011 Allied Chauffeured Cars. Ltd. All Rights Reserved. 
		</div>
		</div>";
	$message = $html_message;
}
$a = array('data1' => 'OK', 'data2' => ''.$j1_id.'','data3' => ''.$message.'');
$json = json_encode($a); 
echo $json;