<?php
// INCLUDE THE REQUIRED FILES.
require_once('../init.php');
include(''.INCLUDE_PATH.'config.php');
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "job.php");
require_once(INCLUDE_PATH . "functions.php");
// ADD HEADERS FOR THE WEBSERVICE.
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: POST, GET');
header('Content-type: application/json');
header('Access-Control-Allow-Headers:Content-Type');
// INITIALIZE DIFFERENT CLASS OBJECTS.
$database = new Database;
// RECEIVE THE DATA IN JSON FORMAT.
$inputJSON = file_get_contents('php://input');
$obj = json_decode($inputJSON,TRUE);
//echo $inputJSON; exit;
//echo json_encode(date('Y-m-d'));exit;
// ---------- BOOKING LISTING STARTS HERE. ---------- //


if( $obj["listing_type"] == 'today' )
{
	$date_from = date('Y-m-d');//'2016-07-07';//date('Y-m-d');
	$date_to = date('Y-m-d');//'2016-07-07';//date('Y-m-d');
	$date_range_display = $date_from;
}
if( $obj["listing_type"] == 'prev' )
{
	$date_from = date('Y-m-d',strtotime($obj["date_to"] . "-1 days"));
	$date_to = $date_from;
	$date_range_display = $date_from;
	//echo $date_from;
	//exit;
}
if( $obj["listing_type"] == 'next' )
{
	$date_to = date('Y-m-d',strtotime($obj["date_from"] . "+1 days"));
	$date_from = $date_to;
	$date_range_display = $date_to;
	//echo $date_from;
	//exit;
}
if( $obj["listing_type"] == 'relode' )
{
	$date_to = date('Y-m-d',strtotime($obj["date_from"]));
	$date_from = $date_to;
	$date_range_display = $date_to;
	//echo $date_from;
	//exit;
}
if( $obj["listing_type"] == 'range' )
{
	$date_from = $obj["date_from"];
	$date_to = $obj["date_to"];
	$date_range_display = $date_from." - ".$date_to;
}
$where .= "WHERE 1 ";
if( $date_from != '' && $date_to != '' )
{
	$where .= " AND job.job_date between '".$date_from."' AND '".$date_to."' ";
}
else
{
	$where .= " AND job.job_date between '1970-01-01' AND '2050-01-01' ";
}
if( $obj['role_id'] == '3' )
{
	$query1 = "SELECT * from charge_acc__contacts where user_id = '".$obj['user_id']."'";
	$result1 = $database->query($query1);
	$row1 = mysql_fetch_array($result1);
	$charge_acc_id = $row1['charge_acc_id'];
	$where .= " AND job__reference.charge_acc_id = '".$charge_acc_id."' ";
}
if( $obj['role_id'] == '2' )
{
	if( $obj["action"] == "list_all_bookings" )
	{
		$where .= " AND job.driver_id = '".$obj['user_id']."' AND job.driver_status in ('3','4','5')  ";
		//$where .= " AND job.driver_id = '".$obj['user_id']."' ";
	}
	if( $obj["action"] == "list_bookings_offered_to_driver" )
	{
		$where .= " AND job.driver_status in ('2') AND job__driver.offered_to = '".$obj['user_id']."'";
	}
}

$query = "
SELECT
*,
job.id as job_id,
job__reference.entered_by as entered_by,
CONCAT(a.fname) as entry_by,
CONCAT(b.fname,'<br/>',b.phone) as bkg_by,";

if( $obj['role_id'] == '2' )
{
	$query .= "
	CONCAT(c.title, ' ', c.fname, ' ', c.lname, '<br/>', c.mobile) as pax,"; 
}
else
{
	$query .= "
	CONCAT(c.title, ' ', c.fname, ' ', c.lname,'<br/>', c.mobile) as pax,";
}

$query .=  "
			CONCAT(d.fname,'<br/>',d.mobile) as driver,
			CONCAT(job__reference.std_title, ' ', job__reference.std_fname, ' ', job__reference.std_lname) as student_name,
			CONCAT(job.frm_line1,' ',job.frm_line2,'<br/><b>',job.frm_sub,'</b>') as from_address,
			CONCAT(job.frm_via_line1,' ',job.frm_via_line2,'<br/><b>',job.frm_via_sub,'</b>') as from_via_address,
			CONCAT(job.to_flight_no,' ',job.to_line1,' ',job.to_line2,'<br/><b>',job.to_sub,'</b>') as to_address,
			CONCAT(job.to_via_line1,' ',job.to_via_line2,'<br/><b>',job.to_via_sub,'</b>') as to_via_address,
			CONCAT('<b>',TIME_FORMAT(job.job_time,'%H:%i'),'</b><br/>',DATE_FORMAT(job.job_date,'%d/%m/%Y')) as date_time,
			CONCAT(DATE_FORMAT(job.job_date,'%d-%m-%Y'),' ',TIME_FORMAT(job.job_time,'%h:%i %p')) as date_time_for_show,
			variable__car_type.details as vehicle_type,
			drivers__movement_status.movement_status as driver_movement_status,
			job__status_ids.details as current_booking_status,
			job__driver_status_ids.details as driver_status,
			job__reference.std_title as std_title,
			job__reference.std_fname as std_fname,
			job__reference.std_lname as std_lname,
			job__reference.under_18 as under_18,
			job__reference.acc_type as acc_type,
			job.driver_notes as driver_notes, 
			job.driver_id as driver_id, 
			job__driver.accepted_amount as driver_price,
			job__driver_extra_pay.amount as extra_pay,
			(if(job__driver_extra_pay.amount!= null || job__driver_extra_pay.amount!= '' ,job__driver_extra_pay.amount, 0) + job__driver.accepted_amount) as driver_fee
			from 
			job
			LEFT JOIN drivers__movement_status ON job.driver_movement_status = drivers__movement_status.id
			LEFT JOIN job__reference ON job.job_reference_id = job__reference.id
			LEFT JOIN job__status_ids ON job.job_status = job__status_ids.id
			LEFT JOIN variable__car_type ON job.car_id = variable__car_type.id
			LEFT JOIN job__driver_status_ids ON job.driver_status = job__driver_status_ids.id
			LEFT JOIN user AS a ON job__reference.entered_by = a.id
			LEFT JOIN user AS b ON job__reference.bkg_by_id = b.id
			LEFT JOIN user AS c ON job__reference.pax_id = c.id
			LEFT JOIN user AS d ON job.driver_id = d.id
			LEFT JOIN charge_acc ON job__reference.charge_acc_id = charge_acc.id
			LEFT JOIN job__driver ON job.id = job__driver.job_id AND job__driver.id=(SELECT MAX(id) FROM job__driver WHERE job_id = job.id)  
			LEFT JOIN job__driver_extra_pay ON job.id = job__driver_extra_pay.job_id 
			".$where;//."
			//ORDER BY ".$obj["jtSorting"]."  LIMIT ".$obj["jtStartIndex"].", ".$obj["jtPageSize"]."";
			
$query1 = "
			SELECT
			*,
			job.id as job_id,
			job__reference.entered_by as entered_by,
			CONCAT(a.fname) as entry_by,
			CONCAT(b.fname,'(',b.phone,')') as bkg_by,";

if($obj['role_id'] == '2' )
{
	$query1 .= "
	CONCAT(c.title, ' ', c.fname, ' ', c.lname) as pax,"; 
}
else
{
	$query1 .= "
	CONCAT(c.title, ' ', c.fname, ' ', c.lname,'(', c.mobile,')') as pax,";
}

$query1 .= "
			CONCAT(d.fname,'(',d.mobile,')') as driver,
			CONCAT(job__reference.std_title, ' ', job__reference.std_fname, ' ', job__reference.std_lname) as student_name,
			CONCAT(job.frm_line1,' ',job.frm_line2,' ',job.frm_sub) as from_address,
			CONCAT(job.frm_via_line1,' ',job.frm_via_line2,' ',job.frm_via_sub) as from_via_address,
			CONCAT(job.to_flight_no,' ',job.to_line1,' ',job.to_line2,' ',job.to_sub) as to_address,
			CONCAT(job.to_via_line1,' ',job.to_via_line2,' ',job.to_via_sub) as to_via_address,
			CONCAT('@',TIME_FORMAT(job.job_time,'%H:%i'),' on ',DATE_FORMAT(job.job_date,'%d/%m/%Y')) as date_time,
			variable__car_type.details as vehicle_type,
			job__status_ids.details as current_booking_status,
			job__driver_status_ids.details as driver_status,
			job__reference.std_title as std_title,
			job__reference.std_fname as std_fname,
			job__reference.std_lname as std_lname,
			job__reference.under_18 as under_18,
			job__reference.acc_type as acc_type,
			job.driver_notes as driver_notes, 
			job.driver_id as driver_id,
			job__driver.accepted_amount as driver_price,
			job__driver_extra_pay.amount as extra_pay,
			(if(job__driver_extra_pay.amount!= null || job__driver_extra_pay.amount!= '' ,job__driver_extra_pay.amount, 0) + job__driver.accepted_amount) as driver_fee
			from 
			job
			LEFT JOIN job__reference ON job.job_reference_id = job__reference.id
			LEFT JOIN job__status_ids ON job.job_status = job__status_ids.id
			LEFT JOIN variable__car_type ON job.car_id = variable__car_type.id
			LEFT JOIN job__driver_status_ids ON job.driver_status = job__driver_status_ids.id
			LEFT JOIN user AS a ON job__reference.entered_by = a.id
			LEFT JOIN user AS b ON job__reference.bkg_by_id = b.id
			LEFT JOIN user AS c ON job__reference.pax_id = c.id
			LEFT JOIN user AS d ON job.driver_id = d.id
			LEFT JOIN charge_acc ON job__reference.charge_acc_id = charge_acc.id
			LEFT JOIN job__driver ON job.id = job__driver.job_id AND job__driver.id=(SELECT MAX(id) FROM job__driver WHERE job_id = job.id)  
			LEFT JOIN job__driver_extra_pay ON job.id = job__driver_extra_pay.job_id  
			".$where;//."
			//ORDER BY ".$obj["jtSorting"]."  LIMIT ".$obj["jtStartIndex"].", ".$obj["jtPageSize"]."";
			
$result = $database->query($query);
$total_rows = mysql_num_rows($result);
$rows = array();
while($row = mysql_fetch_array($result))
{
	$rows[] = $row;
}

$return_data['list_data'] = $rows;
$return_data['date_range_display'] = $date_range_display;
$return_data['date_from'] = $date_from;
$return_data['date_to'] = $date_to;
$return_data['query'] = $query1;
$json = json_encode($return_data); 
echo $json;