<?php
// INCLUDE THE REQUIRED FILES.
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(INCLUDE_PATH . "functions.php");
require_once(INCLUDE_PATH . "config.php");
// ADD HEADERS FOR THE WEBSERVICE.
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: POST, GET');
header('Content-type: application/json');
header('Access-Control-Allow-Headers:Content-Type');
// INITIALIZE DIFFERENT CLASS OBJECTS.
$database = new Database;
// RECEIVE THE DATA IN JSON FORMAT.
$inputJSON = file_get_contents('php://input');
$obj = json_decode($inputJSON,TRUE);

//Get record count
$query = "SELECT COUNT(*) AS RecordCount FROM user where role_id='2'";
$result = $database->query($query);
$row = mysql_fetch_array($result);
$recordCount = $row['RecordCount'];

$query = 	"SELECT
			user.id as id,
			DATE_FORMAT(user.created_on,'%d/%m/%Y') AS created_on,
			user.role_id as role_id,
			user.title as title,
			user.fname as fname,
			user.lname as lname,
			user.password as password,
			user.email as email,
			user.mobile as mobile,
			user.phone as phone,
			user.hidden as hidden, 
			drivers.id as driver_id,
			drivers.user_id as user_id,
			drivers.bus_name as bus_name,
			drivers.dc as dc,
			drivers.dc_exp as dc_exp,
			drivers.lic as lic,
			drivers.lic_exp as lic_exp,
			drivers.abn as abn,
			drivers.notes as notes,
			drivers.acc_bal as acc_bal
			FROM user
			LEFT JOIN drivers ON user.id = drivers.user_id
			where user.role_id = '2'
			ORDER BY
			user.fname ASC ";
$result = $database->query($query);
$rows = array();
while($row = mysql_fetch_array($result))
{
	$rows[] = $row;
}

$jTableResult = array();
$jTableResult['Result'] = "OK";
$jTableResult['TotalRecordCount'] = $recordCount;
$jTableResult['Records'] = $rows;
print json_encode($jTableResult);