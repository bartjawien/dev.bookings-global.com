<?php
// INCLUDE THE REQUIRED FILES.
require_once('../init.php');
include(''.INCLUDE_PATH.'config.php');
require_once(CLASSES_PATH . "database.php");
// ADD HEADERS FOR THE WEBSERVICE.
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: POST, GET');
header('Content-type: application/json');
header('Access-Control-Allow-Headers:Content-Type');
// INITIALIZE DIFFERENT CLASS OBJECTS.
$database = new Database;
// RECEIVE THE DATA IN JSON FORMAT.
$inputJSON = file_get_contents('php://input');
$obj = json_decode($inputJSON,TRUE);

if( $obj['action'] == 'get_all_driver_lat_longs' )
{
	$sql = "SELECT a.*, b.* FROM user as a, drivers b WHERE a.role_id='2' AND a.id=b.user_id AND a.user_lat!='' AND a.user_lng!=''";
	$result = $database->query($sql);
	$total_rows = mysql_num_rows($result);
	
	$sql1 = "SELECT * FROM user WHERE id=".$obj['user_id'];
	$result1 = $database->query($sql1);
	$row1 = mysql_fetch_array($result1);

	$rows = array();
	while($row = mysql_fetch_array($result))
	{
		$rows[] = $row;
	}
	
	$return_data = array();
	$return_data['record_count'] = $total_rows;
	$return_data['user_lat'] = $row1['user_lat'];
	$return_data['user_lng'] = $row1['user_lng'];
	$return_data['records'] = $rows;
	
	echo json_encode($return_data);
}
if( $obj['action'] == 'get_driver_lat_longs_for_job' )
{
	$sql = "SELECT a.*, b.* FROM user as a, drivers b WHERE a.role_id='2' AND a.id=b.user_id AND a.user_lat!='' AND a.user_lng!='' AND a.id=".$obj['driver_id'];
	$result = $database->query($sql);
	$total_rows = mysql_num_rows($result);
	
	$sql1 = "SELECT * FROM user WHERE id=".$obj['user_id'];
	$result1 = $database->query($sql1);
	$row1 = mysql_fetch_array($result1);

	$rows = array();
	while($row = mysql_fetch_array($result))
	{
		$rows[] = $row;
	}
	
	$return_data = array();
	$return_data['record_count'] = $total_rows;
	$return_data['user_lat'] = $row1['user_lat'];
	$return_data['user_lng'] = $row1['user_lng'];
	$return_data['records'] = $rows;
	
	echo json_encode($return_data);
}