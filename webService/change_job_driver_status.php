<?php
// INCLUDE THE REQUIRED FILES.
require_once('../init.php');
include(''.INCLUDE_PATH.'config.php');
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "mailer.php");
require_once(CLASSES_PATH . "account.php"); 
require_once(CLASSES_PATH . "transaction.php");
require_once(CLASSES_PATH . "job.php");
require_once(CLASSES_PATH . "jobReference.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "vehicle.php");
require_once(INCLUDE_PATH . "functions_date_time.php");
// ADD HEADERS FOR THE WEBSERVICE.
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: POST, GET');
header('Content-type: application/json');
header('Access-Control-Allow-Headers:Content-Type');
// INITIALIZE DIFFERENT CLASS OBJECTS.
$database = new database;
$mailer = new Mailer();
$account = new Account();
$job = new Job();
$job_reference = new jobReference();
$user = new User();
// RECEIVE THE DATA IN JSON FORMAT.
$inputJSON = file_get_contents('php://input');
$obj = json_decode($inputJSON,TRUE);
if( $obj['user_role'] =='2' )
{
	if( $obj['action'] == 'bid_job_offer_accept' )
	{
		$job_id = $obj['job_id'];
		$user_id = $obj['user_id'];
		$fee = $obj['fee'];
		
		$query = "Select * from job__driver where job_id='".$job_id."' and offered_to='".$user_id."'";
		$result = $database->query($query);
		$row_num = mysql_num_rows($result);
		$query6="SELECT job__reference.bkg_by_id as booking_made_by, job__reference.pax_id as paxId FROM job__reference INNER JOIN job ON job__reference.id=job.job_reference_id WHERE job.id='".$job_id."'";
		$result6 = mysql_fetch_array($database->query($query6));
		
		if( $row_num >= 1 )
		{
			$job_details = $job->getJobDetails($job_id);
			$job_ref_details = $job_reference->getJobReferenceDetails($job_details['job_reference_id']);
			$driver_details = $user->getUserDetails($user_id);
			$driver_notes =	$job_details['driver_notes'];
			
			if( $job_details['driver_status'] == '1' || $job_details['driver_status'] == '2' ) //Allocated or Sent
			{
				$job->updateJob($job_id, 'driver_status', '3'); // change status of the job to Accepted
				$job->updateJob($job_id, 'driver_id', $user_id); // Put this driver in job table
				$job->updateJob($job_id, 'driver_price', $fee); // Put the driver price in the job table
				$job->addNewDriverToJob($job_id, '', '', '', '', $user_id, $fee, '', '', '', '', '');
				$job->addJobDriverLog($job_id, ''.$driver_details['fname'].' '.$driver_details['lname'].'', 'Job Accepted', 'Accepted by '.$driver_details['fname'].' '.$driver_details['lname'].' for '.$fee.'', '');
				if( $job_details['driver_status'] == '1' )
				{
					$old_status = "Allocated";
				}
				else
				{ 
					$old_status = "Sent";
					$customer_message="Your job(JOB ID- ".$job_id.") accepted by driver name ".$driver_details['fname']." ".$driver_details['lname']." Phone No - '".$driver_details['mobile']."' / '".$driver_details['phone']."'";
					if($result6['booking_made_by']==$result6['paxId'])
					{
						$push_title = 'Job Alert';
						$job->commonGooglePushNotification($result6['booking_made_by'],$customer_message,$push_title);
					}
					else
					{
						$push_title = 'Job Alert';
						$job->commonGooglePushNotification($result6['booking_made_by'],$customer_message,$push_title);
						$job->commonGooglePushNotification($result6['paxId'],$customer_message,$push_title);
					}
				}
				$job->addJobLog($job_id, $user_id, 'Job Accepted by '.$driver_details['fname'].' '.$driver_details['lname'].'', 'Accepted', $old_status);
				$mailer->sendJobDetailsToDriver($job_id, $user_id, $fee, $driver_notes);
				$status_message = 'Thank you for Accepting the job. An email has been sent to you with the details of the job. Please check your email';
				$status = 200;
				
			} 
			if( $job_details['driver_status'] == '3' || $job_details['driver_status'] == '4' ) //Accepted OR PAID
			{
				$job_details = $job->getJobDetails($job_id);
				
				$status_message = 'Sorry, another driver has taken this booking.';
				$status = 201;
			}			
		}
		else
		{
			$status_message = 'Sorry, this job is not on offer anymore. Thank you.';
			$status = 201;
		}
		
		echo json_encode( array('status'=>$status,'status_message'=>$status_message) );
		
	}
}