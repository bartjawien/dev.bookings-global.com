<?php
require_once('../init.php');
include(''.INCLUDE_PATH.'config.php');
include(''.CLASSES_PATH.'database.php');
include(''.CLASSES_PATH.'login.php');
//include(''.CLASSES_PATH.'mailer.php');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: POST, GET');
header('Content-type: application/json');
header('Access-Control-Allow-Headers:Content-Type');
$login = new Login();
$inputJSON = file_get_contents('php://input');
$obj = json_decode($inputJSON,TRUE);
$status = '';
$ip_address=$obj['ip'];
$login_result = $login->check_login_for_app($obj['email'],$obj['password'],'', $ip_address,$obj['device_token'],$obj['lat'],$obj['lng']);
echo json_encode($login_result);
