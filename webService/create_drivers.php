<?php
// INCLUDE THE REQUIRED FILES.
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(INCLUDE_PATH . "functions.php");
require_once(INCLUDE_PATH . "config.php");
// ADD HEADERS FOR THE WEBSERVICE.
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: POST, GET');
header('Content-type: application/json');
header('Access-Control-Allow-Headers:Content-Type');
// INITIALIZE DIFFERENT CLASS OBJECTS.
$database = new Database;
// RECEIVE THE DATA IN JSON FORMAT.
$inputJSON = file_get_contents('php://input');
$obj = json_decode($inputJSON,TRUE);

foreach(array_keys($obj) as $key)
{
	$clean[$key] = mysql_real_escape_string($obj[$key]);
}

if( $obj['action'] == 'add_driver' )
{
	$query1 = "INSERT INTO user
								(
									id,
									created_on,
									role_id,
									title,
									fname,
									lname,
									password,
									email,
									mobile,
									phone
								) 
							VALUES
								(
									NULL,
									CURRENT_TIMESTAMP,
									'2',
									'".$clean['title']."',
									'".$clean['fname']."',
									'".$clean['lname']."',
									'".$clean['password']."',
									'".$clean['email']."',
									'".$clean['mobile']."',
									'".$clean['phone']."'
								)";

	$result = $database->query($query1);
	$row = mysql_fetch_array($result);
	$id = mysql_insert_id();

	$query = "INSERT INTO drivers
									(
										id, 
										user_id, 
										bus_name, 
										dc, 
										dc_exp,	
										lic,
										lic_exp, 
										abn,
										notes
									) 
								VALUES
									(
										NULL,
										'".$id."',
										'".$clean['bus_name']."',
										'".$clean['dc']."',
										'".$clean['dc_exp']."',
										'".$clean['lic']."',
										'".$clean['lic_exp']."',
										'".$clean['abn']."',
										'".$clean['notes']."'
									)";
	$result = $database->query($query);

	$query = "INSERT INTO drivers__accounts
											(
												id,
												user_id,
												acc_table_name,
												acc_name,
												created_on
											) 
										VALUES
											(
												NULL,
												'".$id."',
												'drivers__accounts_".$id."',
												'".$clean['title']." ".$clean['fname']." ".$clean['lname']."',
												'CURRENT_TIMESTAMP'
											)";
	$result = $database->query($query);

	$query = "	
				CREATE TABLE IF NOT EXISTS drivers__accounts_".$id." (
				id int(11) NOT NULL AUTO_INCREMENT,
				trx_id int(11) NOT NULL,
				trx_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
				entered_by varchar(25) NOT NULL DEFAULT 'SYSTEM',
				description varchar(200) NOT NULL,
				acc_dr varchar(50) NOT NULL,
				acc_cr varchar(50) NOT NULL,
				amt_dr decimal(10,2) NOT NULL,
				amt_cr decimal(10,2) NOT NULL,
				PRIMARY KEY (id)
				) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
	$result = $database->query($query);

	$db = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
	$q  = "CREATE TRIGGER update_real_balance_".$id." AFTER INSERT ON drivers__accounts_".$id."
			FOR EACH ROW BEGIN
			declare total_debits decimal(10,2);
			declare total_credits decimal(10,2);
			SELECT sum(amt_dr) INTO total_debits FROM drivers__accounts_".$id.";
			SELECT sum(amt_cr) INTO total_credits FROM drivers__accounts_".$id.";
			SET @acc_balance = total_credits - total_debits;
			update drivers SET acc_bal = @acc_balance where user_id = ".$id.";
			END;
		";
	$db->multi_query($q);
}

if( $obj['action'] == 'edit_driver' )
{
	$query1 = "UPDATE user SET title='".$clean['title']."', fname='".$clean['fname']."', lname='".$clean['lname']."', password='".$clean['password']."', email='".$clean['email']."', mobile='".$clean['mobile']."', phone='".$clean['phone']."' WHERE id='".$clean['user_id']."'";
	$result1 = $database->query($query1);
	
	$query = "UPDATE drivers SET bus_name='".$clean['bus_name']."',	dc='".$clean['dc']."', dc_exp='".$clean['dc_exp']."', lic='".$clean['lic']."', lic_exp='".$clean['lic_exp']."',	abn='".$clean['notes']."' WHERE user_id='".$clean['user_id']."'";
	$result = $database->query($query);
	
	$query2 = "UPDATE drivers__accounts SET acc_name='".$clean['title']." ".$clean['fname']." ".$clean['lname']."' WHERE  user_id='".$clean['user_id']."'";
	$result2 = $database->query($query2);
}