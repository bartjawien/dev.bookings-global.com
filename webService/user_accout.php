<?php
ini_set("display_errors", 0);
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "chargeAccount.php");
require_once(CLASSES_PATH . "user.php");
require_once(INCLUDE_PATH . "functions.php");
require_once(CLASSES_PATH . "job.php");
require_once(CLASSES_PATH . "jobReference.php");
require_once(CLASSES_PATH . "mailer.php");
include(''.INCLUDE_PATH.'settings.php');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: POST, GET');
header('Content-type: application/json');
header('Access-Control-Allow-Headers:Content-Type');
//Recive inputes
$inputJSON = file_get_contents('php://input');
$obj = json_decode($inputJSON,TRUE);
$database = new Database;
$charge_account = new chargeAccount();
$user = new user();
$job = new Job();
$job_ref = new jobReference();
/********************************************** ********************************/
if(isset($obj['action']))
{
	if($obj["action"] == "create_charge_accounts")
	{
		
		if(checkforDuplicates('charge_acc', 'account_name', ''.$obj['account_name'].'')=='0')
			{
				$new_charge_acc_id = $charge_account->addChargeAccount('', $obj['acc_type'], $obj['account_name'], $obj['myob_no']);
				$row = $charge_account->getChargeAccountDetails($new_charge_acc_id);
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['Record'] = $row;
				$jTableResult['Message'] = "New Charge Account Created Successfully";
			}
		else
			{
				$jTableResult = array();
				$jTableResult['Result'] = "ERROR";
				$jTableResult['Message'] = "Duplicate Charge Account Name";
			}
		echo json_encode($jTableResult);
	}
	if($obj["action"] == "create_charge_account_contact")
	{
		if( $obj['id'] != '')
			{
				
				$new_contact_id = $charge_account->addChargeAccountContact($obj['title'], $obj['fname'], $obj['lname'], $obj['password'], $obj['email'], $obj['mobile'], $obj['phone'], $obj['preference'], $obj['id'], $obj['type_id']);
				if($new_contact_id != '')
					{
						$row = $user->getUserDetails($new_contact_id);
						$jTableResult = array();
						$jTableResult['Result'] = "OK";
						$jTableResult['Record'] = $row;
						
					}
				else
					{
						$jTableResult = array();
						$jTableResult['Result'] 	= "ERROR";
						$jTableResult['Message'] 	= "There was a problem adding this contact. Remedy - Please refresh the browser and try creating again.";
					}
			}
		else
			{
				$jTableResult = array();
				$jTableResult['Result'] = "ERROR";
				$jTableResult['Message'] = "Charge Account ID Blank. Remedy - Please refresh the browser and try creating again.";
			}
		echo json_encode($jTableResult);
	}
	if($obj["action"] == "get_all_contacts_for_customer")
	{
		//Get  all contact details
			$charge_account_details=array();
			$query = "SELECT * from charge_acc__contacts where user_id = '".$obj['user_id']."'";
			$result = $database->query($query);
			$row = mysql_fetch_assoc($result);
			$query5 = "SELECT * from charge_acc where id = '".$row['charge_acc_id']."'";
			$result5 = $database->query($query5);
			$row5 = mysql_fetch_assoc($result5);
			$query2 = "SELECT * from charge_acc__contacts where charge_acc_id = '".$row['charge_acc_id']."'";
			$result2 = $database->query($query2);
			while($row2 = mysql_fetch_assoc($result2))
			{
				$query3 = "SELECT * from user where id = '".$row2['user_id']."'";
				$result3 = $database->query($query3);
				$charge_account_details['Record'][] = mysql_fetch_assoc($result3);;
			}
			$charge_account_details['Result'] 			= "OK";
			$charge_account_details['account_type'] 	= $row5['acc_type'];
			$charge_account_details['charge_acc_id']	= $row['charge_acc_id'];
			echo json_encode($charge_account_details);
	}
	if($obj["action"] == "get_all_states")
	{
		//Get all state details
			$charge_account_details=array();
			$query = "SELECT * from variable__states order by order_id ASC";
			$result2 = $database->query($query);
			while($row2 = mysql_fetch_assoc($result2))
			{
				$charge_account_details['Record'][] = $row2;
			}
			$charge_account_details['Result'] = "OK";
			echo json_encode($charge_account_details);
	}
	if($obj["action"] == "get_all_cars")
	{
		//Get all state details
			$charge_account_details=array();
			$query = "SELECT * FROM variable__car_type order by order_id ASC";
			$result2 = $database->query($query);
			while($row2 = mysql_fetch_assoc($result2))
			{
				$charge_account_details['Record'][] = $row2;
			}
			for($i=0;$i<$_MAX_PAX_NOS;$i++)
			{
				$charge_account_details['Pax_nose'][]=$i;
			}
			for($j=0;$j<$_MAX_LUGGAGE;$j++)
			{
				$charge_account_details['Luggage_nose'][]=$j;
			}
			for($k=0;$k<$_MAX_BABY_SEAT;$k++)
			{
				$charge_account_details['baby_seat'][]=$k;
			}
			for($l=0;$l<$_MAX_BOOSTER_SEAT;$l++)
			{
				$charge_account_details['booster_seat'][]=$l;
			}
			for($m=0;$m<$_MAX_CAPSULE;$m++)
			{
				$charge_account_details['capsul_seat'][]=$m;
			}
			$charge_account_details['Result'] = "OK";
			echo json_encode($charge_account_details);
	}
	if($obj["action"] == "update_profile")
	{
		//Get all state details
			$charge_account_details=array();
			$query = "UPDATE user SET title='".$obj['title']."',fname='".$obj['first_name']."',lname='".$obj['last_name']."',email='".$obj['email']."' WHERE id='".$obj['user_id']."'";
			$result2 = $database->query($query);
			$charge_account_details=array();
			if($result2=='true')
			{
				$charge_account_details['Result'] = "OK";
				$charge_account_details['message'] = "Profile updated successfully";
			}
			else
			{
				$charge_account_details['Result'] = "NO";
				$charge_account_details['message'] = "Profile not updated";
			}
			//$charge_account_details['Result'] = "OK";
			echo json_encode($charge_account_details);
	}
	if($obj["action"] == "all_driver_profile")
	{
		//Get all state details
			//$charge_account_details=array();
			$query = "SELECT * FROM user where role_id='2' AND hidden='0' order by fname,lname ASC";
			$result = $database->query($query);
			$account=array();
			$a=array();
			while($row = mysql_fetch_assoc($result))	
			{
				$account['acc_id']=$row['id'];
				$account['acc_name']=$row['fname']." ".$row['lname'];
				array_push($a,$account);
			}
			
			$json = json_encode($a); 
			echo $json;
	}
	// reset password
	if($obj["action"] == "common_reset_password")
	{
		//Get all state details
			//$charge_account_details=array();
			$query = "SELECT * FROM user where id='".$obj["user_id"]."'";
			$result = mysql_fetch_array($database->query($query));
			if($result['password']==$obj["current_password"])
			{
				$query2 = "UPDATE user SET password='".$obj["new_password"]."' where id='".$obj["user_id"]."'";
				$result2 = $database->query($query2);
				$jTableResult['Result'] = "OK";
				$jTableResult['Message'] = "Password updated successfully";
			}
			else
			{
				$jTableResult['Result'] = "NOTOK";
				$jTableResult['Message'] = "Entered password is wrong";
			}
			$json = json_encode($jTableResult); 
			echo $json;
	}
	// track driver
	if($obj["action"] == "track_driver_location")
	{
		//Get all state details
			//$charge_account_details=array();
			
				$query2 = "UPDATE user SET 	user_lat='".$obj["lat"]."',user_lng='".$obj["lng"]."' where id='".$obj["user_id"]."'";
				$result2 = $database->query($query2);
				$jTableResult['Result'] = "OK";
				$jTableResult['Message'] = "Latlong updated";
			
			$json = json_encode($jTableResult); 
			echo $json;
	}
	// edit job details 
	if($obj["action"] == "job_details_for_admin")
	{
		//Get all state details
			//$charge_account_details=array();
			
				$query2 = "SELECT * FROM job__reference INNER JOIN job ON job__reference.id=job.job_reference_id WHERE job.job_reference_id='".$obj["job_ref_id"]."'";
				$result2 = $database->query($query2);
				$data=array();
				while($row = mysql_fetch_array($result2))	
				{
					array_push($data,$row);
				}
				$jTableResult['Jobs']=$data;
				$jTableResult['Result'] = "OK";
				$jTableResult['Message'] = "Jobs details";
			
			$json = json_encode($jTableResult); 
			echo $json;
	}
	// change driver status for job
	if($obj["action"] == "change_driver_status_for_job")
	{
		//Get all state details
			//$charge_account_details=array();
			
				if( $obj['driver_status'] == '1' && $obj['driver_id'] !='' )  // allocate
				{
					$job->updateJob($obj['job_id'], 'driver_status', '1');  // change the driver status to Allocate
					$job->addNewDriverToJob($obj['job_id'], $obj['driver_id'], $obj['driver_price'], '', '', '', '', '', '', '', '', '','');  // add this to job__driver table
					$driver_details = $user->getUserDetails($obj['driver_id']);  // get details of this driver
					$job->addJobDriverLog($obj['job_id'], $obj['user_id'], 'Job Allocated', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$obj['j1_driver_price'].'', '');
					$push_title="Job ".$obj['job_id'];
					$push_message = 'Job Id : '.$obj['job_id'].' has been allocated to you.';
					$job->commonGooglePushNotification($obj['driver_id'],$push_message,$push_title);
					
				}
				
				// FOR DRIVER/S OFFER.
				if( $obj['driver_status'] == '2' && !empty($obj['driver_id']) && $obj['driver_price'] !='' )  // send
				{
						$mailer = new mailer();
						$job->updateJob($obj['job_id'], 'driver_status', '2');  // change the driver status to Sent
						$job->updateJob($obj['job_id'], 'driver_id', $obj['driver_id']);  // change the driver status to Sent
						$job->updateJob($obj['job_id'], 'driver_price', $obj['driver_price']);  // change the driver status to Sent
						$driver_details = $user->getUserDetails($obj['driver_id']);  // get details of this driver
						$job->addNewDriverToJob($obj['job_id'], '', '', $obj['driver_id'], $obj['driver_price'], '', '', '', '', '', '', '','');  // add this to job__driver table
						$job->addJobDriverLog($obj['job_id'], $obj['user_id'], 'Job Offered', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$obj['driver_price'].'', '');
						$mailer->sendJobOfferToDriver($obj['driver_id'], $obj['job_id'], $obj['driver_price'],'');  // send job offer to the driver
						$push_title="Job ".$obj['job_id'];
						$push_message = 'Job Id : '.$obj['job_id'].' has been offered to you.';
						$job->commonGooglePushNotification($obj['driver_id'],$push_message,$push_title);
				}
				if(($obj['driver_status'] == '3' || $obj['driver_status'] == '4' || $obj['driver_status'] == '5') && !empty($obj['driver_id']) && $obj['driver_price'] !='' )  // accepted
				{
					$mailer = new mailer();
					$job->updateJob($obj['job_id'], 'driver_status',$obj['driver_status']);  // change the driver status to Sent
					$job->updateJob($obj['job_id'], 'driver_id', $obj['driver_id']);  // change the driver status to Sent
					$job->updateJob($obj['job_id'], 'driver_price', $obj['driver_price']);  // change the driver status to Sent
					$job->addNewDriverToJob($obj['job_id'], '', '', '', '', $obj['driver_id'], $obj['driver_price'], '', '', '', '', '','');  // add this to job__driver table
					$q1=$database->query("UPDATE job SET driver_id='".$obj['driver_id']."' WHERE id='".$obj['job_id']."'");
					$driver_details = $user->getUserDetails($obj['driver_id']);  // get details of this driver
					$job->addJobDriverLog($obj['job_id'], $obj['user_id'], 'Job Accepted', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$obj['driver_price'].'', '');
					//$mailer->sendJobDetailsToDriver($obj['job_id'], $obj['driver_id'], $obj['driver_price'],'');  // send job acceptance email with job details to the driver
						//$jTableResult['Result']=$q1;
				}
			$jTableResult['Result']='OK';
			$jTableResult['Message']="Driver status for this job successfully changed";
			$json = json_encode($jTableResult); 
			echo $json;
	}
	if($obj["action"] == "create_charge_account_contact_from_booking")
	{
		$query2 = "SELECT * FROM user WHERE email='".$obj["new_pax_email"]."' AND role_id='3'";
		$result2 = $database->query($query2);
		if(mysql_num_rows($result2)==0)
		{
			if( $obj['account_charge_id'] != '')
				{
					
					$new_contact_id = $charge_account->addChargeAccountContact($obj['new_pax_title'], $obj['new_pax_fname'], $obj['new_pax_lname'], $obj['new_pax_password'], $obj['new_pax_email'], $obj['new_pax_mobile'], $obj['new_pax_phone'], $obj['new_pax_preference'], $obj['account_charge_id'], $obj['new_pax_type']);
					if($new_contact_id != '')
						{
							$row = $user->getUserDetails($new_contact_id);
							$jTableResult = array();
							$jTableResult['Result'] = "OK";
							$jTableResult['Record'] = $row;
							
						}
					else
						{
							$jTableResult = array();
							$jTableResult['Result'] 	= "ERROR";
							$jTableResult['Message'] 	= "There was a problem adding this contact. Remedy - Please refresh the browser and try creating again.";
						}
				}
			else
				{
					$jTableResult = array();
					$jTableResult['Result'] = "ERROR";
					$jTableResult['Message'] = "Charge Account ID Blank. Remedy - Please refresh the browser and try creating again.";
				}
		}
		else
		{
			$jTableResult = array();
			$jTableResult['Result'] = "ERROR";
			$jTableResult['Message'] = "This email already register with this account.";
		}
		echo json_encode($jTableResult);
	}
}
