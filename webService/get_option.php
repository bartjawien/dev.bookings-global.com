<?php
ini_set("display_errors", 1);
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "fareEstimator.php");
require_once(CLASSES_PATH . "job.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "chargeAccount.php");
session_start();
$database = new Database;
//For list of all points of interest
// ADD HEADERS FOR THE WEBSERVICE.
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: POST, GET');
header('Content-type: application/json');
header('Access-Control-Allow-Headers:Content-Type');
$inputJSON = file_get_contents('php://input');
$obj = json_decode($inputJSON,TRUE);
if(isset($obj['all_pois']))
{
	if($obj['ROLE_ID'] == '1')
		{
			$query = "SELECT * from pois order by order_id ASC";
			$result = $database->query($query);
			$address=array();
			$a=array();
			while($row = mysql_fetch_assoc($result))	
			{
				//$data .='<input type="radio" name="add_sel_add_1" value="'.$row['id'].'">'.$row['line1'].'<br/>';
				$address['addrss_list']=$row['line1'].', '.$row['line2'].', '.$row['sub'].', '.$row['postcode'];
				$address['addrss_id']=$row['address_id'];
				array_push($a,$address);
			}
						//$data .="</div>";
			//$a = array('data' => ''.$data.'');
			$json = json_encode($a); 
			echo $json;
		}
	if($obj['ROLE_ID'] == '3')
		{
			$query = "SELECT * from charge_acc__contacts where user_id = '".$obj['USER_ID']."'";
			$database = new database;
			$result = $database->query($query);
			$row = mysql_fetch_assoc($result);
			$query = "SELECT * from charge_acc__pois where charge_acc_id = '".$row['charge_acc_id']."' order by sub ASC";
			$result = $database->query($query);
			$address=array();
			$a=array();
			while($row = mysql_fetch_assoc($result))	
			{
				//$data .='<input type="radio" name="add_sel_add_1" value="'.$row['id'].'">'.$row['line1'].', '.$row['line2'].', '.$row['sub'].', '.$row['state'].', '.$row['postcode'].'<br/>';
				$address['addrss_list']=$row['line1'].', '.$row['line2'].', '.$row['sub'].', '.$row['postcode'];
				$address['addrss_id']=$row['address_id'];
				array_push($a,$address);
			}
			
			$json = json_encode($a); 
			echo $json;
		}
}

//Details of one of the points of interest
if(isset($obj['a_pois']))
	{
		if($_SESSION['ROLE_ID'] == '1')
			{
				$query = "SELECT * from pois where id = '".$_GET['a_pois']."'";
				$result = $database->query($query);
				$row = mysql_fetch_assoc($result);
				$a = array('data1' => ''.$row['line1'].'', 'data2' => ''.$row['line2'].'', 'data3' => ''.$row['sub'].'', 'data4' => ''.$row['postcode'].'');
				$json = json_encode($a); 
				echo $json;
			}
		if($_SESSION['ROLE_ID'] == '3')
			{
				$query = "SELECT * from charge_acc__pois where id = '".$_GET['a_pois']."'";
				$result = $database->query($query);
				$row = mysql_fetch_assoc($result);
				$a = array('data1' => ''.$row['line1'].'', 'data2' => ''.$row['line2'].'', 'data3' => ''.$row['sub'].'', 'data4' => ''.$row['postcode'].'');
				$json = json_encode($a); 
				echo $json;
			}
	}
//For list of all points of interest
if(isset($obj['all_pax_add']))
	{
		$query = "SELECT *,
					user__address.id as address_id
					from 
					user__address, variable__states
					where 
					user__address.state = variable__states.id
					AND
					user__address.user_id='".$obj['all_pax_add']."'";
					$address=array();
					$a=array();
		$result = $database->query($query);
		while($row = mysql_fetch_assoc($result))	
		{
			$address['addrss_list']=$row['line1'].', '.$row['line2'].', '.$row['sub'].', '.$row['postcode'];
			$address['addrss_id']=$row['address_id'];
			array_push($a,$address);
			//$data .='<input type="radio" name="add_sel_add_1" value="'.$row['address_id'].'">'.$row['line1'].', '.$row['line2'].', '.$row['sub'].', '.$row['details'].'<br/>';
		}
		$json = json_encode($a); 
		echo $json;
	}
//Details of one of the address
if(isset($_GET['an_add']))
	{
		$query = "SELECT * from user__address where id = '".$_GET['an_add']."'";
		$result = $database->query($query);
		$row = mysql_fetch_assoc($result);
		$a = array('data1' => ''.$row['line1'].'', 'data2' => ''.$row['line2'].'', 'data3' => ''.$row['sub'].'', 'data4' => ''.$row['postcode'].'');
		$json = json_encode($a); 
		echo $json;
	}
//Get the type of Account and return all the charge account types
if(isset($obj['acc_type']))
	{
		/*if($obj['option_type'] == 'invoice' || $obj['option_type'] == 'invoice_listing' || $obj['option_type'] == 'invoice_un')
		{
			$data .= '<label>Charge Account </label>';
		}*/
		$account=array();
		$a=array();
		$query = "SELECT * from charge_acc where acc_type='".$obj['acc_type']."' order by account_name ASC";
		$result = $database->query($query);
		/*if($obj['option_type'] == 'invoice_listing')
		{
			$data .= '<select name="charge_acc_for_listing" id="charge_acc_for_listing" data-placeholder="Select Charge Account..." class="chosen-select" style="width:350px;">
					<option value="">Please Select</option>';
		}
		else if($obj['option_type'] == 'invoice_un')
		{
			$data .= '<select name="charge_acc_un" id="charge_acc_un" data-placeholder="Select Charge Account..." class="chosen-select" style="width:350px;">
					<option value="">Please Select</option>';
		}
		else
		{
			$data .= '<select name="charge_acc" id="charge_acc" data-placeholder="Select Charge Account..." class="chosen-select" style="width:350px;">
					<option value="">Please Select</option>';
		}*/
		
		while($row = mysql_fetch_assoc($result))	
		{
			$account['acc_id']=$row['id'];
			$account['acc_name']=$row['account_name'];
			array_push($a,$account);
		}
		
		$json = json_encode($a); 
		echo $json;
	}
//Get the type of Account and return all the charge account types
if(isset($obj['acc_contacts']))
	{
		
		$query1 = "SELECT * from
					charge_acc__contacts, user
					where charge_acc__contacts.user_id = user.id
					AND charge_acc__contacts.charge_acc_id='".$obj['acc_contacts']."' 
					AND (charge_acc__contacts.type_id = '1' OR charge_acc__contacts.type_id = '2' OR charge_acc__contacts.type_id = '3')
					AND user.hidden = 0
					order by user.fname";
		$account=array();
		$a=array();
		$result1 = $database->query($query1);
		
		while($row1 = mysql_fetch_assoc($result1))	
		{
			//$data1 .='<option value="'.$row1['id'].'">'.$row1['fname'].' '.$row1['lname'].'</option>';
			$account['acc_id']=$row1['id'];
			$account['acc_name']=$row1['fname'].' '.$row1['lname'];
			array_push($a,$account);
		}
		
		$json = json_encode($a); 
		echo $json;
	}
//Get the contact id and return the prefernce of this contact
if(isset($obj['acc_contact_id']))
	{
		$query = "SELECT * from user where id='".$obj['acc_contact_id']."'";
		$result = $database->query($query);
		$row = mysql_fetch_assoc($result);
		$data1 = $row['preference'].'';
		$a = array('data1' => $data1);
		$json = json_encode($a); 
		echo $json;
	}
//Get the pax id and return the prefernce of this contact
if(isset($_GET['acc_pax_id']))
	{
		$query = "SELECT * from user where id='".$_GET['acc_pax_id']."'";
		$result = $database->query($query);
		$row = mysql_fetch_assoc($result);
		$data1 = 'Preference - '.$row['preference'].'';
		$a = array('data1' => $data1);
		$json = json_encode($a); 
		echo $json;
	}
if(isset($_POST['from']) && isset($_POST['to']))
	{	
		$fareEstimator 	= 	new fareEstimator();
		$google_value 	= 	$fareEstimator->getGoogleDistanceTime($_POST['from'], $_POST['to'], $raw = false);
		$google_ret_value 	= 	$fareEstimator->getGoogleDistanceTime($_POST['ret_from'], $_POST['ret_to'], $raw = false);
		$a = array('data1' => $google_value['distance'], 'data2' => $google_ret_value['distance']);
		$json = json_encode($a); 
		echo $json;
	}
if(isset($_GET['booking_id_1']))
	{
		
		$booking = new booking();
		$user = new user();
		$bkg_1_details = $booking->getBookingDetails($_GET['booking_id_1']);
		
		$bkg_1_booking_log = $booking->getBookingLog('booking', $bkg_1_details['booking_id']);
		$bkg_1_booking_price_log = $booking->getBookingLog('booking__price', $bkg_1_details['booking_price_id']);
		$bkg_1_booking_ref_log = $booking->getBookingLog('booking__ref', $bkg_1_details['booking_referece_id']);
		$bkg_1_booking_ref_details_log = $booking->getBookingLog('booking__ref_details', $bkg_1_details['booking_referece_details_id']);
		
		$table .= "<table id='dialog_table'><tr><th colspan='5'>Log for Booking ID ".$bkg_1_details['booking_id']."</th></tr>
			<tr><th>Entered By</th><th>Logged On</th><th>Log Message</th><th>New Value</th><th>Old Value</th></tr>";
		if(empty($bkg_1_booking_log) && empty($bkg_1_booking_price) && empty($bkg_1_booking_ref_log) && empty($bkg_1_booking_ref_details_log))
			{
				$table .="<tr><td colspan='5'>No Records Found....</td></tr>";
			}
		else
			{
				if(!empty($bkg_1_booking_log))
					{
						foreach($bkg_1_booking_log as $value)
							{
								$entered_by_name = $user->getUserDetails($value['entered_by']);
								$table .='<tr><td>'.$entered_by_name['fname'].' '.$entered_by_name['lname'].'</td>
											<td>'.$value['created_on'].'</td>
											<td>'.$value['message'].'</td>
											<td>'.$value['new_value'].'</td>
											<td>'.$value['old_value'].'</td>
										</tr>';
							}
					}
				if(!empty($bkg_1_booking_price_log))
					{
						foreach($bkg_1_booking_price_log as $value)
							{
								$entered_by_name = $user->getUserDetails($value['entered_by']);
								$table .='<tr><td>'.$entered_by_name['fname'].' '.$entered_by_name['lname'].'</td>
											<td>'.$value['created_on'].'</td>
											<td>'.$value['message'].'</td>
											<td>'.$value['new_value'].'</td>
											<td>'.$value['old_value'].'</td>
										</tr>';
							}
					}
				if(!empty($bkg_1_booking_ref_log))
					{
						foreach($bkg_1_booking_ref_log as $value)
							{
								$entered_by_name = $user->getUserDetails($value['entered_by']);
								$table .='<tr><td>'.$entered_by_name['fname'].' '.$entered_by_name['lname'].'</td>
											<td>'.$value['created_on'].'</td>
											<td>'.$value['message'].'</td>
											<td>'.$value['new_value'].'</td>
											<td>'.$value['old_value'].'</td>
										</tr>';
							}
					}
				if(!empty($bkg_1_booking_ref_details_log))
					{
						foreach($bkg_1_booking_ref_details_log as $value)
							{
								$entered_by_name = $user->getUserDetails($value['entered_by']);
								$table .='<tr><td>'.$entered_by_name['fname'].' '.$entered_by_name['lname'].'</td>
											<td>'.$value['created_on'].'</td>
											<td>'.$value['message'].'</td>
											<td>'.$value['new_value'].'</td>
											<td>'.$value['old_value'].'</td>
										</tr>';
							}
					}
			}
		$table .= "</table>";
		if(isset($_GET['booking_id_2']))
			{
				$bkg_2_details = $booking->getBookingDetails($_GET['booking_id_2']);
				
				$bkg_2_booking_log = $booking->getBookingLog('booking', $bkg_2_details['booking_id']);
				$bkg_2_booking_price_log = $booking->getBookingLog('booking__price', $bkg_2_details['booking_price_id']);
				$bkg_2_booking_ref_log = $booking->getBookingLog('booking__ref', $bkg_2_details['booking_referece_id']);
				$bkg_2_booking_ref_details_log = $booking->getBookingLog('booking__ref_details', $bkg_2_details['booking_referece_details_id']);
				$table .= "<table id='dialog_table'><tr><th colspan='5'>Log for Booking ID ".$bkg_2_details['booking_id']."</th></tr>
							<tr><th>Entered By</th><th>Logged On</th><th>Log Message</th><th>New Value</th><th>Old Value</th></tr>";
				if(empty($bkg_2_booking_log) && empty($bkg_2_booking_price) && empty($bkg_2_booking_ref_log) && empty($bkg_2_booking_ref_details_log))
					{
						$table .="<tr><td colspan='5'>No Records Found....</td></tr>";
					}
				else
					{
						if(!empty($bkg_2_booking_log))
							{
								foreach($bkg_2_booking_log as $value)
									{
										$entered_by_name = $user->getUserDetails($value['entered_by']);
										$table .='<tr><td>'.$entered_by_name['fname'].' '.$entered_by_name['lname'].'</td>
													<td>'.$value['created_on'].'</td>
													<td>'.$value['message'].'</td>
													<td>'.$value['new_value'].'</td>
													<td>'.$value['old_value'].'</td>
												</tr>';
									}
							}
						if(!empty($bkg_2_booking_price_log))
							{
								foreach($bkg_2_booking_price_log as $value)
									{
										$entered_by_name = $user->getUserDetails($value['entered_by']);
										$table .='<tr><td>'.$entered_by_name['fname'].' '.$entered_by_name['lname'].'</td>
													<td>'.$value['created_on'].'</td>
													<td>'.$value['message'].'</td>
													<td>'.$value['new_value'].'</td>
													<td>'.$value['old_value'].'</td>
												</tr>';
									}
							}
						if(!empty($bkg_2_booking_ref_log))
							{
								foreach($bkg_2_booking_ref_log as $value)
									{
										$entered_by_name = $user->getUserDetails($value['entered_by']);
										$table .='<tr><td>'.$entered_by_name['fname'].' '.$entered_by_name['lname'].'</td>
													<td>'.$value['created_on'].'</td>
													<td>'.$value['message'].'</td>
													<td>'.$value['new_value'].'</td>
													<td>'.$value['old_value'].'</td>
												</tr>';
									}
							}
						if(!empty($bkg_2_booking_ref_details_log))
							{
								foreach($bkg_2_booking_ref_details_log as $value)
									{
										$entered_by_name = $user->getUserDetails($value['entered_by']);
										$table .='<tr><td>'.$entered_by_name['fname'].' '.$entered_by_name['lname'].'</td>
													<td>'.$value['created_on'].'</td>
													<td>'.$value['message'].'</td>
													<td>'.$value['new_value'].'</td>
													<td>'.$value['old_value'].'</td>
												</tr>';
									}
							}
					}
			}
		$table .= "</table>";
		$a = array('data' => ''.$table.'');
		$json = json_encode($a); 
		echo $json;
	}
	
	if(isset($_POST['acc_cont_id']))
	{
		$query = "SELECT * from user where id='".$_POST['acc_cont_id']."'";
		$result = $database->query($query);
		$row = mysql_fetch_assoc($result);
		$data1 = '<table><th colspan="2">Edit Contact:</th><tr>
							<td valign="top">Name</td>
							<td colspan="1">
								<select name="edit_acc_title" id="edit_acc_title">
									<option value=""></option>
									<option value="Mr"';
									if($row['title'] == 'Mr') {$data1 .= 'selected';} 
									$data1 .= '>Mr</option>
									<option value="Mrs"'; 
									if($row['title'] == 'Mrs') {$data1 .= 'selected';}
									$data1 .= '>Mrs</option>
									<option value="Ms"'; 
									if($row['title'] == 'Ms') {$data1 .=  'selected';} 
									$data1 .= '>Ms</option>
									<option value="Sir"'; 
									if($row['title'] == 'Sir') {$data1 .=  'selected';}
									$data1 .= '>Sir</option>
									<option value="Prof"'; 
									if($row['title'] == 'Prof') {$data1 .=  'selected';} $data1 .= '>Prof</option>
								</select>
								<input type="text" name="edit_acc_fname" id="edit_acc_fname" placeholder="First Name" value="'.$row['fname'].'">
								<input type="text" name="edit_acc_lname" id="edit_acc_lname" placeholder="Last Name" value="'.$row['lname'].'"></td></tr>
						<tr><td valign="top">Mobile</td>		
								<td><input type="text" name="edit_acc_mobile" id="edit_acc_mobile" placeholder="Mobile" value="'.$row['mobile'].'">
							</td></tr>
						<tr><td valign="top">Phone</td>	
								<td><input type="text" name="edit_acc_phone" id="edit_acc_phone" placeholder="Phone" value="'.$row['phone'].'">
						</td></tr>	
						<tr><td valign="top">Email</td>
								<td><input type="text" name="edit_acc_email" id="edit_acc_email" placeholder="Email" value="'.$row['email'].'">
							</td>
						</tr>
						<tr>
							<td>Preference:<br/>(If any)</td>
							<td colspan="2">
								<textarea name="edit_acc_preference" id="edit_acc_preference" cols="30" rows="2">'.$row['preference'].'</textarea>
							</td>
						</tr>
						<tr>
							<td colspan="2"><input type="hidden" name="acc_id" id="acc_id" value="'.$row['id'].'" /><input type="button" name="update_acc" id="update_acc" value="Update" class="sml_btn" /></td>
						</tr>
					</table>';
		/*$a = array('data1' => $data1);
		$json = json_encode($a); 
		echo $json;*/
		echo $data1;
	}
	
	if(isset($_POST['update_acc']))
	{
	
		if($_POST['acc_id'] != '')
		{
			$acc_id = $_POST['acc_id'] ;
			$user= new User;
			$user->updateUser($acc_id, 'title', $_POST['title']);
			$user->updateUser($acc_id, 'fname', $_POST['fname']);
			$user->updateUser($acc_id, 'lname', $_POST['lname']);
			$user->updateUser($acc_id, 'phone', $_POST['phone']);
			$user->updateUser($acc_id, 'mobile', $_POST['mobile']);
			$user->updateUser($acc_id, 'email', $_POST['email']);
			$user->updateUser($acc_id, 'preference', $_POST['preference']);
			echo 'Data updated Successfully';
		}
	}
	
	if(isset($_GET['acc_contacts_after_edit']))
	{
		$query1 = "SELECT * from
					charge_acc__contacts, user
					where charge_acc__contacts.user_id = user.id
					AND charge_acc__contacts.charge_acc_id='".$_GET['acc_contacts_after_edit']."' 
					AND (charge_acc__contacts.type_id = '1' OR charge_acc__contacts.type_id = '3')
					order by user.fname";
		$result1 = $database->query($query1);
		$data1 .= '<select name="acc_contact" id="acc_contact" data-placeholder="Select Booked By..." class="chosen-select" style="width:350px;">
					<option value="">Please Select</option>';
		while($row1 = mysql_fetch_assoc($result1))	
				{
					if($row1['id'] == $_GET['acc_id'])
					{
						$data1 .='<option value="'.$row1['id'].'" selected>'.$row1['fname'].' '.$row1['lname'].'</option>';
					}
					else
					{
						$data1 .='<option value="'.$row1['id'].'">'.$row1['fname'].' '.$row1['lname'].'</option>';
					}
					
				}
		$data1 .="</select>";
		
		
		
		$a = array('data' => $data1);
		$json = json_encode($a); 
		echo $json;
	}
	if(isset($_POST['pax_cont_id']))
	{
		$query = "SELECT * from user where id='".$_POST['pax_cont_id']."'";
		$result = $database->query($query);
		$row = mysql_fetch_assoc($result);
		$data1 = '<table><th colspan="2">Edit Contact:</th><tr>
							<td valign="top">Name</td>
							<td colspan="1">
								<select name="edit_pax_title" id="edit_pax_title">
									<option value=""></option>
									<option value="Mr"';
									if($row['title'] == 'Mr') {$data1 .= 'selected';} 
									$data1 .= '>Mr</option>
									<option value="Mrs"'; 
									if($row['title'] == 'Mrs') {$data1 .= 'selected';}
									$data1 .= '>Mrs</option>
									<option value="Ms"'; 
									if($row['title'] == 'Ms') {$data1 .=  'selected';} 
									$data1 .= '>Ms</option>
									<option value="Dr"'; 
									if($row['title'] == 'Dr') {$data1 .=  'selected';}
									$data1 .= '>Dr</option>
									<option value="Prof"'; 
									if($row['title'] == 'Prof') {$data1 .=  'selected';} $data1 .= '>Prof</option>
								</select>
								<input type="text" name="edit_pax_fname" id="edit_pax_fname" placeholder="First Name" value="'.$row['fname'].'">
								<input type="text" name="edit_pax_lname" id="edit_pax_lname" placeholder="Last Name" value="'.$row['lname'].'"></td></tr>
						<tr><td valign="top">Mobile</td>		
								<td><input type="text" name="edit_pax_mobile" id="edit_pax_mobile" placeholder="Mobile" value="'.$row['mobile'].'">
							</td></tr>
						<tr><td valign="top">Phone</td>	
								<td><input type="text" name="edit_pax_phone" id="edit_pax_phone" placeholder="Phone" value="'.$row['phone'].'">
						</td></tr>	
						<tr><td valign="top">Email</td>
								<td><input type="text" name="edit_pax_email" id="edit_pax_email" placeholder="Email" value="'.$row['email'].'">
							</td>
						</tr>
						<tr>
							<td>Preference:<br/>(If any)</td>
							<td colspan="2">
								<textarea name="edit_pax_preference" id="edit_pax_preference" cols="30" rows="2">'.$row['preference'].'</textarea>
							</td>
						</tr>
						<tr>
							<td colspan="2"><input type="hidden" name="edit_pax_id" id="edit_pax_id" value="'.$row['id'].'" /><input type="button" name="update_pax" id="update_pax" value="Update" class="sml_btn" /></td>
						</tr>
					</table>';
		/*$a = array('data1' => $data1);
		$json = json_encode($a); 
		echo $json;*/
		echo $data1;
	}
	
	if(isset($_POST['update_pax']))
	{
	
		if($_POST['pax_id'] != '')
		{
			$pax_id = $_POST['pax_id'] ;
			$user= new User;
			$user->updateUser($pax_id, 'title', $_POST['title']);
			$user->updateUser($pax_id, 'fname', $_POST['fname']);
			$user->updateUser($pax_id, 'lname', $_POST['lname']);
			$user->updateUser($pax_id, 'phone', $_POST['phone']);
			$user->updateUser($pax_id, 'mobile', $_POST['mobile']);
			$user->updateUser($pax_id, 'email', $_POST['email']);
			$user->updateUser($pax_id, 'preference', $_POST['preference']);
			echo 'Data updated Successfully';
		}
	}
	
	if(isset($_GET['pax_contacts_after_edit']))
	{
		$query2 = "SELECT * from
					charge_acc__contacts, user
					where charge_acc__contacts.user_id = user.id
					AND charge_acc__contacts.charge_acc_id = '".$_GET['pax_contacts_after_edit']."'
					AND (charge_acc__contacts.type_id = '1' OR charge_acc__contacts.type_id = '2')
					order by user.fname";
		$result2 = $database->query($query2);
		$data .= '<select name="acc_passenger" id="acc_passenger" data-placeholder="Select Passenger..." class="chosen-select" style="width:350px;">
					<option value="">Please Select</option>';
		while($row2 = mysql_fetch_assoc($result2))	
				{
					if($row2['id'] == $_GET['pax_id'])
					{
						$data .='<option value="'.$row2['id'].'" selected>'.$row2['fname'].' '.$row2['lname'].'</option>';
					}
					else
					{
						$data .='<option value="'.$row2['id'].'">'.$row2['fname'].' '.$row2['lname'].'</option>';
					}
					$data .='<option value="'.$row2['id'].'">'.$row2['fname'].' '.$row2['lname'].'</option>';
				}
		$data .="</select>";
		
		$a = array('data' => $data);
		$json = json_encode($a); 
		echo $json;
	}
	
	if(isset($_GET['all_driver']))
	{
		$data1='<form name="collective_driver_allocation" id="collective_driver_allocation" method="post">
				<input type="hidden" name="add_driver" id="add_driver" value="1" />
				<input type="hidden" name="job_id_arr" id="job_id_arr" value="'.$_GET['job_id_arr'].'" />
				<select name="j1_driver_status" id="j1_driver_status" >
					<option value=""> </option>
					<option value="3">Accepted</option>
					<option value="4">Paid</option>
				</select>
						<span id="j1_driver_row"> 
							<select name="j1_driver" id="j1_driver" >
								<option value="">Please Select</option>';
								$query = "SELECT * FROM user where role_id='2' AND hidden='0' order by fname,lname ASC";
								$database = new database;
								$result = $database->query($query);
								while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
									{
										$data1 .= "<option value=$row[id]>$row[fname] $row[lname]</option>";
									}
								$data1 .=  '</select> 
						</span>';
						
						/*$data1 .=  '<span id="j1_driver_row_multiple"> 
							<select name="j1_drivers[]" id="j1_driver2" class="chosen" multiple="true" style="width:250px;" >
								<option value="">Please Select</option>';
								$query = "SELECT * FROM user where role_id='2' AND hidden='0' order by fname,lname ASC";
								$database = new database;
								$result = $database->query($query);
								while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
									{
										$data1 .=  "<option value=$row[id]>$row[fname] $row[lname]</option>";
									}
								$data1 .=  '</select>
						</span>';*/
						
						$data1 .=  '<span id="j1_driver_price_span" > Price: <input type="text" name="j1_driver_price" id="j1_driver_price" placeholder="0.00" size="5"><br/></span>';
					$data1 .=  '<br /> <span id="j1_driver_notes_span" > Driver\'s Notes: <textarea name="j1_driver_notes" id="j1_driver_notes" placeholder="Driver\'s notes" ></textarea><br/></span>
								<br /> <span id="j1_submit_driver" ><input type="button" name="j1_submit_driver_button" id="j1_submit_driver_button" value="Submit" /></span>
								</form>';

		$a = array('data1' => $data1);
		$json = json_encode($a); 
		echo $json;
	}
	
	if(isset($_POST['add_driver']))
	{
		require_once(CLASSES_PATH . "user.php");
		require_once(CLASSES_PATH . 'vehicle.php');
		require_once(CLASSES_PATH . "job.php");
		require_once(CLASSES_PATH . "mailer.php");
		
		$user = new user();
		$job = new Job();
		$mailer = new mailer();
		
		$job_arr = explode(',',$_POST['job_id_arr']);
		//print_r($job_arr);
			/*
			if($_POST['j1_driver_status'] == '1' && $_POST['j1_driver'] !='') //Allocate
				{
					$driver_details_arr = $user->getUserDetails($_POST['j1_driver']); // get details of this driver
					foreach ($job_arr as $j1_id)
					{					
						$job->updateJob($j1_id, 'driver_status', '1'); // change the driver status to Allocate
						$job->addNewDriverToJob($j1_id, $_POST['j1_driver'], $_POST['j1_driver_price'], '', '', '', '', '', '', '', '', '',''); // Add this to job__driver table
						$job->addJobDriverLog($j1_id, $_SESSION['USER_ID'], 'Job Allocated', ''.$driver_details_arr['fname'].' '.$driver_details_arr['lname'].' for $ '.$_POST['j1_driver_price'].'', '');
						$additional_message .= "Booking ID -  ".$j1_id ." allocated to - <b>".$driver_details_arr['fname']." ".$driver_details_arr['lname']." for $ ".$_POST['j1_driver_price']."</b><br/>";
					}
					
				}
			if($_POST['j1_driver_status'] == '2' && !empty($_POST['j1_drivers']) && $_POST['j1_driver_price'] !='') //Send
				{					
					$job->updateJob($j1_id, 'driver_status', '2'); // change the driver status to Sent
					foreach($_POST['j1_drivers'] as $key => $value)	
					{
						$driver_details = $user->getUserDetails($value); // get details of this driver
						foreach ($job_arr as $j1_id)
						{
							$job->addNewDriverToJob($j1_id, '', '', $value, $_POST['j1_driver_price'], '', '', '', '', '', '', '',''); // Add this to job__driver table
							$job->addJobDriverLog($j1_id, $_SESSION['USER_ID'], 'Job Offered', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$_POST['j1_driver_price'].'', '');
							$mailer->sendJobOfferToDriver($value, $j1_id, $_POST['j1_driver_price'],$_POST['j1_driver_notes']); // Send job offer to the driver
							$additional_message .= "Booking ID -  ".$j1_id ." offered to - <b>".$driver_details['fname']." ".$driver_details['lname']." for $ ".$_POST['j1_driver_price']."</b><br/>";
						}	
					}
				}*/
			if($_POST['j1_driver_status'] == '3' && !empty($_POST['j1_driver']) && $_POST['j1_driver_price'] !='') //Accepted
				{					
					$driver_details_arr = $user->getUserDetails($_POST['j1_driver']); // get details of this driver					
					$additional_message .= "Booking ID - ";
					
					foreach ($job_arr as $j1_id)
					{					
						$job->updateJob($j1_id, 'driver_status', '3');
						$job->updateJob($j1_id, 'driver_id', $_POST['j1_driver']);
						$job->updateJob($j1_id, 'driver_price', $_POST['j1_driver_price'] ); // Put the driver price in the job table						
						$job_driver_id = $job->addNewDriverToJob($j1_id, '', '', '', '', $_POST['j1_driver'], $_POST['j1_driver_price'], '', '', '', '', '',''); // Add this to job__driver table
						$job->addNewCollectiveBooking($_POST['job_id_arr'], $job_driver_id,$_POST['j1_driver'] );
						$job->addJobDriverLog($j1_id, $_SESSION['USER_ID'], 'Job Accepted', ''.$driver_details_arr['fname'].' '.$driver_details_arr['lname'].' for $ '.$_POST['j1_driver_price'].'', '');
						$j_message= ($j_message!='')?$j_message.', '.$j1_id : $j1_id;								
					}
					$additional_message .= $j_message . " Accepted by - <b>".$driver_details_arr['fname']." ".$driver_details_arr['lname']." for $ ".$_POST['j1_driver_price']."</b><br/>";
					$mailer->sendCollectiveJobDetailsToDriver($_POST['job_id_arr'], $_POST['j1_driver'], $_POST['j1_driver_price'],$_POST['j1_driver_notes']); // Send job acceptance email with job details to the driver
				} 
		echo '<b>'.$additional_message.'</b>';
	}
	
?>