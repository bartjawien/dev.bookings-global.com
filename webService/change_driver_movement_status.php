<?php
// INCLUDE THE REQUIRED FILES.
require_once('../init.php');
include(''.INCLUDE_PATH.'config.php');
require_once(CLASSES_PATH . "job.php");
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "mailer.php");
// ADD HEADERS FOR THE WEBSERVICE.
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: POST, GET');
header('Content-type: application/json');
header('Access-Control-Allow-Headers:Content-Type');
// INITIALIZE DIFFERENT CLASS OBJECTS.
$database = new database;
$job = new job;
// RECEIVE THE DATA IN JSON FORMAT.
$inputJSON = file_get_contents('php://input');
$obj = json_decode($inputJSON,TRUE);

$job_id = $obj['job_id'];
$user_id = $obj['user_id'];
$status = $obj['status'];

if( $obj['user_role'] == 2 )
{
	if( $obj['index'] == 4 )
	{
		$query6 = "SELECT job__reference.bkg_by_id as booking_made_by, job__reference.pax_id as paxId FROM job__reference INNER JOIN job ON job__reference.id=job.job_reference_id WHERE job.id='".$job_id."'";
		$result6 = mysql_fetch_array($database->query($query6));
		
		$push_message = 'Your driver for Job Id : '.$job_id.' is running late. Sorry for the inconvenience.';
		
		if( $result6['booking_made_by'] == $result6['paxId'] )
		{
			$push_title="Job alert";
			$job->commonGooglePushNotification($result6['booking_made_by'],$push_message,$push_title);
		}
		else
		{
			$push_title="Job alert";
			$job->commonGooglePushNotification($result6['booking_made_by'],$push_message,$push_title);
			$job->commonGooglePushNotification($result6['paxId'],$push_message,$push_title);
		}
		
		echo json_encode( array( "status" => 200 ));
	}
	else
	{
		$sql_query = mysql_query("SELECT * FROM drivers__movement_status WHERE movement_status='$status'");
		$fet_res = mysql_fetch_array($sql_query);
		$status_id = $fet_res['id'];
		
		$query6 = "SELECT job__reference.bkg_by_id as booking_made_by, job__reference.pax_id as paxId FROM job__reference INNER JOIN job ON job__reference.id=job.job_reference_id WHERE job.id='".$job_id."'";
		$result6 = mysql_fetch_array($database->query($query6));

		$sql_query2 = mysql_query("UPDATE job SET driver_movement_status='$status_id' WHERE id='$job_id' AND driver_id='$user_id'");

		if( $sql_query2 )
		{
			if( $status_id == 1 )
			{
				$push_message = 'Your driver for Job Id : '.$job_id.' is proceding for pickup.';
			}
			if( $status_id == 2 )
			{
				$push_message = 'Your driver for Job Id : '.$job_id.' has arrived for pickup.';
			}
			if( $status_id == 3 )
			{
				$push_message = 'Your trip (Job Id : '.$job_id.') has been started.';
			}
			if( $status_id == 4 )
			{
				$push_message = 'Your trip (Job Id : '.$job_id.') has been ended.';
			}
			
			if( $result6['booking_made_by'] == $result6['paxId'] )
			{
				$push_title="Job alert";
				$job->commonGooglePushNotification($result6['booking_made_by'],$push_message,$push_title);
			}
			else
			{
				$push_title="Job alert";
				$job->commonGooglePushNotification($result6['booking_made_by'],$push_message,$push_title);
				$job->commonGooglePushNotification($result6['paxId'],$push_message,$push_title);
			}
			
			echo json_encode( array('status'=>200,'status_message'=>'Driver movement status successfully updated.') );
		}
		else
		{
			echo json_encode( array('status'=>201,'status_message'=>'Status could not be updated.') );
		}
	}
}