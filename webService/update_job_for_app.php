<?php
ini_set("display_errors", 0);
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "chargeAccount.php");
require_once(CLASSES_PATH . "job.php");
require_once(CLASSES_PATH . "jobReference.php");
require_once(CLASSES_PATH . "address.php");
require_once(CLASSES_PATH . "mailer.php");
require_once(CLASSES_PATH . "vehicle.php");
require_once(CLASSES_PATH . "transaction.php");
require_once(CLASSES_PATH . "account.php");
require_once(CLASSES_PATH . "chargeAccount.php");
require_once(INCLUDE_PATH . "functions_date_time.php");
session_start();
$database = new database;

$charge_acc = new chargeAccount();
$user = new user();
$address = new address();
$job = new job();
$job_reference = new JobReference();
$mailer = new mailer();
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: POST, GET');
header('Content-type: application/json');
header('Access-Control-Allow-Headers:Content-Type');
//Recive inputes
$inputJSON = file_get_contents('php://input');
$obj = json_decode($inputJSON,TRUE);
/********************************************************************/  
if(isset($obj['form_submit'])) 
	{
		$j1_details = $job->getJobDetails($obj['j1_id']); 
		$job_ref_details = $job_reference->getJobReferenceDetails($j1_details['job_reference_id']);
		$pax_id = isset($obj['acc_passenger'])?$obj['acc_passenger']:'';  
		$contact_id = isset($obj['acc_contact'])?$obj['acc_contact']:'';
		$return_message .="<b>Following log generated for this Booking Reference</b><br/>";
		$push_message = 'Job Id : '.$obj['j1_id'].' has been edited.';
		$push_title = $obj['j1_id'];
		$job->commonGooglePushNotification($obj['acc_contact'],$push_message,$push_title);
		if($job_ref_details['job_src'] != $obj['job_src'] && $obj['job_src'] !='')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'job_src', $obj['job_src']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Booking Source Edited', $obj['job_src'], $job_ref_details['job_src']);
				$return_message .="Booking Source Edited from - ".$job_ref_details['job_src']." to  ".$obj['job_src']."<br/>";
			}
		if($job_ref_details['conf_to_client'] == '1')
			{
				if($job_ref_details['bkg_by_id'] == $obj['acc_contact']) // means booking by id has not been changed on edit
					{
					/*	$user_details = $user->getUserDetails($obj['acc_contact']);
						$mailer->sendMailToClientOnBooking($job_ref_details['id'], '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
						$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Email Confirmation Sent to Booking Made By', '', '');*/
						$contact_id = $obj['acc_contact'];
					}
				else
					{
						// do nothing here and send the confirmation if required in the step where a new bkg by id is getting added
					}
			}
		if($job_ref_details['conf_to_pax'] == '1')
			{
				if($job_ref_details['pax_id'] == $obj['acc_passenger']) // means pax has not been changed on edit
					{
						
						/*$user_details = $user->getUserDetails($obj['acc_passenger']);
						$mailer->sendMailToClientOnBooking($job_ref_details['id'], '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
						$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Email Confirmation Sent to Passenger', '', '');*/
					}
				else
					{
						// do nothing here and send the confirmation if required in the step where a pax is getting added
					}
			}
		
		if($job_ref_details['order_ref'] != $obj['order_ref'])
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'order_ref',$obj['order_ref']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Order Reference Edited', $obj['order_ref'], $job_ref_details['order_ref']);
				$return_message .="Order Reference Edited from - ".$job_ref_details['order_ref']." to  ".$obj['order_ref']."<br/>";
			}
		
		if($job_ref_details['acc_type'] != $obj['acc_type'])
			{
				$query1 = "SELECT * from variable__charge_acc_type where id = ".$job_ref_details['acc_type']."";
				$result1 = $database->query($query1);
				$row1 = mysql_fetch_array($result1);
				$old_details = $row1['details'];
				
				$query2 = "SELECT * from variable__charge_acc_type where id = ".$obj['acc_type']."";
				$result2 = $database->query($query2);
				$row2 = mysql_fetch_array($result2);
				$new_details = $row2['details'];
				
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'acc_type', $obj['acc_type']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Account Type Edited', $new_details, $old_details);
				$return_message .="Account Type Edited from - ".$old_details." to  ".$new_details."<br/>";
			}
			
		if($job_ref_details['std_id'] != $obj['std_id'])
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'std_id', $obj['std_id']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Student ID Edited', $obj['std_id'], $job_ref_details['std_id']);
				$return_message .="Student ID Edited from - ".$job_ref_details['std_id']." to  ".$obj['std_id']."<br/>";
			}
		if($job_ref_details['std_title'] != $obj['std_title'])
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'std_title', $obj['std_title']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Student Title Edited', $obj['std_title'], $job_ref_details['std_title']);
				$return_message .="Student Title Edited from - ".$job_ref_details['std_title']." to  ".$obj['std_title']."<br/>";
			}
		if($job_ref_details['std_fname'] != $obj['std_fname'])
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'std_fname', mysql_real_escape_string($obj['std_fname']));
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Student First Name Edited', $obj['std_fname'], $job_ref_details['std_fname']);
				$return_message .="Student First Name Edited from - ".$job_ref_details['std_fname']." to  ".$obj['std_fname']."<br/>";
			}
		if($job_ref_details['std_lname'] != $obj['std_lname'])
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'std_lname', mysql_real_escape_string($obj['std_lname']));
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Student Last Name Edited', $obj['std_lname'], $job_ref_details['std_lname']);
				$return_message .="Student Last Name Edited from - ".$job_ref_details['std_lname']." to  ".$obj['std_lname']."<br/>";
			}
		if($job_ref_details['std_ph'] != $obj['std_ph'])
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'std_ph', $obj['std_ph']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Student Phone Edited', $obj['std_ph'], $job_ref_details['std_ph']);
				$return_message .="Student Phone Edited from - ".$job_ref_details['std_ph']." to  ".$obj['std_ph']."<br/>";
			}
		if($job_ref_details['std_email'] != $obj['std_email'])
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'std_email', $obj['std_email']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Student Email Edited', $obj['std_email'], $job_ref_details['std_email']);
				$return_message .="Student Email Edited from - ".$job_ref_details['std_email']." to  ".$obj['std_email']."<br/>";
			}
		if($job_ref_details['under_18'] != $obj['under_18'] && $obj['under_18'] !='')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'under_18', $obj['under_18']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Student below 18 check Edited', $obj['under_18'], $job_ref_details['under_18']);
				$return_message .="Student Below 18 Check Edited from - ".$job_ref_details['under_18']." to  ".$obj['under_18']."<br/>";
			}
		if($job_ref_details['is_homestay'] != $obj['is_homestay'] && $obj['is_homestay'] !='')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'is_homestay', $obj['is_homestay']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Student Has Homestay Edited', $obj['is_homestay'], $job_ref_details['is_homestay']);
				$return_message .="Student Has Homestay Edited - ".$job_ref_details['is_homestay']." to  ".$obj['is_homestay']."<br/>";
			}
		if($job_ref_details['is_homestay'] != $obj['is_homestay'] && $obj['is_homestay'] !='')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'is_homestay', $obj['is_homestay']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Student Has Homestay Edited', $obj['is_homestay'], $job_ref_details['is_homestay']);
				$return_message .="Student Has Homestay Edited - ".$job_ref_details['is_homestay']." to  ".$obj['is_homestay']."<br/>";
			}
		if($job_ref_details['std_guar_title'] != $obj['std_guar_title'] && $obj['std_guar_title'] !='')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'std_guar_title', $obj['std_guar_title']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Student Gurdian Title Edited', $obj['std_guar_title'], $job_ref_details['std_guar_title']);
				$return_message .="Student Gurdaian Title Edited - ".$job_ref_details['std_guar_title']." to  ".$obj['std_guar_title']."<br/>";
			}	
		if($job_ref_details['std_guar_fname'] != $obj['std_guar_fname'] && $obj['std_guar_fname'] !='')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'std_guar_fname', $obj['std_guar_fname']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Student Gurdian First Name Edited', $obj['std_guar_fname'], $job_ref_details['std_guar_fname']);
				$return_message .="Student Gurdaian First Name Edited - ".$job_ref_details['std_guar_fname']." to  ".$obj['std_guar_fname']."<br/>";
			}
		if($job_ref_details['std_guar_lname'] != $obj['std_guar_lname'] && $obj['std_guar_lname'] !='')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'std_guar_lname', $obj['std_guar_lname']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Student Gurdian Last Name Edited', $obj['std_guar_lname'], $job_ref_details['std_guar_lname']);
				$return_message .="Student Gurdaian Last Name Edited - ".$job_ref_details['std_guar_lname']." to  ".$obj['std_guar_lname']."<br/>";
			}	
		if($job_ref_details['std_guar_email'] != $obj['std_guar_email'] && $obj['std_guar_email'] !='')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'std_guar_email', $obj['std_guar_email']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Student Gurdian Email Edited', $obj['std_guar_email'], $job_ref_details['std_guar_email']);
				$return_message .="Student Gurdaian Email Edited - ".$job_ref_details['std_guar_email']." to  ".$obj['std_guar_email']."<br/>";
			}	
		if($job_ref_details['std_guar_phone'] != $obj['std_guar_phone'] && $obj['std_guar_phone'] !='')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'std_guar_phone', $obj['std_guar_phone']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Student Gurdian Phone Edited', $obj['std_guar_phone'], $job_ref_details['std_guar_phone']);
				$return_message .="Student Gurdaian Phone Edited - ".$job_ref_details['std_guar_phone']." to  ".$obj['std_guar_phone']."<br/>";
			}		
		if($job_ref_details['on_behalf_title'] != $obj['on_behalf_title'] && $obj['on_behalf_title'] !='')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'on_behalf_title', $obj['on_behalf_title']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'On Behalf Title Edited', $obj['on_behalf_title'], $job_ref_details['on_behalf_title']);
				$return_message .="On Behalf Title Edited - ".$job_ref_details['on_behalf_title']." to  ".$obj['on_behalf_title']."<br/>";
			}	
		if($job_ref_details['on_behalf_fname'] != $obj['on_behalf_fname'] && $obj['on_behalf_fname'] !='')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'on_behalf_fname', $obj['on_behalf_fname']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'On Behalf Firstname Edited', $obj['on_behalf_fname'], $job_ref_details['on_behalf_fname']);
				$return_message .="On Behalf Firstname Edited - ".$job_ref_details['on_behalf_fname']." to  ".$obj['on_behalf_fname']."<br/>";
			}	
		if($job_ref_details['on_behalf_lname'] != $obj['on_behalf_lname'] && $obj['on_behalf_lname'] !='')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'on_behalf_lname', $obj['on_behalf_lname']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'On Behalf Lastname Edited', $obj['on_behalf_lname'], $job_ref_details['on_behalf_lname']);
				$return_message .="On Behalf Lastname Edited - ".$job_ref_details['on_behalf_lname']." to  ".$obj['on_behalf_lname']."<br/>";
			}
		if($job_ref_details['on_behalf_email'] != $obj['on_behalf_email'] && $obj['on_behalf_email'] !='')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'on_behalf_email', $obj['on_behalf_email']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'On Behalf Email Edited', $obj['on_behalf_email'], $job_ref_details['on_behalf_email']);
				$return_message .="On Behalf Email Edited - ".$job_ref_details['on_behalf_email']." to  ".$obj['on_behalf_email']."<br/>";
			}
		if($job_ref_details['on_behalf_phone'] != $obj['on_behalf_phone'] && $obj['on_behalf_phone'] !='')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'on_behalf_phone', $obj['on_behalf_phone']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'On Behalf Phone Edited', $obj['on_behalf_phone'], $job_ref_details['on_behalf_phone']);
				$return_message .="On Behalf Phone Edited - ".$job_ref_details['on_behalf_phone']." to  ".$obj['on_behalf_phone']."<br/>";
			}
		if($job_ref_details['agents_name'] != $obj['agents_name'] && $obj['agents_name'] !='')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'agents_name', $obj['agents_name']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Agents Name Edited', $obj['agents_name'], $job_ref_details['agents_name']);
				$return_message .="Agents Name Edited - ".$job_ref_details['agents_name']." to  ".$obj['agents_name']."<br/>";
			}	
		if($job_ref_details['agents_email'] != $obj['agents_email'] && $obj['agents_email'] !='')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'agents_email', $obj['agents_email']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Agents Email Edited', $obj['agents_email'], $job_ref_details['agents_email']);
				$return_message .="Agents Email Edited - ".$job_ref_details['agents_email']." to  ".$obj['agents_email']."<br/>";
			}
		if($job_ref_details['destination_contact_name'] != $obj['destination_contact_name'] && $obj['destination_contact_name'] !='')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'destination_contact_name', $obj['destination_contact_name']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Destination Contact Name Edited', $obj['destination_contact_name'], $job_ref_details['destination_contact_name']);
				$return_message .="Destination Contact Name Edited - ".$job_ref_details['destination_contact_name']." to  ".$obj['destination_contact_name']."<br/>";
			}
		if($job_ref_details['destination_contact_phone'] != $obj['destination_contact_phone'] && $obj['destination_contact_phone'] !='')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'destination_contact_phone', $obj['destination_contact_phone']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Destination Contact Phone Edited', $obj['destination_contact_phone'], $job_ref_details['destination_contact_phone']);
				$return_message .="Destination Contact Phone Edited - ".$job_ref_details['destination_contact_phone']." to  ".$obj['destination_contact_phone']."<br/>";
			}
		if($job_ref_details['destination_contact_email'] != $obj['destination_contact_email'] && $obj['destination_contact_email'] !='')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'destination_contact_email', $obj['destination_contact_email']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Destination Contact Email Edited', $obj['destination_contact_email'], $job_ref_details['destination_contact_email']);
				$return_message .="Destination Contact Email Edited - ".$job_ref_details['destination_contact_email']." to  ".$obj['destination_contact_email']."<br/>";
			}
			
		if($job_ref_details['conf_to_client'] != $obj['conf_to_client'])
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'conf_to_client', $obj['conf_to_client']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Confirmation to client Edited', $obj['conf_to_client'], $job_ref_details['conf_to_client']);
				$return_message .="Confirmation to client Edited from - ".$job_ref_details['conf_to_client']." to  ".$obj['conf_to_client']."<br/>";
			}

		if($job_ref_details['conf_to_pax'] != $obj['conf_to_pax'])
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'conf_to_pax', $obj['conf_to_pax']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Confirmation to passenger Edited', $obj['conf_to_pax'], $job_ref_details['conf_to_pax']);
				$return_message .="Confirmation to passenger Edited from - ".$job_ref_details['conf_to_pax']." to  ".$obj['conf_to_pax']."<br/>";
			}
			
		if(($job_ref_details['charge_acc_id'] != $obj['charge_acc']) && $obj['charge_acc'] != '') // this means another charge account has been selected from drop down list
			{
				$old_charge_acc = $charge_acc->getChargeAccountDetails($job_ref_details['charge_acc_id']);
				$new_charge_acc = $charge_acc->getChargeAccountDetails($obj['charge_acc']);
				
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'charge_acc_id', $obj['charge_acc']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Charge Account Edited', $new_charge_acc['account_name'], $old_charge_acc['account_name']);
				$return_message .="Charge Account Edited from - ".$old_charge_acc['account_name']." to  ".$new_charge_acc['account_name']."<br/>";
			}
		if($obj['charge_acc'] == '' && $obj['account_name'] != '') // A new new charge account has been entered
			{
				$charge_account = new ChargeAccount();
				$old_charge_account_details = $charge_account->getChargeAccountDetails($job_ref_details['charge_acc_id']);
				$new_charge_acc_id = $charge_account->addChargeAccount($obj['USER_ID'], $obj['account_type'], $obj['account_name'], $obj['myob_no']);
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'charge_acc_id', $new_charge_acc_id);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'New Charge Account Created', $obj['account_name'], $old_charge_account_details['account_name']);
			}
		
		if(($job_ref_details['bkg_by_id'] != $obj['acc_contact']) && $obj['acc_contact'] != '') // this means another booking by has been selected from drop down list
			{
				$old_bkg_by_details = 	$user->getUserDetails($job_ref_details['bkg_by_id']);
				$old_name 			= 	"".$old_bkg_by_details['title']." ".$old_bkg_by_details['fname']." ".$old_bkg_by_details['lname']."";
				
				$new_bkg_by_details = 	$user->getUserDetails($obj['acc_contact']);
				$new_name 			= 	"".$new_bkg_by_details['title']." ".$new_bkg_by_details['fname']." ".$new_bkg_by_details['lname']."";
				
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'bkg_by_id', $obj['acc_contact']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Booking by Edited', $new_name, $old_name);
				//send email if conf_to_client has been ticked
				if($obj['conf_to_client'] == '1')
					{
						$mailer->sendMailToClientOnBooking($job_ref_details['id'], '1', $new_bkg_by_details['email'], $new_bkg_by_details['title'], $new_bkg_by_details['fname'], $new_bkg_by_details['lname']);
						$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Confirmation email sent to the client', '', '');
					}
				$return_message .="Booking by Edited from - ".$old_name." to  ".$new_name."<br/>";
				$contact_id = $obj['acc_contact'];
			}
		if($obj['acc_contact'] == '' && $obj['bkg_by_fname'] != '') // A new booking made by entered
			{
				$old_bkg_by_details = 	$user->getUserDetails($job_ref_details['bkg_by_id']);
				$old_name 			= 	"".$old_bkg_by_details['title']." ".$old_bkg_by_details['fname']." ".$old_bkg_by_details['lname']."";
				$new_name 			= 	"".$obj['bkg_by_title']." ".$obj['bkg_by_fname']." ".$obj['bkg_by_lname']."";
				//create this new booking made by
				$new_bkg_made_by_id = $user->addUser($obj['bkg_by_type'], $obj['bkg_by_title'], $obj['bkg_by_fname'], $obj['bkg_by_lname'], $obj['bkg_by_password'], $obj['bkg_by_email'], $obj['bkg_by_mobile'], $obj['bkg_by_phone'], $obj['bkg_by_preference']);
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'bkg_by_id', $new_bkg_made_by_id);
				//add this to booking reference log
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'New Booking Made By Created', $new_name, $old_name);
				//send mail to this booking made by if conf_to_client is ticked
				if($obj['conf_to_client'] == '1')
					{
						$mailer->sendMailToClientOnBooking($job_ref_details['id'], '1', $obj['bkg_by_email'], $obj['bkg_by_title'], $obj['bkg_by_fname'], $obj['bkg_by_lname']);
						$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Confirmation email sent to new client', '', '');
					}
				$return_message .="Booking by Added from - ".$old_name." to  ".$new_name."<br/>";
				$contact_id = $new_bkg_made_by_id;
			}
		if(($job_ref_details['pax_id'] != $obj['acc_passenger']) && $obj['acc_passenger'] != '') // this means another pax has been selected from drop down list
			{
				//$pax_id = $obj['acc_passenger'];
				$old_pax_details 	= 	$user->getUserDetails($job_ref_details['pax_id']);
				$old_name 			= 	"".$old_pax_details['title']." ".$old_pax_details['fname']." ".$old_pax_details['lname']."";
				
				$new_pax_details 	= 	$user->getUserDetails($obj['acc_passenger']);
				$new_name 			= 	"".$new_pax_details['title']." ".$new_pax_details['fname']." ".$new_pax_details['lname']."";
				
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'pax_id', $obj['acc_passenger']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Pax Edited', $new_name, $old_name);
				//send email if conf_to_pax has been ticked
				if($obj['conf_to_pax'] == '1')
					{
						$mailer->sendMailToClientOnBooking($job_ref_details['id'], '1', $new_pax_details['email'], $new_pax_details['title'], $new_pax_details['fname'], $new_pax_details['lname']);
						$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Confirmation email sent to the passenger', '', '');
					}
				$return_message .="Pax Edited from - ".$old_name." to  ".$new_name."<br/>";
			}
		if($obj['acc_passenger'] == '' && $obj['new_pax_fname'] != '') // A new pax entered
			{
				$old_pax_details 	= 	$user->getUserDetails($job_ref_details['pax_id']);
				$old_name 			= 	"".$old_pax_details['title']." ".$old_pax_details['fname']." ".$old_pax_details['lname']."";
				$new_name 			= 	"".$obj['new_pax_title']." ".$obj['new_pax_fname']." ".$obj['new_pax_lname']."";
				//create this new booking made by
				$new_pax_id = $user->addUser($obj['new_pax_type'], $obj['new_pax_title'], $obj['new_pax_fname'], $obj['new_pax_lname'], $obj['new_pax_password'], $obj['new_pax_email'], $obj['new_pax_mobile'], $obj['new_pax_phone'], $obj['new_pax_preference']);
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'pax_id', $new_pax_id);
				//add this to booking reference log
				$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'New Passenger Created', $new_name, $old_name);
				//send mail to this booking made by if conf_to_pax is ticked
				if($obj['conf_to_pax'] == '1')
					{
						$mailer->sendMailToClientOnBooking($job_ref_details['id'], '1', $obj['new_pax_email'], $obj['new_pax_title'], $obj['new_pax_fname'], $obj['new_pax_lname']);
						$job_reference->addJobReferenceLog($job_ref_details['id'], $obj['USER_ID'], 'Confirmation email sent to the new passenger', '', '');
					}
				$pax_id=$new_pax_id;
				$return_message .="Passenger Added from - ".$old_name." to  ".$new_name."<br/>";
			}
		if($job_ref_details['charge_mode'] != $obj['charge_mode'] && $obj['charge_mode'] != '')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'charge_mode', $obj['charge_mode']);
				$return_message .="Charge Mode Edited from - ".$old_details." to  ".$new_details."<br/>";
			}
		if($job_ref_details['job_type'] != $obj['job_type'] ) //means the booking has been changed to a return booking
			{
				$old_job_type = ($job_ref_details['job_type'] == '1')?'One Way':'Return';
				$new_job_type = ($obj['job_type'] == '1')?'One Way':'Return';
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'job_type', $obj['job_type']);
				$return_message .="Job Type Edited from - ".$old_job_type." to  ".$new_job_type."<br/>";
			}

//--------------------------------------------------------------------------------------------------------------------------------------------------		
		
		if($job_ref_details['is_homestay'] != $obj['is_homestay'] && $obj['is_homestay'] != '')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'is_homestay', $obj['is_homestay']);
				$return_message .="Is Home Stay Edited - ".$job_ref_details['is_homestay']." to  ".$obj['is_homestay']."<br/>";
			}
		$return_message .="<b>Following log generated for Booking ID - ".$j1_details['id']."</b><br/>";
		if($j1_details['job_status'] != '80' && $obj['no_show_j1'] =='1' )// This means Change to No Show (80)
			{
				$job->updateJob($j1_details['id'], 'job_status', '80');
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Booking status edited', 'No Show', 'Confirm');
				
				if($job_ref_details['acc_type'] == '3') //University
					{
						$mailer->sendUniversityMail($job_ref_details['id'], '9');
					}
			
				$return_message .="Booking Status Edited from - Confirmed to  No Show <br/>";
			}
		if($j1_details['job_status'] == '80' && $obj['no_show_j1'] !='1' )
			{
				$job->updateJob($j1_details['id'], 'job_status', '20'); // This means Undo No Show
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Booking status edited', 'Confirm', 'No Show');
				$return_message .="Booking Status Edited from - No Show to  Confirm <br/>";
			}
		if($j1_details['job_date'] != $obj['j1_date_cap'])
			{
				$job->updateJob($j1_details['id'], 'job_date', $obj['j1_date_cap']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Booking Date Edited', formatDate($obj['j1_date_cap'],1), formatDate($j1_details['job_date'],1));
				$return_message .="Booking Date Edited from - ".formatDate($j1_details['job_date'],1)." to  ".formatDate($obj['j1_date_cap'],1)."<br/>";
			}	
		if($j1_details['job_time'] != ''.$obj['j1_time'].':00')
			{
				$job->updateJob($j1_details['id'], 'job_time', $obj['j1_time']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Booking Time Edited', calculateTimeInAmPmFormatWithoutSeconds($obj['j1_time']), calculateTimeInAmPmFormatWithoutSeconds($j1_details['job_time']));
				$return_message .="Booking Time Edited from - ".calculateTimeInAmPmFormatWithoutSeconds($j1_details['job_time'])." to  ".calculateTimeInAmPmFormatWithoutSeconds($obj['j1_time'])."<br/>";
			}
		
		//THIS IS ADDED FOR THE UNIVERSITIES ONLY
		/*
		if(	($j1_details['job_date'] 	!= 	$obj['j1_date_cap'])
				||	($j1_details['job_time'] 	!= 	''.$obj['j1_time'].':00')
				||	($j1_details['to_line1'] 	!= 	$obj['to_line1'])
				||	($j1_details['to_line2'] 	!= 	$obj['to_line2'])
				||	($j1_details['to_sub'] 		!= 	$obj['to_sub'])
				
			)
			{
				if($job_ref_details['acc_type'] == '3')
					{
						$mailer->sendUniversityMail($job_ref_details['id'], '7', '1');
					}
			}
		*/
		
		
		if($obj['j1_driver_status'] !='') // driver status not blank
			{
				$old_driver_status = $job->getDriverStatusDetails($j1_details['driver_status']);
				
				if($obj['j1_driver_status'] =='1') //Allocate
					{
						$driver_details = $user->getUserDetails($obj['j1_driver']); // get details of this driver
						$job->updateJob($j1_details['id'], 'driver_status', '1'); // change the driver status to Allocate
						$job->updateJob($j1_details['id'], 'driver_id', $obj['j1_driver']); // Put this driver in job table
						$job->updateJob($j1_details['id'], 'driver_price', $obj['j1_driver_price']); // Put the driver price in the job table
						$job->addNewDriverToJob($j1_details['id'], $obj['j1_driver'], $obj['j1_driver_price'], '', '', '', '', '', '', '', '', '',''); // Add this to job__driver table
						
						$job->addJobDriverLog($j1_details['id'], $obj['USER_ID'], 'Driver Status Edited', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$obj['j1_driver_price'].'', '');
						$return_message .="Driver Status Edited from - ".$old_driver_status['details']." to  ALLOCATED<br/>";
					}
				if($obj['j1_driver_status'] =='2' && !empty($obj['j1_driver']) && $obj['j1_driver_price'] !='') //Offered/Sent
					{
						$job->updateJob($j1_details['id'], 'driver_status', '2'); // change the driver status to Sent
						
								$driver_details = $user->getUserDetails($obj['j1_driver']); // get details of this driver	
								$job->addNewDriverToJob($j1_details['id'], '', '', $obj['j1_driver'], $obj['j1_driver_price'], '', '', '', '', '', '', '',''); // Add this to job__driver table
								$job->addJobDriverLog($j1_details['id'], $obj['USER_ID'], 'Job Offered', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$obj['j1_driver_price'].'', '');
								$mailer->sendJobOfferToDriver($obj['j1_driver'], $j1_details['id'], $obj['j1_driver_price'],htmlspecialchars($obj['j1_driver_notes'])); // Send job offer to the driver
								$push_message = 'Job Id : '.$j1_details['id'].' has been offered to you.';
								$job->commonGooglePushNotification($obj['j1_driver'],$push_message);
								$return_message .="Job offer sent to selected drivers<br/>";
					}
				if($obj['j1_driver_status'] =='2' && !empty($obj['j1_driver_group']) && $obj['j1_driver_group'] !='') //Offered/Sent
					{
						$job->updateJob($j1_details['id'], 'driver_status', '2'); // change the driver status to Sent
						$query = "SELECT * from drivers__group_drivers where group_id='".$obj['j1_driver_group']."'";
						$result = $database->query($query);
						while($row = mysql_fetch_array($result))
							{
								$driver_details = $user->getUserDetails($row['user_id']); // get details of this driver	
								$job->addNewDriverToJob($j1_details['id'], '', '', $row['user_id'], $obj['j1_driver_price'], '', '', '', '', '', '', '',''); // Add this to job__driver table
								$job->addJobDriverLog($j1_details['id'], $obj['USER_ID'], 'Job Offered', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$obj['j1_driver_price'].'', '');
								$mailer->sendJobOfferToDriver($row['user_id'], $j1_details['id'], $obj['j1_driver_price'],htmlspecialchars($obj['j1_driver_notes'])); // Send job offer to the driver
							}
						$return_message .="Job offer sent to Group drivers<br/>";
					}
				if($obj['j1_driver_status'] == '3' && !empty($obj['j1_driver']) && $obj['j1_driver_price'] !='') //Accepted
					{
						$job->updateJob($j1_details['id'], 'driver_status', '3'); // change the driver status to Sent
						$job->updateJob($j1_details['id'], 'driver_id', $obj['j1_driver']); // Put this driver in job table
						$job->updateJob($j1_details['id'], 'driver_price', $obj['j1_driver_price']); // Put the driver price in the job table
						$job->addNewDriverToJob($j1_details['id'], '', '', '', '', $obj['j1_driver'], $obj['j1_driver_price'], '', '', '', '', '',''); // Add this to job__driver table
						$driver_details = $user->getUserDetails($obj['j1_driver']); // get details of this driver
						$job->addJobDriverLog($j1_details['id'], $obj['USER_ID'], 'Job Accepted (Internal)', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$obj['j1_driver_price'].'', '');
						
						$mailer->sendJobDetailsToDriver($j1_details['id'], $obj['j1_driver'], $obj['j1_driver_price'],htmlspecialchars($obj['j1_driver_notes']));		
					} 
				/*if($obj['j1_driver_status'] =='4' || $obj['j1_driver_status'] =='5') //Rogered/Paid //72238
					{
						$job_driver_details = $job->getJobDriverDetails($obj['j1_job_driver_id']);
						
						if($obj['j1_driver_notes'] != $job_driver_details['driver_notes'])
						{ 
							$job->updateJobDriver($obj['j1_job_driver_id'], 'driver_notes', $obj['j1_driver_notes']);
							$job->addJobDriverLog($j1_details['id'], $obj['USER_ID'], 'Driver notes edited from', ''.$job_driver_details['driver_notes'].' ', $obj['j1_driver_notes']);
						}
					}*/
			}
		if(($obj['j1_driver_notes'] != $j1_details['driver_notes']) && $obj['ROLE_ID'] == '1')
			{ 
				$job->updateJob($j1_details['id'], 'driver_notes', htmlspecialchars($obj['j1_driver_notes']));
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Driver notes edited from', htmlspecialchars($obj['j1_driver_notes']), $j1_details['driver_notes']);
				$return_message .="Driver notes edited from - ".$j1_details['driver_notes']." to  ".htmlspecialchars($obj['j1_driver_notes'])."<br/>";
			}
		if($j1_details['frm_flight_no'] != $obj['j1_flight_no'])
			{
				$job->updateJob($j1_details['id'], 'frm_flight_no', $obj['j1_flight_no']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'From Flight Number Edited', $obj['j1_flight_no'], $j1_details['frm_flight_no']);
				$return_message .="From Flight Number  Edited from - ".$j1_details['frm_flight_no']." to  ".$obj['j1_flight_no']."<br/>";
			}
		
		if($j1_details['frm_line1'] != $obj['j1_line_1'])
			{
				$job->updateJob($j1_details['id'], 'frm_line1', mysql_real_escape_string($obj['j1_line_1']));
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'From Line 1 Edited', $obj['j1_line_1'], $j1_details['frm_line1']);
				$return_message .="From Line 1 Edited from - ".$j1_details['frm_line1']." to  ".$obj['j1_line_1']."<br/>";
			}
		
		if($j1_details['frm_line2'] != $obj['j1_line_2'])
			{
				$job->updateJob($j1_details['id'], 'frm_line2', mysql_real_escape_string($obj['j1_line_2']));
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'From Line 2 Edited', $obj['j1_line_2'], $j1_details['frm_line2']);
				$return_message .="From Line 2 Edited from - ".$j1_details['frm_line2']." to  ".$obj['j1_line_2']."<br/>";
			}
		
		if($j1_details['frm_sub'] != $obj['j1_sub'])
			{
				$job->updateJob($j1_details['id'], 'frm_sub', mysql_real_escape_string($obj['j1_sub']));
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'From Suburb Edited', $obj['j1_sub'], $j1_details['frm_sub']);
				$return_message .="From Suburb Edited from - ".$j1_details['frm_sub']." to  ".$obj['j1_sub']."<br/>";
			}
		
		if($j1_details['frm_pc'] != $obj['j1_pc'])
			{
				$job->updateJob($j1_details['id'], 'frm_pc', $obj['j1_pc']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'From Postcode Edited', $obj['j1_pc'], $j1_details['frm_pc']);
				$return_message .="From Postcode Edited from - ".$j1_details['frm_pc']." to  ".$obj['j1_pc']."<br/>";
			}
		
		if($j1_details['frm_state'] != $obj['j1_state'])
			{
				$job->updateJob($j1_details['id'], 'frm_state', $obj['j1_state']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'From State Edited', $obj['j1_state'], $j1_details['frm_state']);
				$return_message .="From State Edited to <b>".$obj['j1_state']."</b><br/>";
			}
			
		if($j1_details['to_flight_no'] != $obj['j1_to_flight_no'])
			{
				$job->updateJob($j1_details['id'], 'to_flight_no', $obj['j1_to_flight_no']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'To Flight Number Edited', $obj['j1_to_flight_no'], $j1_details['to_flight_no']);
				$return_message .="to Flight Number  Edited from - ".$j1_details['to_flight_no']." to  ".$obj['j1_to_flight_no']."<br/>";
			}
		
		if($j1_details['to_line1'] != $obj['j1_to_line_1'])
			{
				$job->updateJob($j1_details['id'], 'to_line1', mysql_real_escape_string($obj['j1_to_line_1']));
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'To Line 1 Edited', $obj['j1_to_line_1'], $j1_details['to_line1']);
				$return_message .="To Line 1 Edited from - ".$j1_details['to_line1']." to  ".$obj['j1_to_line_1']."<br/>";
			}
		
		if($j1_details['to_line2'] != $obj['j1_to_line_2'])
			{
				$job->updateJob($j1_details['id'], 'to_line2', mysql_real_escape_string($obj['j1_to_line_2']));
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'To Line 2 Edited', $obj['j1_to_line_2'], $j1_details['to_line2']);
				$return_message .="To Line 2 Edited from - ".$j1_details['to_line2']." to  ".$obj['j1_to_line_2']."<br/>";
			}
		
		if($j1_details['to_sub'] != $obj['j1_to_sub'])
			{
				$job->updateJob($j1_details['id'], 'to_sub', mysql_real_escape_string($obj['j1_to_sub']));
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'To Suburb Edited', $obj['j1_to_sub'], $j1_details['to_sub']);
				$return_message .="To Suburb Edited from - ".$j1_details['to_sub']." to  ".$obj['j1_to_sub']."<br/>";
			}
		
		if($j1_details['to_pc'] != $obj['j1_to_pc'])
			{
				$job->updateJob($j1_details['id'], 'to_pc', $obj['j1_to_pc']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'To Postcode Edited', $obj['j1_to_pc'], $j1_details['to_pc']);
				$return_message .="To Postcode Edited from - ".$j1_details['to_pc']." to  ".$obj['j1_to_pc']."<br/>";
			}
		
		if($j1_details['to_state'] != $obj['j1_state'])
			{
				$job->updateJob($j1_details['id'], 'to_state', $obj['j1_state']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'To State Edited', $obj['j1_state'], $j1_details['to_state']);
				$return_message .="To State Edited to <b>".$obj['j1_state']."</b><br/>";
			}
		if($j1_details['frm_via_line1'] != $obj['j1_via_line_1'])
			{
				$job->updateJob($j1_details['id'], 'frm_via_line1', mysql_real_escape_string($obj['j1_via_line_1']));
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'From Via Line 1 Edited', $obj['j1_via_line_1'], $j1_details['frm_via_line1']);
				$return_message .="From Via Line 1 Edited from - ".$j1_details['frm_via_line1']." to  ".$obj['j1_via_line_1']."<br/>";
			}
		
		if($j1_details['frm_via_line2'] != $obj['j1_via_line_2'])
			{
				$job->updateJob($j1_details['id'], 'frm_via_line2', mysql_real_escape_string($obj['j1_via_line_2']));
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'From Via Line 2 Edited', $obj['j1_via_line_2'], $j1_details['frm_via_line2']);
				$return_message .="From Via Line 2 Edited from - ".$j1_details['frm_via_line2']." to  ".$obj['j1_via_line_2']."<br/>";
			}
		
		if($j1_details['frm_via_sub'] != $obj['j1_via_sub'])
			{
				$job->updateJob($j1_details['id'], 'frm_via_sub', mysql_real_escape_string($obj['j1_via_sub']));
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'From Via Suburb Edited', $obj['j1_via_sub'], $j1_details['frm_via_sub']);
				$return_message .="From Via Suburb Edited from - ".$j1_details['frm_via_sub']." to  ".$obj['j1_via_sub']."<br/>";
			}
		
		if($j1_details['frm_via_pc'] != $obj['j1_via_pc'])
			{
				$job->updateJob($j1_details['id'], 'frm_via_pc', $obj['j1_via_pc']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'From Via Postcode Edited', $obj['j1_via_pc'], $j1_details['frm_via_pc']);
				$return_message .="From Via Postcode Edited from - ".$j1_details['frm_via_pc']." to  ".$obj['j1_via_pc']."<br/>";
			}

		if($j1_details['frm_via_state'] != $obj['j1_state'] && $obj['j1_via_sub'] != '')
			{
				$job->updateJob($j1_details['id'], 'frm_via_state', $obj['j1_state']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'To Via State Edited', $obj['j1_state'], $j1_details['to_via_state']);
				$return_message .="To Via State Edited to ".$obj['j1_state']."<br/>";
			}
			
		if($j1_details['to_via_line1'] != $obj['j1_to_via_line_1'])
			{
				$job->updateJob($j1_details['id'], 'to_via_line1', mysql_real_escape_string($obj['j1_to_via_line_1']));
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'To Via Line 1 Edited', $obj['j1_to_via_line_1'], $j1_details['to_via_line1']);
				$return_message .="To Via Line 1 Edited from - ".$j1_details['to_via_line1']." to  ".$obj['j1_to_via_line_1']."<br/>";
			}
		
		if($j1_details['to_via_line2'] != $obj['j1_to_via_line_2'])
			{
				$job->updateJob($j1_details['id'], 'to_via_line2', mysql_real_escape_string($obj['j1_to_line_2']));
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'To Via Line 2 Edited', $obj['j1_to_via_line_2'], $j1_details['to_via_line2']);
				$return_message .="To Via Line 2 Edited from - ".$j1_details['to_via_line2']." to  ".$obj['j1_to_via_line_2']."<br/>";
			}
		
		if($j1_details['to_via_sub'] != $obj['j1_to_via_sub'])
			{
				$job->updateJob($j1_details['id'], 'to_via_sub', mysql_real_escape_string($obj['j1_to_via_sub']));
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'To Via Suburb Edited', $obj['j1_to_via_sub'], $j1_details['to_via_sub']);
				$return_message .="To Via Suburb Edited from - ".$j1_details['to_via_sub']." to  ".$obj['j1_to_via_sub']."<br/>";
			}
		
		if($j1_details['to_via_pc'] != $obj['j1_to_via_pc'])
			{
				$job->updateJob($j1_details['id'], 'to_via_pc', $obj['j1_to_via_pc']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'To Via Postcode Edited', $obj['j1_to_via_pc'], $j1_details['to_via_pc']);
				$return_message .="To Via Postcode Edited from - ".$j1_details['to_via_pc']." to  ".$obj['j1_to_via_pc']."<br/>";
			}
			
		if($j1_details['to_via_state'] != $obj['j1_state'] && $obj['j1_to_via_sub'] != '')
			{
				$job->updateJob($j1_details['id'], 'to_via_state', mysql_real_escape_string($obj['j1_state']));
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'To Via State Edited', $obj['j1_state'], $j1_details['to_via_state']);
				$return_message .="To Via State Edited to ".$obj['j1_state']."<br/>";
			}
		
		if($j1_details['car_id'] != $obj['j1_car_id'])
			{
				$query1 = "SELECT * from variable__car_type where id = ".$j1_details['car_id']."";
				$result1 = $database->query($query1);
				$row1 = mysql_fetch_array($result1);
				$old_details = $row1['details'];
				
				$query2 = "SELECT * from variable__car_type where id = ".$obj['j1_car_id']."";
				$result2 = $database->query($query2);
				$row2 = mysql_fetch_array($result2);
				$new_details = $row2['details'];
				
				$job->updateJob($j1_details['id'], 'car_id', $obj['j1_car_id']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Car Type Edited', $new_details, $old_details);
				$return_message .="Car Type Edited from - ".$old_details." to  ".$new_details."<br/>";
			}
		if(isset($obj['j1_lat']))
		{
			$job->updateJob($j1_details['id'], 'pick_up_lat', $obj['j1_lat']);
			$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Latitude. Edited', $obj['j1_lat'], $j1_details['pick_up_lat']);
			$return_message .="Latitude. Edited from - ".$j1_details['pick_up_lat']." to  ".$obj['j1_lat']."<br/>";
		}
		if(isset($obj['j1_long']))
		{
			$job->updateJob($j1_details['id'], 'pick_up_lng', $obj['j1_long']);
			$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Longitude. Edited', $obj['j1_long'], $j1_details['pick_up_lng']);
			$return_message .="Latitude. Edited from - ".$j1_details['pick_up_lng']." to  ".$obj['j1_long']."<br/>";
		}
		if($j1_details['pax_nos'] != $obj['j1_pax_nos'])
			{
				$job->updateJob($j1_details['id'], 'pax_nos', $obj['j1_pax_nos']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Pax Nos. Edited', $obj['j1_pax_nos'], $j1_details['pax_nos']);
				$return_message .="Pax Nos. Edited from - ".$j1_details['pax_nos']." to  ".$obj['j1_pax_nos']."<br/>";
			}
		if($j1_details['luggage'] != $obj['j1_luggage'])
			{
				$job->updateJob($j1_details['id'], 'luggage', $obj['j1_luggage']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Luggage Nos. Edited', $obj['j1_luggage'], $j1_details['luggage']);
				$return_message .="Luggage Nos. Edited from - ".$j1_details['luggage']." to  ".$obj['j1_luggage']."<br/>";
			}
		if($j1_details['baby_seat'] != $obj['j1_baby_seats'])
			{
				$job->updateJob($j1_details['id'], 'baby_seat', $obj['j1_baby_seats']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Baby Seat Nos. Edited', $obj['j1_baby_seats'], $j1_details['baby_seat']);
				$return_message .="Baby Seat Nos. Edited from - ".$j1_details['baby_seat']." to  ".$obj['j1_baby_seats']."<br/>";
			}
		if($j1_details['booster_seat'] != $obj['j1_booster_seats'])
			{
				$job->updateJob($j1_details['id'], 'booster_seat', $obj['j1_booster_seats']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Booster Seat Nos. Edited', $obj['j1_booster_seats'], $j1_details['booster_seat']);
				$return_message .="Booster Seat Nos. Edited from - ".$j1_details['booster_seat']." to  ".$obj['j1_booster_seats']."<br/>";
			}
		if($j1_details['baby_capsule'] != $obj['j1_baby_capsules'])
			{
				$job->updateJob($j1_details['id'], 'baby_capsule', $obj['j1_baby_capsules']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Baby Capsule Nos. Edited', $obj['j1_baby_capsules'], $j1_details['baby_capsule']);
				$return_message .="Baby Capsule Nos. Edited from - ".$j1_details['baby_capsule']." to  ".$obj['j1_baby_capsules']."<br/>";
			}
		if($j1_details['baby_capsule'] != $obj['j1_baby_capsules'])
			{
				$job->updateJob($j1_details['id'], 'baby_capsule', $obj['j1_baby_capsules']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Baby Capsule Nos. Edited', $obj['j1_baby_capsules'], $j1_details['baby_capsule']);
				$return_message .="Baby Capsule Nos. Edited from - ".$j1_details['baby_capsule']." to  ".$obj['j1_baby_capsules']."<br/>";
			}
		
		if($j1_details['kms'] != $obj['j1_kms'] && $obj['j1_kms'] != '')
			{
				$job->updateJob($j1_details['id'], 'kms', $obj['j1_kms']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Kilometers Edited', $obj['j1_kms'], $j1_details['kms']);
				$return_message .="Kilometers Edited from - ".$j1_details['kms']." to  ".$obj['j1_kms']."<br/>";
			}
		
		if($j1_details['fare'] != $obj['j1_fare'] && $obj['j1_fare'] != '')
			{
				$job->updateJob($j1_details['id'], 'fare', $obj['j1_fare']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Fare Edited', $obj['j1_fare'], $j1_details['fare']);
				$return_message .="Fare Edited from - ".$j1_details['fare']." to  ".$obj['j1_fare']."<br/>";
			}
		if($j1_details['inter'] != $obj['j1_inter'] && $obj['j1_inter'] != '')
			{
				$job->updateJob($j1_details['id'], 'inter', $obj['j1_inter']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'International Fare Edited', $obj['j1_inter'], $j1_details['inter']);
				$return_message .="International Fare Edited from - ".$j1_details['inter']." to  ".$obj['j1_inter']."<br/>";
			}
		if($j1_details['ed'] != $obj['j1_ed'] && $obj['j1_ed'] != '')
			{
				$job->updateJob($j1_details['id'], 'ed', $obj['j1_ed']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Extra Drop/ Pickup Fare Edited', $obj['j1_ed'], $j1_details['ed']);
				$return_message .="Extra Drop/ Pickup Fare Edited from - ".$j1_details['ed']." to  ".$obj['j1_ed']."<br/>";
			}
		if($j1_details['wait'] != $obj['j1_wait'] && $obj['j1_wait'] != '')
			{
				$job->updateJob($j1_details['id'], 'wait', $obj['j1_wait']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Waiting Charges Edited', $obj['j1_wait'], $j1_details['wait']);
				$return_message .="Waiting Charges Edited from - ".$j1_details['wait']." to  ".$obj['j1_wait']."<br/>";
			}
		if($j1_details['tolls'] != $obj['j1_tolls'] && $obj['j1_tolls'] != '')
			{
				$job->updateJob($j1_details['id'], 'tolls', $obj['j1_tolls']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Toll Charges Edited', $obj['j1_tolls'], $j1_details['tolls']);
				$return_message .="Toll Charges Edited from - ".$j1_details['tolls']." to  ".$obj['j1_tolls']."<br/>";
			}
		if($j1_details['bs'] != $obj['j1_bs'] && $obj['j1_bs'] != '')
			{
				$job->updateJob($j1_details['id'], 'bs', $obj['j1_bs']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Baby Seat Charges Edited', $obj['j1_bs'], $j1_details['bs']);
				$return_message .="Baby Seat Charges Edited from - ".$j1_details['bs']." to  ".$obj['j1_bs']."<br/>";
			}
		if($j1_details['park'] != $obj['j1_park'] && $obj['j1_park'] != '')
			{
				$job->updateJob($j1_details['id'], 'park', $obj['j1_park']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Parking Charges Edited', $obj['j1_park'], $j1_details['park']);
				$return_message .="Parking Charges Edited from - ".$j1_details['park']." to  ".$obj['j1_park']."<br/>";
			}
		if($j1_details['ah'] != $obj['j1_ah'] && $obj['j1_ah'] != '')
			{
				$job->updateJob($j1_details['id'], 'ah', $obj['j1_ah']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'After Hour Charges Edited', $obj['j1_ah'], $j1_details['ah']);
				$return_message .="After Hour Charges Edited from - ".$j1_details['ah']." to  ".$obj['j1_ah']."<br/>";
			}
		if($j1_details['me'] != $obj['j1_me'] && $obj['j1_me'] != '')
			{
				$job->updateJob($j1_details['id'], 'me', $obj['j1_me']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Major Event Charges Edited', $obj['j1_me'], $j1_details['me']);
				$return_message .="Major Event Charges Edited from - ".$j1_details['me']." to  ".$obj['j1_me']."<br/>";
			}
		if($j1_details['alc'] != $obj['j1_alc'] && $obj['j1_alc'] != '')
			{
				$job->updateJob($j1_details['id'], 'alc', $obj['j1_alc']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Alcohol Charges Edited', $obj['j1_alc'], $j1_details['alc']);
				$return_message .="Alcohol Charges Edited from - ".$j1_details['alc']." to  ".$obj['j1_alc']."<br/>";
			}
		if($j1_details['fc'] != $obj['j1_fc'] && $obj['j1_fc'] != '')
			{
				$job->updateJob($j1_details['id'], 'fc', $obj['j1_fc']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Food/Coffee Charges Edited', $obj['j1_fc'], $j1_details['fc']);
				$return_message .="Food/Coffee Charges Edited from - ".$j1_details['fc']." to  ".$obj['j1_fc']."<br/>";
			}
		if($j1_details['oth'] != $obj['j1_oth'] && $obj['j1_oth'] != '')
			{
				$job->updateJob($j1_details['id'], 'oth', $obj['j1_oth']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Other Charges Edited', $obj['j1_oth'], $j1_details['oth']);
				$return_message .="Other Charges Edited from - ".$j1_details['oth']." to  ".$obj['j1_oth']."<br/>";
			}
		if($j1_details['tot_fare'] != $obj['j1_tot_fare'] && $obj['j1_tot_fare'] != '')
			{
				$job->updateJob($j1_details['id'], 'tot_fare', $obj['j1_tot_fare']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Total Fare Edited', $obj['j1_tot_fare'], $j1_details['tot_fare']);
				$return_message .="Total Fare Edited from - ".$j1_details['tot_fare']." to  ".$obj['j1_tot_fare']."<br/>";
			}
		if($j1_details['drv_fee'] != $obj['j1_drv_fee'] && $obj['j1_drv_fee'] != '')
			{
				$job->updateJob($j1_details['id'], 'drv_fee', $obj['j1_drv_fee']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Total Driver Fee Edited', $obj['j1_drv_fee'], $j1_details['drv_fee']);
				$return_message .="Total Driver Fee Edited from - ".$j1_details['drv_fee']." to  ".$obj['j1_drv_fee']."<br/>";
			}
		if($j1_details['oth_exp'] != $obj['j1_oth_exp'] && $obj['j1_oth_exp'] != '')
			{
				$job->updateJob($j1_details['id'], 'oth_exp', $obj['j1_oth_exp']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Total Other Expenses Edited', $obj['j1_oth_exp'], $j1_details['oth_exp']);
				$return_message .="Total Other Expenses Edited from - ".$j1_details['oth_exp']." to  ".$obj['j1_oth_exp']."<br/>";
			}
		if($j1_details['profit'] != $obj['j1_profit'] && $obj['j1_profit'] != '')
			{
				$job->updateJob($j1_details['id'], 'profit', $obj['j1_profit']);
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Total Profit Edited', $obj['j1_profit'], $j1_details['profit']);
				$return_message .="Total Profit Edited from - ".$j1_details['profit']." to  ".$obj['j1_profit']."<br/>";
			}
		if($j1_details['ext_notes'] != $obj['j1_ext_notes'])
			{
				$job->updateJob($j1_details['id'], 'ext_notes', htmlspecialchars($obj['j1_ext_notes']));
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'External Notes Edited', htmlspecialchars($obj['j1_ext_notes']), $j1_details['ext_notes']);
				$return_message .="External Notes Edited from - ".$j1_details['ext_notes']." <b>to</b>  ".htmlspecialchars($obj['j1_ext_notes'])."<br/>";
			}
		if($j1_details['int_notes'] != $obj['j1_int_notes'] && $obj['ROLE_ID'] == '1')
			{
				$job->updateJob($j1_details['id'], 'int_notes', htmlspecialchars($obj['j1_int_notes']));
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Internal Notes Edited', htmlspecialchars($obj['j1_int_notes']), $j1_details['int_notes']);
				$return_message .="Internal Notes Edited from - ".$j1_details['int_notes']." <b>to</b>  ".htmlspecialchars($obj['j1_int_notes'])."<br/>";
			}

		if($job_ref_details['job_type'] == '2')
			{
				
				$j2_details = $job->getJobDetails($obj['j2_id']);
				$return_message .="<b>Following log generated for Booking ID - ".$j2_details['id']."</b><br/>";
				
				if($j2_details['job_status'] != '80' && $obj['no_show_j2'] =='1' )
				{
					$job->updateJob($j2_details['id'], 'job_status', '80');
					$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Booking status edited', 'No Show', 'Confirm');
					$return_message .="Booking Status Edited from - Confirm to  No Show <br/>";
				}
				if($j2_details['job_status'] == '80' && $obj['no_show_j2'] !='1' )
				{
					$job->updateJob($j2_details['id'], 'job_status', '20');
					$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Booking status edited', 'Confirm', 'No Show');
					$return_message .="Booking Status Edited from - No Show to  Confirm <br/>";
				}
				
				if($j2_details['job_date'] != $obj['j2_date_cap'])
					{
						$job->updateJob($j2_details['id'], 'job_date', $obj['j2_date_cap']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Booking Date Edited', formatDate($obj['j2_date_cap'],1), formatDate($j2_details['job_date'],1));
						$return_message .="Booking Date Edited from - ".formatDate($j2_details['job_date'],1)." to  ".formatDate($obj['j2_date_cap'],1)."<br/>";
					}	
				if($j2_details['job_time'] != ''.$obj['j2_time'].':00')
					{
						$job->updateJob($j2_details['id'], 'job_time', $obj['j2_time']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Booking Time Edited', calculateTimeInAmPmFormatWithoutSeconds($obj['j2_time']), calculateTimeInAmPmFormatWithoutSeconds($j2_details['job_time']));
						$return_message .="Booking Time Edited from - ".calculateTimeInAmPmFormatWithoutSeconds($j2_details['job_time'])." to  ".calculateTimeInAmPmFormatWithoutSeconds($obj['j2_time'])."<br/>";
					}	
					$push_message = 'Job Id : '.$j2_details['id'].' has been edited.';
					$job->commonGooglePushNotification($obj['acc_contact'],$push_message);
				if($obj['j2_driver_status'] !='') // driver status not blank
					{
						$old_driver_status = $job->getDriverStatusDetails($j2_details['driver_status']);
						
						if($obj['j2_driver_status'] =='1') //Allocate
							{
								$driver_details = $user->getUserDetails($obj['j2_driver']); // get details of this driver
								$job->updateJob($j2_details['id'], 'driver_status', '1'); // change the driver status to Allocate
								$job->updateJob($j2_details['id'], 'driver_id', $obj['j2_driver']); // Put this driver in job table
								$job->updateJob($j2_details['id'], 'driver_price', $obj['j2_driver_price']); // Put the driver price in the job table
								$job->addNewDriverToJob($j2_details['id'], $obj['j2_driver'], $obj['j2_driver_price'], '', '', '', '', '', '', '', '', '',''); // Add this to job__driver table
								
								$job->addJobDriverLog($j2_details['id'], $obj['USER_ID'], 'Driver Status Edited', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$obj['j2_driver_price'].'', '');
								$return_message .="Driver Status Edited from - ".$old_driver_status['details']." to  ALLOCATED<br/>";
							}
						if($obj['j2_driver_status'] =='2' && !empty($obj['j2_drivers']) && $obj['j2_driver_price'] !='') //Offered/Sent
							{
								$job->updateJob($j2_details['id'], 'driver_status', '2'); // change the driver status to Sent
								
										$driver_details = $user->getUserDetails($value); // get details of this driver	
										$job->addNewDriverToJob($j2_details['id'], '', '', $obj['j2_driver'], $obj['j2_driver_price'], '', '', '', '', '', '', '',''); // Add this to job__driver table
										$job->addJobDriverLog($j2_details['id'], $obj['USER_ID'], 'Job Offered', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$obj['j2_driver_price'].'', '');
										$mailer->sendJobOfferToDriver($obj['j2_driver'], $j2_details['id'], $obj['j2_driver_price'],htmlspecialchars($obj['j2_driver_notes'])); // Send job offer to the driver
									
									$push_message = 'Job Id : '.$j2_details['id'].' has been offered to you.';
									$job->commonGooglePushNotification($obj['j2_driver'],$push_message);
									$return_message .="Job offer sent to selected drivers<br/>";
							}
						// For Group Drivers offer	
						if($obj['j2_driver_status'] == '2' && !empty($obj['j2_driver_group']) && $obj['j2_driver_group'] !='') //Send
							{
								$job->updateJob($j2_id, 'driver_status', '2'); // change the driver status to Sent
								
								$query = "SELECT * from drivers__group_drivers where group_id='".$obj['j2_driver_group']."'";
								$result = $database->query($query);
								while($row = mysql_fetch_array($result))
									{
										$driver_details = $user->getUserDetails($row['user_id']); // get details of this driver	
										$job->addNewDriverToJob($j2_details['id'], '', '', $row['user_id'], $obj['j2_driver_price'], '', '', '', '', '', '', '',''); // Add this to job__driver table
										$job->addJobDriverLog($j2_details['id'], $obj['USER_ID'], 'Job Offered', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$obj['j2_driver_price'].'', '');
										$mailer->sendJobOfferToDriver($row['user_id'], $j2_details['id'], $obj['j2_driver_price'],htmlspecialchars($obj['j2_driver_notes'])); // Send job offer to the driver
									}
							}
						if($obj['j2_driver_status'] == '3' && !empty($obj['j2_driver']) && $obj['j2_driver_price'] !='') //Accepted
							{
								$job->updateJob($j2_details['id'], 'driver_status', '3'); // change the driver status to Sent
								$job->updateJob($j2_details['id'], 'driver_id', $obj['j2_driver']); // Put this driver in job table
								$job->updateJob($j2_details['id'], 'driver_price', $obj['j2_driver_price']); // Put the driver price in the job table
								$job->addNewDriverToJob($j2_details['id'], '', '', '', '', $obj['j2_driver'], $obj['j2_driver_price'], '', '', '', '', '',''); // Add this to job__driver table
								$driver_details = $user->getUserDetails($obj['j2_driver']); // get details of this driver
								$job->addJobDriverLog($j2_details['id'], $obj['USER_ID'], 'Job Accepted (Internal)', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$obj['j2_driver_price'].'', '');
										
								$mailer->sendJobDetailsToDriver($j2_details['id'], $obj['j2_driver'], $obj['j2_driver_price'],htmlspecialchars($obj['j2_driver_notes']));	
							}
						
						/*if($obj['j2_driver_status'] =='4' || $obj['j2_driver_status'] =='5') //Rogered/Paid //72238
						{
							$job_driver_details = $job->getJobDriverDetails($obj['j2_job_driver_id']);
							
							if($obj['j2_driver_notes'] != $job_driver_details['driver_notes'])
							{ 
								$job->updateJobDriver($obj['j2_job_driver_id'], 'driver_notes', $obj['j2_driver_notes']);
								$job->addJobDriverLog($j2_details['id'], $obj['USER_ID'], 'Driver notes edited from', ''.$job_driver_details['driver_notes'].' ', $obj['j2_driver_notes']);
							}
						}*/
					}
			if($obj['j2_driver_notes'] != $j2_details['driver_notes'] && $obj['ROLE_ID'] == '1' )
			{ 
				$job->updateJob($j2_details['id'], 'driver_notes', htmlspecialchars($obj['j2_driver_notes']));
				$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Driver notes edited from', htmlspecialchars($obj['j2_driver_notes']), $j2_details['driver_notes']);
				$return_message .="Driver notes edited from - ".$j2_details['driver_notes']." to  ".htmlspecialchars($obj['j2_driver_notes'])."<br/>";
			}
				if($j2_details['frm_flight_no'] != $obj['j2_flight_no'])
					{
						$job->updateJob($j2_details['id'], 'frm_flight_no', $obj['j2_flight_no']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'From Flight Number Edited', $obj['j2_flight_no'], $j2_details['frm_flight_no']);
						$return_message .="From Flight Number  Edited from - ".$j2_details['frm_flight_no']." to  ".$obj['j2_flight_no']."<br/>";
					}
				
				if($j2_details['frm_line1'] != $obj['j2_line_1'])
					{
						$job->updateJob($j2_details['id'], 'frm_line1', mysql_real_escape_string($obj['j2_line_1']));
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'From Line 1 Edited', $obj['j2_line_1'], $j2_details['frm_line1']);
						$return_message .="From Line 1 Edited from - ".$j2_details['frm_line1']." to  ".$obj['j2_line_1']."<br/>";
					}
				
				if($j2_details['frm_line2'] != $obj['j2_line_2'])
					{
						$job->updateJob($j2_details['id'], 'frm_line2', mysql_real_escape_string($obj['j2_line_2']));
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'From Line 2 Edited', $obj['j2_line_2'], $j2_details['frm_line2']);
						$return_message .="From Line 2 Edited from - ".$j2_details['frm_line2']." to  ".$obj['j2_line_2']."<br/>";
					}
				
				if($j2_details['frm_sub'] != $obj['j2_sub'])
					{
						$job->updateJob($j2_details['id'], 'frm_sub', mysql_real_escape_string($obj['j2_sub']));
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'From Suburb Edited', $obj['j2_sub'], $j2_details['frm_sub']);
						$return_message .="From Suburb Edited from - ".$j2_details['frm_sub']." to  ".$obj['j2_sub']."<br/>";
					}
				
				if($j2_details['frm_pc'] != $obj['j2_pc'])
					{
						$job->updateJob($j2_details['id'], 'frm_pc', $obj['j2_pc']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'From Postcode Edited', $obj['j2_pc'], $j2_details['frm_pc']);
						$return_message .="From Postcode Edited from - ".$j2_details['frm_pc']." to  ".$obj['j2_pc']."<br/>";
					}
				
				if($j2_details['frm_state'] != $obj['j2_state'])
					{
						$job->updateJob($j2_details['id'], 'frm_state', $obj['j2_state']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'From State Edited', $obj['j2_state'], $j2_details['frm_state']);
						$return_message .="From State Edited to <b>".$obj['j2_state']."</b><br/>";
					}
					
				if($j2_details['to_flight_no'] != $obj['j2_to_flight_no'])
					{
						$job->updateJob($j2_details['id'], 'to_flight_no', $obj['j2_to_flight_no']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'To Flight Number Edited', $obj['j2_to_flight_no'], $j2_details['to_flight_no']);
						$return_message .="to Flight Number  Edited from - ".$j2_details['to_flight_no']." to  ".$obj['j2_to_flight_no']."<br/>";
					}
				
				if($j2_details['to_line1'] != $obj['j2_to_line_1'])
					{
						$job->updateJob($j2_details['id'], 'to_line1', mysql_real_escape_string($obj['j2_to_line_1']));
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'To Line 1 Edited', $obj['j2_to_line_1'], $j2_details['to_line1']);
						$return_message .="To Line 1 Edited from - ".$j2_details['to_line1']." to  ".$obj['j2_to_line_1']."<br/>";
					}
				
				if($j2_details['to_line2'] != $obj['j2_to_line_2'])
					{
						$job->updateJob($j2_details['id'], 'to_line2', mysql_real_escape_string($obj['j2_to_line_2']));
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'To Line 2 Edited', $obj['j2_to_line_2'], $j2_details['to_line2']);
						$return_message .="To Line 2 Edited from - ".$j2_details['to_line2']." to  ".$obj['j2_to_line_2']."<br/>";
					}
				
				if($j2_details['to_sub'] != $obj['j2_to_sub'])
					{
						$job->updateJob($j2_details['id'], 'to_sub', mysql_real_escape_string($obj['j2_to_sub']));
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'To Suburb Edited', $obj['j2_to_sub'], $j2_details['to_sub']);
						$return_message .="To Suburb Edited from - ".$j2_details['to_sub']." to  ".$obj['j2_to_sub']."<br/>";
					}
				
				if($j2_details['to_pc'] != $obj['j2_to_pc'])
					{
						$job->updateJob($j2_details['id'], 'to_pc', $obj['j2_to_pc']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'To Postcode Edited', $obj['j2_to_pc'], $j2_details['to_pc']);
						$return_message .="To Postcode Edited from - ".$j2_details['to_pc']." to  ".$obj['j2_to_pc']."<br/>";
					}
				
				if($j1_details['to_state'] != $obj['j2_state'])
					{
						$job->updateJob($j2_details['id'], 'to_state', $obj['j2_state']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'To State Edited', $obj['j2_state'], $j2_details['to_state']);
						$return_message .="To State Edited to <b>".$obj['j2_state']."</b><br/>";
					}
					
				if($j2_details['frm_via_line1'] != $obj['j2_via_line_1'])
					{
						$job->updateJob($j2_details['id'], 'frm_via_line1', mysql_real_escape_string($obj['j2_via_line_1']));
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'From Via Line 1 Edited', $obj['j2_via_line_1'], $j2_details['frm_via_line1']);
						$return_message .="From Via Line 1 Edited from - ".$j2_details['frm_via_line1']." to  ".$obj['j2_via_line_1']."<br/>";
					}
				
				if($j2_details['frm_via_line2'] != $obj['j2_via_line_2'])
					{
						$job->updateJob($j2_details['id'], 'frm_via_line2', mysql_real_escape_string($obj['j2_via_line_2']));
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'From Via Line 2 Edited', $obj['j2_via_line_2'], $j2_details['frm_via_line2']);
						$return_message .="From Via Line 2 Edited from - ".$j2_details['frm_via_line2']." to  ".$obj['j2_via_line_2']."<br/>";
					}
				
				if($j2_details['frm_via_sub'] != $obj['j2_via_sub'])
					{
						$job->updateJob($j2_details['id'], 'frm_via_sub', mysql_real_escape_string($obj['j2_via_sub']));
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'From Via Suburb Edited', $obj['j2_via_sub'], $j2_details['frm_via_sub']);
						$return_message .="From Via Suburb Edited from - ".$j2_details['frm_via_sub']." to  ".$obj['j2_via_sub']."<br/>";
					}
				
				if($j2_details['frm_via_pc'] != $obj['j2_via_pc'])
					{
						$job->updateJob($j2_details['id'], 'frm_via_pc', $obj['j2_via_pc']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'From Via Postcode Edited', $obj['j2_via_pc'], $j2_details['frm_via_pc']);
						$return_message .="From Via Postcode Edited from - ".$j2_details['frm_via_pc']." to  ".$obj['j2_via_pc']."<br/>";
					}
					
				if($j1_details['frm_via_state'] != $obj['j2_state'] && $obj['j2_via_sub'] != '')
					{
						$job->updateJob($j2_details['id'], 'frm_via_state', $obj['j2_state']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'From Via State Edited', $obj['j2_state'], $j2_details['to_via_state']);
						$return_message .="From Via State Edited to ".$obj['j2_state']."<br/>";
					}
					
				if($j2_details['to_via_line1'] != $obj['j2_to_via_line_1'])
					{
						$job->updateJob($j2_details['id'], 'to_via_line1', mysql_real_escape_string($obj['j2_to_via_line_1']));
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'To Via Line 1 Edited', $obj['j2_to_via_line_1'], $j2_details['to_via_line1']);
						$return_message .="To Via Line 1 Edited from - ".$j2_details['to_via_line1']." to  ".$obj['j2_to_via_line_1']."<br/>";
					}
				
				if($j2_details['to_via_line2'] != $obj['j2_to_via_line_2'])
					{
						$job->updateJob($j2_details['id'], 'to_via_line2', mysql_real_escape_string($obj['j2_to_line_2']));
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'To Via Line 2 Edited', $obj['j2_to_via_line_2'], $j2_details['to_via_line2']);
						$return_message .="To Via Line 2 Edited from - ".$j2_details['to_via_line2']." to  ".$obj['j2_to_via_line_2']."<br/>";
					}
				
				if($j2_details['to_via_sub'] != $obj['j2_to_via_sub'])
					{
						$job->updateJob($j2_details['id'], 'to_via_sub', mysql_real_escape_string($obj['j2_to_via_sub']));
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'To Via Suburb Edited', $obj['j2_to_via_sub'], $j2_details['to_via_sub']);
						$return_message .="To Via Suburb Edited from - ".$j2_details['to_via_sub']." to  ".$obj['j2_to_via_sub']."<br/>";
					}
				
				if($j2_details['to_via_pc'] != $obj['j2_to_via_pc'])
					{
						$job->updateJob($j2_details['id'], 'to_via_pc', $obj['j2_to_via_pc']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'To Via Postcode Edited', $obj['j2_to_via_pc'], $j2_details['to_via_pc']);
						$return_message .="To Via Postcode Edited from - ".$j2_details['to_via_pc']." to  ".$obj['j2_to_via_pc']."<br/>";
					}
					
				if($j1_details['to_via_state'] != $obj['j2_state'] && $obj['j2_to_via_sub'] != '')
					{
						$job->updateJob($j2_details['id'], 'to_via_state', $obj['j2_state']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'To Via State Edited', $obj['j2_state'], $j2_details['to_via_state']);
						$return_message .="To Via State Edited to ".$obj['j2_state']."<br/>";
					}
					
				if($j2_details['car_id'] != $obj['j2_car_id'])
					{
						$query1 = "SELECT * from variable__car_type where id = ".$j2_details['car_id']."";
						$result1 = $database->query($query1);
						$row1 = mysql_fetch_array($result1);
						$old_details = $row1['details'];
						
						$query2 = "SELECT * from variable__car_type where id = ".$obj['j2_car_id']."";
						$result2 = $database->query($query2);
						$row2 = mysql_fetch_array($result2);
						$new_details = $row2['details'];
						
						$job->updateJob($j2_details['id'], 'car_id', $obj['j2_car_id']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Car Type Edited', $new_details, $old_details);
						$return_message .="Car Type Edited from - ".$old_details." to  ".$new_details."<br/>";
					}
				
				if($j2_details['pax_nos'] != $obj['j2_pax_nos'])
					{
						$job->updateJob($j2_details['id'], 'pax_nos', $obj['j2_pax_nos']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Pax Nos. Edited', $obj['j2_pax_nos'], $j2_details['pax_nos']);
						$return_message .="Pax Nos. Edited from - ".$j2_details['pax_nos']." to  ".$obj['j2_pax_nos']."<br/>";
					}
				if($j2_details['luggage'] != $obj['j2_luggage'])
					{
						$job->updateJob($j2_details['id'], 'luggage', $obj['j2_luggage']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Luggage Nos. Edited', $obj['j2_luggage'], $j2_details['luggage']);
						$return_message .="Luggage Nos. Edited from - ".$j2_details['luggage']." to  ".$obj['j2_luggage']."<br/>";
					}
				if($j2_details['baby_seat'] != $obj['j2_baby_seats'])
					{
						$job->updateJob($j2_details['id'], 'baby_seat', $obj['j2_baby_seats']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Baby Seat Nos. Edited', $obj['j2_baby_seats'], $j2_details['baby_seat']);
						$return_message .="Baby Seat Nos. Edited from - ".$j2_details['baby_seat']." to  ".$obj['j2_baby_seats']."<br/>";
					}
				if($j2_details['booster_seat'] != $obj['j2_booster_seats'])
					{
						$job->updateJob($j2_details['id'], 'booster_seat', $obj['j2_booster_seats']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Booster Seat Nos. Edited', $obj['j2_booster_seats'], $j2_details['booster_seat']);
						$return_message .="Booster Seat Nos. Edited from - ".$j2_details['booster_seat']." to  ".$obj['j2_booster_seats']."<br/>";
					}
				if($j2_details['baby_capsule'] != $obj['j2_baby_capsules'])
					{
						$job->updateJob($j2_details['id'], 'baby_capsule', $obj['j2_baby_capsules']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Baby Capsule Nos. Edited', $obj['j2_baby_capsules'], $j2_details['baby_capsule']);
						$return_message .="Baby Capsule Nos. Edited from - ".$j2_details['baby_capsule']." to  ".$obj['j2_baby_capsules']."<br/>";
					}
				if($j2_details['baby_capsule'] != $obj['j2_baby_capsules'])
					{
						$job->updateJob($j2_details['id'], 'baby_capsule', $obj['j2_baby_capsules']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Baby Capsule Nos. Edited', $obj['j2_baby_capsules'], $j2_details['baby_capsule']);
						$return_message .="Baby Capsule Nos. Edited from - ".$j2_details['baby_capsule']." to  ".$obj['j2_baby_capsules']."<br/>";
					}
				
				if($j2_details['kms'] != $obj['j2_kms'] && $obj['j2_kms'] != '')
					{
						$job->updateJob($j2_details['id'], 'kms', $obj['j2_kms']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Kilometers Edited', $obj['j2_kms'], $j2_details['kms']);
						$return_message .="Kilometers Edited from - ".$j2_details['kms']." to  ".$obj['j2_kms']."<br/>";
					}
				
				if($j2_details['fare'] != $obj['j2_fare'] && $obj['j2_fare'] != '')
					{
						$job->updateJob($j2_details['id'], 'fare', $obj['j2_fare']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Fare Edited', $obj['j2_fare'], $j2_details['fare']);
						$return_message .="Fare Edited from - ".$j2_details['fare']." to  ".$obj['j2_fare']."<br/>";
					}
				if($j2_details['inter'] != $obj['j2_inter'] && $obj['j2_inter'] != '')
					{
						$job->updateJob($j2_details['id'], 'inter', $obj['j2_inter']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'International Fare Edited', $obj['j2_inter'], $j2_details['inter']);
						$return_message .="International Fare Edited from - ".$j2_details['inter']." to  ".$obj['j2_inter']."<br/>";
					}
				if($j2_details['ed'] != $obj['j2_ed'] && $obj['j2_ed'] != '')
					{
						$job->updateJob($j2_details['id'], 'ed', $obj['j2_ed']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Extra Drop/ Pickup Fare Edited', $obj['j2_ed'], $j2_details['ed']);
						$return_message .="Extra Drop/ Pickup Fare Edited from - ".$j2_details['ed']." to  ".$obj['j2_ed']."<br/>";
					}
				if($j2_details['wait'] != $obj['j2_wait'] && $obj['j2_wait'] != '')
					{
						$job->updateJob($j2_details['id'], 'wait', $obj['j2_wait']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Waiting Charges Edited', $obj['j2_wait'], $j2_details['wait']);
						$return_message .="Waiting Charges Edited from - ".$j2_details['wait']." to  ".$obj['j2_wait']."<br/>";
					}
				if($j2_details['tolls'] != $obj['j2_tolls'] && $obj['j2_tolls'] != '')
					{
						$job->updateJob($j2_details['id'], 'tolls', $obj['j2_tolls']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Toll Charges Edited', $obj['j2_tolls'], $j2_details['tolls']);
						$return_message .="Toll Charges Edited from - ".$j2_details['tolls']." to  ".$obj['j2_tolls']."<br/>";
					}
				if($j2_details['bs'] != $obj['j2_bs'] && $obj['j2_bs'] != '')
					{
						$job->updateJob($j2_details['id'], 'bs', $obj['j2_bs']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Baby Seat Charges Edited', $obj['j2_bs'], $j2_details['bs']);
						$return_message .="Baby Seat Charges Edited from - ".$j2_details['bs']." to  ".$obj['j2_bs']."<br/>";
					}
				if($j2_details['park'] != $obj['j2_park'] && $obj['j2_ed'] != '')
					{
						$job->updateJob($j2_details['id'], 'park', $obj['j2_park']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Parking Charges Edited', $obj['j2_park'], $j2_details['park']);
						$return_message .="Parking Charges Edited from - ".$j2_details['park']." to  ".$obj['j2_park']."<br/>";
					}
				if($j2_details['ah'] != $obj['j2_ah'] && $obj['j2_ah'] != '')
					{
						$job->updateJob($j2_details['id'], 'ah', $obj['j2_ah']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'After Hour Charges Edited', $obj['j2_ah'], $j2_details['ah']);
						$return_message .="After Hour Charges Edited from - ".$j2_details['ah']." to  ".$obj['j2_ah']."<br/>";
					}
				if($j2_details['me'] != $obj['j2_me'] && $obj['j2_me'] != '')
					{
						$job->updateJob($j2_details['id'], 'me', $obj['j2_me']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Major Event Charges Edited', $obj['j2_me'], $j2_details['me']);
						$return_message .="Major Event Charges Edited from - ".$j2_details['me']." to  ".$obj['j2_me']."<br/>";
					}
				if($j2_details['alc'] != $obj['j2_alc'] && $obj['j2_alc'] != '')
					{
						$job->updateJob($j2_details['id'], 'alc', $obj['j2_alc']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Alcohol Charges Edited', $obj['j2_alc'], $j2_details['alc']);
						$return_message .="Alcohol Charges Edited from - ".$j2_details['alc']." to  ".$obj['j2_alc']."<br/>";
					}
				if($j2_details['fc'] != $obj['j2_fc'] && $obj['j2_fc'] != '')
					{
						$job->updateJob($j2_details['id'], 'fc', $obj['j2_fc']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Food/Coffee Charges Edited', $obj['j2_fc'], $j2_details['fc']);
						$return_message .="Food/Coffee Charges Edited from - ".$j2_details['fc']." to  ".$obj['j2_fc']."<br/>";
					}
				if($j2_details['oth'] != $obj['j2_oth'] && $obj['j2_oth'] != '')
					{
						$job->updateJob($j2_details['id'], 'oth', $obj['j2_oth']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Other Charges Edited', $obj['j2_oth'], $j2_details['oth']);
						$return_message .="Other Charges Edited from - ".$j2_details['oth']." to  ".$obj['j2_oth']."<br/>";
					}
				if($j2_details['tot_fare'] != $obj['j2_tot_fare'] && $obj['j2_tot_fare'] != '')
					{
						$job->updateJob($j2_details['id'], 'tot_fare', $obj['j2_tot_fare']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Total Fare Edited', $obj['j2_tot_fare'], $j2_details['tot_fare']);
						$return_message .="Total Fare Edited from - ".$j2_details['tot_fare']." to  ".$obj['j2_tot_fare']."<br/>";
					}
				if($j2_details['drv_fee'] != $obj['j2_drv_fee'] && $obj['j2_drv_fee'] != '')
					{
						$job->updateJob($j2_details['id'], 'drv_fee', $obj['j2_drv_fee']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Total Driver Fee Edited', $obj['j2_drv_fee'], $j2_details['drv_fee']);
						$return_message .="Total Driver Fee Edited from - ".$j2_details['drv_fee']." to  ".$obj['j2_drv_fee']."<br/>";
					}
				if($j2_details['oth_exp'] != $obj['j2_oth_exp'] && $obj['j2_oth_exp'] != '')
					{
						$job->updateJob($j2_details['id'], 'oth_exp', $obj['j2_oth_exp']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Total Other Expenses Edited', $obj['j2_oth_exp'], $j2_details['oth_exp']);
						$return_message .="Total Other Expenses Edited from - ".$j2_details['oth_exp']." to  ".$obj['j2_oth_exp']."<br/>";
					}
				if($j2_details['profit'] != $obj['j2_profit'] && $obj['j2_profit'] != '')
					{
						$job->updateJob($j2_details['id'], 'profit', $obj['j2_profit']);
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Total Profit Edited', $obj['j2_profit'], $j2_details['profit']);
						$return_message .="Total Profit Edited from - ".$j2_details['profit']." to  ".$obj['j2_profit']."<br/>";
					}
				if($j2_details['ext_notes'] != $obj['j2_ext_notes'])
					{
						$job->updateJob($j2_details['id'], 'ext_notes', htmlspecialchars($obj['j2_ext_notes']));
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'External Notes Edited', htmlspecialchars($obj['j2_ext_notes']), $j2_details['ext_notes']);
						$return_message .="External Notes Edited from - ".$j2_details['ext_notes']." to  ".htmlspecialchars($obj['j2_ext_notes'])."<br/>";
					}
				if($j2_details['int_notes'] != $obj['j2_int_notes'] && $obj['ROLE_ID'] == '1' )
					{
						$job->updateJob($j2_details['id'], 'int_notes', htmlspecialchars($obj['j2_int_notes']));
						$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Internal Notes Edited', htmlspecialchars($obj['j2_int_notes']), $j2_details['int_notes']);
						$return_message .="Internal Notes Edited from - ".$j2_details['int_notes']." to  ".htmlspecialchars($obj['j2_int_notes'])."<br/>";
					}
			}
			else if($job_ref_details['job_type'] == '1' && $obj['job_type'] == '2')//Section added on 12.08.2014
			{
				$from_state = $address->getStateName($obj['j2_state']);
				$to_state = $from_state;
				if($obj['j1_via_sub'] != '') // if suburb not blank
					{
						$from_via_state = $from_state;
					}
				if($obj['j1_to_via_sub'] != '') // if suburb not blank
					{
						$to_via_state = $from_state;
					}
				if($obj['ROLE_ID'] == '1')	 	//admin
				{
					$job_status_id = '20'; 		//confirmed
				}
			if($obj['ROLE_ID'] == '3')		//client or pax
				{
					$job_status_id = '10'; 		//confirmed
				}
			$job_reference_id = $job_ref_details['id'];
				
				$j2_id = $job->addNewJob
					(
						$job_reference_id,
						$job_status_id,
						$obj['j2_date_cap'],
						$obj['j2_time'],
						mysql_real_escape_string($obj['j2_flight_no']),
						mysql_real_escape_string($obj['j2_line_1']),
						mysql_real_escape_string($obj['j2_line_2']),
						mysql_real_escape_string($obj['j2_sub']),
						mysql_real_escape_string($obj['j2_pc']),
						$from_state,
						mysql_real_escape_string($obj['j2_to_flight_no']),
						mysql_real_escape_string($obj['j2_to_line_1']),
						mysql_real_escape_string($obj['j2_to_line_2']),
						mysql_real_escape_string($obj['j2_to_sub']),
						mysql_real_escape_string($obj['j2_to_pc']),
						$to_state,
						mysql_real_escape_string($obj['j2_via_line_1']),
						mysql_real_escape_string($obj['j2_via_line_2']),
						mysql_real_escape_string($obj['j2_via_sub']),
						mysql_real_escape_string($obj['j2_via_pc']),
						$from_via_state,
						mysql_real_escape_string($obj['j2_to_via_line_1']),
						mysql_real_escape_string($obj['j2_to_via_line_2']),
						mysql_real_escape_string($obj['j2_to_via_sub']),
						mysql_real_escape_string($obj['j2_to_via_pc']),
						$to_via_state,
						$obj['j2_car_id'],
						$obj['j2_pax_nos'],
						$obj['j2_luggage'],
						$obj['j2_baby_seats'],
						$obj['j2_booster_seats'],
						$obj['j2_baby_capsules'],
						'',											//this is driver_status_field (leave blank)
						$obj['j2_kms'],
						$obj['j2_fare'],
						$obj['j2_inter'],
						$obj['j2_ed'],
						$obj['j2_wait'],
						$obj['j2_tolls'],
						$obj['j2_bs'],
						$obj['j2_park'],
						$obj['j2_ah'],
						$obj['j2_me'],
						$obj['j2_alc'],
						$obj['j2_fc'],
						$obj['j2_oth'],
						$obj['j2_tot_fare'],
						$obj['j2_drv_fee'],
						$obj['j2_oth_exp'],
						$obj['j2_profit'],
						htmlspecialchars($obj['j2_ext_notes']),
						htmlspecialchars($obj['j2_int_notes'])
					);
				if($j2_id != '')
					{
						$return_message .= "<b>BOOKING ID -  ".$j2_id."</b><br/>";
					}
				//Add in Job Log Table
				$job->addJobLog($j2_id, $obj['USER_ID'], 'New job created', $j2_id, '');
				$job_reference->updateJobReferenceTable($job_reference_id, 'j2_id', $j2_id);
				//Add in Job Reference Log Table
				$job_reference->addJobReferenceLog($job_reference_id, $obj['USER_ID'], 'Job 2 Added', $j2_id, 'NIL');
				
				//Allocate, Send of Accept Driver
				if($obj['j2_driver_status'] == '1' && $obj['j2_driver'] !='') //Allocate
					{
						$job->updateJob($j2_id, 'driver_status', '1'); // change the driver status to Allocate
						$job->updateJob($j2_id, 'driver_id', $obj['j2_driver']); // Put this driver in job table
						$job->updateJob($j2_id, 'driver_price', $obj['j2_driver_price']); // Put the driver price in the job table
						$job->addNewDriverToJob($j2_id, $obj['j2_driver'], $obj['j2_driver_price'], '', '', '', '', '', '', '', '', '',htmlspecialchars($obj['j2_driver_notes'])); // Add this to job__driver table
						$driver_details = $user->getUserDetails($obj['j2_driver']); // get details of this driver
						$job->addJobDriverLog($j2_id, $obj['USER_ID'], 'Job Allocated', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$obj['j2_driver_price'].'', '');
						$additional_message = "Booking ID -  ".$j2_id ." allocated to - <b>".$driver_details['fname']." ".$driver_details['lname']." for $ ".$obj['j2_driver_price']."</b><br/>";
					}
				if($obj['j2_driver_status'] == '2' && !empty($obj['j2_drivers']) && $obj['j2_driver_price'] !='') //Send/Offer
					{
						$mailer = new mailer();
						$job->updateJob($j2_id, 'driver_status', '2'); // change the driver status to Sent
						foreach($obj['j2_drivers'] as $key => $value)	
							{
								$user->getUserDetails($value); // get details of this driver
								$job->addNewDriverToJob($j2_id, '', '', $value, $obj['j2_driver_price'], '', '', '', '', '', '', '',htmlspecialchars($obj['j2_driver_notes'])); // Add this to job__driver table
								$driver_details = $user->getUserDetails($value); // get details of this driver
								$job->addJobDriverLog($j2_id, $obj['USER_ID'], 'Job Offered', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$obj['j2_driver_price'].'', '');
								$additional_message .= "Booking ID -  ".$j2_id ." offered to - <b>".$driver_details['fname']." ".$driver_details['lname']." for $ ".$obj['j2_driver_price']."</b><br/>";
								$mailer->sendJobOfferToDriver($value, $j2_id, $obj['j2_driver_price'],htmlspecialchars($obj['j2_driver_notes'])); // Send job offer to the driver
							}
					}
				if($obj['j2_driver_status'] == '3' && !empty($obj['j2_driver']) && $obj['j2_driver_price'] !='') //Accepted
					{
						$mailer = new mailer();
						$job->updateJob($j2_id, 'driver_status', '3'); // change the driver status to Sent/Accepted
						$job->updateJob($j2_id, 'driver_id', $obj['j2_driver']); // Put this driver in job table
						$job->updateJob($j2_id, 'driver_price', $obj['j2_driver_price']); // Put the driver price in the job table
						$job->addNewDriverToJob($j2_id, '', '', '', '', $obj['j2_driver'], $obj['j2_driver_price'], '', '', '', '', '',htmlspecialchars($obj['j2_driver_notes'])); // Add this to job__driver table
						$driver_details = $user->getUserDetails($obj['j2_driver']); // get details of this driver
						$job->addJobDriverLog($j2_id, $obj['USER_ID'], 'Job Accepted', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$obj['j2_driver_price'].'', '');
						$additional_message .= "Booking ID -  ".$j2_id ." Accepted by - <b>".$driver_details['fname']." ".$driver_details['lname']." for $ ".$obj['j2_driver_price']."</b><br/>";				
						$mailer->sendJobDetailsToDriver($j2_id, $obj['j2_driver'], $obj['j2_driver_price'],htmlspecialchars($obj['j2_driver_notes'])); // Send job acceptance email with job details to the driver
					}
					
				//Upadate Addresses
				$add_id_5 = $address->checkDuplicateOrEnter($pax_id, $obj['j2_line_1'], $obj['j2_line_2'], $obj['j2_sub'], $obj['j2_pc'], $obj['j2_state']);
				if($add_id_5 != '')
					{
						$additional_message .= 'New Address Added - <b>'.$obj['j2_line_1'].', '.$obj['j2_line_2'].', '.$obj['j2_sub'].', '.$obj['j2_pc'].', '.$obj['j2_state'].'</b><br/>';
					}
				$add_id_6 = $address->checkDuplicateOrEnter($pax_id, $obj['j2_to_line_1'], $obj['j2_to_line_2'], $obj['j2_to_sub'], $obj['j2_to_pc'], $obj['j2_state']);
				if($add_id_6 != '')
					{
						$additional_message .= 'New Address Added - <b>'.$obj['j2_to_line_1'].', '.$obj['j2_to_line_2'].', '.$obj['j2_to_sub'].', '.$obj['j2_to_pc'].', '.$obj['j2_state'].'</b><br/>';
					}
				$add_id_7 = $address->checkDuplicateOrEnter($pax_id, $obj['j2_via_line_1'], $obj['j2_via_line_2'], $obj['j2_via_sub'], $obj['j2_via_pc'], $obj['j2_state']);
				if($add_id_7 != '')
					{
						$additional_message .= 'New Address Added - <b>'.$obj['j2_via_line_1'].', '.$obj['j2_via_line_2'].', '.$obj['j2_via_sub'].', '.$obj['j2_via_pc'].', '.$obj['j2_state'].'</b><br/>';
					}
				$add_id_8 = $address->checkDuplicateOrEnter($pax_id, $obj['j2_to_via_line_1'], $obj['j2_to_via_line_2'], $obj['j2_to_via_sub'], $obj['j2_to_via_pc'], $obj['j2_state']);
				if($add_id_8 != '')
					{
						$additional_message .= 'New Address Added - <b>'.$obj['j2_to_via_line_1'].', '.$obj['j2_to_via_line_2'].', '.$obj['j2_to_via_sub'].', '.$obj['j2_to_via_pc'].', '.$obj['j2_state'].'</b><br/>';
					}
				
				// if booking was made by admin staff, send emails to clients as BOOKING CONFIRMED
				if($obj['ROLE_ID'] == '1')
					{
						$mailer = new Mailer();
						if($job_ref_details['conf_to_client'] == '1') // send this booking to booking made by
							{
								$user = new User();
								$user_details = $user->getUserDetails($contact_id);
								if($user_details['email'] != '')
									{
										$mailer->sendMailToClientOnBooking($job_reference_id, '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
										//$mailer->sendMailToClientOnBooking($job_reference_id, '1', 'sucheta_sw@yahoo.co.in', $user_details['title'], $user_details['fname'], $user_details['lname']);
										$additional_message .= 'Booking confirmation sent to client.<br/>';
									}
								else
									{
										$additional_message .= 'Booking by Email - Not Found. Email not sent to booking made by.<br/>';
									}
							}
						if($job_ref_details['conf_to_pax'] == '1') // send this booking pax
							{
								$user = new User();
								$user_details = $user->getUserDetails($pax_id);
								if($user_details['email'] != '')
									{
										$mailer->sendMailToClientOnBooking($job_reference_id, '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
										//$mailer->sendMailToClientOnBooking($job_reference_id, '1', 'sucheta.testmail@gmail.com', $user_details['title'], $user_details['fname'], $user_details['lname']);
										$additional_message .= 'Booking confirmation sent to passenger.<br/>';
									}
								else
									{
										$additional_message .= 'Passenger Email - Not Found. Email not sent to passenger.<br/>';
									}
							}
						//$mailer->sendMailToAdminOnBooking($job_reference_id);
					}
				// if booking was made by customer send email as received and waiting for confirmation
				if($obj['ROLE_ID'] == '2')
					{
						$mailer = new Mailer();
						if($obj['conf_to_bkg_made_by'] == '1') // send this booking to booking made by
							{
								$user = new User();
								$user_details = $user->getUserDetails($contact_id);
								if($user_details['email'] != '')
									{
										$mailer->sendMailToClientOnBooking($job_reference_id, '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
										$additional_message .= 'Booking confirmation sent to client.<br/>';
									}
								else
									{
										$additional_message .= 'Booking by Email - Not Found. Email not sent to booking made by.<br/>';
									}
							}
						if($obj['conf_to_pax'] == '1') // send this booking pax
							{
								$user = new User();
								$user_details = $user->getUserDetails($pax_id);
								if($user_details['email'] != '')
									{
										$mailer->sendMailToClientOnBooking($job_reference_id, '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
										$additional_message .= 'Booking confirmation sent to passenger.<br/>';
									}
								else
									{
										$additional_message .= 'Passenger Email - Not Found. Email not sent to passenger.<br/>';
									}
							}
						$mailer->sendMailToAdminOnBooking($job_reference_id);
					}
				if($obj['ROLE_ID'] == '3') 
					{
						$mailer = new Mailer();
						$user = new User();
						$user_details = $user->getUserDetails($contact_id);
						if($user_details['email'] != '')
							{
								$mailer->sendMailToClientOnBooking($job_reference_id, '2', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
								$additional_message .= 'Booking waiting to be confirmed by Allied.<br/>';
							}
						else
							{
								$additional_message .= 'We could not sent an email to booked by as no email address was found.<br/>However Allied has received your booking and will confirm as soon as possible.<br/>';
							}
						
						$user_details = $user->getUserDetails($pax_id);
						if($user_details['email'] != '')
							{
								$mailer->sendMailToClientOnBooking($job_reference_id, '2', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
								$additional_message .= 'Booking waiting to be confirmed by Allied.<br/>';
							}
						else
							{
								$additional_message .= 'We could not send an email to the passenger as no email address was found.<br/>However Allied has received your booking and will confirm as soon as possible.<br/>';
							}
						$mailer->sendMailToAdminOnBooking($job_reference_id);
						
					}
				
			}
			//echo $contact_id .'con'.$pax_id.'pax';
			
	} 

//----------------------------------------------------------------------------------------------------------------------------------------

	$address->checkDuplicateOrEnter($pax_id, $obj['j1_line_1'], $obj['j1_line_2'], $obj['j1_sub'], $obj['j1_pc'], $obj['pk_state']);
	$address->checkDuplicateOrEnter($pax_id, $obj['j1_to_line_1'], $obj['j1_to_line_2'], $obj['j1_to_sub'], $obj['j1_to_pc'], $obj['pk_state']);
	$address->checkDuplicateOrEnter($pax_id, $obj['j1_via_line_1'], $obj['j1_via_line_2'], $obj['j1_via_sub'], $obj['j1_via_pc'], $obj['pk_state']);
	$address->checkDuplicateOrEnter($pax_id, $obj['j1_to_via_line_1'], $obj['j1_to_via_line_2'], $obj['j1_to_via_sub'], $obj['j1_to_via_pc'], $obj['pk_state']);	
	$address->checkDuplicateOrEnter($pax_id, $obj['j2_line_1'], $obj['j2_line_2'], $obj['j2_sub'], $obj['j2_pc'], $obj['ret_state']);
	$address->checkDuplicateOrEnter($pax_id, $obj['j2_to_line_1'], $obj['j2_to_line_2'], $obj['j2_to_sub'], $obj['j2_to_pc'], $obj['ret_state']);
	$address->checkDuplicateOrEnter($pax_id, $obj['j2_via_line_1'], $obj['j2_via_line_2'], $obj['j2_via_sub'], $obj['j2_via_pc'], $obj['ret_state']);
	$address->checkDuplicateOrEnter($pax_id, $obj['j2_to_via_line_1'], $obj['j2_to_via_line_2'], $obj['j2_to_via_sub'], $obj['j2_to_via_pc'], $obj['ret_state']);	


if(isset($obj['job_decision']))
	{	
		if($obj['job_decision']=='confirm_jobs') // confirm jobs
			{
				
				$job 			= new job();
				$job_reference 	= new jobReference();
				$mailer 		= new mailer();
				$user			= new user();
				
				
				$j1_details 	= $job->getJobDetails($obj['j1_id']);
				$j2_details 	= $job->getJobDetails($obj['j2_id']);
				
				$old_j1_status 	= $job->getJobStatusDetails($j1_details['job_status']);
				$old_j2_status 	= $job->getJobStatusDetails($j2_details['job_status']);
				
				$job->updateJob($j1_details['id'], 'job_status', '20');
				$job->updateJob($j2_details['id'], 'job_status', '20');
				
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Bkg ID '.$j1_details['id'].' - Confirmed', 'Confirmed', $old_j1_status['details']);
				$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Bkg ID '.$j2_details['id'].' - Confirmed', 'Confirmed', $old_j2_status['details']);
				
				$job_ref_details = $job_reference->getJobReferenceDetails($j1_details['job_reference_id']);
				
				if($job_ref_details['acc_type'] == '3') //if this is booked by university or student
				{
					$mailer->sendMailToClientOnBooking($j1_details['job_reference_id'], '1', $job_ref_details['std_email'], $job_ref_details['std_title'], $job_ref_details['std_fname'], $job_ref_details['std_lname']);
				}
				if($job_ref_details['charge_acc_id'] == '19' || $job_ref_details['charge_acc_id'] == '15')
				{
				}
				else
				{
					if($job_ref_details['conf_to_client'] == '1') // send this booking to booking made by
					{
						$user = new User();
						$user_details = $user->getUserDetails($contact_id);
						if($user_details['email'] != '')
							{
								$mailer->sendMailToClientOnBooking($job_ref_details['id'], '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
								//$mailer->sendMailToClientOnBooking($job_reference_id, '1', 'sucheta_sw@yahoo.co.in', $user_details['title'], $user_details['fname'], $user_details['lname']);
								$additional_message .= 'Booking confirmation sent to client.<br/>';
							}
						else
							{
								$additional_message .= 'Booking by Email - Not Found. Email not sent to booking made by.<br/>';
							}
					}
					
					if($job_ref_details['conf_to_pax'] == '1') // send this booking pax
					{
						$user = new User();
						$user_details = $user->getUserDetails($pax_id);
						if($user_details['email'] != '')
							{
								$mailer->sendMailToClientOnBooking($job_ref_details['id'], '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
								//$mailer->sendMailToClientOnBooking($job_reference_id, '1', 'sucheta.testmail@gmail.com', $user_details['title'], $user_details['fname'], $user_details['lname']);
								$additional_message .= 'Booking confirmation sent to passenger.<br/>';
							}
						else
							{
								$additional_message .= 'Passenger Email - Not Found. Email not sent to passenger.<br/>';
							}
					}
				}	
				
				$job_ref_details2 = $job_reference->getJobReferenceDetails($j2_details['job_reference_id']);
				
				if($job_ref_details2['acc_type'] == '3') //if this is booked by university or student
				{
					$mailer->sendMailToClientOnBooking($j2_details['job_reference_id'], '1', $job_ref_details2['std_email'], $job_ref_details2['std_title'], $job_ref_details2['std_fname'], $job_ref_details2['std_lname']);
				}
				
				if($job_ref_details['charge_acc_id'] == '19' || $job_ref_details['charge_acc_id'] == '15')
				{
				}
				else
				{
					if($job_ref_details2['conf_to_client'] == '1') // send this booking to booking made by
					{
						$user = new User();
						$user_details = $user->getUserDetails($contact_id);
						if($user_details['email'] != '')
							{
								$mailer->sendMailToClientOnBooking($job_ref_details2['id'], '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
								//$mailer->sendMailToClientOnBooking($job_reference_id, '1', 'sucheta_sw@yahoo.co.in', $user_details['title'], $user_details['fname'], $user_details['lname']);
								$additional_message .= 'Booking confirmation sent to client.<br/>';
							}
						else
							{
								$additional_message .= 'Booking by Email - Not Found. Email not sent to booking made by.<br/>';
							}
					}
					
					if($job_ref_details2['conf_to_pax'] == '1') // send this booking pax
					{
						$user = new User();
						$user_details = $user->getUserDetails($pax_id);
						if($user_details['email'] != '')
							{
								$mailer->sendMailToClientOnBooking($job_ref_details2['id'], '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
								//$mailer->sendMailToClientOnBooking($job_reference_id, '1', 'sucheta.testmail@gmail.com', $user_details['title'], $user_details['fname'], $user_details['lname']);
								$additional_message .= 'Booking confirmation sent to passenger.<br/>';
							}
						else
							{
								$additional_message .= 'Passenger Email - Not Found. Email not sent to passenger.<br/>';
							}
					}
				}
				$return_message ="Booking ID ".$obj['j1_id']." & ".$obj['j2_id']."- Confirmed.<br/>Confirmation emails have been sent to both booking made by and pax.";
			}

		elseif($obj['job_decision'] == 'decline_jobs') // decline jobs
			{
				$job 			= new job();
				$job_reference 	= new jobReference();
				$mailer 		= new mailer();
				$user			= new user();
				
				
				$j1_details 	= $job->getJobDetails($obj['j1_id']);
				$j2_details 	= $job->getJobDetails($obj['j2_id']);
				
				$old_j1_status 	= $job->getJobStatusDetails($j1_details['job_status']);
				$old_j2_status 	= $job->getJobStatusDetails($j2_details['job_status']);
				
				$job->updateJob($j1_details['id'], 'job_status', '90');
				$job->updateJob($j2_details['id'], 'job_status', '90');
				
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Bkg ID '.$j1_details['id'].' - Declined', 'Declined', $old_j1_status['details']);
				$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Bkg ID '.$j2_details['id'].' - Declined', 'Declined', $old_j2_status['details']);
				
				$job_ref_details = $job_reference->getJobReferenceDetails($j1_details['job_reference_id']);

				$user_details = $user->getUserDetails($job_ref_details['bkg_by_id']);
				$mailer->sendMailToClientOnBooking($job_ref_details['id'], '2', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
				
				$user_details = $user->getUserDetails($job_ref_details['pax_id']);
				$mailer->sendMailToClientOnBooking($job_ref_details['id'], '2', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
				
				if($job_ref_details['acc_type'] == '3') //if this is booked be university or student
				{
					$mailer->sendMailToClientOnBooking($job_ref_details['id'], '2', $job_ref_details['std_email'], $job_ref_details['std_title'], $job_ref_details['std_fname'], $job_reference_details['std_lname']);
				}
					
				$return_message ="Booking ID ".$obj['j1_id']." - Declined<br/>Booking rejected notification emails have been sent to both booking made by and pax.";
			}
		elseif($obj['job_decision'] == 'cancel_jobs') // cancel jobs
			{
				$job = new job();
				
				$j1_details 	= $job->getJobDetails($obj['j1_id']);
				$j2_details 	= $job->getJobDetails($obj['j2_id']);
				
				$old_j1_status 	= $job->getJobStatusDetails($j1_details['job_status']);
				$old_j2_status 	= $job->getJobStatusDetails($j2_details['job_status']);
				
				$job->updateJob($j1_details['id'], 'job_status', '100');
				$job->updateJob($j2_details['id'], 'job_status', '100');
				
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Bkg ID '.$j1_details['id'].' - Cancelled', 'Cancelled', $old_j1_status['details']);
				$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Bkg ID '.$j2_details['id'].' - Cancelled', 'Cancelled', $old_j2_status['details']);
				
				$return_message ="Booking ID ".$obj['j1_id']." & ".$obj['j1_id']." - Cancelled<br/>Please note, on cancellation, email notification is not sent to anyone.";
			}
		elseif($obj['job_decision'] == 'clone_jobs') // clone jobs
			{
				$job = new job();
				$j1_details = $job->getJobDetails($obj['j1_id']);
				$j2_details = $job->getJobDetails($obj['j2_id']);
				
				$cloned_job_ids = $job->CloneReturnJob($obj['j1_id'], $obj['j2_id']);
				
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Bkg ID '.$j1_details['id'].' - CLONED', 'New Job id '.$cloned_job_ids['j1_id'].'', '');
				$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Bkg ID '.$j2_details['id'].' - CLONED', 'New Job id '.$cloned_job_ids['j2_id'].'', '');
				
				$return_message ="Booking IDs ".$obj['j1_id']." & ".$obj['j2_id']." - CLONED<br/>New Cloned Booking IDs - ".$cloned_job_ids['j1_id']." & ".$cloned_job_ids['j2_id']."";
				$data1 = $cloned_job_ids['j1_id'];
				$data2 = $cloned_job_ids['j2_id'];
				
				
			}
		elseif ($obj['job_decision'] == 'rev_clone_jobs') // reverse clone jobs
			{
				$job = new job();
				$j1_details = $job->getJobDetails($obj['j1_id']);
				$j2_details = $job->getJobDetails($obj['j2_id']);
				
				$cloned_job_ids = $job->ReverseCloneReturnJob($obj['j1_id'], $obj['j2_id']);
				
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Bkg ID '.$j1_details['id'].' - CLONED', 'New Job id '.$cloned_job_ids['j1_id'].'', '');
				$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Bkg ID '.$j2_details['id'].' - CLONED', 'New Job id '.$cloned_job_ids['j2_id'].'', '');
				
				$return_message ="Booking IDs ".$obj['j1_id']." & ".$obj['j2_id']." - REVERSE CLONED<br/>New Cloned Booking IDs - ".$cloned_job_ids['j1_id']." & ".$cloned_job_ids['j2_id']."";
				$data1 = $cloned_job_ids['j1_id'];
				$data2 = $cloned_job_ids['j2_id'];
			}
		elseif($obj['job_decision'] == 'reactivate_jobs') // reactivate jobs
			{
				$job = new job();
				
				$j1_details 	= $job->getJobDetails($obj['j1_id']);
				$j2_details 	= $job->getJobDetails($obj['j2_id']);
				
				$old_j1_status 	= $job->getJobStatusDetails($j1_details['job_status']);
				$old_j2_status 	= $job->getJobStatusDetails($j2_details['job_status']);
				
				$job->updateJob($j1_details['id'], 'job_status', '20');
				$job->updateJob($j2_details['id'], 'job_status', '20');
				
				$job->addJobLog($j1_details['id'], $obj['USER_ID'], 'Bkg ID '.$j1_details['id'].' - Confirmed', 'Confirmed', $old_j1_status['details']);
				$job->addJobLog($j2_details['id'], $obj['USER_ID'], 'Bkg ID '.$j2_details['id'].' - Confirmed', 'Confirmed', $old_j2_status['details']);
				
				$return_message ="Booking ID ".$obj['j1_id']." & ".$obj['j2_id']." - Confirmed<br/>Please note, on confirmation, email notification has not been sent to anyone.";
			}
		elseif($obj['job_decision'] == 'delete_jobs') // delete jobs
			{
				$job = new job();
				$job->deleteJobs($obj['j1_id'], $obj['j2_id']);
				$return_message ="Booking ID ".$obj['j1_id']." &  ".$obj['j2_id']."- DELETED<br/> If this is part of a return booking, then the other booking has been changed to One Way booking";
			}
			
			
			
		else if($obj['job_decision']=='confirm_j1') // confirm job 1
			{
				
				$job 			= new job();
				$job_reference 	= new jobReference();
				$mailer 		= new mailer();
				$user			= new user();
				
				
				$job_details 	= $job->getJobDetails($obj['j1_id']);
				$old_status 	= $job->getJobStatusDetails($job_details['job_status']);
				$job->updateJob($job_details['id'], 'job_status', '20');
				$job->addJobLog($job_details['id'], $obj['USER_ID'], 'Bkg ID '.$job_details['id'].' - Confirmed', 'Confirmed', $old_status['details']);
				$job_ref_details = $job_reference->getJobReferenceDetails($job_details['job_reference_id']);
				
				if($job_ref_details['acc_type'] == '3') //if this is booked by university or student
					{
						$mailer->sendUniversityMail($job_details['job_reference_id'], '6');
					}
				else
					{
						if($job_ref_details['bkg_by_id'] == $job_ref_details['pax_id']) // means booking made by and pax are the same
							{
								$user_details = $user->getUserDetails($job_ref_details['bkg_by_id']);
								$mailer->sendMailToClientForIndividualBooking($obj['j1_id'], '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);								$push_title = $obj['j1_id'];
								$push_message = 'Job Id : '.$obj['j1_id'].' has been confirmed.';
								$job->commonGooglePushNotification($job_ref_details['bkg_by_id'],$push_message,$push_title);
								
							}
						else // send confirmation email to both booking made by and pax
							{
								//send confirmation to booking made by
								$bkg_made_by 		= $user->getUserDetails($job_ref_details['bkg_by_id']);
								$mailer->sendMailToClientForIndividualBooking($obj['j1_id'], '1',  $bkg_made_by['email'], $bkg_made_by['title'], $bkg_made_by['fname'], $bkg_made_by['lname']);
								
								//send confirmation to pax	
								$pax 		= $user->getUserDetails($job_ref_details['pax_id']);
								$mailer->sendMailToClientForIndividualBooking($obj['j1_id'], '1',  $pax['email'], $pax['title'], $pax['fname'], $pax['lname']);
								$push_title = $obj['j1_id'];
								$push_message = 'Job Id : '.$obj['j1_id'].' has been confirmed.';
								$job->commonGooglePushNotification($job_ref_details['bkg_by_id'],$push_message,$push_title);
								$push_message = 'Job Id : '.$obj['j1_id'].' has been confirmed.';
								$job->commonGooglePushNotification($job_ref_details['pax_id'],$push_message,$push_title);
								

							}
					}
				$return_message ="Booking ID ".$obj['j1_id']." - Confirmed.<br/>Confirmation emails have been sent to both booking made by and pax.";
			}
		
		else if($obj['job_decision']=='reconfirm_j1') // confirm job 1
			{
				
				$job 			= new job();
				$job_reference 	= new jobReference();
				$mailer 		= new mailer();
				$user			= new user();
				
				
				$job_details 	= $job->getJobDetails($obj['j1_id']);
				$old_status 	= $job->getJobStatusDetails($job_details['job_status']);
				//$job->updateJob($job_details['id'], 'job_status', '20');
			//	$job->addJobLog($job_details['id'], $obj['USER_ID'], 'Bkg ID '.$job_details['id'].' - Confirmed', 'Confirmed', $old_status['details']);
				$job_ref_details = $job_reference->getJobReferenceDetails($job_details['job_reference_id']);

				if($job_ref_details['acc_type'] == '3') //if this is booked by university or student
					{
						$mailer->sendUniversityMail($job_details['job_reference_id'], '6');
					}
				else
					{
						if($job_ref_details['bkg_by_id'] == $job_ref_details['pax_id']) // means booking made by and pax are the same
							{
								$user_details = $user->getUserDetails($job_ref_details['bkg_by_id']);
								$mailer->sendMailToClientForIndividualBooking($obj['j1_id'], '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
								$push_title = $obj['j1_id'];
								$push_message = 'Job Id : '.$obj['j1_id'].' has been reconfirmed.';
								$job->commonGooglePushNotification($job_ref_details['bkg_by_id'],$push_message,$push_title);
								
							}
						else // send confirmation email to both booking made by and pax
							{
								//send confirmation to booking made by
								$bkg_made_by 		= $user->getUserDetails($job_ref_details['bkg_by_id']);
								$mailer->sendMailToClientForIndividualBooking($obj['j1_id'], '1',  $bkg_made_by['email'], $bkg_made_by['title'], $bkg_made_by['fname'], $bkg_made_by['lname']);
								
								//send confirmation to pax	
								$pax 		= $user->getUserDetails($job_ref_details['pax_id']);
								$mailer->sendMailToClientForIndividualBooking($obj['j1_id'], '1',  $pax['email'], $pax['title'], $pax['fname'], $pax['lname']);
								$push_title = $obj['j1_id'];
								$push_message = 'Job Id : '.$obj['j1_id'].' has been reconfirmed.';
								$job->commonGooglePushNotification($job_ref_details['bkg_by_id'],$push_message,$push_title);
								$push_title = $obj['j1_id'];
								$push_message = 'Job Id : '.$obj['j1_id'].' has been reconfirmed.';
								$job->commonGooglePushNotification($job_ref_details['pax_id'],$push_message,$push_title);
							}
					}
				$return_message ="Confirmation email of Booking ID ".$obj['j1_id']." has been sent to both booking made by and pax.";
			}

		elseif($obj['job_decision'] == 'decline_j1') // decline job 1
			{
				$job 			= new job();
				$job_reference 	= new jobReference();
				$mailer 		= new mailer();
				$user			= new user();
				
				
				$job_details 	= $job->getJobDetails($obj['j1_id']);
				$old_status 	= $job->getJobStatusDetails($job_details['job_status']);
				$job->updateJob($job_details['id'], 'job_status', '90');
				$job->addJobLog($job_details['id'], $obj['USER_ID'], 'Bkg ID '.$job_details['id'].' - Confirmed', 'Confirmed', $old_status['details']);
				$job_ref_details = $job_reference->getJobReferenceDetails($job_details['job_reference_id']);
				
				if($job_ref_details['acc_type'] == '3') //if this is booked by university or student
					{
						$mailer->sendUniversityMail($job_details['job_reference_id'], '4');
					}
				else
					{
						if($job_ref_details['bkg_by_id'] == $job_ref_details['pax_id']) // means booking made by and pax are the same
							{
								$user_details = $user->getUserDetails($job_ref_details['bkg_by_id']);
								$mailer->sendMailToClientForIndividualBooking($obj['j1_id'], '2', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
								
							}
						else // send confirmation email to both booking made by and pax
							{
								//send confirmation to booking made by
								$bkg_made_by 		= $user->getUserDetails($job_ref_details['bkg_by_id']);
								$mailer->sendMailToClientForIndividualBooking($obj['j1_id'], '2',  $bkg_made_by['email'], $bkg_made_by['title'], $bkg_made_by['fname'], $bkg_made_by['lname']);
								
								//send confirmation to pax	
								$pax 		= $user->getUserDetails($job_ref_details['pax_id']);
								$mailer->sendMailToClientForIndividualBooking($obj['j1_id'], '2',  $pax['email'], $pax['title'], $pax['fname'], $pax['lname']);
							}
					}
				$return_message ="Booking ID ".$obj['j1_id']." - Declined<br/>Booking rejected notification emails have been sent to both booking made by and pax.";
			}
		elseif($obj['job_decision'] == 'cancel_j1') // cancel job 1
			{
				$job = new job();
				
				$job_details 	= $job->getJobDetails($obj['j1_id']);
				$job_ref_details = $job_reference->getJobReferenceDetails($job_details['job_reference_id']);
				$old_status 	= $job->getJobStatusDetails($job_details['job_status']);
				$job->updateJob($job_details['id'], 'job_status', '100');
				$job->addJobLog($job_details['id'], $obj['USER_ID'], 'Bkg ID '.$job_details['id'].' - Cancelled', 'Cancelled', $old_status['details']);
				$push_title = $job_details['id'];
				$push_message = 'Job Id : '.$job_details['id'].' has been cancelled by admin.';
				if($job_ref_details['bkg_by_id']==$job_ref_details['pax_id'])
				{
					$job->commonGooglePushNotification($job_ref_details['bkg_by_id'],$push_message,$push_title);
				}
				else
				{
					$job->commonGooglePushNotification($job_ref_details['bkg_by_id'],$push_message,$push_title);
					$job->commonGooglePushNotification($job_ref_details['pax_id'],$push_message,$push_title);
				}
				if($job_ref_details['acc_type'] == '3') //if this is booked by university or student
					{
						$mailer->sendUniversityMail($job_details['job_reference_id'], '8');
					}
				$return_message ="Booking ID ".$obj['j1_id']." - Cancelled<br/>Please note, on cancellation, email notification is not sent to anyone.";
			}
		elseif($obj['job_decision'] == 'clone_j1') // clone job 1
			{
				$job = new job();
				$job_details = $job->getJobDetails($obj['j1_id']);
				$cloned_job_id = $job->cloneIndividualJob($obj['j1_id']);
				$job->addJobLog($job_details['id'], $obj['USER_ID'], 'Bkg ID '.$job_details['id'].' - CLONED', 'New Job id '.$cloned_job_id.'', '');
				$return_message ="Booking ID ".$obj['j1_id']." - CLONED<br/>New Cloned Booking ID - ".$cloned_job_id."";
				$data1 = $cloned_job_id;
			}
		elseif ($obj['job_decision'] == 'rev_clone_j1') // reverse clone job 1
			{
				$job = new job();
				$job_details = $job->getJobDetails($obj['j1_id']);
				$re_cloned_job_id = $job->ReverseCloneIndividualJob($obj['j1_id']);
				$job->addJobLog($job_details['id'], $obj['USER_ID'], 'Bkg ID '.$job_details['id'].' - CLONED', 'New Job id '.$re_cloned_job_id.'', '');
				$return_message ="Booking ID ".$obj['j1_id']." - REVERSE CLONED<br/>New Cloned Booking ID - ".$re_cloned_job_id."";
				$data1 = $re_cloned_job_id;
			}
		elseif($obj['job_decision'] == 'reactivate_j1') // reactivate job 1
			{
				$job = new job();
				
				$job_details 	= $job->getJobDetails($obj['j1_id']);
				$old_status 	= $job->getJobStatusDetails($job_details['job_status']);
				$job->updateJob($job_details['id'], 'job_status', '20');
				$job->addJobLog($job_details['id'], $obj['USER_ID'], 'Bkg ID '.$job_details['id'].' - Confirmed', 'Confirmed', $old_status['details']);
				$return_message ="Booking ID ".$obj['j1_id']." - Confirmed<br/>Please note, on confirmation, email notification is not sent to anyone.";
			}
		elseif($obj['job_decision'] == 'delete_j1') // delete job 1
			{
				$job = new job();
				$job->deleteAJob($obj['j1_id']);
				$return_message ="Booking ID ".$obj['j1_id']." - DELETED<br/> If this is part of a return booking, then the other booking has been changed to One Way booking";
			}
		elseif($obj['job_decision']=='confirm_j2') // confirm job 2
			{
				
				$job 			= new job();
				$job_reference 	= new jobReference();
				$mailer 		= new mailer();
				$user			= new user();
				
				
				$job_details 	= $job->getJobDetails($obj['j2_id']);
				$old_status 	= $job->getJobStatusDetails($job_details['job_status']);
				$job->updateJob($job_details['id'], 'job_status', '20');
				$job->addJobLog($job_details['id'], $obj['USER_ID'], 'Bkg ID '.$job_details['id'].' - Confirmed', 'Confirmed', $old_status['details']);
				$job_ref_details = $job_reference->getJobReferenceDetails($job_details['job_reference_id']);
				
				if($job_ref_details['acc_type'] == '3') //if this is booked by university or student
				{
					$mailer->sendMailToClientOnBooking($job_details['job_reference_id'], '1', $job_ref_details['std_email'], $job_ref_details['std_title'], $job_ref_details['std_fname'], $job_ref_details['std_lname']);
				}
				
				if($job_ref_details['charge_acc_id'] == '19' || $job_ref_details['charge_acc_id'] == '15')
				{
				}
				else
				{
					if($job_ref_details['bkg_by_id'] == $job_ref_details['pax_id']) // means booking made by and pax are the same
						{
							$user_details = $user->getUserDetails($job_ref_details['bkg_by_id']);
							$mailer->sendMailToClientForIndividualBooking($obj['j2_id'], '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
							
						}
					else // send confirmation email to both booking made by and pax
						{
							//send confirmation to booking made by
							$bkg_made_by 		= $user->getUserDetails($job_ref_details['bkg_by_id']);
							$mailer->sendMailToClientForIndividualBooking($obj['j2_id'], '1',  $bkg_made_by['email'], $bkg_made_by['title'], $bkg_made_by['fname'], $bkg_made_by['lname']);
							
							//send confirmation to pax	
							$pax 		= $user->getUserDetails($job_ref_details['pax_id']);
							$mailer->sendMailToClientForIndividualBooking($obj['j2_id'], '1',  $pax['email'], $pax['title'], $pax['fname'], $pax['lname']);
						}
				}
				$return_message ="Booking ID ".$obj['j2_id']." - Confirmed.<br/>Confirmation emails have been sent to both booking made by and pax.";
			}

			else if($obj['job_decision']=='reconfirm_j2') // confirm job 1
			{
				
				$job 			= new job();
				$job_reference 	= new jobReference();
				$mailer 		= new mailer();
				$user			= new user();
				
				
				$job_details 	= $job->getJobDetails($obj['j2_id']);
				$old_status 	= $job->getJobStatusDetails($job_details['job_status']);
				//$job->updateJob($job_details['id'], 'job_status', '20');
			//	$job->addJobLog($job_details['id'], $obj['USER_ID'], 'Bkg ID '.$job_details['id'].' - Confirmed', 'Confirmed', $old_status['details']);
				$job_ref_details = $job_reference->getJobReferenceDetails($job_details['job_reference_id']);

				if($job_ref_details['acc_type'] == '3') //if this is booked by university or student
				{
					$mailer->sendMailToClientOnBooking($job_details['job_reference_id'], '1', $job_ref_details['std_email'], $job_ref_details['std_title'], $job_ref_details['std_fname'], $job_ref_details['std_lname']);
				}
				
				if($job_ref_details['charge_acc_id'] == '19' || $job_ref_details['charge_acc_id'] == '15')
				{
				}
				else
				{
					if($job_ref_details['bkg_by_id'] == $job_ref_details['pax_id']) // means booking made by and pax are the same
						{
							$user_details = $user->getUserDetails($job_ref_details['bkg_by_id']);
							$mailer->sendMailToClientForIndividualBooking($obj['j2_id'], '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
							
						}
					else // send confirmation email to both booking made by and pax
						{
							//send confirmation to booking made by
							$bkg_made_by 		= $user->getUserDetails($job_ref_details['bkg_by_id']);
							$mailer->sendMailToClientForIndividualBooking($obj['j2_id'], '1',  $bkg_made_by['email'], $bkg_made_by['title'], $bkg_made_by['fname'], $bkg_made_by['lname']);
							
							//send confirmation to pax	
							$pax 		= $user->getUserDetails($job_ref_details['pax_id']);
							$mailer->sendMailToClientForIndividualBooking($obj['j2_id'], '1',  $pax['email'], $pax['title'], $pax['fname'], $pax['lname']);
						}
				}
				$return_message ="Confirmation email of Booking ID ".$obj['j2_id']." has been sent to both booking made by and pax.";
			}

			
		elseif($obj['job_decision'] == 'decline_j2') // decline job 2
			{
				$job 			= new job();
				$job_reference 	= new jobReference();
				$mailer 		= new mailer();
				$user			= new user();
				
				
				$job_details 	= $job->getJobDetails($obj['j2_id']);
				$old_status 	= $job->getJobStatusDetails($job_details['job_status']);
				$job->updateJob($job_details['id'], 'job_status', '90');
				$job->addJobLog($job_details['id'], $obj['USER_ID'], 'Bkg ID '.$job_details['id'].' - Confirmed', 'Confirmed', $old_status['details']);
				$job_ref_details = $job_reference->getJobReferenceDetails($job_details['job_reference_id']);

				if($job_ref_details['acc_type'] == '3') //if this is booked by university or student
				{
					$mailer->sendMailToClientOnBooking($job_details['job_reference_id'], '2', $job_ref_details['std_email'], $job_ref_details['std_title'], $job_ref_details['std_fname'], $job_ref_details['std_lname']);
				}
				
				if($job_ref_details['bkg_by_id'] == $job_ref_details['pax_id']) // means booking made by and pax are the same
					{
						$user_details = $user->getUserDetails($job_ref_details['bkg_by_id']);
						$mailer->sendMailToClientForIndividualBooking($obj['j2_id'], '2', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
						
					}
				else // send confirmation email to both booking made by and pax
					{
						//send confirmation to booking made by
						$bkg_made_by 		= $user->getUserDetails($job_ref_details['bkg_by_id']);
						$mailer->sendMailToClientForIndividualBooking($obj['j2_id'], '2',  $bkg_made_by['email'], $bkg_made_by['title'], $bkg_made_by['fname'], $bkg_made_by['lname']);
						
						//send confirmation to pax	
						$pax 		= $user->getUserDetails($job_ref_details['pax_id']);
						$mailer->sendMailToClientForIndividualBooking($obj['j2_id'], '2',  $pax['email'], $pax['title'], $pax['fname'], $pax['lname']);
						

					}
				$return_message ="Booking ID ".$obj['j2_id']." - Declined<br/>Booking rejected notification emails have been sent to both booking made by and pax.";
			}
		elseif($obj['job_decision'] == 'cancel_j2') // cancel job 2
			{
				$job = new job();
				
				$job_details 	= $job->getJobDetails($obj['j2_id']);
				$old_status 	= $job->getJobStatusDetails($job_details['job_status']);
				$job->updateJob($job_details['id'], 'job_status', '100');
				$job->addJobLog($job_details['id'], $obj['USER_ID'], 'Bkg ID '.$job_details['id'].' - Cancelled', 'Cancelled', $old_status['details']);
				$return_message ="Booking ID ".$obj['j2_id']." - Cancelled<br/>Please note, on cancellation, email notification is not sent to anyone.";
			}
		elseif($obj['job_decision'] == 'clone_j2') // clone job 2
			{
				$job = new job();
				$job_details = $job->getJobDetails($obj['j2_id']);
				$cloned_job_id = $job->cloneIndividualJob($obj['j2_id']);
				$job->addJobLog($job_details['id'], $obj['USER_ID'], 'Bkg ID '.$job_details['id'].' - CLONED', 'New Job id '.$cloned_job_id.'', '');
				$return_message ="Booking ID ".$obj['j2_id']." - CLONED<br/>New Cloned Booking ID - ".$cloned_job_id."";
				$data1 = $cloned_job_id;
			}
		elseif ($obj['job_decision'] == 'rev_clone_j2') // reverse clone job 2
			{
				$job = new job();
				$job_details = $job->getJobDetails($obj['j2_id']);
				$re_cloned_job_id = $job->ReverseCloneIndividualJob($obj['j2_id']);
				$job->addJobLog($job_details['id'], $obj['USER_ID'], 'Bkg ID '.$job_details['id'].' - CLONED', 'New Job id '.$re_cloned_job_id.'', '');
				$return_message ="Booking ID ".$obj['j2_id']." - REVERSE CLONED<br/>New Cloned Booking ID - ".$re_cloned_job_id."";
				$data1 = $re_cloned_job_id;
			}
		elseif($obj['job_decision'] == 'reactivate_j2') // reactivate job 2
			{
				$job = new job();
				
				$job_details 	= $job->getJobDetails($obj['j2_id']);
				$old_status 	= $job->getJobStatusDetails($job_details['job_status']);
				$job->updateJob($job_details['id'], 'job_status', '20');
				$job->addJobLog($job_details['id'], $obj['USER_ID'], 'Bkg ID '.$job_details['id'].' - Confirmed', 'Confirmed', $old_status['details']);
				$return_message ="Booking ID ".$obj['j2_id']." - Confirmed<br/>Please note, on confirmation, email notification is not sent to anyone.";
			}
		elseif($obj['job_decision'] == 'delete_j2') // delete job 2
			{
				$job = new job();
				$job->deleteAJob($obj['j2_id']);
				$return_message ="Booking ID ".$obj['j2_id']." - DELETED<br/> If this is part of a return booking, then the other booking has been changed to One Way booking";
			}
		elseif($obj['job_decision'] == 'resend_j1') // resend job 1 details to driver
			{
				$job_details 	= $job->getJobDetails($obj['j1_id']);
				$job_driver_details = $job->getJob_driver_details($obj['j1_id']);
				$driver_extra_pay = $job->get_Job_driver_extra_pay($obj['j1_id']);
				//print_r($job_driver_details);
				if($job_details['driver_id'] != '' || $job_details['driver_id'] != 0)
					{
						$driver_pr = ($job_driver_details['allocated_amount'] !='0.00') ? $job_driver_details['allocated_amount'] : $job_driver_details['accepted_amount'];
						$driver_pr = $driver_pr + $driver_extra_pay['extra_pay'];
						$mailer->sendJobDetailsToDriver($job_details['id'], $job_details['driver_id'], $driver_pr, $job_details['driver_notes']);	
						$job->addJobLog($job_details['id'], $obj['USER_ID'], 'Details resent to driver', '', '');
						$return_message ="Booking ID ".$obj['j1_id']." - details resent to driver. Thanks";
					}
				else
					{
						$return_message ="ERROR!!!! No record of job being accepted or rogered found. Please get the accepted first. Thanks";
					}
			}
			
		elseif($obj['job_decision'] == 'resend_j2') // resend job 2 details to driver
			{
				$job_details 	= $job->getJobDetails($obj['j2_id']);
				$job_driver_details = $job->getJob_driver_details($obj['j2_id']);
				$driver_extra_pay = $job->get_Job_driver_extra_pay($obj['j2_id']);
				if($job_details['driver_id'] != '' || $job_details['driver_id'] != 0)
					{
						$driver_pr = ($job_driver_details['allocated_amount'] !='0.00') ? $job_driver_details['allocated_amount'] : $job_driver_details['accepted_amount'];
						$driver_pr = $driver_pr + $driver_extra_pay['extra_pay'];
						$mailer->sendJobDetailsToDriver($job_details['id'], $job_details['driver_id'], $driver_pr, $job_details['driver_notes']);
						$job->addJobLog($job_details['id'], $obj['USER_ID'], 'Details resent to driver', '', '');				
						$return_message ="Booking ID ".$obj['j2_id']." - details resent to driver. Thanks";
					}
				else
					{
						$return_message ="ERROR!!!! No record of job being accepted or rogered found. Please get the accepted first. Thanks";
					}
			}	
		elseif($obj['job_decision'] == 'pay_j1') // Pay for job 1
			{
				$job = new job();
				$user = new User();
				$transaction = new Transaction();
				$account = new Account();
				
				$j1_details = $job->getJobDetails($obj['j1_id']);
				$old_driver_status = $job->getDriverStatusDetails($j1_details['driver_status']);
				
				$query1 = "SELECT MAX(id) as id FROM job__driver where job_id='".$obj['j1_id']."' AND  accepted_by!='0'";
				$database = new database;
				$result1 = $database->query($query1);
				$row1 = mysql_fetch_array($result1);
				
				$query2 = "SELECT * from job__driver where id='".$row1['id']."'";
				$result2 = $database->query($query2);
				$row2 = mysql_fetch_array($result2);
				$accepted_by = $user->getUserDetails($row2['accepted_by']);
				$accepted_by_mobile = $user->getUserPhones($row2['accepted_by']);
				
				//get entered by
				$entered_by 		= 	$user->getUserDetails($obj['USER_ID']);
				$entered_by_name 	= 	''.$entered_by['fname'].' '.$entered_by['lname'].'';
				
				$acc_cr_label 		= 	'drivers__accounts_'.$row2['accepted_by'].'';
				$acc_dr_label 		= 	'acc__general';

				$description_debit_entry = "Paid for Booking ID # ".$obj['j1_id']." (Acc Credited - ".$acc_cr_label.")";
				$description_credit_entry = "Paid for Booking ID # ".$obj['j1_id']." (Acc Debited - ".$acc_dr_label.")";
				
				
				//Add this in transaction table
				$this_trx_id = $transaction->newTransaction($acc_dr_label, $acc_cr_label);
				//Account entry of debit in Allied General Account
				$acc_dr_entry_id = $account->debitEntry($this_trx_id, $entered_by_name, $description_debit_entry, $acc_dr_label,  $acc_cr_label, $row2['accepted_amount']);
				//Account entry of credit in drivers table	
				$acc_cr_entry_id = $account->creditEntry($this_trx_id, $entered_by_name, $description_credit_entry, $acc_cr_label, $acc_dr_label, $row2['accepted_amount']);
				
				//add this to job__driver table
				$job->addNewDriverToJob($obj['j1_id'], '', '', '', '', '', '', ''.$row2['accepted_by'].'', ''.$row2['accepted_amount'].'', '', '', ''); // Add this to job__driver table
				//change the driver status of the job to paid
				$job->updateJob($obj['j1_id'], 'driver_status', '5'); // PAID
				$job->updateJob($obj['j1_id'], 'driver_id', $row2['accepted_by']); // Put this driver in job table
				$job->updateJob($obj['j1_id'], 'driver_price', $row2['accepted_amount']); // Put the driver price in the job table
				//make a log entry in the job log table
				$job->addJobLog($obj['j1_id'], $entered_by_name, 'Paid $'.$row2['accepted_amount'].' for Bkg ID '.$obj['j1_id'].' to '.$accepted_by['fname'].' '.$accepted_by['lname'].' vide Trx ID -'.$this_trx_id.'', 'PAID', $old_driver_status['details']);
				$return_message ="Booking ID ".$obj['j1_id']." - PAID<br/>";
			}
		elseif($obj['job_decision'] == 'pay_j2') // Pay for job 2
			{
				$job = new job();
				$user = new User();
				$transaction = new Transaction();
				$account = new Account();
				
				$j2_details = $job->getJobDetails($obj['j2_id']);
				$old_driver_status = $job->getDriverStatusDetails($j2_details['driver_status']);
				
				$query1 = "SELECT MAX(id) as id FROM job__driver where job_id='".$obj['j2_id']."' AND  accepted_by!='0'";
				$database = new database;
				$result1 = $database->query($query1);
				$row1 = mysql_fetch_array($result1);
				
				$query2 = "SELECT * from job__driver where id='".$row1['id']."'";
				$result2 = $database->query($query2);
				$row2 = mysql_fetch_array($result2);
				$accepted_by = $user->getUserDetails($row2['accepted_by']);
				$accepted_by_mobile = $user->getUserPhones($row2['accepted_by']);
				
				//get entered by
				$entered_by 		= 	$user->getUserDetails($obj['USER_ID']);
				$entered_by_name 	= 	''.$entered_by['fname'].' '.$entered_by['lname'].'';
				
				$acc_cr_label 		= 	'drivers__accounts_'.$row2['accepted_by'].'';
				$acc_dr_label 		= 	'acc__general';

				$description_debit_entry = "Paid for Booking ID # ".$obj['j2_id']." (Acc Credited - ".$acc_cr_label.")";
				$description_credit_entry = "Paid for Booking ID # ".$obj['j2_id']." (Acc Debited - ".$acc_dr_label.")";
				
				
				//Add this in transaction table
				$this_trx_id = $transaction->newTransaction($acc_dr_label, $acc_cr_label);
				//Account entry of debit in Allied General Account
				$acc_dr_entry_id = $account->debitEntry($this_trx_id, $entered_by_name, $description_debit_entry, $acc_dr_label,  $acc_cr_label, $row2['accepted_amount']);
				//Account entry of credit in drivers table	
				$acc_cr_entry_id = $account->creditEntry($this_trx_id, $entered_by_name, $description_credit_entry, $acc_cr_label, $acc_dr_label, $row2['accepted_amount']);
				
				//add this to job__driver table
				$job->addNewDriverToJob($obj['j2_id'], '', '', '', '', '', '', ''.$row2['accepted_by'].'', ''.$row2['accepted_amount'].'', '', '', ''); // Add this to job__driver table
				//change the driver status of the job to paid
				$job->updateJob($obj['j2_id'], 'driver_status', '5'); // PAID
				$job->updateJob($obj['j2_id'], 'driver_id', $row2['accepted_by']); // Put this driver in job table
				$job->updateJob($obj['j2_id'], 'driver_price', $row2['accepted_amount']); // Put the driver price in the job table
				//make a log entry in the job log table
				$job->addJobLog($obj['j2_id'], $entered_by_name, 'Paid $'.$row2['accepted_amount'].' for Bkg ID '.$obj['j2_id'].' to '.$accepted_by['fname'].' '.$accepted_by['lname'].' vide Trx ID -'.$this_trx_id.'', 'PAID', $old_driver_status['details']);
				$return_message ="Booking ID ".$obj['j2_id']." - PAID<br/>";
			}
		elseif($obj['job_decision'] == 'tear_off_j1') // Tear off driver statuses for job 1
			{
				$database = new database;
				$user = new User();
				//get entered by
				$entered_by 		= 	$user->getUserDetails($obj['USER_ID']);
				$entered_by_name 	= 	''.$entered_by['fname'].' '.$entered_by['lname'].'';
				
				$j1_details = $job->getJobDetails($obj['j1_id']);
				$old_driver_status = $job->getDriverStatusDetails($j1_details['driver_status']);
				
				$query = "DELETE FROM job__driver where job_id='".$obj['j1_id']."' AND  accepted_by!='0'";
				$result = $database->query($query);
				
				$query = "DELETE FROM job__driver where job_id='".$obj['j1_id']."' AND  offered_to!='0'";
				$result = $database->query($query);
				
				$query = "DELETE FROM job__driver where job_id='".$obj['j1_id']."' AND  allocated_to!='0'";
				$result = $database->query($query);
				
				$query = "DELETE FROM job__driver_extra_pay where job_id='".$obj['j1_id']."'";
				$result = $database->query($query);
				
				$job->updateJob($obj['j1_id'], 'drv_fee', '0.00'); // Make the driver fee 0.00
				$job->updateJob($obj['j1_id'], 'driver_status', '0'); // TEAR OFF
				$job->updateJob($obj['j1_id'], 'driver_id', '0'); // Put this driver in job table
				$job->updateJob($obj['j1_id'], 'driver_price', '0.00'); // Put the driver price in the job table
				//make a log entry in the job log table
				$job->addJobLog($obj['j1_id'], $entered_by_name, 'DRIVER STATUS - RESET', 'NIL', $old_driver_status['details']);
				$return_message ="Booking ID ".$obj['j1_id']." - DRIVER STATUS - RESET<br/>";
			}
		elseif($obj['job_decision'] == 'tear_off_j2') // Tear off driver statuses for job 2
			{
				$database = new database;
				$user = new User();
				//get entered by
				$entered_by 		= 	$user->getUserDetails($obj['USER_ID']);
				$entered_by_name 	= 	''.$entered_by['fname'].' '.$entered_by['lname'].'';
				
				$j2_details = $job->getJobDetails($obj['j2_id']);
				$old_driver_status = $job->getDriverStatusDetails($j2_details['driver_status']);
				
				$query = "DELETE FROM job__driver where job_id='".$obj['j2_id']."' AND  accepted_by!='0'";
				$result = $database->query($query);
				
				$query = "DELETE FROM job__driver where job_id='".$obj['j2_id']."' AND  offered_to!='0'";
				$result = $database->query($query);
				
				$query = "DELETE FROM job__driver where job_id='".$obj['j2_id']."' AND  allocated_to!='0'";
				$result = $database->query($query);
				
				$query = "DELETE FROM job__driver_extra_pay where job_id='".$obj['j2_id']."'";
				$result = $database->query($query);
				
				$job->updateJob($obj['j2_id'], 'drv_fee', '0.00'); // Make the driver fee 0.00
				$job->updateJob($obj['j2_id'], 'driver_status', '0'); // TEAR OFF
				$job->updateJob($obj['j2_id'], 'driver_id', '0'); // Put this driver in job table
				$job->updateJob($obj['j2_id'], 'driver_price', '0.00'); // Put the driver price in the job table
				//make a log entry in the job log table
				$job->addJobLog($obj['j2_id'], $entered_by_name, 'DRIVER STATUS - RESET', 'NIL', $old_driver_status['details']);
				$return_message ="Booking ID ".$obj['j2_id']." - DRIVER STATUS - RESET<br/>";
			}
		elseif($obj['job_decision'] == 'pay_extra_j1') // Pay extra to driver for job 1
			{
				$database = new database;
				$transaction = new Transaction();
				$account = new Account();
				$user = new User();
				
				$entered_by 		= 	$user->getUserDetails($obj['USER_ID']);
				$entered_by_name 	= 	''.$entered_by['fname'].' '.$entered_by['lname'].'';
				//First check from job__driver table if there is any driver paid to for this job
				$query1 = "SELECT MAX(id) as id FROM job__driver where job_id='".$obj['j1_id']."' AND  paid_to!='0'";
				$result1 = $database->query($query1);
				$row1 = mysql_fetch_array($result1);
				if($row1['id'] != '') // which driver has the money gone to
					{
						$query2 = "SELECT * from job__driver where id='".$row1['id']."'";
						$result2 = $database->query($query2);
						$row2 = mysql_fetch_array($result2);
						$driver_id = $row2['paid_to'];
					}
				else
					{
						$query3 = "SELECT MAX(id) as id FROM job__driver where job_id='".$obj['j1_id']."' AND  accepted_by!='0'";
						$result3 = $database->query($query3);
						$row3 = mysql_fetch_array($result3);
						if($row3['id'] != '') // which driver has accepted the job
							{
								$query4 = "SELECT * from job__driver where id='".$row3['id']."'";
								$result4 = $database->query($query4);
								$row4 = mysql_fetch_array($result4);
								$driver_id = $row4['accepted_by'];
							}
					}
				
				if($driver_id != '')
					{
						$driver_details = $user->getUserDetails($driver_id);
						$job_details = $job->getJobDetails($obj['j1_id']);
						//get the driver fee from driver table and increase it by this new amount
						$new_driver_fee = $job_details['drv_fee'] + $obj['j1_driver_ex_price'];
						$job->updateJob($obj['j1_id'], 'drv_fee', ''.$new_driver_fee.''); // Make the driver fee 0.00
						//get entered by
						$entered_by 		= 	$user->getUserDetails($obj['USER_ID']);
						$entered_by_name 	= 	''.$entered_by['fname'].' '.$entered_by['lname'].'';
						
						$query5 = "INSERT INTO job__driver_extra_pay (job_id, driver_id, notes, amount)
									VALUES
									('".$obj['j1_id']."', '".$driver_id."', '".$obj['j1_driver_ex_notes']."', '".$obj['j1_driver_ex_price']."')";
						$result5 = $database->query($query5);
						//Add job log
						$job->addJobLog($obj['j1_id'], $entered_by_name, 'DRIVER PAID EXTRA', '$'.$obj['j1_driver_ex_price'].' paid to driver for '.$obj['j1_driver_ex_notes'].'', 'NIL');
						$job->addJobLog($obj['j1_id'], $entered_by_name, 'DRIVER TOTAL FEE CHANGED', ''.$new_driver_fee.'', ''.$job_details['drv_fee'].'');
						$acc_cr_label 		= 	'drivers__accounts_'.$driver_id.'';
						$acc_dr_label 		= 	'acc__general';

						$description_debit_entry = "Extra Paid for Booking ID # ".$obj['j1_id']." (Acc Credited - ".$acc_cr_label.")";
						$description_credit_entry = "Extra Paid for Booking ID # ".$obj['j1_id']." (Acc Debited - ".$acc_dr_label.")";
						//Add this in transaction table
						$this_trx_id = $transaction->newTransaction($acc_dr_label, $acc_cr_label);
						//Account entry of debit in Allied General Account
						$acc_dr_entry_id = $account->debitEntry($this_trx_id, $entered_by_name, $description_debit_entry, $acc_dr_label,  $acc_cr_label, $obj['j1_driver_ex_price']);
						//Account entry of credit in drivers table	
						$acc_cr_entry_id = $account->creditEntry($this_trx_id, $entered_by_name, $description_credit_entry, $acc_cr_label, $acc_dr_label, $obj['j1_driver_ex_price']);

						$return_message ="Paid $".$obj['j1_driver_ex_price']." Extra to ".$driver_details['fname']." ".$driver_details['lname']."<br/>";
					}
				else
					{
						$return_message ="WARNING: No Driver found.<br/>Please check if any driver has accpeted this job or any driver has been paid for this job earlier. Otherwise you cannot make extra payment to the driver using this feature. Thanks";
					}
			}
		elseif($obj['job_decision'] == 'pay_extra_j2') // Pay extra to driver for job 2
			{
				$database = new database;
				$transaction = new Transaction();
				$account = new Account();
				$user = new User();
				
				$entered_by 		= 	$user->getUserDetails($obj['USER_ID']);
				$entered_by_name 	= 	''.$entered_by['fname'].' '.$entered_by['lname'].'';
				//First check from job__driver table if there is any driver paid to for this job
				$query1 = "SELECT MAX(id) as id FROM job__driver where job_id='".$obj['j2_id']."' AND  paid_to!='0'";
				$result1 = $database->query($query1);
				$row1 = mysql_fetch_array($result1);
				if($row1['id'] != '')
					{
						$query2 = "SELECT * from job__driver where id='".$row1['id']."'";
						$result2 = $database->query($query2);
						$row2 = mysql_fetch_array($result2);
						$driver_id = $row2['paid_to'];
					}
				else
					{
						$query3 = "SELECT MAX(id) as id FROM job__driver where job_id='".$obj['j2_id']."' AND  accepted_by!='0'";
						$result3 = $database->query($query3);
						$row3 = mysql_fetch_array($result3);
						if($row3['id'] != '')
							{
								$query4 = "SELECT * from job__driver where id='".$row3['id']."'";
								$result4 = $database->query($query4);
								$row4 = mysql_fetch_array($result4);
								$driver_id = $row4['accepted_by'];
							}
					}
				
				if($driver_id != '')
					{
						$driver_details = $user->getUserDetails($driver_id);
						$job_details = $job->getJobDetails($obj['j2_id']);
						//get the driver fee from driver table and increase it by this new amount
						$new_driver_fee = $job_details['drv_fee'] + $obj['j2_driver_ex_price'];
						$job->updateJob($obj['j2_id'], 'drv_fee', ''.$new_driver_fee.''); // Make the driver fee 0.00
						//get entered by
						$entered_by 		= 	$user->getUserDetails($obj['USER_ID']);
						$entered_by_name 	= 	''.$entered_by['fname'].' '.$entered_by['lname'].'';
						
						$query5 = "INSERT INTO job__driver_extra_pay (job_id, driver_id, notes, amount)
									VALUES
									('".$obj['j2_id']."', '".$driver_id."', '".$obj['j2_driver_ex_notes']."', '".$obj['j2_driver_ex_price']."')";
						$result5 = $database->query($query5);
						//Add job log
						$job->addJobLog($obj['j2_id'], $entered_by_name, 'DRIVER PAID EXTRA', '$'.$obj['j2_driver_ex_price'].' paid to driver for '.$obj['j2_driver_ex_notes'].'', 'NIL');
						$job->addJobLog($obj['j2_id'], $entered_by_name, 'DRIVER TOTAL FEE CHANGED', ''.$new_driver_fee.'', ''.$job_details['drv_fee'].'');
						
						$acc_cr_label 		= 	'drivers__accounts_'.$driver_id.'';
						$acc_dr_label 		= 	'acc__general';

						$description_debit_entry = "Extra Paid for Booking ID # ".$obj['j2_id']." (Acc Credited - ".$acc_cr_label.")";
						$description_credit_entry = "Extra Paid for Booking ID # ".$obj['j2_id']." (Acc Debited - ".$acc_dr_label.")";
						//Add this in transaction table
						$this_trx_id = $transaction->newTransaction($acc_dr_label, $acc_cr_label);
						//Account entry of debit in Allied General Account
						$acc_dr_entry_id = $account->debitEntry($this_trx_id, $entered_by_name, $description_debit_entry, $acc_dr_label,  $acc_cr_label, $obj['j2_driver_ex_price']);
						//Account entry of credit in drivers table	
						$acc_cr_entry_id = $account->creditEntry($this_trx_id, $entered_by_name, $description_credit_entry, $acc_cr_label, $acc_dr_label, $obj['j2_driver_ex_price']);

						$return_message ="Paid $".$obj['j2_driver_ex_price']." Extra to ".$driver_details['fname']." ".$driver_details['lname']."<br/>";
					}
				else
					{
						$return_message ="WARNING: No Driver found.<br/>Please check if any driver has accpeted this job or any driver has been paid for this job earlier. Otherwise you cannot make extra payment to the driver using this feature. Thanks";
					}
			}
		elseif($obj['job_decision'] == 'delete_j1_driver_offer') // Delete offer to driver for j1
			{
				$database = new database;
				$query = "DELETE FROM job__driver where id='".$obj['row_id']."'";
				$result = $database->query($query);
			}
		elseif($obj['job_decision'] == 'delete_j2_driver_offer') // Delete offer to driver for j1
			{
				$database = new database;
				$query = "DELETE FROM job__driver where id='".$obj['row_id']."'";
				$result = $database->query($query);
			}
	}

//$c = array('data' => ''.$return_message.'', 'data1' => ''.$data1.'', 'data2' => ''.$data2.'');
$c = array('data' => ''.$return_message.'<br/>'.$additional_message.'', 'data1' => ''.$data1.'', 'data2' => ''.$data2.'');
$json = json_encode($c); 
echo $json;
?>