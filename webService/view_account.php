<?php
// INCLUDE THE REQUIRED FILES.
require_once('../init.php');
include(''.INCLUDE_PATH.'config.php');
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "chargeAccount.php");
require_once(CLASSES_PATH . "job.php");
require_once(CLASSES_PATH . "user.php");
require_once(INCLUDE_PATH . "functions.php");
$database = new Database;
$charge_account = new chargeAccount();
$user = new user();
// ADD HEADERS FOR THE WEBSERVICE.
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: POST, GET');
header('Content-type: application/json');
header('Access-Control-Allow-Headers:Content-Type');
// INITIALIZE DIFFERENT CLASS OBJECTS.
$database = new Database;
// RECEIVE THE DATA IN JSON FORMAT.
$inputJSON = file_get_contents('php://input');
$obj 		= json_decode($inputJSON,TRUE);

$action_type = $obj['action_type'];
//$json = json_encode($action_type); 

if($action_type=='list_charge_account')
{
	$user_id 	= $obj['user_id'];
	// Fetch all the Charge account 
	$query1 = "SELECT * from charge_acc__contacts where user_id = '".$user_id."'";
				$result1 = $database->query($query1);
				$row1 = mysql_fetch_array($result1);
				$charge_acc_id = $row1['charge_acc_id'];
				$where .= "WHERE 1 ";
				$where .= " AND charge_acc.id = '".$charge_acc_id."' ";
	$query = "SELECT COUNT(*) AS RecordCount FROM charge_acc";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];
				$query = "SELECT COUNT(*) AS RecordCount FROM charge_acc";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];
				$query = "SELECT
							charge_acc.id as id,
							DATE_FORMAT(charge_acc.created_on,'%d/%m/%Y') AS created_on,
							CONCAT(user.title, ' ', user.fname, ' ', user.lname) as created_by,
							charge_acc__payment_type.details as acc_type,
							charge_acc.acc_type as accout_type,
							charge_acc.account_name as account_name,
							charge_acc.myob_no as myob_no
							FROM charge_acc LEFT JOIN user ON charge_acc.created_by = user.id JOIN charge_acc__payment_type
							ON charge_acc.acc_type=charge_acc__payment_type.id ORDER BY charge_acc.account_name";
				$result = $database->query($query);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
	$return_data['list_data'] = $rows;
	$json = json_encode($return_data); 
	echo $json;
}

if($action_type=='list_charge_accounts_contacts')
{
	$account_id 	= $obj['account_id'];
	//Get record count
				$query = "SELECT COUNT(*) AS RecordCount FROM charge_acc__contacts";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];
				$query = "SELECT charge_acc__contacts.id as contact_id,charge_acc__contacts.type_id as type_id,user.id as user_id,user.title as title,
						  user.fname as fname,user.lname as lname,user.password as password,user.email as email,user.mobile as mobile,user.phone as phone,user.preference as preference,
						  user.hidden as hidden FROM charge_acc__contacts LEFT JOIN charge_acc ON charge_acc__contacts.id = charge_acc.id LEFT JOIN user ON charge_acc__contacts.user_id = user.id
							WHERE charge_acc__contacts.charge_acc_id = ".$account_id." ORDER BY charge_acc__contacts.type_id";
							
				$result = $database->query($query);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				$result = $database->query($query);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
	$return_data['list_data'] = $rows;
	$json = json_encode($return_data); 
	echo $json;
}

// Add a new charge account contact 

if($action_type=='create_charge_account_contact')
{
	{
		$data 	= $obj['data'];
		$charge_account_id = $obj['id'];
		$new_contact_id = $charge_account->addChargeAccountContact($data['title'],$data['first_name'], $data['last_name'], $data['password'], $data['email'],$data['mobile'], $data['phone'], $data['preference'], $charge_account_id, $data['type_id']);
		if($new_contact_id != '')
			{
				$row = $user->getUserDetails($new_contact_id);
				$json = array();
				$json['response_code'] = 200;
				$json['response_data'] = $row;
				echo json_encode($json);
			}
		else
			{
				$json = array();
				$json['response_code'] = 100;
				$json['Message'] = "There was a problem adding this contact. Remedy - Please refresh the browser and try creating again.";
			}
	}
}

// Crate a charge account 
if($action_type=='create_charge_accounts')
{
		$data 		= $obj['data'];
		$user_id 	= $obj['user_id'];
		if(checkforDuplicates('charge_acc', 'account_name', ''.$data['account_name'].'')=='0')
			{
				$new_charge_acc_id = $charge_account->addChargeAccount($user_id, $data['acc_type'], $data['account_name'], $data['myob_no']);
				$row = $charge_account->getChargeAccountDetails($new_charge_acc_id);
				$json = array();
				$json['response_code'] = 200;
				$json['Message'] = "New Charge Account Created Successfully";
				echo json_encode($json);
			}
}

if($action_type=='update_charge_accounts_contact')
{
	{
		$data 	= $obj['data'];
		$charge_account_id = $obj['id'];
		$result = $user->updateChargeAccountContact($data['user_id'], 'type_id', $charge_account_id);
		$user->updateUser($data['user_id'], 'fname', $data["fname"]);
		$user->updateUser($data['user_id'], 'title', $data["title"]);
		$user->updateUser($data['user_id'], 'lname', $data["lname"]);
		$user->updateUser($data['user_id'], 'password', $data["password"]);
		$user->updateUser($data['user_id'], 'email', $data["email"]);
		$user->updateUser($data['user_id'], 'mobile', $data["mobile"]);
		$user->updateUser($data['user_id'], 'phone', $data["phone"]);
		$user->updateUser($data['user_id'], 'preference', $data["preference"]);
		$user->updateUser($data['user_id'], 'hidden', $data["hidden"]);
		$json = array();
		$json['response_code'] = 200;
		echo json_encode($json);
	}
}

if($action_type=='update_charge_accounts')
{
	$data 	= $obj['data'];
	$charge_account->updateChargeAccount($data['id'], 'acc_type', $data["acc_type"]);
	$charge_account->updateChargeAccount($data['id'], 'account_name', $data["account_name"]);
	$charge_account->updateChargeAccount($data['id'], 'myob_no', $data["myob_no"]);
	$json = array();
	$json['response_code'] = 200;
	echo json_encode($json);
}
