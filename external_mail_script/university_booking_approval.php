<?php
ini_set("display_errors", 1);
require_once("../init.php");
include(''.INCLUDE_PATH.'config.php');
include(''.INCLUDE_PATH.'settings.php');
require_once(CLASSES_PATH . "mailer.php");
require_once(CLASSES_PATH . "jobReference.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "job.php");
require_once(CLASSES_PATH . "chargeAccount.php");

$database = new database;
$job = new job();
$job_reference = new JobReference();
$mailer = new Mailer();
$user = new User();
$chargeaccount = new chargeAccount();

$job_reference_id = $_GET['job_reference_id'];
$confirm_status = $_GET['confirm_status'];
$admin_id = base64_decode(base64_decode($_GET['admin_id']));
$admin_details = $user->getUserDetails($admin_id);

$job_reference_details = $job_reference->getJobReferenceDetails($job_reference_id);

$j1_details = $job->getJobDetails($job_reference_details['j1_id']);
if($job_reference_details['job_type'] == '2')
	{
		$j2_details = $job->getJobDetails($job_reference_details['j2_id']);
	}
	
if($confirm_status == '1') //confirmed
	{
		if($j1_details['job_status'] =='5') // Received and waiting for approval
			{
				
				$job->updateJob($j1_details['id'], 'job_status', '10'); //change status of the job to update
				$job->addJobLog($j1_details['id'], $admin_id, 'Job Approved by '.$admin_details['title'].' '.$admin_details['fname'].' '.$admin_details['lname'].'', '10', '5'); // add job log
				
				
				//$job_reference->updateJobReferenceTable($job_reference_id, 'bkg_by_id', $admin_id);
				//Add in Job Reference Log Table
				$job_reference->addJobReferenceLog($job_reference_id, $admin_id, 'job approved and sent to allied cars', $j1_details['id'], 'NIL');
				
				//send email to student and person who is booking on behalf of student
				$charge_acc_details = $chargeaccount->getChargeAccountDetails($job_reference_details['charge_acc_id']);

				$mailer->sendUniversityMail($job_reference_id, '5', '0');	
				
				echo "<h1>Booking ID - ".$j1_details['id']."";
				
				echo " has been approved by corresponding charge accounts</h1><br/>
				Booking request sent to Alliedcars.<br/>";
			}
		else
			{
				echo "<h1>Booking ID - ".$j1_details['id']."";
				
				echo " has already been approved by corresponding charge accounts.</h1>
				One of the charge account personnel has already confirmed this job.<br/>
				<a href='".ROOT_ADDRESS."external_mail_script/member_login.php?email=".$admin_details['email']."&password=".$admin_details['password']."'>Login to the system</a>";
			} 
	}
if($confirm_status == '2') //rejected
	{
		if($j1_details['job_status'] =='5') // Received and waiting for confirmation
			{
				$admin_details = $user->getUserDetails($admin_id);
				
				$job->updateJob($j1_details['id'], 'job_status', '90'); //change status of the job to update
				$job->addJobLog($j1_details['id'], $admin_id, 'Job Rejected by '.$admin_details['title'].' '.$admin_details['fname'].' '.$admin_details['lname'].'', '90', '5'); // add job log
				
				////Informing Student about the rejection of booking by university personnel.
				//$mailer->sendMailToClientOnBooking($job_reference_id, '4', $job_reference_details['std_email'], $job_reference_details['std_title'], $job_reference_details['std_fname'], $job_reference_details['std_lname']);
				$mailer->sendUniversityMail($job_reference_id, '8', '0');	

				
			}
		else if($job_details['job_status'] =='10') // Received and waiting for confirmation
			{
				echo "<h1>This job has already been CONFIRMED by other personnel of your charge account. You cannot cancel it any more.<br/></h1>
						To Cancel this job please log in to the system. Please be mindful that a confirmation of booking has 
						already been sent by the system to the customer. So if you are cancelling this booking then please
						call the customer to let them know personally.<br/>
				<a href='".ROOT_ADDRESS."external_mail_script/member_login.php?email=".$admin_details['email']."&password=".$admin_details['password']."'>Login to the system</a>";
			}
		else
			{
				echo "<h1>This job has already been DECLINED by other personnel of your charge account</h1><br/>
				To decline this job now and change the status of the job to cancelled, you will have to login to the system. Please
				keep in mind that an email has been sent to the customer earlier confirming the job. So you may also want to communicate
				with the customer separately to make your cancellation decision known.<br/>
				<a href='".ROOT_ADDRESS."external_mail_script/member_login.php?email=".$admin_details['email']."&password=".$admin_details['password']."'>Login to the system</a>";
			}
		
	}
?>