<?php
require_once("../init.php");
include(''.INCLUDE_PATH.'config.php');
include(''.INCLUDE_PATH.'settings.php');
require_once(CLASSES_PATH . "login.php");
require_once(CLASSES_PATH . "database.php");
$login = new Login();
$email 		= $_GET['email'];
$password	= $_GET['password'];
if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARTDED_FOR'] != '') 
	{
		$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} 
else 
	{
		$ip_address = $_SERVER['REMOTE_ADDR'];
	}
$login = new Login();
$login_result = $login->check_login($email, $password, 1, $ip_address);
if($login_result['result'] == '1')
	{
		header('Location: ../Admin/index.php');
	}
else
	{
		header('Location: ../index.php');
	}