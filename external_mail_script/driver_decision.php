<?php
ini_set("display_errors", 1);
require_once("../init.php");
require_once(CLASSES_PATH . "mailer.php");
require_once(CLASSES_PATH . "account.php"); 
require_once(CLASSES_PATH . "transaction.php");
require_once(CLASSES_PATH . "job.php");
require_once(CLASSES_PATH . "jobReference.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "vehicle.php");
require_once(INCLUDE_PATH . "functions_date_time.php");

$database = new database;
$mailer = new Mailer();
$account = new Account();
$job = new Job();
$job_reference = new jobReference();
$user = new User();


if($_GET['accept_job'] == '1')
	{
		$job_id				=	base64_decode(base64_decode($_GET['job_id']));
		$driver_id			=	base64_decode(base64_decode($_GET['driver_id']));
		$offer_amount		=	base64_decode(base64_decode($_GET['offer_amount']));
		$query = "Select * from job__driver where job_id='".$job_id."' and offered_to='".$driver_id."'";
		$result = $database->query($query);
		$row_num = mysql_num_rows($result);
		$query6="SELECT job__reference.bkg_by_id as booking_made_by, job__reference.pax_id as paxId FROM job__reference INNER JOIN job ON job__reference.id=job.job_reference_id WHERE job.id='".$job_id."'";
		$result6 = mysql_fetch_array($database->query($query6));
		if($row_num >= 1)
			{
				$job_details 		= 	$job->getJobDetails($job_id);
				$job_ref_details 	= 	$job_reference->getJobReferenceDetails($job_details['job_reference_id']);
				$driver_details 	= 	$user->getUserDetails($driver_id);
				$driver_notes		=	$job_details['driver_notes'];
				if($job_details['driver_status'] == '1' || $job_details['driver_status'] == '2') //Allocated or Sent
					{
						$job->updateJob($job_id, 'driver_status', '3'); // change status of the job to Accepted
						$job->updateJob($job_id, 'driver_id', $driver_id); // Put this driver in job table
						$job->updateJob($job_id, 'driver_price', $offer_amount); // Put the driver price in the job table
						$job->addNewDriverToJob($job_id, '', '', '', '', $driver_id, $offer_amount, '', '', '', '', '');
						$job->addJobDriverLog($job_id, ''.$driver_details['fname'].' '.$driver_details['lname'].'', 'Job Accepted', 'Accepted by '.$driver_details['fname'].' '.$driver_details['lname'].' for '.$offer_amount.'', '');
						if($job_details['driver_status'] == '1')
							{ $old_status = "Allocated";}
						else
						{ 
							$old_status = "Sent";
							$job->addJobLog($job_id, $driver_id, 'Job Accepted by '.$driver_details['fname'].' '.$driver_details['lname'].'', 'Accepted', $old_status);
							$mailer->sendJobDetailsToDriver($job_id, $driver_id, $offer_amount,$driver_notes);
							echo'<h1>Thank you for Accepting the job</h1><br/><p>An email has been sent to you with the details of the job. Please check your email</b>';
							$customer_message="Your job(JOB ID- ".$job_id.") accepted by driver name ".$driver_details['fname']." ".$driver_details['lname']." Phone No - '".$driver_details['mobile']."' / '".$driver_details['phone']."'";
							$push_title = 'New job '.$job_id;
							if($result6['booking_made_by']==$result6['paxId'])
							{
								$job->commonGooglePushNotification($result6['booking_made_by'],$customer_message,$push_title);
							}
							else
							{
								$job->commonGooglePushNotification($result6['booking_made_by'],$customer_message,$push_title);
								$job->commonGooglePushNotification($result6['paxId'],$customer_message,$push_title);
							}
						}
					}
				if($job_details['driver_status'] == '3' || $job_details['driver_status'] == '4') //Accepted OR PAID
					{
						$job_details 		= 	$job->getJobDetails($job_id);
						
						echo'<h1><font color="red">SORRY, ANOTHER DRIVER HAS TAKEN THIS BOOKING.</font></h1>
							From : '.$job_details['frm_sub'].'<br/>
							To : '.$job_details['to_sub'].'<br/>
							Date : '.formatDate($job_details['job_date'],1).'<br/>
							Time : '.calculateTimeInAmPmFormatWithoutSeconds($job_details['job_time']).'<br/>
							';
					}
			}
		else
			{
				echo'<h1><font color="red">Sorry, this job is not on offer anymore. Thank you.</font></h1>';
			}
	}
if($_GET['rogerd'] == '1')
	{
		if($_GET['collective'] == '1')
		{
				$jobs				=	base64_decode(base64_decode($_GET['job_id']));
				$driver_id			=	base64_decode(base64_decode($_GET['driver_id']));
				$driver_details 	= 	$user->getUserDetails($driver_id);
				
				$job_ids = explode(',' , $jobs);
				
				foreach($job_ids as $job_id)
				{
					$job_details 		= 	$job->getJobDetails($job_id);
					$query6="SELECT job__reference.bkg_by_id as booking_made_by, job__reference.pax_id as paxId FROM job__reference INNER JOIN job ON job__reference.id=job.job_reference_id WHERE job.id='".$job_id."'";
					$result6 = mysql_fetch_array($database->query($query6));		
					if( $job_details['driver_id'] == $driver_id )
					{
						if($job_details['driver_status'] == '1')
							{ $old_status = "Allocated";}
						elseif($job_details['driver_status'] == '2')
							{ $old_status = "Sent";}
						elseif($job_details['driver_status'] == '3')
							{ $old_status = "Accepted";}
							
						$job->updateJob($job_id, 'driver_status', '4'); // Job Rogered
						$job->updateJob($job_id, 'driver_id', $driver_id); // Put this driver in job table
						$job->updateJob($job_id, 'driver_price', $offer_amount); // Put the driver price in the job table
						$job->addJobLog($job_id, $driver_id, 'Job Rogered '.$driver_details['fname'].' '.$driver_details['lname'].'', 'ROGERED', $old_status);
						echo'<h1><font>Thank you the job '.$job_id.' has been rogered</font></h1><br>';
						$customer_message="Your job(JOB ID- ".$job_id.") has been rogered by driver name ".$driver_details['fname']." ".$driver_details['lname']." Phone No - '".$driver_details['mobile']."' / '".$driver_details['phone']."'";
						$push_title = 'New job '.$job_id;
						if($result6['booking_made_by']==$result6['paxId'])
						{
							$job->commonGooglePushNotification($result6['booking_made_by'],$customer_message,$push_title);
						}
						else
						{
							$job->commonGooglePushNotification($result6['booking_made_by'],$customer_message,$push_title);
							$job->commonGooglePushNotification($result6['paxId'],$customer_message,$push_title);
						}
					}
					else
					{
						echo'<h1>Sorry this job '.$job_id.' has been allocated to somebody else</h1>';
					}
				}
		}
		else{
				$job_id				=	base64_decode(base64_decode($_GET['job_id']));
				$job_details 		= 	$job->getJobDetails($job_id);
				$driver_id			=	base64_decode(base64_decode($_GET['driver_id']));
				$driver_details 	= 	$user->getUserDetails($driver_id);
		
				if( $job_details['driver_id'] == $driver_id )
				{
					if($job_details['driver_status'] == '1')
						{ $old_status = "Allocated";}
					elseif($job_details['driver_status'] == '2')
						{ $old_status = "Sent";}
					elseif($job_details['driver_status'] == '3')
						{ $old_status = "Accepted";}
						
					$job->updateJob($job_id, 'driver_status', '4'); // Job Rogered
					$job->updateJob($job_id, 'driver_id', $driver_id); // Put this driver in job table
					$job->updateJob($job_id, 'driver_price', $offer_amount); // Put the driver price in the job table
					$job->addJobLog($job_id, $driver_id, 'Job Rogered '.$driver_details['fname'].' '.$driver_details['lname'].'', 'ROGERED', $old_status);
					echo'<h1><font>Thank you the job has been rogered</font></h1>';
				}
				else
				{
					echo"<h1><font color='red'>SORRY THIS JOB HAS BEEN ALLOCATED TO SOMEBODY ELSE.<BR><BR>  DO NOT DO THIS JOB AS YOU WILL NOT BE PAID</font></h1>";
				}
		}
	}	
?>