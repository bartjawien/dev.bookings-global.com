<?php
require_once("../init.php");
include(''.INCLUDE_PATH.'config.php');
include(''.INCLUDE_PATH.'settings.php');
require_once(CLASSES_PATH . "mailer.php");
require_once(CLASSES_PATH . "jobReference.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "job.php");
require_once(CLASSES_PATH . "chargeAccount.php");
$database = new database;
$job = new job();
$job_reference = new JobReference();
$mailer = new Mailer();
$user = new User();
$chargeaccounts = new chargeAccount();

$job_reference_id = $_GET['job_reference_id'];
$confirm_status = $_GET['confirm_status'];
$admin_id = base64_decode(base64_decode($_GET['admin_id']));
$admin_details = $user->getUserDetails($admin_id);

$job_reference_details = $job_reference->getJobReferenceDetails($job_reference_id);
$chargeaccount_details = $chargeaccounts->getChargeAccountDetails($job_reference_details['charge_acc_id']);

$j1_details = $job->getJobDetails($job_reference_details['j1_id']);
if($job_reference_details['job_type'] == '2')
	{
		$j2_details = $job->getJobDetails($job_reference_details['j2_id']);
	}
if($confirm_status == '1') //confirmed
	{
		if($j1_details['job_status'] =='10') // Received and waiting for confirmation
			{
				
				$job->updateJob($j1_details['id'], 'job_status', '20'); //change status of the job to update
				$job->addJobLog($j1_details['id'], $admin_id, 'Job Confirmed by '.$admin_details['title'].' '.$admin_details['fname'].' '.$admin_details['lname'].'', '20', '10'); // add job log
				if($job_reference_details['job_type'] == '2')
					{
						$job->updateJob($j2_details['id'], 'job_status', '20'); //change status of the job to update
						$job->addJobLog($j2_details['id'], $admin_id, 'Job Confirmed by '.$admin_details['title'].' '.$admin_details['fname'].' '.$admin_details['lname'].'', '20', '10'); // add job log
					}
				
				
				
				if($job_reference_details['acc_type'] == '3') //if this is booked by university or student
				{
					$mailer->sendUniversityMail($job_reference_id, '6');
				}
				else
				{
					$user_details = $user->getUserDetails($job_reference_details['bkg_by_id']);
					$mailer->sendMailToClientOnBooking($job_reference_id, '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
					$user_details = $user->getUserDetails($job_reference_details['pax_id']);
					$mailer->sendMailToClientOnBooking($job_reference_id, '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
				}
				echo "<h1>Booking ID - ".$j1_details['id']."";
				if($job_reference_details['job_type'] == '2')
					{
						echo  "and ".$j2_details['id']."";
					}
				echo " has been confirmed</h1><br/>
				A confirmation email has been sent to the customer with the details of the booking.<br/>";
			}
		else
			{
				echo "<h1>Booking ID - ".$j1_details['id']."";
				if($job_reference_details['job_type'] == '2')
					{
						echo "and ".$j2_details['id']."";
					}
				echo " has already been confirmed.</h1>
				One of the admins have already confirmed this job.<br/>
				<a href='".ROOT_ADDRESS."external_mail_script/member_login.php?email=".$admin_details['email']."&password=".$admin_details['password']."'>Login to the system</a>";
			}
	}
if($confirm_status == '2') //rejected
	{
		if($j1_details['job_status'] =='10') // Received and waiting for confirmation
			{
				$admin_details = $user->getUserDetails($admin_id); 
				
				$job->updateJob($j1_details['id'], 'job_status', '90'); //change status of the job to update
				$job->addJobLog($j1_details['id'], $admin_id, 'Job Confirmed by '.$admin_details['title'].' '.$admin_details['fname'].' '.$admin_details['lname'].'', '20', '10'); // add job log
				if($job_reference_details['job_type'] == '2')
					{
						$job->updateJob($j1_details['id'], 'job_status', '90'); //change status of the job to update
						$job->addJobLog($j1_details['id'], $admin_id, 'Job Confirmed by '.$admin_details['title'].' '.$admin_details['fname'].' '.$admin_details['lname'].'', '20', '10'); // add job log
					}

				if($job_reference_details['acc_type'] == '3') //if this is booked be university or student
				{
					$mailer->sendUniversityMail($job_reference_id, '8');
				}
				else
				{
					$user_details = $user->getUserDetails($job_reference_details['bkg_by_id']);
					$mailer->sendMailToClientOnBooking($job_reference_id, '3', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
					$user_details = $user->getUserDetails($job_reference_details['pax_id']);
					$mailer->sendMailToClientOnBooking($job_reference_id, '3', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
				}
			}
		else if($job_details['job_status'] =='20') // Received and waiting for confirmation
			{
				echo "<h1>This job has already been CONFIRMED by other admins. You cannot cancel it any more.<br/></h1>
						To Cancel this job please log in to the system. Please be mindful that a confirmation of booking has 
						already been sent by the system to the customer. So if you are cancelling this booking then please
						call the customer to let them know personally.<br/>
				<a href='".ROOT_ADDRESS."external_mail_script/member_login.php?email=".$admin_details['email']."&password=".$admin_details['password']."'>Login to the system</a>";
			}
		else
			{
				echo "<h1>This job has already been DECLINED by other admins</h1><br/>
				To decline this job now and change the status of the job to cancelled, you will have to login to the system. Please
				keep in mind that an email has been sent to the customer earlier confirming the job. So you may also want to communicate
				with the customer separately to make your cancellation decision known.<br/>
				<a href='".ROOT_ADDRESS."external_mail_script/member_login.php?email=".$admin_details['email']."&password=".$admin_details['password']."'>Login to the system</a>";
			}
		
	}
?>