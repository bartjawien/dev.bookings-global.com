<?php
require_once(CLASSES_PATH . "database.php");
class JobReference
	{
/*---------------------------------------------------------------------------------------------------------------------*/
		public function addNewJobReference($entered_by, $job_src, $order_ref, $acc_type, $std_id, $std_title, $std_fname, $std_lname, $std_ph, $std_email, $charge_acc_id, $bkg_by_id, $pax_id, $job_type, $charge_mode, $under_18='0', $child_age_below_7='', $on_behalf='0',$conf_to_client=1,$conf_to_pax=1) // These are the common values for bookings 
			{
				$std_fname = htmlspecialchars($std_fname);
				$std_lname = htmlspecialchars($std_lname);
				 $q = "INSERT INTO job__reference(id, created_on, entered_by, job_src, order_ref, acc_type, std_id, std_title, std_fname, std_lname, std_ph, std_email, charge_acc_id, bkg_by_id, pax_id, job_type, charge_mode, under_18, child_age_below_7, on_behalf, conf_to_client, conf_to_pax)
						VALUES(NULL, CURRENT_TIMESTAMP, '$entered_by', '$job_src', '$order_ref', '$acc_type', '$std_id', '$std_title', '$std_fname', '$std_lname', '$std_ph', '$std_email', '$charge_acc_id', '$bkg_by_id', '$pax_id', '$job_type', '$charge_mode', '$under_18', '$child_age_below_7' , '$on_behalf', '$conf_to_client', '$conf_to_pax' )";
				$database = new database;
				$result = $database->query($q);
				$id = mysql_insert_id();
				return $id;
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function getJobReferenceDetails($job_reference_id)
			{
				$q = "SELECT * from job__reference where id='$job_reference_id'";
				$database = new database;
				$result = $database->query($q);
				$x = mysql_fetch_assoc($result);
				$x['std_fname'] =htmlspecialchars_decode($x['std_fname']);
				$x['std_lname'] =htmlspecialchars_decode($x['std_lname']);
				return $x;
			}	
/*---------------------------------------------------------------------------------------------------------------------*/
		public function addJobReferenceLog($job_reference_id, $entered_by, $message, $new_value, $old_value)
			{
				$database = new database;
				$q = "INSERT INTO log__job_reference (job_reference_id, entered_by, message, new_value, old_value)
						VALUES ('$job_reference_id', '$entered_by', '$message', '$new_value', '$old_value')";
				$result = $database->query($q);
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function getJobLog($job_reference_id)
			{
				$q = "SELECT * from log__job_reference where job_reference_id = '".$job_reference_id."' order by created_on DESC"; 
				$database = new database;
				$result = $database->query($q);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				return $rows;
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function updateJobReferenceTable($job_reference_id, $update_column, $new_value)
			{
				$database = new database;
				if($update_column == 'std_fname')
				{
					$new_value = htmlspecialchars($new_value);
				}
				if($update_column == 'std_lname')
				{
					$new_value = htmlspecialchars($new_value);
				}
				$q = "UPDATE job__reference SET ".$update_column."= '".$new_value."' WHERE id = '".$job_reference_id."'";
				$result = $database->query($q);
			}
/*---------------------------------------------------------------------------------------------------------------------*/
	}	
?>