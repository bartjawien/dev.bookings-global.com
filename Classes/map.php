<?php

class map
	{
		function calculate_distance($from_address, $to_address)
			{
				
				$from 	= 	$from_address;
				$to 	= 	$to_address;

				$from 	= 	urlencode($from);
				$to 	= 	urlencode($to);

				$data = file_get_contents("http://maps.googleapis.com/maps/api/distancematrix/json?origins=".$from."&destinations=".$to."&language=en-EN&sensor=false");
				$data = json_decode($data);

				$time = 0;
				$distance = 0;

				foreach($data->rows[0]->elements as $road) 
					{
						$time += $road->duration->value;
						$distance += $road->distance->value;
					}
				if($distance <= 0 || $distance =='')
					{
						$return_array = array(
								'distance' 	=> 0,
								'time_hrs' 	=> 0,
								'time_min' 	=> 0,
								'time_sec' 	=> 0,
								'from' 		=> ''.$from.'',
								'to' 		=> ''.$to.''
							);
					}
				else
					{
						$distance 	= number_format($distance/1000,2);
						$calc_time	= $time;
						$hours 		= floor($calc_time / 3600);
						$minutes 	= floor(($calc_time / 60) % 60);
						$seconds 	= $calc_time % 60;
						$return_array = array(
										'distance' 	=> ''.$distance.'',
										'time_hrs' 	=> ''.$hours.'',
										'time_min' 	=> ''.$minutes.'',
										'time_sec' 	=> ''.$seconds.'',
										'from' 		=> ''.$from.'',
										'to' 		=> ''.$to.''
									);
					}
				
				return $return_array;
			}
	}
?>