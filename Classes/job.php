<?php
require_once(CLASSES_PATH . "database.php");
//require_once(CLASSES_PATH . "vehicle.php");
class Job
	{
/*---------------------------------------------------------------------------------------------------------------------*/
		public function addNewJob($job_reference_id, $job_status, $job_date, $job_time, $frm_flight_no, $frm_line1, $frm_line2, $frm_sub, $frm_pc, $frm_state, $to_flight_no, $to_line1, $to_line2, $to_sub, $to_pc, $to_state, $frm_via_line1, $frm_via_line2, $frm_via_sub, $frm_via_pc, $frm_via_state, $to_via_line1, $to_via_line2, $to_via_sub, $to_via_state, $to_via_pc, $car_id, $pax_nos, $luggage, $baby_seat, $booster_seat, $baby_capsule, $driver_status, $kms, $fare, $inter, $ed, $wait, $tolls, $bs, $park, $ah, $me, $alc, $fc, $oth, $tot_fare, $drv_fee, $oth_exp, $profit, $ext_notes, $int_notes,$driver_notes='',$lat,$lng) // These are the common values for bookings
			{
				$q = "INSERT INTO job(job_reference_id, job_status, job_date, job_time, frm_flight_no, frm_line1, frm_line2, frm_sub, frm_pc, frm_state, to_flight_no, to_line1, to_line2, to_sub, to_pc, to_state, frm_via_line1, frm_via_line2, frm_via_sub, frm_via_pc, frm_via_state, to_via_line1, to_via_line2, to_via_sub, to_via_pc, to_via_state, car_id, pax_nos, luggage, baby_seat, booster_seat, baby_capsule, driver_status, kms, fare, inter, ed, wait, tolls, bs, park, ah, me, alc, fc, oth, tot_fare, drv_fee, oth_exp, profit, ext_notes, int_notes,driver_notes,pick_up_lat,pick_up_lng)
						VALUES('$job_reference_id', '$job_status', '$job_date', '$job_time', '$frm_flight_no', '$frm_line1', '$frm_line2', '$frm_sub', '$frm_pc', '$frm_state', '$to_flight_no', '$to_line1', '$to_line2', '$to_sub', '$to_pc', '$to_state', '$frm_via_line1', '$frm_via_line2', '$frm_via_sub', '$frm_via_pc', '$frm_via_state', '$to_via_line1', '$to_via_line2', '$to_via_sub', '$to_via_pc', '$to_via_state','$car_id', '$pax_nos', '$luggage', '$baby_seat', '$booster_seat', '$baby_capsule', '$driver_status', '$kms', '$fare', '$inter', '$ed', '$wait', '$tolls', '$bs', '$park', '$ah', '$me', '$alc', '$fc', '$oth', '$tot_fare', '$drv_fee', '$oth_exp', '$profit', '$ext_notes', '$int_notes', '$driver_notes','$lat','$lng')";
				$database = new database;
				$result = $database->query($q);
				$id = mysql_insert_id();
				return $id;
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function addNewDriverToJob($job_id, $allocated_to, $allocated_amount, $offered_to, $offered_amount, $accepted_by, $accepted_amount, $paid_to, $paid_amount, $ex_paid_to, $ex_notes, $ex_amount, $driver_notes='') // These are the common values for bookings
			{
				$q = "INSERT INTO job__driver (job_id, allocated_to, allocated_amount, offered_to, offered_amount, accepted_by, accepted_amount, paid_to, paid_amount, ex_paid_to, ex_notes, ex_amount, driver_notes)
						VALUES('$job_id', '$allocated_to', '$allocated_amount', '$offered_to', '$offered_amount', '$accepted_by', '$accepted_amount', '$paid_to', '$paid_amount', '$ex_paid_to', '$ex_notes', '$ex_amount', '$driver_notes')";
				$database = new database;
				$result = $database->query($q);
				$id = mysql_insert_id();
				return $id;
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function addJobLog($job_id, $entered_by, $message, $new_value, $old_value)
			{
				$database = new database;
				$q = "INSERT INTO log__job (job_id, entered_by, message, new_value, old_value)
						VALUES ('$job_id', '$entered_by', '$message', '$new_value', '$old_value')";
				$result = $database->query($q);
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function addJobDriverLog($job_id, $entered_by, $message, $new_value, $old_value)
			{
				$database = new database;
				$q = "INSERT INTO log__job_driver (job_id, entered_by, message, new_value, old_value)
						VALUES ('$job_id', '$entered_by', '$message', '$new_value', '$old_value')";
				$result = $database->query($q);
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function getJobDetails($job_id)
			{
				$q = "SELECT * from job where id='$job_id'";
				$database = new database;
				$result = $database->query($q);
				$x = mysql_fetch_assoc($result);
				return $x;
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function getJobDriverDetails($job_driver_id)
			{
				$q = "SELECT * from job__driver where id='$job_driver_id'";
				$database = new database;
				$result = $database->query($q);
				$x = mysql_fetch_assoc($result);
				return $x;
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function getJobLog($job_id)
			{
				$q = "SELECT * from log__job where job_id = '".$job_id."' order by created_on DESC"; 
				$database = new database;
				$result = $database->query($q);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				return $rows;
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function getJobDriverLog($job_id)
			{
				$q = "SELECT * from log__job_driver where job_id = '".$job_id."' order by created_on DESC"; 
				$database = new database;
				$result = $database->query($q);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				return $rows;
			}
			
/*---------------------------------------------------------------------------------------------------------------------*/
		public function getJobStatusDetails($id)
			{
				$q = "SELECT * from job__status_ids where id='$id'";
				$database = new database;
				$result = $database->query($q);
				$x = mysql_fetch_assoc($result);
				return $x;
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function getDriverStatusDetails($id)
			{
				$q = "SELECT * from job__driver_status_ids where id='$id'";
				$database = new database;
				$result = $database->query($q);
				$x = mysql_fetch_assoc($result);
				return $x;
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function updateJob($job_id, $update_column, $new_value)
			{
				$database = new database;
				$q = "UPDATE job SET ".$update_column."= '".mysql_real_escape_string($new_value)."' WHERE id = '".$job_id."'";
				$result = $database->query($q);
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function updateJobDriver($id, $update_column, $new_value)
			{
				$database = new database;
				$q = "UPDATE job__driver SET ".$update_column."= '".mysql_real_escape_string($new_value)."' WHERE id = '".$id."'";
				$result = $database->query($q);
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function cloneIndividualJob($job_id) // These are the common values for bookings
			{
				$job_reference = new jobReference();
				$job_details = $this->getJobDetails($job_id);
				$job_ref_details = $job_reference->getJobReferenceDetails($job_details['job_reference_id']);
				
				$new_job_reference_id = $job_reference->addNewJobReference($_SESSION['USER_ID'], $job_ref_details['job_src'], $job_ref_details['order_ref'], $job_ref_details['acc_type'], $job_ref_details['std_id'], $job_ref_details['std_title'], $job_ref_details['std_fname'],$job_ref_details['std_lname'], '', '',$job_ref_details['charge_acc_id'], $job_ref_details['bkg_by_id'], $job_ref_details['pax_id'], '1', $job_ref_details['charge_mode']);
				$job_reference->addJobReferenceLog($new_job_reference_id, $_SESSION['USER_ID'], 'New Job Reference Created', $new_job_reference_id, 'NIL');

				$q = "INSERT INTO job(job_reference_id, job_status, job_date, job_time, frm_flight_no, frm_line1, frm_line2, frm_sub, frm_pc, frm_state, to_flight_no, to_line1, to_line2, to_sub, to_pc, to_state, frm_via_line1, frm_via_line2, frm_via_sub, frm_via_pc, frm_via_state, to_via_line1, to_via_line2, to_via_sub, to_via_pc, to_via_state, car_id, pax_nos, luggage, baby_seat, booster_seat, baby_capsule, driver_status, kms, fare, inter, ed, wait, tolls, bs, park, ah, me, alc, fc, oth, tot_fare, drv_fee, oth_exp, profit, ext_notes, int_notes)
						VALUES('$new_job_reference_id', '10', '".$job_details['job_date']."', '".$job_details['job_time']."', '".$job_details['frm_flight_no']."', '".mysql_real_escape_string($job_details['frm_line1'])."', '".mysql_real_escape_string($job_details['frm_line2'])."', '".$job_details['frm_sub']."', '".$job_details['frm_pc']."', '".$job_details['frm_state']."', '".$job_details['to_flight_no']."', '".mysql_real_escape_string($job_details['to_line1'])."', '".mysql_real_escape_string($job_details['to_line2'])."', '".$job_details['to_sub']."', '".$job_details['to_pc']."', '".$job_details['to_state']."', '".mysql_real_escape_string($job_details['frm_via_line1'])."', '".mysql_real_escape_string($job_details['frm_via_line2'])."', '".$job_details['frm_via_sub']."', '".$job_details['frm_via_pc']."', '".$job_details['frm_via_state']."', '".mysql_real_escape_string($job_details['to_via_line1'])."', '".mysql_real_escape_string($job_details['to_via_line2'])."', '".$job_details['to_via_sub']."', '".$job_details['to_via_pc']."', '".$job_details['to_via_state']."','".$job_details['car_id']."', '".$job_details['pax_nos']."', '".$job_details['luggage']."', '".$job_details['baby_seat']."', '".$job_details['booster_seat']."', '".$job_details['baby_capsule']."', '', '".$job_details['kms']."', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '".mysql_real_escape_string($job_details['ext_notes'])."', '".mysql_real_escape_string($job_details['int_notes'])."')";
				$database = new database;
				$result = $database->query($q);
				$id = mysql_insert_id();
				$this->addJobLog($id, $_SESSION['USER_ID'], 'New job created', $id, 'NIL');
				
				$job_reference->updateJobReferenceTable($new_job_reference_id, 'j1_id', $id);
				$job_reference->addJobReferenceLog($new_job_reference_id, $_SESSION['USER_ID'], 'Job 1 Added', $id, 'NIL');
				return $id;
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function CloneReturnJob($j1_id, $j2_id) // These are the common values for bookings
			{ 
				$job_reference = new jobReference(); 
				$j1_details = $this->getJobDetails($j1_id);
				$j2_details = $this->getJobDetails($j2_id);
				
				$job_ref_details = $job_reference->getJobReferenceDetails($j1_details['job_reference_id']);
																								
				$new_job_reference_id = $job_reference->addNewJobReference($_SESSION['USER_ID'], $job_ref_details['job_src'], $job_ref_details['order_ref'], $job_ref_details['acc_type'], $job_ref_details['std_id'], $job_ref_details['title'], $job_ref_details['std_fname'], $job_ref_details['std_lname'],' ',' ',$job_ref_details['charge_acc_id'], $job_ref_details['bkg_by_id'], $job_ref_details['pax_id'], '2', $job_ref_details['charge_mode']);
				$job_reference->addJobReferenceLog($new_job_reference_id, $_SESSION['USER_ID'], 'New Job Reference Created', $new_job_reference_id, 'NIL');
				
				$q = "INSERT INTO job(job_reference_id, job_status, job_date, job_time, frm_flight_no, frm_line1, frm_line2, frm_sub, frm_pc, frm_state, to_flight_no, to_line1, to_line2, to_sub, to_pc, to_state, frm_via_line1, frm_via_line2, frm_via_sub, frm_via_pc, frm_via_state, to_via_line1, to_via_line2, to_via_sub, to_via_pc, to_via_state, car_id, pax_nos, luggage, baby_seat, booster_seat, baby_capsule, driver_status, kms, fare, inter, ed, wait, tolls, bs, park, ah, me, alc, fc, oth, tot_fare, drv_fee, oth_exp, profit, ext_notes, int_notes)
						VALUES('$new_job_reference_id', '10', '".$j1_details['job_date']."', '".$j1_details['job_time']."', '".$j1_details['frm_flight_no']."', '".mysql_real_escape_string($j1_details['frm_line1'])."', '".mysql_real_escape_string($j1_details['frm_line2'])."', '".$j1_details['frm_sub']."', '".$j1_details['frm_pc']."', '".$j1_details['frm_state']."', '".$j1_details['to_flight_no']."', '".mysql_real_escape_string($j1_details['to_line1'])."', '".mysql_real_escape_string($j1_details['to_line2'])."', '".$j1_details['to_sub']."', '".$j1_details['to_pc']."', '".$j1_details['to_state']."', '".$j1_details['frm_via_line1']."', '".$j1_details['frm_via_line2']."', '".$j1_details['frm_via_sub']."', '".$j1_details['frm_via_pc']."', '".$j1_details['frm_via_state']."', '".$j1_details['to_via_line1']."', '".$j1_details['to_via_line2']."', '".$j1_details['to_via_sub']."', '".$j1_details['to_via_pc']."', '".$j1_details['to_via_state']."', '".$j1_details['car_id']."', '".$j1_details['pax_nos']."', '".$j1_details['luggage']."', '".$j1_details['baby_seat']."', '".$j1_details['booster_seat']."', '".$j1_details['baby_capsule']."', '', '".$j1_details['kms']."', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '0.0', '".mysql_real_escape_string($j1_details['ext_notes'])."', '".mysql_real_escape_string($j1_details['int_notes'])."')";
				$database = new database;
				$result = $database->query($q);
				$new_j1_id = mysql_insert_id();
				$this->addJobLog($new_j1_id, $_SESSION['USER_ID'], 'New job created', $new_j1_id, 'NIL');
				
				$q = "INSERT INTO job(job_reference_id, job_status, job_date, job_time, frm_flight_no, frm_line1, frm_line2, frm_sub, frm_pc, frm_state, to_flight_no, to_line1, to_line2, to_sub, to_pc, to_state, frm_via_line1, frm_via_line2, frm_via_sub, frm_via_pc, frm_via_state, to_via_line1, to_via_line2, to_via_sub, to_via_pc, to_via_state, car_id, pax_nos, luggage, baby_seat, booster_seat, baby_capsule, driver_status, kms, fare, inter, ed, wait, tolls, bs, park, ah, me, alc, fc, oth, tot_fare, drv_fee, oth_exp, profit, ext_notes, int_notes)
						VALUES('$new_job_reference_id', '10', '".$j2_details['job_date']."', '".$j2_details['job_time']."', '".$j2_details['frm_flight_no']."', '".mysql_real_escape_string($j2_details['frm_line1'])."', '".mysql_real_escape_string($j2_details['frm_line2'])."', '".$j2_details['frm_sub']."', '".$j2_details['frm_pc']."', '".$j2_details['frm_state']."', '".$j2_details['to_flight_no']."', '".mysql_real_escape_string($j2_details['to_line1'])."', '".mysql_real_escape_string($j2_details['to_line2'])."', '".$j2_details['to_sub']."', '".$j2_details['to_pc']."', '".$j2_details['to_state']."', '".$j2_details['frm_via_line1']."', '".$j2_details['frm_via_line2']."', '".$j2_details['frm_via_sub']."', '".$j2_details['frm_via_pc']."', '".$j2_details['frm_via_state']."', '".$j2_details['to_via_line1']."', '".$j2_details['to_via_line2']."', '".$j2_details['to_via_sub']."', '".$j2_details['to_via_pc']."', '".$j2_details['to_via_state']."', '".$j2_details['car_id']."', '".$j2_details['pax_nos']."', '".$j2_details['luggage']."', '".$j2_details['baby_seat']."', '".$j2_details['booster_seat']."', '".$j2_details['baby_capsule']."', '', '".$j2_details['kms']."', '".$j2_details['fare']."', '".$j2_details['inter']."', '".$j2_details['ed']."', '".$j2_details['wait']."', '".$j2_details['tolls']."', '".$j2_details['bs']."', '".$j2_details['park']."', '".$j2_details['ah']."', '".$j2_details['me']."', '".$j2_details['alc']."', '".$j2_details['fc']."', '".$j2_details['oth']."', '".$j2_details['tot_fare']."', '".$j2_details['drv_fee']."', '".$j2_details['oth_exp']."', '".$j2_details['profit']."', '".mysql_real_escape_string($j2_details['ext_notes'])."', '".mysql_real_escape_string($j2_details['int_notes'])."')";
				$database = new database;
				$result = $database->query($q);
				$new_j2_id = mysql_insert_id();
				$this->addJobLog($new_j2_id, $_SESSION['USER_ID'], 'New job created', $new_j2_id, 'NIL');
				
				$job_reference->updateJobReferenceTable($new_job_reference_id, 'j1_id', $new_j1_id);
				$job_reference->addJobReferenceLog($new_job_reference_id, $_SESSION['USER_ID'], 'Job 1 Added', $new_j1_id, 'NIL');
				
				$job_reference->updateJobReferenceTable($new_job_reference_id, 'j2_id', $new_j2_id);
				$job_reference->addJobReferenceLog($new_job_reference_id, $_SESSION['USER_ID'], 'Job 2 Added', $new_j2_id, 'NIL');
				
				$x = array('j1_id' => ''.$new_j1_id.'', 'j2_id' => ''.$new_j2_id.'');
				return $x;
			}			
/*---------------------------------------------------------------------------------------------------------------------*/
		public function ReverseCloneIndividualJob($job_id) // These are the common values for bookings
			{
				$job_reference = new jobReference();
				$job_details = $this->getJobDetails($job_id);
				$job_ref_details = $job_reference->getJobReferenceDetails($job_details['job_reference_id']);
				
				$new_job_reference_id = $job_reference->addNewJobReference($_SESSION['USER_ID'], $job_ref_details['job_src'], $job_ref_details['order_ref'], $job_ref_details['acc_type'], $job_ref_details['std_id'], $job_ref_details['std_title'], $job_ref_details['std_fname'], $job_ref_details['std_lname'],'','', $job_ref_details['charge_acc_id'], $job_ref_details['bkg_by_id'], $job_ref_details['pax_id'], '1', $job_ref_details['charge_mode']);
				$job_reference->addJobReferenceLog($new_job_reference_id, $_SESSION['USER_ID'], 'New Job Reference Created', $new_job_reference_id, 'NIL');

				$q = "INSERT INTO job(job_reference_id, job_status, job_date, job_time, frm_flight_no, frm_line1, frm_line2, frm_sub, frm_pc, frm_state, to_flight_no, to_line1, to_line2, to_sub, to_pc, to_state, frm_via_line1, frm_via_line2, frm_via_sub, frm_via_pc, frm_via_state, to_via_line1, to_via_line2, to_via_sub, to_via_pc, to_via_state, car_id, pax_nos, luggage, baby_seat, booster_seat, baby_capsule, driver_status, kms, fare, inter, ed, wait, tolls, bs, park, ah, me, alc, fc, oth, tot_fare, drv_fee, oth_exp, profit, ext_notes, int_notes)
						VALUES('$new_job_reference_id', '20', '".$job_details['job_date']."', '".$job_details['job_time']."', '".$job_details['to_flight_no']."', '".mysql_real_escape_string($job_details['to_line1'])."', '".mysql_real_escape_string($job_details['to_line2'])."', '".$job_details['to_sub']."', '".$job_details['to_pc']."', '".$job_details['to_state']."', '".$job_details['frm_flight_no']."', '".mysql_real_escape_string($job_details['frm_line1'])."', '".mysql_real_escape_string($job_details['frm_line2'])."', '".$job_details['frm_sub']."', '".$job_details['frm_pc']."', '".$job_details['frm_state']."','".$job_details['to_via_line1']."', '".$job_details['to_via_line2']."', '".$job_details['to_via_sub']."', '".$job_details['to_via_pc']."', '".$job_details['to_via_state']."', '".$job_details['frm_via_line1']."', '".$job_details['frm_via_line2']."', '".$job_details['frm_via_sub']."', '".$job_details['frm_via_pc']."', '".$job_details['frm_via_state']."', '".$job_details['car_id']."', '".$job_details['pax_nos']."', '".$job_details['luggage']."', '".$job_details['baby_seat']."', '".$job_details['booster_seat']."', '".$job_details['baby_capsule']."', '', '".$job_details['kms']."', '".$job_details['fare']."', '".$job_details['inter']."', '".$job_details['ed']."', '".$job_details['wait']."', '".$job_details['tolls']."', '".$job_details['bs']."', '".$job_details['park']."', '".$job_details['ah']."', '".$job_details['me']."', '".$job_details['alc']."', '".$job_details['fc']."', '".$job_details['oth']."', '".$job_details['tot_fare']."', '".$job_details['drv_fee']."', '".$job_details['oth_exp']."', '".$job_details['profit']."', '".mysql_real_escape_string($job_details['ext_notes'])."', '".mysql_real_escape_string($job_details['int_notes'])."')";
				$database = new database;
				$result = $database->query($q);
				$id = mysql_insert_id();
				$this->addJobLog($id, $_SESSION['USER_ID'], 'New job created', $id, 'NIL');
				
				$job_reference->updateJobReferenceTable($new_job_reference_id, 'j1_id', $id);
				$job_reference->addJobReferenceLog($new_job_reference_id, $_SESSION['USER_ID'], 'Job 1 Added', $id, 'NIL');
				return $id;
			}
			
/*---------------------------------------------------------------------------------------------------------------------*/
		public function ReverseCloneReturnJob($j1_id, $j2_id) // These are the common values for bookings
			{
				$job_reference = new jobReference();
				
				$j1_details = $this->getJobDetails($j1_id);
				$j2_details = $this->getJobDetails($j2_id);
				
				$job_ref_details = $job_reference->getJobReferenceDetails($j1_details['job_reference_id']);
				
				$new_job_reference_id = $job_reference->addNewJobReference($_SESSION['USER_ID'], $job_ref_details['job_src'], $job_ref_details['order_ref'], $job_ref_details['acc_type'], $job_ref_details['std_id'], $job_ref_details['std_title'], $job_ref_details['std_fname'], $job_ref_details['std_lname'],'','', $job_ref_details['charge_acc_id'], $job_ref_details['bkg_by_id'], $job_ref_details['pax_id'], '2', $job_ref_details['charge_mode']);
				$job_reference->addJobReferenceLog($new_job_reference_id, $_SESSION['USER_ID'], 'New Job Reference Created', $new_job_reference_id, 'NIL');

				$q = "INSERT INTO job(job_reference_id, job_status, job_date, job_time, frm_flight_no, frm_line1, frm_line2, frm_sub, frm_pc, frm_state, to_flight_no, to_line1, to_line2, to_sub, to_pc, to_state, frm_via_line1, frm_via_line2, frm_via_sub, frm_via_pc, frm_via_state, to_via_line1, to_via_line2, to_via_sub, to_via_pc, to_via_state, car_id, pax_nos, luggage, baby_seat, booster_seat, baby_capsule, driver_status, kms, fare, inter, ed, wait, tolls, bs, park, ah, me, alc, fc, oth, tot_fare, drv_fee, oth_exp, profit, ext_notes, int_notes)
						VALUES('$new_job_reference_id', '20', '".$j1_details['job_date']."', '".$j1_details['job_time']."', '".$j1_details['to_flight_no']."', '".mysql_real_escape_string($j1_details['to_line1'])."', '".mysql_real_escape_string($j1_details['to_line2'])."', '".$j1_details['to_sub']."', '".$j1_details['to_pc']."', '".$j1_details['to_state']."', '".$j1_details['frm_flight_no']."', '".mysql_real_escape_string($j1_details['frm_line1'])."', '".mysql_real_escape_string($j1_details['frm_line2'])."', '".$j1_details['frm_sub']."', '".$j1_details['frm_pc']."', '".$j1_details['frm_state']."','".$j1_details['to_via_line1']."', '".$j1_details['to_via_line2']."', '".$j1_details['to_via_sub']."', '".$j1_details['to_via_pc']."', '".$j1_details['to_via_state']."', '".$j1_details['frm_via_line1']."', '".$j1_details['frm_via_line2']."', '".$j1_details['frm_via_sub']."', '".$j1_details['frm_via_pc']."', '".$j1_details['frm_via_state']."', '".$j1_details['car_id']."', '".$j1_details['pax_nos']."', '".$j1_details['luggage']."', '".$j1_details['baby_seat']."', '".$j1_details['booster_seat']."', '".$j1_details['baby_capsule']."', '', '".$j1_details['kms']."', '".$j1_details['fare']."', '".$j1_details['inter']."', '".$j1_details['ed']."', '".$j1_details['wait']."', '".$j1_details['tolls']."', '".$j1_details['bs']."', '".$j1_details['park']."', '".$j1_details['ah']."', '".$j1_details['me']."', '".$j1_details['alc']."', '".$j1_details['fc']."', '".$j1_details['oth']."', '".$j1_details['tot_fare']."', '".$j1_details['drv_fee']."', '".$j1_details['oth_exp']."', '".$j1_details['profit']."', '".mysql_real_escape_string($j1_details['ext_notes'])."', '".mysql_real_escape_string($j1_details['int_notes'])."')";
				$database = new database;
				$result = $database->query($q);
				$new_j1_id = mysql_insert_id();
				$this->addJobLog($new_j1_id, $_SESSION['USER_ID'], 'New job created', $new_j1_id, 'NIL');
				
				$q = "INSERT INTO job(job_reference_id, job_status, job_date, job_time, frm_flight_no, frm_line1, frm_line2, frm_sub, frm_pc, frm_state, to_flight_no, to_line1, to_line2, to_sub, to_pc, to_state, frm_via_line1, frm_via_line2, frm_via_sub, frm_via_pc, frm_via_state, to_via_line1, to_via_line2, to_via_sub, to_via_pc, to_via_state, car_id, pax_nos, luggage, baby_seat, booster_seat, baby_capsule, driver_status, kms, fare, inter, ed, wait, tolls, bs, park, ah, me, alc, fc, oth, tot_fare, drv_fee, oth_exp, profit, ext_notes, int_notes)
						VALUES('$new_job_reference_id', '20', '".$j2_details['job_date']."', '".$j2_details['job_time']."', '".$j2_details['to_flight_no']."', '".mysql_real_escape_string($j2_details['to_line1'])."', '".mysql_real_escape_string($j2_details['to_line2'])."', '".$j2_details['to_sub']."', '".$j2_details['to_pc']."', '".$j2_details['to_state']."', '".$j2_details['frm_flight_no']."', '".mysql_real_escape_string($j2_details['frm_line1'])."', '".mysql_real_escape_string($j2_details['frm_line2'])."', '".$j2_details['frm_sub']."', '".$j2_details['frm_pc']."', '".$j2_details['frm_state']."','".$j2_details['to_via_line1']."', '".$j2_details['to_via_line2']."', '".$j2_details['to_via_sub']."', '".$j2_details['to_via_pc']."', '".$j2_details['to_via_state']."', '".$j2_details['frm_via_line1']."', '".$j2_details['frm_via_line2']."', '".$j2_details['frm_via_sub']."', '".$j2_details['frm_via_pc']."', '".$j2_details['frm_via_state']."', '".$j2_details['car_id']."', '".$j2_details['pax_nos']."', '".$j2_details['luggage']."', '".$j2_details['baby_seat']."', '".$j2_details['booster_seat']."', '".$j2_details['baby_capsule']."', '', '".$j2_details['kms']."', '".$j2_details['fare']."', '".$j2_details['inter']."', '".$j2_details['ed']."', '".$j2_details['wait']."', '".$j2_details['tolls']."', '".$j2_details['bs']."', '".$j2_details['park']."', '".$j2_details['ah']."', '".$j2_details['me']."', '".$j2_details['alc']."', '".$j2_details['fc']."', '".$j2_details['oth']."', '".$j2_details['tot_fare']."', '".$j2_details['drv_fee']."', '".$j2_details['oth_exp']."', '".$j2_details['profit']."', '".mysql_real_escape_string($j2_details['ext_notes'])."', '".mysql_real_escape_string($j2_details['int_notes'])."')";
				$database = new database;
				$result = $database->query($q);
				$new_j2_id = mysql_insert_id();
				$this->addJobLog($new_j2_id, $_SESSION['USER_ID'], 'New job created', $new_j2_id, 'NIL');
				
				$job_reference->updateJobReferenceTable($new_job_reference_id, 'j1_id', $new_j1_id);
				$job_reference->addJobReferenceLog($new_job_reference_id, $_SESSION['USER_ID'], 'Job 1 Added', $new_j1_id, 'NIL');
				
				$job_reference->updateJobReferenceTable($new_job_reference_id, 'j2_id', $new_j2_id);
				
				$x = array('j1_id' => ''.$new_j1_id.'', 'j2_id' => ''.$new_j2_id.'');
				$job_reference->addJobReferenceLog($new_job_reference_id, $_SESSION['USER_ID'], 'Job 2 Added', $new_j2_id, 'NIL');
				return $x;
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function deleteAJob($job_id)
			{
				$database = new database;
				$job_reference = new jobReference();
				$job_details = $this->getJobDetails($job_id);
				$job_ref_details = $job_reference->getJobReferenceDetails($job_details['job_reference_id']);
				
				$q = "DELETE  FROM job where id = '".$job_id."'";
				$result = $database->query($q);
				
				//if this job was a one way job only then delete the job reference
				if($job_ref_details['job_type'] == '1')
					{
						$q = "DELETE  FROM job__reference where id = '".$job_ref_details['id']."'";
						$result = $database->query($q);
					}
				if($job_ref_details['job_type'] == '2')
					{
						//change job type to 1 - one way
						$job_reference->updateJobReferenceTable($job_ref_details['id'], 'job_type', '1');
						
						if($job_ref_details['j1_id'] == $job_id) // means the j1_id needs to be deleted
							{
								//first make j2_id - j1_id
								$job_reference->updateJobReferenceTable($job_ref_details['id'], 'j1_id', $job_ref_details['j2_id']);
								//now make the j2_id 0
								$job_reference->updateJobReferenceTable($job_ref_details['id'], 'j2_id', '0');
							}
						else // means the second job needs to be deleted
							{
								//change the j1_id to 0
								$job_reference->updateJobReferenceTable($job_ref_details['id'], 'j2_id', '0');
							}
					}
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function deleteJobs($j1_id, $j2_id)
			{
				$database = new database;
				$job_reference = new jobReference();
				
				$j1_details = $this->getJobDetails($j1_id);
				$j2_details = $this->getJobDetails($j2_id);
				
				$job_ref_details = $job_reference->getJobReferenceDetails($job_details['job_reference_id']);
				
				$q = "DELETE  FROM job where id = '".$j1_id."'";
				$result = $database->query($q);
				
				$q = "DELETE  FROM job where id = '".$j2_id."'";
				$result = $database->query($q);
				
				$q = "DELETE  FROM job__reference where id = '".$job_ref_details['id']."'";
				$result = $database->query($q);
			}


/*---------------------------------------------------------------------------------------------------------------------*/
		public function getHTMLofWholeBooking($job_reference_id, $clickable_id='0')
			{
				$job_reference = new jobReference();
				$user = new User();
				require_once(CLASSES_PATH . "vehicle.php");
				$vehicle = new Vehicle();
				
				$job_reference_details = $job_reference->getJobReferenceDetails($job_reference_id);
				$bkg_by_details = $user->getUserDetails($job_reference_details['bkg_by_id']);
				$pax_details = $user->getUserDetails($job_reference_details['pax_id']);
				$j1_details = $this->getJobDetails($job_reference_details['j1_id']);
				$student_name = ''.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].'';
				$html .='<table align="left" border="1" cellpadding="5" cellspacing="0" width="60%" style="border-collapse: collapse; border-color: #666; ">';
							if($job_reference_details['std_fname'] != '')
								{
									$html .='
									<tr>
										<td width="50%"><strong>Student Name:</strong> </td>
										<td style="background: #eee;">'.$student_name.' (ID - '.$job_reference_details['std_id'].')</td>
									</tr>
									<tr>
										<td width="50%"><strong>Student Email:</strong></td>
										<td style="background: #eee;">'.$job_reference_details['std_email'].'</td>
									</tr>
									<tr>
										<td width="50%"><strong>Student Phone:</strong></td>
										<td style="background: #eee;">'.$job_reference_details['std_ph'].'</td>
									</tr>';
								}
							else
								{
									$html .='
									<tr>
										<td width="50%"><strong>Passenger Name:</strong> </td>
										<td style="background: #eee;">'.$pax_details['title'].' '.$pax_details['fname'].' '.$pax_details['lname'].'</td>
									</tr>
									<tr>
										<td width="50%"><strong>Passenger Email:</strong></td>
										<td style="background: #eee;">'.$pax_details['email'].'</td>
									</tr>
									<tr>
										<td width="50%"><strong>Passenger Phone:</strong></td>
										<td style="background: #eee;">'.$pax_details['mobile'].' '.$pax_details['phone'].'</td>
									</tr>';
								}
							$html .='	
							<tr>
								<td width="50%"><strong>Booking Type:</strong></td>
								<td style="background: #eee;">'.(($job_reference_details['job_type']=='1')?'One Way':'Return').'</td>
							</tr>
							<tr>';
							if($_SESSION['ROLE_ID'] == '1' || $clickable_id == '1')
							{
								$html .='<td colspan="2" style="background: #d6d3d3;"><strong>Details of Booking ID - <a href="http://dev.bookings-global.com.au/Forms/show_booking_details.php?job_id='.$job_reference_details['j1_id'].'" >'.$job_reference_details['j1_id'].'</a></strong></td>';
							}
							else
							{
								$html .='<td colspan="2" style="background: #d6d3d3;"><strong>Details of Booking ID - '.$job_reference_details['j1_id'].'</strong></td>';
							}
							$html .='
							</tr>
							<tr>
								<td width="50%"><strong>';
							if($job_reference_details['std_fname'] != '')
								$html .=' Arrival Date: ';
							else
								$html .=' Journey Date: ';
							$html .='	</strong></td>
								<td style="background: #eee;"><strong>'.date("l, d F, Y", strtotime($j1_details['job_date'])).'</strong></td>
							</tr>
							<tr>
								<td width="50%"><strong>';
							if($job_reference_details['std_fname'] != '')
								$html .=' Arrival Time: ';
							else
								$html .=' Journey Time: ';
							$html .='	</strong></td>
								<td style="background: #eee;"><strong>'.date("H:i A", strtotime($j1_details['job_time'])).'</strong></td>
							</tr>
							<tr>
								<td width="50%"><strong>';
							if($job_reference_details['std_fname'] != '')
								$html .=' Pickup From: ';
							else
								$html .=' Journey From: ';
							$html .='	</strong></td>
								<td style="background: #eee;">';
									if($j1_details['frm_flight_no']!=''){ $html.= 'Flight No. - '.$j1_details['frm_flight_no'].'<br/>';}
									if($j1_details['frm_line1']!=''){ $html.= ''.$j1_details['frm_line1'].'<br/>';}
									if($j1_details['frm_line2']!=''){ $html.= ''.$j1_details['frm_line2'].'<br/>';}
									if($j1_details['frm_sub']!=''){ $html.= ''.$j1_details['frm_sub'].'';}
									if($j1_details['frm_pc']!=''){ $html.= ' '.$j1_details['frm_pc'].'';}
									if($j1_details['frm_state']!=''){ $html.= ' '.$j1_details['frm_state'].'<br/>';}
									if($j1_details['frm_via_sub'] != '')
										{
											$html.='<strong>TO</strong><br/>';
											if($j1_details['frm_via_line1']!=''){ $html.= ''.$j1_details['frm_via_line1'].'<br/>';}
											if($j1_details['frm_via_line2']!=''){ $html.= ''.$j1_details['frm_via_line2'].'<br/>';}
											if($j1_details['frm_via_sub']!=''){ $html.= ''.$j1_details['frm_via_sub'].'';}
											if($j1_details['frm_via_pc']!=''){ $html.= ' '.$j1_details['frm_via_pc'].'';}
											if($j1_details['frm_via_state']!=''){ $html.= ' '.$j1_details['frm_via_state'].'<br/>';}
										}
								$html.='
								</td>
							</tr>
							<tr>
								<td width="50%"><strong>';
							if($job_reference_details['std_fname'] != '')
								$html .=' Destination: ';
							else
								$html .=' Journey To: ';
							$html .='	</strong></td>
								<td style="background: #eee;">';
									if($j1_details['to_flight_no']!=''){ $html.= 'Flight No. - '.$j1_details['to_flight_no'].'<br/>';}
									if($j1_details['to_line1']!=''){ $html.= ''.$j1_details['to_line1'].'<br/>';}
									if($j1_details['to_line2']!=''){ $html.= ''.$j1_details['to_line2'].'<br/>';}
									if($j1_details['to_sub']!=''){ $html.= ''.$j1_details['to_sub'].'';}
									if($j1_details['to_pc']!=''){ $html.= ' '.$j1_details['to_pc'].'';}
									if($j1_details['to_state']!=''){ $html.= ' '.$j1_details['to_state'].'<br/>';}
									if($j1_details['to_via_sub'] != '')
										{
											$html.='<strong>TO</strong><br/>';
											if($j1_details['to_via_line1']!=''){ $html.= ''.$j1_details['to_via_line1'].'<br/>';}
											if($j1_details['to_via_line2']!=''){ $html.= ''.$j1_details['to_via_line2'].'<br/>';}
											if($j1_details['to_via_sub']!=''){ $html.= ''.$j1_details['to_via_sub'].'';}
											if($j1_details['to_via_pc']!=''){ $html.= ' '.$j1_details['to_via_pc'].'';}
											if($j1_details['to_via_state']!=''){ $html.= ' '.$j1_details['to_via_state'].'<br/>';}
										}
								$html.='
								</td>
							</tr>
							<tr>
								<td width="50%"><strong>Other Requests:</strong></td>
								<td style="background: #eee;">';
									if($j1_details['car_id']!=''){ $html.= 'Vehicle Requested. - '.$vehicle->getCarTypeType($j1_details['car_id']).'<br/>';}
									if($j1_details['pax_nos']!=''){ $html.= 'No. of Passengers - '.$j1_details['pax_nos'].'<br/>';}
									if($j1_details['luggage'] > 0){ $html.= 'No. of Luggage - '.$j1_details['luggage'].'<br/>';}
									if($j1_details['baby_seat']>0){ $html.= 'No. of Baby Seats - '.$j1_details['baby_seat'].'<br/>';}
									if($j1_details['booster_seat']>0){ $html.= 'No. of Booster Seats - '.$j1_details['booster_seat'].'<br/>';}
									if($j1_details['baby_capsule']>0){ $html.= 'No. of Baby Capsules - '.$j1_details['baby_capsule'].'<br/>';}
									if($j1_details['ext_notes']!=''){ $html.= '<strong>Extra Notes - '.$j1_details['ext_notes'].'</strong><br/>';}
								$html.='
								</td>
							</tr>';
							if($job_reference_details['job_type'] == '2')
								{
									$j2_details = $this->getJobDetails($job_reference_details['j2_id']);
									if($_SESSION['ROLE_ID'] == '1')
									{
										$html .='<tr><td colspan="2" style="background: #d6d3d3;"><strong>Details of Booking ID - <a href="http://dev.bookings-global.com.au/Forms/show_booking_details.php?job_id='.$job_reference_details['j2_id'].'" >'.$job_reference_details['j2_id'].'</a></strong></td></tr>';
									}
									else
									{
										$html .='<tr><td colspan="2" style="background: #d6d3d3;"><strong>Details of Booking ID - '.$job_reference_details['j2_id'].'</strong></td></tr>';
									}
									$html.='
									<tr>
										<td width="50%"><strong>Journey Date:</strong></td>
										<td style="background: #eee;"><strong>'.date("l, d F, Y", strtotime($j2_details['job_date'])).'</strong></td>
									</tr>
									<tr>
										<td width="50%"><strong>Journey Time:</strong></td>
										<td style="background: #eee;"><strong>'.date("H:i A", strtotime($j2_details['job_time'])).'</strong></td>
									</tr>
									<tr>
										<td width="50%"><strong>Journey from:</strong></td>
										<td style="background: #eee;">';
											if($j2_details['frm_flight_no']!=''){ $html.= 'Flight No. - '.$j2_details['frm_flight_no'].'<br/>';}
											if($j2_details['frm_line1']!=''){ $html.= ''.$j2_details['frm_line1'].'<br/>';}
											if($j2_details['frm_line2']!=''){ $html.= ''.$j2_details['frm_line2'].'<br/>';}
											if($j2_details['frm_sub']!=''){ $html.= ''.$j2_details['frm_sub'].'';}
											if($j2_details['frm_pc']!=''){ $html.= ' '.$j2_details['frm_pc'].'';}
											if($j2_details['frm_state']!=''){ $html.= ' '.$j2_details['frm_state'].'<br/>';}
											if($j2_details['frm_via_sub'] != '')
												{
													$html.='<strong>TO</strong><br/>';
													if($j2_details['frm_via_line1']!=''){ $html.= ''.$j2_details['frm_via_line1'].'<br/>';}
													if($j2_details['frm_via_line2']!=''){ $html.= ''.$j2_details['frm_via_line2'].'<br/>';}
													if($j2_details['frm_via_sub']!=''){ $html.= ''.$j2_details['frm_via_sub'].'';}
													if($j2_details['frm_via_pc']!=''){ $html.= ' '.$j2_details['frm_via_pc'].'';}
													if($j2_details['frm_via_state']!=''){ $html.= ' '.$j2_details['frm_via_state'].'<br/>';}
												}
										$html.='
										</td>
									</tr>
									<tr>
										<td width="50%"><strong>Journey To:</strong></td>
										<td style="background: #eee;">';
											if($j2_details['to_flight_no']!=''){ $html.= 'Flight No. - '.$j2_details['to_flight_no'].'<br/>';}
											if($j2_details['to_line1']!=''){ $html.= ''.$j2_details['to_line1'].'<br/>';}
											if($j2_details['to_line2']!=''){ $html.= ''.$j2_details['to_line2'].'<br/>';}
											if($j2_details['to_sub']!=''){ $html.= ''.$j2_details['to_sub'].'';}
											if($j2_details['to_pc']!=''){ $html.= ' '.$j2_details['to_pc'].'';}
											if($j2_details['to_state']!=''){ $html.= ' '.$j2_details['to_state'].'<br/>';}
											if($j2_details['to_via_sub'] != '')
												{
													$html.='<strong>TO</strong><br/>';
													if($j2_details['to_via_line1']!=''){ $html.= ''.$j2_details['to_via_line1'].'<br/>';}
													if($j2_details['to_via_line2']!=''){ $html.= ''.$j2_details['to_via_line2'].'<br/>';}
													if($j2_details['to_via_sub']!=''){ $html.= ''.$j2_details['to_via_sub'].'';}
													if($j2_details['to_via_pc']!=''){ $html.= ' '.$j2_details['to_via_pc'].'';}
													if($j2_details['to_via_state']!=''){ $html.= ' '.$j2_details['to_via_state'].'<br/>';}
												}
										$html.='
										</td>
									</tr>
									<tr>
										<td width="50%"><strong>Other Requests:</strong></td>
										<td style="background: #eee;">';
											if($j2_details['car_id']!=''){ $html.= 'Vehicle Requested. -'.$vehicle->getCarTypeType($j2_details['car_id']).'<br/>';}
											if($j2_details['pax_nos']!=''){ $html.= 'No. of Passengers - '.$j2_details['pax_nos'].'<br/>';}
											if($j2_details['luggage'] > 0){ $html.= 'No. of Luggage - '.$j2_details['luggage'].'<br/>';}
											if($j2_details['baby_capsule']>0){ $html.= 'No. of Baby Capsules - '.$j2_details['baby_capsule'].'<br/>';}
											if($j2_details['baby_seat']>0){ $html.= 'No. of Baby Seats - '.$j2_details['baby_seat'].'<br/>';}
											if($j2_details['booster_seat']>0){ $html.= 'No. of Booster Seats - '.$j2_details['booster_seat'].'<br/>';}
											if($j2_details['ext_notes']!=''){ $html.= '<strong>Extra Notes - '.$j2_details['ext_notes'].'</strong><br/>';}
										$html.='
										</td>
									</tr>';
								}
							if($job_reference_details['under_18'] != '0')
							{
								
								$html.='
								<tr>
									<td width="50%" style="background: #adaaaa; color:#FC0808;"><strong>Student Under 18:</strong> </td>
									<td style="background: #adaaaa; color:#FC0808; ">Yes</td>
								</tr>';
							}
							if($job_reference_details['std_fname'] != '')
								{
									if($job_reference_details['on_behalf']=='1')
										{
											$html.='
											<tr>
												<td width="50%" style="background: #adaaaa;"><strong>Booked by:</strong> </td>
												<td style="background: #adaaaa;">'.$job_reference_details['on_behalf_title'].' '.$job_reference_details['on_behalf_fname'].' '.$job_reference_details['on_behalf_lname'].'</td>
											</tr>';
										}
									else
										{
											$html.='
											<tr>
												<td width="50%" style="background: #adaaaa;"><strong>Booked by:</strong> </td>
												<td style="background: #adaaaa;">'.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' (ID - '.$job_reference_details['std_id'].')</td>
											</tr>';
										}
								} 
							else
								{
									$html.='
									<tr>
										<td width="50%" style="background: #adaaaa;"><strong>Booked by:</strong> </td>
										<td style="background: #adaaaa;">'.$bkg_by_details['title'].' '.$bkg_by_details['fname'].' '.$bkg_by_details['lname'].'<br/>
										'.$bkg_by_details['mobile'].' '.$bkg_by_details['phone'].'<br/>
										'.$bkg_by_details['email'].'
										</td>
									</tr>';
								}
							$html.=
							'<tr>
								<td width="50%" style="background: #adaaaa;"><strong>Booking made on:</strong> </td>
								<td style="background: #adaaaa;">'.date("l, d F, Y", strtotime($job_reference_details['created_on'])).' at '.date("H:i A", strtotime($job_reference_details['created_on'])).'</td>
							</tr>
						</table>';
				return $html;
			}
	
/*---------------------------------------------------------------------------------------------------------------------*/
		public function getHTMLforIndividualBooking($job_id)
			{
				
				$job_reference = new jobReference();
				$user = new User();
				$vehicle = new Vehicle();
				
				$job_details = $this->getJobDetails($job_id);
				$job_reference_details = $job_reference->getJobReferenceDetails($job_details['job_reference_id']);
				$bkg_by_details = $user->getUserDetails($job_reference_details['bkg_by_id']);
				$pax_details = $user->getUserDetails($job_reference_details['pax_id']);
				$student_name = ''.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].'';
				
				$html .='<table align="left" border="1" cellpadding="5" cellspacing="0" width="60%" style="border-collapse: collapse; border-color: #666; ">';
							if($job_reference_details['std_fname'] != '')
								{
									$html .='
									<tr>
										<td width="50%"><strong>Student Name:</strong> </td>
										<td style="background: #eee;">'.$student_name.' (ID - '.$job_reference_details['std_id'].')</td>
									</tr>
									<tr>
										<td width="50%"><strong>Student Email:</strong></td>
										<td style="background: #eee;">'.$job_reference_details['std_email'].'</td>
									</tr>
									<tr>
										<td width="50%"><strong>Student Phone:</strong></td>
										<td style="background: #eee;">'.$job_reference_details['std_ph'].'</td>
									</tr>';
								}
							else
								{
									$html .='
									<tr>
										<td width="50%"><strong>Passenger Name:</strong> </td>
										<td style="background: #eee;">'.$pax_details['title'].' '.$pax_details['fname'].' '.$pax_details['lname'].'</td>
									</tr>
									<tr>
										<td width="50%"><strong>Passenger Email:</strong></td>
										<td style="background: #eee;">'.$pax_details['email'].'</td>
									</tr>
									<tr>
										<td width="50%"><strong>Passenger Phone:</strong></td>
										<td style="background: #eee;">'.$pax_details['mobile'].' '.$pax_details['phone'].'</td>
									</tr>';
								} 
							$html .='
							<tr>
								<td colspan="2">Please Note: If this is a return booking, you will receive a separate email for other leg of the journey</td>
							</tr>
							<tr>
								<td width="50%"><strong>Booking Type:</strong></td>
								<td style="background: #eee;">'.(($job_reference_details['job_type']=='1')?'One Way':'Return').'</td>
							</tr>';
							
							if($_SESSION['ROLE_ID'] == '1')
							{
								$html .='<tr><td colspan="2" style="background: #d6d3d3;"><strong>Details of Booking ID - <a href="http://dev.bookings-global.com.au/Forms/show_booking_details.php?job_id='.$job_details['id'].'" >'.$job_details['id'].'</a></strong></td></tr>';
							}
							else
							{
								$html .='<tr><td colspan="2" style="background: #d6d3d3;"><strong>Details of Booking ID - '.$job_details['id'].'</strong></td></tr>';
							}
							
							$html .='<tr>
								<td width="50%"><strong>';
							if($job_reference_details['std_fname'] != '')
								$html .=' Arrival Date: ';
							else
								$html .=' Journey Date: ';
							$html .='	</strong></td>
								<td style="background: #eee;"><strong>'.date("l, d F, Y", strtotime($job_details['job_date'])).'</strong></td>
							</tr>
							<tr>
								<td width="50%"><strong>';
							if($job_reference_details['std_fname'] != '')
								$html .=' Arrival Time: ';
							else
								$html .=' Journey Time: ';
							$html .='	</strong></td>
								<td style="background: #eee;"><strong>'.date("H:i A", strtotime($job_details['job_time'])).'</strong></td>
							</tr>
							<tr>
								<td width="50%"><strong>';
							if($job_reference_details['std_fname'] != '')
								$html .=' Pickup From: ';
							else
								$html .=' Journey From: ';	
							$html .='	</strong></td>
								<td style="background: #eee;">';
									if($job_details['frm_flight_no']!=''){ $html.= 'Flight No. - '.$job_details['frm_flight_no'].'<br/>';}
									if($job_details['frm_line1']!=''){ $html.= ''.$job_details['frm_line1'].'<br/>';}
									if($job_details['frm_line2']!=''){ $html.= ''.$job_details['frm_line2'].'<br/>';}
									if($job_details['frm_sub']!=''){ $html.= ''.$job_details['frm_sub'].'';}
									if($job_details['frm_pc']!=''){ $html.= ' '.$job_details['frm_pc'].'';}
									if($job_details['frm_state']!=''){ $html.= ' '.$job_details['frm_state'].'<br/>';}
									if($job_details['frm_via_sub'] != '')
										{
											$html.='<strong>TO</strong><br/>';
											if($job_details['frm_via_line1']!=''){ $html.= ''.$job_details['frm_via_line1'].'<br/>';}
											if($job_details['frm_via_line2']!=''){ $html.= ''.$job_details['frm_via_line2'].'<br/>';}
											if($job_details['frm_via_sub']!=''){ $html.= ''.$job_details['frm_via_sub'].'';}
											if($job_details['frm_via_pc']!=''){ $html.= ' '.$job_details['frm_via_pc'].'';}
											if($job_details['frm_via_state']!=''){ $html.= ' '.$job_details['frm_via_state'].'<br/>';}
										}
								$html.='
								</td>
							</tr>
							<tr>
								<td width="50%"><strong>';
							if($job_reference_details['std_fname'] != '')
								$html .=' Destination: ';
							else
								$html .=' Journey To: ';
							$html .='	</strong></td>
								<td style="background: #eee;">';
									if($job_details['to_flight_no']!=''){ $html.= 'Flight No. - '.$job_details['to_flight_no'].'<br/>';}
									if($job_details['to_line1']!=''){ $html.= ''.$job_details['to_line1'].'<br/>';}
									if($job_details['to_line2']!=''){ $html.= ''.$job_details['to_line2'].'<br/>';}
									if($job_details['to_sub']!=''){ $html.= ''.$job_details['to_sub'].'';}
									if($job_details['to_pc']!=''){ $html.= ' '.$job_details['to_pc'].'';}
									if($job_details['to_state']!=''){ $html.= ' '.$job_details['to_state'].'<br/>';}
									if($job_details['to_via_sub'] != '')
										{
											$html.='<strong>TO</strong><br/>';
											if($job_details['to_via_line1']!=''){ $html.= ''.$job_details['to_via_line1'].'<br/>';}
											if($job_details['to_via_line2']!=''){ $html.= ''.$job_details['to_via_line2'].'<br/>';}
											if($job_details['to_via_sub']!=''){ $html.= ''.$job_details['to_via_sub'].'';}
											if($job_details['to_via_pc']!=''){ $html.= ' '.$job_details['to_via_pc'].'';}
											if($job_details['to_via_state']!=''){ $html.= ' '.$job_details['to_via_state'].'<br/>';}
										}
								$html.='
								</td>
							</tr>
							<tr>
								<td width="50%"><strong>Other Requests:</strong></td>
								<td style="background: #eee;">';
									if($job_details['car_id']!=''){ $html.= 'Vehicle Requested. - '.$vehicle->getCarTypeType($job_details['car_id']).'<br/>';}
									if($job_details['pax_nos']!=''){ $html.= 'No. of Passengers - '.$job_details['pax_nos'].'<br/>';}
									if($job_details['luggage'] > 0){ $html.= 'No. of Luggage - '.$job_details['luggage'].'<br/>';}
									if($job_details['baby_capsule']>0){ $html.= 'No. of Baby Capsules - '.$job_details['baby_capsule'].'<br/>';}
									if($job_details['baby_seat']>0){ $html.= 'No. of Baby Seats - '.$job_details['baby_seat'].'<br/>';}
									if($job_details['booster_seat']>0){ $html.= 'No. of Booster Seats - '.$job_details['booster_seat'].'<br/>';}
									if($job_details['ext_notes']!=''){ $html.= '<strong>Extra Notes - '.$job_details['ext_notes'].'</strong><br/>';}
								$html.='
								</td>
							</tr>';
							 
							if($job_reference_details['under_18'] != '0')
							{
								
								$html.='
								<tr>
									<td width="50%" style="background: #adaaaa; color:#FC0808;"><strong>Student Under 18:</strong> </td>
									<td style="background: #adaaaa; color:#FC0808; ">Yes</td>
								</tr>';
							}
							
							if($job_reference_details['std_fname'] != '' && $job_reference_details['entered_by']=='1' )
							{
								
								if($job_reference_details['on_behalf']=='1')
								{
									$bkg_details = substr($j1_details['int_notes'] , strpos($j1_details['int_notes'], 'Booked by') +16 );
									$bkg_arr = explode(' ', $bkg_details);
									$html.='
									<tr>
										<td width="50%" style="background: #adaaaa;"><strong>Booked by:</strong> </td>
										<td style="background: #adaaaa;">'.$bkg_arr[0].' '.$bkg_arr[1].' '.$bkg_arr[2].'<br/>
									'.$bkg_arr[3].'<br/>
									'.$bkg_arr[4].'
									</td>
									</tr>';
								}
								else
								{
									$html.='
									<tr>
										<td width="50%" style="background: #adaaaa;"><strong>Booked by:</strong> </td>
										<td style="background: #adaaaa;">'.$student_name.' (ID - '.$job_reference_details['std_id'].')
										</td>
									</tr>';
								}
							}
							else
							{
								$html .=
								'<tr>
									<td width="50%" style="background: #adaaaa;"><strong>Booked by:</strong> </td>
									<td style="background: #adaaaa;">'.$bkg_by_details['title'].' '.$bkg_by_details['fname'].' '.$bkg_by_details['lname'].'<br/>
									'.$bkg_by_details['mobile'].' '.$bkg_by_details['phone'].'<br/>
									'.$bkg_by_details['email'].'
									</td>
								</tr>';
							}
							
							$html .=
							'<tr>
								<td width="50%" style="background: #adaaaa;"><strong>Booking made on:</strong> </td>
								<td style="background: #adaaaa;">'.date("l, d F, Y", strtotime($job_reference_details['created_on'])).' at '.date("H:i A", strtotime($job_reference_details['created_on'])).'</td>
							</tr>';
				return $html;
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function getHTMLforIndividualJobForDriver($job_id)
			{
				$job_reference = new jobReference();
				$user = new User();
				$vehicle = new Vehicle();
				
				$job_details = $this->getJobDetails($job_id);
				$job_reference_details = $job_reference->getJobReferenceDetails($job_details['job_reference_id']);
				
				$pax_details = $user->getUserDetails($job_reference_details['pax_id']);
				$student_name = ''.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].'';
				$html .='<p>JOB DETAILS</p><br/>
						<table align="left" border="1" cellpadding="5" cellspacing="0" width="100%" style="border-collapse: collapse; border-color: #666; ">';
						if($job_reference_details['std_fname'] != '')
							{
								$html .='
								<tr>
									<td width="30%"><strong>Passenger Name:</strong> </td>
									<td style="background: #eee;">'.$student_name.' (ID - '.$job_reference_details['std_id'].')</td>
								</tr>';
							}
						else
							{
								$html .='
								<tr>
									<td width="30%"><strong>Passenger Name:</strong> </td>
									<td style="background: #eee;">'.$pax_details['title'].' '.$pax_details['fname'].' '.$pax_details['lname'].'</td>
								</tr>';
							}
						$html .='
							<tr>
								<td width="30%"><strong> Date:</strong></td>
								<td style="background: #eee;"><strong>'.date("l, d F, Y", strtotime($job_details['job_date'])).'</strong></td>
							</tr>
							<tr>
								<td width="30%"><strong> Time:</strong></td>
								<td style="background: #eee;"><strong>'.date("H:i A", strtotime($job_details['job_time'])).'</strong></td>
							</tr>
							<tr>
								<td width="30%"><strong> Pickup From:</strong></td>
								<td style="background: #eee;">';
									if($job_details['frm_flight_no']!=''){ $html.= 'Flight No. -'.$job_details['frm_flight_no'].'<br/>';}
									if($job_details['frm_line1']!=''){ $html.= ''.$job_details['frm_line1'].'<br/>';}
									if($job_details['frm_line2']!=''){ $html.= ''.$job_details['frm_line2'].'<br/>';}
									if($job_details['frm_sub']!=''){ $html.= ''.$job_details['frm_sub'].'';}
									if($job_details['frm_pc']!=''){ $html.= ' '.$job_details['frm_pc'].'';}
									if($job_details['frm_state']!=''){ $html.= ' '.$job_details['frm_state'].'<br/><br/>';}
									if($job_details['frm_via_sub'] != '')
										{
											$html.='<strong>2nd PICKUP-DROP OFF</strong><br/>';
											if($job_details['frm_via_line1']!=''){ $html.= ''.$job_details['frm_via_line1'].'<br/>';}
											if($job_details['frm_via_line2']!=''){ $html.= ''.$job_details['frm_via_line2'].'<br/>';}
											if($job_details['frm_via_sub']!=''){ $html.= ''.$job_details['frm_via_sub'].'';}
											if($job_details['frm_via_pc']!=''){ $html.= ' '.$job_details['frm_via_pc'].'';}
											if($job_details['frm_via_state']!=''){ $html.= ' '.$job_details['frm_via_state'].'<br/>';}
										}
								$html.='
								</td>
							</tr>
							<tr>
								<td width="30%"><strong> Drop Off:</strong></td>
								<td style="background: #eee;">';
									if($job_details['to_flight_no']!=''){ $html.= 'Flight No. - '.$job_details['to_flight_no'].'<br/>';}
									if($job_details['to_line1']!=''){ $html.= ''.$job_details['to_line1'].'<br/>';}
									if($job_details['to_line2']!=''){ $html.= ''.$job_details['to_line2'].'<br/>';}
									if($job_details['to_sub']!=''){ $html.= ''.$job_details['to_sub'].'';}
									if($job_details['to_pc']!=''){ $html.= ' '.$job_details['to_pc'].'';}
									if($job_details['to_state']!=''){ $html.= ' '.$job_details['to_state'].'<br/><br/>';}
									if($job_details['to_via_sub'] != '')
										{
											$html.='<strong>LAST DROP</strong><br/>';
											if($job_details['to_via_line1']!=''){ $html.= ''.$job_details['to_via_line1'].'<br/>';}
											if($job_details['to_via_line2']!=''){ $html.= ''.$job_details['to_via_line2'].'<br/>';}
											if($job_details['to_via_sub']!=''){ $html.= ''.$job_details['to_via_sub'].'';}
											if($job_details['to_via_pc']!=''){ $html.= ' '.$job_details['to_via_pc'].'';}
											if($job_details['to_via_state']!=''){ $html.= ' '.$job_details['to_via_state'].'<br/>';}
										}
								$html.='
								</td>
							</tr>
							<tr>
								<td width="30%"><strong>Other Requests:</strong></td>
								<td style="background: #eee;">';
									if($job_details['car_id']!=''){ $html.= 'Vehicle Requested. - '.$vehicle->getCarTypeType($job_details['car_id']).'<br/>';}
									if($job_details['pax_nos']!=''){ $html.= 'No. of Passengers - '.$job_details['pax_nos'].'<br/>';}
									if($job_details['luggage'] > 0){ $html.= 'No. of Luggage - '.$job_details['luggage'].'<br/>';}
									if($job_details['baby_capsule']>0){ $html.= 'No. of Baby Capsules - '.$job_details['baby_capsule'].'<br/>';}
									if($job_details['baby_seat']>0){ $html.= 'No. of Baby Seats - '.$job_details['baby_seat'].'<br/>';}
									if($job_details['booster_seat']>0){ $html.= 'No. of Booster Seats - '.$job_details['booster_seat'].'<br/>';}
									/*if($job_details['ext_notes']!=''){ $html.= '<strong>Extra Notes - '.$job_details['ext_notes'].'</strong><br/>';}*/
								$html.='
								</td>
							</tr>
						</table>
						<div style="clear:both">&nbsp;</div>';
				return $html;
			}
/*---------------------------------------------------------------------------------------------------------------------*/			
		public function getIndividualJobHtml($job_id)
			{
				$singleBooking = $this->getindividualBookingDetails($job_id);
				if(count($singleBooking))
					{
						$myDateTime = DateTime::createFromFormat('d-m-y H:i:s', $singleBooking['bkg_date'].' '.$singleBooking['bkg_time']);
						$from_date= $myDateTime->format('d-m-Y');
						$from_time= $myDateTime->format('H:i');
						$html = '<table align="center" border="1"  cellpadding="5" cellspacing="0" width="100%" style="border-collapse: collapse; border-color: #666; ">
								<tr><td colspan="2"> Details of Booking Id '.$job_id.'</td></tr>
								<tr><td width="50%"><strong>Journey Date:</strong> </td><td style="background: #eee;">'.$from_date.'</td></tr>
								<tr><td width="50%"><strong>Journey Time:</strong> </td><td style="background: #eee;">'.$from_time.'</td></tr>
								<tr><td width="50%"><strong>Car type</strong></td><td>'.$singleBooking['car_type'].'</td></tr>
								<tr><td width="50%"><strong>No. of Passenger:</strong> </td><td style="background: #eee;">'.$singleBooking['pax_nos'].'</td></tr>
								<tr><td width="50%"><strong>Luggage</strong></td><td>'.$singleBooking['luggage'].'</td></tr>';
						$html = ($singleBooking['frm_flight_no']!='' || $singleBooking['to_flight_no']!='' || $singleBooking['baby_seat']!='0' || $singleBooking['booster_seat']!='0' || $singleBooking['baby_capsule']!='0')?$html.'<tr><td colspan="2"> <strong >Extra </strong></td></tr>':$html;
						$html = ($singleBooking['frm_flight_no']!='')? $html.'<tr><td width="50%"><strong>From Flight number:</strong> </td><td style="background: #eee;">'.$singleBooking['frm_flight_no'].'</td></tr>':$html;
						$html = ($singleBooking['to_flight_no']!='')? $html.'<tr><td width="50%"><strong>To Flight number:</strong> </td><td style="background: #eee;">'.$singleBooking['to_flight_no'].'</td></tr>':$html;
						$html = ($singleBooking['baby_seat']!='0')? $html.'<tr><td width="50%"><strong>Baby Seat:</strong> </td><td style="background: #eee;">'.$singleBooking['baby_seat'].'</td></tr>':$html;
						$html = ($singleBooking['booster_seat']!='0')? $html.'<tr><td width="50%"><strong>Booster:</strong> </td><td style="background: #eee;">'.$singleBooking['booster_seat'].'</td></tr>':$html;
						$html = ($singleBooking['baby_capsule']!='0')? $html.'<tr><td width="50%"><strong>Baby Capsule:</strong> </td><td style="background: #eee;">'.$singleBooking['baby_capsule'].'</td></tr>':$html;
						$html .='</table><br />';
					}
				return $html;
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function getAdminDetails()
			{
				$q = "SELECT * from user where role_id = '1'";
				$database = new database;
				$result = $database->query($q);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				return $rows;
			}
/*---------------------------------------------------------------------------------------------------------------------*/	

		public function getHTMLReportOfDriver($rows)
			{
				if($_SESSION['ROLE_ID'] == '1')
				{
					$htmlstr = '<table width="100%" >
								<tr style="background-color:#EAF9FC;"><th style="text-align:left" >Job Id</th><th style="text-align:left" >Job Date</th>
									<th style="text-align:left" >Driver</th><th style="text-align:left" >Vehicle</th>
									<th style="text-align:left" >Pax</th><th style="text-align:left" >From</th><th style="text-align:left" >Via</th><th style="text-align:left" >To</th><th style="text-align:left" >Via</th>
									<th style="text-align:left" >Driver Price</th><th style="text-align:left" >Extra Pay</th><th style="text-align:left" >Driver Fee</th></tr>';
					$i=0;
					foreach($rows as $row)
					{
						if($i%2== 0)
						{
							$htmlstr .= "<tr style='background-color:#F0F0F0;'>
									<td style='width:50px;' ><a href='http://dev.bookings-global.com.au/Forms/show_booking_details.php?job_id=".$row['job_id']."' target='_blank' >".$row['job_id']."</a></td><td style='width:80px;' >".$row['date_time']."</td><td style='width:90px;' >".$row['driver']."</td><td style='width:60px;' >".$row['vehicle_type']."</td><td>".$row['pax']."</td>
									<td>".$row['from_address']."</td><td>".$row['from_via_address']."</td><td>".$row['to_address']."</td><td style='width:50px;' >".$row['to_via_address']."</td><td style='width:50px;' >".$row['driver_price']."</td>
									<td style='width:50px;' >".$row['extra_pay']."</td><td style='width:50px;' >".$row['driver_fee']."</td></tr>";
						}
						else
						{
							$htmlstr .= "<tr>
									<td style='width:50px;' ><a href='http://dev.bookings-global.com.au/Forms/show_booking_details.php?job_id=".$row['job_id']."' target='_blank' >".$row['job_id']."</a></td><td style='width:80px;' >".$row['date_time']."</td><td style='width:90px;' >".$row['driver']."</td><td style='width:60px;' >".$row['vehicle_type']."</td><td>".$row['pax']."</td>
									<td>".$row['from_address']."</td><td>".$row['from_via_address']."</td><td>".$row['to_address']."</td><td style='width:50px;' >".$row['to_via_address']."</td><td style='width:50px;' >".$row['driver_price']."</td>
									<td style='width:50px;' >".$row['extra_pay']."</td><td style='width:50px;' >".$row['driver_fee']."</td></tr>";
						}
						$i++;
						
					}
				}
				else if($_SESSION['ROLE_ID'] == '2')
				{
					
					$htmlstr = '<table width="100%">
								<tr style="background-color:#EAF9FC;">
									<th style="text-align:left" >Job Id</th><th style="text-align:left" >Job Date</th><th style="text-align:left" >Vehicle</th><th style="text-align:left" >Pax</th>
									<th style="text-align:left" >From</th><th style="text-align:left" >Via</th><th style="text-align:left" >To</th><th style="text-align:left" >Via</th>
									<th style="text-align:left" >Driver</th><th style="text-align:left" >Driver Status</th><th style="text-align:left" >Driver Price</th><th style="text-align:left" >Extra Pay</th><th style="text-align:left" >Driver Fee</th></tr>';
					$i=0;
					foreach($rows as $row)
					{
						if($i%2== 0) 
						{
							$htmlstr .= "<tr style='background-color:#F0F0F0;'>
									<td>".$row['job_id']."</td><td style='width:5%'>".$row['date_time']."</td><td style='width:5%'>".$row['vehicle_type']."</td><td style='width:5%'>".$row['pax']."</td>
									<td style='width:10%'>".$row['from_address']."</td><td style='width:2%'>".$row['from_via_address']."</td><td style='width:10%'>".$row['to_address']."</td><td style='width:2%'>".$row['to_via_address']."</td>
									<td style='width:10%'>".$row['driver']."</td><td style='width:10%'>".$row['driver_status']."</td><td style='width:10%'>".$row['driver_notes']."</td><td style='width:2%'>".$row['driver_price']."</td>
									<td>".$row['extra_pay']."</td><td>".$row['driver_fee']."</td></tr>";
						}
						else
						{
							$htmlstr .= "<tr>
									<td>".$row['job_id']."</td><td style='width:5%'>".$row['date_time']."</td><td style='width:5%'>".$row['vehicle_type']."</td><td style='width:5%'>".$row['pax']."</td>
									<td style='width:10%'>".$row['from_address']."</td><td style='width:2%'>".$row['from_via_address']."</td><td style='width:10%'>".$row['to_address']."</td><td style='width:2%'>".$row['to_via_address']."</td>
									<td style='width:10%'>".$row['driver']."</td><td style='width:10%'>".$row['driver_status']."</td><td style='width:10%'>".$row['driver_notes']."</td><td style='width:2%'>".$row['driver_price']."</td>
									<td>".$row['extra_pay']."</td><td>".$row['driver_fee']."</td></tr>";
						}
						$i++;
									
					}
				}
				else
				{
					$htmlstr = '<table width="100%" >
								<tr style="background-color:#EAF9FC;"><th style="text-align:left" >Job Id</th><th style="text-align:left" >Job Date</th>
									<th style="text-align:left" >Vehicle</th>
									<th style="text-align:left" >Pax</th><th style="text-align:left" >From</th><th style="text-align:left" >Via</th><th style="text-align:left" >To</th><th style="text-align:left" >Via</th></tr>';
					$i=0;
					foreach($rows as $row)
					{
						if($i%2== 0)
						{
							$htmlstr .= "<tr style='background-color:#F0F0F0;'>
									<td><a href='http://dev.bookings-global.com.au/Forms/show_booking_details.php?job_id=".$row['job_id']."' target='_blank' >".$row['job_id']."</a></td><td>".$row['date_time']."</td><td>".$row['vehicle_type']."</td><td>".$row['pax']."</td>
									<td>".$row['from_address']."</td><td>".$row['from_via_address']."</td><td>".$row['to_address']."</td><td>".$row['to_via_address']."</td></tr>";
						}
						else
						{
							$htmlstr .= "<tr>
									<td><a href='http://dev.bookings-global.com.au/Forms/show_booking_details.php?job_id=".$row['job_id']."' target='_blank' >".$row['job_id']."</a></td><td>".$row['date_time']."</td><td>".$row['vehicle_type']."</td><td>".$row['pax']."</td>
									<td>".$row['from_address']."</td><td>".$row['from_via_address']."</td><td>".$row['to_address']."</td><td>".$row['to_via_address']."</td></tr>";
						}
						$i++;
									
					}
				}
				$htmlstr .= "</table>";
				
				return $htmlstr;
			}
/*---------------------------------------------------------------------------------------------------------------------*/	

		public function getPDFReportOfDriver($rows)
			{
				if($_SESSION['ROLE_ID'] == '1')
				{
					$htmlstr = '<table style="width:100%" >
								<tr style="background-color:#EAF9FC;"><th style="text-align:left" >Job Id</th><th style="text-align:left" >Job Date</th>
									<th style="text-align:left" >Driver</th><th style="text-align:left" >Vehicle</th>
									<th style="text-align:left" >Pax</th><th style="text-align:left" >From</th><th style="text-align:left" >Via</th><th style="text-align:left" >To</th><th style="text-align:left" >Via</th>
									<th style="text-align:left" >Driver Price</th><th style="text-align:left" >Extra Pay</th><th style="text-align:left" >Driver Fee</th></tr>';
					$i=0;
					foreach($rows as $row)
					{
						if($i%2== 0)
						{
							$htmlstr .= "<tr style='background-color:#F0F0F0;'>
									<td style='width:50px;' ><a href='http://dev.bookings-global.com.au/Forms/show_booking_details.php?job_id=".$row['job_id']."' target='_blank' >".$row['job_id']."</a></td><td style='width:80px;' >".$row['date_time']."</td><td style='width:90px;' >".$row['driver']."</td><td style='width:60px;' >".$row['vehicle_type']."</td><td>".$row['pax']."</td>
									<td>".$row['from_address']."</td><td>".$row['from_via_address']."</td><td>".$row['to_address']."</td><td style='width:50px;' >".$row['to_via_address']."</td><td style='width:50px;' >".$row['driver_price']."</td>
									<td style='width:50px;' >".$row['extra_pay']."</td><td style='width:50px;' >".$row['driver_fee']."</td></tr>";
						}
						else
						{
							$htmlstr .= "<tr>
									<td style='width:50px;' ><a href='http://dev.bookings-global.com.au/Forms/show_booking_details.php?job_id=".$row['job_id']."' target='_blank' >".$row['job_id']."</a></td><td style='width:80px;' >".$row['date_time']."</td><td style='width:90px;' >".$row['driver']."</td><td style='width:60px;' >".$row['vehicle_type']."</td><td>".$row['pax']."</td>
									<td>".$row['from_address']."</td><td>".$row['from_via_address']."</td><td>".$row['to_address']."</td><td style='width:50px;' >".$row['to_via_address']."</td><td style='width:50px;' >".$row['driver_price']."</td>
									<td style='width:50px;' >".$row['extra_pay']."</td><td style='width:50px;' >".$row['driver_fee']."</td></tr>";
						}
						$i++;
						
					}
				}
				else if($_SESSION['ROLE_ID'] == '2')
				{
					
					$htmlstr = '<table style="width:100%" >
								<tr style="background-color:#EAF9FC;">
									<th style="text-align:left" >Job Id</th><th style="text-align:left" >Job Date</th><th style="text-align:left" >Vehicle</th><th style="text-align:left" >Pax</th>
									<th style="text-align:left" >From</th><th style="text-align:left" >Via</th><th style="text-align:left" >To</th><th style="text-align:left" >Via</th>
									<th style="text-align:left" >Driver</th><th style="text-align:left" >Driver Status</th><th style="text-align:left" >Driver Price</th><th style="text-align:left" >Extra Pay</th><th style="text-align:left" >Driver Fee</th></tr>';
					$i=0;
					foreach($rows as $row)
					{
						if($i%2== 0) 
						{
							$htmlstr .= "<tr style='background-color:#F0F0F0;'>
									<td>".$row['job_id']."</td><td style='width:5%'>".$row['date_time']."</td><td style='width:5%'>".$row['vehicle_type']."</td><td style='width:5%'>".$row['pax']."</td>
									<td style='width:10%'>".$row['from_address']."</td><td style='width:2%'>".$row['from_via_address']."</td><td style='width:10%'>".$row['to_address']."</td><td style='width:2%'>".$row['to_via_address']."</td>
									<td style='width:10%'>".$row['driver']."</td><td style='width:10%'>".$row['driver_status']."</td><td style='width:10%'>".$row['driver_notes']."</td><td style='width:2%'>".$row['driver_price']."</td>
									<td>".$row['extra_pay']."</td><td>".$row['driver_fee']."</td></tr>";
						}
						else
						{
							$htmlstr .= "<tr>
									<td>".$row['job_id']."</td><td style='width:5%'>".$row['date_time']."</td><td style='width:5%'>".$row['vehicle_type']."</td><td style='width:5%'>".$row['pax']."</td>
									<td style='width:10%'>".$row['from_address']."</td><td style='width:2%'>".$row['from_via_address']."</td><td style='width:10%'>".$row['to_address']."</td><td style='width:2%'>".$row['to_via_address']."</td>
									<td style='width:10%'>".$row['driver']."</td><td style='width:10%'>".$row['driver_status']."</td><td style='width:10%'>".$row['driver_notes']."</td><td style='width:2%'>".$row['driver_price']."</td>
									<td>".$row['extra_pay']."</td><td>".$row['driver_fee']."</td></tr>";
						}
						$i++;
									
					}
				}
				else
				{
					$htmlstr = '<table style="width:100%" >
								<tr style="background-color:#EAF9FC;"><th style="text-align:left" >Job Id</th><th style="text-align:left" >Job Date</th>
									<th style="text-align:left" >Vehicle</th>
									<th style="text-align:left" >Pax</th><th style="text-align:left" >From</th><th style="text-align:left" >Via</th><th style="text-align:left" >To</th><th style="text-align:left" >Via</th></tr>';
					$i=0;
					foreach($rows as $row)
					{
						if($i%2== 0)
						{
							$htmlstr .= "<tr style='background-color:#F0F0F0;'>
									<td><a href='http://dev.bookings-global.com.au/Forms/show_booking_details.php?job_id=".$row['job_id']."' target='_blank' >".$row['job_id']."</a></td><td>".$row['date_time']."</td><td>".$row['vehicle_type']."</td><td>".$row['pax']."</td>
									<td>".$row['from_address']."</td><td>".$row['from_via_address']."</td><td>".$row['to_address']."</td><td>".$row['to_via_address']."</td></tr>";
						}
						else
						{
							$htmlstr .= "<tr>
									<td><a href='http://dev.bookings-global.com.au/Forms/show_booking_details.php?job_id=".$row['job_id']."' target='_blank' >".$row['job_id']."</a></td><td>".$row['date_time']."</td><td>".$row['vehicle_type']."</td><td>".$row['pax']."</td>
									<td>".$row['from_address']."</td><td>".$row['from_via_address']."</td><td>".$row['to_address']."</td><td>".$row['to_via_address']."</td></tr>";
						}
						$i++;
									
					}
				}
				$htmlstr .= "</table>";
				
				return $htmlstr;
			

			}
			
		public function getJob_driver_details($j1_id)
		{
			$database = new database;
			$query1 = "SELECT * FROM job__driver where id=(SELECT MAX(id) FROM job__driver WHERE job_id ='".$j1_id."' AND  (allocated_to <> '0' || accepted_by <>0) ) ";
			$result1 = $database->query($query1);
			$row = mysql_fetch_array($result1);
			return $row;
		}
		public function get_Job_driver_extra_pay($job_id)
		{
			$database = new database;
			$query1 = "select sum(amount) as extra_pay from job__driver_extra_pay where job_id = '".$job_id."'";
			$result1 = $database->query($query1);
			$row = mysql_fetch_array($result1);
			return $row;
		}
		
		public function addJobPaymentStatusTable($job_id, $payment_status_id, $changed_by)
			{
				$database = new database;
				$q = "INSERT INTO job__current_payment_status (job_id, payment_status_id, changed_by) VALUES
						(
							'".$job_id."',
							'".$reminder_status_id."',
							'".$changed_by."'
						)";
				
				$database = new database;
				$result = $database->query($q);
			}
			
		public function getHTMLBookingList($rows)
			{

				$htmlstr = '<table style="width:100%;" >
							<tr style="background-color:#EAF9FC;">
								<th>Job Id</th>
								<th>Bkg Status</th>
								<th>Driver Status</th>
								<th>Driver</th>
								<th>State</th>
								<th>Bkg Time</th>
								<th>Vehicle</th>
								<th>Pax Name</th>
								<th>Flight</th>
								<th>From</th>
								<th>Via</th>
								<th>DROP</th>
								<th>LAST DROP</th>
								<th>Ext Note</th>
								<th>Int Note</th>
								<th>Bkg By</th>
								<th>Acc Name</th>
								<th>Std ID</th>
								<th>Std Name</th>
								<th>Pax#</th>
								<th>Lug</th>
								<th>BC</th>
								<th>BS</th>
								<th>B</th>
								<th>Method</th>
								<th>Entered By</th>
								<th>Driver Notes</th>
							</tr>';
					
				$i=0;
				foreach($rows as $row)
				{
					if($i%2== 0)
						{
							$htmlstr .= "<tr style='background-color:#F0F0F0;'>
										<td>".$row['job_id']."</td>
										<td>".$row['current_booking_status']."</td>
										<td>".$row['driver_status']."</td>
										<td style='width:50px;'>".$row['driver']."</td>
										<td>".$row['frm_state']."</td>
										<td style='width:50px;'>".$row['date_time']."</td>
										<td>".$row['vehicle_type']."</td>
										<td style='width:50px;'>".$row['pax']."</td>
										<td>".$row['frm_flight_no']."</td>
										<td style='width:50px;'>".$row['from_address']."</td>
										<td>".$row['from_via_address']."</td>
										<td style='width:50px;'>".$row['to_address']."</td>
										<td>".$row['to_via_address']."</td>
										<td style='width:50px;'>".$row['ext_notes']."</td>
										<td style='width:50px;'>".$row['int_notes']."</td>
										<td style='width:50px;'>".$row['bkg_by']."</td>
										<td style='width:50px;'>".$row['account_name']."</td>
										<td>".$row['std_id']."</td>
										<td style='width:50px;'>".$row['student_name']."</td>
										<td>".$row['pax_nos']."</td>
										<td>".$row['luggage']."</td>
										<td>".$row['baby_capsule']."</td>
										<td>".$row['baby_seat']."</td>
										<td>".$row['booster_seat']."</td>
										<td>".$row['job_src']."</td>
										<td>".$row['entry_by']."</td>
										<td>".$row['driver_notes']."</td>
										</tr>";
						}
						else
						{
							$htmlstr .= "<tr>
								<td>".$row['job_id']."</td>
								<td>".$row['current_booking_status']."</td>
								<td>".$row['driver_status']."</td>
								<td>".$row['driver']."</td>
								<td>".$row['frm_state']."</td>
								<td>".$row['date_time']."</td>
								<td>".$row['vehicle_type']."</td>
								<td>".$row['pax']."</td>
								<td>".$row['frm_flight_no']."</td>
								<td>".$row['from_address']."</td>
								<td>".$row['from_via_address']."</td>
								<td>".$row['to_address']."</td>
								<td>".$row['to_via_address']."</td>
								<td>".$row['ext_notes']."</td>
								<td>".$row['int_notes']."</td>
								<td>".$row['bkg_by']."</td>
								<td>".$row['account_name']."</td>
								<td>".$row['std_id']."</td>
								<td>".$row['student_name']."</td>
								<td>".$row['pax_nos']."</td>
								<td>".$row['luggage']."</td>
								<td>".$row['baby_capsule']."</td>
								<td>".$row['baby_seat']."</td>
								<td>".$row['booster_seat']."</td>
								<td>".$row['job_src']."</td>
								<td>".$row['entry_by']."</td>
								<td>".$row['driver_notes']."</td>
								</tr>";
						}
				}
				$i++;
				$htmlstr .= "</table>";
				
				return $htmlstr;
			}
			
/*---------------------------------------------------------------------------------------------------------------------*/
		public function addNewCollectiveBooking($job_ids, $job_driver_id, $driver_id) // These are the common values for bookings
			{
				$q = "INSERT INTO collective_booking (job_ids, driver_id, job_driver_id)
						VALUES('$job_ids', '$driver_id', '$job_driver_id')";
				$database = new database;
				$result = $database->query($q);
				$id = mysql_insert_id();
				return $id;
			}
/*---------------------------------------------------------------- notification function  08-08-2016 ---------------------------------*/
	public function commonGooglePushNotification($user_id,$message,$title)
	{
		$url = 'https://android.googleapis.com/gcm/send'; 
		
		//$registration_id = "APA91bGQ87KWJTeEnOkZGsT_J4kBPvfDA2p4KFIVz74UXgjbz3In0bG-JxTZp_VPPMSWEm6RQ7u_1dMzUsfNaPOeNox2gLQXtq1D4-HbgR5h03zH3DoKW_pVBtaFDNa8IAnoyP3vWnKKG4xCCP1QBsTLaY5ozVjdykniqLCTpr0Spj31Xfv6fciu8dAvZCaQZ0pGl2mrUWMg";        
		$q = "SELECT * FROM user WHERE id='".$user_id."'";
		$database = new database;
		$result = $database->query($q);
		$row = mysql_fetch_array($result);
		$registration_id = $row['device_token'];
			if($registration_id!='')
			{
				//$registration_id = '90ef590185c302d2';  
				$message =$message;      
					 $reg_id = array($registration_id);
					  $fields = array(
						'registration_ids' =>  $reg_id,  
						'data' => array(
										"title"		=> $title,
										"message" 	=> $message,
										'vibrate'   => 2,
										'sound'     => 3)
					);   
					 $headers = array(
						'Authorization: key=' . 'AIzaSyBf1FhLdXROfafCkUqUkCCt9I_ASzPvA4I',                         
						'Content-Type: application/json'
					);
					$ch = curl_init();  
			   
					// Set the url, number of POST vars, POST data
					curl_setopt($ch, CURLOPT_URL, $url);	
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 ); 
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			
					// Disabling SSL Certificate support temporarly
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					
					//curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
					
					curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
					 $result = curl_exec($ch);
					
					if ($result === FALSE) {
						die('Curl failed: ' . curl_error($ch)); exit;  
					} 
					//echo $result;          
					curl_close($ch); 
			}
		}
		//address to latlong 08-08-2016
		public function getlatlong($address)
		{
			$url = 'http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false';
			$json = file_get_contents($url);
			$data=json_decode($json);
			//print_r($data);
			$status = $data->status;
			$address_of_latlong=array();
			if($status=="OK")
			{
				$address_of_latlong['lat']=$data->results[0]->geometry->location->lat;
				$address_of_latlong['lang']=$data->results[0]->geometry->location->lng;
				return $address_of_latlong;
			}
			else
			{
				return false;
			}
		}
}