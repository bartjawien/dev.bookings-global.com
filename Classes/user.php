<?php
require_once(CLASSES_PATH . "database.php");
class User
	{
/*---------------------------------------------------------------------------------------------------------------------*/
		public function getUserDetails($id)
			{
				$q = "SELECT * from user where id='$id'";
				$database = new database;
				$result = $database->query($q);
				$x = mysql_fetch_assoc($result);
				$x['fname'] =htmlspecialchars_decode($x['fname']);
				$x['lname'] =htmlspecialchars_decode($x['lname']);
				return $x;
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function getUserAddresses($user_id, $order_by)
			{
				$q = "SELECT * FROM user__address where user_id = $user_id ORDER by $order_id";
				$database = new database;
				$result = $database->query($q);
				$x = mysql_fetch_assoc($result);
				return $x;
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function getUserAddress($address_id)
			{
				$q = "SELECT * FROM user__address where id = '$address_id'";
				$database = new database;
				$result = $database->query($q);
				$x = mysql_fetch_assoc($result);
				return $x;
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function getUserPhones($user_id)
			{
				$q = "SELECT * FROM user__phone where user_id = '$user_id'";
				$database = new database;
				$result = $database->query($q);
				$x = mysql_fetch_assoc($result);
				return $x;
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function addUser($role_id, $title, $fname, $lname, $password, $email, $mobile, $phone)
			{
				$fname = htmlspecialchars($fname,ENT_NOQUOTES);
				$lname = htmlspecialchars($lname, ENT_NOQUOTES);
				$q = "INSERT INTO user(id, created_on, role_id, title, fname, lname, password, email, mobile, phone, preference)
						VALUES(NULL, CURRENT_TIMESTAMP, '$role_id', '$title', '$fname', '$lname', '$password', '$email', '$mobile', '$phone', '$preference')";
				$database = new database;
				$result = $database->query($q);
				$id = mysql_insert_id();
				return $id;
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function addUserAddress($user_id, $type_id, $line1, $line2, $sub, $postcode, $state)
			{
				$q = "INSERT INTO user__address(id, created_on, user_id, type_id, line1, line2, sub, postcode, state)
						VALUES(NULL, CURRENT_TIMESTAMP, '$user_id', '$type_id', '$line1', '$line2', '$sub', '$postcode', '$state')";
				$database = new database;
				$result = $database->query($q);
				$id = mysql_insert_id();
				return $id;
			}	
/*---------------------------------------------------------------------------------------------------------------------*/
		public function deleteUser($id)
			{
				$database = new database;
				$q1 = "DELETE FROM user WHERE id = ".$id."";
				$result1 = $database->query($q);
				$q2 = "DELETE FROM charge_acc__contacts WHERE user_id = ".$id."";
				$result2 = $database->query($q2);
				
				$this->deleteAllUserAddresses($id);
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function deleteAUserAddress($address_id)
			{
				$database = new database;
				$q = "DELETE FROM user__address WHERE id = ".$address_id."";
				$result = $database->query($q);
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function deleteAllUserAddresses($user_id)
			{
				$database = new database;
				$q = "DELETE FROM user__address WHERE user_id = ".$user_id."";
				$result = $database->query($q);
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function updateChargeAccountContact($id, $update_column, $new_value)
			{
				$database = new database;
				$q = "UPDATE charge_acc__contacts SET 
						".$update_column."	= '".$new_value."'
						WHERE user_id 			= ".$id."";
				$result = $database->query($q);
				
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function updateUser($id, $update_column, $new_value)
			{
				$database = new database;
				if($update_column == 'fname')
				{
					$new_value = htmlspecialchars($new_value, ENT_NOQUOTES);
				}
				if($update_column == 'lname')
				{
					$new_value = htmlspecialchars($new_value, ENT_NOQUOTES);
				}
				$q = "UPDATE user SET 
						".$update_column."	= '".$new_value."'
						WHERE id 			= ".$id."";
				$result = $database->query($q);
				
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function updateUserAddress($id, $update_column, $new_value)
			{
				$database = new database;
				$q = "UPDATE user__address SET 
						".$update_column."	= '".$new_value."'
						WHERE id 			= ".$id."";
				$result = $database->query($q);
				
			}
/*---------------------------------------------------------------------------------------------------------------------*/

public function getHTMLDriverList($rows)
			{

				$htmlstr = '<table style="width:100%;" >
							<tr style="background-color:#EAF9FC;">
								<th style="text-align:left" >Id</th>
								<th style="text-align:left" >Title</th>
								<th style="text-align:left" >Fname</th>
								<th style="text-align:left" >Lname</th>
								<th style="text-align:left" >Password</th>
								<th style="text-align:left" >Email</th>
								<th style="text-align:left" >Mobile</th>
								<th style="text-align:left" >Phone</th>
								<th style="text-align:left" >Notes</th>
								<th style="text-align:left" >Acc Bal</th>
								<th style="text-align:left" >Visibility</th>
							</tr>';
					
				$i=0;
				foreach($rows as $row)
				{
					if($i%2== 0)
						{
							$htmlstr .= "<tr style='background-color:#F0F0F0;'>
											<td>".$row['id']."</td>
											<td>".$row['title']."</td>
											<td>".$row['fname']."</td>
											<td>".$row['lname']."</td>
											<td style='width:50px;'>".$row['password']."</td>
											<td style='width:50px;'>".$row['email']."</td>
											<td>".$row['mobile']."</td>
											<td>".$row['phone']."</td>
											<td style='width:150px;'>".$row['notes']."</td>
											<td>".$row['acc_bal']."</td>
											<td>".(($row['hidden'] == 0)?'Current':'Left')."</td>
										</tr>";
						}
						else
						{
							$htmlstr .= "<tr>
											<td>".$row['id']."</td>
											<td>".$row['title']."</td>
											<td>".$row['fname']."</td>
											<td>".$row['lname']."</td>
											<td>".$row['password']."</td>
											<td>".$row['email']."</td>
											<td>".$row['mobile']."</td>
											<td>".$row['phone']."</td>
											<td>".$row['notes']."</td>
											<td>".$row['acc_bal']."</td>
											<td>".(($row['hidden'] == 0)?'Current':'Left')."</td>
										</tr>";
						}
				}
				$i++;
				$htmlstr .= "</table>";
				
				return $htmlstr;
			}
			
			public function getHTMLAdminList($rows)
			{

				$htmlstr = '<table style="width:100%;" >
							<tr style="background-color:#EAF9FC;">
								<th style="text-align:left" >Id</th>
								<th style="text-align:left" >Title</th>
								<th style="text-align:left" >Fname</th>
								<th style="text-align:left" >Lname</th>
								<th style="text-align:left" >Password</th>
								<th style="text-align:left" >Email</th>
								<th style="text-align:left" >Mobile</th>
								<th style="text-align:left" >Phone</th>
								<th style="text-align:left" >Visibility</th>
							</tr>';
					
				$i=0;
				foreach($rows as $row)
				{
					if($i%2== 0)
						{
							$htmlstr .= "<tr style='background-color:#F0F0F0;'>
											<td>".$row['id']."</td>
											<td>".$row['title']."</td>
											<td>".$row['fname']."</td>
											<td>".$row['lname']."</td>
											<td style='width:50px;'>".$row['password']."</td>
											<td style='width:50px;'>".$row['email']."</td>
											<td>".$row['mobile']."</td>
											<td>".$row['phone']."</td>
											<td>".(($row['hidden'] == 0)?'Current':'Left')."</td>
										</tr>";
						}
						else
						{
							$htmlstr .= "<tr>
											<td>".$row['id']."</td>
											<td>".$row['title']."</td>
											<td>".$row['fname']."</td>
											<td>".$row['lname']."</td>
											<td>".$row['password']."</td>
											<td>".$row['email']."</td>
											<td>".$row['mobile']."</td>
											<td>".$row['phone']."</td>
											<td>".(($row['hidden'] == 0)?'Current':'Left')."</td>
										</tr>";
						}
				}
				$i++;
				$htmlstr .= "</table>";
				
				return $htmlstr;
			}
			
			public function getAllDriverDetailsOfChar($char)
			{
				$q = "SELECT * from user where fname LIKE '$char%' and fname <>'".$char." 1' and role_id='2'";
				$database = new database;
				$result = $database->query($q);
				$i=0;
				while($x = mysql_fetch_assoc($result))
				{
					$dr_arr[$i]['title'] =$x['title'];
					$dr_arr[$i]['fname'] =htmlspecialchars_decode($x['fname']);
					$dr_arr[$i]['lname'] =htmlspecialchars_decode($x['lname']);
					$dr_arr[$i]['id'] =$x['id'];
					$dr_arr[$i]['email'] =$x['email'];
					$i++;
				}
				return $dr_arr;
			}
	}
?>