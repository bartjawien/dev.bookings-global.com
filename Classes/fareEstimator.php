<?php
class fareEstimator 
	{
/*---------------------------------------------------------------------------------------------------------------------*/
		function getGoogleDistanceTime($start, $finish, $raw = false)
			{
				if(strcmp($start, $finish) == 0)
					{
						$time = 0;
						if($raw)
							{
								$time .= ' seconds';
							}
						return array('distance' => 0, 'time' => $time);
					}
				$start  = urlencode($start);
				$finish = urlencode($finish);
				$distance   = 'unknown';
				$time       = 'unknown';
				$url = 'http://maps.googleapis.com/maps/api/directions/xml?origin='.urlencode($start).'&destination='.urlencode($finish).'&sensor=false';
				if($data = file_get_contents($url))
					{
						
						$xml = new SimpleXMLElement($data);
						if(isset($xml->route->leg->duration->value) AND (int)$xml->route->leg->duration->value > 0)
							{
								if($raw)
									{
										$distance = (string)$xml->route->leg->distance->text;
										$time     = (string)$xml->route->leg->duration->text;
									}
								else
									{
										$distance = (int)$xml->route->leg->distance->value / 1000; 
										$time     = (int)$xml->route->leg->duration->value/60;
									}
								
							}
						else
							{
								$distance 	= 0;
								$time	 	= 0;
								$message	= 'Cannot Calculate distance and time';
								return array('distance' => $distance, 'time' => $time, 'message' =>$message);
							}
					$message = 'distance and time calculated';
					return array('distance' => $distance, 'time' => $time, 'message' =>$message);
					}
				else
				{
					$distance 	= 0;
					$time	 	= 0;
					$message	= 'Cannot resolve URL';
					return array('distance' => $distance, 'time' => $time, 'message' =>$message);
				}
			}
/*---------------------------------------------------------------------------------------------------------------------*/
	}
?>