<?php
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "user.php");
class chargeAccount
	{
/*---------------------------------------------------------------------------------------------------------------------*/
		public function getChargeAccountDetails($id)
			{
				$q = "SELECT * from charge_acc where id='$id'";
				$database = new database;
				$result = $database->query($q);
				$x = mysql_fetch_assoc($result);
				return $x;
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function getChargeAccountContacts($charge_acc_id, $order_by)
			{
				$q = "SELECT * from
					charge_acc__contacts, user
					where 
					charge_acc__contacts.user_id = user.id
					AND  charge_acc__contacts.charge_acc_id='".$charge_acc_id."'
					order by ".$order_by."";
				$database = new database;
				$result = $database->query($q);
				$x = mysql_fetch_assoc($result);
				return $x;
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function addChargeAccount($created_by, $acc_type, $account_name, $myob_no)
			{
				$q = "INSERT INTO charge_acc(id, created_on, created_by, acc_type, account_name, myob_no)
						VALUES(NULL, CURRENT_TIMESTAMP, '$created_by', '$acc_type', '$account_name', '$myob_no')";
				$database = new database;
				$result = $database->query($q);
				$id = mysql_insert_id();
				return $id;
			}
			
/*---------------------------------------------------------------------------------------------------------------------*/
		public function addChargeAccountContact($title, $fname, $lname, $password, $email, $mobile, $phone, $preference, $charge_acc_id, $type_id)
			{
				$database = new database;
				$user = new user();
				$q ="INSERT INTO user(id, created_on, role_id, title, fname, lname, password, email, mobile, phone, preference) 
						VALUES(NULL, CURRENT_TIMESTAMP, '3', '$title', '$fname', '$lname', '$password', '$email', '$mobile', '$phone', '$preference')";
				$result = $database->query($q);
				$new_user_id = mysql_insert_id();
				
				
				$q = "INSERT INTO charge_acc__contacts(id, charge_acc_id,user_id,type_id)
						VALUES(NULL, '$charge_acc_id', '$new_user_id', '$type_id')";
				$result = $database->query($q);
				return $new_user_id;
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function deleteChargeAccount($id)
			{
				$q = "DELETE FROM charge_acc WHERE id = ".$id."";
				$database = new database;
				$result = $database->query($q);
				$id = mysql_insert_id();
				return $id;
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function deleteChargeAccountContact($user_id, $charge_acc_id)
			{
				$database = new database;
				$q = "DELETE FROM user WHERE id = ".$user_id."";
				$result = $database->query($q);
				$q = "DELETE FROM charge_acc__contacts WHERE charge_acc_id = ".$charge_acc_id." AND user_id = ".$user_id."";
				$result = $database->query($q);
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function updateChargeAccount($charge_acc_id, $update_column, $new_value)
			{
				$database = new database;
				$q = "UPDATE charge_acc SET 
						".$update_column."		= '".$new_value."'
						WHERE id 	= ".$charge_acc_id."";
				$result = $database->query($q);
				
			}
/*---------------------------------------------------------------------------------------------------------------------*/
		public function updateChargeAccountContact($row_id, $update_column, $new_value)
			{
				$database = new database;
				$q = "UPDATE charge_acc__contacts SET 
						".$update_column."		= '".$new_value."'
						WHERE id 	= ".charge_acc_id."";
				$result = $database->query($q);
				
			}
/*----------------------------------------------------------------------------------------------------------------------*/
		public function getAllChargeAccountContacts($charge_acc_id)
			{
				$q = "SELECT * from
					charge_acc__contacts, user
					where 
					charge_acc__contacts.user_id = user.id
					AND  charge_acc__contacts.charge_acc_id='".$charge_acc_id."'";
					
				$database = new database;
				$result = $database->query($q);
				while($row1 = mysql_fetch_assoc($result))	
				{
					$rows[] = $row1;
				}
				return $rows;
			}

/*---------------------------------------------------------------------------------------------------------------------------*/
public function getChargeAccountDetailsByName($name)
			{
				$q = "SELECT * from charge_acc where account_name LIKE '%".mysql_real_escape_string($name)."%' order by account_name limit 0, 20";
				$database = new database;
				$result = $database->query($q);
				$return_arr = array();
				while($row = mysql_fetch_array($result, MYSQL_ASSOC))	
					{			
						$row_array['label']  = "".$row['account_name']."";
						$row_array['val'] = $row['id'];
						
						array_push( $return_arr, $row_array );
					}
					return $return_arr;
			}
			
			public function getHTMLAccountList($rows)
			{

				$htmlstr = '<table style="width:100%;" >
							<tr style="background-color:#EAF9FC;">
								<th style="text-align:left" >Id</th>
								<th style="text-align:left" >Account Name</th>
								<th style="text-align:left" >Account Type</th>
								<th style="text-align:left" >MYOB NO</th>
								<th style="text-align:left" >Created BY</th>
							</tr>';
					
				$i=0;
				foreach($rows as $row)
				{
					if($i%2== 0)
						{
							$htmlstr .= "<tr style='background-color:#F0F0F0;'>
											<td>".$row['id']."</td>
											<td>".$row['account_name']."</td>
											<td>".$row['acc_type']."</td>
											<td>".$row['myob_no']."</td>
											<td>".$row['created_by']."</td>
										</tr>";
						}
						else
						{
							$htmlstr .= "<tr>
											<td>".$row['id']."</td>
											<td>".$row['account_name']."</td>
											<td>".$row['acc_type']."</td>
											<td>".$row['myob_no']."</td>
											<td>".$row['created_by']."</td>
										</tr>";
						}
				}
				$i++;
				$htmlstr .= "</table>";
				
				return $htmlstr;
			}
			
	}
?>