<?php
require 'phpmailer/PHPMailerAutoload.php';
require_once("phpmailer/class.phpmailer.php");
require_once(CLASSES_PATH . "job.php");
require_once(CLASSES_PATH . "chargeAccount.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "jobReference.php");
include(''.INCLUDE_PATH.'functions_date_time.php');

/*
//FOR BOOKINGS	
	
		public function sendBookingReceiptToSomeone($job_reference_id, $receiver_email, $receiver_name)
		
		public function sendMailToChargeAccountsOnBooking($job_reference_id)
		
		public function sendMailToClientOnBooking($job_reference_id, $email_type, $receiver_email, $receiver_title, $receiver_fname, $receiver_lname)

		public function sendMailToClientForIndividualBooking($job_id, $email_type, $receiver_email, $receiver_title, $receiver_fname, $receiver_lname)

		public function sendMailToAdminOnBooking($job_reference_id)
		
		public function sendJobOfferToDriver($driver_id, $job_id, $offer_amount, $driver_notes='')
		
		public function sendJobDetailsToDriver($job_id, $driver_id, $job_price, $driver_notes='')

//FOR DRIVERS

		public function mass_email($first_char, $subject, $body)
		
		public function sendCollectiveJobDetailsToDriver($job_ids, $driver_id, $job_price, $driver_notes='')

		public function sendBulkJobOfferToADriver($driver_id, $jobs_array)

		public function sendBulkJobDetailsToADriver($driver_id, $unique_code)

FOR INVOICE
		
		public function sendInvoice($from, $fromname, $to_email, $to_title, $to_fname, $to_lname,$this_invoice_id, $mail_body)

//FOR UNIVERSITIES		
				
		public function sendMailToMonashClaytonStudent($job_reference_id, $email_type, $receiver_email, $receiver_title, $receiver_fname, $receiver_lname)
				
		public function sendMailToMonashCollegeStudent($job_reference_id, $email_type, $receiver_email, $receiver_title, $receiver_fname, $receiver_lname)
				
		public function sendMailToMonashAbroadStudent($job_reference_id, $email_type, $receiver_email, $receiver_title, $receiver_fname, $receiver_lname)
				
		public function sendMailToMelbourneUniversityStudent($job_reference_id, $email_type, $receiver_email, $receiver_title, $receiver_fname, $receiver_lname)
				
		public function sendMailToDeakinUniversityStudent($job_reference_id, $email_type, $receiver_email, $receiver_title, $receiver_fname, $receiver_lname)
		
		public function sendMailToLatrobeUniversityStudent($job_reference_id, $email_type, $receiver_email, $receiver_title, $receiver_fname, $receiver_lname)
		
		public function sendNoShowEmailForUnderEighteenStudents($job_id, $receiver_email, $receivers_name)

		public function sendNoShowEmailForStudents($job_id, $receiver_email, $receivers_name)

		public function sendHomeStayEmailToUniversity($job_id, $university_name, $email_to)
		
*/

class Mailer
	{
/*---------------------------------------------------------------------------------------------------------------------*/	
								
		const DOCUMENT_HEADER 			= 	'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
											<html xmlns="http://www.w3.org/1999/xhtml">
											<head>
												<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
												<title>Booking Confirmation</title>
												<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
											</head>';
											
		const BODY_START				=	'<body style="margin: 0; padding: 0;">';
		const EMAIL_HTML_HEADER 		= 	'<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; background: #eee;">
												<tr>
													<td align="left" width="30%">
														<img src="http://dev.bookings-global.com.au/Images/logo_Allied.png" alt="Allied Cars" height="40%" style="display: block;" />
													</td>
													<td align="right" width="70%">
														<table border="0" cellpadding="5" cellspacing="0" style="font-size:70%;">
															<tr>
																<td colspan="2"><b>Bookings and Support</b></td>
															</tr>
															<tr>
																<td>Phone:</td>
																<td>(+61 3) 8383 9999</td>
															</tr>
															<tr>
																<td>Email:</td>
																<td>alliedcars@alliedcars.com.au</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>';
			const EMAIL_HTML_FOOTER		=	'<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0" style="border-collapse: collapse; border-color: #666; background: #3b0552;">
												<tr>
													<td align="center" style="color: #ffffff; font-family: Arial, sans-serif; font-size: 85%;">
														Copyright &#169; 1997-2014 Allied Chauffeured Cars Australia. All Rights Reserved.
													</td>
												</tr>
												<tr>
													<td align="center" bgcolor="#70bbd9" style="padding: 5px 0 5px 0; font-size: 85%;">
														<a href="http://alliedcars.com.au">Allied Cars Website</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;  
														<a href="http://alliedcars.com.au/email_disclaimer">Email Disclaimer</a>
													</td>
												</tr>
											</table>';
											
			const BODY_END				=	'</body></html>';
			
			const MOBILE_HEADER =	'<!doctype html>
										<html xmlns="http://www.w3.org/1999/xhtml">
											<head>
												<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
												<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
												<title>Allied Cars</title>
												<style type="text/css">
													.ReadMsgBody
														{
															width: 100%;
															background-color: #ffffff;
														}
													.ExternalClass
														{
															width: 100%;
															background-color: #ffffff;
														}
													body 
														{
															width: 100%;
															background-color: #ffffff;
															margin: 0;
															padding: 0;
															-webkit-font-smoothing: antialiased;
															font-family: Arial, Helvetica, sans-serif;
															font-size: 1.3em
														}
													table 
														{
															border-collapse: collapse;
														}
													.center {text-align: center!important;}
													.left {text-align: left!important;}
													.backImg{ width:100%;}
													
													.responsive-image
														{
															width: 100%;
															background-size: 100% 100%;
															background-repeat:no-repeat;
														}
													@media only screen and (max-width: 640px) 
														{
															.deviceWidth {
															width: 100% !important;
															padding: 0;
														}
													@media only screen and (max-width: 479px) 
														{
															.deviceWidth {width: 100% !important;padding: 0;}
															.center {text-align: center!important;}
															.left {text-align: left!important;}
														}
												</style>
											</head>
											<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix">
												<table width="100%" align="center" style="border:2px solid #dddddd;">
													<tr>
														<td>
															<table width="100%" bgcolor="#fafafa" style="border-bottom:2px solid #dddddd; border-spacing: 5px; padding: 5px; text-align:left; font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #687074; line-height: 20px;">
																<tr>
																	<td width="100%" ><!--Start logo-->
																		<table  border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth">
																			<tr>
																				<td class="center" style="padding: 10px 20px 10px 20px"><img src="http://dev.bookings-global.com.au/Images/logo_Allied.png" alt="Allied Cars" height="90%" style="display: block;" /></td>
																			</tr>
																		</table>                  
																		<table  border="0" cellpadding="0" cellspacing="0" align="right" class="deviceWidth">
																			<tr>
																				<td class="left" style="font-size: 14px; color: #272727; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 10px 20px 10px 20px;"> Email: <a href="mailto:alliedcars@alliedcars.com.au" target="_blank" style="color:#00AEEF;">alliedcars@alliedcars.com.au</a></td>
																			<tr/>
																			<tr>
																				<td class="left" style="font-size: 14px; color: #272727; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 10px 20px 10px 20px;"> Website: <a href="http://www.alliedcar.com.au" target="_blank" style="color:#00AEEF;">www.alliedcar.com.au</a></td>
																			</tr>
																			<tr>
																				<td class="left" style="font-size: 14px; color: #272727; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 10px 20px 10px 20px;"> Phone: <a href="+61 3 8383 9999" target="_blank" style="color:#00AEEF;">(+61 3) 8383 9999</a></td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															<table width="100%" border="0" cellpadding="15px" cellspacing="0" align="center" class="deviceWidth" style="background:fff; font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #687074; line-height: 20px;">
																<tbody>
																	<tr>
																		<td>';
																		
																		
			const MOBILE_FOOTER = 										'</td>
																	</tr>
																</tbody>									
															</table>
															<table width="100%" bgcolor="#fafafa"  border="0" cellpadding="10px" cellspacing="0" align="left" class="deviceWidth">
																<tbody>
																	<tr>
																		<td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: justify; font-family: Arial, Helvetica, sans-serif; line-height: 20px; vertical-align: middle; padding: 10px 15px 10px; border-top:1px solid #dddddd;">
																			<b>EMAIL DISCLAIMER :</b><br/>
																			This message contains confidential information and is intended only for the individual named. If you are not the named addressee, you should not disseminate, distribute or copy this email. Please notify the sender immediately by email at alliedcars@alliedcars.com.au. If you have received this email by mistake then delete this email from your system. Email transmission cannot be guaranteed to be secure or error-free, as information could be intercepted, corrupted, lost, destroyed, arrive late or incomplete, or contain viruses. The sender, therefore, does not accept liability for any errors or omissions in the contents of this message which arise as a result of email transmission. Finally, the recipient should check this email and any attachments for the presence of viruses. The organization accepts no liability for any damage caused by any virus transmitted by this email.                        
																		</td>
																	</tr>
																</tbody>
															</table>
															<table width="100%" bgcolor="#fafafa"  border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth"  >
																<tr>
																	<td class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 20px; vertical-align: middle; padding: 10px 15px 10px; border-top:1px solid #dddddd;" >
																		<a href="http://alliedcar.com.au" style="color:#687074;">Allied Cars</a><br/>
																		<img src="http://dev.bookings-global.com.au/Images/logo_Allied.png" alt="Allied Cars" height="60%" style="display: block;" />
																	</td>
																</tr>
																<tr>
																	<td class="center" bgcolor="#272727" style="font-size: 12px; color: #fff; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 10px 50px 10px 50px;" > Copyright © Allied Cars 2015 </td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</body>
										</html>';
		
//----------------------------------------------------------------------------------------------------------------------/					
		public function sendMailToChargeAccountsOnBooking($job_reference_id)
			{
				$mail 	= new PHPMailer;
				$mail->CharSet = "UTF-8";
				$job = new job();
				$job_reference 	= new JobReference();
				$chargeaccounts = new chargeAccount();
				
				$job_reference_details 	= $job_reference->getJobReferenceDetails($job_reference_id);
				
				$booking_html = $job->getHTMLofWholeBooking($job_reference_id);
				$admin_array = $chargeaccounts->getAllChargeAccountContacts($job_reference_details['charge_acc_id']);
				$j1_details = $job->getJobDetails($job_reference_details['j1_id']);

				$mail->From 	= 'alliedcars@alliedcars.com.au';
				$mail->FromName = 'Allied Cars';
				foreach($admin_array AS $admin)
					{
						if($admin['email'] != '')
						{
							$mail 	= new PHPMailer;
							$mail->CharSet = "UTF-8";							
							$mail->From 	= 'alliedcars@alliedcars.com.au';
							$mail->FromName = 'Allied Cars';
							$admin_id = base64_encode(base64_encode($admin['id']));
							  
							//$mail->addAddress('sucheta.testmail@gmail.com', $admin['fname'].' '.$admin['lname']);  // Add a recipient
							$mail->addAddress($admin['email'], $admin['fname'].' '.$admin['lname']);  // Add a recipient
							$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
							$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
							$mail->isHTML(true);                                  // Set email format to HTML
							if($job_reference_details['acc_type'] == '3')
							{
								$mail->Subject = 'Requesting Approval for Booking for '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' '.$job_reference_details['std_id'].' - '.date("l, d F, Y", strtotime($j1_details['job_date']));
							}
							else{
								$mail->Subject = 'Requisition for Booking';
							}
							
							 
							$mail->Body    = self::DOCUMENT_HEADER;
							$mail->Body    .= self::BODY_START;
							$mail->Body    .= self::EMAIL_HTML_HEADER;
							$mail->Body    .= '<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0" style="border-collapse: collapse; border-color: #fff; background: #45cb37;">
													<tr>
														<td align="left" style="color: #ffffff; font-family: Arial, sans-serif; font-size: 200%">
															<strong>Booking Received</strong>
														</td>
													</tr>
												</table>
												<br/>
												<strong>Dear '.$admin['fname'].' '.$admin['lname'].', </strong><br/>
												We have received the following booking request. <br/><br/>';
							$mail->Body		.= $job->getHTMLofWholeBooking($job_reference_id); 
							
							$mail->Body		.= '<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0"><tr><td>';
							$mail->Body		.= "<a href='".ROOT_ADDRESS."external_mail_script/university_booking_approval.php?job_reference_id=".$job_reference_id."&admin_id=".$admin_id."&confirm_status=1'>Approve this Booking</a ><br /><br />";
							$mail->Body		.= "<a href='".ROOT_ADDRESS."external_mail_script/university_booking_approval.php?job_reference_id=".$job_reference_id."&admin_id=".$admin_id."&confirm_status=2'>Decline this booking</a ><br /><br />";
							$mail->Body		.= '</td><tr></table>';
							$mail->Body    .= self::EMAIL_HTML_FOOTER;
							$mail->Body    .= self::BODY_END;
							$mail->send();
						}
					}
			} 	

//----------------------------------------------------------------------------------------------------------------------/		
		public function sendBookingReceiptToSomeone($job_reference_id, $receiver_email, $receiver_name)
			{	
				//initialize classes agents
				
				$mail 			= new PHPMailer;
				$job_reference 	= new JobReference();
				$job 			= new Job();
				$chargeaccounts = new chargeAccount();
				
				$job_reference_details 	= $job_reference->getJobReferenceDetails($job_reference_id);
				$j1_details 			= $job->getJobDetails($job_reference_details['j1_id']);
				$chargeaccount_details = $chargeaccounts->getChargeAccountDetails($job_reference_details['charge_acc_id']);
					
				$booking_html = $job->getHTMLofWholeBooking($job_reference_id);
				
				$mail->From 	= 'alliedcars@alliedcars.com.au';
				$mail->FromName = 'Allied Cars';
				$mail->addAddress($receiver_email, ''.$receiver_name.'');  // Add a recipient Monash
				$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
				$mail->WordWrap = 50;
				$mail->isHTML(true);
				
				$mail->Subject = "Received Booking ID - ".$job_reference_details['j1_id']."";
				
				$mail->Body    .= self::DOCUMENT_HEADER;
				$mail->Body    .= self::BODY_START;
				$mail->Body    .= self::EMAIL_HTML_HEADER;
				$mail->Body    .= '<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0" style="border-collapse: collapse; border-color: #fff; background: #45cb37;">
										<tr>
											<td align="left" style="color: #ffffff; font-family: Arial, sans-serif; font-size: 200%">';
				
				
				$mail->Body    .= '<strong>PICK UP CONFIRMATION - Booking ID - '.$job_reference_details['j1_id'].'</strong></td></tr></table><br/>';
				$mail->Body    .= '<strong>Dear '.$receiver_name.',</strong><br/><br/>';
				
				$mail->Body  .='<strong>IF YOU CAN\'T FIND YOUR DRIVER, FIND A PUBLIC PHONE AND RING ALLIED ON 1800 350 850</STRONG> (this is a <strong>free</strong> call, <strong>NO</strong> coins needed)<br /><br />';
				$mail->Body    .='<strong>YOU NEED TO PRINT THIS CONFIRMATION EMAIL AND BRING IT WITH YOU TO SHOW THE ALLIED REPRESENTATIVE.</strong><br /><br />';
				$mail->Body  .="Thank you for your airport pickup request as per details below. <br/><br/>
											Please review your booking details and let us know immediately if they are not accurate. <br /><br />
											<strong>IMPORTANT INFORMATION</strong><br/><br/>
											A driver from Allied Chauffeured Cars (Allied) will meet you in Melbourne Airport's International Arrival Hall T2. If you are arriving on an International Flight, turn right when you leave the customs area and go towards 'The Meeting Point' (near Yes Optus Mobile Shop).
											Your driver will hold an 'ALLIED' or 'Monash' sign for easy identification and will take you to your destination as per your instructions below.<br /><br />
											If you are arriving on a Domestic Flight, make your way to the arrivals area in the international terminal T2. <br /><br />
											<strong>No driver waiting at Melbourne Airport</strong><br/><br/>
											Go to 'The Meeting Point'.  If your driver is not there within 15 minutes find the nearest public telephone and call Allied on 1800 350 850 (this is a <strong>free</strong> call, <strong>NO</strong> coins needed) so they can locate your driver. When calling, give Allied your Student ID number (this is in your offer letter), family name and given name.<br/><br/>
											<STRONG>Please note,</STRONG> if you do not arrive at your booked time or do not arrive at all without informing us, you will have to pay a <strong>cancellation fee </strong> to ".$chargeaccount_details['account_name']." . <br /><br />
											You can contact us by email alliedcars@alliedcars.com.au or by phone <strong>(+61 3) 8383 9999.</strong> <br /><br />
											<strong>City of Melbourne - International Student Welcome Booth</strong><br/><br/>
											The City of Melbourne operates the 'Student Welcome Desk' at Melbourne Airport where volunteers greet you and provide information about welfare services, public transport, housing, employment options and social integration to help you with your stay in Melbourne.<br/><br/> 
											<strong>Changes to or cancellation of your flight</strong><br/><br/>
											To change or cancel your airport pickup booking, contact us at least one full working day before you arrive to avoid additional costs.  Our office hours are 9am to 5pm Monday to Friday, Australian Eastern Standard Time.<br/><br/> 
											To change your arrival details on Saturday or Sunday, telephone +61 3 8383 9999.<br/><br/> 
											If you are overseas and miss your flight or if your flight is delayed, please telephone +61 3 8383 9999 immediately to confirm your new arrival details.  This will ensure your driver is at Melbourne Airport to meet you.<br/><br/>
											<br/><br/>
											
								"; 
				$mail->Body    .= $booking_html;
				
				$mail->Body    .= "<table align='center' width='100%'  cellpadding='5' cellspacing='0'>
									<tr>
										<td>
										Thank you for booking with us.<br/>Kind Regards, <br /><br/><strong>Allied Chauffeured Cars Australia</strong><br/>ABN: 38 076 977 136<br /><br/>
										<strong>Email:</strong> alliedcars@alliedcars.com.au <br />
										<strong>Telephone:</strong> 61 3 8383 9999 <br /><br/>
										</td>
									</tr>
									<tr>
										<td>
											New bookings or bookings cancellations and or amendments are valid only on receipt of our confirmation. <br /><br />
										</td>
									</tr>
								</table>";
				$mail->Body    .= self::EMAIL_HTML_FOOTER;
				$mail->Body    .= self::BODY_END;
				$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Melbourne_Airport_Map_RMIT.pdf",'Airport_Map.pdf','base64','application/pdf'); 
				$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/WIS_SIGN.pdf",'WIS.pdf','base64','application/pdf');
				$mail->send();
			}			
/*---------Send booking confirmation to booking made by a pax-----------------------------------------------------------------*/	
		public function sendMailToClientOnBooking($job_reference_id, $email_type, $receiver_email, $receiver_title, $receiver_fname, $receiver_lname)
			{
				// $email_type = 1; Means Booking Confirmed
				// $email_type = 2; Means Booking Received by not confirmed yet
				// $email_type = 3; Means Booking Rejected
				// $email_type = 4; Means Booking not approved by university
				
				//initialize classes
				
				$mail 			= new PHPMailer;
				$job_reference 	= new JobReference();
				$job 			= new Job();
				$chargeaccounts = new chargeAccount();
				
				$job_reference_details 	= $job_reference->getJobReferenceDetails($job_reference_id);
				$j1_details 			= $job->getJobDetails($job_reference_details['j1_id']);
				$chargeaccount_details = $chargeaccounts->getChargeAccountDetails($job_reference_details['charge_acc_id']);
				
				if($email_type == '1' && $job_reference_details['acc_type'] == '3' && $chargeaccount_details['id'] == '9')
					{
						$this->sendMailToMonashClaytonStudent($job_reference_id, $email_type, $receiver_email, $receiver_title, $receiver_fname, $receiver_lname);
					}
				else if($email_type == '1' && $job_reference_details['acc_type'] == '3' && $chargeaccount_details['id'] == '55')
					{
						$this->sendMailToMonashCollegeStudent($job_reference_id, $email_type, $receiver_email, $receiver_title, $receiver_fname, $receiver_lname);
					}
				else if($email_type == '1' && $job_reference_details['acc_type'] == '3' && $chargeaccount_details['id'] == '19')
					{
						$this->sendMailToMonashAbroadStudent($job_reference_id, $email_type, $receiver_email, $receiver_title, $receiver_fname, $receiver_lname);
					}
				else if($email_type == '1' && $job_reference_details['acc_type'] == '3' && $chargeaccount_details['id'] == '473')
					{
						$this->sendMailToMelbourneUniversityStudent($job_reference_id, $email_type, $receiver_email, $receiver_title, $receiver_fname, $receiver_lname);
					}
				else if($email_type == '1' && $job_reference_details['acc_type'] == '3' && $chargeaccount_details['id'] == '506')
					{
						$this->sendMailToDeakinUniversityStudent($job_reference_id, $email_type, $receiver_email, $receiver_title, $receiver_fname, $receiver_lname);
					}
				else if($email_type == '1' && $job_reference_details['acc_type'] == '3' && $chargeaccount_details['id'] == '599')
					{
						$this->sendMailToLatrobeUniversityStudent($job_reference_id, $email_type, $receiver_email, $receiver_title, $receiver_fname, $receiver_lname);
					}
				else
				{
					if($job_reference_details['job_type'] == '2') //return job
						{
							$j2_details = $job->getJobDetails($job_reference_details['j2_id']);
						}
					$booking_html = $job->getHTMLofWholeBooking($job_reference_id);
					
					$mail->From 	= 'alliedcars@alliedcars.com.au';
					$mail->FromName = 'Allied Cars';
					$mail->addAddress($receiver_email, ''.$receiver_title.' '.$receiver_fname.' '.$receiver_lname.'');  // Add a recipient
					$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
					$mail->WordWrap = 50;
					$mail->isHTML(true);
					
					if($email_type == '1')
						{
							if($job_reference_details['job_type'] == '1')
								{
									$mail->Subject = "Confirmation for Booking for ".$job_reference_details['std_title']." ".$job_reference_details['std_fname']." ".$job_reference_details['std_lname']." ".$job_reference_details['std_id']." - ".formatDate($j1_details['job_date'], '3')." - Booking ID - ".$job_reference_details['j1_id']."";
								}
							if($job_reference_details['job_type'] == '2')
								{
									$mail->Subject = "Confirmation of Booking IDs - ".$job_reference_details['j1_id']." and ".$job_reference_details['j2_id']."";
								}
						}
					if($email_type == '2')
						{
							if($job_reference_details['job_type'] == '1')
								{
									//$mail->Subject = "Bookings Received, waiting for confirmation, Booking ID - ".$job_reference_details['j1_id']."";
									$mail->Subject = "".$job_reference_details['std_id']." - Requesting Approval for Booking for ".$job_reference_details['std_title']." ".$job_reference_details['std_fname']." ".$job_reference_details['std_lname']." - ".formatDate($j1_details['job_date'], '3')." - Booking ID - ".$job_reference_details['j1_id']."";
								}
							if($job_reference_details['job_type'] == '2')
								{
									$mail->Subject = "Bookings Received, waiting for confirmation, Booking IDs - ".$job_reference_details['j1_id']." and ".$job_reference_details['j2_id']."";
								}
						}
					if($email_type == '3')
						{
							if($job_reference_details['job_type'] == '1')
								{
									$mail->Subject = "Booking Request Declined, Booking ID - ".$job_reference_details['j1_id']."";
								}
							if($job_reference_details['job_type'] == '2')
								{
									$mail->Subject = "Booking Request Declined, Booking IDs - ".$job_reference_details['j1_id']." and ".$job_reference_details['j2_id']."";
								}
						}
					if($email_type == '4')
						{
							if($job_reference_details['job_type'] == '1')
								{
									$mail->Subject = "Booking Request Declined by University, Booking ID - ".$job_reference_details['j1_id']."";
								}
						}
					$mail->Body    .= self::DOCUMENT_HEADER;
					$mail->Body    .= self::BODY_START;
					$mail->Body    .= self::EMAIL_HTML_HEADER;
					$mail->Body    .= '<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0" style="border-collapse: collapse; border-color: #fff; background: #45cb37;">
											<tr>
												<td align="left" style="color: #ffffff; font-family: Arial, sans-serif; font-size: 200%">';
												
					// $email_type = 1; Means Booking Confirmed
					// $email_type = 2; Means Booking Received but not confirmed yet
					// $email_type = 3; Means Booking Rejected
					// $email_type = 4; Means Booking not approved by university
					
					if($email_type == '1')
						{	
							$mail->Body    .= '<strong>Arrival Pickup Confirmation</strong></td></tr></table><br/>';
						}
					if($email_type == '2')
						{
							$mail->Body    .= '<strong>Booking Request Received</strong></td></tr></table><br/>';
						}
					if($email_type == '3')
						{
							$mail->Body    .= '<strong>Booking Request Failed</strong></td></tr></table><br/>';
						}
					if($email_type == '4')
						{ 
							$mail->Body    .= '<strong>Booking Request Failed</strong></td></tr></table><br/>';
						}
					if($email_type =='1' && $job_reference_details['acc_type'] == '3' && $chargeaccount_details['id'] == '15' ){
						//$mail->Body    .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Student<br /><strong>Dear '.$receiver_title.' '.$receiver_fname.' '.$receiver_lname.',</strong><br/><br/>';
						$mail->Body    .= '<strong>Dear '.$receiver_title.' '.$receiver_fname.' '.$receiver_lname.',</strong><br/><br/>';
					}
					else{
						$mail->Body    .= '<strong>Dear '.$receiver_title.' '.$receiver_fname.' '.$receiver_lname.',</strong><br/><br/>';
					}
					
					if($email_type == '1')
						{
							if($job_reference_details['acc_type'] == '3') 
							{
								//if($chargeaccount_details['account_name'] == 'RMIT University')
								if($chargeaccount_details['id'] == '15')
								{
									$mail->Body .= "Thank you for booking with us and we are now pleased to confirm your arrival pick-up. <br /><br />
													Our staff will meet you at the airport. They will be holding a placard <strong>\"RMIT UNIVERSITY\"</strong>. <br /><br />
													You have to let us know the number of luggage you have.  Please note if your excess luggage do not fit in a standard sedan's boot, you will have to arrange a taxi to transport any excess luggage. <br /><br />
													Please refrain from bringing food into Australia. As this <strong>will</strong> delay your immigration and customs clearance time and you <strong>may incur extra charges.</strong><br /><br />
													You must look for Allied's driver <strong>immediately</strong> at the Meeting Point of your arriving terminal.<br /><br />
													If you experience a delay or cannot find Allied's driver, you must ring Allied on <strong>1800 350 850</strong> (FREE CALL) from any public phone.<br /><br />
													Please note a no show fee of <strong>AU$105</strong> applies if you do not inform Allied of any changes to your arrival itinerary or do not use the service.<br /><br />
													
													If you arrive at:<br /><br />
													<img src='http://dev.bookings-global.com.au/Images/bullet_icon.png' alt=' ' width='10px' height='9px' />&nbsp; <strong>Melbourne Airport-T2 (International) </strong> , you will be met at the \" Meeting Point \" located next to the Groups and Tours desk. A large red sign denotes the area. Refer to \"Key 11\" of the attached Melbourne Airport Map..<br /><br />
													<img src='http://dev.bookings-global.com.au/Images/bullet_icon.png' alt=' ' width='10px' height='9px' />&nbsp; <strong>Qantas Domestic Terminal</strong>, please go to the \"Meeting Point\" located opposite Baggage Carousel 3.  If you can't find your driver at the Domestic terminal, please go to <strong>T2</strong> (<strong>International</strong>, next door to your right).<br /><br />
													<img src='http://dev.bookings-global.com.au/Images/bullet_icon.png' alt=' ' width='10px' height='9px' />&nbsp; <strong>Virgin Australia Terminal</strong>, please go to the \"Meeting Point\" below the escalators. If you can't find your driver at the Domestic terminal, please go to <strong>T2</strong> (<strong>International</strong>, next door to your left). <br /><br />
													<img src='http://dev.bookings-global.com.au/Images/bullet_icon.png' alt=' ' width='10px' height='9px' />&nbsp; <strong>Terminal 4 (Tiger Airways)</strong>, you have to ring Allied Chauffeured Cars on <strong>1800 350 850 </strong> from <strong>any</strong> public phone (Free Call), the staff will direct you to the meeting point. <br /><br /><br />
													If you are unable to find our staff or have made changes to your arrival itinerary, you must call us. <br/>
													Otherwise we cannot guarantee airport pick-up and you will be charged for the service. <br />";
									
								}
								else
								{
									
									$mail->Body  .="Thank you for booking with us and we are now pleased to confirm your arrival details. <br /><br />
												Please review the booking details and let us know immediately if they are not accurate. <br /><br />
												<strong>IMPORTANT INFORMATION</strong><br/><br/>
												It is <strong>very</strong> important for <strong>Allied Chauffeured Cars </strong> to know how much luggage you are going to have, if you haven't done so please email <strong>Allied Chauffeured Cars</strong> to let them know. <br /><br />
												Our staff will meet you at the airport. <br /><br />
												If you are arriving from an International Flight, clear customs and make your way into the arrivals area (turn right when exiting Customs), where your driver will be waiting for you near Yes Optus shop.<br /><br />
												If you are arriving from a Domestic Flight, make your way to the arrivals area in the international terminal and your driver will be waiting for you near Yes Optus shop. <br /><br />
												If you can't find your driver once you arrive in Melbourne please ring Allied on our toll free number <strong>1800 350 850.</strong> <br /><br />
												You have <strong>forty five minutes</strong> from the moment that the plane lands to meet your driver.<br /><br />
												<STRONG>Please note,</STRONG> if you do not arrive at your booked time or do not arrive at all without informing us, you will have to pay a <strong>cancellation fee of A$105</strong> to ".$chargeaccount_details['account_name']." . <br /><br />
												You can contact us by email alliedcars@alliedcars.com.au or by phone <strong>(+61 3) 8383 9999.</strong> <br /><br />
												"; 
									
								}
							}
							else
							{
								$mail->Body  .='This is a confirmation for booking request you have made with us..<br/>
											Please review the booking details and let us know immediately if they are not accurate.<br/>
											You can contact us by email '.MAIL_EMAIL.' Also you can contact us by phone '. MAIL_PHONE.' <br/><br/>';
							}
						}
					if($email_type == '2')
						{
							$mail->Body  .='We have received your booking request. We will get get back to you shortly with a confirmation email.<br/>
											In the mean time, please review the booking details and let us know immediately if they are not accurate.<br/>
											You can contact us by email '.MAIL_EMAIL.' Also you can contact us by phone '. MAIL_PHONE.' <br/><br/>';
						}
					if($email_type == '3')
						{
							$mail->Body  .='This is to let you know that at this moment the time and date you have requested, we are unable to accommodate your booking request. However, please do not hesitate to contact us, if there is any changes to your trip itinerary. We really appreciate your business and would always like to server you with professionalism and smile.<br/><br/>';
						}
					if($email_type == '4')
						{
							$mail->Body  .='This is to let you know that at this moment the time and date you have requested, we are unable to accommodate your booking request. <br/><br/>';
						}
					//if($email_type =='1' && $job_reference_details['acc_type'] == '3' && $chargeaccount_details['id'] == '15' )// Commented out as RMIT Students were not getting the details of the booking
					if($email_type =='1' && $job_reference_details['acc_type'] == '3')
						{
							$mail->Body    .= $booking_html;
						}
					else 
						{
							$mail->Body    .= $booking_html;
						}
					
					if($email_type == '4')
						{
							$chargeaccount_details = $chargeaccounts->getChargeAccountDetails($job_reference_details['charge_acc_id']);
							$mail->Body    .= '<table align="center" width="100%"  cellpadding="5" cellspacing="0">
											<tr>
												<td>
												Thank you.<br/>Kind Regards<br/>Admin<br/><strong>'.$chargeaccount_details['account_name'].'</strong><br/><br/>
												</td>
											</tr>
										</table>';
							$mail->Body    .= self::EMAIL_HTML_FOOTER;
							$mail->Body    .= self::BODY_END;
						}
					else if($email_type == '1')
					{
						if($chargeaccount_details['id'] == '15') //RMIT
							{
								$mail->Body    .= "<table align='center' width='100%'  cellpadding='5' cellspacing='0'>
											<tr>
												<td>
												<br /><strong>Allied Chauffeured Cars </strong><br /><br/>
												<strong> 1800 350 850 (Free call within Australia)</strong> <br />
												<strong>+613 8383 9999 (Calling From Overseas)</strong>  <br /><br/>
												</td>
											</tr>
										</table>"; 
								$mail->Body    .= self::EMAIL_HTML_FOOTER;
								$mail->Body    .= self::BODY_END;
								$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Melbourne_Airport_Map_RMIT.pdf",'Airport_Map.pdf','base64','application/pdf');
								$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/RMIT.pdf",'RMIT.pdf','base64','application/pdf');
								$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/WIS_SIGN.pdf",'WIS.pdf','base64','application/pdf');
								$mail->send();
							}
						else
						{
							$mail->Body    .= "<table align='center' width='100%'  cellpadding='5' cellspacing='0'>
										<tr>
											<td>
											Thank you for booking with us.<br/>Kind Regards, <br /><br/><strong>Allied Chauffeured Cars Australia</strong><br/>ABN: 38 076 977 136<br /><br/>
											<strong>Email:</strong> alliedcars@alliedcars.com.au <br />
											<strong>Telephone:</strong> 61 3 8383 9999 <br /><br/>
											</td>
										</tr>
										<tr>
											<td>
												<BR />
												New bookings or bookings cancellations and or amendments are valid <strong>only</strong> on receipt of our confirmation. <br /><br />
												Cancellations and or amendments for sedans need at least 2 hours notice, 4 hours in New Zealand and 24 hours in Darwin. <br /><br />
												Minibuses and Coaches 24 hours.  Major events 48 hours cancellation and or amendment notice. <br /><br />
												Please be aware that our office <STRONG>closes on public holidays.</STRONG> Except on Melbourne Cup Day. <br /><br />
												All new bookings or bookings cancellations and or amendments must be made through the office, during office hours.<br /><br />
												<br />Our car's service is 24 hours a day, 
												<br />Our office hours are
												<br />Monday to Friday  8am to 7pm
												<br />Saturday &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1pm to 5pm
												<br />Sunday &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1pm to 7pm
												<br />
											</td>
										</tr>
									</table>";
							$mail->Body    .= self::EMAIL_HTML_FOOTER;
							$mail->Body    .= self::BODY_END;
						}
					}
					else
					{
						$mail->Body    .= '<table align="center" width="100%"  cellpadding="5" cellspacing="0">
										<tr>
											<td>
											Thank you for booking with us.<br/>Kind Regards<br/>Admin<br/><strong>Allied Cars</strong><br/><br/>
											</td>
										</tr>
									</table>';
						$mail->Body    .= self::EMAIL_HTML_FOOTER;
						$mail->Body    .= self::BODY_END;
					}
					
					$mail->send();
				}
			}

/*---------send booking confirmation to booking made by a pax-----------------------------------------------------------------*/	
		public function sendMailToClientForIndividualBooking($job_id, $email_type, $receiver_email, $receiver_title, $receiver_fname, $receiver_lname)
			{
				// $email_type = 1; Means Job Confirmed
				// $email_type = 2; Means Job Declined
				
				//initialize classes
				
				$mail 			= new PHPMailer;
				$job_reference 	= new JobReference();
				$job 			= new Job();
				
				$job_details 			= $job->getJobDetails($job_id);
				$job_reference_details 	= $job_reference->getJobReferenceDetails($job_details['job_reference_id']);
				
				$booking_html = $job->getHTMLforIndividualBooking($job_id);
				
				$mail->From 	= 'alliedcars@alliedcars.com.au';
				$mail->FromName = 'Allied Chauffeured Cars Australia Pty Ltd';
				$mail->addAddress($receiver_email, ''.$receiver_title.' '.$receiver_fname.' '.$receiver_lname.'');  // Add a recipient
				$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
				$mail->WordWrap = 50;
				$mail->isHTML(true);
				
				if($email_type == '1')
					{
						$mail->Subject = "Confirmation of Booking ID - ".$job_id."";
					}
				if($email_type == '2')
					{
						$mail->Subject = "Not Confirmed, Apology of Booking ID - ".$job_id."";
					}
				$mail->Body    .= self::DOCUMENT_HEADER;
				$mail->Body    .= self::BODY_START;
				$mail->Body    .= self::EMAIL_HTML_HEADER;
				$mail->Body    .= '<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0" style="border-collapse: collapse; border-color: #fff; background: #45cb37;">
										<tr>
											<td align="left" style="color: #ffffff; font-family: Arial, sans-serif; font-size: 200%">';
				if($email_type == '1')
					{	
						$mail->Body    .= '<strong>Booking Confirmation</strong></td></tr></table><br/>';
					}
				if($email_type == '2')
					{
						$mail->Body    .= '<strong>Booking - Not Confirmed (Declined)</strong></td></tr></table><br/>';
					}
				$mail->Body    .= '<strong>Dear '.$receiver_title.' '.$receiver_fname.' '.$receiver_lname.',</strong><br/>';
				if($email_type == '1')
					{
						$mail->Body  .='This is a confirmation for booking request you have made with us..<br/>
										In the mean time, please review the booking details and let us know immediately if they are not accurate.<br/>
										You can contact us by email '.MAIL_EMAIL.' Also you can contact us by phone '. MAIL_PHONE.' <br/><br/>';
					}
				if($email_type == '2')
					{
						$mail->Body  .='This is to let you know that at this moment the time and date you have requested, we are unable to accommodate your booking request. However, please do not hesitate to contact us, if there is any changes to your trip itinerary. We really appreciate your business and would always like to server you with professionalism and smile.<br/><br/>';
					}
				$mail->Body    .= $booking_html;
				$mail->Body    .= '<table align="center" width="100%"  cellpadding="5" cellspacing="0">
									<tr>
										<td>
										Thank you for booking with us.<br/>Kind Regards<br/>Admin<br/><strong>Allied Cars</strong><br/><br/>
										</td>
									</tr>
								</table>';
				$mail->Body    .= self::EMAIL_HTML_FOOTER;
				$mail->Body    .= self::BODY_END;
				$mail->send();
			}
			
//-----------------------send mail to Admins when bkg_src is website---------------------------------------------------------------/
		public function sendMailToAdminOnBooking($job_reference_id)
			{
				$mail 	= new PHPMailer;
				$mail->CharSet = "UTF-8";
				$job = new job(); 
				$job_reference 	= new JobReference();
				$chargeAccount = new chargeAccount();
				$user = new User();
				
				$job_reference_details 	= $job_reference->getJobReferenceDetails($job_reference_id);
				
				$booking_html = $job->getHTMLofWholeBooking($job_reference_id,'1');
				$admin_array = $job->getAdminDetails();
				
				$charge_acc_details = $chargeAccount->getChargeAccountDetails($job_reference_details['charge_acc_id']);
				$j1_details = $job->getJobDetails($job_reference_details['j1_id']);
				$pax_details = $user->getUserDetails($job_reference_details['pax_id']);
				
				$mail->From 	= 'alliedcars@alliedcars.com.au';
				$mail->FromName = 'Allied Chauffeured Cars Australia Pty Ltd';
				foreach($admin_array AS $admin)
					{
						$mail 	= new PHPMailer; 
						$mail->CharSet = "UTF-8";
						$mail->From 	= 'alliedcars@alliedcars.com.au';
						$mail->FromName = 'Allied Cars';
						$admin_id = base64_encode(base64_encode($admin['id']));
						 
						//$mail->addAddress('rotoman@bigpond.com', $admin['fname'].' '.$admin['lname']);  // Add a recipient
						$mail->addAddress($admin['email'], $admin['fname'].' '.$admin['lname']);  // Add a recipient
						$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
						$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
						$mail->isHTML(true);                                  // Set email format to HTML
						//$mail->Subject = $charge_acc_details['account_name'].' Requisition for Booking';
						
						if($job_reference_details['acc_type'] == '3')
						{
							$mail->Subject =  $charge_acc_details['account_name'].' Requisition for Booking for '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' '.$job_reference_details['std_id'].' - '.date("l, d F, Y", strtotime($j1_details['job_date']));
						}
						else
						{
							$mail->Subject =  $charge_acc_details['account_name'].' Requisition for Booking for '.$pax_details['title'].' '.$pax_details['fname'].' '.$pax_details['lname'].' - '.date("l, d F, Y", strtotime($j1_details['job_date']));
						}					
						
						$mail->Body    = self::DOCUMENT_HEADER;
						$mail->Body    .= self::BODY_START;
						$mail->Body    .= self::EMAIL_HTML_HEADER;
						$mail->Body    .= '<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0" style="border-collapse: collapse; border-color: #fff; background: #45cb37;">
												<tr>
													<td align="left" style="color: #ffffff; font-family: Arial, sans-serif; font-size: 200%">
														<strong>Booking Received for your Approval</strong>
													</td>
												</tr>
											</table>
											<br/>
											<strong>Dear '.$admin['fname'].', </strong><br/>
											We have received the following booking request';
						if($job_reference_details['acc_type'] == '3')
						{
							$mail->Body		.= 	' from '.$charge_acc_details['account_name'];
						}
						$mail->Body		.= 	'. <br/><br/>';
						$mail->Body		.= $booking_html;
						
						$mail->Body		.= '<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0"><tr><td>';
						$mail->Body		.= "<a href='".ROOT_ADDRESS."external_mail_script/booking_decision.php?job_reference_id=".$job_reference_id."&admin_id=".$admin_id."&confirm_status=1'>Confirm this Booking</a ><br /><br />";
						$mail->Body		.= "<a href='".ROOT_ADDRESS."external_mail_script/booking_decision.php?job_reference_id=".$job_reference_id."&admin_id=".$admin_id."&confirm_status=2'>Decline this booking</a ><br /><br />";
						$mail->Body		.= '</td><tr></table>';
						$mail->Body    .= self::EMAIL_HTML_FOOTER;
						$mail->Body    .= self::BODY_END;
						$mail->send();
					}
			}
			
/*-----------------------------------------------------------------------------------------------------------*/		
		
		public function sendInvoice($receivers_name, $receivers_email, $email_body, $invoice_id)
			{
				$mail 			= new PHPMailer;
				$mail->From 	= 'alliedcars@alliedcars.com.au';
				$mail->FromName = 'Allied Cars';
				$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
				$mail->WordWrap = 50;
				$mail->isHTML(true);
				
				$mail->addAddress($receivers_email, $receivers_name);  // Add a recipient
				
				$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/invoices/".$invoice_id.".pdf",'Invoice_'.$invoice_id.'.pdf','base64','application/pdf');
				$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/invoices/".$invoice_id.".html",'Invoice_'.$invoice_id.'.html');
				$mail->Subject = 'Invoice No. '.$invoice_id.' from Allied Chauffeured Cars Australia Pty Ltd';
				
				$mail->Body    .= self::MOBILE_HEADER;
				$mail->Body    .= $email_body;
				$mail->Body    .= self::MOBILE_FOOTER;
				
				
				if(!$mail->send()) 
					{
					   $message =  'Message could not be sent.';
					   $message =  'Mailer Error: ' . $mail->ErrorInfo;
					   exit;
					}
			}

//-----------------------send mail to Driver on Job Offer---------------------------------------------------------------/
		public function sendJobOfferToDriver($driver_id, $job_id, $offer_amount, $driver_notes='')
			{
				$mail 	= new PHPMailer;
				$mail->CharSet = "UTF-8";
				$job = new job();
				$user = new user();
				
				$job_reference 	= new JobReference();
				$job_ref_details 	= $job_reference->getJobReferenceDetails($job_reference_id);
				$job_details 	= $job->getJobDetails($job_id);
				$driver_details = $user->getUserDetails($driver_id);
				
				$mail->From 	= 'alliedcars@alliedcars.com.au';
				$mail->FromName = 'Allied Cars';
				$encoded_job_id 	= base64_encode(base64_encode($job_id));
				$encoded_driver_id 	= base64_encode(base64_encode($driver_id));
				$encoded_offer_amount 	= base64_encode(base64_encode($offer_amount));
				$mail->addAddress($driver_details['email'], $driver_details['title'].' '.$driver_details['fname'].' '.$driver_details['lname']);  // Add a recipient

				$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
				$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
				$mail->isHTML(true);                                  // Set email format to HTML
				$mail->Subject = 'New job offer from Allied Cars';
				$mail->Body    = self::DOCUMENT_HEADER;
				$mail->Body    .= '<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0" style="border-collapse: collapse; border-color: #fff; background: #45cb37;">
										<tr>
											<td align="left" style="color: #ffffff; font-family: Arial, sans-serif; font-size: 100%" colspan="2">
												<strong>JOB OFFER</strong>
											</td>
										</tr>
										<tr>
											<td>Date</td>
											<td>'.date("l, d F, Y", strtotime($job_details['job_date'])).'</td>
										</tr>
										<tr>
											<td>Time</td>
											<td>'.date("H:i A", strtotime($job_details['job_time'])).'</td>
										</tr>
										<tr>
											<td>From</td>
											<td>'.$job_details['frm_flight_no'].' '.$job_details['frm_sub'].'';
											if($job_details['frm_via_sub'] != '')
												{
													$mail->Body    .= ' <b>TO '.$job_details['frm_via_sub'].'</b>';
												}
				$mail->Body    .= ' 		</td>
										</tr>
										<tr>
											<td>To</td>
											<td>'.$job_details['to_sub'].'';
										if($job_details['to_via_sub'] != '')
												{
													$mail->Body    .= ' <b>TO '.$job_details['to_via_sub'].'</b>';
												}
				$mail->Body    .= ' 		</td>
										</tr>
										<tr>
											<td>Price</td>
											<td>$'.$offer_amount.'</td>
										</tr>
										<tr>
											<td>Note</td>
											<td>'.$driver_notes.'</td>
										</tr>
									</table>
									<br/>
									<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0" style="border-collapse: collapse; border-color: #fff; background: #fff;">
										<tr>
											<td><b><a href="'.ROOT_ADDRESS.'external_mail_script/driver_decision.php?job_id='.$encoded_job_id.'&driver_id='.$encoded_driver_id.'&offer_amount='.$encoded_offer_amount.'&accept_job=1">Accept this Job</a ><b/></td>
										</tr>
									</table><br/><br/>';
				$mail->Body    .= self::BODY_END;

				$mail->send();
				return $driver_details['email'];
			}
			
//----------------------------------------------------------------------------------------------------------------------/						
		public function sendJobDetailsToDriver($job_id, $driver_id, $job_price, $driver_notes='')
			{
				$mail 	= new PHPMailer;
				$mail->CharSet = "UTF-8";
				$job = new job();
				$user = new user();
				$job_reference 	= new JobReference();
				
				$encoded_job_id 	= base64_encode(base64_encode($job_id));
				$encoded_driver_id 	= base64_encode(base64_encode($driver_id));
				
				$job_details 	= $job->getJobDetails($job_id);
				$job_ref_details 	= $job_reference->getJobReferenceDetails($job_details['job_reference_id']);
				
				$driver_details = $user->getUserDetails($driver_id);
				
				$mail->From 	= 'alliedcars@alliedcars.com.au';
				$mail->FromName = 'Allied Cars';
				$mail->addAddress($driver_details['email'], $driver_details['title'].' '.$driver_details['fname'].' '.$driver_details['lname']);  // Add a recipient
				//$mail->addAddress('sucheta_sw@yahoo.co.in', $driver_details['title'].' '.$driver_details['fname'].' '.$driver_details['lname']);  // Add a recipient
				$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
				$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
				$mail->isHTML(true);                                  // Set email format to HTML
				
				

				$mail->Subject = 'Job Details from Allied Cars';
				$mail->Body    = self::DOCUMENT_HEADER;
				$mail->Body    .= $job->getHTMLforIndividualJobForDriver($job_id);
				$mail->Body    .= 	'<table align="left" border="1" cellpadding="5" cellspacing="0" width="100%" style="border-collapse: collapse; border-color: #666; "><tr>
											<td width="30%"><strong>Job Price:</strong></td>
											<td style="background: #eee;"><b>$ '.$job_price.'</b></td>
										</tr>
										<tr>
										<td width="30%"><strong>Notes:</strong></td>
											<td style="background: #eee;"><b> '.$driver_notes.'</b></td>
										</tr>
										<tr>
										<td width="30%"><strong>We require you to send an invoice if <br>doing 5 or more jobs per week.</strong></td>
										</tr>
										<tr>
											<td width="30%"><strong>Include the numbers on the right ----><br>when sending your invoice ---> <br> </strong></td>
											<td style="background: #eee;"><b>'.$job_id.'    <br>     $ '.$job_price.'</b></td>
										</tr>
									</table><br/>';
				$mail->Body    .= 	'<br/><table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0" style="border-collapse: collapse; border-color: #fff; background: #fff;">
										<tr>
											<td><b><a href="'.ROOT_ADDRESS.'external_mail_script/driver_decision.php?job_id='.$encoded_job_id.'&driver_id='.$encoded_driver_id.'&rogerd=1">Roger and Confirm</a ><b/></td>
										</tr>
									</table>';
				$mail->Body    .= self::BODY_END;
				
				if($job_ref_details['charge_acc_id'] == '55') //Monash college send the map with instructions Drivers
					{
						$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/WIS_SIGN.pdf",'WIS.pdf','base64','application/pdf');
						$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Monash_College.pdf",'Monash_College_Map.pdf','base64','application/pdf');
						$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Monash_College_Sign.pdf",'Monash_College_Sign.pdf','base64','application/pdf');
						
					}
				if($job_ref_details['charge_acc_id'] == '9' || $job_ref_details['charge_acc_id'] == '19') //Monash connect or monash abroad send the map with instructions
					{
						$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/WIS_SIGN.pdf",'WIS.pdf','base64','application/pdf');
						$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/monash_connect_and_abroad.pdf",'Monash_Campus_Map.pdf','base64','application/pdf');
					}
				if($job_ref_details['charge_acc_id'] == '15') //RMIT send the map with instructions Drivers
					{
						$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/WIS_SIGN.pdf",'WIS.pdf','base64','application/pdf');
						$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/RMIT.pdf",'RMIT.pdf','base64','application/pdf');
					}
				else // common attachments
					{
						// $mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Allied_Corp_Sign.pdf",'Allied Sign.pdf','base64','application/pdf');
					}
				$mail->send();
			}
		
/*-----------------------------------------------------------------------------------------------------------*/				
		public function sendCollectiveJobDetailsToDriver($job_ids, $driver_id, $job_price, $driver_notes='')
			{
				$mail 	= new PHPMailer;
				$mail->CharSet = "UTF-8";
				$job = new job();
				$user = new user();
				$job_reference 	= new JobReference();
				
				$encoded_job_id 	= base64_encode(base64_encode($job_ids));
				$encoded_driver_id 	= base64_encode(base64_encode($driver_id));
				
				
				$driver_details = $user->getUserDetails($driver_id);
				
				$mail->From 	= 'alliedcars@alliedcars.com.au';
				$mail->FromName = 'Allied Cars';
				$mail->addAddress($driver_details['email'], $driver_details['title'].' '.$driver_details['fname'].' '.$driver_details['lname']);  // Add a recipient
				//$mail->addAddress('sucheta_sw@yahoo.co.in', $driver_details['title'].' '.$driver_details['fname'].' '.$driver_details['lname']);  // Add a recipient
				$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
				$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
				$mail->isHTML(true);                                  // Set email format to HTML
				$mail->Subject = 'Job Details from Allied Cars';
				$mail->Body    = self::DOCUMENT_HEADER;
						
				$job_arr = explode(',',$job_ids);
				foreach($job_arr as $job_id)
				{
					$job_details 	= $job->getJobDetails($job_id);
					//$job_ref_details 	= $job_reference->getJobReferenceDetails($job_details['job_reference_id']);
					$mail->Body    .= $job->getHTMLforIndividualJobForDriver($job_id);
				}
				$mail->Body    .= '<table align="left" border="1" cellpadding="5" cellspacing="0" width="60%" style="border-collapse: collapse; border-color: #666; "><tr>
											<td width="50%"><strong>Job Price</strong></td>
											<td style="background: #eee;"><b>$ '.$job_price.'</b></td>
										</tr>
										<tr>
											<td width="50%"><strong>Note</strong></td>
											<td style="background: #eee;"><b> '.$driver_notes.'</b></td>
										</tr>
									</table><br/>';
				$mail->Body    .= '<br/><table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0" style="border-collapse: collapse; border-color: #fff; background: #fff;">
										<tr>
											<td><b><a href="'.ROOT_ADDRESS.'external_mail_script/driver_decision.php?job_id='.$encoded_job_id.'&driver_id='.$encoded_driver_id.'&rogerd=1&collective=1">Roger and Confirm</a ><b/></td>
										</tr>
									</table><br/><br/>';
				$mail->Body    .= self::BODY_END;

				$mail->send();
				
			}
			
//-----------------------send Bulk Job Offer to Driver---------------------------------------------------------------/
		public function sendBulkJobOfferToADriver($driver_id, $jobs_array)
			{
				$mail 	= new PHPMailer;
				$mail->CharSet = "UTF-8";
				$job = new job();
				$user = new user();
				
				$encoded_driver_id 		= 	base64_encode(base64_encode($driver_id));
				$driver_details 		= 	$user->getUserDetails($driver_id);
				
				$mail->From 	= 'alliedcars@alliedcars.com.au';
				$mail->FromName = 'Allied Cars';
				$mail->addAddress($driver_details['email'], $driver_details['title'].' '.$driver_details['fname'].' '.$driver_details['lname']);  // Add a recipient

				$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
				$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
				$mail->isHTML(true);
				
				$number_of_jobs 	= 	count($jobs_array);
				$mail->Subject 		= 	'New '.$number_of_jobs.' Jobs Offerd from Allied Cars';
				$mail->Body    		= 	self::DOCUMENT_HEADER;
				$mail->Body    		.= 	'The following <b>'.$number_of_jobs.'</b> Jobs have been offered to you. Please read them carefully before accepting.<br/><br/>';
				$mail->Body    		.= 	'<table align="left" width="60%" cellpadding="5" cellspacing="0" style="border: 1px #000 solid; border-collapse: collapse;">';
				
				$unique_code 				= 	substr(md5(uniqid(mt_rand(), true)) , 0, 8);
				$encoded_unique_code 		= 	base64_encode(base64_encode($unique_code));
				for($i=0; $i<$number_of_jobs; $i++)
					{
						$q = "INSERT INTO job__bulk_accept (unique_code, job_id, driver_id, job_price, job_notes)
						VALUES('".$unique_code."', '".$jobs_array[$i]['job_id']."', '".$driver_id."', '".$jobs_array[$i]['job_price']."', '".$jobs_array[$i]['driver_notes']."')";
						$database = new database;
						$result = $database->query($q);
						$job_details 	= 	$job->getJobDetails($jobs_array[$i]['job_id']);
						$offer_amount	=	$jobs_array[$i]['job_price'];
						$driver_notes	=	$jobs_array[$i]['driver_notes'];
						$mail->Body    	.= 	'<tr>
												<td width="35%" style="border: 1px #336699 solid;  background: #ccffff;" valign="top">
													<h1>'.date("H:i A", strtotime($job_details['job_time'])).'</h1>
													'.date("l, d F, Y", strtotime($job_details['job_date'])).'
												</td>
												<td width="65%" style="border: 1px #336699 solid;">
													<table width="100%" cellpadding="5">
														<tr>
															<td width="40%" valign="center">
																<h3>'.$job_details['frm_flight_no'].'<br/>
																'.$job_details['frm_sub'].'</h3>';
																if($job_details['frm_via_sub'] != '')
																	{
																		$mail->Body .= '<br/><b>VIA '.$job_details['frm_via_sub'].'</b>';
																	}
																	
														$mail->Body.= 
															'</td>
															<td width="20%" valign="center"><h1>&#8594;</h1></td>
															<td width="40%" valign="center">
																<h3>'.$job_details['to_sub'].'</h3>';
																if($job_details['to_via_sub'] != '')
																	{
																		$mail->Body .= '<br/><b>TO '.$job_details['to_via_sub'].'</b>';
																	}
														$mail->Body.= 
															'</td>	
														</tr>
														<tr>
															<td colspan="3" style="background:#ffffcc;"><h2>$'.$offer_amount.'</h2></td>
														</tr>';
														if($driver_notes != '')
															{
																$mail->Body.= '<tr><td colspan="3" style="background:#ffffcc;"><b>Notes</b> - '.$driver_notes.'</td></tr>';
															}
													$mail->Body.= '
													</table>
												</td>
											</tr>';
					}
								$mail->Body .= '
										</table>
										<br/>
										<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0" style="border-collapse: collapse; border-color: #fff; background: #fff;">
											<tr>
												<td><b><a href="'.ROOT_ADDRESS.'external_mail_script/driver_bulk_decision.php?unique_code='.$encoded_unique_code.'&driver_id='.$encoded_driver_id.'&decision_status=1">Accept these Jobs</a ><b/></td>
											</tr>
										</table><br/><br/>';
				$mail->Body    .= self::BODY_END;
				$mail->send();
			}

//-----------------------send Job Details on Bulk offer---------------------------------------------------------------/
		public function sendBulkJobDetailsToADriver($driver_id, $unique_code)
			{
				require_once(CLASSES_PATH . "vehicle.php");
				$mail 	= new PHPMailer;
				$job_reference = new jobReference();
				$vehicle = new Vehicle();
				$mail->CharSet = "UTF-8";
				$job = new job();
				$user = new user();
				
				$encoded_driver_id 		= 	base64_encode(base64_encode($driver_id));
				$encoded_unique_code	= 	base64_encode(base64_encode($unique_code));
				$driver_details 		= 	$user->getUserDetails($driver_id);
				
				$mail->From 	= 'alliedcars@alliedcars.com.au';
				$mail->FromName = 'Allied Cars';
				$mail->addAddress($driver_details['email'], $driver_details['title'].' '.$driver_details['fname'].' '.$driver_details['lname']);  // Add a recipient

				$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
				$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
				$mail->isHTML(true);
				
				$database = new database;
				$q = "SELECT * FROM job__bulk_accept where unique_code = '".$unique_code."'";
				$result = $database->query($q);
				$number_of_jobs = mysql_num_rows($result);
				$mail->Subject 		= 	'Job details of '.$number_of_jobs.' Jobs Accepted from Allied Cars';
				$mail->Body    		= 	self::DOCUMENT_HEADER;
				$mail->Body    		.= 	'The following <b>'.$number_of_jobs.'</b> Jobs have been accepted by you. Please read the details carefully and ROGER.<br/><br/>';
				$mail->Body    		.= 	'<table align="left" width="60%" cellpadding="5" cellspacing="0" style="border: 1px #000 solid; border-collapse: collapse;">';
				
				while($row = mysql_fetch_array($result))
					{
						$job_details 			= 	$job->getJobDetails($row['job_id']);
						$job_reference_details 	= 	$job_reference->getJobReferenceDetails($job_details['job_reference_id']);
						$pax_details 			= 	$user->getUserDetails($job_reference_details['pax_id']);
						$mail->Body    	.= 	'<tr>
												<td width="35%" style="border: 1px #336699 solid;  background: #ccffff;" valign="top">
													<h1>'.date("H:i A", strtotime($job_details['job_time'])).'</h1>
													'.date("l, d F, Y", strtotime($job_details['job_date'])).'
												</td>
												<td width="65%" style="border: 1px #336699 solid;">
													<table width="100%" cellpadding="5">
														<tr>
															<td colspan="3" style="border-bottom: 2px #000 dashed;">';
															if($job_reference_details['std_fname'] != '')
																{
																	$mail->Body.= '<h2>'.$student_name.' (ID - '.$job_reference_details['std_id'].')</h2>';
																}
															else
																{
																	$mail->Body.= '<h2>'.$pax_details['title'].' '.$pax_details['fname'].' '.$pax_details['lname'].'</h2>';
																}
															$mail->Body.= '
															</td>
														<tr>
															<td valign="center" width="45%" style="background:#edeefc;">';
																if($job_details['frm_flight_no']!='')	{ $mail->Body.= '<h3>Flight No. -'.$job_details['frm_flight_no'].'</h3>';}
																if($job_details['frm_line1']!='')		{ $mail->Body.= ''.$job_details['frm_line1'].'<br/>';}
																if($job_details['frm_line2']!='')		{ $mail->Body.= ''.$job_details['frm_line2'].'<br/>';}
																if($job_details['frm_sub']!='')			{ $mail->Body.= '<b>'.$job_details['frm_sub'].'</b>';}
																if($job_details['frm_pc']!='')			{ $mail->Body.= ' '.$job_details['frm_pc'].'';}
																if($job_details['frm_state']!='')		{ $mail->Body.= ' '.$job_details['frm_state'].'<br/>';}
																
																if($job_details['frm_via_sub'] != '')
																	{
																		$mail->Body.='<strong>2nd PICKUP-DROP OFF</strong><br/>';
																		if($job_details['frm_via_line1']!='')	{ $mail->Body.= '<h3>'.$job_details['frm_via_line1'].'</h3>';}
																		if($job_details['frm_via_line2']!='')	{ $mail->Body.= ''.$job_details['frm_via_line2'].'<br/>';}
																		if($job_details['frm_via_sub']!='')		{ $mail->Body.= '<b>'.$job_details['frm_via_sub'].'</b> ';}
																		if($job_details['frm_via_pc']!='')		{ $mail->Body.= ' '.$job_details['frm_via_pc'].'</b>';}
																		if($job_details['frm_via_state']!='')	{ $mail->Body.= ' '.$job_details['frm_via_state'].'';}
																	}
																	
															$mail->Body.= 
															'</td>
															<td width="10%" valign="center"><h1>&#8594;</h1></td>
															<td valign="center" width="45%" style="background:#edeefc;">';
																if($job_details['to_flight_no']!='')	{ $mail->Body.= '<h3>Flight No. - '.$job_details['to_flight_no'].'</h3>';}
																if($job_details['to_line1']!='')		{ $mail->Body.= ''.$job_details['to_line1'].'<br/>';}
																if($job_details['to_line2']!='')		{ $mail->Body.= ''.$job_details['to_line2'].'<br/>';}
																if($job_details['to_sub']!='')			{ $mail->Body.= '<b>'.$job_details['to_sub'].'</b>';}
																if($job_details['to_pc']!='')			{ $mail->Body.= ' '.$job_details['to_pc'].'';}
																if($job_details['to_state']!='')		{ $mail->Body.= ' '.$job_details['to_state'].'<br/>';}
																if($job_details['to_via_sub'] != '')
																	{
																		$mail->Body.='<strong>LAST DROP</strong><br/>';
																		if($job_details['to_via_line1']!='')	{ $mail->Body.= ''.$job_details['to_via_line1'].'<br/>';}
																		if($job_details['to_via_line2']!='')	{ $mail->Body.= ''.$job_details['to_via_line2'].'<br/>';}
																		if($job_details['to_via_sub']!='')		{ $mail->Body.= '<b>'.$job_details['to_via_sub'].'</b>';}
																		if($job_details['to_via_pc']!='')		{ $mail->Body.= ' '.$job_details['to_via_pc'].'';}
																		if($job_details['to_via_state']!='')	{ $mail->Body.= ' '.$job_details['to_via_state'].'';}
																	}
															$mail->Body.= 
															'</td>	
														</tr>
														<tr>
															<td colspan="3" style="background:#ffffcc;">';
																if($job_details['car_id']!='')		{ $mail->Body.= 'Vehicle Requested. - '.$vehicle->getCarTypeType($job_details['car_id']).'<br/>';}
																if($job_details['pax_nos']!='')		{ $mail->Body.= 'No. of Passengers - '.$job_details['pax_nos'].'<br/>';}
																if($job_details['luggage'] > 0)		{ $mail->Body.= 'No. of Luggage - '.$job_details['luggage'].'<br/>';}
																if($job_details['baby_capsule']>0)	{ $mail->Body.= 'No. of Baby Capsules - '.$job_details['baby_capsule'].'<br/>';}
																if($job_details['baby_seat']>0)		{ $mail->Body.= 'No. of Baby Seats - '.$job_details['baby_seat'].'<br/>';}
																if($job_details['booster_seat']>0)	{ $mail->Body.= 'No. of Booster Seats - '.$job_details['booster_seat'].'<br/>';}
															$mail->Body.= 
															'</td style="border-top: 1px #000 solid;">
														<tr>
															<td colspan="3" style="background:#ffffcc;"><h2>$'.$row['job_price'].'</h2></td>
														</tr>';
														if($row['job_notes'] != '')
															{
																$mail->Body.= '<tr><td colspan="3" style="background:#ffffcc;"><b>Notes</b> - '.$row['job_notes'].'</td></tr>';
															}
													$mail->Body.= '
													</table>
												</td>
											</tr>';
					}
								$mail->Body .= '
										</table>
										<br/>
										<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0" style="border-collapse: collapse; border-color: #fff; background: #fff;">
											<tr>
												<td><b><a href="'.ROOT_ADDRESS.'external_mail_script/driver_bulk_decision.php?unique_code='.$encoded_unique_code.'&driver_id='.$encoded_driver_id.'&decision_status=2">ROGER AND CONFIRM</a ><b/></td>
											</tr>
										</table><br/><br/>';
										
				$mail->Body    .= self::BODY_END;
				$mail->send();
			}	

//-----------------------send list of jobs on bulk accept - here the acceptance is done by the admin--------------------------------------------------------/
		public function sendBulkJobAcceptDetailsToADriver($driver_id, $jobs_array)
			{
				require_once(CLASSES_PATH . "vehicle.php");
				
				$mail 	= new PHPMailer;
				$mail->CharSet = "UTF-8";
				
				$job = new job();
				$user = new user();
				$job_reference = new jobReference();
				$vehicle = new Vehicle();
				
				$encoded_driver_id 		= 	base64_encode(base64_encode($driver_id));
				$unique_code 			= 	substr(md5(uniqid(mt_rand(), true)) , 0, 8);
				$encoded_unique_code 	= 	base64_encode(base64_encode($unique_code));
				$driver_details 		= 	$user->getUserDetails($driver_id);
				
				$mail->From 	= 'alliedcars@alliedcars.com.au';
				$mail->FromName = 'Allied Cars';
				$mail->addAddress($driver_details['email'], $driver_details['title'].' '.$driver_details['fname'].' '.$driver_details['lname']);  // Add a recipient

				$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
				$mail->WordWrap = 50;   // Set word wrap to 50 characters
				$mail->isHTML(true);

				$mail->Subject 		= 	'Job details of '.$number_of_jobs.' Jobs Accepted from Allied Cars';
				$mail->Body    		= 	self::DOCUMENT_HEADER;
				$mail->Body    		.= 	'The following <b>'.$number_of_jobs.'</b> Jobs have been accepted by you. Please read the details carefully and ROGER.<br/><br/>';
				$mail->Body    		.= 	'<table align="left" width="60%" cellpadding="5" cellspacing="0" style="border: 1px #000 solid; border-collapse: collapse;">';
				
				$number_of_jobs 	= 	count($jobs_array);
				for($i=0; $i<$number_of_jobs; $i++)
					{
						$database = new database;
						$q = "INSERT INTO job__bulk_accept (unique_code, job_id, driver_id, job_price, job_notes)
						VALUES('".$unique_code."', '".$jobs_array[$i]['job_id']."', '".$driver_id."', '".$jobs_array[$i]['job_price']."', '".$jobs_array[$i]['driver_notes']."')";
						$result = $database->query($q);
						
						$job_details 	= 	$job->getJobDetails($jobs_array[$i]['job_id']);
						$job_amount		=	$jobs_array[$i]['job_price'];
						$driver_notes	=	$jobs_array[$i]['driver_notes'];

						$job_reference_details 	= 	$job_reference->getJobReferenceDetails($job_details['job_reference_id']);
						$pax_details 			= 	$user->getUserDetails($job_reference_details['pax_id']);
						$mail->Body    	.= 	'<tr>
												<td width="35%" style="border: 1px #336699 solid;  background: #ccffff;" valign="top">
													<h1>'.date("H:i A", strtotime($job_details['job_time'])).'</h1>
													'.date("l, d F, Y", strtotime($job_details['job_date'])).'
												</td>
												<td width="65%" style="border: 1px #336699 solid;">
													<table width="100%" cellpadding="5">
														<tr>
															<td colspan="3" style="border-bottom: 2px #000 dashed;">';
															if($job_reference_details['std_fname'] != '')
																{
																	$mail->Body.= '<h2>'.$student_name.' (ID - '.$job_reference_details['std_id'].')</h2>';
																}
															else
																{
																	$mail->Body.= '<h2>'.$pax_details['title'].' '.$pax_details['fname'].' '.$pax_details['lname'].'</h2>';
																}
															$mail->Body.= '
															</td>
														<tr>
															<td valign="center" width="45%" style="background:#edeefc;">';
																if($job_details['frm_flight_no']!='')	{ $mail->Body.= '<h3>Flight No. -'.$job_details['frm_flight_no'].'</h3>';}
																if($job_details['frm_line1']!='')		{ $mail->Body.= ''.$job_details['frm_line1'].'<br/>';}
																if($job_details['frm_line2']!='')		{ $mail->Body.= ''.$job_details['frm_line2'].'<br/>';}
																if($job_details['frm_sub']!='')			{ $mail->Body.= '<b>'.$job_details['frm_sub'].'</b>';}
																if($job_details['frm_pc']!='')			{ $mail->Body.= ' '.$job_details['frm_pc'].'';}
																if($job_details['frm_state']!='')		{ $mail->Body.= ' '.$job_details['frm_state'].'<br/>';}
																
																if($job_details['frm_via_sub'] != '')
																	{
																		$mail->Body.='<strong>2nd PICKUP-DROP OFF</strong><br/>';
																		if($job_details['frm_via_line1']!='')	{ $mail->Body.= '<h3>'.$job_details['frm_via_line1'].'</h3>';}
																		if($job_details['frm_via_line2']!='')	{ $mail->Body.= ''.$job_details['frm_via_line2'].'<br/>';}
																		if($job_details['frm_via_sub']!='')		{ $mail->Body.= '<b>'.$job_details['frm_via_sub'].'</b> ';}
																		if($job_details['frm_via_pc']!='')		{ $mail->Body.= ' '.$job_details['frm_via_pc'].'</b>';}
																		if($job_details['frm_via_state']!='')	{ $mail->Body.= ' '.$job_details['frm_via_state'].'';}
																	}
																	
															$mail->Body.= 
															'</td>
															<td width="10%" valign="center"><h1>&#8594;</h1></td>
															<td valign="center" width="45%" style="background:#edeefc;">';
																if($job_details['to_flight_no']!='')	{ $mail->Body.= '<h3>Flight No. - '.$job_details['to_flight_no'].'</h3>';}
																if($job_details['to_line1']!='')		{ $mail->Body.= ''.$job_details['to_line1'].'<br/>';}
																if($job_details['to_line2']!='')		{ $mail->Body.= ''.$job_details['to_line2'].'<br/>';}
																if($job_details['to_sub']!='')			{ $mail->Body.= '<b>'.$job_details['to_sub'].'</b>';}
																if($job_details['to_pc']!='')			{ $mail->Body.= ' '.$job_details['to_pc'].'';}
																if($job_details['to_state']!='')		{ $mail->Body.= ' '.$job_details['to_state'].'<br/>';}
																if($job_details['to_via_sub'] != '')
																	{
																		$mail->Body.='<strong>LAST DROP</strong><br/>';
																		if($job_details['to_via_line1']!='')	{ $mail->Body.= ''.$job_details['to_via_line1'].'<br/>';}
																		if($job_details['to_via_line2']!='')	{ $mail->Body.= ''.$job_details['to_via_line2'].'<br/>';}
																		if($job_details['to_via_sub']!='')		{ $mail->Body.= '<b>'.$job_details['to_via_sub'].'</b>';}
																		if($job_details['to_via_pc']!='')		{ $mail->Body.= ' '.$job_details['to_via_pc'].'';}
																		if($job_details['to_via_state']!='')	{ $mail->Body.= ' '.$job_details['to_via_state'].'';}
																	}
															$mail->Body.= 
															'</td>	
														</tr>
														<tr>
															<td colspan="3" style="background:#ffffcc;">';
																if($job_details['car_id']!='')		{ $mail->Body.= 'Vehicle Requested. - '.$vehicle->getCarTypeType($job_details['car_id']).'<br/>';}
																if($job_details['pax_nos']!='')		{ $mail->Body.= 'No. of Passengers - '.$job_details['pax_nos'].'<br/>';}
																if($job_details['luggage'] > 0)		{ $mail->Body.= 'No. of Luggage - '.$job_details['luggage'].'<br/>';}
																if($job_details['baby_capsule']>0)	{ $mail->Body.= 'No. of Baby Capsules - '.$job_details['baby_capsule'].'<br/>';}
																if($job_details['baby_seat']>0)		{ $mail->Body.= 'No. of Baby Seats - '.$job_details['baby_seat'].'<br/>';}
																if($job_details['booster_seat']>0)	{ $mail->Body.= 'No. of Booster Seats - '.$job_details['booster_seat'].'<br/>';}
															$mail->Body.= 
															'</td style="border-top: 1px #000 solid;">
														<tr>
															<td colspan="3" style="background:#ffffcc;"><h2>$'.$job_details['driver_price'].'</h2></td>
														</tr>';
														if($job_details['job_notes'] != '')
															{
																$mail->Body.= '<tr><td colspan="3" style="background:#ffffcc;"><b>Job Notes</b> - '.$job_details['job_notes'].'</td></tr>';
															}
														if($job_details['driver_notes'] != '')
															{
																$mail->Body.= '<tr><td colspan="3" style="background:#ffffcc;"><b>Driver Notes</b> - '.$job_details['driver_notes'].'</td></tr>';
															}
													$mail->Body.= '
													</table>
												</td>
											</tr>';
					}
								$mail->Body .= '
										</table>
										<br/>
										<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0" style="border-collapse: collapse; border-color: #fff; background: #fff;">
											<tr>
												<td><b><a href="'.ROOT_ADDRESS.'external_mail_script/driver_bulk_decision.php?unique_code='.$encoded_unique_code.'&driver_id='.$encoded_driver_id.'&decision_status=2">ROGER AND CONFIRM</a ><b/></td>
											</tr>
										</table><br/><br/>';
										
				$mail->Body    .= self::BODY_END;
				$mail->send();
			}									
			

/*-----------------------------------------------------------------------------------------------------------*/				
		public function mass_email($first_char, $subject, $body)
			{
				$mail = new PHPMailer;
				$user = new User();
				
				$mail->From 	= 'alliedcars@alliedcars.com.au';
				$mail->FromName = 'Allied Cars';
				
				$all_driver_details = $user->getAllDriverDetailsOfChar($first_char); // get details of this driver
						
				foreach($all_driver_details as $driver_details)
				{ 
				
					$mail->Body    = self::DOCUMENT_HEADER;
					$mail->Body    .= self::BODY_START;
					$mail->Body    .= self::EMAIL_HTML_HEADER;

					//$mail->Body    .= '<strong>Booking Confirmation</strong></td></tr></table><br/>';
					$mail->Body    .= '<strong>Dear '.$driver_details['title'].' '.$driver_details['fname'].' '.$driver_details['lname'].',</strong><br/><br/>';
					
					 
					$mail->Body    .= $body;
					
					$mail->Body    .= "<table align='center' width='100%'  cellpadding='5' cellspacing='0'>
										<tr>
											<td>
											Thank you for booking with us.<br/>Kind Regards, <br /><br/><strong>Allied Chauffeured Cars Australia</strong><br/>ABN: 38 076 977 136<br /><br/>
											<strong>Email:</strong> alliedcars@alliedcars.com.au <br />
											<strong>Telephone:</strong> 61 3 8383 9999 <br /><br/>
											</td>
										</tr>
										<tr>
											<td>
												New bookings or bookings cancellations and or amendments are valid only on receipt of our confirmation. <br /><br />
												Cancellations and or amendments for sedans need at least 2 hours notice, 4 hours in New Zealand and 24 hours in Darwin. <br /><br />
												Minibuses and Coaches 24 hours.  Major events 48 hours cancellation and or amendment notice. <br /><br />
												Please be aware that our office closes on public holidays. Except on Melbourne Cup Day. <br /><br />
												All new bookings or bookings cancellations and or amendments must be made through the office, during office hours.<br /><br />
												<br />Our car's service is 24 hours a day, 
												<br />Our office hours are
												<br />Monday to Friday  8am to 8pm
												<br />Saturday &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1pm to 5pm
												<br />Sunday &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1pm to 8pm
											</td>
										</tr>
									</table>";
					$mail->Body    .= self::EMAIL_HTML_FOOTER;
					$mail->Body    .= self::BODY_END;
				
					$mail->addAddress($driver_details['email'], ''.$driver_details['title'].' '.$driver_details['fname'].' '.$driver_details['lname'].'');  // Add a recipient
					//$mail->addAddress('sucheta_sw@yahoo.co.in', ''.$to_title.' '.$to_fname.' '.$to_lname.'');  // Add a recipient
					$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
					$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
					$mail->isHTML(true);                                  // Set email format to HTML
					$mail->Subject = $subject;
					$mail->AltBody = $mail->Body;
					if(!$mail->send()) 
					{
					   $message =  'Message could not be sent.';
					   $message =  'Mailer Error: ' . $mail->ErrorInfo;
					   exit;
					}
				}
			}	

//----------------------------------------------------------------------------------------------------------------------/					
		public function sendMailToMonashClaytonStudent($job_reference_id, $email_type, $receiver_email, $receiver_title, $receiver_fname, $receiver_lname)
			{
				//initialize classes
				$mail 			= new PHPMailer;
				$job_reference 	= new JobReference();
				$job 			= new Job();
				$chargeaccounts = new chargeAccount();
				
				$job_reference_details 	= $job_reference->getJobReferenceDetails($job_reference_id);
				$j1_details 			= $job->getJobDetails($job_reference_details['j1_id']);
				$chargeaccount_details = $chargeaccounts->getChargeAccountDetails($job_reference_details['charge_acc_id']);
					
				$booking_html = $job->getHTMLofWholeBooking($job_reference_id);
				
				$mail->From 	= 'alliedcars@alliedcars.com.au';
				$mail->FromName = 'Allied Cars';
				$mail->addAddress($receiver_email, ''.$receiver_title.' '.$receiver_fname.' '.$receiver_lname.'');  // Add a recipient
				$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
				$mail->WordWrap = 50;
				$mail->isHTML(true);
				
				
				if($job_reference_details['job_type'] == '1')
					{
						$mail->Subject = "Confirmation of Booking ID - ".$job_reference_details['j1_id']."";
					}
				if($job_reference_details['job_type'] == '2')
					{
						$mail->Subject = "Confirmation of Booking IDs - ".$job_reference_details['j1_id']." and ".$job_reference_details['j2_id']."";
					}
					
				
				$mail->Body    .= self::DOCUMENT_HEADER;
				$mail->Body    .= self::BODY_START;
				$mail->Body    .= self::EMAIL_HTML_HEADER;
				$mail->Body    .= '<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0" style="border-collapse: collapse; border-color: #fff; background: #45cb37;">
										<tr>
											<td align="left" style="color: #ffffff; font-family: Arial, sans-serif; font-size: 200%">';
				
				
				$mail->Body    .= '<strong>Booking Confirmation</strong></td></tr></table><br/>';
				$mail->Body    .= '<strong>Dear '.$receiver_title.' '.$receiver_fname.' '.$receiver_lname.',</strong><br/><br/>';
				
				$mail->Body  .="Thank you for booking with us and we are now pleased to confirm your arrival details. <br />
											Please review the booking details and let us know immediately if they are not accurate. <br /><br />
											<strong>IMPORTANT INFORMATION</strong><br/><br/>
											If you are arriving from an International Flight, clear customs and make your way into the arrivals area (turn right when exiting Customs), where your driver will be waiting for you near Yes Optus shop and Travelex desk.<br /><br />
											If you are arriving from a Domestic Flight, make your way to the arrivals area in the international terminal and your driver will be waiting for you near Yes Optus shop and Travelex desk. <br /><br />
											If you can't find your driver once you arrive in Melbourne please ring Allied on our toll free number <strong>1800 350 850.</strong> <br /><br />
											<STRONG>Please note,</STRONG> You need to inform Allied Chauffeured Cars if your arrival date and flight number change as well as any relevant information about your pickup. ".$chargeaccount_details['account_name']." . <br /><br />
											You can contact us by email alliedcars@alliedcars.com.au or by phone <strong>(+61 3) 8383 9999.</strong> <br /><br />
											"; 
				$mail->Body    .= $booking_html;
				
				$mail->Body    .= "<table align='center' width='100%'  cellpadding='5' cellspacing='0'>
									<tr>
										<td>
										Thank you for booking with us.<br/>Kind Regards, <br /><br/><strong>Allied Chauffeured Cars Australia</strong><br/>ABN: 38 076 977 136<br /><br/>
										<strong>Email:</strong> alliedcars@alliedcars.com.au <br />
										<strong>Telephone:</strong> 61 3 8383 9999 <br /><br/>
										</td>
									</tr>
									<tr>
										<td>
											New bookings or bookings cancellations and or amendments are valid only on receipt of our confirmation. <br /><br />
											Cancellations and or amendments for sedans need at least 2 hours notice, 4 hours in New Zealand and 24 hours in Darwin. <br /><br />
											Minibuses and Coaches 24 hours.  Major events 48 hours cancellation and or amendment notice. <br /><br />
											Please be aware that our office closes on public holidays. Except on Melbourne Cup Day. <br /><br />
											All new bookings or bookings cancellations and or amendments must be made through the office, during office hours.<br /><br />
											<br />Our car's service is 24 hours a day, 
											<br />Our office hours are
											<br />Monday to Friday  8am to 7pm
											<br />Saturday &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1pm to 5pm
											<br />Sunday &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1pm to 7pm
										</td>
									</tr>
								</table>";
				$mail->Body    .= self::EMAIL_HTML_FOOTER;
				$mail->Body    .= self::BODY_END;
				$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Airport_Map.pdf",'Airport_Map.pdf','base64','application/pdf');  
				
				$mail->send();
			}
//----------------------------------------------------------------------------------------------------------------------/					
		public function sendMailToMonashCollegeStudent($job_reference_id, $email_type, $receiver_email, $receiver_title, $receiver_fname, $receiver_lname)
			{
				//initialize classes
				
				$mail 			= new PHPMailer;
				$job_reference 	= new JobReference();
				$job 			= new Job();
				$chargeaccounts = new chargeAccount();
				
				$job_reference_details 	= $job_reference->getJobReferenceDetails($job_reference_id);
				$j1_details 			= $job->getJobDetails($job_reference_details['j1_id']);
				$chargeaccount_details = $chargeaccounts->getChargeAccountDetails($job_reference_details['charge_acc_id']);
					
				$booking_html = $job->getHTMLofWholeBooking($job_reference_id);
				
				$mail->From 	= 'alliedcars@alliedcars.com.au';
				$mail->FromName = 'Allied Cars';
				$mail->addAddress($receiver_email, ''.$receiver_title.' '.$receiver_fname.' '.$receiver_lname.'');  // Add a recipient
				$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
				$mail->WordWrap = 50;
				$mail->isHTML(true);
				
				
				if($job_reference_details['job_type'] == '1')
					{
						$mail->Subject = "Confirmation of Booking ID - ".$job_reference_details['j1_id']."";
					}
				if($job_reference_details['job_type'] == '2')
					{
						$mail->Subject = "Confirmation of Booking IDs - ".$job_reference_details['j1_id']." and ".$job_reference_details['j2_id']."";
					}
				$mail->Body    .= self::DOCUMENT_HEADER;
				$mail->Body    .= self::BODY_START;
				$mail->Body    .= self::EMAIL_HTML_HEADER;
				$mail->Body    .= '<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0" style="border-collapse: collapse; border-color: #fff; background: #45cb37;">
										<tr>
											<td align="left" style="color: #ffffff; font-family: Arial, sans-serif; font-size: 200%">';
				
				
				$mail->Body    .= '<strong>PICK UP CONFIRMATION</strong></td></tr></table><br/>';
				$mail->Body    .= '<strong>Dear '.$receiver_title.' '.$receiver_fname.' '.$receiver_lname.',</strong><br/><br/>';
				$mail->Body    .='<strong>IF YOU CAN\'T FIND YOUR DRIVER, FIND A PUBLIC PHONE AND RING ALLIED ON 1800 350 850</STRONG> (this is a <strong>free</strong> call, <strong>NO</strong> coins needed)<br /><br />';
				$mail->Body    .='<strong>YOU NEED TO PRINT THIS CONFIRMATION EMAIL AND BRING IT WITH YOU TO SHOW THE ALLIED REPRESENTATIVE.</strong><br /><br />';
				$mail->Body  .="Thank you for your airport pickup request as per details below. <br/><br/>
											Please review your booking details and let us know immediately if they are not accurate. <br /><br />
											<strong>IMPORTANT INFORMATION</strong><br/><br/>
											A driver from Allied Chauffeured Cars (Allied) will meet you in Melbourne Airport's International Arrival Hall T2. If you are arriving on an International Flight, turn right when you leave the customs area and go towards 'The Meeting Point' (near Yes Optus Mobile Shop).
											Your driver will hold an 'ALLIED' sign for easy identification and will take you to your destination as per your instructions below.<br /><br />
											If you are arriving on a Domestic Flight, make your way to the arrivals area in the international terminal T2. <br /><br />
											<strong>No driver waiting at Melbourne Airport</strong><br/><br/>
											Go to 'The Meeting Point'.  If your driver is not there within 15 minutes find the nearest public telephone and call Allied on 1800 350 850 (this is a <strong>free</strong> call, <strong>NO</strong> coins needed) so they can locate your driver. When calling, give Allied your Monash Student ID number (this is in your Monash offer letter), family name and given name.<br/><br/>
											<STRONG>Please note,</STRONG> if you do not arrive at your booked time or do not arrive at all without informing us, you will have to pay a <strong>cancellation fee of A$100 </strong> to ".$chargeaccount_details['account_name']." . <br /><br />
											You can contact us by email alliedcars@alliedcars.com.au or by phone <strong>(+61 3) 8383 9999.</strong> <br /><br />
											<strong>City of Melbourne - International Student Welcome Booth</strong><br/><br/>
											The City of Melbourne operates the 'Student Welcome Desk' at Melbourne Airport where volunteers greet you and provide information about welfare services, public transport, housing, employment options and social integration to help you with your stay in Melbourne.<br/><br/> 
											<strong>Changes to or cancellation of your flight</strong><br/><br/>
											To change or cancel your airport pickup booking, contact us at least one full working day before you arrive to avoid additional costs.  Our office hours are 9am to 5pm Monday to Friday, Australian Eastern Standard Time.<br/><br/> 
											To change your arrival details on Saturday or Sunday, telephone +61 3 8383 9999.<br/><br/> 
											If you are overseas and miss your flight or if your flight is delayed, please telephone +61 3 8383 9999 immediately to confirm your new arrival details.  This will ensure your driver is at Melbourne Airport to meet you.<br/><br/>
											<strong>Enrolment</strong><br/><br/>
											Each course has its own enrolment date, time and location.<br/><br/> 
											Monash College students:  https://www.monashcollege.edu.au/live-and-study-in-australia/prepare-to-arrive/enrolment-and-orientation <br/><br/>
											<strong>Orientation program</strong><br/><br/>
											If you are studying the English Language, Foundation Year or Monash College Diploma program, your enrolment and orientation information can be found at:<br/>
											https://www.monashcollege.edu.au/live-and-study-in-australia/prepare-to-arrive/enrolment-and-orientation <br/><br/>
											You must attend Orientation  You will get a lot of valuable information about studying at Monash including your enrolment, study timetable and learning how to access Monash University's computer resources.<br/><br/>  
											Orientation includes sessions on academic and study skills, Australian culture, campus tours and social opportunities to meet other students.<br/><br/>
											We wish you a pleasant journey and look forward to welcoming you to Monash.<br/><br/>
											Kind regards<br/>
											Monash College<br/><br/>

											"; 
				$mail->Body    .= $booking_html;
				
				$mail->Body    .= "<table align='center' width='100%'  cellpadding='5' cellspacing='0'>
									<tr>
										<td>
										Thank you for booking with us.<br/>Kind Regards, <br /><br/><strong>Allied Chauffeured Cars Australia</strong><br/>ABN: 38 076 977 136<br /><br/>
										<strong>Email:</strong> alliedcars@alliedcars.com.au <br />
										<strong>Telephone:</strong> 61 3 8383 9999 <br /><br/>
										</td>
									</tr>
									<tr>
										<td>
											New bookings or bookings cancellations and or amendments are valid only on receipt of our confirmation. <br /><br />
										</td>
									</tr>
								</table>";
				$mail->Body    .= self::EMAIL_HTML_FOOTER;
				$mail->Body    .= self::BODY_END;
				$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Airport_Map.pdf",'Airport_Map.pdf','base64','application/pdf');  
				
				$mail->send();
			}
//----------------------------------------------------------------------------------------------------------------------/					
		public function sendMailToMonashAbroadStudent($job_reference_id, $email_type, $receiver_email, $receiver_title, $receiver_fname, $receiver_lname)
			{
				//initialize classes
				
				$mail 			= new PHPMailer;
				$job_reference 	= new JobReference();
				$job 			= new Job();
				$chargeaccounts = new chargeAccount();
				
				$job_reference_details 	= $job_reference->getJobReferenceDetails($job_reference_id);
				$j1_details 			= $job->getJobDetails($job_reference_details['j1_id']);
				$chargeaccount_details = $chargeaccounts->getChargeAccountDetails($job_reference_details['charge_acc_id']);
					
				$booking_html = $job->getHTMLofWholeBooking($job_reference_id);
				
				$mail->From 	= 'alliedcars@alliedcars.com.au';
				$mail->FromName = 'Allied Cars';
				$mail->addAddress($receiver_email, ''.$receiver_title.' '.$receiver_fname.' '.$receiver_lname.'');  // Add a recipient
				$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
				$mail->WordWrap = 50;
				$mail->isHTML(true);
				
				if($job_reference_details['job_type'] == '1')
					{
						$mail->Subject = "Confirmation of Booking ID - ".$job_reference_details['j1_id']."";
					}
				if($job_reference_details['job_type'] == '2')
					{
						$mail->Subject = "Confirmation of Booking IDs - ".$job_reference_details['j1_id']." and ".$job_reference_details['j2_id']."";
					}
					
				
				$mail->Body    .= self::DOCUMENT_HEADER;
				$mail->Body    .= self::BODY_START;
				$mail->Body    .= self::EMAIL_HTML_HEADER;
				$mail->Body    .= '<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0" style="border-collapse: collapse; border-color: #fff; background: #45cb37;">
										<tr>
											<td align="left" style="color: #ffffff; font-family: Arial, sans-serif; font-size: 200%">';
				
				
				$mail->Body    .= '<strong>PICK UP CONFIRMATION</strong></td></tr></table><br/>';
				$mail->Body    .= '<strong>Dear '.$receiver_title.' '.$receiver_fname.' '.$receiver_lname.',</strong><br/><br/>';
				
				$mail->Body  .="Thank you for booking with us and we are now pleased to confirm your arrival details. <br /><br />
												Please review the booking details and let us know immediately if they are not accurate. <br /><br />
												<strong>IMPORTANT INFORMATION</strong><br/><br/>
												It is <strong>very</strong> important for <strong>Allied Chauffeured Cars </strong> to know how much luggage you are going to have, if you haven't done so please email <strong>Allied Chauffeured Cars</strong> to let them know. <br /><br />
												Our staff will meet you at the airport. They will be holding a placard <strong>\"ALLIED\"</strong>. <br /><br />
												If you are arriving from an International Flight, clear customs and make your way into the arrivals area (turn right when exiting Customs), where your driver will be waiting for you near Yes Optus shop and Travelex desk.<br /><br />
												If you are arriving from a Domestic Flight, make your way to the arrivals area in the international terminal and your driver will be waiting for you near Yes Optus shop and Travelex desk. <br /><br />
												<strong>No driver waiting at Melbourne Airport</strong><br/><br/>
												Go to 'The Meeting Point'. If your driver is not there within 15 minutes find the nearest public telephone and call Allied <strong>1800 350 850.</strong> (this is a <strong>free</strong> call, <strong>NO</strong> coins needed) so they can locate your driver. 
												When calling, give Allied your Monash Student ID number (this is in your Monash offer letter), family name and given name.<br /><br />
												It is <strong>very</strong> important to find your driver as soon as you clear customs. You have a <strong>maximum</strong> of<strong> forty minutes</strong> from the moment that the plane lands to meet your driver.<br /><br />
												You can contact us by email alliedcars@alliedcars.com.au or by phone <strong>(+61 3) 8383 9999.</strong> <br /><br />
												<strong>City of Melbourne - International Student Welcome Booth</strong><br/><br/>
												The City of Melbourne operates the 'Student Welcome Desk' at Melbourne Airport where volunteers greet you and provide information about welfare services, 
												public transport, housing, employment options and social integration to help you with your stay in Melbourne.<br/><br/>
												<strong>Changes to or cancellation of your flight</strong><br/><br/>
												To change or cancel your airport pickup booking, contact us at least one full working day before you arrive to avoid additional costs. Our office hours are 9am to 5pm Monday to Friday, Australian Eastern Standard Time.<br/><br/>
												To change your arrival details on Saturday or Sunday, telephone +61 3 8383 9999.<br/><br/>
												If you are overseas and miss your flight or if your flight is delayed, please telephone +61 3 8383 9999 immediately to confirm your new arrival details. This will ensure your driver is at Melbourne Airport to meet you.<br/><br/>
												<strong>Please note,</strong> if you do not arrive at your booked time or do not arrive at all without informing us, you will have to pay a <strong>cancellation fee of A$100</strong> to ".$chargeaccount_details['account_name']." . <br /><br />
												";  
				$mail->Body    .= $booking_html;
				
				$mail->Body    .= "<table align='center' width='100%'  cellpadding='5' cellspacing='0'>
									<tr>
										<td>
										Thank you for booking with us.<br/>Kind Regards, <br /><br/><strong>Allied Chauffeured Cars Australia</strong><br/>ABN: 38 076 977 136<br /><br/>
										<strong>Email:</strong> alliedcars@alliedcars.com.au <br />
										<strong>Telephone:</strong> 61 3 8383 9999 <br /><br/>
										</td>
									</tr>
									<tr>
										<td>
											New bookings or bookings cancellations and or amendments are valid only on receipt of our confirmation. <br /><br />
										</td>
									</tr>
								</table>";
				$mail->Body    .= self::EMAIL_HTML_FOOTER;
				$mail->Body    .= self::BODY_END;
				$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Airport_Map.pdf",'Airport_Map.pdf','base64','application/pdf');  
				$mail->send();
			}
//----------------------------------------------------------------------------------------------------------------------/					
		public function sendMailToMelbourneUniversityStudent($job_reference_id, $email_type, $receiver_email, $receiver_title, $receiver_fname, $receiver_lname)
			{
				//initialize classes
				
				$mail 			= new PHPMailer;
				$job_reference 	= new JobReference();
				$job 			= new Job();
				$chargeaccounts = new chargeAccount();
				
				$job_reference_details 	= $job_reference->getJobReferenceDetails($job_reference_id);
				$j1_details 			= $job->getJobDetails($job_reference_details['j1_id']);
				$chargeaccount_details = $chargeaccounts->getChargeAccountDetails($job_reference_details['charge_acc_id']);
					
				$booking_html = $job->getHTMLofWholeBooking($job_reference_id);
				
				$mail->From 	= 'alliedcars@alliedcars.com.au';
				$mail->FromName = 'Allied Cars';
				$mail->addAddress($receiver_email, ''.$receiver_title.' '.$receiver_fname.' '.$receiver_lname.'');  // Add a recipient
				$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
				$mail->WordWrap = 50;
				$mail->isHTML(true);
				
				if($job_reference_details['job_type'] == '1')
					{
						$mail->Subject = "Confirmation of Booking ID - ".$job_reference_details['j1_id']."";
					}
				if($job_reference_details['job_type'] == '2')
					{
						$mail->Subject = "Confirmation of Booking IDs - ".$job_reference_details['j1_id']." and ".$job_reference_details['j2_id']."";
					}
					
				
				$mail->Body    .= self::DOCUMENT_HEADER;
				$mail->Body    .= self::BODY_START;
				$mail->Body    .= self::EMAIL_HTML_HEADER;
				$mail->Body    .= '<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0" style="border-collapse: collapse; border-color: #fff; background: #45cb37;">
										<tr>
											<td align="left" style="color: #ffffff; font-family: Arial, sans-serif; font-size: 200%">';
				
				
				$mail->Body    .= '<strong>Booking Confirmation</strong></td></tr></table><br/>';
				$mail->Body    .= '<strong>Dear '.$receiver_title.' '.$receiver_fname.' '.$receiver_lname.',</strong><br/><br/>';
				
				$mail->Body  .="Thank you for booking with us and we are now pleased to confirm your arrival details. <br /><br />
												Please review the booking details and let us know immediately if they are not accurate. <br /><br />
												<strong>IMPORTANT INFORMATION</strong><br/><br/>
												It is <strong>very</strong> important for <strong>Allied Chauffeured Cars </strong> to know how much luggage you are going to have, if you haven't done so please email <strong>Allied Chauffeured Cars</strong> to let them know. <br /><br />
												Our staff will meet you at the airport. They will be holding a placard <strong>\"ALLIED\"</strong>. <br /><br />
												If you are arriving from an International Flight, clear customs and make your way into the arrivals area (turn right when exiting Customs), where your driver will be waiting for you near Yes Optus shop and Travelex desk.<br /><br />
												If you are arriving from a Domestic Flight, make your way to the arrivals area in the international terminal and your driver will be waiting for you near Yes Optus shop and Groups and Travelex desk. <br /><br />
												If you can't find your driver once you arrive in Melbourne please ring Allied on our toll free number <strong>1800 350 850.</strong> <br /><br />
												You have <strong>forty five minutes</strong> from the moment that the plane lands to meet your driver.<br /><br />
												<STRONG>Please note,</STRONG> if you do not arrive at your booked time or do not arrive at all without informing us, you will have to pay a <strong>cancellation fee of A$95</strong> to ".$chargeaccount_details['account_name']." . <br /><br />
												You can contact us by email alliedcars@alliedcars.com.au or by phone <strong>(+61 3) 8383 9999.</strong> <br /><br />
												";  
				$mail->Body    .= $booking_html;
				
				$mail->Body    .= "<table align='center' width='100%'  cellpadding='5' cellspacing='0'>
									<tr>
										<td>
										Thank you for booking with us.<br/>Kind Regards, <br /><br/><strong>Allied Chauffeured Cars Australia</strong><br/>ABN: 38 076 977 136<br /><br/>
										<strong>Email:</strong> alliedcars@alliedcars.com.au <br />
										<strong>Telephone:</strong> 61 3 8383 9999 <br /><br/>
										</td>
									</tr>
									<tr>
										<td>
											New bookings or bookings cancellations and or amendments are valid only on receipt of our confirmation. <br /><br />
											Cancellations and or amendments for sedans need at least 2 hours notice, 4 hours in New Zealand and 24 hours in Darwin. <br /><br />
											Minibuses and Coaches 24 hours.  Major events 48 hours cancellation and or amendment notice. <br /><br />
											Please be aware that our office closes on public holidays. Except on Melbourne Cup Day. <br /><br />
											All new bookings or bookings cancellations and or amendments must be made through the office, during office hours.<br /><br />
											<br />Our car's service is 24 hours a day, 
											<br />Our office hours are
											<br />Monday to Friday  8am to 7pm
											<br />Saturday &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1pm to 5pm
											<br />Sunday &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1pm to 7pm
										</td>
									</tr>
								</table>";
				$mail->Body    .= self::EMAIL_HTML_FOOTER;
				$mail->Body    .= self::BODY_END;

				$mail->send();
			}
//----------------------------------------------------------------------------------------------------------------------/					
		public function sendMailToDeakinUniversityStudent($job_reference_id, $email_type, $receiver_email, $receiver_title, $receiver_fname, $receiver_lname)
			{
				//initialize classes
				
				$mail 			= new PHPMailer;
				$job_reference 	= new JobReference();
				$job 			= new Job();
				$chargeaccounts = new chargeAccount();
				
				$job_reference_details 	= $job_reference->getJobReferenceDetails($job_reference_id);
				$j1_details 			= $job->getJobDetails($job_reference_details['j1_id']);
				$chargeaccount_details = $chargeaccounts->getChargeAccountDetails($job_reference_details['charge_acc_id']);
					
				$booking_html = $job->getHTMLofWholeBooking($job_reference_id);
				
				$mail->From 	= 'alliedcars@alliedcars.com.au';
				$mail->FromName = 'Allied Cars';
				$mail->addAddress($receiver_email, ''.$receiver_title.' '.$receiver_fname.' '.$receiver_lname.'');  // Add a recipient
				$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
				$mail->WordWrap = 50;
				$mail->isHTML(true);
				
				if($job_reference_details['job_type'] == '1')
					{
						$mail->Subject = "Confirmation of Booking ID - ".$job_reference_details['j1_id']."";
					}
				if($job_reference_details['job_type'] == '2')
					{
						$mail->Subject = "Confirmation of Booking IDs - ".$job_reference_details['j1_id']." and ".$job_reference_details['j2_id']."";
					}
					
				
				$mail->Body    .= self::DOCUMENT_HEADER;
				$mail->Body    .= self::BODY_START;
				$mail->Body    .= self::EMAIL_HTML_HEADER;
				$mail->Body    .= '<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0" style="border-collapse: collapse; border-color: #fff; background: #45cb37;">
										<tr>
											<td align="left" style="color: #ffffff; font-family: Arial, sans-serif; font-size: 200%">';
				
				
				$mail->Body    .= '<strong>Booking Confirmation</strong></td></tr></table><br/>';
				$mail->Body    .= '<strong>Dear '.$receiver_title.' '.$receiver_fname.' '.$receiver_lname.',</strong><br/><br/>';
				
				$mail->Body  .="Thank you for booking with us and we are now pleased to confirm your arrival details. <br /><br />
												Please review the booking details and let us know immediately if they are not accurate. <br /><br />
												<strong>IMPORTANT INFORMATION</strong><br/><br/>
												It is <strong>very</strong> important for <strong>Allied Chauffeured Cars </strong> to know how much luggage you are going to have, if you haven't done so please email <strong>Allied Chauffeured Cars</strong> to let them know. <br /><br />
												Our staff will meet you at the airport. They will be holding a placard <strong>\"ALLIED\"</strong>. <br /><br />
												If you are arriving from an International Flight, clear customs and make your way into the arrivals area (turn right when exiting Customs), where your driver will be waiting for you near Yes Optus shop and Travelex desk.<br /><br />
												If you are arriving from a Domestic Flight, make your way to the arrivals area in the international terminal and your driver will be waiting for you near Yes Optus shop and Travelex desk. <br /><br />
												If you can't find your driver once you arrive in Melbourne please ring Allied on our toll free number <strong>1800 350 850.</strong> <br /><br />
												You have <strong>forty five minutes</strong> from the moment that the plane lands to meet your driver.<br /><br />
												<STRONG>Please note,</STRONG> if you do not arrive at your booked time or do not arrive at all without informing us, you will have to pay a <strong>cancellation fee of A$95</strong> to ".$chargeaccount_details['account_name']." . <br /><br />
												You can contact us by email alliedcars@alliedcars.com.au or by phone <strong>(+61 3) 8383 9999.</strong> <br /><br />
												";  
				$mail->Body    .= $booking_html;
				
				$mail->Body    .= "<table align='center' width='100%'  cellpadding='5' cellspacing='0'>
									<tr>
										<td>
										Thank you for booking with us.<br/>Kind Regards, <br /><br/><strong>Allied Chauffeured Cars Australia</strong><br/>ABN: 38 076 977 136<br /><br/>
										<strong>Email:</strong> alliedcars@alliedcars.com.au <br />
										<strong>Telephone:</strong> 61 3 8383 9999 <br /><br/>
										</td>
									</tr>
									<tr>
										<td>
											New bookings or bookings cancellations and or amendments are valid only on receipt of our confirmation. <br /><br />
											Cancellations and or amendments for sedans need at least 2 hours notice, 4 hours in New Zealand and 24 hours in Darwin. <br /><br />
											Minibuses and Coaches 24 hours.  Major events 48 hours cancellation and or amendment notice. <br /><br />
											Please be aware that our office closes on public holidays. Except on Melbourne Cup Day. <br /><br />
											All new bookings or bookings cancellations and or amendments must be made through the office, during office hours.<br /><br />
											<br />Our car's service is 24 hours a day, 
											<br />Our office hours are
											<br />Monday to Friday  8am to 7pm
											<br />Saturday &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1pm to 5pm
											<br />Sunday &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1pm to 7pm
										</td>
									</tr>
								</table>";
				$mail->Body    .= self::EMAIL_HTML_FOOTER;
				$mail->Body    .= self::BODY_END;
				$mail->send();
			}
/*-----------------------------------------------------------------------------------------------------------*/				
		public function sendMailToLatrobeUniversityStudent($job_reference_id, $email_type, $receiver_email, $receiver_title, $receiver_fname, $receiver_lname)
			{
				//initialize classes
				
				$mail 			= new PHPMailer;
				$job_reference 	= new JobReference();
				$job 			= new Job();
				$chargeaccounts = new chargeAccount();
				
				$job_reference_details 	= $job_reference->getJobReferenceDetails($job_reference_id);
				$j1_details 			= $job->getJobDetails($job_reference_details['j1_id']);
				$chargeaccount_details = $chargeaccounts->getChargeAccountDetails($job_reference_details['charge_acc_id']);
					
				$booking_html = $job->getHTMLofWholeBooking($job_reference_id);
				
				$mail->From 	= 'alliedcars@alliedcars.com.au';
				$mail->FromName = 'Allied Cars';
				$mail->addAddress($receiver_email, ''.$receiver_title.' '.$receiver_fname.' '.$receiver_lname.'');  // Add a recipient
				$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
				$mail->WordWrap = 50;
				$mail->isHTML(true);
				
				if($job_reference_details['job_type'] == '1')
				{
					$mail->Subject = "Confirmation of Booking ID - ".$job_reference_details['j1_id']."";
				}
				if($job_reference_details['job_type'] == '2')
				{
					$mail->Subject = "Confirmation of Booking IDs - ".$job_reference_details['j1_id']." and ".$job_reference_details['j2_id']."";
				}
					
				
				$mail->Body    .= self::DOCUMENT_HEADER;
				$mail->Body    .= self::BODY_START;
				$mail->Body    .= self::EMAIL_HTML_HEADER;
				$mail->Body    .= '<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0" style="border-collapse: collapse; border-color: #fff; background: #45cb37;">
										<tr>
											<td align="left" style="color: #ffffff; font-family: Arial, sans-serif; font-size: 200%">';
				
				
				$mail->Body    .= '<strong>Booking Confirmation</strong></td></tr></table><br/>';
				$mail->Body    .= '<strong>Dear '.$receiver_title.' '.$receiver_fname.' '.$receiver_lname.',</strong><br/><br/>';
				
				$mail->Body  .="Thank you for booking with us and we are now pleased to confirm your arrival details. <br /><br />
												Please review the booking details and let us know immediately if they are not accurate. <br /><br />
												<strong>IMPORTANT INFORMATION</strong><br/><br/>
												It is <strong>very</strong> important for <strong>Allied Chauffeured Cars </strong> to know how much luggage you are going to have, if you haven't done so please email <strong>Allied Chauffeured Cars</strong> to let them know. <br /><br />
												Our staff will meet you at the airport. They will be holding a placard <strong>\"Latrobe University\"</strong>. <br /><br />
												If you are arriving from an International Flight, clear customs and make your way into the arrivals area (turn right when exiting Customs), where your driver will be waiting for you near Yes Optus shop Travelex desk.<br /><br />
												If you are arriving from a Domestic Flight, make your way to the arrivals area in the international terminal and your driver will be waiting for you near Yes Optus shop and Travelex desk. <br /><br />
												If you can't find your driver once you arrive in Melbourne please ring Allied on our toll free number <strong>1800 350 850.</strong> <br /><br />
												You have <strong>forty five minutes</strong> from the moment that the plane lands to meet your driver.<br /><br />
												<STRONG>Please note,</STRONG> if you do not arrive at your booked time or do not arrive at all without informing us, you will have to pay a <strong>cancellation fee of A$95</strong> to ".$chargeaccount_details['account_name']." . <br /><br />
												You can contact us by email alliedcars@alliedcars.com.au or by phone <strong>(+61 3) 8383 9999.</strong> <br /><br />
												";  
				$mail->Body    .= $booking_html;
				
				$mail->Body    .= "<table align='center' width='100%'  cellpadding='5' cellspacing='0'>
									<tr>
										<td>
										Thank you for booking with us.<br/>Kind Regards, <br /><br/><strong>Allied Chauffeured Cars Australia</strong><br/>ABN: 38 076 977 136<br /><br/>
										<strong>Email:</strong> alliedcars@alliedcars.com.au <br />
										<strong>Telephone:</strong> 61 3 8383 9999 <br /><br/>
										</td>
									</tr>
									<tr>
										<td>
											New bookings or bookings cancellations and or amendments are valid only on receipt of our confirmation. <br /><br />
											Cancellations and or amendments for sedans need at least 2 hours notice, 4 hours in New Zealand and 24 hours in Darwin. <br /><br />
											Minibuses and Coaches 24 hours.  Major events 48 hours cancellation and or amendment notice. <br /><br />
											Please be aware that our office closes on public holidays. Except on Melbourne Cup Day. <br /><br />
											All new bookings or bookings cancellations and or amendments must be made through the office, during office hours.<br /><br />
											<br />Our car's service is 24 hours a day, 
											<br />Our office hours are
											<br />Monday to Friday  8am to 8pm
											<br />Saturday &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1pm to 5pm
											<br />Sunday &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1pm to 8pm
										</td>
									</tr>
								</table>";
				$mail->Body    .= self::EMAIL_HTML_FOOTER;
				$mail->Body    .= self::BODY_END;
				$mail->send();
			}
/*-----------------------------------------------------------------------------------------------------------*/				
		public function sendNoShowEmailForUnderEighteenStudents($job_id, $receiver_email, $receivers_name)
			{
				$mail 			= new PHPMailer;
				$job_reference 	= new JobReference();
				$job 			= new Job();
				
				$job_details 			= $job->getJobDetails($job_id);
				$job_reference_details 	= $job_reference->getJobReferenceDetails($job_details['job_reference_id']);
				
						
				$mail->From 	= 'alliedcars@alliedcars.com.au';
				$mail->FromName = 'Allied Cars';
				$mail->addAddress($receiver_email, ''.$receivers_name.'');  // Add a recipient
				$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
				$mail->WordWrap = 50;
				$mail->isHTML(true);
				
				$mail->Subject = "NO SHOW UNDER 18 for Booking for ".$job_reference_details['std_title']." ".$job_reference_details['std_fname']." ".$job_reference_details['std_lname']." ".$job_reference_details['std_id']." - ".formatDate($job_details['job_date'], '3')." at ".calculateTimeInAmPmFormatWithoutSeconds($job_details['job_time']).", Booking ID - ".$job_reference_details['j1_id']."";
				$mail->Body    .= self::DOCUMENT_HEADER;
				$mail->Body    .= self::BODY_START;
				$mail->Body    .= self::EMAIL_HTML_HEADER;
				$mail->Body    .= '<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0" style="border-collapse: collapse; border-color: #fff; background: #45cb37;">
										<tr>
											<td align="left" style="color: #ffffff; font-family: Arial, sans-serif; font-size: 200%">';
				$mail->Body    .= '<strong>For : '.$receivers_name.'</strong></td></tr></table><br/>';
				
				$mail->Body    .= '<p>The following job for an under 18 student was reported as NO SHOW by the driver</p>
									<table>
										<tr>
											<td>Job ID</td><td>'.$job_id.'</td>
										</tr>
										<tr>
											<td>Job Date/Time</td><td>'.formatDate($job_details['job_date'], '3').' at '.calculateTimeInAmPmFormatWithoutSeconds($job_details['job_time']).'</td>
										</tr>
										<tr>
											<td>Student Name</td><td>'.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].'</td>
										</tr>
										<tr>
											<td>Student ID</td><td>'.$job_reference_details['std_id'].'</td>
										</tr>
									</table>
									<p>This email is for your attention and information only.
									Thanks<br/>
									Allied Cars';
				$mail->Body    .= self::EMAIL_HTML_FOOTER;
				$mail->Body    .= self::BODY_END;
					
				$mail->send();
			}
		
/*-----------------------------------------------------------------------------------------------------------*/		
		public function sendNoShowEmailForStudents($job_id, $receiver_email, $receivers_name)
			{
				$mail 			= new PHPMailer;
				$job_reference 	= new JobReference();
				$job 			= new Job();
				
				$job_details 			= $job->getJobDetails($job_id);
				$job_reference_details 	= $job_reference->getJobReferenceDetails($job_details['job_reference_id']);
				
						
				$mail->From 	= 'alliedcars@alliedcars.com.au';
				$mail->FromName = 'Allied Cars';
				$mail->addAddress($receiver_email, ''.$receivers_name.'');  // Add a recipient
				$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
				$mail->WordWrap = 50;
				$mail->isHTML(true);
				
				$mail->Subject = "NO SHOW for Booking for ".$job_reference_details['std_title']." ".$job_reference_details['std_fname']." ".$job_reference_details['std_lname']." ".$job_reference_details['std_id']." - ".formatDate($job_details['job_date'], '3')." at ".calculateTimeInAmPmFormatWithoutSeconds($job_details['job_time']).", Booking ID - ".$job_reference_details['j1_id']."";
				$mail->Body    .= self::DOCUMENT_HEADER;
				$mail->Body    .= self::BODY_START;
				$mail->Body    .= self::EMAIL_HTML_HEADER;
				$mail->Body    .= '<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0" style="border-collapse: collapse; border-color: #fff; background: #45cb37;">
										<tr>
											<td align="left" style="color: #ffffff; font-family: Arial, sans-serif; font-size: 200%">';
				$mail->Body    .= '<strong>For : '.$receivers_name.'</strong></td></tr></table><br/>';
				
				$mail->Body    .= '<p>For the following Student, driver has reported a NO SHOW.</p>
									<table>
										<tr>
											<td>Job ID</td><td>'.$job_id.'</td>
										</tr>
										<tr>
											<td>Job Date/Time</td><td>'.formatDate($job_details['job_date'], '3').' at '.calculateTimeInAmPmFormatWithoutSeconds($job_details['job_time']).'</td>
										</tr>
										<tr>
											<td>Student Name</td><td>'.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].'</td>
										</tr>
										<tr>
											<td>Student ID</td><td>'.$job_reference_details['std_id'].'</td>
										</tr>
									</table>
									<p>This email is for your attention and information only.
									Thanks<br/>
									Allied Cars';
				$mail->Body    .= self::EMAIL_HTML_FOOTER;
				$mail->Body    .= self::BODY_END;
					
				$mail->send();
			}
/*-----------------------------------------------------------------------------------------------------------*/		
		public function sendHomeStayEmailToUniversity($job_id, $university_name, $email_to)
			{
				$mail 			= new PHPMailer;
				$job_reference 	= new JobReference();
				$job 			= new Job();
				
				$job_details 			= $job->getJobDetails($job_id);
				$job_reference_details 	= $job_reference->getJobReferenceDetails($job_details['job_reference_id']);
				$booking_html 			= $job->getHTMLofWholeBooking($job_reference_id);
				
						
				$mail->From 	= 'alliedcars@alliedcars.com.au';
				$mail->FromName = 'Allied Cars';
				$mail->addAddress($receiver_email, ''.$receivers_name.'');  // Add a recipient
				$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
				$mail->WordWrap = 50;
				$mail->isHTML(true);
				
				$mail->Subject = "Student Home Stay Notification -> ".$job_reference_details['std_title']." ".$job_reference_details['std_fname']." ".$job_reference_details['std_lname']." ".$job_reference_details['std_id']." - ".formatDate($job_details['job_date'], '3')." at ".calculateTimeInAmPmFormatWithoutSeconds($job_details['job_time']).", Booking ID - ".$job_reference_details['j1_id']."";
				$mail->Body    .= self::DOCUMENT_HEADER;
				$mail->Body    .= self::BODY_START;
				$mail->Body    .= self::EMAIL_HTML_HEADER;
				$mail->Body    .= '<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0" style="border-collapse: collapse; border-color: #fff; background: #45cb37;">
										<tr>
											<td align="left" style="color: #ffffff; font-family: Arial, sans-serif; font-size: 200%">';
				$mail->Body    .= '<strong>For : '.$receivers_name.'</strong></td></tr></table><br/>';
				
				if($_POST['monash_college_homestay'] == '1')
					{
						$mail->Body    .= 'Notification for Monash College Home Stay';
					}
				if($_POST['monash_connect_homestay'] == '1')
					{
						$mail->Body    .= 'Notification for Monash Connect Home Stay';
					}
				if($_POST['rmit_homestay'] == '1')
					{
						$mail->Body    .= 'Notification for RMIT Home Stay';
					}
				$mail->Body    .= 	'<p>Following job has been flagged for Home Stay. The job details are as follows.';
				$mail->Body    .= 	$booking_html;
				$mail->Body    .= 	'<p>Following job has been flagged for Home Stay. The job details are as follows.
									<p>This email is for your attention and information only.
									Thanks<br/>
									Allied Cars';
				$mail->Body    .= 	self::EMAIL_HTML_FOOTER;
				$mail->Body    .= 	self::BODY_END;
					
				$mail->send();
			}
/*-----------------------------------------------------------------------------------------------------------*/	
		
		public function sendUniversityMail($job_reference_id, $process_type, $edit_flag=NULL)
			{
				/*
				PARTIES
				1 	- 	STUDENT
				2	- 	ON BEHALF OF
				3 	- 	STUDENTS GUARDIAN
				4 	- 	STUDENTS AGENT
				5 	- 	UNIVERSITY UNDER 18 DEPARTMENT
				6 	- 	UNIVERSITY HOME STAY DEPARTMENT
				7 	- 	UNIVERSITY CONTACT
				8 	- 	ALLIED ADMIN
				
				//WHO CAN - STUDENT ONLY
					
					1 	ON ENTERING STUDENT BOOKING (WEBSITE)
							-	send notification to - 1, 2, 4, 7
				
				//WHO CAN - UNIVERSITY STAFF
					
					2 	ON ENTERING STUDENT BOOKING BY UNIVERSITY STAFF IN BMS
							-	send notification to - 8
	
				//WHO CAN - UNIVERSITY STAFF
				
					3 	ON EDITING STUDENT BOOKING (before confirmation)
							-	send notification to - NONE
								
					4 	ON DECLINING STUDENT BOOKING (before confirmation)
							-	send notification to - 1, 2, 3, 4
							
					5	ON APPROVING STUDENT BOOKING 
							-	send notification to - NONE
				
				//WHO CAN - ALLIED CAR ADMIN OR UNIVERSITY STAFF
				
					6	ON CONFIRMING STUDENT BOOKING
						ON ENTERING STUDENT BOOKING BY ALLIED CAR ADMIN IN BMS
							-	send notification to - 1, 2 , 4, 5, 6
							
				//WHO CAN - ALLIED CAR ADMIN, University Staff
							
					7 	ON EDITING STUDENT BOOKING - 
							DATE/TIME AND DROPOFF LOCATION ARE SAME
							-	send notification to - 1, 2, 4, 7, 8
							IF DATE/TIME AND DROPOFF LOCATION CHANGES
							-	send notification to - 1, 2, 3, 4, 5, 6, 7, 8
							
				//WHO CAN - ALLIED CAR ADMIN, University Staff
					8 	ON CANCELLING STUDENT BOOKING 
							(OVER 18)
							-	send notification to - 1, 2, 4, 6, 7, 8
							(UNDER 18)
							-	send notification to - 1, 2, 3, 4, 5, 6, 7, 8

				//WHO CAN - ALLIED CAR ADMIN OR DRIVER
				
					9 	ON REPORTING NOW SHOW 
							(OVER 18)
							-	send notification to - 1, 2, 4, 5, 7, 8
							(UNDER 18)
							-	send notification to - 1, 2, 3, 4, 5, 6, 7, 8
				*/
				
				
				$job_reference 	= new JobReference();
				$job 			= new Job();
				$chargeaccounts = new chargeAccount();
				
				$job_reference_details 		= 	$job_reference->getJobReferenceDetails($job_reference_id);
				$j1_details 				= 	$job->getJobDetails($job_reference_details['j1_id']);
				$chargeaccount_details 		= 	$chargeaccounts->getChargeAccountDetails($job_reference_details['charge_acc_id']);
				
				$booking_html = $job->getHTMLofWholeBooking($job_reference_id);
		
//	1 	ON ENTERING STUDENT BOOKING (WEBSITE)
				if($process_type == '1')
					{
						// 1 - TO STUDENT
						if($job_reference_details['std_email'] != '')
							{
								$mail 			= new PHPMailer;
								$mail->From 	= 'alliedcars@alliedcars.com.au';
								$mail->FromName = 'Allied Cars';
								$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
								$mail->WordWrap = 50;
								$mail->isHTML(true);
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
								$mail->addAddress($job_reference_details['std_email'],  ''.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].'');
								$mail->Body    	.=	self::MOBILE_HEADER;
								
								$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].'), AWAITING APPROVAL from '.$chargeaccount_details['account_name'].' ';
								$mail->Body    	.= 	'<h3>AWAITING APPROVAL</h3>';
								$mail->Body    	.= 	'<strong>Dear '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].',</strong><br/><br/>';
								$mail->Body  	.=	'Thank you for booking with us. Your booking is awaiting approval from '.$chargeaccount_details['account_name'].' and
													once approved, you will be notified by email.<br/><br/>
													Please allow three (3) working days to receive your booking confirmation. If you  did not receive it, please email us to request your booking confirmation.<br/><br/>
													Meanwhile please review the booking details and let us know immediately if they are not accurate.<br/><br/>';
								$mail->Body    	.= 	$booking_html;
								$mail->Body    	.= 	self::MOBILE_FOOTER;
								$mail->send();
							}
						// 2 - TO ON BEHALF OF
						if($job_reference_details['on_behalf'] == '1' && $job_reference_details['on_behalf_email'] != '')
							{
								$mail 			= new PHPMailer;
								$mail->From 	= 'alliedcars@alliedcars.com.au';
								$mail->FromName = 'Allied Cars';
								$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
								$mail->WordWrap = 50;
								$mail->isHTML(true);
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');								
								$mail->addAddress($job_reference_details['on_behalf_email'],  ''.$job_reference_details['on_behalf_title'].' '.$job_reference_details['on_behalf_fname'].' '.$job_reference_details['on_behalf_lname'].'');
								$mail->Body    	.=	self::MOBILE_HEADER;
								
								if($job_reference_details['under_18'] == '1')
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].') UNDER 18, AWAITING APPROVAL from '.$chargeaccount_details['account_name'].' ';
									}
								else
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].'), AWAITING APPROVAL from '.$chargeaccount_details['account_name'].' ';
									}
								
								$mail->Body    	.= 	'<h3>AWAITING APPROVAL</h3>';
								$mail->Body    	.= 	'<strong>Dear '.$job_reference_details['on_behalf_title'].' '.$job_reference_details['on_behalf_fname'].' '.$job_reference_details['on_behalf_lname'].',</strong><br/><br/>';
								$mail->Body  	.=	'Thank you for booking with us. Your booking is awaiting approval from '.$chargeaccount_details['account_name'].' and
													once approved, you will be notified by email.<br/><br/>
													Please allow three (3) working days to receive your booking confirmation. If you  did not receive it, please email us to request your booking confirmation.<br/><br/>
													Meanwhile please review the booking details and let us know immediately if they are not accurate.<br/><br/>';
								$mail->Body    	.= 	$booking_html;
								$mail->Body    	.= 	self::MOBILE_FOOTER;
								$mail->send();
							}
						
						// 3 - TO GUARDIAN
						if($job_reference_details['under_18'] == '1' && $job_reference_details['stu_guar_email'] != '')
							{
								// Do Nothing
							}
							
						// 4 - TO AGENT
						if($job_reference_details['has_agent'] == '1' && $job_reference_details['agents_email'] != '')
							{
								$mail 			= new PHPMailer;
								$mail->From 	= 'alliedcars@alliedcars.com.au';
								$mail->FromName = 'Allied Cars';
								$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
								$mail->WordWrap = 50;
								$mail->isHTML(true);
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');								
								$mail->addAddress($job_reference_details['agents_email'],  $job_reference_details['agents_name']);
								$mail->Body    	.=	self::MOBILE_HEADER;
								
								if($job_reference_details['under_18'] == '1')
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].') UNDER 18, AWAITING APPROVAL from '.$chargeaccount_details['account_name'].' ';
									}
								else
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].'), AWAITING APPROVAL from '.$chargeaccount_details['account_name'].' ';
									}
								
								$mail->Body    	.= 	'<h3>AWAITING APPROVAL</h3>';
								$mail->Body    	.= 	'<strong>Dear '.$job_reference_details['agents_name'].',</strong><br/><br/>';
								$mail->Body  	.=	'Thank you for booking with us. Your booking is awaiting approval from '.$chargeaccount_details['account_name'].' and
													once approved, you will be notified by email.<br/><br/>
													Please allow three (3) working days to receive your booking confirmation. If you  did not receive it, please email us to request your booking confirmation.<br/><br/>
													Meanwhile please review the booking details and let us know immediately if they are not accurate, by replying to this email.<br/><br/>';
								$mail->Body    	.= 	$booking_html;
								$mail->Body    	.= 	self::MOBILE_FOOTER;
								$mail->send();
							}
							
						// 5 - UNDER 18 Department
						if($job_reference_details['under_18'] == '1')
							{
								// Do Nothing
							}
						
						// 6 - HOMESTAY Department
						if($job_reference_details['is_homestay'] == '1')
							{
								// Do Nothing
							}
							
						// 7 - TO UNIVERSITY
						$admin_array = $chargeaccounts->getAllChargeAccountContacts($job_reference_details['charge_acc_id']);
						foreach($admin_array AS $admin)
							{
								if($admin['email'] != '')
									{
										$mail 			= new PHPMailer;
										$mail->From 	= 'alliedcars@alliedcars.com.au';
										$mail->FromName = 'Allied Cars';
										$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
										$mail->WordWrap = 50;
										$mail->isHTML(true);
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');								
										$mail->addAddress($admin['email'],  ''.$admin['title'].' '.$admin['fname'].' '.$admin['lname'].'');
										$mail->Body    	.=	self::MOBILE_HEADER;
										if($job_reference_details['under_18'] == '1')
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].', Pickup Date '.formatDate($j1_details['job_date'], 12).' APPROVAL REQUEST for UNDER 18 Booking ID - '.$job_reference_details['j1_id'].'';
											}
										if($job_reference_details['is_homestay'] == '1')
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].', Pickup Date '.formatDate($j1_details['job_date'], 12).' APPROVAL REQUEST for HOMESTAY Booking ID - '.$job_reference_details['j1_id'].'';
											}	
										else
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].', Pickup Date '.formatDate($j1_details['job_date'], 12).' APPROVAL REQUEST for Booking ID - '.$job_reference_details['j1_id'].'';
											}
										
										$mail->Body    	.= 	'<h3>AWAITING APPROVAL</h3>';
										$mail->Body  	.=	'A new booking request has been received from a student of '.$chargeaccount_details['account_name'].'. 
															Please review the booking to approve.<br/><br/>
															The following are the details of the booking.<br/><br/>';
										$mail->Body    	.= 	$booking_html;
										$mail->Body		.= '<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0"><tr><td>';
										$mail->Body		.= "<a href='".ROOT_ADDRESS."external_mail_script/university_booking_approval.php?job_reference_id=".$job_reference_id."&admin_id=".$admin_id."&confirm_status=1'>Approve this Booking</a ><br /><br />";
										$mail->Body		.= "<a href='".ROOT_ADDRESS."external_mail_script/university_booking_approval.php?job_reference_id=".$job_reference_id."&admin_id=".$admin_id."&confirm_status=2'>Decline this Booking</a ><br /><br />";
										$mail->Body		.= '</td><tr></table>';
										$mail->Body    	.= 	self::MOBILE_FOOTER;
										$mail->send();
									}
							}
						
					}
				
//	2 	ON ENTERING STUDENT BOOKING BY UNIVERSITY STAFF IN BMS

				if($process_type == '2')
					{
						// 1 - TO STUDENT
						if($job_reference_details['std_email'] != '')
							{
								// Do Nothing
							}
						// 2 - TO ON BEHALF OF
						if($job_reference_details['on_behalf'] == '1' && $job_reference_details['on_behalf_email'] != '')
							{
								// Do Nothing
							}
						// 3 - TO GUARDIAN
						if($job_reference_details['under_18'] == '1' && $job_reference_details['stu_guar_email'] != '')
							{
								// Do Nothing
							}
						// 4 - TO AGENT
						if($job_reference_details['has_agent'] == '1' && $job_reference_details['agents_email'] != '')
							{
								// Do Nothing
							}
						// 5 - UNDER 18 Department
						if($job_reference_details['under_18'] == '1')
							{
								// Do Nothing
							}
						// 6 - HOMESTAY Department
						if($job_reference_details['is_homestay'] == '1')
							{
								// Do Nothing
							}
						// 8 - TO ALLIED CAR ADMIN ONLY
						$admin_array = $job->getAdminDetails();
						foreach($admin_array AS $admin)
							{
								if($admin['email'] != '')
									{
										$mail 			= new PHPMailer;
										$mail->From 	= 'alliedcars@alliedcars.com.au';
										$mail->FromName = 'Allied Cars';
										$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
										$mail->WordWrap = 50;
										$mail->isHTML(true);
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
										$mail->addAddress($admin['email'],  ''.$admin['title'].' '.$admin['fname'].' '.$admin['lname'].'');
										$mail->Body    	.=	self::MOBILE_HEADER;
										if($job_reference_details['under_18'] == '1')
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' UNDER 18, Pickup Date '.formatDate($j1_details['job_date'], 12).' CONFIRMATION REQUEST for Booking ID - '.$job_reference_details['j1_id'].'';
											}
										else
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].', Pickup Date '.formatDate($j1_details['job_date'], 12).' CONFIRMATION REQUEST for Booking ID - '.$job_reference_details['j1_id'].'';
											}
										
										$mail->Body    	.= 	'<h3>AWAITING APPROVAL</h3>';
										$mail->Body  	.=	'A new booking has been approved by '.$chargeaccount_details['account_name'].' for a student. 
															Please review the booking and make necessary changes before confirming.<br/><br/>
															The following are the details of the booking.<br/><br/>';
										$mail->Body    	.= 	$booking_html;
										$mail->Body		.= '<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0"><tr><td>';
										$mail->Body		.= "<a href='".ROOT_ADDRESS."external_mail_script/booking_decision.php?job_reference_id=".$job_reference_id."&admin_id=".$admin_id."&confirm_status=1'>Confirm this Booking</a ><br /><br />";
										$mail->Body		.= "<a href='".ROOT_ADDRESS."external_mail_script/booking_decision.php?job_reference_id=".$job_reference_id."&admin_id=".$admin_id."&confirm_status=2'>Decline this booking</a ><br /><br />";
										$mail->Body		.= '</td><tr></table>';
										$mail->Body    	.= 	self::MOBILE_FOOTER;
										$mail->send();
									}
							}
					}
					
//	3 	ON EDITING STUDENT BOOKING (before confirmation)
				if($process_type == '3')
					{
						// 1 - TO STUDENT
						if($job_reference_details['std_email'] != '')
							{
								// Do Nothing
							}
						// 2 - TO ON BEHALF OF
						if($job_reference_details['on_behalf'] == '1' && $job_reference_details['on_behalf_email'] != '')
							{
								// Do Nothing
							}
						// 3 - TO GUARDIAN
						if($job_reference_details['under_18'] == '1' && $job_reference_details['stu_guar_email'] != '')
							{
								// Do Nothing
							}
						// 4 - TO AGENT
						if($job_reference_details['has_agent'] == '1' && $job_reference_details['agents_email'] != '')
							{
								// Do Nothing
							}
						// 5 - UNDER 18 Department
						if($job_reference_details['under_18'] == '1')
							{
								// Do Nothing
							}
						// 6 - HOMESTAY Department
						if($job_reference_details['is_homestay'] == '1')
							{
								// Do Nothing
							}
					}
				
//	4 - ON DECLINING STUDENT BOOKING (before confirmation)
				if($process_type == '4')
					{
						// 1 - TO STUDENT
						if($job_reference_details['std_email'] != '')
							{
								$mail 			= new PHPMailer;
								$mail->From 	= 'alliedcars@alliedcars.com.au';
								$mail->FromName = 'Allied Cars';
								$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
								$mail->WordWrap = 50;
								$mail->isHTML(true);
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');				
								$mail->addAddress($job_reference_details['std_email'],  ''.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].'');
								$mail->Body    	.=	self::MOBILE_HEADER;
								$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].'), BOOKING REQUEST DECLINED from '.$chargeaccount_details['account_name'].' ';
								$mail->Body    	.= 	'<h3>BOOKING REQUEST DECLINED</h3>';
								$mail->Body    	.= 	'<strong>Dear '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].',</strong><br/><br/>';
								$mail->Body  	.=	'Your arrival booking request has been declined by your institution.  Please email them to clarify   Pl.<br/><br/>
													You will have to make your own transport arrangement to your pre-arranged destination.<br/><br/>';
													if($job_reference_details['charge_acc_id'] == '15') //For RMIT
														{
															$mail->Body  	.=	'Please contact isarrival@rmit.edu.au should you require further assistance.<br/><br/>';
														}
								$mail->Body  	.=	'Copy of your declined booking request is as follows';
								$mail->Body    	.= 	$booking_html;
								$mail->Body    	.= 	self::MOBILE_FOOTER;
								$mail->send();
							}
						// 2 - TO ON BEHALF OF
						if($job_reference_details['on_behalf'] == '1' && $job_reference_details['on_behalf_email'] != '')
							{
								$mail 			= new PHPMailer;
								$mail->From 	= 'alliedcars@alliedcars.com.au';
								$mail->FromName = 'Allied Cars';
								$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
								$mail->WordWrap = 50;
								$mail->isHTML(true);
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
								$mail->addAddress($job_reference_details['on_behalf_email'],  ''.$job_reference_details['on_behalf_title'].' '.$job_reference_details['on_behalf_fname'].' '.$job_reference_details['on_behalf_lname'].'');
								$mail->Body    	.=	self::MOBILE_HEADER;
								if($job_reference_details['under_18'] == '1')
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].')  UNDER 18, BOOKING REQUEST DECLINED from '.$chargeaccount_details['account_name'].' ';
									}
								else
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].'), BOOKING REQUEST DECLINED from '.$chargeaccount_details['account_name'].' ';
									}
								
								$mail->Body    	.= 	'<h3>BOOKING REQUEST DECLINED</h3>';
								$mail->Body    	.= 	'<strong>Dear '.$job_reference_details['on_behalf_title'].' '.$job_reference_details['on_behalf_fname'].' '.$job_reference_details['on_behalf_lname'].',</strong><br/><br/>';
								$mail->Body  	.=	'Your arrival booking request has been declined by your institution.  Please email them to clarify.<br/><br/>
													You will have to make your own transport arrangement to your pre-arranged destination.<br/><br/>';
													if($job_reference_details['charge_acc_id'] == '15') //For RMIT
														{
															$mail->Body  	.=	'Please contact isarrival@rmit.edu.au should you require further assistance.<br/><br/>';
														}
								$mail->Body  	.=	'Copy of your declined booking request is as follows';
								$mail->Body    	.= 	$booking_html;
								$mail->Body    	.= 	self::MOBILE_FOOTER;
								$mail->send();
							}
						// 3 - TO GUARDIAN
						if($job_reference_details['std_guar_email'] != '')
							{
								$mail 			= new PHPMailer;
								$mail->From 	= 'alliedcars@alliedcars.com.au';
								$mail->FromName = 'Allied Cars';
								$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
								$mail->WordWrap = 50;
								$mail->isHTML(true);
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
								$mail->addAddress($job_reference_details['std_guar_email'],  ''.$job_reference_details['std_guar_title'].' '.$job_reference_details['std_guar_fname'].' '.$job_reference_details['std_guar_lname'].'');
								$mail->Body    	.=	self::MOBILE_HEADER;
								if($job_reference_details['under_18'] == '1')
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].') UNDER 18, BOOKING REQUEST DECLINED from '.$chargeaccount_details['account_name'].' ';
									}
								else
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].'), BOOKING REQUEST DECLINED from '.$chargeaccount_details['account_name'].' ';
									}
								$mail->Body    	.= 	'<h3>BOOKING REQUEST DECLINED</h3>';
								$mail->Body    	.= 	'<strong>Dear '.$job_reference_details['std_guar_title'].' '.$job_reference_details['std_guar_fname'].' '.$job_reference_details['std_guar_lname'].',</strong><br/><br/>';
								$mail->Body  	.=	'Your arrival booking request has been declined please contact your institution.<br/><br/>
													You will have to make your own transport arrangement to your pre-arranged destination.<br/><br/>';
													if($job_reference_details['charge_acc_id'] == '15') //For RMIT
														{
															$mail->Body  	.=	'Please contact isarrival@rmit.edu.au should you require further assistance.<br/><br/>';
														}
								$mail->Body  	.=	'Copy of your declined booking request is as follows';
								$mail->Body    	.= 	$booking_html;
								$mail->Body    	.= 	self::MOBILE_FOOTER;
								$mail->send();
							}
							
						// 4 - TO AGENT
						if($job_reference_details['has_agent'] == '1' && $job_reference_details['agents_email'] != '')
							{
								$mail 			= new PHPMailer;
								$mail->From 	= 'alliedcars@alliedcars.com.au';
								$mail->FromName = 'Allied Cars';
								$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
								$mail->WordWrap = 50;
								$mail->isHTML(true);
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
								$mail->addAddress($job_reference_details['agents_email'],  $job_reference_details['agents_name']);
								$mail->Body    	.=	self::MOBILE_HEADER;
								if($job_reference_details['under_18'] == '1')
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_guar_title'].' '.$job_reference_details['std_guar_fname'].' '.$job_reference_details['std_guar_lname'].' ('.$job_reference_details['std_id'].') UNDER 18, BOOKING REQUEST DECLINED from '.$chargeaccount_details['account_name'].' ';
									}
								else
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_guar_title'].' '.$job_reference_details['std_guar_fname'].' '.$job_reference_details['std_guar_lname'].' ('.$job_reference_details['std_id'].'), BOOKING REQUEST DECLINED from '.$chargeaccount_details['account_name'].' ';
									}
								
								$mail->Body    	.= 	'<h3>BOOKING REQUEST DECLINED</h3>';
								$mail->Body    	.= 	'<strong>'.$job_reference_details['agents_name'].',</strong><br/><br/>';
								$mail->Body  	.=	'Your arrival booking request has been declined as you had previously used the free service.<br/><br/>
													You will have to make your own transport arrangement to your pre-arranged destination.<br/><br/>';
													if($job_reference_details['charge_acc_id'] == '15') //For RMIT
														{
															$mail->Body  	.=	'Please contact isarrival@rmit.edu.au should you require further assistance.<br/><br/>';
														}
								$mail->Body  	.=	'Copy of your declined booking request is as follows';
								$mail->Body    	.= 	$booking_html;
								$mail->Body    	.= 	self::MOBILE_FOOTER;
								$mail->send();
							}
							
						// 5 - UNDER 18 Department
						if($job_reference_details['under_18'] == '1')
							{
								// Do Nothing
							}
						// 6 - HOMESTAY Department
						if($job_reference_details['is_homestay'] == '1')
							{
								// Do Nothing
							}
					}
					
//	5	ON APPROVING STUDENT BOOKING (By University Admin)
				if($process_type == '5')
					{
						// 1 - TO STUDENT
						if($job_reference_details['std_email'] != '')
							{
								// Do Nothing
							}
						// 2 - TO ON BEHALF OF
						if($job_reference_details['on_behalf'] == '1' && $job_reference_details['on_behalf_email'] != '')
							{
								// Do Nothing
							}
						// 3 - TO GUARDIAN
						if($job_reference_details['under_18'] == '1' && $job_reference_details['stu_guar_email'] != '')
							{
								// Do Nothing
							}
						// 4 - TO AGENT
						if($job_reference_details['has_agent'] == '1' && $job_reference_details['agents_email'] != '')
							{
								// Do Nothing
							}
						// 5 - UNDER 18 Department
						if($job_reference_details['under_18'] == '1')
							{
								// Do Nothing
							}
						// 6 - HOMESTAY Department
						if($job_reference_details['is_homestay'] == '1')
							{
								// Do Nothing
							}

						// 8 - TO ALLIED CAR ADMIN ONLY
						if($job_reference_details['has_agent'] == '1' && $job_reference_details['agents_email'] != '')
							{
								$admin_array = $job->getAdminDetails();
								foreach($admin_array AS $admin)
									{
										if($admin['email'] != '')
											{
												$mail 			= new PHPMailer;
												$mail->From 	= 'alliedcars@alliedcars.com.au';
												$mail->FromName = 'Allied Cars';
												$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
												$mail->WordWrap = 50;
												$mail->isHTML(true);
												//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
												//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
												$mail->addAddress($admin['email'],  ''.$admin['title'].' '.$admin['fname'].' '.$admin['lname'].'');
												$mail->Body    	.=	self::MOBILE_HEADER;
												if($job_reference_details['under_18'] == '1')
													{
														$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' UNDER 18, Pickup Date '.formatDate($j1_details['job_date'], 12).' CONFIRMATION REQUEST for Booking ID - '.$job_reference_details['j1_id'].'';
													}
												else
													{
														$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].', Pickup Date '.formatDate($j1_details['job_date'], 12).' CONFIRMATION REQUEST for Booking ID - '.$job_reference_details['j1_id'].'';
													}
												
												$mail->Body    	.= 	'<h3>AWAITING APPROVAL</h3>';
												$mail->Body  	.=	'A new booking approved by '.$chargeaccount_details['account_name'].' for a student. 
																	Please review the booking to confirm.<br/><br/>
																	The following are the details of the booking.<br/><br/>';
												$mail->Body    	.= 	$booking_html;
												$mail->Body		.= '<table align="center" width="100%"  border="0"  cellpadding="5" cellspacing="0"><tr><td>';
												$mail->Body		.= "<a href='".ROOT_ADDRESS."external_mail_script/booking_decision.php?job_reference_id=".$job_reference_id."&admin_id=".$admin_id."&confirm_status=1'>Confirm this Booking</a ><br /><br />";
												$mail->Body		.= "<a href='".ROOT_ADDRESS."external_mail_script/booking_decision.php?job_reference_id=".$job_reference_id."&admin_id=".$admin_id."&confirm_status=2'>Decline this booking</a ><br /><br />";
												$mail->Body		.= '</td><tr></table>';
												$mail->Body    	.= 	self::MOBILE_FOOTER;
												$mail->send();
											}
									}
							}
					}
				
//	6	ON CONFIRMING STUDENT BOOKING - By Allied Car
				//	send notification to - 1, 2 , 4, 5, 6
				if($process_type == '6')
					{
						// 1 - TO STUDENT
						if($job_reference_details['std_email'] != '')
							{
								$mail 			= new PHPMailer;
								$mail->From 	= 'alliedcars@alliedcars.com.au';
								$mail->FromName = 'Allied Cars';
								$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
								$mail->WordWrap = 50;
								$mail->isHTML(true);
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
								if($job_reference_details['under_18'] == '1')
									{
										$data = ''.$chargeaccount_details['account_name'].' UNDER 18 Student - '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].') : Job ID - '.$job_reference_details['j1_id'].', Date -  '.formatDate($j1_details['job_date'], 11).', Time - '.calculateTimeInAmPmFormatWithoutSeconds($j1_details['job_time']).', Destination - '.$j1_details['to_line1'].', '.$j1_details['to_line2'].', '.$j1_details['to_sub'].', '.$j1_details['to_pc'].', '.$j1_details['to_state'].'';
									}
								else
									{
										$data = ''.$chargeaccount_details['account_name'].' Student - '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].') : Job ID - '.$job_reference_details['j1_id'].', Date -  '.formatDate($j1_details['job_date'], 11).', Time - '.calculateTimeInAmPmFormatWithoutSeconds($j1_details['job_time']).', Destination - '.$j1_details['to_line1'].', '.$j1_details['to_line2'].', '.$j1_details['to_sub'].', '.$j1_details['to_pc'].', '.$j1_details['to_state'].'';
									}
								$qr = file_get_contents("https://api.qrserver.com/v1/create-qr-code/?size=200x200&data=".urlencode($data)."");
								
								$mail->addStringAttachment($qr, "qr_code.png");
								
								$mail->addAddress($job_reference_details['std_email'],  ''.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].'');
								
								if($job_reference_details['charge_acc_id'] == '15') // RMIT Student
									{
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Meeting_Point.png",'meeting_point.png','base64','application/png');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/melbourne_airport_RMIT.pdf",'Airport_Map.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/RMIT.pdf",'RMIT.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/WIS_SIGN.pdf",'WIS.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Zhongwen_Airport_Pickup_RMIT.pdf",'Zhongwen.pdf','base64','application/png');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Vietnamise_rmit.pdf",'Dich_vu_dón_tai_sân_bay.pdf','base64','application/png');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Feedback.pdf",'Feedback.pdf','base64','application/pdf');
									}
								if($job_reference_details['charge_acc_id'] == '9') // Connect Student
									{
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/melbourne_airport_RMIT.pdf",'Airport_Map.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Meeting_Point.png",'meeting_point.png','base64','application/png');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/WIS_SIGN.pdf",'WIS.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Airport_Map_Monash_A.pdf",'Airport_Map_a.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Feedback.pdf",'Feedback.pdf','base64','application/pdf');
									}	
								if($job_reference_details['charge_acc_id'] == '55') // College Student
									{
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/melbourne_airport_RMIT.pdf",'Airport_Map.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Meeting_Point.png",'meeting_point.png','base64','application/png');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Monash_College_Sign.pdf",'Monash.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/WIS_SIGN.pdf",'WIS.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Airport_Map_Monash_A.pdf",'Airport_Map_a.pdf','base64','application/pdf');
									}		
								if($job_reference_details['charge_acc_id'] == '19') // Abroad Student
									{
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Mel_Air_Abroad.pdf",'Airport_Map.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Meeting_Point.png",'meeting_point.png','base64','application/png');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/WIS_SIGN.pdf",'WIS.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Airport_Map_Monash_A.pdf",'Airport_Map_a.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Feedback.pdf",'Feedback.pdf','base64','application/pdf');
									}
								else // common attachments Student
									{
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/melbourne_airport_RMIT.pdf",'Airport_Map.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Meeting_Point.png",'meeting_point.png','base64','application/png');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/WIS_SIGN.pdf",'WIS.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Feedback.pdf",'Feedback.pdf','base64','application/pdf');
									}
									
								$mail->Body    	.=	self::MOBILE_HEADER;
								$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].'), BOOKING CONFIRMED by '.$chargeaccount_details['account_name'].' ';
								$mail->Body    	.= 	'<h3>BOOKING CONFIRMED</h3>';
								$mail->Body    	.= 	'<strong>Dear '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].',</strong><br/><br/>';
								
								if($job_reference_details['charge_acc_id'] == '55') // MONASH COLLEGE
									{
										$mail->Body  	.=	'Thank you for booking with us and we are now pleased to confirm your arrival details. <br /><br />
															Please review the booking details and let us know immediately if they are not accurate. <br /><br />
															
															<strong>IMPORTANT INFORMATION</strong><br/><br/>
															
															<strong>IF YOU CAN&#39;T FIND YOUR DRIVER, FIND A PUBLIC PHONE AND RING ALLIED ON 1800 350 850</STRONG> (this is a <strong>free</strong>
															call, <strong>NO</strong> coins needed)<br /><br />
															
															<strong>YOU NEED TO PRINT THIS CONFIRMATION EMAIL AND BRING IT WITH YOU TO SHOW THE ALLIED REPRESENTATIVE.</strong><br /><br />

															A driver from Allied Chauffeured Cars (Allied) will meet you in Melbourne Airport&#39;s International Arrival Hall T2.
															If you are arriving on an International Flight, turn right when you leave the customs area and go towards &#39;The Meeting Point&#39;
															(near Yes Optus Mobile Shop). Your driver will hold an &#39;ALLIED&#39; sign for easy identification and will take you to your
															destination as per your instructions below.<br /><br />
															
															If you are arriving on a Domestic Flight, make your way to the arrivals area in the International Terminal T2. <br /><br />
															
															If you arrive at:<br /><br />
															<img src="http://dev.bookings-global.com.au/Images/bullet_icon.png" width="10px" height="9px" />&nbsp; <strong>Qantas Domestic Terminal</strong>, please go to the "Meeting Point" located opposite Baggage Carousel 3.  If you can&#39;t find your driver at the Domestic terminal, please go to <strong>T2</strong> (<strong>International</strong>, next door to your right).<br /><br />
															<img src="http://dev.bookings-global.com.au/Images/bullet_icon.png" width="10px" height="9px" />&nbsp; <strong>Virgin Australia Terminal</strong>, please go to the "Meeting Point" below the escalators. If you can&#39;t find your driver at the Domestic terminal, please go to <strong>T2</strong> (<strong>International</strong>, next door to your left). <br /><br />
															<img src="http://dev.bookings-global.com.au/Images/bullet_icon.png" width="10px" height="9px" />&nbsp; <strong>Terminal 4 (Tiger Airways)</strong>, you have to ring Allied Chauffeured Cars on <strong>1800 350 850 </strong> from <strong>any</strong> public phone (Free Call), the staff will direct you to the meeting point. <br /><br /><br />

															
															<strong>No driver waiting at Melbourne Airport</strong><br/><br/>
															
															Go to &#39;The Meeting Point&#39;.  If your driver is not there within 15 minutes find the nearest public telephone and call Allied
															on 1800 350 850 (this is a <strong>free</strong> call, <strong>NO</strong> coins needed) so they can locate your driver.
															When calling, give Allied your Student ID number (this is in your offer letter), family name and given name.<br/><br/>
															
															<STRONG>Please note,</STRONG> if you do not arrive at your booked time or do not arrive at all without informing us,
															you will have to pay a <strong>cancellation fee </strong> to '.$chargeaccount_details['account_name'].' . <br /><br />
															
															You can contact us by email alliedcars@alliedcars.com.au or by phone <strong>(+61 3) 8383 9999.</strong> <br /><br />
															
															<strong>City of Melbourne - International Student Welcome Booth</strong><br/><br/>
															
															The City of Melbourne operates the &#39;Student Welcome Desk&#39; at Melbourne Airport where volunteers greet you and provide information
															about welfare services, public transport, housing, employment options and social integration to help you with your stay in Melbourne.<br/><br/> 
															
															<strong>Changes to or cancellation of your flight</strong><br/><br/>
															
															To change or cancel your airport pickup booking, contact us at least one full working day before you arrive to avoid additional costs.
															Our office hours are 9am to 5pm Monday to Friday, Australian Eastern Standard Time.<br/><br/> 
															
															To change your arrival details on Saturday or Sunday, telephone +61 3 8383 9999.<br/><br/> 
															
															If you are overseas and miss your flight or if your flight is delayed, please telephone +61 3 8383 9999 immediately to confirm
															your new arrival details.  This will ensure your driver is at Melbourne Airport to meet you.<br/><br/>
															<br/><br/>
															
															MONASH COLLEGE
															<strong>Enrolment</strong><br/><br/>
															Each course has its own enrolment date, time and location.<br/><br/> 
															Monash College students:  https://www.monashcollege.edu.au/live-and-study-in-australia/prepare-to-arrive/enrolment-and-orientation <br/><br/>
															<strong>Orientation program</strong><br/><br/>
															If you are studying the English Language, Foundation Year or Monash College Diploma program, your enrolment and orientation information can be found at:<br/>
															https://www.monashcollege.edu.au/live-and-study-in-australia/prepare-to-arrive/enrolment-and-orientation <br/><br/>
															You must attend Orientation  You will get a lot of valuable information about studying at Monash including your enrolment, study timetable and learning how to access Monash University&#39;s computer resources.<br/><br/>  
															Orientation includes sessions on academic and study skills, Australian culture, campus tours and social opportunities to meet other students.<br/><br/>
															We wish you a pleasant journey and look forward to welcoming you to Monash.<br/><br/>
															Kind regards<br/>
															Monash College<br/><br/>';
									}
								else if($job_reference_details['charge_acc_id'] == '9') // MONASH CONNECT
									{
										$mail->Body  	.=	'Thank you for your airport pickup request for the '.formatDate($j1_details ['job_date'],12).' on '.$j1_details ['frm_flight_no'].' arriving at '.calculateTimeInAmPmFormatWithoutSeconds($j1_details ['job_time']).'.<br/><br/>
															A driver from Allied Chauffeured Cars (Allied) will meet you in Melbourne Airport&#39;s International Arrival Hall. Turn right when you leave the customs area and head toward &#34;The Meeting Point&#34;.<br/>
															The driver will hold an &#34;ALLIED&#34; sign for easy identification and will take you to the following address:<br/>
															'.$j1_details ['to_line1'].'<br/>
															'.$j1_details ['to_line2'].'<br/>
															'.$j1_details ['to_sub'].' '.$j1_details ['to_pc'].'<br/>
															'.$j1_details ['to_state'].'<br/>
															<b>City of Melbourne - International Student Welcome Booth</b><br/>
															At key times of the year, the City of Melbourne operates the "Student Welcome Desk " at Melbourne Airport. At the welcome desk, volunteers greet you
															and provide information about welfare services, public transport, housing, employment options and social integration to help you with your stay in
															Melbourne.<br/><br/>
															
															<b>No driver waiting at Melbourne Airport</b><br/>
															Go to &#34;The Meeting Point&#34;.  If your driver is not there within 15 minutes find the nearest public telephone and call Allied on 1800 350 850
															(this is a free call).  They will locate your driver. When calling, give Allied your Monash Student ID number (this is in your Monash offer letter),
															family name and given names.<br/><br/>
															
															<b>Changes to or cancellation of your flight</b><br/>
															To change or cancel your airport pickup booking, contact us at least one full working day before you arrive to avoid additional costs.  Our office
															hours are 9am to 5pm Monday to Friday, Australian Eastern Standard Time.<br/>
															
															To change your arrival details on Saturday or Sunday, telephone +61 3 8383 9999.<br/><br/>
															
															If you are overseas and miss your flight or if your flight is delayed, please telephone +61 3 8383 9999 immediately to confirm your new arrival details.
															This will ensure your driver is at Melbourne Airport to meet you.<br/><br/>
															
															<b>Enrolment</b><br/>
															Each course has its own enrolment date, time and location. Refer to the following website for details:  http://monash.edu/study/international/enrolment/<br/><br/>

															<b>Orientation program<b><br/>
															Details of the Orientation program will be available on the following website: http://monash.edu/orientation/<br/><br/>

															In preparation for your arrival on campus, create your personalised Orientation schedule using the eOrientation planner. <br/><br/>
															 
															You must attend Orientation  You will get a lot of valuable information about studying at Monash University including; your enrolment, study timetable and learning how to access the University&#39;s computer resources. <br/> <br/>

															Orientation includes sessions on academic and study skills, Australian culture and working in Australia, campus tours and social opportunities to meet other students.<br/><br/>

															We wish you a pleasant journey and look forward to welcoming you to Monash University.<br/><br/>
															Thank you.<br/>
															Kind Regards<br/>
															<b>'.$chargeaccount_details['account_name'].'</b><br/><br/>
															';
									}
								else if($job_reference_details['charge_acc_id'] == '15') // RMIT
									{
										$mail->Body  	.=	'Thank you for booking with us and we are now pleased to confirm your arrival pick-up. Please show this email confirmation to Allied&#39;s staff.<br/><br/>
															Our driver will meet you at the airport and will be holding a placard <b>"RMIT UNIVERSITY"</b>.<br/><br/>
															
															You have to let us know the number of luggage you have.  Please note if your excess luggage do not fit in a standard sedan&#39;s boot, you will
															have to arrange a taxi to transport any excess luggage. <br/><br/>
															
															Please refrain from bringing food into Australia. As this <b>will</b> delay your immigration and customs clearance time and you <b>may incur extra charges</b>.<br/><br/>
															
															You must look for Allied&#39;s driver <b>immediately</b> at the Meeting Point of your arriving terminal. <br/><br/>
															
															If you experience a delay or cannot find Allied&#39;s driver, you must ring Allied on <b>1800 350 850</b> (FREE CALL) from any public phone. <br/><br/>
															
															Please note a no show fee of <b>AU$105</b> applies if you do not inform Allied of any changes to your arrival itinerary or do not use the service. <br/><br/>
															If you arrive at:
															<ul>
																<li><b>Melbourne Airport-T2 (International) </b>, Please go to the Meeting Point near OPTUS store/ near the TRAVELEX Foreign Exchange counter.  Refer to the "Key 8 & 10" of the attached Melbourne Airport Map.</li>
																<li><b>Qantas Domestic Terminal</b>, please go to the &#34;Meeting Point&#34; located opposite Baggage Carousel 3.  If you can&#39;t find your driver at the Domestic terminal, please go to <b>T2 International</b>, next door to your right). </li>
																<li><b>Virgin Australia Terminal</b>, please go to the &#34;Meeting Point&#34; below the escalators. If you can&#39;t find your driver at the Domestic terminal, please go to<b> T2 (International) </b>, next door to your left). </li>
																<li><b>Terminal 4 (Tiger Airways) </b>, you have to ring Allied Chauffeured Cars on <b>1800 350 850</b> from <b>any</b> public phone (Free Call), the staff will direct you to the meeting point. </li>
															</ul>
															If you are unable to find our staff or have made changes to your arrival itinerary, you must call us. Otherwise we cannot guarantee airport pick-up and you will be charged for the service. <br/>
															Thank you.
															Kind Regards<br/>
															Admin<br/>
															<b>'.$chargeaccount_details['account_name'].'</b><br/><br/>
															';
									}
									else if($job_reference_details['charge_acc_id'] == '871') // AHN STUDENT
									{
										$mail->Body  	.=	'Thank you for your airport pickup request for the '.formatDate($j1_details ['job_date'],12).' on '.$j1_details ['frm_flight_no'].' arriving at '.calculateTimeInAmPmFormatWithoutSeconds($j1_details ['job_time']).'.<br/><br/>
															A driver from Allied Chauffeured Cars (Allied) will meet you in Melbourne Airport&#39;s International Arrival Hall. Turn right when you leave the customs area and head toward &#34;The Meeting Point&#34;.<br/>
															The driver will hold an &#34;ALLIED&#34; sign for easy identification and will take you to the following address:<br/>
															'.$j1_details ['to_line1'].'<br/>
															'.$j1_details ['to_line2'].'<br/>
															'.$j1_details ['to_sub'].' '.$j1_details ['to_pc'].'<br/>
															'.$j1_details ['to_state'].'<br/><br/>
															
															<b>INTRUCTIONS</b><br/>
															
															<b>International Arrival</b>
															Please print this notice before arriving in Melbourne and have it ready to show your driver.<br/>
															Exit customs at door on right hand side.<br/>
															AFTER LEAVING THE PLANE GO DIRECTLY to meet your driver near the meeting point next to the Yes Optus shop (please look at attached picture and maps)
															
															<b>No driver waiting at Melbourne Airport</b><br/>
															Go to &#34;The Meeting Point&#34;.  If your driver is not there within 15 minutes find the nearest public telephone and call Allied on 1800 350 850
															(this is a free call).  They will locate your driver. When calling, give Allied your Monash Student ID number (this is in your Monash offer letter),
															family name and given names.<br/><br/>
															
															<b>Changes to or cancellation of your flight</b><br/>
															To change or cancel your airport pickup booking, contact us at least one full working day before you arrive to avoid additional costs.  Our office
															hours are 9am to 5pm Monday to Friday, Australian Eastern Standard Time.<br/>
															
															To change your arrival details on Saturday or Sunday, telephone +61 3 8383 9999.<br/><br/>
															
															If you are overseas and miss your flight or if your flight is delayed, please telephone +61 3 8383 9999 immediately to confirm your new arrival details.
															This will ensure your driver is at Melbourne Airport to meet you.<br/><br/>
															
															<b>City of Melbourne - International Student Welcome Booth</b><br/>
															At key times of the year, the City of Melbourne operates the "Student Welcome Desk " at Melbourne Airport. At the welcome desk, volunteers greet you
															and provide information about welfare services, public transport, housing, employment options and social integration to help you with your stay in
															Melbourne.<br/><br/>

															We wish you a pleasant journey and look forward to welcoming you to Australia.<br/><br/>
															Thank you.<br/>
															Kind Regards<br/>
															<b>'.$chargeaccount_details['account_name'].'</b><br/><br/>
															';
									}
								else //common body
									{
										$mail->Body  	.=	'Thank you for booking with us and we are now pleased to confirm your arrival details. <br /><br />
															Please review the booking details and let us know immediately if they are not accurate. <br /><br />
															
															<strong>IMPORTANT INFORMATION</strong><br/><br/>
															
															<strong>IF YOU CAN\'T FIND YOUR DRIVER, FIND A PUBLIC PHONE AND RING ALLIED ON 1800 350 850</STRONG> (this is a <strong>free</strong>
															call, <strong>NO</strong> coins needed)<br /><br />
															
															<strong>YOU NEED TO PRINT THIS CONFIRMATION EMAIL AND BRING IT WITH YOU TO SHOW THE ALLIED REPRESENTATIVE.</strong><br /><br />

															A driver from Allied Chauffeured Cars (Allied) will meet you in Melbourne Airport&#39;s International Arrival Hall T2.
															If you are arriving on an International Flight, turn right when you leave the customs area and go towards &#39;The Meeting Point&#39;
															(near Yes Optus Mobile Shop). Your driver will hold an &#39;ALLIED&#39; sign for easy identification and will take you to your
															destination as per your instructions below.<br /><br />
															
															If you are arriving on a Domestic Flight, make your way to the arrivals area in the International Terminal T2. <br /><br />
															
															If you arrive at:<br /><br />
															<img src="http://dev.bookings-global.com.au/Images/bullet_icon.png" width="10px" height="9px" />&nbsp; <strong>Qantas Domestic Terminal</strong>, please go to the "Meeting Point" located opposite Baggage Carousel 3.  If you can&#39;t find your driver at the Domestic terminal, please go to <strong>T2</strong> (<strong>International</strong>, next door to your right).<br /><br />
															<img src="http://dev.bookings-global.com.au/Images/bullet_icon.png" width="10px" height="9px" />&nbsp; <strong>Virgin Australia Terminal</strong>, please go to the "Meeting Point" below the escalators. If you can&#39;t find your driver at the Domestic terminal, please go to <strong>T2</strong> (<strong>International</strong>, next door to your left). <br /><br />
															<img src="http://dev.bookings-global.com.au/Images/bullet_icon.png" width="10px" height="9px" />&nbsp; <strong>Terminal 4 (Tiger Airways)</strong>, you have to ring Allied Chauffeured Cars on <strong>1800 350 850 </strong> from <strong>any</strong> public phone (Free Call), the staff will direct you to the meeting point. <br /><br /><br />

															
															<strong>No driver waiting at Melbourne Airport</strong><br/><br/>
															
															Go to &#39;The Meeting Point&#39;.  If your driver is not there within 15 minutes find the nearest public telephone and call Allied
															on 1800 350 850 (this is a <strong>free</strong> call, <strong>NO</strong> coins needed) so they can locate your driver.
															When calling, give Allied your Student ID number (this is in your offer letter), family name and given name.<br/><br/>
															
															<STRONG>Please note,</STRONG> if you do not arrive at your booked time or do not arrive at all without informing us,
															you will have to pay a <strong>cancellation fee </strong> to '.$chargeaccount_details['account_name'].' . <br /><br />
															
															You can contact us by email alliedcars@alliedcars.com.au or by phone <strong>(+61 3) 8383 9999.</strong> <br /><br />
															
															<strong>City of Melbourne - International Student Welcome Booth</strong><br/><br/>
															
															The City of Melbourne operates the &#39;Student Welcome Desk&#39; at Melbourne Airport where volunteers greet you and provide information
															about welfare services, public transport, housing, employment options and social integration to help you with your stay in Melbourne.<br/><br/> 
															
															<strong>Changes to or cancellation of your flight</strong><br/><br/>
															
															To change or cancel your airport pickup booking, contact us at least one full working day before you arrive to avoid additional costs.
															Our office hours are 9am to 5pm Monday to Friday, Australian Eastern Standard Time.<br/><br/> 
															
															To change your arrival details on Saturday or Sunday, telephone +61 3 8383 9999.<br/><br/> 
															
															If you are overseas and miss your flight or if your flight is delayed, please telephone +61 3 8383 9999 immediately to confirm
															your new arrival details.  This will ensure your driver is at Melbourne Airport to meet you.<br/><br/>
															<br/><br/>
															
															Thank you for booking with us.<br/>
															Kind Regards, <br /><br/>
															<strong>Allied Chauffeured Cars Australia</strong><br/>ABN: 38 076 977 136<br /><br/>
															<strong>Email:</strong> alliedcars@alliedcars.com.au <br />
															<strong>Telephone:</strong> 61 3 8383 9999 <br /><br/>';
									}	
								$mail->Body    	.= 	$booking_html;
								$mail->Body    	.= 	self::MOBILE_FOOTER;
								$mail->send();
							}
						
						// 2 - TO ON BEHALF OF
						if($job_reference_details['on_behalf'] == '1' && $job_reference_details['on_behalf_email'] != '')
							{
								$mail 			= new PHPMailer;
								$mail->From 	= 'alliedcars@alliedcars.com.au';
								$mail->FromName = 'Allied Cars';
								$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
								$mail->WordWrap = 50;
								$mail->isHTML(true);
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
								$mail->addAddress($job_reference_details['on_behalf_email'],  ''.$job_reference_details['on_behalf_title'].' '.$job_reference_details['on_behalf_fname'].' '.$job_reference_details['on_behalf_lname'].'');
								
								$mail->addStringAttachment($qr, "qr_code.png");
								
								$mail->addAddress($job_reference_details['std_email'],  ''.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].'');
								
								if($job_reference_details['charge_acc_id'] == '15') // RMIT behalf
									{
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Meeting_Point.png",'meeting_point.png','base64','application/png');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/melbourne_airport_RMIT.pdf",'Airport_Map.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/RMIT.pdf",'RMIT.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/WIS_SIGN.pdf",'WIS.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Zhongwen_Airport_Pickup_RMIT.pdf",'Zhongwen.pdf','base64','application/png');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Vietnamise_rmit.pdf",'Dich_vu_dón_tai_sân_bay.pdf','base64','application/png');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Feedback.pdf",'Feedback.pdf','base64','application/pdf');
									}
								if($job_reference_details['charge_acc_id'] == '55') // Monash College Behalf
									{
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/melbourne_airport_RMIT.pdf",'Airport_Map.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Meeting_Point.png",'meeting_point.png','base64','application/png');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Monash_College_Sign.pdf",'Monash.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/WIS_SIGN.pdf",'WIS.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Airport_Map_Monash_A.pdf",'Airport_Map_a.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Feedback.pdf",'Feedback.pdf','base64','application/pdf');
									}
								if($job_reference_details['charge_acc_id'] == '9') // Monash Connect Behalf
									{
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/melbourne_airport_RMIT.pdf",'Airport_Map.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Meeting_Point.png",'meeting_point.png','base64','application/png');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/WIS_SIGN.pdf",'WIS.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Airport_Map_Monash_A.pdf",'Airport_Map_a.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Feedback.pdf",'Feedback.pdf','base64','application/pdf');
									}
								if($job_reference_details['charge_acc_id'] == '19') // Monash Abroad Behalf
									{
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/melbourne_airport_RMIT.pdf",'Airport_Map.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Meeting_Point.png",'meeting_point.png','base64','application/png');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/WIS_SIGN.pdf",'WIS.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Airport_Map_Monash_A.pdf",'Airport_Map_a.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Feedback.pdf",'Feedback.pdf','base64','application/pdf');
									}	
								else // common attachments
									{
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Meeting_Point.png",'meeting_point.png','base64','application/png');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/WIS_SIGN.pdf",'WIS.pdf','base64','application/pdf');
									}
								$mail->Body    	.=	self::MOBILE_HEADER;
								if($job_reference_details['under_18'] == '1')
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].') UNDER 18, BOOKING CONFIRMED by '.$chargeaccount_details['account_name'].' ';
									}
								else
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].'), BOOKING CONFIRMED by '.$chargeaccount_details['account_name'].' ';
									}
								
								$mail->Body    	.= 	'<h3>BOOKING CONFIRMED</h3>';
								$mail->Body    	.= 	'<strong>Dear '.$job_reference_details['on_behalf_title'].' '.$job_reference_details['on_behalf_fname'].' '.$job_reference_details['on_behalf_lname'].',</strong><br/><br/>';
								$mail->Body  	.=	'Your booking has been approved by '.$chargeaccount_details['account_name'].'.<br/><br/><br/>';
								if($job_reference_details['charge_acc_id'] == '55') // MONASH COLLEGE
									{
										$mail->Body  	.=	'We are now pleased to confirm your students arrival details. <br /><br />
															Please review the booking details and let us know immediately if they are not accurate. <br /><br />
															
															<strong>IMPORTANT INFORMATION</strong><br/><br/>
															
															<strong>IF YOU CAN&#39;T FIND YOUR DRIVER, FIND A PUBLIC PHONE AND RING ALLIED ON 1800 350 850</STRONG> (this is a <strong>free</strong>
															call, <strong>NO</strong> coins needed)<br /><br />
															
															<strong>YOU NEED TO PRINT THIS CONFIRMATION EMAIL AND BRING IT WITH YOU TO SHOW THE ALLIED REPRESENTATIVE.</strong><br /><br />

															A driver from Allied Chauffeured Cars (Allied) will meet you in Melbourne Airport&#39;s International Arrival Hall T2.
															If you are arriving on an International Flight, turn right when you leave the customs area and go towards &#39;The Meeting Point&#39;
															(near Yes Optus Mobile Shop). Your driver will hold an &#39;ALLIED&#39; sign for easy identification and will take you to your
															destination as per your instructions below.<br /><br />
															
															If you are arriving on a Domestic Flight, make your way to the arrivals area in the International Terminal T2. <br /><br />
															
															If you arrive at:<br /><br />
															<img src="http://dev.bookings-global.com.au/Images/bullet_icon.png" width="10px" height="9px" />&nbsp; <strong>Qantas Domestic Terminal</strong>, please go to the "Meeting Point" located opposite Baggage Carousel 3.  If you can&#39;t find your driver at the Domestic terminal, please go to <strong>T2</strong> (<strong>International</strong>, next door to your right).<br /><br />
															<img src="http://dev.bookings-global.com.au/Images/bullet_icon.png" width="10px" height="9px" />&nbsp; <strong>Virgin Australia Terminal</strong>, please go to the "Meeting Point" below the escalators. If you can&#39;t find your driver at the Domestic terminal, please go to <strong>T2</strong> (<strong>International</strong>, next door to your left). <br /><br />
															<img src="http://dev.bookings-global.com.au/Images/bullet_icon.png" width="10px" height="9px" />&nbsp; <strong>Terminal 4 (Tiger Airways)</strong>, you have to ring Allied Chauffeured Cars on <strong>1800 350 850 </strong> from <strong>any</strong> public phone (Free Call), the staff will direct you to the meeting point. <br /><br /><br />

															
															<strong>No driver waiting at Melbourne Airport</strong><br/><br/>
															
															Go to &#39;The Meeting Point&#39;.  If your driver is not there within 15 minutes find the nearest public telephone and call Allied
															on 1800 350 850 (this is a <strong>free</strong> call, <strong>NO</strong> coins needed) so they can locate your driver.
															When calling, give Allied your Student ID number (this is in your offer letter), family name and given name.<br/><br/>
															
															<STRONG>Please note,</STRONG> if you do not arrive at your booked time or do not arrive at all without informing us,
															you will have to pay a <strong>cancellation fee </strong> to '.$chargeaccount_details['account_name'].' . <br /><br />
															
															You can contact us by email alliedcars@alliedcars.com.au or by phone <strong>(+61 3) 8383 9999.</strong> <br /><br />
															
															<strong>City of Melbourne - International Student Welcome Booth</strong><br/><br/>
															
															The City of Melbourne operates the &#39;Student Welcome Desk&#39; at Melbourne Airport where volunteers greet you and provide information
															about welfare services, public transport, housing, employment options and social integration to help you with your stay in Melbourne.<br/><br/> 
															
															<strong>Changes to or cancellation of your flight</strong><br/><br/>
															
															To change or cancel your airport pickup booking, contact us at least one full working day before you arrive to avoid additional costs.
															Our office hours are 9am to 5pm Monday to Friday, Australian Eastern Standard Time.<br/><br/> 
															
															To change your arrival details on Saturday or Sunday, telephone +61 3 8383 9999.<br/><br/> 
															
															If you are overseas and miss your flight or if your flight is delayed, please telephone +61 3 8383 9999 immediately to confirm
															your new arrival details.  This will ensure your driver is at Melbourne Airport to meet you.<br/><br/>
															<br/><br/>
															
															MONASH COLLEGE
															<strong>Enrolment</strong><br/><br/>
															Each course has its own enrolment date, time and location.<br/><br/> 
															Monash College students:  https://www.monashcollege.edu.au/live-and-study-in-australia/prepare-to-arrive/enrolment-and-orientation <br/><br/>
															<strong>Orientation program</strong><br/><br/>
															If you are studying the English Language, Foundation Year or Monash College Diploma program, your enrolment and orientation information can be found at:<br/>
															https://www.monashcollege.edu.au/live-and-study-in-australia/prepare-to-arrive/enrolment-and-orientation <br/><br/>
															You must attend Orientation  You will get a lot of valuable information about studying at Monash including your enrolment, study timetable and learning how to access Monash University&#39;s computer resources.<br/><br/>  
															Orientation includes sessions on academic and study skills, Australian culture, campus tours and social opportunities to meet other students.<br/><br/>
															We wish you a pleasant journey and look forward to welcoming you to Monash.<br/><br/>
															Kind regards<br/>
															Monash College<br/><br/>';
									}
								else if($job_reference_details['charge_acc_id'] == '9') // MONASH CONNECT
									{
										$mail->Body  	.=	'Thank you for your airport pickup request for the '.formatDate($j1_details ['job_date'],12).' on '.$j1_details ['frm_flight_no'].' arriving at '.calculateTimeInAmPmFormatWithoutSeconds($j1_details ['job_time']).'.<br/><br/>
															A driver from Allied Chauffeured Cars (Allied) will meet you in Melbourne Airport&#39;s International Arrival Hall. Turn right when you leave the customs area and head toward &#34;The Meeting Point&#34;.<br/>
															The driver will hold a &#34;Monash&#34; sign for easy identification and will take you to the following address:<br/>
															'.$j1_details ['to_line1'].'<br/>
															'.$j1_details ['to_line2'].'<br/>
															'.$j1_details ['to_sub'].' '.$j1_details ['to_pc'].'<br/>
															'.$j1_details ['to_state'].'<br/>
															<b>City of Melbourne - International Student Welcome Booth</b><br/>
															At key times of the year, the City of Melbourne operates the "Student Welcome Desk " at Melbourne Airport. At the welcome desk, volunteers greet you
															and provide information about welfare services, public transport, housing, employment options and social integration to help you with your stay in
															Melbourne.<br/><br/>
															
															<b>No driver waiting at Melbourne Airport</b><br/>
															Go to &#34;The Meeting Point&#34;.  If your driver is not there within 15 minutes find the nearest public telephone and call Allied on 1800 350 850
															(this is a free call).  They will locate your driver. When calling, give Allied your Monash Student ID number (this is in your Monash offer letter),
															family name and given names.<br/><br/>
															
															<b>Changes to or cancellation of your flight</b><br/>
															To change or cancel your airport pickup booking, contact us at least one full working day before you arrive to avoid additional costs.  Our office
															hours are 9am to 5pm Monday to Friday, Australian Eastern Standard Time.<br/>
															
															To change your arrival details on Saturday or Sunday, telephone +61 3 8383 9999.<br/><br/>
															
															If you are overseas and miss your flight or if your flight is delayed, please telephone +61 3 8383 9999 immediately to confirm your new arrival details.
															This will ensure your driver is at Melbourne Airport to meet you.<br/><br/>
															
															<b>Enrolment</b><br/>
															Each course has its own enrolment date, time and location. Refer to the following website for details:  http://monash.edu/study/international/enrolment/<br/><br/>

															<b>Orientation program<b><br/>
															Details of the Orientation program will be available on the following website: http://monash.edu/orientation/<br/><br/>

															In preparation for your arrival on campus, create your personalised Orientation schedule using the eOrientation planner. <br/><br/>
															 
															You must attend Orientation  You will get a lot of valuable information about studying at Monash University including; your enrolment, study timetable and learning how to access the University&#39;s computer resources. <br/> <br/>

															Orientation includes sessions on academic and study skills, Australian culture and working in Australia, campus tours and social opportunities to meet other students.<br/><br/>

															We wish you a pleasant journey and look forward to welcoming you to Monash University.<br/><br/>
															Thank you.<br/>
															Kind Regards<br/>
															<b>'.$chargeaccount_details['account_name'].'</b><br/><br/>
															';
									}
								else if($job_reference_details['charge_acc_id'] == '15') // RMIT
									{
										$mail->Body  	.=	'Thank you for booking with us and we are now pleased to confirm your arrival pick-up. Please show this email confirmation to Allied&#39;s staff.<br/><br/>
															Our driver will meet you at the airport and will be holding a placard <b>"RMIT UNIVERSITY"</b>.<br/><br/>
															
															You have to let us know the number of luggage you have.  Please note if your excess luggage do not fit in a standard sedan&#39;s boot, you will
															have to arrange a taxi to transport any excess luggage. <br/><br/>
															
															Please refrain from bringing food into Australia. As this <b>will</b> delay your immigration and customs clearance time and you <b>may incur extra charges</b>.<br/><br/>
															
															You must look for Allied&#39;s driver <b>immediately</b> at the Meeting Point of your arriving terminal. <br/><br/>
															
															If you experience a delay or cannot find Allied&#39;s driver, you must ring Allied on <b>1800 350 850</b> (FREE CALL) from any public phone. <br/><br/>
															
															Please note a no show fee of <b>AU$105</b> applies if you do not inform Allied of any changes to your arrival itinerary or do not use the service. <br/><br/>
															If you arrive at:
															<ul>
																<li><b>Melbourne Airport-T2 (International) </b>, Please go to the Meeting Point near OPTUS store/ near the TRAVELEX Foreign Exchange counter.  Refer to the "Key 8 & 10" of the attached Melbourne Airport Map.</li>
																<li><b>Qantas Domestic Terminal</b>, please go to the &#34;Meeting Point&#34; located opposite Baggage Carousel 3.  If you can&#39;t find your driver at the Domestic terminal, please go to <b>T2 International</b>, next door to your right). </li>
																<li><b>Virgin Australia Terminal</b>, please go to the &#34;Meeting Point&#34; below the escalators. If you can&#39;t find your driver at the Domestic terminal, please go to<b> T2 (International) </b>, next door to your left). </li>
																<li><b>Terminal 4 (Tiger Airways) </b>, you have to ring Allied Chauffeured Cars on <b>1800 350 850</b> from <b>any</b> public phone (Free Call), the staff will direct you to the meeting point. </li>
															</ul>
															If you are unable to find our staff or have made changes to your arrival itinerary, you must call us. Otherwise we cannot guarantee airport pick-up and you will be charged for the service. <br/>
															Thank you.
															Kind Regards<br/>
															Admin<br/>
															<b>'.$chargeaccount_details['account_name'].'</b><br/><br/>
															';
									}
								else //common body
									{
										$mail->Body  	.=	'Thank you for booking with us and we are now pleased to confirm your students arrival details. <br /><br />
															Please review the booking details and let us know immediately if they are not accurate. <br /><br />
															
															<strong>IMPORTANT INFORMATION</strong><br/><br/>
															
															<strong>IF YOU CAN\'T FIND YOUR DRIVER, FIND A PUBLIC PHONE AND RING ALLIED ON 1800 350 850</STRONG> (this is a <strong>free</strong>
															call, <strong>NO</strong> coins needed)<br /><br />
															
															<strong>YOU NEED TO PRINT THIS CONFIRMATION EMAIL AND BRING IT WITH YOU TO SHOW THE ALLIED REPRESENTATIVE.</strong><br /><br />

															A driver from Allied Chauffeured Cars (Allied) will meet you in Melbourne Airport&#39;s International Arrival Hall T2.
															If you are arriving on an International Flight, turn right when you leave the customs area and go towards &#39;The Meeting Point&#39;
															(near Yes Optus Mobile Shop). Your driver will hold an &#39;ALLIED&#39; sign for easy identification and will take you to your
															destination as per your instructions below.<br /><br />
															
															If you are arriving on a Domestic Flight, make your way to the arrivals area in the International Terminal T2. <br /><br />
															
															If you arrive at:<br /><br />
															<img src="http://dev.bookings-global.com.au/Images/bullet_icon.png" width="10px" height="9px" />&nbsp; <strong>Qantas Domestic Terminal</strong>, please go to the "Meeting Point" located opposite Baggage Carousel 3.  If you can&#39;t find your driver at the Domestic terminal, please go to <strong>T2</strong> (<strong>International</strong>, next door to your right).<br /><br />
															<img src="http://dev.bookings-global.com.au/Images/bullet_icon.png" width="10px" height="9px" />&nbsp; <strong>Virgin Australia Terminal</strong>, please go to the "Meeting Point" below the escalators. If you can&#39;t find your driver at the Domestic terminal, please go to <strong>T2</strong> (<strong>International</strong>, next door to your left). <br /><br />
															<img src="http://dev.bookings-global.com.au/Images/bullet_icon.png" width="10px" height="9px" />&nbsp; <strong>Terminal 4 (Tiger Airways)</strong>, you have to ring Allied Chauffeured Cars on <strong>1800 350 850 </strong> from <strong>any</strong> public phone (Free Call), the staff will direct you to the meeting point. <br /><br /><br />

															
															<strong>No driver waiting at Melbourne Airport</strong><br/><br/>
															
															Go to &#39;The Meeting Point&#39;.  If your driver is not there within 15 minutes find the nearest public telephone and call Allied
															on 1800 350 850 (this is a <strong>free</strong> call, <strong>NO</strong> coins needed) so they can locate your driver.
															When calling, give Allied your Student ID number (this is in your offer letter), family name and given name.<br/><br/>
															
															<STRONG>Please note,</STRONG> if you do not arrive at your booked time or do not arrive at all without informing us,
															you will have to pay a <strong>cancellation fee </strong> to '.$chargeaccount_details['account_name'].' . <br /><br />
															
															You can contact us by email alliedcars@alliedcars.com.au or by phone <strong>(+61 3) 8383 9999.</strong> <br /><br />
															
															<strong>City of Melbourne - International Student Welcome Booth</strong><br/><br/>
															
															The City of Melbourne operates the &#39;Student Welcome Desk&#39; at Melbourne Airport where volunteers greet you and provide information
															about welfare services, public transport, housing, employment options and social integration to help you with your stay in Melbourne.<br/><br/> 
															
															<strong>Changes to or cancellation of your flight</strong><br/><br/>
															
															To change or cancel your airport pickup booking, contact us at least one full working day before you arrive to avoid additional costs.
															Our office hours are 9am to 5pm Monday to Friday, Australian Eastern Standard Time.<br/><br/> 
															
															To change your arrival details on Saturday or Sunday, telephone +61 3 8383 9999.<br/><br/> 
															
															If you are overseas and miss your flight or if your flight is delayed, please telephone +61 3 8383 9999 immediately to confirm
															your new arrival details.  This will ensure your driver is at Melbourne Airport to meet you.<br/><br/>
															<br/><br/>
															
															Thank you for booking with us.<br/>
															Kind Regards, <br /><br/>
															<strong>Allied Chauffeured Cars Australia</strong><br/>ABN: 38 076 977 136<br /><br/>
															<strong>Email:</strong> alliedcars@alliedcars.com.au <br />
															<strong>Telephone:</strong> 61 3 8383 9999 <br /><br/>';
									}	
								$mail->Body    	.= 	$booking_html;
								$mail->Body    	.= 	self::MOBILE_FOOTER;
								$mail->send();
							}
							
						// 4 - TO AGENT
						if($job_reference_details['has_agent'] == '1' && $job_reference_details['agents_email'] != '')
							{
								$mail 			= new PHPMailer;
								$mail->From 	= 'alliedcars@alliedcars.com.au';
								$mail->FromName = 'Allied Cars';
								$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
								$mail->WordWrap = 50;
								$mail->isHTML(true);
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
								$mail->addAddress($job_reference_details['agents_email'],  $job_reference_details['agents_name']);
								$mail->Body    	.=	self::MOBILE_HEADER;
								
								$mail->addStringAttachment($qr, "qr_code.png");
								
								$mail->addAddress($job_reference_details['std_email'],  ''.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].'');
								
								if($job_reference_details['charge_acc_id'] == '15') // RMIT
									{
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Meeting_Point.png",'meeting_point.png','base64','application/png');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/melbourne_airport_RMIT.pdf",'Airport_Map.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/RMIT.pdf",'RMIT.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/WIS_SIGN.pdf",'WIS.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Zhongwen_Airport_Pickup_RMIT.pdf",'Zhongwen.pdf','base64','application/png');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Vietnamise_rmit.pdf",'Dich_vu_dón_tai_sân_bay.pdf','base64','application/png');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Feedback.pdf",'Feedback.pdf','base64','application/pdf');
									}
								if($job_reference_details['charge_acc_id'] == '55') // Monash College
									{
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/melbourne_airport_RMIT.pdf",'Airport_Map.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Meeting_Point.png",'meeting_point.png','base64','application/png');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Monash_College_Sign.pdf",'Monash.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/WIS_SIGN.pdf",'WIS.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Airport_Map_Monash_A.pdf",'Airport_Map_a.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Feedback.pdf",'Feedback.pdf','base64','application/pdf');
									}
								if($job_reference_details['charge_acc_id'] == '9') // Monash Connect
									{
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/melbourne_airport_RMIT.pdf",'Airport_Map.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Meeting_Point.png",'meeting_point.png','base64','application/png');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/WIS_SIGN.pdf",'WIS.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Airport_Map_Monash_A.pdf",'Airport_Map_a.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Feedback.pdf",'Feedback.pdf','base64','application/pdf');
									}
								if($job_reference_details['charge_acc_id'] == '19') // Monash Abroad
									{
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/melbourne_airport_RMIT.pdf",'Airport_Map.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Meeting_Point.png",'meeting_point.png','base64','application/png');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/WIS_SIGN.pdf",'WIS.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Airport_Map_Monash_A.pdf",'Airport_Map_a.pdf','base64','application/pdf');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Feedback.pdf",'Feedback.pdf','base64','application/pdf');
									}			
								else // common attachments
									{
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/Meeting_Point.png",'meeting_point.png','base64','application/png');
										$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/mail_attachments/WIS_SIGN.pdf",'WIS.pdf','base64','application/pdf');
									}
									
								if($job_reference_details['under_18'] == '1')
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].') UNDER 18, BOOKING CONFIRMED by '.$chargeaccount_details['account_name'].' ';
									}
								else
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].'), BOOKING CONFIRMED by '.$chargeaccount_details['account_name'].' ';
									}
								
								$mail->Body    	.= 	'<h3>BOOKING CONFIRMED</h3>';
								$mail->Body    	.= 	'<strong>'.$job_reference_details['agents_name'].',</strong><br/><br/>';
								if($job_reference_details['charge_acc_id'] == '55') // MONASH COLLEGE
									{
										$mail->Body  	.=	'Thank you for booking with us and we are now pleased to confirm your students arrival details. <br /><br />
															Please review the booking details and let us know immediately if they are not accurate. <br /><br />
															
															<strong>IMPORTANT INFORMATION</strong><br/><br/>
															
															<strong>IF YOU CAN&#39;T FIND YOUR DRIVER, FIND A PUBLIC PHONE AND RING ALLIED ON 1800 350 850</STRONG> (this is a <strong>free</strong>
															call, <strong>NO</strong> coins needed)<br /><br />
															
															<strong>YOU NEED TO PRINT THIS CONFIRMATION EMAIL AND BRING IT WITH YOU TO SHOW THE ALLIED REPRESENTATIVE.</strong><br /><br />

															A driver from Allied Chauffeured Cars (Allied) will meet you in Melbourne Airport&#39;s International Arrival Hall T2.
															If you are arriving on an International Flight, turn right when you leave the customs area and go towards &#39;The Meeting Point&#39;
															(near Yes Optus Mobile Shop). Your driver will hold an &#39;ALLIED&#39; sign for easy identification and will take you to your
															destination as per your instructions below.<br /><br />
															
															If you are arriving on a Domestic Flight, make your way to the arrivals area in the International Terminal T2. <br /><br />
															
															If you arrive at:<br /><br />
															<img src="http://dev.bookings-global.com.au/Images/bullet_icon.png" width="10px" height="9px" />&nbsp; <strong>Qantas Domestic Terminal</strong>, please go to the "Meeting Point" located opposite Baggage Carousel 3.  If you can&#39;t find your driver at the Domestic terminal, please go to <strong>T2</strong> (<strong>International</strong>, next door to your right).<br /><br />
															<img src="http://dev.bookings-global.com.au/Images/bullet_icon.png" width="10px" height="9px" />&nbsp; <strong>Virgin Australia Terminal</strong>, please go to the "Meeting Point" below the escalators. If you can&#39;t find your driver at the Domestic terminal, please go to <strong>T2</strong> (<strong>International</strong>, next door to your left). <br /><br />
															<img src="http://dev.bookings-global.com.au/Images/bullet_icon.png" width="10px" height="9px" />&nbsp; <strong>Terminal 4 (Tiger Airways)</strong>, you have to ring Allied Chauffeured Cars on <strong>1800 350 850 </strong> from <strong>any</strong> public phone (Free Call), the staff will direct you to the meeting point. <br /><br /><br />

															
															<strong>No driver waiting at Melbourne Airport</strong><br/><br/>
															
															Go to &#39;The Meeting Point&#39;.  If your driver is not there within 15 minutes find the nearest public telephone and call Allied
															on 1800 350 850 (this is a <strong>free</strong> call, <strong>NO</strong> coins needed) so they can locate your driver.
															When calling, give Allied your Student ID number (this is in your offer letter), family name and given name.<br/><br/>
															
															<STRONG>Please note,</STRONG> if you do not arrive at your booked time or do not arrive at all without informing us,
															you will have to pay a <strong>cancellation fee </strong> to '.$chargeaccount_details['account_name'].' . <br /><br />
															
															You can contact us by email alliedcars@alliedcars.com.au or by phone <strong>(+61 3) 8383 9999.</strong> <br /><br />
															
															<strong>City of Melbourne - International Student Welcome Booth</strong><br/><br/>
															
															The City of Melbourne operates the &#39;Student Welcome Desk&#39; at Melbourne Airport where volunteers greet you and provide information
															about welfare services, public transport, housing, employment options and social integration to help you with your stay in Melbourne.<br/><br/> 
															
															<strong>Changes to or cancellation of your flight</strong><br/><br/>
															
															To change or cancel your airport pickup booking, contact us at least one full working day before you arrive to avoid additional costs.
															Our office hours are 9am to 5pm Monday to Friday, Australian Eastern Standard Time.<br/><br/> 
															
															To change your arrival details on Saturday or Sunday, telephone +61 3 8383 9999.<br/><br/> 
															
															If you are overseas and miss your flight or if your flight is delayed, please telephone +61 3 8383 9999 immediately to confirm
															your new arrival details.  This will ensure your driver is at Melbourne Airport to meet you.<br/><br/>
															<br/><br/>
															
															MONASH COLLEGE
															<strong>Enrolment</strong><br/><br/>
															Each course has its own enrolment date, time and location.<br/><br/> 
															Monash College students:  https://www.monashcollege.edu.au/live-and-study-in-australia/prepare-to-arrive/enrolment-and-orientation <br/><br/>
															<strong>Orientation program</strong><br/><br/>
															If you are studying the English Language, Foundation Year or Monash College Diploma program, your enrolment and orientation information can be found at:<br/>
															https://www.monashcollege.edu.au/live-and-study-in-australia/prepare-to-arrive/enrolment-and-orientation <br/><br/>
															You must attend Orientation  You will get a lot of valuable information about studying at Monash including your enrolment, study timetable and learning how to access Monash University&#39;s computer resources.<br/><br/>  
															Orientation includes sessions on academic and study skills, Australian culture, campus tours and social opportunities to meet other students.<br/><br/>
															We wish you a pleasant journey and look forward to welcoming you to Monash.<br/><br/>
															Kind regards<br/>
															Monash College<br/><br/>';
									}
								else if($job_reference_details['charge_acc_id'] == '9') // MONASH CONNECT
									{
										$mail->Body  	.=	'Thank you for your airport pickup request for the '.formatDate($j1_details ['job_date'],12).' on '.$j1_details ['frm_flight_no'].' arriving at '.calculateTimeInAmPmFormatWithoutSeconds($j1_details ['job_time']).'.<br/><br/>
															A driver from Allied Chauffeured Cars (Allied) will meet you in Melbourne Airport&#39;s International Arrival Hall. Turn right when you leave the customs area and head toward &#34;The Meeting Point&#34;.<br/>
															The driver will hold a &#34;ALLIED&#34; sign for easy identification and will take you to the following address:<br/>
															'.$j1_details ['to_line1'].'<br/>
															'.$j1_details ['to_line2'].'<br/>
															'.$j1_details ['to_sub'].' '.$j1_details ['to_pc'].'<br/>
															'.$j1_details ['to_state'].'<br/>
															<b>City of Melbourne - International Student Welcome Booth</b><br/>
															At key times of the year, the City of Melbourne operates the "Student Welcome Desk " at Melbourne Airport. At the welcome desk, volunteers greet you
															and provide information about welfare services, public transport, housing, employment options and social integration to help you with your stay in
															Melbourne.<br/><br/>
															
															<b>No driver waiting at Melbourne Airport</b><br/>
															Go to &#34;The Meeting Point&#34;.  If your driver is not there within 15 minutes find the nearest public telephone and call Allied on 1800 350 850
															(this is a free call).  They will locate your driver. When calling, give Allied your Monash Student ID number (this is in your Monash offer letter),
															family name and given names.<br/><br/>
															
															<b>Changes to or cancellation of your flight</b><br/>
															To change or cancel your airport pickup booking, contact us at least one full working day before you arrive to avoid additional costs.  Our office
															hours are 9am to 5pm Monday to Friday, Australian Eastern Standard Time.<br/>
															
															To change your arrival details on Saturday or Sunday, telephone +61 3 8383 9999.<br/><br/>
															
															If you are overseas and miss your flight or if your flight is delayed, please telephone +61 3 8383 9999 immediately to confirm your new arrival details.
															This will ensure your driver is at Melbourne Airport to meet you.<br/><br/>
															
															<b>Enrolment</b><br/>
															Each course has its own enrolment date, time and location. Refer to the following website for details:  http://monash.edu/study/international/enrolment/<br/><br/>

															<b>Orientation program<b><br/>
															Details of the Orientation program will be available on the following website: http://monash.edu/orientation/<br/><br/>

															In preparation for your arrival on campus, create your personalised Orientation schedule using the eOrientation planner. <br/><br/>
															 
															You must attend Orientation  You will get a lot of valuable information about studying at Monash University including; your enrolment, study timetable and learning how to access the University&#39;s computer resources. <br/> <br/>

															Orientation includes sessions on academic and study skills, Australian culture and working in Australia, campus tours and social opportunities to meet other students.<br/><br/>

															We wish you a pleasant journey and look forward to welcoming you to Monash University.<br/><br/>
															Thank you.<br/>
															Kind Regards<br/>
															<b>'.$chargeaccount_details['account_name'].'</b><br/><br/>
															';
									}
								else if($job_reference_details['charge_acc_id'] == '871') // AHN 
									{
										$mail->Body  	.=	'Thank you for your airport pickup request.<br/><br/>
															
															The driver will hold a &#34;ALLIED&#34; sign for easy identification and will take you to the following address:<br/>
															'.$j1_details ['to_line1'].'<br/>
															'.$j1_details ['to_line2'].'<br/>
															'.$j1_details ['to_sub'].' '.$j1_details ['to_pc'].'<br/>
															'.$j1_details ['to_state'].'<br/>
															
															<b>PLANE DELAY OR CANCELLATION</b><br/>
															*Please not that there is a luggage limit.  You are able to put the equivalent of 2 suitcases and 1 carry bag into your pickup car.  If you exceed this limit your driver will be unable to take you to your homestay.<br/> 
															AHN must be advised of any changes to your plans prior to your arrival.  If you change your flight or arrival details; you must give AHN 24 business hours notice, otherwise you will be charged the cost of the airport pickup service.<br/>
															If you miss your connecting flight, please notify Allied Chauffeured Cars on 1800 350 850 or AHN on +61 3 9435 6621<br/>
															
															If you require further assistance please do not hesitate to contact us:
																During business hours on +61 3 9435 6621.
																After hours on +61 2 8905 0321 if outside Australia or 1300 697 829 from inside Australia<br/>
															
															If you are overseas and miss your flight or if your flight is delayed, please telephone +61 3 8383 9999 immediately to confirm your new arrival details.
															This will ensure your driver is at Melbourne Airport to meet you.<br/><br/>
															
															We wish you a pleasant journey and look forward to welcoming you to Australia.<br/><br/>
															Thank you.<br/>
															Kind Regards<br/>
															<b>'.$chargeaccount_details['account_name'].'</b><br/><br/>
															';
									}
								else if($job_reference_details['charge_acc_id'] == '15') // RMIT
									{
										$mail->Body  	.=	'Thank you for booking with us and we are now pleased to confirm your arrival pick-up. Please show this email confirmation to Allied&#39;s driver.<br/><br/>
															Our driver will meet you at the airport and will be holding a placard <b>"RMIT UNIVERSITY"</b>.<br/><br/>
															
															You have to let us know the number of luggage you have.  Please note if your excess luggage do not fit in a standard sedan&#39;s boot, you will
															have to arrange a taxi to transport any excess luggage. <br/><br/>
															
															Please refrain from bringing food into Australia. As this <b>will</b> delay your immigration and customs clearance time and you <b>may incur extra charges</b>.<br/><br/>
															
															You must look for Allied&#39;s driver <b>immediately</b> at the Meeting Point of your arriving terminal. <br/><br/>
															
															If you experience a delay or cannot find Allied&#39;s driver, you must ring Allied on <b>1800 350 850</b> (FREE CALL) from any public phone. <br/><br/>
															
															Please note a no show fee of <b>AU$105</b> applies if you do not inform Allied of any changes to your arrival itinerary or do not use the service. <br/><br/>
															If you arrive at:
															<ul>
																<li><b>Melbourne Airport-T2 (International) </b>, Please go to the Meeting Point near OPTUS store/ near the TRAVELEX Foreign Exchange counter.  Refer to the "Key 8 & 10" of the attached Melbourne Airport Map.</li>
																<li><b>Qantas Domestic Terminal</b>, please go to the &#34;Meeting Point&#34; located opposite Baggage Carousel 3.  If you can&#39;t find your driver at the Domestic terminal, please go to <b>T2 International</b>, next door to your right). </li>
																<li><b>Virgin Australia Terminal</b>, please go to the &#34;Meeting Point&#34; below the escalators. If you can&#39;t find your driver at the Domestic terminal, please go to<b> T2 (International) </b>, next door to your left). </li>
																<li><b>Terminal 4 (Tiger Airways) </b>, you have to ring Allied Chauffeured Cars on <b>1800 350 850</b> from <b>any</b> public phone (Free Call), the staff will direct you to the meeting point. </li>
															</ul>
															If you are unable to find our staff or have made changes to your arrival itinerary, you must call us. Otherwise we cannot guarantee airport pick-up and you will be charged for the service. <br/>
															Thank you.
															Kind Regards<br/>
															Admin<br/>
															<b>'.$chargeaccount_details['account_name'].'</b><br/><br/>
															';
									}
								else //common body
									{
										$mail->Body  	.=	'Thank you for booking with us and we are now pleased to confirm your students arrival details. <br /><br />
															Please review the booking details and let us know immediately if they are not accurate. <br /><br />
															
															<strong>IMPORTANT INFORMATION</strong><br/><br/>
															
															<strong>IF YOU CAN\'T FIND YOUR DRIVER, FIND A PUBLIC PHONE AND RING ALLIED ON 1800 350 850</STRONG> (this is a <strong>FREE</strong>
															call, <strong>NO</strong> coins needed)<br /><br />
															
															<strong>YOU NEED TO PRINT THIS CONFIRMATION EMAIL AND BRING IT WITH YOU TO SHOW THE ALLIED REPRESENTATIVE.</strong><br /><br />

															A driver from Allied Chauffeured Cars (Allied) will meet you in Melbourne Airport&#39;s International Arrival Hall T2.
															If you are arriving on an International Flight, turn right when you leave the customs area and go towards &#39;The Meeting Point&#39;
															(near Yes Optus Mobile Shop). Your driver will hold an &#39;ALLIED&#39; sign for easy identification and will take you to your
															destination as per your instructions below.<br /><br />
															
															If you are arriving on a Domestic Flight, make your way to the arrivals area in the International Terminal T2. <br /><br />
															
															If you arrive at:<br /><br />
															<img src="http://dev.bookings-global.com.au/Images/bullet_icon.png" width="10px" height="9px" />&nbsp; <strong>Qantas Domestic Terminal</strong>, please go to the "Meeting Point" located opposite Baggage Carousel 3.  If you can&#39;t find your driver at the Domestic terminal, please go to <strong>T2</strong> (<strong>International</strong>, next door to your right).<br /><br />
															<img src="http://dev.bookings-global.com.au/Images/bullet_icon.png" width="10px" height="9px" />&nbsp; <strong>Virgin Australia Terminal</strong>, please go to the "Meeting Point" below the escalators. If you can&#39;t find your driver at the Domestic terminal, please go to <strong>T2</strong> (<strong>International</strong>, next door to your left). <br /><br />
															<img src="http://dev.bookings-global.com.au/Images/bullet_icon.png" width="10px" height="9px" />&nbsp; <strong>Terminal 4 (Tiger Airways)</strong>, you have to ring Allied Chauffeured Cars on <strong>1800 350 850 </strong> from <strong>any</strong> public phone (Free Call), the staff will direct you to the meeting point. <br /><br /><br />

															
															<strong>No driver waiting at Melbourne Airport</strong><br/><br/>
															
															Go to &#39;The Meeting Point&#39;.  If your driver is not there within 15 minutes find the nearest public telephone and call Allied
															on 1800 350 850 (this is a <strong>free</strong> call, <strong>NO</strong> coins needed) so they can locate your driver.
															When calling, give Allied your Student ID number (this is in your offer letter), family name and given name.<br/><br/>
															
															<STRONG>Please note,</STRONG> if you do not arrive at your booked time or do not arrive at all without informing us,
															you will have to pay a <strong>cancellation fee </strong> to '.$chargeaccount_details['account_name'].' . <br /><br />
															
															You can contact us by email alliedcars@alliedcars.com.au or by phone <strong>(+61 3) 8383 9999.</strong> <br /><br />
															
															<strong>City of Melbourne - International Student Welcome Booth</strong><br/><br/>
															
															The City of Melbourne operates the &#39;Student Welcome Desk&#39; at Melbourne Airport where volunteers greet you and provide information
															about welfare services, public transport, housing, employment options and social integration to help you with your stay in Melbourne.<br/><br/> 
															
															<strong>Changes to or cancellation of your flight</strong><br/><br/>
															
															To change or cancel your airport pickup booking, contact us at least one full working day before you arrive to avoid additional costs.
															Our office hours are 9am to 5pm Monday to Friday, Australian Eastern Standard Time.<br/><br/> 
															
															If you are overseas and miss your flight or if your flight is delayed, please telephone +61 3 8383 9999 immediately to confirm
															your new arrival details.  This will ensure your driver is at Melbourne Airport to meet you.<br/><br/>
															<br/><br/>
															
															Thank you for booking with us.<br/>
															Kind Regards, <br /><br/>
															<strong>Allied Chauffeured Cars Australia</strong><br/>ABN: 38 076 977 136<br /><br/>
															<strong>Email:</strong> alliedcars@alliedcars.com.au <br />
															<strong>Telephone:</strong> 61 3 8383 9999 <br /><br/>';
									}						
								$mail->Body    	.= 	$booking_html;
								$mail->Body    	.= 	self::MOBILE_FOOTER;
								$mail->send();
							}
						
						// 5 - Under 18 Department
						if($job_reference_details['under_18'] == '1')
							{
								$query = "SELECT * from variable__university where charge_acc_id = '".$job_reference_details['charge_acc_id']."'";
								$database = new database;
								$result = $database->query($query);
								$row = mysql_fetch_assoc($result);
						
								if($row['under_18_email'] != '')
									{
										$mail 			= new PHPMailer;
										$mail->From 	= 'alliedcars@alliedcars.com.au';
										$mail->FromName = 'Allied Cars';
										$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
										$mail->WordWrap = 50;
										$mail->isHTML(true);
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
										$mail->addAddress($row['under_18_email'],  ''.$chargeaccount_details['account_name'].' UNDER 18');
										$mail->Body    	.=	self::MOBILE_HEADER;
										if($job_reference_details['under_18'] == '1')
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' UNDER 18, Pickup Date '.formatDate($j1_details['job_date'], 12).' BOOKING CONFIRMED for Booking ID - '.$job_reference_details['j1_id'].'';
											}
										else
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].', Pickup Date '.formatDate($j1_details['job_date'], 12).' BOOKING CONFIRMED for Booking ID - '.$job_reference_details['j1_id'].'';
											}
										
										$mail->Body    	.= 	'<h3>BOOKING CONFIRMED</h3>';
										$mail->Body  	.=	'The following are the details of booking for under 18 student of '.$chargeaccount_details['account_name'].'.<br/><br/>';
										$mail->Body    	.= 	$booking_html;
										$mail->Body    	.= 	self::MOBILE_FOOTER;
										$mail->send();
									}
							}
							
						// 6- Home Stay Department
						if($job_reference_details['is_homestay'] == '1')
							{
								$query = "SELECT * from variable__university where charge_acc_id = '".$job_reference_details['charge_acc_id']."'";
								$database = new database;
								$result = $database->query($query);
								$row = mysql_fetch_assoc($result);
						
								if($row['under_18_email'] != '')
									{
										$mail 			= new PHPMailer;
										$mail->From 	= 'alliedcars@alliedcars.com.au';
										$mail->FromName = 'Allied Cars';
										$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
										$mail->WordWrap = 50;
										$mail->isHTML(true);
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
										$mail->addAddress($row['homestay_email'],  ''.$chargeaccount_details['account_name'].' '.$row['homestay_label'].'');
										$mail->Body    	.=	self::MOBILE_HEADER;
										if($job_reference_details['under_18'] == '1')
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' UNDER 18, Pickup Date '.formatDate($j1_details['job_date'], 12).' BOOKING CONFIRMED for Booking ID - '.$job_reference_details['j1_id'].'';
											}
										else
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].', Pickup Date '.formatDate($j1_details['job_date'], 12).' BOOKING CONFIRMED for Booking ID - '.$job_reference_details['j1_id'].'';
											}
										
										$mail->Body    	.= 	'<h3>BOOKING CONFIRMED</h3>';
										$mail->Body  	.=	'The following are the details of booking for '.$chargeaccount_details['account_name'].' student availing '.$row['homestay_label'].'.<br/><br/>';
										$mail->Body    	.= 	$booking_html;
										$mail->Body    	.= 	self::MOBILE_FOOTER;
										$mail->send();
									}
							}
							
						
					}
				
// 	7 	ON EDITING STUDENT BOOKING - DATE/TIME AND DROPOFF LOCATION ARE SAME
				//	if edit_flage = 0, send notification to - 1, 2, 4, 7 (edit flag 0 means limited editing)
				//	if edit_flage = 1, send notification to - 1, 2, 3, 4, 5, 6, 7 (edit flag 0 means limited editing)
				if($process_type == '7')
					{
						
						//Send to student
						$mail 			= new PHPMailer;
						$mail->From 	= 'alliedcars@alliedcars.com.au';
						$mail->FromName = 'Allied Cars';
						$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
						$mail->WordWrap = 50;
						$mail->isHTML(true);
						//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
						//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
						$mail->addAddress($job_reference_details['std_email'],  ''.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].'');
						$mail->Body    	.=	self::MOBILE_HEADER;
						if($job_reference_details['under_18'] == '1')
							{
								$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].') UNDER 18, BOOKING EDITED';
							}
						else
							{
								$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].'), BOOKING EDITED';
							}
						
						$mail->Body    	.= 	'<h3>BOOKING EDITED</h3>';
						$mail->Body    	.= 	'<strong>Dear '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].',</strong><br/><br/>';
						$mail->Body  	.=	'Your booking has been edited by Allied Cars.<br/><br/>
											Please review the booking details and let us know immediately if they are not accurate.<br/><br/>';
						$mail->Body    	.= 	$booking_html;
						$mail->Body    	.= 	self::MOBILE_FOOTER;
						$mail->send();
						
						// 2 - TO ON BEHALF OF
						if($job_reference_details['on_behalf'] == '1' && $job_reference_details['on_behalf_email'] != '')
							{
								$mail 			= new PHPMailer;
								$mail->From 	= 'alliedcars@alliedcars.com.au';
								$mail->FromName = 'Allied Cars';
								$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
								$mail->WordWrap = 50;
								$mail->isHTML(true);
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
								$mail->addAddress($job_reference_details['on_behalf_email'],  ''.$job_reference_details['on_behalf_title'].' '.$job_reference_details['on_behalf_fname'].' '.$job_reference_details['on_behalf_lname'].'');
								$mail->Body    	.=	self::MOBILE_HEADER;
								if($job_reference_details['under_18'] == '1')
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].') UNDER 18,BOOKING EDITED';
									}
								else
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].'), BOOKING EDITED';
									}
								
								$mail->Body    	.= 	'<h3>BOOKING EDITED</h3>';
								$mail->Body    	.= 	'<strong>Dear '.$job_reference_details['on_behalf_title'].' '.$job_reference_details['on_behalf_fname'].' '.$job_reference_details['on_behalf_lname'].',</strong><br/><br/>';
								$mail->Body  	.=	'Your booking has been edited by Allied Cars.<br/><br/>
													Please review the booking details and let us know immediately if they are not accurate.<br/><br/>';
								$mail->Body    	.= 	$booking_html;
								$mail->Body    	.= 	self::MOBILE_FOOTER;
								$mail->send();
							}
						// 3 - TO GUARDIAN
						if($job_reference_details['under_18'] == '1' && $job_reference_details['stu_guar_email'] != '')
							{
								// Do Nothing
							}	
						// 4 - TO AGENT
						if($job_reference_details['has_agent'] == '1' && $job_reference_details['agents_email'] != '')
							{
								$mail 			= new PHPMailer;
								$mail->From 	= 'alliedcars@alliedcars.com.au';
								$mail->FromName = 'Allied Cars';
								$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
								$mail->WordWrap = 50;
								$mail->isHTML(true);
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
								$mail->addAddress($job_reference_details['agents_email'],  $job_reference_details['agents_name']);
								$mail->Body    	.=	self::MOBILE_HEADER;
								if($job_reference_details['under_18'] == '1')
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].') UNDER 18, BOOKING EDITED';
									}
								else
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].'), BOOKING EDITED';
									}
								
								$mail->Body    	.= 	'<h3>BOOKING EDITED</h3>';
								$mail->Body    	.= 	'<strong>'.$job_reference_details['agents_name'].',</strong><br/><br/>';
								$mail->Body  	.=	'Your booking has been edited by Allied Cars.<br/><br/>
													Please review the booking details and let us know immediately if they are not accurate.<br/><br/>';
								$mail->Body    	.= 	$booking_html;
								$mail->Body    	.= 	self::MOBILE_FOOTER;
								$mail->send();
							}
						// 5 - UNDER 18 Department
						if($job_reference_details['under_18'] == '1')
							{
								// Do Nothing
							}
						// 6 - HOMESTAY Department
						if($job_reference_details['is_homestay'] == '1')
							{
								// Do Nothing
							}	
						// 7 - TO UNIVERSITY
						$admin_array = $chargeaccounts->getAllChargeAccountContacts($job_reference_details['charge_acc_id']);
						foreach($admin_array AS $admin)
							{
								if($admin['email'] != '')
									{
										$mail 			= new PHPMailer;
										$mail->From 	= 'alliedcars@alliedcars.com.au';
										$mail->FromName = 'Allied Cars';
										$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
										$mail->WordWrap = 50;
										$mail->isHTML(true);
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
										$mail->addAddress($admin['email'],  ''.$admin['title'].' '.$admin['fname'].' '.$admin['lname'].'');
										$mail->Body    	.=	self::MOBILE_HEADER;
										if($job_reference_details['under_18'] == '1')
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' UNDER 18, Booking ID - '.$job_reference_details['j1_id'].' EDITED';
											}
										else
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].', Booking ID - '.$job_reference_details['j1_id'].' EDITED';
											}
										
										$mail->Body    	.= 	'<h3>BOOKING EDITED</h3>';
										$mail->Body  	.=	'The following student booking has been edited by Allied Cars.<br/><br/>
															Please review the booking details and let us know immediately if they are not accurate.<br/><br/>';
										$mail->Body    	.= 	$booking_html;
										$mail->Body    	.= 	self::MOBILE_FOOTER;
										$mail->send();
									}
							}
						if($edit_flag == '1') // means pickup data and time or dropoff address changed
							{
								// 3 - TO GUARDIAN
								if($job_reference_details['under_18'] == '1' && $job_reference_details['stu_guar_email'] != '')
									{
										// Do Nothing
									}	
								// 5 - UNDER 18 Department and not to RMIT
								if($job_reference_details['under_18'] == '1' && $job_reference_details['id'] !='15')
									{
										$query = "SELECT * from variable__university where charge_acc_id = '".$job_reference_details['charge_acc_id']."'";
										$database = new database;
										$result = $database->query($query);
										$row = mysql_fetch_assoc($result);
								
										if($row['under_18_email'] != '')
											{
												$mail 			= new PHPMailer;
												$mail->From 	= 'alliedcars@alliedcars.com.au';
												$mail->FromName = 'Allied Cars';
												$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
												$mail->WordWrap = 50;
												$mail->isHTML(true);
												//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
												//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
												$mail->addAddress($row['under_18_email'],  ''.$chargeaccount_details['account_name'].' UNDER 18');
												$mail->Body    	.=	self::MOBILE_HEADER;
												if($job_reference_details['under_18'] == '1')
													{
														$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' UNDER 18, Pickup Date '.formatDate($j1_details['job_date'], 12).' Booking ID - '.$job_reference_details['j1_id'].' EDITED';
													}
												else
													{
														$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].', Pickup Date '.formatDate($j1_details['job_date'], 12).' Booking ID - '.$job_reference_details['j1_id'].' EDITED';
													}
												
												$mail->Body    	.= 	'<h3>BOOKING EDITED</h3>';
												$mail->Body  	.=	'The following booking for under 18 student of '.$chargeaccount_details['account_name'].' has been edited.<br/><br/>';
												$mail->Body    	.= 	$booking_html;
												$mail->Body    	.= 	self::MOBILE_FOOTER;
												$mail->send();
											}
									}
								// 6 - HOMESTAY Department
								if($job_reference_details['is_homestay'] == '1')
									{
										$query = "SELECT * from variable__university where charge_acc_id = '".$job_reference_details['charge_acc_id']."'";
										$database = new database;
										$result = $database->query($query);
										$row = mysql_fetch_assoc($result);
								
										if($row['homestay_email'] != '')
											{
												$mail 			= new PHPMailer;
												$mail->From 	= 'alliedcars@alliedcars.com.au';
												$mail->FromName = 'Allied Cars';
												$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
												$mail->WordWrap = 50;
												$mail->isHTML(true);
												//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
												//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
												$mail->addAddress($row['homestay_email'],  ''.$chargeaccount_details['account_name'].' '.$row['homestay_label'].'');
												$mail->Body    	.=	self::MOBILE_HEADER;
												if($job_reference_details['under_18'] == '1')
													{
														$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' UNDER 18, Pickup Date '.formatDate($j1_details['job_date'], 12).' Booking ID - '.$job_reference_details['j1_id'].' EDITED';
													}
												else
													{
														$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].', Pickup Date '.formatDate($j1_details['job_date'], 12).' Booking ID - '.$job_reference_details['j1_id'].' EDITED';
													}
												
												$mail->Body    	.= 	'<h3>BOOKING EDITED</h3>';
												$mail->Body  	.=	'The following booking for under 18 student of '.$chargeaccount_details['account_name'].' has been edited.<br/><br/>';
												$mail->Body    	.= 	$booking_html;
												$mail->Body    	.= 	self::MOBILE_FOOTER;
												$mail->send();
											}
									}	
							}
					}
				
// 	8 	ON BOOKING CANCELLATION
				if($process_type == '8')
					{
						//Send to student
						$mail 			= new PHPMailer;
						$mail->From 	= 'alliedcars@alliedcars.com.au';
						$mail->FromName = 'Allied Cars';
						$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
						$mail->WordWrap = 50;
						$mail->isHTML(true);
						//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
						//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
						$mail->addAddress($job_reference_details['std_email'],  ''.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].'');
						$mail->Body    	.=	self::MOBILE_HEADER;
						if($job_reference_details['under_18'] == '1')
							{
								$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].') UNDER 18, BOOKING CANCELLED';
							}
						else
							{
								$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].'), BOOKING CANCELLED';
							}
						
						$mail->Body    	.= 	'<h3>BOOKING CANCELLED</h3>';
						$mail->Body    	.= 	'<strong>Dear '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].',</strong><br/><br/>';
						$mail->Body  	.=	'Your booking has been cancelled by Allied Cars.<br/><br/>';
						$mail->Body    	.= 	$booking_html;
						$mail->Body    	.= 	self::MOBILE_FOOTER;
						$mail->send();
								
						// 2 - TO ON BEHALF OF
						if($job_reference_details['on_behalf'] == '1' && $job_reference_details['on_behalf_email'] != '')
							{
								$mail 			= new PHPMailer;
								$mail->From 	= 'alliedcars@alliedcars.com.au';
								$mail->FromName = 'Allied Cars';
								$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
								$mail->WordWrap = 50;
								$mail->isHTML(true);
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
								$mail->addAddress($job_reference_details['on_behalf_email'],  ''.$job_reference_details['on_behalf_title'].' '.$job_reference_details['on_behalf_fname'].' '.$job_reference_details['on_behalf_lname'].'');
								$mail->Body    	.=	self::MOBILE_HEADER;
								if($job_reference_details['under_18'] == '1')
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].') UNDER 18, BOOKING CANCELLED';
									}
								else
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].'),BOOKING CANCELLED';
									}
								
								$mail->Body    	.= 	'<h3>BOOKING CANCELLED</h3>';
								$mail->Body    	.= 	'<strong>Dear '.$job_reference_details['on_behalf_title'].' '.$job_reference_details['on_behalf_fname'].' '.$job_reference_details['on_behalf_lname'].',</strong><br/><br/>';
								$mail->Body  	.=	'Your booking has been cancelled by Allied Cars.<br/><br/>';
								$mail->Body    	.= 	$booking_html;
								$mail->Body    	.= 	self::MOBILE_FOOTER;
								$mail->send();
							}
						// 3 - TO GUARDIAN
						if($job_reference_details['under_18'] == '1' && $job_reference_details['stu_guar_email'] != '')
							{
								// Do Nothing
							}	
						// 4 - TO AGENT
						if($job_reference_details['has_agent'] == '1' && $job_reference_details['agents_email'] != '')
							{
								$mail 			= new PHPMailer;
								$mail->From 	= 'alliedcars@alliedcars.com.au';
								$mail->FromName = 'Allied Cars';
								$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
								$mail->WordWrap = 50;
								$mail->isHTML(true);
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
								$mail->addAddress($job_reference_details['agents_email'],  $job_reference_details['agents_name']);
								$mail->Body    	.=	self::MOBILE_HEADER;
								if($job_reference_details['under_18'] == '1')
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].') UNDER 18, BOOKING CANCELLED';
									}
								else
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].'), BOOKING CANCELLED';
									}
								
								$mail->Body    	.= 	'<h3>BOOKING CANCELLED</h3>';
								$mail->Body    	.= 	'<strong>'.$job_reference_details['agents_name'].',</strong><br/><br/>';
								$mail->Body  	.=	'Your booking has been cancelled by Allied Cars.<br/><br/>';
								$mail->Body    	.= 	$booking_html;
								$mail->Body    	.= 	self::MOBILE_FOOTER;
								$mail->send();
							}
						// 5 - UNDER 18 Department
						if($job_reference_details['under_18'] == '1')
							{
								// Do Nothing
							}
						// 6 - HOMESTAY Department
						if($job_reference_details['is_homestay'] == '1')
							{
								// Do Nothing
							}	
						// 7 - TO UNIVERSITY
						$admin_array = $chargeaccounts->getAllChargeAccountContacts($job_reference_details['charge_acc_id']);
						foreach($admin_array AS $admin)
							{
								if($admin['email'] != '')
									{
										$mail 			= new PHPMailer;
										$mail->From 	= 'alliedcars@alliedcars.com.au';
										$mail->FromName = 'Allied Cars';
										$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
										$mail->WordWrap = 50;
										$mail->isHTML(true);
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
										$mail->addAddress($admin['email'],  ''.$admin['title'].' '.$admin['fname'].' '.$admin['lname'].'');
										$mail->Body    	.=	self::MOBILE_HEADER;
										if($job_reference_details['under_18'] == '1')
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' UNDER 18, Booking ID - '.$job_reference_details['j1_id'].' CANCELLED';
											}
										else
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].', Booking ID - '.$job_reference_details['j1_id'].' CANCELLED';
											}
										
										$mail->Body    	.= 	'<h3>BOOKING CANCELLED</h3>';
										$mail->Body  	.=	'The following student booking has been Cancelled by Allied Cars.<br/><br/>
															Please review the booking details and let us know immediately if they are not accurate.<br/><br/>';
										$mail->Body    	.= 	$booking_html;
										$mail->Body    	.= 	self::MOBILE_FOOTER;
										$mail->send();
									}
							}
								
						// 5 - UNDER 18 Department
						if($job_reference_details['under_18'] == '1')
							{
								// 3 - TO GUARDIAN
								if($job_reference_details['under_18'] == '1' && $job_reference_details['stu_guar_email'] != '')
									{
										$mail 			= new PHPMailer;
										$mail->From 	= 'alliedcars@alliedcars.com.au';
										$mail->FromName = 'Allied Cars';
										$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
										$mail->WordWrap = 50;
										$mail->isHTML(true);
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
										$mail->addAddress($job_reference_details['std_guar_email'],  ''.$job_reference_details['std_guar_title'].' '.$job_reference_details['std_guar_fname'].' '.$job_reference_details['std_guar_lname'].'');
										$mail->Body    	.=	self::MOBILE_HEADER;
										if($job_reference_details['under_18'] == '1')
											{
												$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].') UNDER 18, CANCELLED';
											}
										else
											{
												$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].'), CANCELLED';
											}
										
										$mail->Body    	.= 	'<h3>BOOKING CANCELLED</h3>';
										$mail->Body    	.= 	'<strong>Dear '.$job_reference_details['std_guar_title'].' '.$job_reference_details['std_guar_fname'].' '.$job_reference_details['std_guar_lname'].',</strong><br/><br/>';
										$mail->Body  	.=	'The following student booking has been Cancelled by Allied Cars.<br/><br/>
															Please review the booking details and let us know immediately if they are not accurate.<br/><br/>';
										$mail->Body    	.= 	$booking_html;
										$mail->Body    	.= 	self::MOBILE_FOOTER;
										$mail->send();
									}	
								// 5 To UNDER 18 Department
								$query = "SELECT * from variable__university where charge_acc_id = '".$job_reference_details['charge_acc_id']."'";
								$database = new database;
								$result = $database->query($query);
								$row = mysql_fetch_assoc($result);
						
								if($row['under_18_email'] != '')
									{
										$mail 			= new PHPMailer;
										$mail->From 	= 'alliedcars@alliedcars.com.au';
										$mail->FromName = 'Allied Cars';
										$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
										$mail->WordWrap = 50;
										$mail->isHTML(true);
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
										$mail->addAddress($row['under_18_email'],  ''.$chargeaccount_details['account_name'].' UNDER 18');
										$mail->Body    	.=	self::MOBILE_HEADER;
										if($job_reference_details['under_18'] == '1')
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' UNDER 18, Pickup Date '.formatDate($j1_details['job_date'], 12).' Booking ID - '.$job_reference_details['j1_id'].' CANCELLED';
											}
										else
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].', Pickup Date '.formatDate($j1_details['job_date'], 12).' Booking ID - '.$job_reference_details['j1_id'].' CANCELLED';
											}
										$mail->Body    	.= 	'<h3>BOOKING CANCELLED</h3>';
										$mail->Body  	.=	'The following student booking has been Cancelled by Allied Cars.<br/><br/>
															Please review the booking details and let us know immediately if they are not accurate.<br/><br/>';
										$mail->Body    	.= 	$booking_html;
										$mail->Body    	.= 	self::MOBILE_FOOTER;
										$mail->send();
									}
							}
						// 6 - HOMESTAY Department
						if($job_reference_details['is_homestay'] == '1')
							{
								$query = "SELECT * from variable__university where charge_acc_id = '".$job_reference_details['charge_acc_id']."'";
								$database = new database;
								$result = $database->query($query);
								$row = mysql_fetch_assoc($result);
						
								if($row['homestay_email'] != '')
									{
										$mail 			= new PHPMailer;
										$mail->From 	= 'alliedcars@alliedcars.com.au';
										$mail->FromName = 'Allied Cars';
										$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
										$mail->WordWrap = 50;
										$mail->isHTML(true);
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
										$mail->addAddress($row['homestay_email'],  ''.$chargeaccount_details['account_name'].' '.$row['homestay_label'].'');
										$mail->Body    	.=	self::MOBILE_HEADER;
										if($job_reference_details['under_18'] == '1')
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' UNDER 18, Pickup Date '.formatDate($j1_details['job_date'], 12).' Booking ID - '.$job_reference_details['j1_id'].' CANCELLED';
											}
										else
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].', Pickup Date '.formatDate($j1_details['job_date'], 12).' Booking ID - '.$job_reference_details['j1_id'].' CANCELLED';
											}
										
										$mail->Body    	.= 	'<h3>BOOKING CANCELLED</h3>';
										$mail->Body  	.=	'The following student booking has been Cancelled by Allied Cars.<br/><br/>
															Please review the booking details and let us know immediately if they are not accurate.<br/><br/>';
										$mail->Body    	.= 	$booking_html;
										$mail->Body    	.= 	self::MOBILE_FOOTER;
										$mail->send();
									}
							}	
					}				
				
// 	9 	ON NO SHOW

				if($process_type == '9')
					{
						//Send to student
						$mail 			= new PHPMailer;
						$mail->From 	= 'alliedcars@alliedcars.com.au';
						$mail->FromName = 'Allied Cars';
						$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
						$mail->WordWrap = 50;
						$mail->isHTML(true);
						//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
						//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
						$mail->addAddress($job_reference_details['std_email'],  ''.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].'');
						$mail->Body    	.=	self::MOBILE_HEADER;
						$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].'), NO SHOW';
						$mail->Body    	.= 	'<h3>DRIVER REPORTED - NO SHOW</h3>';
						$mail->Body    	.= 	'<strong>Dear '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].',</strong><br/><br/>';
						$mail->Body  	.=	'For the following booking, driver from Allied Cars has reported No Show for student.<br/><br/>';
						$mail->Body    	.= 	$booking_html;
						$mail->Body    	.= 	self::MOBILE_FOOTER;
						$mail->send();
								
						// 2 - TO ON BEHALF OF
						if($job_reference_details['on_behalf'] == '1' && $job_reference_details['on_behalf_email'] != '')
							{
								$mail 			= new PHPMailer;
								$mail->From 	= 'alliedcars@alliedcars.com.au';
								$mail->FromName = 'Allied Cars';
								$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
								$mail->WordWrap = 50;
								$mail->isHTML(true);
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
								$mail->addAddress($job_reference_details['on_behalf_email'],  ''.$job_reference_details['on_behalf_title'].' '.$job_reference_details['on_behalf_fname'].' '.$job_reference_details['on_behalf_lname'].'');
								$mail->Body    	.=	self::MOBILE_HEADER;
								if($job_reference_details['under_18'] == '1')
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].') UNDER 18, NO SHOW';
									}
								else
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].'), NO SHOW';
									}
								
								$mail->Body    	.= 	'<h3>DRIVER REPORTED - NO SHOW</h3>';
								$mail->Body    	.= 	'<strong>Dear '.$job_reference_details['on_behalf_title'].' '.$job_reference_details['on_behalf_fname'].' '.$job_reference_details['on_behalf_lname'].',</strong><br/><br/>';
								$mail->Body  	.=	'For the following booking, driver from Allied Cars has reported No Show for student.<br/><br/>';
								$mail->Body    	.= 	$booking_html;
								$mail->Body    	.= 	self::MOBILE_FOOTER;
								$mail->send();
							}
						// 3 - TO GUARDIAN
						if($job_reference_details['under_18'] == '1' && $job_reference_details['stu_guar_email'] != '')
							{
								// Do Nothing
							}	
						// 4 - TO AGENT
						if($job_reference_details['has_agent'] == '1' && $job_reference_details['agents_email'] != '')
							{
								$mail 			= new PHPMailer;
								$mail->From 	= 'alliedcars@alliedcars.com.au';
								$mail->FromName = 'Allied Cars';
								$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
								$mail->WordWrap = 50;
								$mail->isHTML(true);
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
								//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
								$mail->addAddress($job_reference_details['agents_email'],  $job_reference_details['agents_name']);
								$mail->Body    	.=	self::MOBILE_HEADER;
								if($job_reference_details['under_18'] == '1')
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].') UNDER 18, NO SHOW';
									}
								else
									{
										$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].'), NO SHOW';
									}
								
								$mail->Body    	.= 	'<h3>DRIVER REPORTED - NO SHOW</h3>';
								$mail->Body    	.= 	'<strong>Dear '.$job_reference_details['agents_name'].',</strong><br/><br/>';
								$mail->Body  	.=	'For the following booking, driver from Allied Cars has reported No Show for student.<br/><br/>';
								$mail->Body    	.= 	$booking_html;
								$mail->Body    	.= 	self::MOBILE_FOOTER;
								$mail->send();
							}
						// 7 - TO UNIVERSITY
						$admin_array = $chargeaccounts->getAllChargeAccountContacts($job_reference_details['charge_acc_id']);
						foreach($admin_array AS $admin)
							{
								if($admin['email'] != '')
									{
										$mail 			= new PHPMailer;
										$mail->From 	= 'alliedcars@alliedcars.com.au';
										$mail->FromName = 'Allied Cars';
										$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
										$mail->WordWrap = 50;
										$mail->isHTML(true);
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
										$mail->addAddress($admin['email'],  ''.$admin['title'].' '.$admin['fname'].' '.$admin['lname'].'');
										$mail->Body    	.=	self::MOBILE_HEADER;
										if($job_reference_details['under_18'] == '1')
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' UNDER 18, Booking ID - '.$job_reference_details['j1_id'].' NO SHOW';
											}
										else
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].', Booking ID - '.$job_reference_details['j1_id'].' NO SHOW';
											}
										
										$mail->Body    	.= 	'<h3>DRIVER REPORTED - NO SHOW</h3>';
										$mail->Body  	.=	'For the following booking, driver from Allied Cars has reported No Show for student.<br/><br/>';
										$mail->Body    	.= 	$booking_html;
										$mail->Body    	.= 	self::MOBILE_FOOTER;
										$mail->send();
									}
							}
								
						// 5 - UNDER 18 Department
						if($job_reference_details['under_18'] == '1')
							{
								// 3 - TO GUARDIAN
								if($job_reference_details['under_18'] == '1' && $job_reference_details['stu_guar_email'] != '')
									{
										$mail 			= new PHPMailer;
										$mail->From 	= 'alliedcars@alliedcars.com.au';
										$mail->FromName = 'Allied Cars';
										$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
										$mail->WordWrap = 50;
										$mail->isHTML(true);
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
										$mail->addAddress($job_reference_details['std_guar_email'],  ''.$job_reference_details['std_guar_title'].' '.$job_reference_details['std_guar_fname'].' '.$job_reference_details['std_guar_lname'].'');
										$mail->Body    	.=	self::MOBILE_HEADER;
										if($job_reference_details['under_18'] == '1')
											{
												$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].') UNDER 18, NO SHOW';
											}
										else
											{
												$mail->Subject 	= 	'Booking ID - '.$job_reference_details['j1_id'].', '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' ('.$job_reference_details['std_id'].'), NO SHOW';
											}
										
										$mail->Body    	.= 	'<h3>DRIVER REPORTED - NO SHOW</h3>';
										$mail->Body    	.= 	'<strong>Dear '.$job_reference_details['std_guar_title'].' '.$job_reference_details['std_guar_fname'].' '.$job_reference_details['std_guar_lname'].',</strong><br/><br/>';
										$mail->Body  	.=	'For the following booking, driver from Allied Cars has reported No Show for student.<br/><br/>';
										$mail->Body    	.= 	$booking_html;
										$mail->Body    	.= 	self::MOBILE_FOOTER;
										$mail->send();
									}	
								// 5 To UNDER 18 Department
								$query = "SELECT * from variable__university where charge_acc_id = '".$job_reference_details['charge_acc_id']."'";
								$database = new database;
								$result = $database->query($query);
								$row = mysql_fetch_assoc($result);
						
								if($row['under_18_email'] != '')
									{
										$mail 			= new PHPMailer;
										$mail->From 	= 'alliedcars@alliedcars.com.au';
										$mail->FromName = 'Allied Cars';
										$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
										$mail->WordWrap = 50;
										$mail->isHTML(true);
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
										$mail->addAddress($row['under_18_email'],  ''.$chargeaccount_details['account_name'].' UNDER 18');
										$mail->Body    	.=	self::MOBILE_HEADER;
										if($job_reference_details['under_18'] == '1')
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' UNDER 18, Pickup Date '.formatDate($j1_details['job_date'], 12).' Booking ID - '.$job_reference_details['j1_id'].' NO SHOW';
											}
										else
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].', Pickup Date '.formatDate($j1_details['job_date'], 12).' Booking ID - '.$job_reference_details['j1_id'].' NO SHOW';
											}
										
										$mail->Body    	.= 	'<h3>DRIVER REPORTED - NO SHOW</h3>';
										$mail->Body  	.=	'For the following booking, driver from Allied Cars has reported No Show for student.<br/><br/>';
										$mail->Body    	.= 	$booking_html;
										$mail->Body    	.= 	self::MOBILE_FOOTER;
										$mail->send();
									}
							}
						// 6 - HOMESTAY Department
						if($job_reference_details['is_homestay'] == '1')
							{
								$query = "SELECT * from variable__university where charge_acc_id = '".$job_reference_details['charge_acc_id']."'";
								$database = new database;
								$result = $database->query($query);
								$row = mysql_fetch_assoc($result);
						
								if($row['homestay_email'] != '')
									{
										$mail 			= new PHPMailer;
										$mail->From 	= 'alliedcars@alliedcars.com.au';
										$mail->FromName = 'Allied Cars';
										$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
										$mail->WordWrap = 50;
										$mail->isHTML(true);
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'header_image');
										//$mail->AddEmbeddedImage('Images/logo_Allied.png', 'footer_image');
										$mail->addAddress($row['homestay_email'],  ''.$chargeaccount_details['account_name'].' '.$row['homestay_label'].'');
										$mail->Body    	.=	self::MOBILE_HEADER;
										if($job_reference_details['under_18'] == '1')
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].' UNDER 18, Pickup Date '.formatDate($j1_details['job_date'], 12).' Booking ID - '.$job_reference_details['j1_id'].' NO SHOW';
											}
										else
											{
												$mail->Subject 	= 	''.$job_reference_details['std_id'].' '.$job_reference_details['std_title'].' '.$job_reference_details['std_fname'].' '.$job_reference_details['std_lname'].', Pickup Date '.formatDate($j1_details['job_date'], 12).' Booking ID - '.$job_reference_details['j1_id'].' NO SHOW';
											}
										
										$mail->Body    	.= 	'<h3>DRIVER REPORTED - NO SHOW</h3>';
										$mail->Body  	.=	'For the following booking, driver from Allied Cars has reported No Show for student.<br/><br/>';
										$mail->Body    	.= 	$booking_html;
										$mail->Body    	.= 	self::MOBILE_FOOTER;
										$mail->send();
									}
							}	
					}								
			}
		public function testEmail()
			{
				$mail 			= new PHPMailer;
				$mail->From 	= 'alliedcars@alliedcars.com.au';
				$mail->FromName = 'Allied Cars';
				$mail->addReplyTo('alliedcars@alliedcars.com.au', 'Allied Cars');
				$mail->WordWrap = 50;
				$mail->isHTML(true);
				$mail->addAddress('bbobbymahalanaabis@gmail.com',  'Anirvan Mahalanabis');
				//$mail->AddEmbeddedImage('/var/www/bookingsglobal/Images/logo_Allied.png', 'header_image');
				//$mail->AddEmbeddedImage('/var/www/bookingsglobal/Images/logo_Allied.png', 'footer_image');
				$mail->Body    	.=	self::MOBILE_HEADER;
				$mail->Body    	.= 	self::MOBILE_FOOTER;
				$mail->send();
				//$mime_message = $mail->CreateBody(); //Retrieve the message content
				//echo $mime_message; // Echo it to the screen or send it using whatever method you want
			}
	}
?>