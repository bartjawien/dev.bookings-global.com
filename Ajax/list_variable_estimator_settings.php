<?php
ini_set("display_errors", 0);
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(INCLUDE_PATH . "functions.php");

$database = new Database;
try
	{
/*-------------------LISTING-----------------------------------------------------------------------------------------------------*/
		if($_POST['search_string'])
			{
				$database = new database;
				$query = "SELECT COUNT(*) AS RecordCount FROM cars
							WHERE 
							(id like '%".$_POST['search_string']."%'
							OR car_type like '%".$_POST['search_string']."%')";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];
				
				
				$query = "SELECT * FROM cars
							WHERE 
							(id like '%".$_POST['search_string']."%'
							OR car_type like '%".$_POST['search_string']."%'
							ORDER BY 
							".$_GET["jtSorting"]." LIMIT ".$_GET["jtStartIndex"].", ".$_GET["jtPageSize"]."";
				$result = $database->query($query);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['Records'] = $rows;
				print json_encode($jTableResult);
			}
/*-------------------LISTING-----------------------------------------------------------------------------------------------------*/

		else if($_GET["action"] == "list_estimator_variable")
			{
				//Get record count
				$query = "SELECT COUNT(*) AS RecordCount FROM cars";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];

				//Get records from database
				$query = "SELECT *
							FROM cars
							ORDER BY
							".$_GET["jtSorting"]." LIMIT ".$_GET["jtStartIndex"].", ".$_GET["jtPageSize"]."";
				$result = $database->query($query);
				//Add all records to an array
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}

				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['Records'] = $rows;
				print json_encode($jTableResult);
			}

/*-------------------CREATE-----------------------------------------------------------------------------------------------------*/

		else if($_GET["action"] == "create_estimator_variable")
			{
				foreach(array_keys($_POST) as $key)
					{
						$clean[$key] = mysql_real_escape_string($_POST[$key]);
					}
				$result = mysql_query("INSERT INTO cars
										(
											id,
											car_type,
											min_fare,
											rate_per_km,
											driver_com_fixed,
											driver_com_percentage
										) 
									VALUES
										(
											NULL,
											'".$clean['car_type']."',
											'".$clean['min_fare']."',
											'".$clean['rate_per_km']."',
											'".$clean['driver_com_fixed']."',
											'".$clean['driver_com_percentage']."'
										)");

				$result = mysql_query("SELECT * FROM cars WHERE id = LAST_INSERT_ID();");
				$row = mysql_fetch_array($result);
				
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['Record'] = $row;
				print json_encode($jTableResult);
			}
/*-------------------UPDATE-----------------------------------------------------------------------------------------------------*/

		else if($_GET["action"] == "update_estimator_variable")
			{
				$database = new database;
				//Update record into database
				foreach(array_keys($_POST) as $key)
					{
						$clean[$key] = mysql_real_escape_string($_POST[$key]);
					}
				$query = "UPDATE cars SET 
							car_type				= '".$clean["car_type"]."',
							min_fare				= '".$clean["min_fare"]."',
							rate_per_km				= '".$clean["rate_per_km"]."',
							driver_com_fixed		= '".$clean["driver_com_fixed"]."',
							driver_com_percentage	= '".$clean["driver_com_percentage"]."'
							WHERE id 	= ".$clean['id']."";
				$result = $database->query($query);
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}

/*-------------------DELETE-----------------------------------------------------------------------------------------------------*/

		else if($_GET["action"] == "delete_estimator_variable")
			{
				$database = new database;
				//Delete from database
				$query = "DELETE FROM cars WHERE id = ".$_POST["id"]."";
				$result = $database->query($query);
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}
/*-------------------EXPORT-----------------------------------------------------------------------------------------------------*/
			
		else if($_GET["action"] == "export_excel")
			{
				
				require_once '../Classes/PHPExcel.php';
				
				$database = new database;
				$objPHPExcel = new PHPExcel();
				
								// Create a first sheet
				$objPHPExcel->getProperties()->setCreator("".BUSINESS_NAME."")
							 ->setLastModifiedBy("".$_SESSION['TITLE']." ".$_SESSION['FNAME']." ".$_SESSION['LNAME']."")
							 ->setTitle("Fare Estimator Settings - Variable Type for ".BUSINESS_NAME."")
							 ->setSubject("Fare Estimator Settings - Variable Type for ".BUSINESS_NAME."")
							 ->setDescription("Fare Estimator Settings - Variable Type for ".BUSINESS_NAME."")
							 ->setKeywords("Fare Estimator Settings - Variable Type")
							 ->setCategory("Fare Estimator Settings - Variable Type");
							 
				$objPHPExcel->setActiveSheetIndex(0);
				$objPHPExcel->getActiveSheet()->setCellValue('A1', "ID");
				$objPHPExcel->getActiveSheet()->setCellValue('B1', "CAR TYPE");
				$objPHPExcel->getActiveSheet()->setCellValue('C1', "MINIMUM FARE");
				$objPHPExcel->getActiveSheet()->setCellValue('D1', "RATE PER KM");
				$objPHPExcel->getActiveSheet()->setCellValue('E1', "DRIVER DISCOUNT FIXED");
				$objPHPExcel->getActiveSheet()->setCellValue('F1', "DRIVER DISCOUNT PERCENTAGE");
				
				// Hide "ID" column
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setVisible(false);
				
				$query = "SELECT *
							FROM cars
							ORDER BY id ASC";
				$result = $database->query($query);

				$i = 2;
				while($row = mysql_fetch_array($result))
					{
						$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A'.$i, $row['id'])
							->setCellValue('B'.$i, $row['car_type'])
							->setCellValue('C'.$i, $row['min_fare'])
							->setCellValue('D'.$i, $row['rate_per_km'])
							->setCellValue('E'.$i, $row['driver_com_fixed'])
							->setCellValue('F'.$i, $row['driver_com_percentage']);
						$i++;
					}
				$todays_date = Date("d-m-Y-His");
				header('Content-Type: application/vnd.ms-excel');
				$fare_estimator_report = "Variable_FE_".$todays_date.".xls";
				header('Content-Disposition: attachment;filename='.$fare_estimator_report.'');
				header('Cache-Control: max-age=0');
				$objPHPExcel->getActiveSheet()->setTitle('Variable FE Settings');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$objWriter->save('php://output');
			}
						
	}
catch(Exception $ex)
	{
		//Return error message
		$jTableResult = array();
		$jTableResult['Result'] = "ERROR";
		$jTableResult['Message'] = $ex->getMessage();
		print json_encode($jTableResult);
	}
?>