<?php
ini_set("display_errors", 0);
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(INCLUDE_PATH . "functions.php");
session_start();

$database = new Database;
try
	{
/*-------------------LISTING-----------------------------------------------------------------------------------------------------*/
		/*if($_POST['search_string'])
			{
				$database = new database;
				$query = "SELECT COUNT(*) AS RecordCount FROM user
							WHERE 
							(id like '%".$_POST['search_string']."%'
							OR title like '%".$_POST['search_string']."%'
							OR fname like '%".$_POST['search_string']."%'
							OR lname like '%".$_POST['search_string']."%'
							OR password like '%".$_POST['search_string']."%'
							AND role_id = '1')";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];
				
				
				$query = "SELECT * FROM user
							WHERE 
							(id like '%".$_POST['search_string']."%'
							OR title like '%".$_POST['search_string']."%'
							OR fname like '%".$_POST['search_string']."%'
							OR lname like '%".$_POST['search_string']."%'
							OR password like '%".$_POST['search_string']."%'
							AND role_id = '1'
							ORDER BY 
							".$_GET["jtSorting"]." LIMIT ".$_GET["jtStartIndex"].", ".$_GET["jtPageSize"]."";
				$result = $database->query($query);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['Records'] = $rows;
				print json_encode($jTableResult);
			}*/
/*-------------------LISTING-----------------------------------------------------------------------------------------------------*/

		if($_GET["action"] == "list_booking_log")
			{
				//Get record count
				$query = "SELECT COUNT(*) AS RecordCount FROM log__job";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];

				//Get records from database
				$query = "SELECT *, 
							concat(user.title,' ',user.fname,' ',user.lname) AS entered_by,
							DATE_FORMAT(log__job.created_on,'%d/%m/%Y') AS created_on
							FROM log__job
							LEFT JOIN user ON user.id = log__job.entered_by 
							ORDER BY log__job.id DESC
							LIMIT ".$_GET["jtStartIndex"].", ".$_GET["jtPageSize"]."";
							//echo $query;
				$result = $database->query($query);
				//Add all records to an array
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
			
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['Records'] = $rows;
				print json_encode($jTableResult);
			}
		
		
/*-------------------DELETE-----------------------------------------------------------------------------------------------------*/

		else if($_GET["action"] == "delete_booking_log")
			{
				$database = new database;
				//Delete from database
				$query = "DELETE FROM log__job WHERE id in( ".$_POST["id"].")";
				$result = $database->query($query);
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}
		
						
	}
catch(Exception $ex)
	{
		//Return error message
		$jTableResult = array();
		$jTableResult['Result'] = "ERROR";
		$jTableResult['Message'] = $ex->getMessage();
		print json_encode($jTableResult);
	}
?>