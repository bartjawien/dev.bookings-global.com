<?php
ini_set("display_errors", 0);
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(INCLUDE_PATH . "functions.php");

$database = new Database;
try
	{
/*-------------------LISTING-----------------------------------------------------------------------------------------------------*/
		if($_POST['search_string'])
			{
				$database = new database;
				$query = "SELECT COUNT(*) AS RecordCount FROM cars__prices
							WHERE 
							id like '%".$_POST['search_string']."%'
							OR suburb like '%".$_POST['search_string']."%'";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];
				
				
				$query = "SELECT * FROM cars__prices
							WHERE 
							id like '%".$_POST['search_string']."%'
							OR suburb like '%".$_POST['search_string']."%'
							ORDER BY 
							".$_GET["jtSorting"]." LIMIT ".$_GET["jtStartIndex"].", ".$_GET["jtPageSize"]."";
				$result = $database->query($query);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['Records'] = $rows;
				print json_encode($jTableResult);
			}
/*-------------------LISTING-----------------------------------------------------------------------------------------------------*/

		else if($_GET["action"] == "list_estimator_fixed")
			{
				//Get record count
				$query = "SELECT COUNT(*) AS RecordCount FROM cars__prices";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];

				//Get records from database
				$query = "SELECT *
							FROM cars__prices
							ORDER BY
							".$_GET["jtSorting"]." LIMIT ".$_GET["jtStartIndex"].", ".$_GET["jtPageSize"]."";
				$result = $database->query($query);
				//Add all records to an array
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}

				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['Records'] = $rows;
				print json_encode($jTableResult);
			}

/*-------------------CREATE-----------------------------------------------------------------------------------------------------*/

		else if($_GET["action"] == "create_estimator_fixed")
			{
				foreach(array_keys($_POST) as $key)
					{
						$clean[$key] = mysql_real_escape_string($_POST[$key]);
					}
				$result = mysql_query("INSERT INTO cars__prices
										(
											id,
											suburb,
											cbd,
											driver_fee,
											melb_airport,
											driver_fee_ap,
											monash_sedan,
											rmit_Sedan,
											driver_fee_sedan,
											monash_van,
											RMIT_van,
											driver_fee_Van,
											monash_commuter,
											RMIT_commuter,
											Driver_commuter,
											avalon_airport
										) 
									VALUES
										(
											NULL,
											'".$clean['suburb']."',
											'".$clean['cbd']."',
											'".$clean['driver_fee']."',
											'".$clean['melb_airport']."',
											'".$clean['driver_fee_ap']."',
											'".$clean['monash_sedan']."',
											'".$clean['RMIT_Sedan']."',
											'".$clean['driver_fee_sedan']."',
											'".$clean['monash_van']."',
											'".$clean['RMIT_van']."',
											'".$clean['driver_fee_Van']."',
											'".$clean['monash_commuter']."',
											'".$clean['RMIT_commuter']."',
											'".$clean['Driver_commuter']."',
											'".$clean['avalon_airport']."'
										)");

				$result = mysql_query("SELECT * FROM cars__prices WHERE id = LAST_INSERT_ID();");
				$row = mysql_fetch_array($result);
				
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['Record'] = $row;
				print json_encode($jTableResult);
			}
/*-------------------UPDATE-----------------------------------------------------------------------------------------------------*/

		else if($_GET["action"] == "update_estimator_fixed")
			{
				$database = new database;
				//Update record into database   goes below avalon_airport WHERE id 	= ".$clean['id']."";   avalon_airport ends with "'
				foreach(array_keys($_POST) as $key)
					{
						$clean[$key] = mysql_real_escape_string($_POST[$key]);
					}
				$query = "UPDATE cars__prices SET 
							suburb				= '".$clean["suburb"]."',
							cbd					= '".$clean["cbd"]."',
							driver_fee			= '".$clean['driver_fee']."',
							melb_airport		= '".$clean["melb_airport"]."',
							driver_fee_ap		= '".$clean['driver_fee_ap']."',
							monash_sedan		= '".$clean['monash_sedan']."',
							RMIT_Sedan			= '".$clean['RMIT_Sedan']."',
							driver_fee_sedan	= '".$clean['driver_fee_sedan']."',
							moansh_van			= '".$clean['monash_van']."',
							RMIT_van			= '".$clean['RMIT_van']."',
							driver_fee_Van		= '".$clean['driver_fee_Van']."',
							monash_commuter		= '".$clean['monash_commuter']."',
							RMIT_commuter		= '".$clean['RMIT_commuter']."',
							Driver_commuter		= '".$clean['Driver_commuter']."',
							avalon_airport		= '".$clean["avalon_airport"]."";
							
				$result = $database->query($query);
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}

/*-------------------DELETE-----------------------------------------------------------------------------------------------------*/

		else if($_GET["action"] == "delete_estimator_fixed")
			{
				$database = new database;
				//Delete from database
				$query = "DELETE FROM cars__prices WHERE id = ".$_POST["id"]."";
				$result = $database->query($query);
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}
/*-------------------EXPORT-----------------------------------------------------------------------------------------------------*/
			
		else if($_GET["action"] == "export_excel")
			{
				
				require_once '../Classes/PHPExcel.php';
				
				$database = new database;
				$objPHPExcel = new PHPExcel();
				
								// Create a first sheet
				$objPHPExcel->getProperties()->setCreator("".BUSINESS_NAME."")
							 ->setLastModifiedBy("".$_SESSION['TITLE']." ".$_SESSION['FNAME']." ".$_SESSION['LNAME']."")
							 ->setTitle("Fare Estimator Settings - Fixed Type for ".BUSINESS_NAME."")
							 ->setSubject("Fare Estimator Settings - Fixed Type for ".BUSINESS_NAME."")
							 ->setDescription("Fare Estimator Settings - Fixed Type for ".BUSINESS_NAME."")
							 ->setKeywords("Fare Estimator Settings - Fixed Type")
							 ->setCategory("Fare Estimator Settings - Fixed Type");
							 
				$objPHPExcel->setActiveSheetIndex(0);
				$objPHPExcel->getActiveSheet()->setCellValue('A1', "ID");
				$objPHPExcel->getActiveSheet()->setCellValue('B1', "SUBURB");
				$objPHPExcel->getActiveSheet()->setCellValue('C1', "TO CBD");
				$objPHPExcel->getActiveSheet()->setCellValue('D1', "TO MELBOURNE AIRPORT");
				$objPHPExcel->getActiveSheet()->setCellValue('E1', "FEE DRIVER CORPORATE");
				$objPHPExcel->getActiveSheet()->setCellValue('F1', "FEE DRIVER UNI");
				$objPHPExcel->getActiveSheet()->setCellValue('G1', "TO AVALON AIRPORT");
				
				// Hide "ID" column
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setVisible(false);
				
				$query = "SELECT *
							FROM cars__prices
							ORDER BY id ASC";
				$result = $database->query($query);

				$i = 2;
				while($row = mysql_fetch_array($result))
					{
						$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A'.$i, $row['id'])
							->setCellValue('B'.$i, $row['suburb'])
							->setCellValue('C'.$i, $row['cbd'])
							->setCellValue('D'.$i, $row['melb_airport'])
							->setCellValue('E'.$i, $row['avalon_airport'])
							->setCellValue('F'.$i, $row['RMIT_van']);
						$i++;
					}
				$todays_date = Date("d-m-Y-His");
				header('Content-Type: application/vnd.ms-excel');
				$fare_estimator_report = "Fixed_FE_".$todays_date.".xls";
				header('Content-Disposition: attachment;filename='.$fare_estimator_report.'');
				header('Cache-Control: max-age=0');
				$objPHPExcel->getActiveSheet()->setTitle('Fixed FE Settings');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$objWriter->save('php://output');
			}
						
	}
catch(Exception $ex)
	{
		//Return error message
		$jTableResult = array();
		$jTableResult['Result'] = "ERROR";
		$jTableResult['Message'] = $ex->getMessage();
		print json_encode($jTableResult);
	}
?>