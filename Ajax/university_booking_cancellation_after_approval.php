<?php
ini_set("display_errors", 1);
require_once("../init.php");
include(''.INCLUDE_PATH.'config.php');
include(''.INCLUDE_PATH.'settings.php');
require_once(CLASSES_PATH . "mailer.php");
require_once(CLASSES_PATH . "jobReference.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "job.php");
require_once(CLASSES_PATH . "chargeAccount.php");

$database = new database;
$job = new job();
$job_reference = new JobReference();
$mailer = new Mailer();
$user = new User();
$chargeaccount = new chargeAccount();

$job_id= $_POST['job_id'];
$user_id = $_SESSION['USER_ID'];
$user_details = $user->getUserDetails($user_id);
$j1_details = $job->getJobDetails($job_id);
$job_reference_id = $j1_details['job_reference_id'];
$job_reference_details = $job_reference->getJobReferenceDetails($job_reference_id);
//print_r($job_reference_details);

if($job_reference_details['job_type'] == '2')
	{
		$j2_details = $job->getJobDetails($job_reference_details['j2_id']);
	}

if($j1_details['job_status'] =='10') // Received and waiting for approval
	{
		
		$job->updateJob($j1_details['id'], 'job_status', '100'); //change status of the job to update

		$job->addJobLog($j1_details['id'], $user_id, 'Job Cancelled by '.$user_details['title'].' '.$user_details['fname'].' '.$user_details['lname'].'', '10', '100'); // add job log
		
		$job_reference->updateJobReferenceTable($job_reference_id, 'bkg_by_id', $user_id);
		//Add in Job Reference Log Table
		$job_reference->addJobReferenceLog($job_reference_id, $user_id, 'job cancelled by customer after approval', $j1_details['id'], 'NIL');
		
		$mailer->sendUniversityMail($job_reference_id, '8', '0');
		echo "This job has been cancelled.";
		
	}
?>