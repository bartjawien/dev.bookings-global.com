<?php
ini_set("display_errors", 0);
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "chargeAccount.php");
require_once(CLASSES_PATH . "booking.php");
require_once(CLASSES_PATH . "address.php");
session_start();
$database = new database;
$charge_acc = new chargeAccount();
$user = new user();
$address = new address();
$booking = new booking();
$bkg_1_id = $_GET['booking_id_1'];
$bkg_1_details = $booking->getBookingDetails($bkg_1_id);
if($bkg_1_details['bkg_type'] == '2')
	{
		$bkg_2_id = $_GET['booking_id_2'];
		$bkg_2_details = $booking->getBookingDetails($bkg_2_id);
	}
$ret_val = $booking->addBooking(
1, //booking__status
1, //booking__status_confirm_bkg_by
1, //booking__status_confirm_pax
1, //booking__status_driver
1, //booking__status_invoice
$_SESSION['USER_ID'],
$bkg_1_details['bkg_src'],
$bkg_1_details['order_ref'],
$bkg_1_details['acc_type'],
$bkg_1_details['std_id'],
$bkg_1_details['std_name'],
$bkg_1_details['charge_acc_id'],
$bkg_1_details['bkg_by_id'],
$bkg_1_details['pax_id'],
$bkg_1_details['bkg_type'],
$bkg_1_details['bkg_notes'],
$bkg_1_details['extra_notes'],
$bkg_1_details['int_notes'],
$bkg_1_details['bkg_date'],
$bkg_1_details['bkg_time'],
$bkg_1_details['frm_flight_no'],
$bkg_1_details['frm_line1'],
$bkg_1_details['frm_line2'],
$bkg_1_details['frm_sub'],
$bkg_1_details['from_pc'],
$bkg_1_details['frm_state'],
$bkg_1_details['to_flight_no'],
$bkg_1_details['to_line1'],
$bkg_1_details['to_line2'],
$bkg_1_details['to_sub'],
$bkg_1_details['to_pc'],
$bkg_1_details['to_state'],
$bkg_1_details['car_id'],
$bkg_1_details['pax_nos'],
$bkg_1_details['luggage'],
$bkg_1_details['baby_seat'],
$bkg_1_details['booster_seat'],
$bkg_1_details['baby_capsule'],
$bkg_1_details['charge_mode'],
$bkg_1_details['kms'],
$bkg_1_details['fare'],
$bkg_1_details['inter'],
$bkg_1_details['ed'],
$bkg_1_details['wait'],
$bkg_1_details['tolls'],
$bkg_1_details['bs'],
$bkg_1_details['park'],
$bkg_1_details['ah'],
$bkg_1_details['me'],
$bkg_1_details['alc'],
$bkg_1_details['fc'],
$bkg_1_details['oth'],
$bkg_1_details['tot_fare'],
$bkg_1_details['drv_fee'],
$bkg_1_details['oth_exp'],
$bkg_1_details['profit'],
$bkg_2_details['bkg_date'],
$bkg_2_details['bkg_time'],
$bkg_2_details['frm_flight_no'],
$bkg_2_details['frm_line1'],
$bkg_2_details['frm_line2'],
$bkg_2_details['frm_sub'],
$bkg_2_details['frm_pc'],
$bkg_2_details['frm_state'],
$bkg_2_details['to_flight_no'],
$bkg_2_details['to_line1'],
$bkg_2_details['to_line2'],
$bkg_2_details['to_sub'],
$bkg_2_details['to_pc'],
$bkg_2_details['to_state'],
$bkg_2_details['car_id'],
$bkg_2_details['pax_nos'],
$bkg_2_details['luggage'],
$bkg_2_details['baby_seat'],
$bkg_2_details['booster_seat'],
$bkg_2_details['baby_capsule'],
$bkg_2_details['kms'],
$bkg_2_details['fare'],
$bkg_2_details['inter'],
$bkg_2_details['ed'],
$bkg_2_details['wait'],
$bkg_2_details['tolls'],
$bkg_2_details['bs'],
$bkg_2_details['park'],
$bkg_2_details['ah'],
$bkg_2_details['me'],
$bkg_2_details['alc'],
$bkg_2_details['fc'],
$bkg_2_details['oth'],
$bkg_2_details['tot_fare'],
$bkg_2_details['drv_fee'],
$bkg_2_details['oth_exp'],
$bkg_2_details['profit']
);

$return_message .= "<b>Booking has been Cloned</b><br/><br/>";
$return_message .= "Following bookings has been generated<br/><br/>";
$return_message .= "<b>BOOKING ID -  ".$ret_val['bkg_id_1']."</b><br/>";
if($ret_val['bkg_id_2'] != '')
	{
		$return_message .= "<b>BOOKING ID -  ".$ret_val['bkg_id_2']."</b><br/>";
	}
$a = array('data' => ''.$return_message.'');
$json = json_encode($a); 
echo $json;
?>