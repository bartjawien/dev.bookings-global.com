<?php
ini_set("display_errors", 0);
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "job.php");
require_once(INCLUDE_PATH . "functions.php");
session_start();

$database = new Database;
try
	{
	
/*-------------------LISTING-----------------------------------------------------------------------------------------------------*/
		if($_GET["action"] == "list_all_bookings")
			{
				$where .= "WHERE 1 ";
				if($_GET["dt_from"] != ''  && $_GET["dt_to"] != '')
					{
						$where .= " AND job.job_date between '".$_GET["dt_from"]."' AND '".$_GET["dt_to"]."' ";
					}
				else
					{
						$where .= " AND job.job_date between '2014-01-01' AND '2100-04-01' "; // I've changed this to 2100 to show bookings beyond 2017 
						//This has been changed to avoid accidental submits
						//$today = date('Y-m-d');
						//$where .= " AND job.job_date between '".$today."' AND '".$today."' ";
					}
				if($_GET['search_field'] != '' && $_GET['search_text'] != '')
					{ 
						if($_GET['search_field'] == 'id' || $_GET['search_field'] == 'frm_flight_no'  || $_GET['search_field'] == 'frm_sub'  || $_GET['search_field'] == 'to_sub')
							{
								$where .= " AND job.".$_GET['search_field']." = '".$_GET['search_text']."' ";
							}
						else if($_GET['search_field'] == 'bkg_state' )
							{
								$where .= " AND job.frm_state LIKE '%".$_GET['search_text']."%' ";
							}
						else if($_GET['search_field'] == 'charge_acc_id' )
							{
								$where .= " AND job__reference.charge_acc_id IN  ( SELECT id FROM charge_acc WHERE account_name LIKE '%".$_GET['search_text']."%' ) ";
							}
						else if($_GET['search_field'] == 'bkg_by_id' || $_GET['search_field'] == 'pax_id' )
							{
								$where .= " AND job__reference.".$_GET['search_field']." IN  ( SELECT id FROM user WHERE fname LIKE '%".$_GET['search_text']."%' OR lname LIKE '%".$_GET['search_text']."%' ) ";
							}
						else if($_GET['search_field'] == 'std_id' || $_GET['search_field'] == 'std_fname'  || $_GET['search_field'] == 'std_lname')
							{
								$where .= " AND job__reference.".$_GET['search_field']." = '".$_GET['search_text']."'  ";
							}
						else if($_GET['search_field'] == 'driver' )
							{
								$where .= " AND job.driver_id IN  ( SELECT id FROM user WHERE fname LIKE '%".$_GET['search_text']."%' OR lname LIKE '%".$_GET['search_text']."%' AND role_id ='2') ";
							}
						else if($_GET['search_field'] == 'phone' )
							{
								$where .= " AND ( b.phone LIKE '%".$_GET['search_text']."%' OR d.mobile LIKE '%".$_GET['search_text']."%' OR c.mobile LIKE '%".$_GET['search_text']."%') ";
							}
					}
				if($_GET['search_job_status'] !='')
					{
						$where .= " AND job.job_status = '".$_GET['search_job_status']."'  ";
						if($_GET["search_by_charge_account_id"] != '' )
						{
							$where .= " AND job__reference.charge_acc_id = '".$_GET["search_by_charge_account_id"]."'";
						}
					}
				if($_GET["under_18"] != '' )
					{
						$where .= " AND job__reference.under_18 = '".$_GET["under_18"]."'";
					}
				if($_SESSION['ROLE_ID'] == '3')
					{
						$query1 = "SELECT * from charge_acc__contacts where user_id = '".$_SESSION['USER_ID']."'";
						$result1 = $database->query($query1);
						$row1 = mysql_fetch_array($result1);
						$charge_acc_id = $row1['charge_acc_id'];
						$where .= " AND job__reference.charge_acc_id = '".$charge_acc_id."' ";
					}
				if($_SESSION['ROLE_ID'] == '2')
					{
						$where .= " AND job.driver_id = '".$_SESSION['USER_ID']."' AND job.driver_status in ('3','4')  ";
					}
				//Get record count
				//$query = "SELECT COUNT(*) AS RecordCount FROM job";
				//$result = $database->query($query);
				//$row = mysql_fetch_array($result);
				//$recordCount = $row['RecordCount'];
				
				$query = "
				SELECT
				*,
				job.id as job_id,
				job__reference.entered_by as entered_by,
				CONCAT(a.fname) as entry_by,
				CONCAT(b.fname,'<br/>',b.phone) as bkg_by,";
				
				if($_SESSION['ROLE_ID']== '2' )
					{
						$query .= "
						CONCAT(c.title, ' ', c.fname, ' ', c.lname) as pax,"; 
					}
				else
					{
						$query .= "
						CONCAT(c.title, ' ', c.fname, ' ', c.lname,'<br/>', c.mobile) as pax,";
					}
				
				$query .="
				CONCAT(d.fname,'<br/>',d.mobile) as driver,
				CONCAT(job__reference.std_title, ' ', job__reference.std_fname, ' ', job__reference.std_lname) as student_name,
				CONCAT(job.frm_line1,' ',job.frm_line2,'<br/><b>',job.frm_sub,'</b>') as from_address,
				CONCAT(job.frm_via_line1,' ',job.frm_via_line2,'<br/><b>',job.frm_via_sub,'</b>') as from_via_address,
				CONCAT(job.to_flight_no,' ',job.to_line1,' ',job.to_line2,'<br/><b>',job.to_sub,'</b>') as to_address,
				CONCAT(job.to_via_line1,' ',job.to_via_line2,'<br/><b>',job.to_via_sub,'</b>') as to_via_address,
				CONCAT('<b>',TIME_FORMAT(job.job_time,'%H:%i'),'</b><br/>',DATE_FORMAT(job.job_date,'%d/%m/%Y')) as date_time,
				variable__car_type.details as vehicle_type,
				job__status_ids.details as current_booking_status,
				job__driver_status_ids.details as driver_status,
				job__reference.std_title as std_title,
				job__reference.std_fname as std_fname,
				job__reference.std_lname as std_lname,
				job__reference.under_18 as under_18,
				job__reference.acc_type as acc_type,
				job.driver_notes as driver_notes, 
				job__driver.accepted_amount as driver_price,
				job__driver_extra_pay.amount as extra_pay,
				(if(job__driver_extra_pay.amount!= null || job__driver_extra_pay.amount!= '' ,job__driver_extra_pay.amount, 0) + job__driver.accepted_amount) as driver_fee
				from 
				job
				LEFT JOIN job__status_ids ON job.job_status = job__status_ids.id
				LEFT JOIN variable__car_type ON job.car_id = variable__car_type.id
				LEFT JOIN job__driver_status_ids ON job.driver_status = job__driver_status_ids.id
				LEFT JOIN user AS d ON job.driver_id = d.id
				LEFT JOIN job__driver_extra_pay ON job.id = job__driver_extra_pay.job_id 
				LEFT JOIN job__reference ON job.job_reference_id = job__reference.id
				LEFT JOIN user AS a ON job__reference.entered_by = a.id
				LEFT JOIN user AS b ON job__reference.bkg_by_id = b.id
				LEFT JOIN user AS c ON job__reference.pax_id = c.id
				LEFT JOIN charge_acc ON job__reference.charge_acc_id = charge_acc.id
				LEFT JOIN job__driver ON job.id = job__driver.job_id AND job__driver.id=(SELECT MAX(id) FROM job__driver WHERE job_id = job.id)  
				".$where."
				ORDER BY ".$_GET["jtSorting"]."  LIMIT ".$_GET["jtStartIndex"].", ".$_GET["jtPageSize"]."";
				
				
				$query1 = "
				SELECT
				*,
				job.id as job_id,
				job__reference.entered_by as entered_by,
				CONCAT(a.fname) as entry_by,
				CONCAT(b.fname,'(',b.phone,')') as bkg_by,";

				if($_SESSION['ROLE_ID']== '2' )
					{
						$query1 .= "
						CONCAT(c.title, ' ', c.fname, ' ', c.lname) as pax,"; 
					}
				else
					{
						$query1 .= "
						CONCAT(c.title, ' ', c.fname, ' ', c.lname,'(', c.mobile,')') as pax,";
					}

				$query1 .="
				CONCAT(d.fname,'(',d.mobile,')') as driver,
				CONCAT(job__reference.std_title, ' ', job__reference.std_fname, ' ', job__reference.std_lname) as student_name,
				CONCAT(job.frm_line1,' ',job.frm_line2,' ',job.frm_sub) as from_address,
				CONCAT(job.frm_via_line1,' ',job.frm_via_line2,' ',job.frm_via_sub) as from_via_address,
				CONCAT(job.to_flight_no,' ',job.to_line1,' ',job.to_line2,' ',job.to_sub) as to_address,
				CONCAT(job.to_via_line1,' ',job.to_via_line2,' ',job.to_via_sub) as to_via_address,
				CONCAT('@',TIME_FORMAT(job.job_time,'%H:%i'),' on ',DATE_FORMAT(job.job_date,'%d/%m/%Y')) as date_time,
				variable__car_type.details as vehicle_type,
				job__status_ids.details as current_booking_status,
				job__driver_status_ids.details as driver_status,
				job__reference.std_title as std_title,
				job__reference.std_fname as std_fname,
				job__reference.std_lname as std_lname,
				job__reference.under_18 as under_18,
				job__reference.acc_type as acc_type,
				job.driver_notes as driver_notes, 
				job__driver.accepted_amount as driver_price,
				job__driver_extra_pay.amount as extra_pay,
				(if(job__driver_extra_pay.amount!= null || job__driver_extra_pay.amount!= '' ,job__driver_extra_pay.amount, 0) + job__driver.accepted_amount) as driver_fee
				from 
				job
				LEFT JOIN job__status_ids ON job.job_status = job__status_ids.id
				LEFT JOIN variable__car_type ON job.car_id = variable__car_type.id
				LEFT JOIN job__driver_status_ids ON job.driver_status = job__driver_status_ids.id
				LEFT JOIN user AS d ON job.driver_id = d.id
				LEFT JOIN job__driver_extra_pay ON job.id = job__driver_extra_pay.job_id 
				LEFT JOIN job__reference ON job.job_reference_id = job__reference.id
				LEFT JOIN user AS a ON job__reference.entered_by = a.id
				LEFT JOIN user AS b ON job__reference.bkg_by_id = b.id
				LEFT JOIN user AS c ON job__reference.pax_id = c.id
				LEFT JOIN charge_acc ON job__reference.charge_acc_id = charge_acc.id
				LEFT JOIN job__driver ON job.id = job__driver.job_id AND job__driver.id=(SELECT MAX(id) FROM job__driver WHERE job_id = job.id)  
				".$where."
				ORDER BY ".$_GET["jtSorting"]."  LIMIT ".$_GET["jtStartIndex"].", ".$_GET["jtPageSize"]."";
				
				//The following is to store the search values in a session variable that can be used to download the search values as excel or pdf.
				session_start();
				//first unset the values if found
				if(isset($_SESSION['TODAYS_BOOKINGS']))
					{
						unset($_SESSION['TODAYS_BOOKINGS']);
						$_SESSION['TODAYS_BOOKINGS'] = array();
					}
				$_SESSION['TODAYS_BOOKINGS'] = $query1;
				

				
				$result = $database->query($query);
				$total_rows= mysql_num_rows($result);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
					
				
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $total_rows;
				$jTableResult['Records'] = $rows;
				$jTableResult['query'] = $query;
				$jTableResult['query1'] = $query1;
				print json_encode($jTableResult);
			}
		/*-------------------EXPORT-----------------------------------------------------------------------------------------------------*/
			
		else if($_GET["action"] == "export_excel")
			{
				require_once '../Classes/PHPExcel.php';
				
				$database = new database;
				$objPHPExcel = new PHPExcel();
				
				// Create a first sheet
				$objPHPExcel->getProperties()->setCreator("".BUSINESS_NAME."")
							 ->setLastModifiedBy("".$_SESSION['TITLE']." ".$_SESSION['FNAME']." ".$_SESSION['LNAME']."")
							 ->setTitle("BOOKINGS ".BUSINESS_NAME."")
							 ->setSubject("BOOKINGS ".BUSINESS_NAME."")
							 ->setDescription("BOOKINGS ".BUSINESS_NAME."")
							 ->setKeywords("BOOKINGS")
							 ->setCategory("BOOKINGS");
							 
				$objPHPExcel->setActiveSheetIndex(0);
				$objPHPExcel->getActiveSheet()->setCellValue('A1', "JOB ID");
				$objPHPExcel->getActiveSheet()->setCellValue('B1', "Bkg TIME and DATE");
				$objPHPExcel->getActiveSheet()->setCellValue('C1', "Different Driver");
				$objPHPExcel->getActiveSheet()->setCellValue('D1', "Driver");
				$objPHPExcel->getActiveSheet()->setCellValue('E1', "STD NAME");
				$objPHPExcel->getActiveSheet()->setCellValue('F1', "STD ID");
				$objPHPExcel->getActiveSheet()->setCellValue('G1', "FLIGHT");
				$objPHPExcel->getActiveSheet()->setCellValue('H1', "TOC");
				$objPHPExcel->getActiveSheet()->setCellValue('I1', "DESTINATION");
				$objPHPExcel->getActiveSheet()->setCellValue('J1', "Pax#");
				//$objPHPExcel->getActiveSheet()->setCellValue('P1', "Bkg By");
				//$objPHPExcel->getActiveSheet()->setCellValue('Q1', "Acc Name");
				$objPHPExcel->getActiveSheet()->setCellValue('K1', "LUG");
				$objPHPExcel->getActiveSheet()->setCellValue('L1', "Actual Lug");
				//$objPHPExcel->getActiveSheet()->setCellValue('V1', "BC");
				//$objPHPExcel->getActiveSheet()->setCellValue('W1', "BS");
				//$objPHPExcel->getActiveSheet()->setCellValue('X1', "B");
				//$objPHPExcel->getActiveSheet()->setCellValue('Y1', "Method");
				//$objPHPExcel->getActiveSheet()->setCellValue('Z1', "Entered By");
				$objPHPExcel->getActiveSheet()->setCellValue('M1', "NOTES");
				//$objPHPExcel->getActiveSheet()->setCellValue('B1', "Bkg Status");
				//$objPHPExcel->getActiveSheet()->setCellValue('C1', "Driver Status");
				//$objPHPExcel->getActiveSheet()->setCellValue('E1', "State");
				//$objPHPExcel->getActiveSheet()->setCellValue('G1', "Vehicle");
				//$objPHPExcel->getActiveSheet()->setCellValue('D1', "Pax Name");
				//$objPHPExcel->getActiveSheet()->setCellValue('J1', "FROM");
				//$objPHPExcel->getActiveSheet()->setCellValue('K1', "VIA");
				//$objPHPExcel->getActiveSheet()->setCellValue('M1', "LAST DROP");
				//$objPHPExcel->getActiveSheet()->setCellValue('N1', "Ext Note");
				//$objPHPExcel->getActiveSheet()->setCellValue('O1', "Int Note");
				
				
				$styleArray = array( 'font' => array( 'bold' => true, ), 'alignment' => array( 'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT, ), 'borders' => array( 'top' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN, ), ), 'fill' => array( 'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR, 'rotation' => 90, 'startcolor' => array( 'argb' => 'FFA0A0A0', ), 'endcolor' => array( 'argb' => 'FFFFFFFF', ), ), );
				$objPHPExcel->getActiveSheet()->getStyle('A1:AA1')->applyFromArray($styleArray);
				// Hide "ID" column
				//$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setVisible(true);
				
				
				$i = 2;
				session_start();
				$query = $_SESSION['TODAYS_BOOKINGS'];
				$result = $database->query($query);
				$total_rows= mysql_num_rows($result);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$objPHPExcel->setActiveSheetIndex(0)
									->setCellValue('A'.$i, $row['job_id'])
									->setCellValue('B'.$i, $row['date_time'])
									->setCellValue('C'.$i, $row['Change_driver'])
									->setCellValue('D'.$i, $row['driver'])
									->setCellValue('E'.$i, $row['student_name'])
									->setCellValue('F'.$i, $row['std_id'])
									->setCellValue('G'.$i, $row['frm_flight_no'])
									->setCellValue('H'.$i, $row['TOC'])
									->setCellValue('I'.$i, $row['to_address'])
									->setCellValue('J'.$i, $row['pax_nos'])
									//->setCellValue('P'.$i, $row['bkg_by'])
									//->setCellValue('Q'.$i, $row['account_name'])
									->setCellValue('K'.$i, $row['luggage'])
									->setCellValue('L'.$i, $row['actual_luggage'])
									//->setCellValue('V'.$i, $row['baby_capsule'])
									//->setCellValue('W'.$i, $row['baby_seat'])
									//->setCellValue('X'.$i, $row['booster_seat'])
									//->setCellValue('Y'.$i, $row['job_src'])
									//->setCellValue('Z'.$i, $row['entry_by'])
									->setCellValue('M'.$i, $row['driver_notes'])
									//->setCellValue('B'.$i, $row['current_booking_status'])
									//->setCellValue('C'.$i, $row['driver_status'])
									//->setCellValue('E'.$i, $row['frm_state'])
									//->setCellValue('G'.$i, $row['vehicle_type'])
									//->setCellValue('D'.$i, $row['pax'])
									//->setCellValue('J'.$i, $row['from_address'])
									//->setCellValue('K'.$i, $row['from_via_address'])
									//->setCellValue('M'.$i, $row['to_via_address'])
									//->setCellValue('N'.$i, $row['ext_notes'])
									//->setCellValue('O'.$i, $row['int_notes'])
									
									;
						$i++;
					}
				$todays_date = Date("d-m-Y-His");
				header('Content-Type: application/vnd.ms-excel');
				$bookings_report = "BOOKINGS_REPORT_".$todays_date.".xls";
				header('Content-Disposition: attachment;filename='.$bookings_report.'');
				header('Cache-Control: max-age=0');
				$objPHPExcel->getActiveSheet()->setTitle('Bkg_Report');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$objWriter->save('php://output');
			}
			else if($_GET["action"] == "export_html")
			{
				
				$job = new Job;
				
				$query = $_SESSION['TODAYS_BOOKINGS'];
				$result = $database->query($query);
				$total_rows= mysql_num_rows($result);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				$htmlstr = $job->getHTMLBookingList($rows);
				$todays_date = Date("d-m-Y-His");
				$file_name = "../html_report/BOOKINGS_REPORT_".$todays_date.".html";
				
				$myfile = fopen($file_name, "w") or die("Unable to open file!");
				fwrite($myfile, $htmlstr);
				fclose($myfile);
				
				header("Cache-Control: public");
				header("Content-Description: File Transfer");
				header("Content-Length: ". filesize("$file_name").";");
				header("Content-Disposition: attachment; filename=$file_name");
				header("Content-Type: application/octet-stream; "); 
				header("Content-Transfer-Encoding: binary");
				readfile($file_name);
			}
			
			else if($_GET["action"] == "export_pdf")
			{
				require_once('../html2pdf/html2pdf.class.php');
				$job = new Job;
				
				$query = $_SESSION['TODAYS_BOOKINGS'];
				$result = $database->query($query);
				$total_rows= mysql_num_rows($result);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				$htmlstr = $job->getHTMLBookingList($rows);
				$todays_date = Date("d-m-Y-His");
				$file_name = "../html_report/BOOKINGS_REPORT_".$todays_date.".pdf";
				
					
				$html2pdf = new HTML2PDF('P', 'A2', 'en');
				$html2pdf->WriteHTML($htmlstr);
				
				$pdfdoc = $html2pdf->Output($file_name , 'F');
				
				header("Cache-Control: public");
				header("Content-Description: File Transfer");
				header("Content-Length: ". filesize("$file_name").";");
				header("Content-Disposition: attachment; filename=$file_name");
				header("Content-Type: application/pdf "); 
				 
				readfile($file_name);
				
			}

	}
catch(Exception $ex)
	{
		//Return error message
		$jTableResult = array();
		$jTableResult['Result'] = "ERROR";
		$jTableResult['Message'] = $ex->getMessage();
		print json_encode($jTableResult);
	}
?>