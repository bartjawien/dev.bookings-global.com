<script src="../Scripts/reports_job_driver.js"></script>

<form name="search_rows" id="search_rows" method="post" action="../Ajax/report_jobs_allotted_to_drivers.php">
	<div style="width:100%">
		
			<div style="float:left; padding:10px; border:1px solid black; width:50%;">
				<h1> Must Contain</h1>
				<div class="mst_contain">
					<label> Driver &nbsp;&nbsp; </label>
					<input type="text" name="search_by_drivers" id="search_by_drivers" value="" placeholder="Type driver" />
					<input type="hidden" name="search_by_drivers_id" id="search_by_drivers_id" value=""  />
				</div>
				<div class="mst_contain">
					<label> Passenger &nbsp;&nbsp; </label>
					<input type="text" name="search_by_pax" id="search_by_pax" value="" placeholder="Type passenger" />
					<input type="hidden" name="search_by_pax_id" id="search_by_pax_id" value=""  />
				</div>
				<div class="mst_contain">
					<label> Car &nbsp;&nbsp; </label>
					<input type="text" name="search_by_car" id="search_by_car" value="" placeholder="Type car" />
					<input type="hidden" name="search_by_car_id" id="search_by_car_id" value=""  />
				</div> 
				<div class="mst_contain">
					<label> Charge To &nbsp;&nbsp; </label>
					<input type="text" name="search_by_charge_account" id="search_by_charge_account" value="" placeholder="Type charge account" />
					<input type="hidden" name="search_by_charge_account_id" id="search_by_charge_account_id" value=""  />
				</div>
				<div class="mst_contain">
					<label> Booking Source &nbsp;&nbsp; </label>
					<select name="search_by_job_src" id="search_by_job_src" >
						<option value=""> Select Job Source</option>
						<option value="Email"> Email</option>
						<option value="Website"> Website</option>
						<option value="Phone"> Phone</option>
						<option value="Others"> Others</option>
					</select>
				</div>
				<div class="mst_contain">
					<label> Driver status &nbsp;&nbsp; </label>
					<select name="search_by_driver_status" id="search_by_driver_status" >
						<option value=""> Select Driver Status</option>
						<option value="1">Allocate</option>
						<option value="2">Send</option>
						<option value="3">Accepted</option>
						<option value="4">Paid</option>
					</select>
				</div>
			</div>
		
			<div style="float:left; padding:10px; border:1px solid black; margin:10px; width:40%;">
				<h1> Switches</h1>
				<div class="swt_head">
					<label>
						Job Statuses
					</label>
					<span>
						Must Be
					</span>
					<span>
						Must Not Be
					</span>
					<span>
						Don't Care
					</span>
				</div>
				
				<div class="swt_row">
					<label>
						Waiting
					</label>
					<span>
						<input type="radio" name="swt5" id="swt5" value="1" />
					</span>
					<span>
						<input type="radio" name="swt5" id="swt5" value="-1" />
					</span>
					<span>
						<input type="radio" name="swt5" id="swt5" value="0" checked />
					</span>
				</div>

				<div class="swt_row">
					<label>
						Received
					</label>
					<span>
						<input type="radio" name="swt10" id="swt10" value="1" />
					</span>
					<span>
						<input type="radio" name="swt10" id="swt10" value="-1" />
					</span>
					<span>
						<input type="radio" name="swt10" id="swt10" value="0" checked />
					</span>
				</div>
				
				<div class="swt_row">
					<label>
						Confirmed
					</label>
					<span>
						<input type="radio" name="swt20" id="swt20" value="1" />
					</span>
					<span>
						<input type="radio" name="swt20" id="swt20" value="-1" />
					</span>
					<span>
						<input type="radio" name="swt20" id="swt20" value="0" checked />
					</span>
				</div>
				
				<div class="swt_row">
					<label>
						Declined
					</label>
					<span>
						<input type="radio" name="swt90" id="swt90" value="1" />
					</span>
					<span>
						<input type="radio" name="swt90" id="swt90" value="-1" />
					</span>
					<span>
						<input type="radio" name="swt90" id="swt90" value="0" checked />
					</span>
				</div>
				
				<div class="swt_row">
					<label>
						Cancelled
					</label>
					<span>
						<input type="radio" name="swt100" id="swt100" value="1" />
					</span>
					<span>
						<input type="radio" name="swt100" id="swt100" value="-1" />
					</span>
					<span>
						<input type="radio" name="swt100" id="swt100" value="0" checked />
					</span>
				</div>
			</div>
		
	</div> 
	<div style="padding:10px; width:100%; float:right;">
		<input type="hidden" name="tab_name" id="tab_name" value="advanced" />
		<input type="hidden" name="advanced_pressed_button" id="advanced_pressed_button" value="" />
		<input type="submit" class="sml_btn" name="advanced_search_specific" id="advanced_search_specific" value="Search">
		<input type="submit" class="big_btn"  name="download_html" id="download_html" value="Download as Html" /> &nbsp;&nbsp;
		<input type="submit" class="big_btn"  name="download_pdf" id="download_pdf" value="Download as Pdf" />
		<input type="submit" class="big_btn"  name="download_excel" id="download_excel" value="Download as Excel" />
	</div>
</form> 

	<div style="clear:both"></div>

