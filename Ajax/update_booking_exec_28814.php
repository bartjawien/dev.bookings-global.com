<?php
ini_set("display_errors", 0);
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "chargeAccount.php");
require_once(CLASSES_PATH . "job.php");
require_once(CLASSES_PATH . "jobReference.php");
require_once(CLASSES_PATH . "address.php");
require_once(CLASSES_PATH . "mailer.php");
require_once(CLASSES_PATH . "vehicle.php");
require_once(CLASSES_PATH . "transaction.php");
require_once(CLASSES_PATH . "account.php");
require_once(CLASSES_PATH . "chargeAccount.php");
require_once(INCLUDE_PATH . "functions_date_time.php");
session_start();
$database = new database;

$charge_acc = new chargeAccount();
$user = new user();
$address = new address();
$job = new job();
$job_reference = new JobReference();
$mailer = new mailer();

if(isset($_GET['form_submit']))
	{
		$j1_details = $job->getJobDetails($_POST['j1_id']);
		$job_ref_details = $job_reference->getJobReferenceDetails($j1_details['job_reference_id']);
		
		$return_message .="<b>Following log generated for this Booking Reference</b><br/>";
		
		if($job_ref_details['job_src'] != $_POST['job_src'] && $_POST['job_src'] !='')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'job_src', $_POST['job_src']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $_SESSION['USER_ID'], 'Booking Source Edited', $_POST['job_src'], $job_ref_details['job_src']);
				$return_message .="Booking Source Edited from - ".$job_ref_details['job_src']." to  ".$_POST['job_src']."<br/>";
			}
		if($job_ref_details['conf_to_client'] == '1')
			{
				if($job_ref_details['bkg_by_id'] == $_POST['acc_contact']) // means booking by id has not been changed on edit
					{
						$user_details = $user->getUserDetails($_POST['acc_contact']);
						$mailer->sendMailToClientOnBooking($job_ref_details['id'], '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
						$job_reference->addJobReferenceLog($job_ref_details['id'], $_SESSION['USER_ID'], 'Email Confirmation Sent to Booking Made By', '', '');
					}
				else
					{
						// do nothing here and send the confirmation if required in the step where a new bkg by id is getting added
					}
			}
		if($job_ref_details['conf_to_pax'] == '1')
			{
				if($job_ref_details['pax_id'] == $_POST['acc_passenger']) // means pax has not been changed on edit
					{
						$user_details = $user->getUserDetails($_POST['acc_passenger']);
						$mailer->sendMailToClientOnBooking($job_ref_details['id'], '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
						$job_reference->addJobReferenceLog($job_ref_details['id'], $_SESSION['USER_ID'], 'Email Confirmation Sent to Passenger', '', '');
					}
				else
					{
						// do nothing here and send the confirmation if required in the step where a pax is getting added
					}
			}
		
		if($job_ref_details['order_ref'] != $_POST['order_ref'])
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'order_ref', $_POST['order_ref']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $_SESSION['USER_ID'], 'Order Reference Edited', $_POST['order_ref'], $job_ref_details['order_ref']);
				$return_message .="Order Reference Edited from - ".$job_ref_details['order_ref']." to  ".$_POST['order_ref']."<br/>";
			}
		
		if($job_ref_details['acc_type'] != $_POST['acc_type'])
			{
				$query1 = "SELECT * from variable__charge_acc_type where id = ".$job_ref_details['acc_type']."";
				$result1 = $database->query($query1);
				$row1 = mysql_fetch_array($result1);
				$old_details = $row1['details'];
				
				$query2 = "SELECT * from variable__charge_acc_type where id = ".$_POST['acc_type']."";
				$result2 = $database->query($query2);
				$row2 = mysql_fetch_array($result2);
				$new_details = $row2['details'];
				
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'acc_type', $_POST['acc_type']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $_SESSION['USER_ID'], 'Account Type Edited', $new_details, $old_details);
				$return_message .="Account Type Edited from - ".$old_details." to  ".$new_details."<br/>";
			}
			
		if($job_ref_details['std_id'] != $_POST['std_id'])
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'std_id', $_POST['std_id']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $_SESSION['USER_ID'], 'Student ID Edited', $_POST['std_id'], $job_ref_details['std_id']);
				$return_message .="Student ID Edited from - ".$job_ref_details['std_id']." to  ".$_POST['std_id']."<br/>";
			}
		if($job_ref_details['std_title'] != $_POST['std_title'])
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'std_title', $_POST['std_title']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $_SESSION['USER_ID'], 'Student Title Edited', $_POST['std_title'], $job_ref_details['std_title']);
				$return_message .="Student Title Edited from - ".$job_ref_details['std_title']." to  ".$_POST['std_title']."<br/>";
			}
		if($job_ref_details['std_fname'] != $_POST['std_fname'])
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'std_fname', $_POST['std_fname']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $_SESSION['USER_ID'], 'Student First Name Edited', $_POST['std_fname'], $job_ref_details['std_fname']);
				$return_message .="Student First Name Edited from - ".$job_ref_details['std_fname']." to  ".$_POST['std_fname']."<br/>";
			}
		if($job_ref_details['std_lname'] != $_POST['std_lname'])
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'std_lname', $_POST['std_lname']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $_SESSION['USER_ID'], 'Student Last Name Edited', $_POST['std_lname'], $job_ref_details['std_lname']);
				$return_message .="Student Last Name Edited from - ".$job_ref_details['std_lname']." to  ".$_POST['std_lname']."<br/>";
			}

		if(($job_ref_details['charge_acc_id'] != $_POST['charge_acc']) && $_POST['charge_acc'] != '') // this means another charge account has been selected from drop down list
			{
				$old_charge_acc = $charge_acc->getChargeAccountDetails($job_ref_details['charge_acc_id']);
				$new_charge_acc = $charge_acc->getChargeAccountDetails($_POST['charge_acc']);
				
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'charge_acc_id', $_POST['charge_acc']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $_SESSION['USER_ID'], 'Charge Account Edited', $new_charge_acc['account_name'], $old_charge_acc['account_name']);
				$return_message .="Charge Account Edited from - ".$old_charge_acc['account_name']." to  ".$new_charge_acc['account_name']."<br/>";
			}
		if($_POST['charge_acc'] == '' && $_POST['account_name'] != '') // A new new charge account has been entered
			{
				
				$charge_account = new ChargeAccount();
				$old_charge_account_details = $charge_account->getChargeAccountDetails($job_ref_details['charge_acc_id']);
				$new_charge_acc_id = $charge_account->addChargeAccount($_SESSION['USER_ID'], $_POST['account_type'], $_POST['account_name'], $_POST['myob_no']);
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'charge_acc_id', $new_charge_acc_id);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $_SESSION['USER_ID'], 'New Charge Account Created', $_POST['account_name'], $old_charge_account_details['account_name']);
			}
		
		if(($job_ref_details['bkg_by_id'] != $_POST['acc_contact']) && $_POST['acc_contact'] != '') // this means another booking by has been selected from drop down list
			{
				$old_bkg_by_details = 	$user->getUserDetails($job_ref_details['bkg_by_id']);
				$old_name 			= 	"".$old_bkg_by_details['title']." ".$old_bkg_by_details['fname']." ".$old_bkg_by_details['lname']."";
				
				$new_bkg_by_details = 	$user->getUserDetails($_POST['acc_contact']);
				$new_name 			= 	"".$new_bkg_by_details['title']." ".$new_bkg_by_details['fname']." ".$new_bkg_by_details['lname']."";
				
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'bkg_by_id', $_POST['acc_contact']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $_SESSION['USER_ID'], 'Booking by Edited', $new_name, $old_name);
				//send email if conf_to_client has been ticked
				if($_POST['conf_to_client'] == '1')
					{
						$mailer->sendMailToClientOnBooking($job_ref_details['id'], '1', $new_bkg_by_details['email'], $new_bkg_by_details['title'], $new_bkg_by_details['fname'], $new_bkg_by_details['lname']);
						$job_reference->addJobReferenceLog($job_ref_details['id'], $_SESSION['USER_ID'], 'Confirmation email sent to the client', '', '');
					}
				$return_message .="Booking by Edited from - ".$old_name." to  ".$new_name."<br/>";
			}
		if($_POST['acc_contact'] == '' && $_POST['bkg_by_fname'] != '') // A new booking made by entered
			{
				$old_bkg_by_details = 	$user->getUserDetails($job_ref_details['bkg_by_id']);
				$old_name 			= 	"".$old_bkg_by_details['title']." ".$old_bkg_by_details['fname']." ".$old_bkg_by_details['lname']."";
				$new_name 			= 	"".$_POST['bkg_by_title']." ".$_POST['bkg_by_fname']." ".$_POST['bkg_by_lname']."";
				//create this new booking made by
				$new_bkg_made_by_id = $user->addUser($_POST['bkg_by_type'], $_POST['bkg_by_title'], $_POST['bkg_by_fname'], $_POST['bkg_by_lname'], $_POST['bkg_by_password'], $_POST['bkg_by_email'], $_POST['bkg_by_mobile'], $_POST['bkg_by_phone'], $_POST['bkg_by_preference']);
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'bkg_by_id', $new_bkg_made_by_id);
				//add this to booking reference log
				$job_reference->addJobReferenceLog($job_ref_details['id'], $_SESSION['USER_ID'], 'New Booking Made By Created', $new_name, $old_name);
				//send mail to this booking made by if conf_to_client is ticked
				if($_POST['conf_to_client'] == '1')
					{
						$mailer->sendMailToClientOnBooking($job_ref_details['id'], '1', $_POST['bkg_by_email'], $_POST['bkg_by_title'], $_POST['bkg_by_fname'], $_POST['bkg_by_lname']);
						$job_reference->addJobReferenceLog($job_ref_details['id'], $_SESSION['USER_ID'], 'Confirmation email sent to new client', '', '');
					}
				$return_message .="Booking by Added from - ".$old_name." to  ".$new_name."<br/>";
			}
		if(($job_ref_details['pax_id'] != $_POST['acc_passenger']) && $_POST['acc_passenger'] != '') // this means another pax has been selected from drop down list
			{
				$old_pax_details 	= 	$user->getUserDetails($job_ref_details['pax_id']);
				$old_name 			= 	"".$old_pax_details['title']." ".$old_pax_details['fname']." ".$old_pax_details['lname']."";
				
				$new_pax_details 	= 	$user->getUserDetails($_POST['acc_passenger']);
				$new_name 			= 	"".$new_pax_details['title']." ".$new_pax_details['fname']." ".$new_pax_details['lname']."";
				
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'pax_id', $_POST['acc_passenger']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $_SESSION['USER_ID'], 'Pax Edited', $new_name, $old_name);
				//send email if conf_to_pax has been ticked
				if($_POST['conf_to_pax'] == '1')
					{
						$mailer->sendMailToClientOnBooking($job_ref_details['id'], '1', $new_pax_details['email'], $new_pax_details['title'], $new_pax_details['fname'], $new_pax_details['lname']);
						$job_reference->addJobReferenceLog($job_ref_details['id'], $_SESSION['USER_ID'], 'Confirmation email sent to the passenger', '', '');
					}
				$return_message .="Pax Edited from - ".$old_name." to  ".$new_name."<br/>";
			}
		if($_POST['acc_passenger'] == '' && $_POST['new_pax_fname'] != '') // A new pax entered
			{
				$old_pax_details 	= 	$user->getUserDetails($job_ref_details['pax_id']);
				$old_name 			= 	"".$old_pax_details['title']." ".$old_pax_details['fname']." ".$old_pax_details['lname']."";
				$new_name 			= 	"".$_POST['new_pax_title']." ".$_POST['new_pax_fname']." ".$_POST['new_pax_lname']."";
				//create this new booking made by
				$new_pax_id = $user->addUser($_POST['new_pax_type'], $_POST['new_pax_title'], $_POST['new_pax_fname'], $_POST['new_pax_lname'], $_POST['new_pax_password'], $_POST['new_pax_email'], $_POST['new_pax_mobile'], $_POST['new_pax_phone'], $_POST['new_pax_preference']);
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'pax_id', $new_pax_id);
				//add this to booking reference log
				$job_reference->addJobReferenceLog($job_ref_details['id'], $_SESSION['USER_ID'], 'New Passenger Created', $new_name, $old_name);
				//send mail to this booking made by if conf_to_pax is ticked
				if($_POST['conf_to_pax'] == '1')
					{
						$mailer->sendMailToClientOnBooking($job_ref_details['id'], '1', $_POST['new_pax_email'], $_POST['new_pax_title'], $_POST['new_pax_fname'], $_POST['new_pax_lname']);
						$job_reference->addJobReferenceLog($job_ref_details['id'], $_SESSION['USER_ID'], 'Confirmation email sent to the new passenger', '', '');
					}
				$return_message .="Passenger Added from - ".$old_name." to  ".$new_name."<br/>";
			}
		if($job_ref_details['charge_mode'] != $_POST['charge_mode'] && $_POST['charge_mode'] != '')
			{
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'charge_mode', $_POST['charge_mode']);
				$return_message .="Charge Mode Edited from - ".$old_details." to  ".$new_details."<br/>";
			}
		/*
		if(($job_ref_details['job_type'] == '1') &&  $_POST['job_type'] == '2') //means the booking has been changed to a return booking
			{
				$query1 = "SELECT * from variable__charge_mode where id = ".$job_ref_details['charge_mode']."";
				$result1 = $database->query($query1);
				$row1 = mysql_fetch_array($result1);
				$old_details = $row1['details'];
				
				$query2 = "SELECT * from variable__charge_mode where id = ".$_POST['charge_mode']."";
				$result2 = $database->query($query2);
				$row2 = mysql_fetch_array($result2);
				$new_details = $row2['details'];
				
				$job_reference->updateJobReferenceTable($job_ref_details['id'], 'charge_mode', $_POST['charge_mode']);
				$job_reference->addJobReferenceLog($job_ref_details['id'], $_SESSION['USER_ID'], 'Charge Mode Edited', $new_details, $old_details);
				$return_message .="Charge Mode Edited from - ".$old_details." to  ".$new_details."<br/>";
			}
		*/
//--------------------------------------------------------------------------------------------------------------------------------------------------		
		$return_message .="<b>Following log generated for Booking ID - ".$j1_details['id']."</b><br/>";
		if($j1_details['job_date'] != $_POST['j1_date_cap'])
			{
				$job->updateJob($j1_details['id'], 'job_date', $_POST['j1_date_cap']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Booking Date Edited', formatDate($_POST['j1_date_cap'],1), formatDate($j1_details['job_date'],1));
				$return_message .="Booking Date Edited from - ".formatDate($j1_details['job_date'],1)." to  ".formatDate($_POST['j1_date_cap'],1)."<br/>";
			}	
		if($j1_details['job_time'] != ''.$_POST['j1_time'].':00')
			{
				$job->updateJob($j1_details['id'], 'job_time', $_POST['j1_time']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Booking Time Edited', calculateTimeInAmPmFormatWithoutSeconds($_POST['j1_time']), calculateTimeInAmPmFormatWithoutSeconds($j1_details['job_time']));
				$return_message .="Booking Time Edited from - ".calculateTimeInAmPmFormatWithoutSeconds($j1_details['job_time'])." to  ".calculateTimeInAmPmFormatWithoutSeconds($_POST['j1_time'])."<br/>";
			}	
		if($_POST['j1_driver_status'] !='') // driver status not blank
			{
				$old_driver_status = $job->getDriverStatusDetails($j1_details['driver_status']);
				
				if($_POST['j1_driver_status'] =='1') //Allocate
					{
						$driver_details = $user->getUserDetails($_POST['j1_driver']); // get details of this driver
						$job->updateJob($j1_details['id'], 'driver_status', '1'); // change the driver status to Allocate
						$job->updateJob($j1_details['id'], 'driver_id', $_POST['j1_driver']); // Put this driver in job table
						$job->updateJob($j1_details['id'], 'driver_price', $_POST['j1_driver_price']); // Put the driver price in the job table
						$job->addNewDriverToJob($j1_details['id'], $_POST['j1_driver'], $_POST['j1_driver_price'], '', '', '', '', '', '', '', '', ''); // Add this to job__driver table
						
						$job->addJobDriverLog($j1_details['id'], $_SESSION['USER_ID'], 'Driver Status Edited', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$_POST['j1_driver_price'].'', '');
						$return_message .="Driver Status Edited from - ".$old_driver_status['details']." to  ALLOCATED<br/>";
					}
				if($_POST['j1_driver_status'] =='2') //Offered/Sent
					{
						$job->updateJob($j1_details['id'], 'driver_status', '2'); // change the driver status to Sent
						foreach($_POST['j1_drivers'] as $key => $value)	
							{
								$driver_details = $user->getUserDetails($value); // get details of this driver	
								$job->addNewDriverToJob($j1_details['id'], '', '', $value, $_POST['j1_driver_price'], '', '', '', '', '', '', ''); // Add this to job__driver table
								$job->addJobDriverLog($j1_details['id'], $_SESSION['USER_ID'], 'Job Offered', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$_POST['j1_driver_price'].'', '');
								$mailer->sendJobOfferToDriver($value, $j1_details['id'], $_POST['j1_driver_price']); // Send job offer to the driver
							}
						$return_message .="Job offer sent to selected drivers<br/>";
					}
				if($_POST['j1_driver_status'] == '3' && !empty($_POST['j1_driver']) && $_POST['j1_driver_price'] !='') //Accepted
					{
						$job->updateJob($j1_details['id'], 'driver_status', '3'); // change the driver status to Sent
						$job->updateJob($j1_details['id'], 'driver_id', $_POST['j1_driver']); // Put this driver in job table
						$job->updateJob($j1_details['id'], 'driver_price', $_POST['j1_driver_price']); // Put the driver price in the job table
						$job->addNewDriverToJob($j1_details['id'], '', '', '', '', $_POST['j1_driver'], $_POST['j1_driver_price'], '', '', '', '', ''); // Add this to job__driver table
						$driver_details = $user->getUserDetails($_POST['j1_driver']); // get details of this driver
						$job->addJobDriverLog($j1_details['id'], $_SESSION['USER_ID'], 'Job Accepted (Internal)', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$_POST['j1_driver_price'].'', '');
						
						$mailer->sendJobDetailsToDriver($j1_details['id'], $_POST['j1_driver'], $_POST['j1_driver_price']);		
					}
			}
		if($j1_details['frm_flight_no'] != $_POST['j1_flight_no'])
			{
				$job->updateJob($j1_details['id'], 'frm_flight_no', $_POST['j1_flight_no']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'From Flight Number Edited', $_POST['j1_flight_no'], $j1_details['frm_flight_no']);
				$return_message .="From Flight Number  Edited from - ".$j1_details['frm_flight_no']." to  ".$_POST['j1_flight_no']."<br/>";
			}
		
		if($j1_details['frm_line1'] != $_POST['j1_line_1'])
			{
				$job->updateJob($j1_details['id'], 'frm_line1', $_POST['j1_line_1']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'From Line 1 Edited', $_POST['j1_line_1'], $j1_details['frm_line1']);
				$return_message .="From Line 1 Edited from - ".$j1_details['frm_line1']." to  ".$_POST['j1_line_1']."<br/>";
			}
		
		if($j1_details['frm_line2'] != $_POST['j1_line_2'])
			{
				$job->updateJob($j1_details['id'], 'frm_line2', $_POST['j1_line_2']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'From Line 2 Edited', $_POST['j1_line_2'], $j1_details['frm_line2']);
				$return_message .="From Line 2 Edited from - ".$j1_details['frm_line2']." to  ".$_POST['j1_line_2']."<br/>";
			}
		
		if($j1_details['frm_sub'] != $_POST['j1_sub'])
			{
				$job->updateJob($j1_details['id'], 'frm_sub', $_POST['j1_sub']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'From Suburb Edited', $_POST['j1_sub'], $j1_details['frm_sub']);
				$return_message .="From Suburb Edited from - ".$j1_details['frm_sub']." to  ".$_POST['j1_sub']."<br/>";
			}
		
		if($j1_details['frm_pc'] != $_POST['j1_pc'])
			{
				$job->updateJob($j1_details['id'], 'frm_pc', $_POST['j1_pc']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'From Postcode Edited', $_POST['j1_pc'], $j1_details['frm_pc']);
				$return_message .="From Postcode Edited from - ".$j1_details['frm_pc']." to  ".$_POST['j1_pc']."<br/>";
			}
		
		if($j1_details['frm_state'] != $_POST['j1_state'])
			{
				$job->updateJob($j1_details['id'], 'frm_state', $_POST['j1_state']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'From State Edited', $_POST['j1_state'], $j1_details['frm_state']);
				$return_message .="From State Edited to <b>".$_POST['j1_state']."</b><br/>";
			}
			
		if($j1_details['to_flight_no'] != $_POST['j1_to_flight_no'])
			{
				$job->updateJob($j1_details['id'], 'to_flight_no', $_POST['j1_to_flight_no']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'To Flight Number Edited', $_POST['j1_to_flight_no'], $j1_details['to_flight_no']);
				$return_message .="to Flight Number  Edited from - ".$j1_details['to_flight_no']." to  ".$_POST['j1_to_flight_no']."<br/>";
			}
		
		if($j1_details['to_line1'] != $_POST['j1_to_line_1'])
			{
				$job->updateJob($j1_details['id'], 'to_line1', $_POST['j1_to_line_1']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'To Line 1 Edited', $_POST['j1_to_line_1'], $j1_details['to_line1']);
				$return_message .="To Line 1 Edited from - ".$j1_details['to_line1']." to  ".$_POST['j1_to_line_1']."<br/>";
			}
		
		if($j1_details['to_line2'] != $_POST['j1_to_line_2'])
			{
				$job->updateJob($j1_details['id'], 'to_line2', $_POST['j1_to_line_2']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'To Line 2 Edited', $_POST['j1_to_line_2'], $j1_details['to_line2']);
				$return_message .="To Line 2 Edited from - ".$j1_details['to_line2']." to  ".$_POST['j1_to_line_2']."<br/>";
			}
		
		if($j1_details['to_sub'] != $_POST['j1_to_sub'])
			{
				$job->updateJob($j1_details['id'], 'to_sub', $_POST['j1_to_sub']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'To Suburb Edited', $_POST['j1_to_sub'], $j1_details['to_sub']);
				$return_message .="To Suburb Edited from - ".$j1_details['to_sub']." to  ".$_POST['j1_to_sub']."<br/>";
			}
		
		if($j1_details['to_pc'] != $_POST['j1_to_pc'])
			{
				$job->updateJob($j1_details['id'], 'to_pc', $_POST['j1_to_pc']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'To Postcode Edited', $_POST['j1_to_pc'], $j1_details['to_pc']);
				$return_message .="To Postcode Edited from - ".$j1_details['to_pc']." to  ".$_POST['j1_to_pc']."<br/>";
			}
		
		if($j1_details['to_state'] != $_POST['j1_state'])
			{
				$job->updateJob($j1_details['id'], 'to_state', $_POST['j1_state']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'To State Edited', $_POST['j1_state'], $j1_details['to_state']);
				$return_message .="To State Edited to <b>".$_POST['j1_state']."</b><br/>";
			}
		if($j1_details['frm_via_line1'] != $_POST['j1_via_line_1'])
			{
				$job->updateJob($j1_details['id'], 'frm_via_line1', $_POST['j1_via_line_1']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'From Via Line 1 Edited', $_POST['j1_via_line_1'], $j1_details['frm_via_line1']);
				$return_message .="From Via Line 1 Edited from - ".$j1_details['frm_via_line1']." to  ".$_POST['j1_via_line_1']."<br/>";
			}
		
		if($j1_details['frm_via_line2'] != $_POST['j1_via_line_2'])
			{
				$job->updateJob($j1_details['id'], 'frm_via_line2', $_POST['j1_via_line_2']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'From Via Line 2 Edited', $_POST['j1_via_line_2'], $j1_details['frm_via_line2']);
				$return_message .="From Via Line 2 Edited from - ".$j1_details['frm_via_line2']." to  ".$_POST['j1_via_line_2']."<br/>";
			}
		
		if($j1_details['frm_via_sub'] != $_POST['j1_via_sub'])
			{
				$job->updateJob($j1_details['id'], 'frm_via_sub', $_POST['j1_via_sub']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'From Via Suburb Edited', $_POST['j1_via_sub'], $j1_details['frm_via_sub']);
				$return_message .="From Via Suburb Edited from - ".$j1_details['frm_via_sub']." to  ".$_POST['j1_via_sub']."<br/>";
			}
		
		if($j1_details['frm_via_pc'] != $_POST['j1_via_pc'])
			{
				$job->updateJob($j1_details['id'], 'frm_via_pc', $_POST['j1_via_pc']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'From Via Postcode Edited', $_POST['j1_via_pc'], $j1_details['frm_via_pc']);
				$return_message .="From Via Postcode Edited from - ".$j1_details['frm_via_pc']." to  ".$_POST['j1_via_pc']."<br/>";
			}

		if($j1_details['frm_via_state'] != $_POST['j1_state'] && $_POST['j1_via_sub'] != '')
			{
				$job->updateJob($j1_details['id'], 'frm_via_state', $_POST['j1_state']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'To Via State Edited', $_POST['j1_state'], $j1_details['to_via_state']);
				$return_message .="To Via State Edited to ".$_POST['j1_state']."<br/>";
			}
			
		if($j1_details['to_via_line1'] != $_POST['j1_to_via_line_1'])
			{
				$job->updateJob($j1_details['id'], 'to_via_line1', $_POST['j1_to_via_line_1']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'To Via Line 1 Edited', $_POST['j1_to_via_line_1'], $j1_details['to_via_line1']);
				$return_message .="To Via Line 1 Edited from - ".$j1_details['to_via_line1']." to  ".$_POST['j1_to_via_line_1']."<br/>";
			}
		
		if($j1_details['to_via_line2'] != $_POST['j1_to_via_line_2'])
			{
				$job->updateJob($j1_details['id'], 'to_via_line2', $_POST['j1_to_line_2']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'To Via Line 2 Edited', $_POST['j1_to_via_line_2'], $j1_details['to_via_line2']);
				$return_message .="To Via Line 2 Edited from - ".$j1_details['to_via_line2']." to  ".$_POST['j1_to_via_line_2']."<br/>";
			}
		
		if($j1_details['to_via_sub'] != $_POST['j1_to_via_sub'])
			{
				$job->updateJob($j1_details['id'], 'to_via_sub', $_POST['j1_to_via_sub']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'To Via Suburb Edited', $_POST['j1_to_via_sub'], $j1_details['to_via_sub']);
				$return_message .="To Via Suburb Edited from - ".$j1_details['to_via_sub']." to  ".$_POST['j1_to_via_sub']."<br/>";
			}
		
		if($j1_details['to_via_pc'] != $_POST['j1_to_via_pc'])
			{
				$job->updateJob($j1_details['id'], 'to_via_pc', $_POST['j1_to_via_pc']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'To Via Postcode Edited', $_POST['j1_to_via_pc'], $j1_details['to_via_pc']);
				$return_message .="To Via Postcode Edited from - ".$j1_details['to_via_pc']." to  ".$_POST['j1_to_via_pc']."<br/>";
			}
			
		if($j1_details['to_via_state'] != $_POST['j1_state'] && $_POST['j1_to_via_sub'] != '')
			{
				$job->updateJob($j1_details['id'], 'to_via_state', $_POST['j1_state']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'To Via State Edited', $_POST['j1_state'], $j1_details['to_via_state']);
				$return_message .="To Via State Edited to ".$_POST['j1_state']."<br/>";
			}
		
		if($j1_details['car_id'] != $_POST['j1_car_id'])
			{
				$query1 = "SELECT * from variable__car_type where id = ".$j1_details['car_id']."";
				$result1 = $database->query($query1);
				$row1 = mysql_fetch_array($result1);
				$old_details = $row1['details'];
				
				$query2 = "SELECT * from variable__car_type where id = ".$_POST['j1_car_id']."";
				$result2 = $database->query($query2);
				$row2 = mysql_fetch_array($result2);
				$new_details = $row2['details'];
				
				$job->updateJob($j1_details['id'], 'car_id', $_POST['j1_car_id']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Car Type Edited', $new_details, $old_details);
				$return_message .="Car Type Edited from - ".$old_details." to  ".$new_details."<br/>";
			}
		
		if($j1_details['pax_nos'] != $_POST['j1_pax_nos'])
			{
				$job->updateJob($j1_details['id'], 'pax_nos', $_POST['j1_pax_nos']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Pax Nos. Edited', $_POST['j1_pax_nos'], $j1_details['pax_nos']);
				$return_message .="Pax Nos. Edited from - ".$j1_details['pax_nos']." to  ".$_POST['j1_pax_nos']."<br/>";
			}
		if($j1_details['luggage'] != $_POST['j1_luggage'])
			{
				$job->updateJob($j1_details['id'], 'luggage', $_POST['j1_luggage']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Luggage Nos. Edited', $_POST['j1_luggage'], $j1_details['luggage']);
				$return_message .="Luggage Nos. Edited from - ".$j1_details['luggage']." to  ".$_POST['j1_luggage']."<br/>";
			}
		if($j1_details['baby_seat'] != $_POST['j1_baby_seats'])
			{
				$job->updateJob($j1_details['id'], 'baby_seat', $_POST['j1_baby_seats']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Baby Seat Nos. Edited', $_POST['j1_baby_seats'], $j1_details['baby_seat']);
				$return_message .="Baby Seat Nos. Edited from - ".$j1_details['baby_seat']." to  ".$_POST['j1_baby_seats']."<br/>";
			}
		if($j1_details['booster_seat'] != $_POST['j1_booster_seats'])
			{
				$job->updateJob($j1_details['id'], 'booster_seat', $_POST['j1_booster_seats']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Booster Seat Nos. Edited', $_POST['j1_booster_seats'], $j1_details['booster_seat']);
				$return_message .="Booster Seat Nos. Edited from - ".$j1_details['booster_seat']." to  ".$_POST['j1_booster_seats']."<br/>";
			}
		if($j1_details['baby_capsule'] != $_POST['j1_baby_capsules'])
			{
				$job->updateJob($j1_details['id'], 'baby_capsule', $_POST['j1_baby_capsules']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Baby Capsule Nos. Edited', $_POST['j1_baby_capsules'], $j1_details['baby_capsule']);
				$return_message .="Baby Capsule Nos. Edited from - ".$j1_details['baby_capsule']." to  ".$_POST['j1_baby_capsules']."<br/>";
			}
		if($j1_details['baby_capsule'] != $_POST['j1_baby_capsules'])
			{
				$job->updateJob($j1_details['id'], 'baby_capsule', $_POST['j1_baby_capsules']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Baby Capsule Nos. Edited', $_POST['j1_baby_capsules'], $j1_details['baby_capsule']);
				$return_message .="Baby Capsule Nos. Edited from - ".$j1_details['baby_capsule']." to  ".$_POST['j1_baby_capsules']."<br/>";
			}
		
		if($j1_details['kms'] != $_POST['j1_kms'] && $_POST['j1_kms'] != '')
			{
				$job->updateJob($j1_details['id'], 'kms', $_POST['j1_kms']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Kilometers Edited', $_POST['j1_kms'], $j1_details['kms']);
				$return_message .="Kilometers Edited from - ".$j1_details['kms']." to  ".$_POST['j1_kms']."<br/>";
			}
		
		if($j1_details['fare'] != $_POST['j1_fare'] && $_POST['j1_fare'] != '')
			{
				$job->updateJob($j1_details['id'], 'fare', $_POST['j1_fare']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Fare Edited', $_POST['j1_fare'], $j1_details['fare']);
				$return_message .="Fare Edited from - ".$j1_details['fare']." to  ".$_POST['j1_fare']."<br/>";
			}
		if($j1_details['inter'] != $_POST['j1_inter'] && $_POST['j1_inter'] != '')
			{
				$job->updateJob($j1_details['id'], 'inter', $_POST['j1_inter']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'International Fare Edited', $_POST['j1_inter'], $j1_details['inter']);
				$return_message .="International Fare Edited from - ".$j1_details['inter']." to  ".$_POST['j1_inter']."<br/>";
			}
		if($j1_details['ed'] != $_POST['j1_ed'] && $_POST['j1_ed'] != '')
			{
				$job->updateJob($j1_details['id'], 'ed', $_POST['j1_ed']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Extra Drop/ Pickup Fare Edited', $_POST['j1_ed'], $j1_details['ed']);
				$return_message .="Extra Drop/ Pickup Fare Edited from - ".$j1_details['ed']." to  ".$_POST['j1_ed']."<br/>";
			}
		if($j1_details['wait'] != $_POST['j1_wait'] && $_POST['j1_wait'] != '')
			{
				$job->updateJob($j1_details['id'], 'wait', $_POST['j1_wait']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Waiting Charges Edited', $_POST['j1_wait'], $j1_details['wait']);
				$return_message .="Waiting Charges Edited from - ".$j1_details['wait']." to  ".$_POST['j1_wait']."<br/>";
			}
		if($j1_details['tolls'] != $_POST['j1_tolls'] && $_POST['j1_tolls'] != '')
			{
				$job->updateJob($j1_details['id'], 'tolls', $_POST['j1_tolls']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Toll Charges Edited', $_POST['j1_tolls'], $j1_details['tolls']);
				$return_message .="Toll Charges Edited from - ".$j1_details['tolls']." to  ".$_POST['j1_tolls']."<br/>";
			}
		if($j1_details['bs'] != $_POST['j1_bs'] && $_POST['j1_bs'] != '')
			{
				$job->updateJob($j1_details['id'], 'bs', $_POST['j1_bs']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Baby Seat Charges Edited', $_POST['j1_bs'], $j1_details['bs']);
				$return_message .="Baby Seat Charges Edited from - ".$j1_details['bs']." to  ".$_POST['j1_bs']."<br/>";
			}
		if($j1_details['park'] != $_POST['j1_park'] && $_POST['j1_park'] != '')
			{
				$job->updateJob($j1_details['id'], 'park', $_POST['j1_park']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Parking Charges Edited', $_POST['j1_park'], $j1_details['park']);
				$return_message .="Parking Charges Edited from - ".$j1_details['park']." to  ".$_POST['j1_park']."<br/>";
			}
		if($j1_details['ah'] != $_POST['j1_ah'] && $_POST['j1_ah'] != '')
			{
				$job->updateJob($j1_details['id'], 'ah', $_POST['j1_ah']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'After Hour Charges Edited', $_POST['j1_ah'], $j1_details['ah']);
				$return_message .="After Hour Charges Edited from - ".$j1_details['ah']." to  ".$_POST['j1_ah']."<br/>";
			}
		if($j1_details['me'] != $_POST['j1_me'] && $_POST['j1_me'] != '')
			{
				$job->updateJob($j1_details['id'], 'me', $_POST['j1_me']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Major Event Charges Edited', $_POST['j1_me'], $j1_details['me']);
				$return_message .="Major Event Charges Edited from - ".$j1_details['me']." to  ".$_POST['j1_me']."<br/>";
			}
		if($j1_details['alc'] != $_POST['j1_alc'] && $_POST['j1_alc'] != '')
			{
				$job->updateJob($j1_details['id'], 'alc', $_POST['j1_alc']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Alcohol Charges Edited', $_POST['j1_alc'], $j1_details['alc']);
				$return_message .="Alcohol Charges Edited from - ".$j1_details['alc']." to  ".$_POST['j1_alc']."<br/>";
			}
		if($j1_details['fc'] != $_POST['j1_fc'] && $_POST['j1_fc'] != '')
			{
				$job->updateJob($j1_details['id'], 'fc', $_POST['j1_fc']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Food/Coffee Charges Edited', $_POST['j1_fc'], $j1_details['fc']);
				$return_message .="Food/Coffee Charges Edited from - ".$j1_details['fc']." to  ".$_POST['j1_fc']."<br/>";
			}
		if($j1_details['oth'] != $_POST['j1_oth'] && $_POST['j1_oth'] != '')
			{
				$job->updateJob($j1_details['id'], 'oth', $_POST['j1_oth']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Other Charges Edited', $_POST['j1_oth'], $j1_details['oth']);
				$return_message .="Other Charges Edited from - ".$j1_details['oth']." to  ".$_POST['j1_oth']."<br/>";
			}
		if($j1_details['tot_fare'] != $_POST['j1_tot_fare'] && $_POST['j1_tot_fare'] != '')
			{
				$job->updateJob($j1_details['id'], 'tot_fare', $_POST['j1_tot_fare']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Total Fare Edited', $_POST['j1_tot_fare'], $j1_details['tot_fare']);
				$return_message .="Total Fare Edited from - ".$j1_details['tot_fare']." to  ".$_POST['j1_tot_fare']."<br/>";
			}
		if($j1_details['drv_fee'] != $_POST['j1_drv_fee'] && $_POST['j1_drv_fee'] != '')
			{
				$job->updateJob($j1_details['id'], 'drv_fee', $_POST['j1_drv_fee']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Total Driver Fee Edited', $_POST['j1_drv_fee'], $j1_details['drv_fee']);
				$return_message .="Total Driver Fee Edited from - ".$j1_details['drv_fee']." to  ".$_POST['j1_drv_fee']."<br/>";
			}
		if($j1_details['oth_exp'] != $_POST['j1_oth_exp'] && $_POST['j1_oth_exp'] != '')
			{
				$job->updateJob($j1_details['id'], 'oth_exp', $_POST['j1_oth_exp']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Total Other Expenses Edited', $_POST['j1_oth_exp'], $j1_details['oth_exp']);
				$return_message .="Total Other Expenses Edited from - ".$j1_details['oth_exp']." to  ".$_POST['j1_oth_exp']."<br/>";
			}
		if($j1_details['profit'] != $_POST['j1_profit'] && $_POST['j1_profit'] != '')
			{
				$job->updateJob($j1_details['id'], 'profit', $_POST['j1_profit']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Total Profit Edited', $_POST['j1_profit'], $j1_details['profit']);
				$return_message .="Total Profit Edited from - ".$j1_details['profit']." to  ".$_POST['j1_profit']."<br/>";
			}
		if($j1_details['ext_notes'] != $_POST['j1_ext_notes'])
			{
				$job->updateJob($j1_details['id'], 'ext_notes', $_POST['j1_ext_notes']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'External Notes Edited', $_POST['j1_ext_notes'], $j1_details['ext_notes']);
				$return_message .="External Notes Edited from - ".$j1_details['ext_notes']." to  ".$_POST['j1_ext_notes']."<br/>";
			}
		if($j1_details['int_notes'] != $_POST['j1_int_notes'])
			{
				$job->updateJob($j1_details['id'], 'int_notes', $_POST['j1_int_notes']);
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Internal Notes Edited', $_POST['j1_int_notes'], $j1_details['int_notes']);
				$return_message .="Internal Notes Edited from - ".$j1_details['int_notes']." to  ".$_POST['j1_int_notes']."<br/>";
			}

		if($job_ref_details['job_type'] == '2')
			{
				
				$j2_details = $job->getJobDetails($_POST['j2_id']);
				$return_message .="<b>Following log generated for Booking ID - ".$j2_details['id']."</b><br/>";
				
				if($j2_details['job_date'] != $_POST['j2_date_cap'])
					{
						$job->updateJob($j2_details['id'], 'job_date', $_POST['j2_date_cap']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Booking Date Edited', formatDate($_POST['j2_date_cap'],1), formatDate($j2_details['job_date'],1));
						$return_message .="Booking Date Edited from - ".formatDate($j2_details['job_date'],1)." to  ".formatDate($_POST['j2_date_cap'],1)."<br/>";
					}	
				if($j2_details['job_time'] != ''.$_POST['j2_time'].':00')
					{
						$job->updateJob($j2_details['id'], 'job_time', $_POST['j2_time']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Booking Time Edited', calculateTimeInAmPmFormatWithoutSeconds($_POST['j2_time']), calculateTimeInAmPmFormatWithoutSeconds($j2_details['job_time']));
						$return_message .="Booking Time Edited from - ".calculateTimeInAmPmFormatWithoutSeconds($j2_details['job_time'])." to  ".calculateTimeInAmPmFormatWithoutSeconds($_POST['j2_time'])."<br/>";
					}	
				if($_POST['j2_driver_status'] !='') // driver status not blank
					{
						$old_driver_status = $job->getDriverStatusDetails($j2_details['driver_status']);
						
						if($_POST['j2_driver_status'] =='1') //Allocate
							{
								$driver_details = $user->getUserDetails($_POST['j2_driver']); // get details of this driver
								$job->updateJob($j2_details['id'], 'driver_status', '1'); // change the driver status to Allocate
								$job->updateJob($j2_details['id'], 'driver_id', $_POST['j2_driver']); // Put this driver in job table
								$job->updateJob($j2_details['id'], 'driver_price', $_POST['j2_driver_price']); // Put the driver price in the job table
								$job->addNewDriverToJob($j2_details['id'], $_POST['j2_driver'], $_POST['j2_driver_price'], '', '', '', '', '', '', '', '', ''); // Add this to job__driver table
								
								$job->addJobDriverLog($j2_details['id'], $_SESSION['USER_ID'], 'Driver Status Edited', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$_POST['j2_driver_price'].'', '');
								$return_message .="Driver Status Edited from - ".$old_driver_status['details']." to  ALLOCATED<br/>";
							}
						if($_POST['j2_driver_status'] =='2') //Offered/Sent
							{
								$job->updateJob($j2_details['id'], 'driver_status', '2'); // change the driver status to Sent
								foreach($_POST['j2_drivers'] as $key => $value)	
									{
										$driver_details = $user->getUserDetails($value); // get details of this driver	
										$job->addNewDriverToJob($j2_details['id'], '', '', $value, $_POST['j1_driver_price'], '', '', '', '', '', '', ''); // Add this to job__driver table
										$job->addJobDriverLog($j2_details['id'], $_SESSION['USER_ID'], 'Job Offered', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$_POST['j2_driver_price'].'', '');
										$mailer->sendJobOfferToDriver($value, $j2_details['id'], $_POST['j2_driver_price']); // Send job offer to the driver
									}
								$return_message .="Job offer sent to selected drivers<br/>";
							}
						if($_POST['j2_driver_status'] == '3' && !empty($_POST['j2_driver']) && $_POST['j2_driver_price'] !='') //Accepted
							{
								$job->updateJob($j2_details['id'], 'driver_status', '3'); // change the driver status to Sent
								$job->updateJob($j2_details['id'], 'driver_id', $_POST['j2_driver']); // Put this driver in job table
								$job->updateJob($j2_details['id'], 'driver_price', $_POST['j2_driver_price']); // Put the driver price in the job table
								$job->addNewDriverToJob($j2_details['id'], '', '', '', '', $_POST['j2_driver'], $_POST['j2_driver_price'], '', '', '', '', ''); // Add this to job__driver table
								$driver_details = $user->getUserDetails($_POST['j2_driver']); // get details of this driver
								$job->addJobDriverLog($j2_details['id'], $_SESSION['USER_ID'], 'Job Accepted (Internal)', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$_POST['j2_driver_price'].'', '');
										
								$mailer->sendJobDetailsToDriver($j2_details['id'], $_POST['j2_driver'], $_POST['j2_driver_price']);	
							}
					}
				if($j2_details['frm_flight_no'] != $_POST['j2_flight_no'])
					{
						$job->updateJob($j2_details['id'], 'frm_flight_no', $_POST['j2_flight_no']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'From Flight Number Edited', $_POST['j2_flight_no'], $j2_details['frm_flight_no']);
						$return_message .="From Flight Number  Edited from - ".$j2_details['frm_flight_no']." to  ".$_POST['j2_flight_no']."<br/>";
					}
				
				if($j2_details['frm_line1'] != $_POST['j2_line_1'])
					{
						$job->updateJob($j2_details['id'], 'frm_line1', $_POST['j2_line_1']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'From Line 1 Edited', $_POST['j2_line_1'], $j2_details['frm_line1']);
						$return_message .="From Line 1 Edited from - ".$j2_details['frm_line1']." to  ".$_POST['j2_line_1']."<br/>";
					}
				
				if($j2_details['frm_line2'] != $_POST['j2_line_2'])
					{
						$job->updateJob($j2_details['id'], 'frm_line2', $_POST['j2_line_2']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'From Line 2 Edited', $_POST['j2_line_2'], $j2_details['frm_line2']);
						$return_message .="From Line 2 Edited from - ".$j2_details['frm_line2']." to  ".$_POST['j2_line_2']."<br/>";
					}
				
				if($j2_details['frm_sub'] != $_POST['j2_sub'])
					{
						$job->updateJob($j2_details['id'], 'frm_sub', $_POST['j2_sub']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'From Suburb Edited', $_POST['j2_sub'], $j2_details['frm_sub']);
						$return_message .="From Suburb Edited from - ".$j2_details['frm_sub']." to  ".$_POST['j2_sub']."<br/>";
					}
				
				if($j2_details['frm_pc'] != $_POST['j2_pc'])
					{
						$job->updateJob($j2_details['id'], 'frm_pc', $_POST['j2_pc']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'From Postcode Edited', $_POST['j2_pc'], $j2_details['frm_pc']);
						$return_message .="From Postcode Edited from - ".$j2_details['frm_pc']." to  ".$_POST['j2_pc']."<br/>";
					}
				
				if($j2_details['frm_state'] != $_POST['j2_state'])
					{
						$job->updateJob($j2_details['id'], 'frm_state', $_POST['j2_state']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'From State Edited', $_POST['j2_state'], $j2_details['frm_state']);
						$return_message .="From State Edited to <b>".$_POST['j2_state']."</b><br/>";
					}
					
				if($j2_details['to_flight_no'] != $_POST['j2_to_flight_no'])
					{
						$job->updateJob($j2_details['id'], 'to_flight_no', $_POST['j2_to_flight_no']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'To Flight Number Edited', $_POST['j2_to_flight_no'], $j2_details['to_flight_no']);
						$return_message .="to Flight Number  Edited from - ".$j2_details['to_flight_no']." to  ".$_POST['j2_to_flight_no']."<br/>";
					}
				
				if($j2_details['to_line1'] != $_POST['j2_to_line_1'])
					{
						$job->updateJob($j2_details['id'], 'to_line1', $_POST['j2_to_line_1']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'To Line 1 Edited', $_POST['j2_to_line_1'], $j2_details['to_line1']);
						$return_message .="To Line 1 Edited from - ".$j2_details['to_line1']." to  ".$_POST['j2_to_line_1']."<br/>";
					}
				
				if($j2_details['to_line2'] != $_POST['j2_to_line_2'])
					{
						$job->updateJob($j2_details['id'], 'to_line2', $_POST['j2_to_line_2']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'To Line 2 Edited', $_POST['j2_to_line_2'], $j2_details['to_line2']);
						$return_message .="To Line 2 Edited from - ".$j2_details['to_line2']." to  ".$_POST['j2_to_line_2']."<br/>";
					}
				
				if($j2_details['to_sub'] != $_POST['j2_to_sub'])
					{
						$job->updateJob($j2_details['id'], 'to_sub', $_POST['j2_to_sub']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'To Suburb Edited', $_POST['j2_to_sub'], $j2_details['to_sub']);
						$return_message .="To Suburb Edited from - ".$j2_details['to_sub']." to  ".$_POST['j2_to_sub']."<br/>";
					}
				
				if($j2_details['to_pc'] != $_POST['j2_to_pc'])
					{
						$job->updateJob($j2_details['id'], 'to_pc', $_POST['j2_to_pc']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'To Postcode Edited', $_POST['j2_to_pc'], $j2_details['to_pc']);
						$return_message .="To Postcode Edited from - ".$j2_details['to_pc']." to  ".$_POST['j2_to_pc']."<br/>";
					}
				
				if($j1_details['to_state'] != $_POST['j2_state'])
					{
						$job->updateJob($j2_details['id'], 'to_state', $_POST['j2_state']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'To State Edited', $_POST['j2_state'], $j2_details['to_state']);
						$return_message .="To State Edited to <b>".$_POST['j2_state']."</b><br/>";
					}
					
				if($j2_details['frm_via_line1'] != $_POST['j2_via_line_1'])
					{
						$job->updateJob($j2_details['id'], 'frm_via_line1', $_POST['j2_via_line_1']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'From Via Line 1 Edited', $_POST['j2_via_line_1'], $j2_details['frm_via_line1']);
						$return_message .="From Via Line 1 Edited from - ".$j2_details['frm_via_line1']." to  ".$_POST['j2_via_line_1']."<br/>";
					}
				
				if($j2_details['frm_via_line2'] != $_POST['j2_via_line_2'])
					{
						$job->updateJob($j2_details['id'], 'frm_via_line2', $_POST['j2_via_line_2']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'From Via Line 2 Edited', $_POST['j2_via_line_2'], $j2_details['frm_via_line2']);
						$return_message .="From Via Line 2 Edited from - ".$j2_details['frm_via_line2']." to  ".$_POST['j2_via_line_2']."<br/>";
					}
				
				if($j2_details['frm_via_sub'] != $_POST['j2_via_sub'])
					{
						$job->updateJob($j2_details['id'], 'frm_via_sub', $_POST['j2_via_sub']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'From Via Suburb Edited', $_POST['j2_via_sub'], $j2_details['frm_via_sub']);
						$return_message .="From Via Suburb Edited from - ".$j2_details['frm_via_sub']." to  ".$_POST['j2_via_sub']."<br/>";
					}
				
				if($j2_details['frm_via_pc'] != $_POST['j2_via_pc'])
					{
						$job->updateJob($j2_details['id'], 'frm_via_pc', $_POST['j2_via_pc']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'From Via Postcode Edited', $_POST['j2_via_pc'], $j2_details['frm_via_pc']);
						$return_message .="From Via Postcode Edited from - ".$j2_details['frm_via_pc']." to  ".$_POST['j2_via_pc']."<br/>";
					}
					
				if($j1_details['frm_via_state'] != $_POST['j2_state'] && $_POST['j2_via_sub'] != '')
					{
						$job->updateJob($j2_details['id'], 'frm_via_state', $_POST['j2_state']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'From Via State Edited', $_POST['j2_state'], $j2_details['to_via_state']);
						$return_message .="From Via State Edited to ".$_POST['j2_state']."<br/>";
					}
					
				if($j2_details['to_via_line1'] != $_POST['j2_to_via_line_1'])
					{
						$job->updateJob($j2_details['id'], 'to_via_line1', $_POST['j2_to_via_line_1']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'To Via Line 1 Edited', $_POST['j2_to_via_line_1'], $j2_details['to_via_line1']);
						$return_message .="To Via Line 1 Edited from - ".$j2_details['to_via_line1']." to  ".$_POST['j2_to_via_line_1']."<br/>";
					}
				
				if($j2_details['to_via_line2'] != $_POST['j2_to_via_line_2'])
					{
						$job->updateJob($j2_details['id'], 'to_via_line2', $_POST['j2_to_line_2']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'To Via Line 2 Edited', $_POST['j2_to_via_line_2'], $j2_details['to_via_line2']);
						$return_message .="To Via Line 2 Edited from - ".$j2_details['to_via_line2']." to  ".$_POST['j2_to_via_line_2']."<br/>";
					}
				
				if($j2_details['to_via_sub'] != $_POST['j2_to_via_sub'])
					{
						$job->updateJob($j2_details['id'], 'to_via_sub', $_POST['j2_to_via_sub']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'To Via Suburb Edited', $_POST['j2_to_via_sub'], $j2_details['to_via_sub']);
						$return_message .="To Via Suburb Edited from - ".$j2_details['to_via_sub']." to  ".$_POST['j2_to_via_sub']."<br/>";
					}
				
				if($j2_details['to_via_pc'] != $_POST['j2_to_via_pc'])
					{
						$job->updateJob($j2_details['id'], 'to_via_pc', $_POST['j2_to_via_pc']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'To Via Postcode Edited', $_POST['j2_to_via_pc'], $j2_details['to_via_pc']);
						$return_message .="To Via Postcode Edited from - ".$j2_details['to_via_pc']." to  ".$_POST['j2_to_via_pc']."<br/>";
					}
					
				if($j1_details['to_via_state'] != $_POST['j2_state'] && $_POST['j2_to_via_sub'] != '')
					{
						$job->updateJob($j2_details['id'], 'to_via_state', $_POST['j2_state']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'To Via State Edited', $_POST['j2_state'], $j2_details['to_via_state']);
						$return_message .="To Via State Edited to ".$_POST['j2_state']."<br/>";
					}
					
				if($j2_details['car_id'] != $_POST['j2_car_id'])
					{
						$query1 = "SELECT * from variable__car_type where id = ".$j2_details['car_id']."";
						$result1 = $database->query($query1);
						$row1 = mysql_fetch_array($result1);
						$old_details = $row1['details'];
						
						$query2 = "SELECT * from variable__car_type where id = ".$_POST['j2_car_id']."";
						$result2 = $database->query($query2);
						$row2 = mysql_fetch_array($result2);
						$new_details = $row2['details'];
						
						$job->updateJob($j2_details['id'], 'car_id', $_POST['j2_car_id']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Car Type Edited', $new_details, $old_details);
						$return_message .="Car Type Edited from - ".$old_details." to  ".$new_details."<br/>";
					}
				
				if($j2_details['pax_nos'] != $_POST['j2_pax_nos'])
					{
						$job->updateJob($j2_details['id'], 'pax_nos', $_POST['j2_pax_nos']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Pax Nos. Edited', $_POST['j2_pax_nos'], $j2_details['pax_nos']);
						$return_message .="Pax Nos. Edited from - ".$j2_details['pax_nos']." to  ".$_POST['j2_pax_nos']."<br/>";
					}
				if($j2_details['luggage'] != $_POST['j2_luggage'])
					{
						$job->updateJob($j2_details['id'], 'luggage', $_POST['j2_luggage']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Luggage Nos. Edited', $_POST['j2_luggage'], $j2_details['luggage']);
						$return_message .="Luggage Nos. Edited from - ".$j2_details['luggage']." to  ".$_POST['j2_luggage']."<br/>";
					}
				if($j2_details['baby_seat'] != $_POST['j2_baby_seats'])
					{
						$job->updateJob($j2_details['id'], 'baby_seat', $_POST['j2_baby_seats']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Baby Seat Nos. Edited', $_POST['j2_baby_seats'], $j2_details['baby_seat']);
						$return_message .="Baby Seat Nos. Edited from - ".$j2_details['baby_seat']." to  ".$_POST['j2_baby_seats']."<br/>";
					}
				if($j2_details['booster_seat'] != $_POST['j2_booster_seats'])
					{
						$job->updateJob($j2_details['id'], 'booster_seat', $_POST['j2_booster_seats']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Booster Seat Nos. Edited', $_POST['j2_booster_seats'], $j2_details['booster_seat']);
						$return_message .="Booster Seat Nos. Edited from - ".$j2_details['booster_seat']." to  ".$_POST['j2_booster_seats']."<br/>";
					}
				if($j2_details['baby_capsule'] != $_POST['j2_baby_capsules'])
					{
						$job->updateJob($j2_details['id'], 'baby_capsule', $_POST['j2_baby_capsules']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Baby Capsule Nos. Edited', $_POST['j2_baby_capsules'], $j2_details['baby_capsule']);
						$return_message .="Baby Capsule Nos. Edited from - ".$j2_details['baby_capsule']." to  ".$_POST['j2_baby_capsules']."<br/>";
					}
				if($j2_details['baby_capsule'] != $_POST['j2_baby_capsules'])
					{
						$job->updateJob($j2_details['id'], 'baby_capsule', $_POST['j2_baby_capsules']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Baby Capsule Nos. Edited', $_POST['j2_baby_capsules'], $j2_details['baby_capsule']);
						$return_message .="Baby Capsule Nos. Edited from - ".$j2_details['baby_capsule']." to  ".$_POST['j2_baby_capsules']."<br/>";
					}
				
				if($j2_details['kms'] != $_POST['j2_kms'] && $_POST['j2_kms'] != '')
					{
						$job->updateJob($j2_details['id'], 'kms', $_POST['j2_kms']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Kilometers Edited', $_POST['j2_kms'], $j2_details['kms']);
						$return_message .="Kilometers Edited from - ".$j2_details['kms']." to  ".$_POST['j2_kms']."<br/>";
					}
				
				if($j2_details['fare'] != $_POST['j2_fare'] && $_POST['j2_fare'] != '')
					{
						$job->updateJob($j2_details['id'], 'fare', $_POST['j2_fare']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Fare Edited', $_POST['j2_fare'], $j2_details['fare']);
						$return_message .="Fare Edited from - ".$j2_details['fare']." to  ".$_POST['j2_fare']."<br/>";
					}
				if($j2_details['inter'] != $_POST['j2_inter'] && $_POST['j2_inter'] != '')
					{
						$job->updateJob($j2_details['id'], 'inter', $_POST['j2_inter']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'International Fare Edited', $_POST['j2_inter'], $j2_details['inter']);
						$return_message .="International Fare Edited from - ".$j2_details['inter']." to  ".$_POST['j2_inter']."<br/>";
					}
				if($j2_details['ed'] != $_POST['j2_ed'] && $_POST['j2_ed'] != '')
					{
						$job->updateJob($j2_details['id'], 'ed', $_POST['j2_ed']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Extra Drop/ Pickup Fare Edited', $_POST['j2_ed'], $j2_details['ed']);
						$return_message .="Extra Drop/ Pickup Fare Edited from - ".$j2_details['ed']." to  ".$_POST['j2_ed']."<br/>";
					}
				if($j2_details['wait'] != $_POST['j2_wait'] && $_POST['j2_wait'] != '')
					{
						$job->updateJob($j2_details['id'], 'wait', $_POST['j2_wait']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Waiting Charges Edited', $_POST['j2_wait'], $j2_details['wait']);
						$return_message .="Waiting Charges Edited from - ".$j2_details['wait']." to  ".$_POST['j2_wait']."<br/>";
					}
				if($j2_details['tolls'] != $_POST['j2_tolls'] && $_POST['j2_tolls'] != '')
					{
						$job->updateJob($j2_details['id'], 'tolls', $_POST['j2_tolls']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Toll Charges Edited', $_POST['j2_tolls'], $j2_details['tolls']);
						$return_message .="Toll Charges Edited from - ".$j2_details['tolls']." to  ".$_POST['j2_tolls']."<br/>";
					}
				if($j2_details['bs'] != $_POST['j2_bs'] && $_POST['j2_bs'] != '')
					{
						$job->updateJob($j2_details['id'], 'bs', $_POST['j2_bs']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Baby Seat Charges Edited', $_POST['j2_bs'], $j2_details['bs']);
						$return_message .="Baby Seat Charges Edited from - ".$j2_details['bs']." to  ".$_POST['j2_bs']."<br/>";
					}
				if($j2_details['park'] != $_POST['j2_park'] && $_POST['j2_ed'] != '')
					{
						$job->updateJob($j2_details['id'], 'park', $_POST['j2_park']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Parking Charges Edited', $_POST['j2_park'], $j2_details['park']);
						$return_message .="Parking Charges Edited from - ".$j2_details['park']." to  ".$_POST['j2_park']."<br/>";
					}
				if($j2_details['ah'] != $_POST['j2_ah'] && $_POST['j2_ah'] != '')
					{
						$job->updateJob($j2_details['id'], 'ah', $_POST['j2_ah']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'After Hour Charges Edited', $_POST['j2_ah'], $j2_details['ah']);
						$return_message .="After Hour Charges Edited from - ".$j2_details['ah']." to  ".$_POST['j2_ah']."<br/>";
					}
				if($j2_details['me'] != $_POST['j2_me'] && $_POST['j2_me'] != '')
					{
						$job->updateJob($j2_details['id'], 'me', $_POST['j2_me']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Major Event Charges Edited', $_POST['j2_me'], $j2_details['me']);
						$return_message .="Major Event Charges Edited from - ".$j2_details['me']." to  ".$_POST['j2_me']."<br/>";
					}
				if($j2_details['alc'] != $_POST['j2_alc'] && $_POST['j2_alc'] != '')
					{
						$job->updateJob($j2_details['id'], 'alc', $_POST['j2_alc']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Alcohol Charges Edited', $_POST['j2_alc'], $j2_details['alc']);
						$return_message .="Alcohol Charges Edited from - ".$j2_details['alc']." to  ".$_POST['j2_alc']."<br/>";
					}
				if($j2_details['fc'] != $_POST['j2_fc'] && $_POST['j2_fc'] != '')
					{
						$job->updateJob($j2_details['id'], 'fc', $_POST['j2_fc']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Food/Coffee Charges Edited', $_POST['j2_fc'], $j2_details['fc']);
						$return_message .="Food/Coffee Charges Edited from - ".$j2_details['fc']." to  ".$_POST['j2_fc']."<br/>";
					}
				if($j2_details['oth'] != $_POST['j2_oth'] && $_POST['j2_oth'] != '')
					{
						$job->updateJob($j2_details['id'], 'oth', $_POST['j2_oth']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Other Charges Edited', $_POST['j2_oth'], $j2_details['oth']);
						$return_message .="Other Charges Edited from - ".$j2_details['oth']." to  ".$_POST['j2_oth']."<br/>";
					}
				if($j2_details['tot_fare'] != $_POST['j2_tot_fare'] && $_POST['j2_tot_fare'] != '')
					{
						$job->updateJob($j2_details['id'], 'tot_fare', $_POST['j2_tot_fare']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Total Fare Edited', $_POST['j2_tot_fare'], $j2_details['tot_fare']);
						$return_message .="Total Fare Edited from - ".$j2_details['tot_fare']." to  ".$_POST['j2_tot_fare']."<br/>";
					}
				if($j2_details['drv_fee'] != $_POST['j2_drv_fee'] && $_POST['j2_drv_fee'] != '')
					{
						$job->updateJob($j2_details['id'], 'drv_fee', $_POST['j2_drv_fee']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Total Driver Fee Edited', $_POST['j2_drv_fee'], $j2_details['drv_fee']);
						$return_message .="Total Driver Fee Edited from - ".$j2_details['drv_fee']." to  ".$_POST['j2_drv_fee']."<br/>";
					}
				if($j2_details['oth_exp'] != $_POST['j2_oth_exp'] && $_POST['j2_oth_exp'] != '')
					{
						$job->updateJob($j2_details['id'], 'oth_exp', $_POST['j2_oth_exp']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Total Other Expenses Edited', $_POST['j2_oth_exp'], $j2_details['oth_exp']);
						$return_message .="Total Other Expenses Edited from - ".$j2_details['oth_exp']." to  ".$_POST['j2_oth_exp']."<br/>";
					}
				if($j2_details['profit'] != $_POST['j2_profit'] && $_POST['j2_profit'] != '')
					{
						$job->updateJob($j2_details['id'], 'profit', $_POST['j2_profit']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Total Profit Edited', $_POST['j2_profit'], $j2_details['profit']);
						$return_message .="Total Profit Edited from - ".$j2_details['profit']." to  ".$_POST['j2_profit']."<br/>";
					}
				if($j2_details['ext_notes'] != $_POST['j2_ext_notes'])
					{
						$job->updateJob($j2_details['id'], 'ext_notes', $_POST['j2_ext_notes']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'External Notes Edited', $_POST['j2_ext_notes'], $j2_details['ext_notes']);
						$return_message .="External Notes Edited from - ".$j2_details['ext_notes']." to  ".$_POST['j2_ext_notes']."<br/>";
					}
				if($j2_details['int_notes'] != $_POST['j2_int_notes'])
					{
						$job->updateJob($j2_details['id'], 'int_notes', $_POST['j2_int_notes']);
						$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Internal Notes Edited', $_POST['j2_int_notes'], $j2_details['int_notes']);
						$return_message .="Internal Notes Edited from - ".$j2_details['int_notes']." to  ".$_POST['j2_int_notes']."<br/>";
					}
			}
	}


//----------------------------------------------------------------------------------------------------------------------------------------
	$address->checkDuplicateOrEnter($user_id, $_POST['j1_line_1'], $_POST['j1_line_2'], $_POST['j1_sub'], $_POST['j1_pc'], $_POST['pk_state']);
	$address->checkDuplicateOrEnter($user_id, $_POST['j1_to_line_1'], $_POST['j1_to_line_2'], $_POST['j1_to_sub'], $_POST['j1_to_pc'], $_POST['pk_state']);
	$address->checkDuplicateOrEnter($user_id, $_POST['j2_line_1'], $_POST['j2_line_2'], $_POST['j2_sub'], $_POST['j2_pc'], $_POST['ret_state']);
	$address->checkDuplicateOrEnter($user_id, $_POST['j2_to_line_1'], $_POST['j2_to_line_2'], $_POST['j2_to_sub'], $_POST['j2_to_pc'], $_POST['ret_state']);


if(isset($_GET['job_decision']))
	{	
		if($_GET['job_decision']=='confirm_jobs') // confirm jobs
			{
				
				$job 			= new job();
				$job_reference 	= new jobReference();
				$mailer 		= new mailer();
				$user			= new user();
				
				
				$j1_details 	= $job->getJobDetails($_GET['j1_id']);
				$j2_details 	= $job->getJobDetails($_GET['j2_id']);
				
				$old_j1_status 	= $job->getJobStatusDetails($j1_details['job_status']);
				$old_j2_status 	= $job->getJobStatusDetails($j2_details['job_status']);
				
				$job->updateJob($j1_details['id'], 'job_status', '20');
				$job->updateJob($j2_details['id'], 'job_status', '20');
				
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$j1_details['id'].' - Confirmed', 'Confirmed', $old_j1_status['details']);
				$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$j2_details['id'].' - Confirmed', 'Confirmed', $old_j2_status['details']);
				
				$job_ref_details = $job_reference->getJobReferenceDetails($j1_details['job_reference_id']);
				
				
				$user_details = $user->getUserDetails($job_ref_details['bkg_by_id']);
				$user_emails = $user->getUserEmails($contact_id);
				foreach($user_emails as $key => $value)
					{
						$mailer->sendMailToClientOnBooking($job_ref_details['id'], '1', $value, $user_details['title'], $user_details['fname'], $user_details['lname']);
					}
				$user_details = $user->getUserDetails($job_ref_details['pax_id']);
				$user_emails = $user->getUserEmails($pax_id);
				foreach($user_emails as $key => $value)
					{
						$mailer->sendMailToClientOnBooking($job_ref_details['id'], '1', $value, $user_details['title'], $user_details['fname'], $user_details['lname']);
					}
				$return_message ="Booking ID ".$_GET['j1_id']." & ".$_GET['j2_id']."- Confirmed.<br/>Confirmation emails have been sent to both booking made by and pax.";
			}

		elseif($_GET['job_decision'] == 'decline_jobs') // decline jobs
			{
				$job 			= new job();
				$job_reference 	= new jobReference();
				$mailer 		= new mailer();
				$user			= new user();
				
				
				$j1_details 	= $job->getJobDetails($_GET['j1_id']);
				$j2_details 	= $job->getJobDetails($_GET['j2_id']);
				
				$old_j1_status 	= $job->getJobStatusDetails($j1_details['job_status']);
				$old_j2_status 	= $job->getJobStatusDetails($j2_details['job_status']);
				
				$job->updateJob($j1_details['id'], 'job_status', '90');
				$job->updateJob($j2_details['id'], 'job_status', '90');
				
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$j1_details['id'].' - Declined', 'Declined', $old_j1_status['details']);
				$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$j2_details['id'].' - Declined', 'Declined', $old_j2_status['details']);
				
				$job_ref_details = $job_reference->getJobReferenceDetails($j1_details['job_reference_id']);

				$user_details = $user->getUserDetails($job_ref_details['bkg_by_id']);
				$user_emails = $user->getUserEmails($contact_id);
				foreach($user_emails as $key => $value)
					{
						$mailer->sendMailToClientOnBooking($job_ref_details['id'], '2', $value, $user_details['title'], $user_details['fname'], $user_details['lname']);
					}
				$user_details = $user->getUserDetails($job_ref_details['pax_id']);
				$user_emails = $user->getUserEmails($pax_id);
				foreach($user_emails as $key => $value)
					{
						$mailer->sendMailToClientOnBooking($job_ref_details['id'], '2', $value, $user_details['title'], $user_details['fname'], $user_details['lname']);
					}
					
				$return_message ="Booking ID ".$_GET['j1_id']." - Declined<br/>Booking rejected notification emails have been sent to both booking made by and pax.";
			}
		elseif($_GET['job_decision'] == 'cancel_jobs') // cancel jobs
			{
				$job = new job();
				
				$j1_details 	= $job->getJobDetails($_GET['j1_id']);
				$j2_details 	= $job->getJobDetails($_GET['j2_id']);
				
				$old_j1_status 	= $job->getJobStatusDetails($j1_details['job_status']);
				$old_j2_status 	= $job->getJobStatusDetails($j2_details['job_status']);
				
				$job->updateJob($j1_details['id'], 'job_status', '100');
				$job->updateJob($j2_details['id'], 'job_status', '100');
				
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$j1_details['id'].' - Cancelled', 'Cancelled', $old_j1_status['details']);
				$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$j2_details['id'].' - Cancelled', 'Cancelled', $old_j2_status['details']);
				
				$return_message ="Booking ID ".$_GET['j1_id']." & ".$_GET['j1_id']." - Cancelled<br/>Please note, on cancellation, email notification is not sent to anyone.";
			}
		elseif($_GET['job_decision'] == 'clone_jobs') // clone jobs
			{
				$job = new job();
				$j1_details = $job->getJobDetails($_GET['j1_id']);
				$j2_details = $job->getJobDetails($_GET['j2_id']);
				
				$cloned_job_ids = $job->CloneReturnJob($_GET['j1_id'], $_GET['j2_id']);
				
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$j1_details['id'].' - CLONED', 'New Job id '.$cloned_job_ids['j1_id'].'', '');
				$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$j2_details['id'].' - CLONED', 'New Job id '.$cloned_job_ids['j2_id'].'', '');
				
				$return_message ="Booking IDs ".$_GET['j1_id']." & ".$_GET['j2_id']." - CLONED<br/>New Cloned Booking IDs - ".$cloned_job_ids['j1_id']." & ".$cloned_job_ids['j2_id']."";
				$data1 = $cloned_job_ids['j1_id'];
				$data2 = $cloned_job_ids['j2_id'];
			}
		elseif ($_GET['job_decision'] == 'rev_clone_jobs') // reverse clone jobs
			{
				$job = new job();
				$j1_details = $job->getJobDetails($_GET['j1_id']);
				$j2_details = $job->getJobDetails($_GET['j2_id']);
				
				$cloned_job_ids = $job->ReverseCloneReturnJob($_GET['j1_id'], $_GET['j2_id']);
				
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$j1_details['id'].' - CLONED', 'New Job id '.$cloned_job_ids['j1_id'].'', '');
				$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$j2_details['id'].' - CLONED', 'New Job id '.$cloned_job_ids['j2_id'].'', '');
				
				$return_message ="Booking IDs ".$_GET['j1_id']." & ".$_GET['j2_id']." - REVERSE CLONED<br/>New Cloned Booking IDs - ".$cloned_job_ids['j1_id']." & ".$cloned_job_ids['j2_id']."";
				$data1 = $cloned_job_ids['j1_id'];
				$data2 = $cloned_job_ids['j2_id'];
			}
		elseif($_GET['job_decision'] == 'reactivate_jobs') // reactivate jobs
			{
				$job = new job();
				
				$j1_details 	= $job->getJobDetails($_GET['j1_id']);
				$j2_details 	= $job->getJobDetails($_GET['j2_id']);
				
				$old_j1_status 	= $job->getJobStatusDetails($j1_details['job_status']);
				$old_j2_status 	= $job->getJobStatusDetails($j2_details['job_status']);
				
				$job->updateJob($j1_details['id'], 'job_status', '20');
				$job->updateJob($j2_details['id'], 'job_status', '20');
				
				$job->addJobLog($j1_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$j1_details['id'].' - Confirmed', 'Confirmed', $old_j1_status['details']);
				$job->addJobLog($j2_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$j2_details['id'].' - Confirmed', 'Confirmed', $old_j2_status['details']);
				
				$return_message ="Booking ID ".$_GET['j1_id']." & ".$_GET['j2_id']." - Confirmed<br/>Please note, on confirmation, email notification has not been sent to anyone.";
			}
		elseif($_GET['job_decision'] == 'delete_jobs') // delete jobs
			{
				$job = new job();
				$job->deleteJobs($_GET['j1_id'], $_GET['j2_id']);
				$return_message ="Booking ID ".$_GET['j1_id']." &  ".$_GET['j2_id']."- DELETED<br/> If this is part of a return booking, then the other booking has been changed to One Way booking";
			}
			
			
			
		else if($_GET['job_decision']=='confirm_j1') // confirm job 1
			{
				
				$job 			= new job();
				$job_reference 	= new jobReference();
				$mailer 		= new mailer();
				$user			= new user();
				
				
				$job_details 	= $job->getJobDetails($_GET['j1_id']);
				$old_status 	= $job->getJobStatusDetails($job_details['job_status']);
				$job->updateJob($job_details['id'], 'job_status', '20');
				$job->addJobLog($job_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$job_details['id'].' - Confirmed', 'Confirmed', $old_status['details']);
				$job_ref_details = $job_reference->getJobReferenceDetails($job_details['job_reference_id']);

				if($job_ref_details['bkg_by_id'] == $job_ref_details['pax_id']) // means booking made by and pax are the same
					{
						$user_details = $user->getUserDetails($job_ref_details['bkg_by_id']);
						$user_emails = $user->getUserEmails($job_ref_details['bkg_by_id']);
						foreach($user_emails as $key => $value)
							{
								$mailer->sendMailToClientForIndividualBooking($_GET['j1_id'], '1', $value, $user_details['title'], $user_details['fname'], $user_details['lname']);
							}
					}
				else // send confirmation email to both booking made by and pax
					{
						//send confirmation to booking made by
						$bkg_made_by 		= $user->getUserDetails($job_ref_details['bkg_by_id']);
						$bkg_made_by_emails = $user->getUserEmails($job_ref_details['bkg_by_id']);
						foreach($bkg_made_by_emails as $key => $value)
							{
								$mailer->sendMailToClientForIndividualBooking($_GET['j1_id'], '1',  $value, $bkg_made_by['title'], $bkg_made_by['fname'], $bkg_made_by['lname']);
							}
						//send confirmation to pax	
						$pax 		= $user->getUserDetails($job_ref_details['pax_id']);
						$pax_emails = $user->getUserEmails($job_ref_details['pax_id']);
						foreach($pax_emails as $key => $value)
							{
								$mailer->sendMailToClientForIndividualBooking($_GET['j1_id'], '1',  $value, $pax['title'], $pax['fname'], $pax['lname']);
							}

					}
				$return_message ="Booking ID ".$_GET['j1_id']." - Confirmed.<br/>Confirmation emails have been sent to both booking made by and pax.";
			}

		elseif($_GET['job_decision'] == 'decline_j1') // decline job 1
			{
				$job 			= new job();
				$job_reference 	= new jobReference();
				$mailer 		= new mailer();
				$user			= new user();
				
				
				$job_details 	= $job->getJobDetails($_GET['j1_id']);
				$old_status 	= $job->getJobStatusDetails($job_details['job_status']);
				$job->updateJob($job_details['id'], 'job_status', '90');
				$job->addJobLog($job_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$job_details['id'].' - Confirmed', 'Confirmed', $old_status['details']);
				$job_ref_details = $job_reference->getJobReferenceDetails($job_details['job_reference_id']);

				if($job_ref_details['bkg_by_id'] == $job_ref_details['pax_id']) // means booking made by and pax are the same
					{
						$user_details = $user->getUserDetails($job_ref_details['bkg_by_id']);
						$user_emails = $user->getUserEmails($job_ref_details['bkg_by_id']);
						foreach($user_emails as $key => $value)
							{
								$mailer->sendMailToClientForIndividualBooking($_GET['j1_id'], '2', $value, $user_details['title'], $user_details['fname'], $user_details['lname']);
							}
					}
				else // send confirmation email to both booking made by and pax
					{
						//send confirmation to booking made by
						$bkg_made_by 		= $user->getUserDetails($job_ref_details['bkg_by_id']);
						$bkg_made_by_emails = $user->getUserEmails($job_ref_details['bkg_by_id']);
						foreach($bkg_made_by_emails as $key => $value)
							{
								$mailer->sendMailToClientForIndividualBooking($_GET['j1_id'], '2',  $value, $bkg_made_by['title'], $bkg_made_by['fname'], $bkg_made_by['lname']);
							}
						//send confirmation to pax	
						$pax 		= $user->getUserDetails($job_ref_details['pax_id']);
						$pax_emails = $user->getUserEmails($job_ref_details['pax_id']);
						foreach($pax_emails as $key => $value)
							{
								$mailer->sendMailToClientForIndividualBooking($_GET['j1_id'], '2',  $value, $pax['title'], $pax['fname'], $pax['lname']);
							}

					}
				$return_message ="Booking ID ".$_GET['j1_id']." - Declined<br/>Booking rejected notification emails have been sent to both booking made by and pax.";
			}
		elseif($_GET['job_decision'] == 'cancel_j1') // cancel job 1
			{
				$job = new job();
				
				$job_details 	= $job->getJobDetails($_GET['j1_id']);
				$old_status 	= $job->getJobStatusDetails($job_details['job_status']);
				$job->updateJob($job_details['id'], 'job_status', '100');
				$job->addJobLog($job_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$job_details['id'].' - Cancelled', 'Cancelled', $old_status['details']);
				$return_message ="Booking ID ".$_GET['j1_id']." - Cancelled<br/>Please note, on cancellation, email notification is not sent to anyone.";
			}
		elseif($_GET['job_decision'] == 'clone_j1') // clone job 1
			{
				$job = new job();
				$job_details = $job->getJobDetails($_GET['j1_id']);
				$cloned_job_id = $job->cloneIndividualJob($_GET['j1_id']);
				$job->addJobLog($job_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$job_details['id'].' - CLONED', 'New Job id '.$cloned_job_id.'', '');
				$return_message ="Booking ID ".$_GET['j1_id']." - CLONED<br/>New Cloned Booking ID - ".$cloned_job_id."";
				$data1 = $cloned_job_id;
			}
		elseif ($_GET['job_decision'] == 'rev_clone_j1') // reverse clone job 1
			{
				$job = new job();
				$job_details = $job->getJobDetails($_GET['j1_id']);
				$re_cloned_job_id = $job->ReverseCloneIndividualJob($_GET['j1_id']);
				$job->addJobLog($job_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$job_details['id'].' - CLONED', 'New Job id '.$re_cloned_job_id.'', '');
				$return_message ="Booking ID ".$_GET['j1_id']." - REVERSE CLONED<br/>New Cloned Booking ID - ".$re_cloned_job_id."";
				$data1 = $re_cloned_job_id;
			}
		elseif($_GET['job_decision'] == 'reactivate_j1') // reactivate job 1
			{
				$job = new job();
				
				$job_details 	= $job->getJobDetails($_GET['j1_id']);
				$old_status 	= $job->getJobStatusDetails($job_details['job_status']);
				$job->updateJob($job_details['id'], 'job_status', '20');
				$job->addJobLog($job_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$job_details['id'].' - Confirmed', 'Confirmed', $old_status['details']);
				$return_message ="Booking ID ".$_GET['j1_id']." - Confirmed<br/>Please note, on confirmation, email notification is not sent to anyone.";
			}
		elseif($_GET['job_decision'] == 'delete_j1') // delete job 1
			{
				$job = new job();
				$job->deleteAJob($_GET['j1_id']);
				$return_message ="Booking ID ".$_GET['j1_id']." - DELETED<br/> If this is part of a return booking, then the other booking has been changed to One Way booking";
			}
		elseif($_GET['job_decision']=='confirm_j2') // confirm job 2
			{
				
				$job 			= new job();
				$job_reference 	= new jobReference();
				$mailer 		= new mailer();
				$user			= new user();
				
				
				$job_details 	= $job->getJobDetails($_GET['j2_id']);
				$old_status 	= $job->getJobStatusDetails($job_details['job_status']);
				$job->updateJob($job_details['id'], 'job_status', '20');
				$job->addJobLog($job_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$job_details['id'].' - Confirmed', 'Confirmed', $old_status['details']);
				$job_ref_details = $job_reference->getJobReferenceDetails($job_details['job_reference_id']);

				if($job_ref_details['bkg_by_id'] == $job_ref_details['pax_id']) // means booking made by and pax are the same
					{
						$user_details = $user->getUserDetails($job_ref_details['bkg_by_id']);
						$user_emails = $user->getUserEmails($job_ref_details['bkg_by_id']);
						foreach($user_emails as $key => $value)
							{
								$mailer->sendMailToClientForIndividualBooking($_GET['j1_id'], '1', $value, $user_details['title'], $user_details['fname'], $user_details['lname']);
							}
					}
				else // send confirmation email to both booking made by and pax
					{
						//send confirmation to booking made by
						$bkg_made_by 		= $user->getUserDetails($job_ref_details['bkg_by_id']);
						$bkg_made_by_emails = $user->getUserEmails($job_ref_details['bkg_by_id']);
						foreach($bkg_made_by_emails as $key => $value)
							{
								$mailer->sendMailToClientForIndividualBooking($_GET['j2_id'], '1',  $value, $bkg_made_by['title'], $bkg_made_by['fname'], $bkg_made_by['lname']);
							}
						//send confirmation to pax	
						$pax 		= $user->getUserDetails($job_ref_details['pax_id']);
						$pax_emails = $user->getUserEmails($job_ref_details['pax_id']);
						foreach($pax_emails as $key => $value)
							{
								$mailer->sendMailToClientForIndividualBooking($_GET['j2_id'], '1',  $value, $pax['title'], $pax['fname'], $pax['lname']);
							}

					}
				$return_message ="Booking ID ".$_GET['j2_id']." - Confirmed.<br/>Confirmation emails have been sent to both booking made by and pax.";
			}

		elseif($_GET['job_decision'] == 'decline_j2') // decline job 2
			{
				$job 			= new job();
				$job_reference 	= new jobReference();
				$mailer 		= new mailer();
				$user			= new user();
				
				
				$job_details 	= $job->getJobDetails($_GET['j2_id']);
				$old_status 	= $job->getJobStatusDetails($job_details['job_status']);
				$job->updateJob($job_details['id'], 'job_status', '90');
				$job->addJobLog($job_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$job_details['id'].' - Confirmed', 'Confirmed', $old_status['details']);
				$job_ref_details = $job_reference->getJobReferenceDetails($job_details['job_reference_id']);

				if($job_ref_details['bkg_by_id'] == $job_ref_details['pax_id']) // means booking made by and pax are the same
					{
						$user_details = $user->getUserDetails($job_ref_details['bkg_by_id']);
						$user_emails = $user->getUserEmails($job_ref_details['bkg_by_id']);
						foreach($user_emails as $key => $value)
							{
								$mailer->sendMailToClientForIndividualBooking($_GET['j2_id'], '2', $value, $user_details['title'], $user_details['fname'], $user_details['lname']);
							}
					}
				else // send confirmation email to both booking made by and pax
					{
						//send confirmation to booking made by
						$bkg_made_by 		= $user->getUserDetails($job_ref_details['bkg_by_id']);
						$bkg_made_by_emails = $user->getUserEmails($job_ref_details['bkg_by_id']);
						foreach($bkg_made_by_emails as $key => $value)
							{
								$mailer->sendMailToClientForIndividualBooking($_GET['j2_id'], '2',  $value, $bkg_made_by['title'], $bkg_made_by['fname'], $bkg_made_by['lname']);
							}
						//send confirmation to pax	
						$pax 		= $user->getUserDetails($job_ref_details['pax_id']);
						$pax_emails = $user->getUserEmails($job_ref_details['pax_id']);
						foreach($pax_emails as $key => $value)
							{
								$mailer->sendMailToClientForIndividualBooking($_GET['j2_id'], '2',  $value, $pax['title'], $pax['fname'], $pax['lname']);
							}

					}
				$return_message ="Booking ID ".$_GET['j2_id']." - Declined<br/>Booking rejected notification emails have been sent to both booking made by and pax.";
			}
		elseif($_GET['job_decision'] == 'cancel_j2') // cancel job 2
			{
				$job = new job();
				
				$job_details 	= $job->getJobDetails($_GET['j2_id']);
				$old_status 	= $job->getJobStatusDetails($job_details['job_status']);
				$job->updateJob($job_details['id'], 'job_status', '100');
				$job->addJobLog($job_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$job_details['id'].' - Cancelled', 'Cancelled', $old_status['details']);
				$return_message ="Booking ID ".$_GET['j2_id']." - Cancelled<br/>Please note, on cancellation, email notification is not sent to anyone.";
			}
		elseif($_GET['job_decision'] == 'clone_j2') // clone job 2
			{
				$job = new job();
				$job_details = $job->getJobDetails($_GET['j2_id']);
				$cloned_job_id = $job->cloneIndividualJob($_GET['j2_id']);
				$job->addJobLog($job_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$job_details['id'].' - CLONED', 'New Job id '.$cloned_job_id.'', '');
				$return_message ="Booking ID ".$_GET['j2_id']." - CLONED<br/>New Cloned Booking ID - ".$cloned_job_id."";
				$data1 = $cloned_job_id;
			}
		elseif ($_GET['job_decision'] == 'rev_clone_j2') // reverse clone job 2
			{
				$job = new job();
				$job_details = $job->getJobDetails($_GET['j2_id']);
				$re_cloned_job_id = $job->ReverseCloneIndividualJob($_GET['j2_id']);
				$job->addJobLog($job_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$job_details['id'].' - CLONED', 'New Job id '.$re_cloned_job_id.'', '');
				$return_message ="Booking ID ".$_GET['j2_id']." - REVERSE CLONED<br/>New Cloned Booking ID - ".$re_cloned_job_id."";
				$data1 = $re_cloned_job_id;
			}
		elseif($_GET['job_decision'] == 'reactivate_j2') // reactivate job 2
			{
				$job = new job();
				
				$job_details 	= $job->getJobDetails($_GET['j2_id']);
				$old_status 	= $job->getJobStatusDetails($job_details['job_status']);
				$job->updateJob($job_details['id'], 'job_status', '20');
				$job->addJobLog($job_details['id'], $_SESSION['USER_ID'], 'Bkg ID '.$job_details['id'].' - Confirmed', 'Confirmed', $old_status['details']);
				$return_message ="Booking ID ".$_GET['j2_id']." - Confirmed<br/>Please note, on confirmation, email notification is not sent to anyone.";
			}
		elseif($_GET['job_decision'] == 'delete_j2') // delete job 2
			{
				$job = new job();
				$job->deleteAJob($_GET['j2_id']);
				$return_message ="Booking ID ".$_GET['j2_id']." - DELETED<br/> If this is part of a return booking, then the other booking has been changed to One Way booking";
			}
		elseif($_GET['job_decision'] == 'resend_j1') // resend job 1 details to driver
			{
				$job_details 	= $job->getJobDetails($_GET['j1_id']);
				if($job_details['driver_id'] != '' || $job_details['driver_id'] != 0)
					{
						$mailer->sendJobDetailsToDriver($job_details['id'], $job_details['driver_id'], $job_details['driver_id']);	
						$job->addJobLog($job_details['id'], $_SESSION['USER_ID'], 'Details resent to driver', '', '');
						$return_message ="Booking ID ".$_GET['j1_id']." - details resent to driver. Thanks";
					}
				else
					{
						$return_message ="ERROR!!!! No record of job being accepted or rogered found. Please get the accepted first. Thanks";
					}
			}
			
		elseif($_GET['job_decision'] == 'resend_j2') // resend job 2 details to driver
			{
				$job_details 	= $job->getJobDetails($_GET['j2_id']);
				if($job_details['driver_id'] != '' || $job_details['driver_id'] != 0)
					{
						$mailer->sendJobDetailsToDriver($job_details['id'], $job_details['driver_id'], $job_details['driver_id']);
						$job->addJobLog($job_details['id'], $_SESSION['USER_ID'], 'Details resent to driver', '', '');				
						$return_message ="Booking ID ".$_GET['j2_id']." - details resent to driver. Thank you";
					}
				else
					{
						$return_message ="ERROR!!!! No record of job being accepted or rogered found. Please get the accepted first. Thank you";
					}
			}	
		elseif($_GET['job_decision'] == 'pay_j1') // Pay for job 1
			{
				$job = new job();
				$user = new User();
				$transaction = new Transaction();
				$account = new Account();
				
				$j1_details = $job->getJobDetails($_GET['j1_id']);
				$old_driver_status = $job->getDriverStatusDetails($j1_details['driver_status']);
				
				$query1 = "SELECT MAX(id) as id FROM job__driver where job_id='".$_GET['j1_id']."' AND  accepted_by!='0'";
				$database = new database;
				$result1 = $database->query($query1);
				$row1 = mysql_fetch_array($result1);
				
				$query2 = "SELECT * from job__driver where id='".$row1['id']."'";
				$result2 = $database->query($query2);
				$row2 = mysql_fetch_array($result2);
				$accepted_by = $user->getUserDetails($row2['accepted_by']);
				$accepted_by_mobile = $user->getUserPhones($row2['accepted_by']);
				
				//get entered by
				$entered_by 		= 	$user->getUserDetails($_SESSION['USER_ID']);
				$entered_by_name 	= 	''.$entered_by['fname'].' '.$entered_by['lname'].'';
				
				$acc_cr_label 		= 	'drivers__accounts_'.$row2['accepted_by'].'';
				$acc_dr_label 		= 	'acc__general';

				$description_debit_entry = "Paid for Booking ID # ".$_GET['j1_id']." (Acc Credited - ".$acc_cr_label.")";
				$description_credit_entry = "Paid for Booking ID # ".$_GET['j1_id']." (Acc Debited - ".$acc_dr_label.")";
				
				
				//Add this in transaction table
				$this_trx_id = $transaction->newTransaction($acc_dr_label, $acc_cr_label);
				//Account entry of debit in Allied General Account
				$acc_dr_entry_id = $account->debitEntry($this_trx_id, $entered_by_name, $description_debit_entry, $acc_dr_label,  $acc_cr_label, $row2['accepted_amount']);
				//Account entry of credit in drivers table	
				$acc_cr_entry_id = $account->creditEntry($this_trx_id, $entered_by_name, $description_credit_entry, $acc_cr_label, $acc_dr_label, $row2['accepted_amount']);
				
				//add this to job__driver table
				$job->addNewDriverToJob($_GET['j1_id'], '', '', '', '', '', '', ''.$row2['accepted_by'].'', ''.$row2['accepted_amount'].'', '', '', ''); // Add this to job__driver table
				//change the driver status of the job to paid
				$job->updateJob($_GET['j1_id'], 'driver_status', '5'); // PAID
				$job->updateJob($_GET['j1_id'], 'driver_id', $row2['accepted_by']); // Put this driver in job table
				$job->updateJob($_GET['j1_id'], 'driver_price', $row2['accepted_amount']); // Put the driver price in the job table
				//make a log entry in the job log table
				$job->addJobLog($_GET['j1_id'], $entered_by_name, 'Paid $'.$row2['accepted_amount'].' for Bkg ID '.$_GET['j1_id'].' to '.$accepted_by['fname'].' '.$accepted_by['lname'].' vide Trx ID -'.$this_trx_id.'', 'PAID', $old_driver_status['details']);
				$return_message ="Booking ID ".$_GET['j1_id']." - PAID<br/>";
			}
		elseif($_GET['job_decision'] == 'pay_j2') // Pay for job 2
			{
				$job = new job();
				$user = new User();
				$transaction = new Transaction();
				$account = new Account();
				
				$j2_details = $job->getJobDetails($_GET['j2_id']);
				$old_driver_status = $job->getDriverStatusDetails($j2_details['driver_status']);
				
				$query1 = "SELECT MAX(id) as id FROM job__driver where job_id='".$_GET['j2_id']."' AND  accepted_by!='0'";
				$database = new database;
				$result1 = $database->query($query1);
				$row1 = mysql_fetch_array($result1);
				
				$query2 = "SELECT * from job__driver where id='".$row1['id']."'";
				$result2 = $database->query($query2);
				$row2 = mysql_fetch_array($result2);
				$accepted_by = $user->getUserDetails($row2['accepted_by']);
				$accepted_by_mobile = $user->getUserPhones($row2['accepted_by']);
				
				//get entered by
				$entered_by 		= 	$user->getUserDetails($_SESSION['USER_ID']);
				$entered_by_name 	= 	''.$entered_by['fname'].' '.$entered_by['lname'].'';
				
				$acc_cr_label 		= 	'drivers__accounts_'.$row2['accepted_by'].'';
				$acc_dr_label 		= 	'acc__general';

				$description_debit_entry = "Paid for Booking ID # ".$_GET['j2_id']." (Acc Credited - ".$acc_cr_label.")";
				$description_credit_entry = "Paid for Booking ID # ".$_GET['j2_id']." (Acc Debited - ".$acc_dr_label.")";
				
				
				//Add this in transaction table
				$this_trx_id = $transaction->newTransaction($acc_dr_label, $acc_cr_label);
				//Account entry of debit in Allied General Account
				$acc_dr_entry_id = $account->debitEntry($this_trx_id, $entered_by_name, $description_debit_entry, $acc_dr_label,  $acc_cr_label, $row2['accepted_amount']);
				//Account entry of credit in drivers table	
				$acc_cr_entry_id = $account->creditEntry($this_trx_id, $entered_by_name, $description_credit_entry, $acc_cr_label, $acc_dr_label, $row2['accepted_amount']);
				
				//add this to job__driver table
				$job->addNewDriverToJob($_GET['j2_id'], '', '', '', '', '', '', ''.$row2['accepted_by'].'', ''.$row2['accepted_amount'].'', '', '', ''); // Add this to job__driver table
				//change the driver status of the job to paid
				$job->updateJob($_GET['j2_id'], 'driver_status', '5'); // PAID
				$job->updateJob($_GET['j2_id'], 'driver_id', $row2['accepted_by']); // Put this driver in job table
				$job->updateJob($_GET['j2_id'], 'driver_price', $row2['accepted_amount']); // Put the driver price in the job table
				//make a log entry in the job log table
				$job->addJobLog($_GET['j2_id'], $entered_by_name, 'Paid $'.$row2['accepted_amount'].' for Bkg ID '.$_GET['j2_id'].' to '.$accepted_by['fname'].' '.$accepted_by['lname'].' vide Trx ID -'.$this_trx_id.'', 'PAID', $old_driver_status['details']);
				$return_message ="Booking ID ".$_GET['j2_id']." - PAID<br/>";
			}
		elseif($_GET['job_decision'] == 'tear_off_j1') // Tear off driver statuses for job 1
			{
				$database = new database;
				$user = new User();
				//get entered by
				$entered_by 		= 	$user->getUserDetails($_SESSION['USER_ID']);
				$entered_by_name 	= 	''.$entered_by['fname'].' '.$entered_by['lname'].'';
				
				$j1_details = $job->getJobDetails($_GET['j1_id']);
				$old_driver_status = $job->getDriverStatusDetails($j1_details['driver_status']);
				
				$query = "DELETE FROM job__driver where job_id='".$_GET['j1_id']."' AND  accepted_by!='0'";
				$result = $database->query($query);
				
				$query = "DELETE FROM job__driver where job_id='".$_GET['j1_id']."' AND  offered_to!='0'";
				$result = $database->query($query);
				
				$query = "DELETE FROM job__driver where job_id='".$_GET['j1_id']."' AND  allocated_to!='0'";
				$result = $database->query($query);
				
				
				$job->updateJob($_GET['j1_id'], 'drv_fee', '0.00'); // Make the driver fee 0.00
				$job->updateJob($_GET['j1_id'], 'driver_status', '0'); // TEAR OFF
				$job->updateJob($_GET['j1_id'], 'driver_id', '0'); // Put this driver in job table
				$job->updateJob($_GET['j1_id'], 'driver_price', '0.00'); // Put the driver price in the job table
				//make a log entry in the job log table
				$job->addJobLog($_GET['j1_id'], $entered_by_name, 'DRIVER STATUS - RESET', 'NIL', $old_driver_status['details']);
				$return_message ="Booking ID ".$_GET['j1_id']." - DRIVER STATUS - RESET<br/>";
			}
		elseif($_GET['job_decision'] == 'tear_off_j2') // Tear off driver statuses for job 2
			{
				$database = new database;
				$user = new User();
				//get entered by
				$entered_by 		= 	$user->getUserDetails($_SESSION['USER_ID']);
				$entered_by_name 	= 	''.$entered_by['fname'].' '.$entered_by['lname'].'';
				
				$j2_details = $job->getJobDetails($_GET['j2_id']);
				$old_driver_status = $job->getDriverStatusDetails($j2_details['driver_status']);
				
				$query = "DELETE FROM job__driver where job_id='".$_GET['j2_id']."' AND  accepted_by!='0'";
				$result = $database->query($query);
				
				$query = "DELETE FROM job__driver where job_id='".$_GET['j2_id']."' AND  offered_to!='0'";
				$result = $database->query($query);
				
				$query = "DELETE FROM job__driver where job_id='".$_GET['j2_id']."' AND  allocated_to!='0'";
				$result = $database->query($query);
				
				$job->updateJob($_GET['j2_id'], 'drv_fee', '0.00'); // Make the driver fee 0.00
				$job->updateJob($_GET['j2_id'], 'driver_status', '0'); // TEAR OFF
				$job->updateJob($_GET['j2_id'], 'driver_id', '0'); // Put this driver in job table
				$job->updateJob($_GET['j2_id'], 'driver_price', '0.00'); // Put the driver price in the job table
				//make a log entry in the job log table
				$job->addJobLog($_GET['j2_id'], $entered_by_name, 'DRIVER STATUS - RESET', 'NIL', $old_driver_status['details']);
				$return_message ="Booking ID ".$_GET['j2_id']." - DRIVER STATUS - RESET<br/>";
			}
		elseif($_GET['job_decision'] == 'pay_extra_j1') // Pay extra to driver for job 1
			{
				$database = new database;
				$transaction = new Transaction();
				$account = new Account();
				$user = new User();
				
				$entered_by 		= 	$user->getUserDetails($_SESSION['USER_ID']);
				$entered_by_name 	= 	''.$entered_by['fname'].' '.$entered_by['lname'].'';
				//First check from job__driver table if there is any driver paid to for this job
				$query1 = "SELECT MAX(id) as id FROM job__driver where job_id='".$_GET['j1_id']."' AND  paid_to!='0'";
				$result1 = $database->query($query1);
				$row1 = mysql_fetch_array($result1);
				if($row1['id'] != '') // which driver has the money gone to
					{
						$query2 = "SELECT * from job__driver where id='".$row1['id']."'";
						$result2 = $database->query($query2);
						$row2 = mysql_fetch_array($result2);
						$driver_id = $row2['paid_to'];
					}
				else
					{
						$query3 = "SELECT MAX(id) as id FROM job__driver where job_id='".$_GET['j1_id']."' AND  accepted_by!='0'";
						$result3 = $database->query($query3);
						$row3 = mysql_fetch_array($result3);
						if($row3['id'] != '') // which driver has accepted the job
							{
								$query4 = "SELECT * from job__driver where id='".$row3['id']."'";
								$result4 = $database->query($query4);
								$row4 = mysql_fetch_array($result4);
								$driver_id = $row4['accepted_by'];
							}
					}
				
				if($driver_id != '')
					{
						$driver_details = $user->getUserDetails($driver_id);
						$job_details = $job->getJobDetails($_GET['j1_id']);
						//get the driver fee from driver table and increase it by this new amount
						$new_driver_fee = $job_details['drv_fee'] + $_GET['j1_driver_ex_price'];
						$job->updateJob($_GET['j1_id'], 'drv_fee', ''.$new_driver_fee.''); // Make the driver fee 0.00
						//get entered by
						$entered_by 		= 	$user->getUserDetails($_SESSION['USER_ID']);
						$entered_by_name 	= 	''.$entered_by['fname'].' '.$entered_by['lname'].'';
						
						$query5 = "INSERT INTO job__driver_extra_pay (job_id, driver_id, notes, amount)
									VALUES
									('".$_GET['j1_id']."', '".$driver_id."', '".$_GET['j1_driver_ex_notes']."', '".$_GET['j1_driver_ex_price']."')";
						$result5 = $database->query($query5);
						//Add job log
						$job->addJobLog($_GET['j1_id'], $entered_by_name, 'DRIVER PAID EXTRA', '$'.$_GET['j1_driver_ex_price'].' paid to driver for '.$_GET['j1_driver_ex_notes'].'', 'NIL');
						$job->addJobLog($_GET['j1_id'], $entered_by_name, 'DRIVER TOTAL FEE CHANGED', ''.$new_driver_fee.'', ''.$job_details['drv_fee'].'');
						$acc_cr_label 		= 	'drivers__accounts_'.$driver_id.'';
						$acc_dr_label 		= 	'acc__general';

						$description_debit_entry = "Extra Paid for Booking ID # ".$_GET['j1_id']." (Acc Credited - ".$acc_cr_label.")";
						$description_credit_entry = "Extra Paid for Booking ID # ".$_GET['j1_id']." (Acc Debited - ".$acc_dr_label.")";
						//Add this in transaction table
						$this_trx_id = $transaction->newTransaction($acc_dr_label, $acc_cr_label);
						//Account entry of debit in Allied General Account
						$acc_dr_entry_id = $account->debitEntry($this_trx_id, $entered_by_name, $description_debit_entry, $acc_dr_label,  $acc_cr_label, $_GET['j1_driver_ex_price']);
						//Account entry of credit in drivers table	
						$acc_cr_entry_id = $account->creditEntry($this_trx_id, $entered_by_name, $description_credit_entry, $acc_cr_label, $acc_dr_label, $_GET['j1_driver_ex_price']);

						$return_message ="Paid $".$_GET['j1_driver_ex_price']." Extra to ".$driver_details['fname']." ".$driver_details['lname']."<br/>";
					}
				else
					{
						$return_message ="WARNING: No Driver found.<br/>Please check if any driver has accpeted this job or any driver has been paid for this job earlier. Otherwise you cannot make extra payment to the driver using this feature. Thanks";
					}
			}
		elseif($_GET['job_decision'] == 'pay_extra_j2') // Pay extra to driver for job 2
			{
				$database = new database;
				$transaction = new Transaction();
				$account = new Account();
				$user = new User();
				
				$entered_by 		= 	$user->getUserDetails($_SESSION['USER_ID']);
				$entered_by_name 	= 	''.$entered_by['fname'].' '.$entered_by['lname'].'';
				//First check from job__driver table if there is any driver paid to for this job
				$query1 = "SELECT MAX(id) as id FROM job__driver where job_id='".$_GET['j2_id']."' AND  paid_to!='0'";
				$result1 = $database->query($query1);
				$row1 = mysql_fetch_array($result1);
				if($row1['id'] != '')
					{
						$query2 = "SELECT * from job__driver where id='".$row1['id']."'";
						$result2 = $database->query($query2);
						$row2 = mysql_fetch_array($result2);
						$driver_id = $row2['paid_to'];
					}
				else
					{
						$query3 = "SELECT MAX(id) as id FROM job__driver where job_id='".$_GET['j2_id']."' AND  accepted_by!='0'";
						$result3 = $database->query($query3);
						$row3 = mysql_fetch_array($result3);
						if($row3['id'] != '')
							{
								$query4 = "SELECT * from job__driver where id='".$row3['id']."'";
								$result4 = $database->query($query4);
								$row4 = mysql_fetch_array($result4);
								$driver_id = $row4['accepted_by'];
							}
					}
				
				if($driver_id != '')
					{
						$driver_details = $user->getUserDetails($driver_id);
						$job_details = $job->getJobDetails($_GET['j2_id']);
						//get the driver fee from driver table and increase it by this new amount
						$new_driver_fee = $job_details['drv_fee'] + $_GET['j2_driver_ex_price'];
						$job->updateJob($_GET['j2_id'], 'drv_fee', ''.$new_driver_fee.''); // Make the driver fee 0.00
						//get entered by
						$entered_by 		= 	$user->getUserDetails($_SESSION['USER_ID']);
						$entered_by_name 	= 	''.$entered_by['fname'].' '.$entered_by['lname'].'';
						
						$query5 = "INSERT INTO job__driver_extra_pay (job_id, driver_id, notes, amount)
									VALUES
									('".$_GET['j2_id']."', '".$driver_id."', '".$_GET['j2_driver_ex_notes']."', '".$_GET['j2_driver_ex_price']."')";
						$result5 = $database->query($query5);
						//Add job log
						$job->addJobLog($_GET['j2_id'], $entered_by_name, 'DRIVER PAID EXTRA', '$'.$_GET['j2_driver_ex_price'].' paid to driver for '.$_GET['j2_driver_ex_notes'].'', 'NIL');
						$job->addJobLog($_GET['j2_id'], $entered_by_name, 'DRIVER TOTAL FEE CHANGED', ''.$new_driver_fee.'', ''.$job_details['drv_fee'].'');
						
						$acc_cr_label 		= 	'drivers__accounts_'.$driver_id.'';
						$acc_dr_label 		= 	'acc__general';

						$description_debit_entry = "Extra Paid for Booking ID # ".$_GET['j2_id']." (Acc Credited - ".$acc_cr_label.")";
						$description_credit_entry = "Extra Paid for Booking ID # ".$_GET['j2_id']." (Acc Debited - ".$acc_dr_label.")";
						//Add this in transaction table
						$this_trx_id = $transaction->newTransaction($acc_dr_label, $acc_cr_label);
						//Account entry of debit in Allied General Account
						$acc_dr_entry_id = $account->debitEntry($this_trx_id, $entered_by_name, $description_debit_entry, $acc_dr_label,  $acc_cr_label, $_GET['j2_driver_ex_price']);
						//Account entry of credit in drivers table	
						$acc_cr_entry_id = $account->creditEntry($this_trx_id, $entered_by_name, $description_credit_entry, $acc_cr_label, $acc_dr_label, $_GET['j2_driver_ex_price']);

						$return_message ="Paid $".$_GET['j2_driver_ex_price']." Extra to ".$driver_details['fname']." ".$driver_details['lname']."<br/>";
					}
				else
					{
						$return_message ="WARNING: No Driver found.<br/>Please check if any driver has accpeted this job or any driver has been paid for this job earlier. Otherwise you cannot make extra payment to the driver using this feature. Thanks";
					}
			}
		elseif($_GET['job_decision'] == 'delete_j1_driver_offer') // Delete offer to driver for j1
			{
				$database = new database;
				$query = "DELETE FROM job__driver where id='".$_GET['row_id']."'";
				$result = $database->query($query);
			}
		elseif($_GET['job_decision'] == 'delete_j2_driver_offer') // Delete offer to driver for j1
			{
				$database = new database;
				$query = "DELETE FROM job__driver where id='".$_GET['row_id']."'";
				$result = $database->query($query);
			}
	}

$c = array('data' => ''.$return_message.'', 'data1' => ''.$data1.'', 'data2' => ''.$data2.'');
$json = json_encode($c); 
echo $json;
?>