<?php
ini_set("display_errors", 0);
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "chargeAccount.php");
require_once(CLASSES_PATH . "job.php");
require_once(CLASSES_PATH . "jobReference.php");
require_once(CLASSES_PATH . "address.php");
require_once(CLASSES_PATH . "mailer.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "vehicle.php");
session_start();
$database = new database;
$charge_acc = new chargeAccount();
$user = new user();
$job = new Job();
$job_ref = new jobReference();
$address = new Address();
//First do some validation checks

$result_session_check ='';
$result_charge_acc_check = '';
$result_contact_check = '';
$result_pax_check = '';
$result_j1_dt_time_check = '';
$result_j2_dt_time_check = '';

//check if session has been timed out
if($_SESSION['ROLE_ID'] != '')
{
	$result_session_check =1;
	$timeout_msg='';
	//Check 1, see if charge account id or charge account name is provided
	if($_POST['charge_acc'] != '' || $_POST['account_name'] != '')
		{$result_charge_acc_check = 1; }
	else{$error_message .= "Check - <b>No Charge Account Selected</b><br/>";}
	//Check 2, see if contact or first name is provided
	if($_POST['acc_contact'] != '' || $_POST['bkg_by_fname'] != '')
		{$result_contact_check = 1; }
	else{$error_message .= "Check - <b>No Account Contact Selected</b><br/>";}
	//Check 3, see if pax or pax first name is provided
	if($_SESSION['ROLE_ID'] == '1') 
		{
			if($_POST['acc_passenger'] != '' || $_POST['new_pax_fname'] != '')
				{$result_pax_check = 1; }
			else{$error_message .= "Check - <b>No Passenger Selected</b><br/>";}
		}
	else
		{
			$result_pax_check = 1;
		}
	//Check 4, j1 date and time provided
	if($_POST['j1_date'] != '' && $_POST['j1_time'] != '')
		{$result_j1_dt_time_check = 1; }
	else{$error_message .= "Check - <b>Journey 1 Date or Time - blank</b><br/>";}
	//Check 5, j1 date and time provided
	if($_POST['job_type'] == '1')
		{
			{$result_j2_dt_time_check = 1; }
		}
	elseif($_POST['job_type'] == '2')
		{
			if($_POST['j2_date'] != '' && $_POST['j2_time'] != '')
				{$result_j2_dt_time_check = 1;}
			else{$error_message .= "Check -<b>Return Journey Date or Time - blank</b><br/>";}
		}
}
else
{
	$error_message .= "Check - <b>Apologies, you’ve taken longer than 24 minutes to complete the booking and for security reasons the system has automatically logged you out. Please click OK and login again and input the booking in under 24 minutes</b><br/>";
	$timeout_msg = "timeout"; 
}

if($result_session_check == '1' && $result_charge_acc_check == '1' && $result_contact_check == '1' && $result_pax_check == '1' && $result_j1_dt_time_check == '1' && $result_j2_dt_time_check == '1')
	{
		//------------ADD NEW CHARGE ACC, CONTACT AND PASSENGER IF REQUIRED-------------------------------------------------------------------//
		if($_POST['charge_acc'] != '') //means charge account was selected from drop down list
			{
				$charge_acc_id = $_POST['charge_acc'];
			}
		else // means new charge account entered
			{
				$new_account_name = mysql_real_escape_string($_POST['account_name']);
				if($new_account_name != '')
					{
						$charge_acc_id = $charge_acc->addChargeAccount($_SESSION['USER_ID'], $_POST['account_type'], $new_account_name, $_POST['myob_no']);
					}
				if($charge_acc_id !='')
					{
						$additional_message .= "New Charge Account Created -  <b>".$new_account_name."</b><br/>";
					}
			}
		if($_POST['acc_contact'] != '')  //means booking made by selected from drop down list
			{
				$contact_id = $_POST['acc_contact'];
			}
		else	 // means new booking made by entered
			{
				$new_bkg_by_fname 		= mysql_real_escape_string($_POST['bkg_by_fname']);
				$new_bkg_by_lname 		= mysql_real_escape_string($_POST['bkg_by_lname']);
				$new_bkg_by_password 	= mysql_real_escape_string($_POST['bkg_by_password']);
				if($new_bkg_by_fname != '')
					{
						$contact_id = $charge_acc->addChargeAccountContact($_POST['bkg_by_title'], $new_bkg_by_fname, $new_bkg_by_lname, $new_bkg_by_password, $_POST['bkg_by_email'], $_POST['bkg_by_mobile'], $_POST['bkg_by_phone'], $_POST['bkg_by_preference'], $charge_acc_id, $_POST['bkg_by_type']);
					}
				if($contact_id !='')
					{
						$additional_message .= "New Contact Created -  <b>".$_POST['bkg_by_title']." ".$new_bkg_by_fname." ".$new_bkg_by_lname."</b><br/>";
					}
			}
		if($_POST['acc_passenger'] != '')  //means pax selected from drop down list
			{
				$pax_id = $_POST['acc_passenger'];
			}
		else	 // means new passenger entered
			{
				if(($_POST['bkg_by_fname'] != $_POST['new_pax_fname']) && ($_POST['bkg_by_lname'] != $_POST['new_pax_lname']))
					{
						$new_pax_fname 		= mysql_real_escape_string($_POST['new_pax_fname']);
						$new_pax_lname 		= mysql_real_escape_string($_POST['new_pax_lname']);
						$new_pax_password 	= mysql_real_escape_string($_POST['new_pax_password']);
						if($new_pax_fname != '')
							{
								$pax_id = $charge_acc->addChargeAccountContact($_POST['new_pax_title'], $new_pax_fname, $new_pax_lname, $new_pax_password, $_POST['new_pax_email'],$_POST['new_pax_mobile'],$_POST['new_pax_phone'], $_POST['new_pax_preference'], $charge_acc_id, $_POST['new_pax_type']);
							}
						if($pax_id !='')
							{
								$additional_message .= "New Pax Created -  <b>".$_POST['new_pax_title']." ".$new_pax_fname." ".$new_pax_lname."</b><br/>";
							}
					}
				else
					{
						$pax_id = $contact_id;
					}
			}
//------------END - ADD NEW CHARGE ACC, CONTACT AND PASSENGER IF REQUIRED-------------------------------------------------------------------//			

		if($_SESSION['ROLE_ID'] == '1')	 	//admin
			{
				$job_status_id = '20'; 		//confirmed
			}
		if($_SESSION['ROLE_ID'] == '3')		//client or pax
			{
				$job_status_id = '10'; 		//Received
			}
		$from_state = $address->getStateName($_POST['j1_state']);
		$to_state = $from_state;
		if($_POST['j1_via_sub'] != '') // if suburb not blank
			{
				$from_via_state = $from_state;
			}
		if($_POST['j1_to_via_sub'] != '') // if suburb not blank
			{
				$to_via_state = $from_state;
			}
		$conf_to_client=($_POST['conf_to_bkg_made_by']=='1')? 1:(($_POST['conf_to_client']==1)? 1:0);
		$conf_to_pax= $_POST['conf_to_pax'];
		//Add values to job reference table
		$job_reference_id = $job_ref->addNewJobReference(
															$_SESSION['USER_ID'],
															$_POST['job_src'],
															$_POST['order_ref'],
															$_POST['acc_type'],
															$_POST['std_id'],
															$_POST['std_title'],
															mysql_real_escape_string($_POST['std_fname']),
															mysql_real_escape_string($_POST['std_lname']),
															'',
															$_POST['std_email'],
															$charge_acc_id,
															$contact_id,
															$pax_id,
															$_POST['job_type'],
															$_POST['charge_mode'],
															$_POST['under_18'],
															'',
															'0',
															$conf_to_client,
															$conf_to_pax
														);
														
		//Add job reference log
		$job_ref->addJobReferenceLog($job_reference_id, $_SESSION['USER_ID'], 'New Job Reference Created', $job_reference_id, 'NIL');
		
		//If On behalf is there
			
			//If student guardian is there
			if($_POST['under_18'] == '1')
				{
					$job_ref->updateJobReferenceTable($job_reference_id, 'under_18', '1');
					$job_ref->updateJobReferenceTable($job_reference_id, 'std_guar_title', $_POST['std_guar_title']);
					$job_ref->updateJobReferenceTable($job_reference_id, 'std_guar_fname', $_POST['std_guar_fname']);
					$job_ref->updateJobReferenceTable($job_reference_id, 'std_guar_lname', $_POST['std_guar_lname']);
					$job_ref->updateJobReferenceTable($job_reference_id, 'std_guar_email', $_POST['std_guar_email']);
					$job_ref->updateJobReferenceTable($job_reference_id, 'std_guar_phone', $_POST['std_guar_phone']);
					$job_ref->updateJobReferenceTable($job_reference_id, 'under_18_email', $_POST['under_18_email']);
				}
				
			//If there is an agent
			if($_POST['has_agent'] == '1')
				{
					$job_ref->updateJobReferenceTable($job_reference_id, 'has_agent', '1');
					$job_ref->updateJobReferenceTable($job_reference_id, 'agents_name', $_POST['agents_name']);
					$job_ref->updateJobReferenceTable($job_reference_id, 'agents_email', $_POST['agents_email']);
				}
			if($_POST['has_destination_contact'] == '1')
				{
					$job_ref->updateJobReferenceTable($job_reference_id, 'has_destination_contact', '1');
					$job_ref->updateJobReferenceTable($job_reference_id, 'destination_contact_name', $_POST['destination_contact_name']);
					$job_ref->updateJobReferenceTable($job_reference_id, 'destination_contact_phone', $_POST['destination_contact_phone']);
					$job_ref->updateJobReferenceTable($job_reference_id, 'destination_contact_email', $_POST['destination_contact_email']);
				}	
				
			//If homestay add this value to homestay
			if($_POST['is_homestay'] == '1')
				{
					$job_ref->updateJobReferenceTable($job_reference_id, 'is_homestay', '1');
					$job_ref->updateJobReferenceTable($job_reference_id, 'homestay_email', $_POST['homestay_email']);
				}
		if($_POST['j1_line_2']!='' || $row9['j1_line_2']!=NULL)
		{
			$j1_full_address=$_POST['j1_line_1'].','.$_POST['j1_line_2'].','.$_POST['j1_sub'].','.$_POST['j1_pc'];
		}
		else
		{
			$j1_full_address=$_POST['j1_line_1'].','.$_POST['j1_sub'].','.$_POST['j1_pc'];
		}
		$j1_lat_long=$job->getlatlong($j1_full_address);
		//add the values to job table
		$j1_id = $job->addNewJob
			(
				$job_reference_id,
				$job_status_id,
				$_POST['j1_date_cap'],
				$_POST['j1_time'],
				mysql_real_escape_string($_POST['j1_flight_no']),
				mysql_real_escape_string($_POST['j1_line_1']),
				mysql_real_escape_string($_POST['j1_line_2']),
				mysql_real_escape_string($_POST['j1_sub']),
				mysql_real_escape_string($_POST['j1_pc']),
				$from_state,
				mysql_real_escape_string($_POST['j1_to_flight_no']),
				mysql_real_escape_string($_POST['j1_to_line_1']),
				mysql_real_escape_string($_POST['j1_to_line_2']),
				mysql_real_escape_string($_POST['j1_to_sub']),
				mysql_real_escape_string($_POST['j1_to_pc']),
				$to_state,
				mysql_real_escape_string($_POST['j1_via_line_1']),
				mysql_real_escape_string($_POST['j1_via_line_2']),
				mysql_real_escape_string($_POST['j1_via_sub']),
				mysql_real_escape_string($_POST['j1_via_pc']),
				$from_via_state,
				mysql_real_escape_string($_POST['j1_to_via_line_1']),
				mysql_real_escape_string($_POST['j1_to_via_line_2']),
				mysql_real_escape_string($_POST['j1_to_via_sub']),
				mysql_real_escape_string($_POST['j1_to_via_pc']),
				$to_via_state,
				$_POST['j1_car_id'],
				$_POST['j1_pax_nos'],
				$_POST['j1_luggage'],
				$_POST['j1_baby_seats'],
				$_POST['j1_booster_seats'],
				$_POST['j1_baby_capsules'],
				'',											//this is driver_status_field (leave blank)
				$_POST['j1_kms'],
				$_POST['j1_fare'],
				$_POST['j1_inter'],
				$_POST['j1_ed'],
				$_POST['j1_wait'],
				$_POST['j1_tolls'],
				$_POST['j1_bs'],
				$_POST['j1_park'],
				$_POST['j1_ah'],
				$_POST['j1_me'],
				$_POST['j1_alc'],
				$_POST['j1_fc'],
				$_POST['j1_oth'],
				$_POST['j1_tot_fare'],
				$_POST['j1_drv_fee'],
				$_POST['j1_oth_exp'],
				$_POST['j1_profit'],
				mysql_real_escape_string($_POST['j1_ext_notes']),
				mysql_real_escape_string($_POST['j1_int_notes']),
				mysql_real_escape_string($_POST['j1_driver_notes']),
				$j1_lat_long['lat'],
				$j1_lat_long['lang']
			);
		if($j1_id != '')
			{
				$return_message .= "<b>Your Booking has been successful</b><br/><br/>";
				$return_message .= "Following bookings has been generated<br/><br/>";
				$return_message .= "<b>BOOKING ID -  ".$j1_id."</b><br/>";
			}
		$query56 = "SELECT * from user where role_id='1'";
		$result56 = $database->query($query56);
		while($row9 = mysql_fetch_array($result56))
		{
			if($row9['device_token']!='' || $row9['device_token']!=NULL)
			{
				$push_title = 'New job '.$j1_id;
				$push_message = 'A new job (Job Id : '.$j1_id.') has been created.';
				$job->commonGooglePushNotification($row9['id'],$push_message,$push_title);
			}
		}
		//Add in Job Log Table
		$job->addJobLog($j1_id, $_SESSION['USER_ID'], 'New job created', $j1_id, '');
		//Add this job in job reference table
		$job_ref->updateJobReferenceTable($job_reference_id, 'j1_id', $j1_id);
		//Add in Job Reference Log Table
		$job_ref->addJobReferenceLog($job_reference_id, $_SESSION['USER_ID'], 'Job 1 Added', $j1_id, 'NIL');
		
		//Allocate, Send of Accept Driver
		if($_POST['j1_driver_status'] == '1' && $_POST['j1_driver'] !='') //Allocate
			{
				$job->updateJob($j1_id, 'driver_status', '1'); // change the driver status to Allocate
				$job->addNewDriverToJob($j1_id, $_POST['j1_driver'], $_POST['j1_driver_price'], '', '', '', '', '', '', '', '', '',''); // Add this to job__driver table
				$driver_details = $user->getUserDetails($_POST['j1_driver']); // get details of this driver
				$job->addJobDriverLog($j1_id, $_SESSION['USER_ID'], 'Job Allocated', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$_POST['j1_driver_price'].'', '');
				$additional_message .= "Booking ID -  ".$j1_id ." allocated to - <b>".$driver_details['fname']." ".$driver_details['lname']." for $ ".$_POST['j1_driver_price']."</b><br/>";
			}
		// For Driver/s offer
		if($_POST['j1_driver_status'] == '2' && !empty($_POST['j1_drivers']) && $_POST['j1_driver_price'] !='') //Send
			{
				$mailer = new mailer();
				$job->updateJob($j1_id, 'driver_status', '2'); // change the driver status to Sent
				
				foreach($_POST['j1_drivers'] as $key => $value)	
					{
						$driver_details = $user->getUserDetails($value); // get details of this driver
						$job->addNewDriverToJob($j1_id, '', '', $value, $_POST['j1_driver_price'], '', '', '', '', '', '', '',''); // Add this to job__driver table
						$job->addJobDriverLog($j1_id, $_SESSION['USER_ID'], 'Job Offered', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$_POST['j1_driver_price'].'', '');
						$mailer->sendJobOfferToDriver($value, $j1_id, $_POST['j1_driver_price'],$_POST['j1_driver_notes']); // Send job offer to the driver
						$additional_message .= "Booking ID -  ".$j1_id ." offered to - <b>".$driver_details['fname']." ".$driver_details['lname']." for $ ".$_POST['j1_driver_price']."</b><br/>";
						if($driver_details['device_token']!='' || $driver_details['device_token']!=NULL)
						{
							$push_title = 'New job '.$j1_id;
							$push_message = 'Job Id : '.$j1_id.' has been offered to you.';
							$job->commonGooglePushNotification($value,$push_message,$push_title);
						}
					}
			}
		// For Group Drivers offer	
		if($_POST['j1_driver_status'] == '2' && !empty($_POST['j1_driver_group']) && $_POST['j1_driver_group'] !='') //Send
			{
				
				$mailer = new mailer();
				$job->updateJob($j1_id, 'driver_status', '2'); // change the driver status to Sent
				
				$query = "SELECT * from drivers__group_drivers where group_id='".$_POST['j1_driver_group']."'";
				$result = $database->query($query);
				while($row = mysql_fetch_array($result))
					{
						$driver_details = $user->getUserDetails($row['user_id']); // get details of this driver
						$job->addNewDriverToJob($j1_id, '', '', $row['user_id'], $_POST['j1_driver_price'], '', '', '', '', '', '', '',''); // Add this to job__driver table
						$job->addJobDriverLog($j1_id, $_SESSION['USER_ID'], 'Job Offered', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$_POST['j1_driver_price'].'', '');
						$mailer->sendJobOfferToDriver($row['user_id'], $j1_id, $_POST['j1_driver_price'],$_POST['j1_driver_notes']); // Send job offer to the driver
						$additional_message .= "Booking ID -  ".$j1_id ." offered to - <b>".$driver_details['fname']." ".$driver_details['lname']." for $ ".$_POST['j1_driver_price']."</b><br/>";
						if($driver_details['device_token']!='' || $driver_details['device_token']!=NULL)
						{
							$push_title = 'New job '.$j1_id;
							$push_message = 'Job Id : '.$j1_id.' has been offered to you.';
							$job->commonGooglePushNotification($row['user_id'],$push_message,$push_title);
						}
					}
			}
		if($_POST['j1_driver_status'] == '3' && !empty($_POST['j1_driver']) && $_POST['j1_driver_price'] !='') //Accepted
			{
				$mailer = new mailer();
				$job->updateJob($j1_id, 'driver_status', '3'); // change the driver status to Sent
				$job->addNewDriverToJob($j1_id, '', '', '', '', $_POST['j1_driver'], $_POST['j1_driver_price'], '', '', '', '', '',''); // Add this to job__driver table
				$driver_details = $user->getUserDetails($_POST['j1_driver']); // get details of this driver
				$job->addJobDriverLog($j1_id, $_SESSION['USER_ID'], 'Job Accepted', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$_POST['j1_driver_price'].'', '');
				$additional_message .= "Booking ID -  ".$j1_id ." Accepted by - <b>".$driver_details['fname']." ".$driver_details['lname']." for $ ".$_POST['j1_driver_price']."</b><br/>";		
				$mailer->sendJobDetailsToDriver($j1_id, $_POST['j1_driver'], $_POST['j1_driver_price'],$_POST['j1_driver_notes']); // Send job acceptance email with job details to the driver
			}
		//Upadate Addresses
		$add_id_1 = $address->checkDuplicateOrEnter($pax_id, $_POST['j1_line_1'], $_POST['j1_line_2'], $_POST['j1_sub'], $_POST['j1_pc'], $_POST['j1_state']);
		if($add_id_1 != '')
			{
				$additional_message .= 'New Address Added - <b>'.$_POST['j1_line_1'].', '.$_POST['j1_line_2'].', '.$_POST['j1_sub'].', '.$_POST['j1_pc'].', '.$_POST['j1_state'].'</b><br/>';
			}
		$add_id_2 = $address->checkDuplicateOrEnter($pax_id, $_POST['j1_to_line_1'], $_POST['j1_to_line_2'], $_POST['j1_to_sub'], $_POST['j1_to_pc'], $_POST['j1_state']);
		if($add_id_2 != '')
			{
				$additional_message .= 'New Address Added - <b>'.$_POST['j1_to_line_1'].', '.$_POST['j1_to_line_2'].', '.$_POST['j1_to_sub'].', '.$_POST['j1_to_pc'].', '.$_POST['j1_state'].'</b><br/>';
			}
		$add_id_3 = $address->checkDuplicateOrEnter($pax_id, $_POST['j1_via_line_1'], $_POST['j1_via_line_2'], $_POST['j1_via_sub'], $_POST['j1_via_pc'], $_POST['j1_state']);
		if($add_id_3 != '')
			{
				$additional_message .= 'New Address Added - <b>'.$_POST['j1_via_line_1'].', '.$_POST['j1_via_line_2'].', '.$_POST['j1_via_sub'].', '.$_POST['j1_via_pc'].', '.$_POST['j1_state'].'</b><br/>';
			}
		$add_id_4 = $address->checkDuplicateOrEnter($pax_id, $_POST['j1_to_via_line_1'], $_POST['j1_to_via_line_2'], $_POST['j1_to_via_sub'], $_POST['j1_to_via_pc'], $_POST['j1_state']);
		if($add_id_4 != '')
			{
				$additional_message .= 'New Address Added - <b>'.$_POST['j1_to_via_line_1'].', '.$_POST['j1_to_via_line_2'].', '.$_POST['j1_to_via_sub'].', '.$_POST['j1_to_via_pc'].', '.$_POST['j1_state'].'</b><br/>';
			}

		if($_POST['job_type'] == '2')
			{
				$from_state = $address->getStateName($_POST['j2_state']);
				$to_state = $from_state;
				if($_POST['j1_via_sub'] != '') // if suburb not blank
					{
						$from_via_state = $from_state;
					}
				if($_POST['j1_to_via_sub'] != '') // if suburb not blank
					{
						$to_via_state = $from_state;
					}
				if($_POST['j2_line_2']!='' || $row9['j2_line_2']!=NULL)
				{
					$j2_full_address=$_POST['j2_line_1'].','.$_POST['j2_line_2'].','.$_POST['j2_sub'].','.$_POST['j2_pc'];
				}
				else
				{
					$j2_full_address=$_POST['j2_line_1'].','.$_POST['j2_sub'].','.$_POST['j2_pc'];
				}
				$j2_lat_long=$job->getlatlong($j2_full_address);
				$j2_id = $job->addNewJob
					(
						$job_reference_id,
						$job_status_id,
						$_POST['j2_date_cap'],
						$_POST['j2_time'],
						mysql_real_escape_string($_POST['j2_flight_no']),
						mysql_real_escape_string($_POST['j2_line_1']),
						mysql_real_escape_string($_POST['j2_line_2']),
						mysql_real_escape_string($_POST['j2_sub']),
						mysql_real_escape_string($_POST['j2_pc']),
						$from_state,
						mysql_real_escape_string($_POST['j2_to_flight_no']),
						mysql_real_escape_string($_POST['j2_to_line_1']),
						mysql_real_escape_string($_POST['j2_to_line_2']),
						mysql_real_escape_string($_POST['j2_to_sub']),
						mysql_real_escape_string($_POST['j2_to_pc']),
						$to_state,
						mysql_real_escape_string($_POST['j2_via_line_1']),
						mysql_real_escape_string($_POST['j2_via_line_2']),
						mysql_real_escape_string($_POST['j2_via_sub']),
						mysql_real_escape_string($_POST['j2_via_pc']),
						$from_via_state,
						mysql_real_escape_string($_POST['j2_to_via_line_1']),
						mysql_real_escape_string($_POST['j2_to_via_line_2']),
						mysql_real_escape_string($_POST['j2_to_via_sub']),
						mysql_real_escape_string($_POST['j2_to_via_pc']),
						$to_via_state,
						$_POST['j2_car_id'],
						$_POST['j2_pax_nos'],
						$_POST['j2_luggage'],
						$_POST['j2_baby_seats'],
						$_POST['j2_booster_seats'],
						$_POST['j2_baby_capsules'],
						'',											//this is driver_status_field (leave blank)
						$_POST['j2_kms'],
						$_POST['j2_fare'],
						$_POST['j2_inter'],
						$_POST['j2_ed'],
						$_POST['j2_wait'],
						$_POST['j2_tolls'],
						$_POST['j2_bs'],
						$_POST['j2_park'],
						$_POST['j2_ah'],
						$_POST['j2_me'],
						$_POST['j2_alc'],
						$_POST['j2_fc'],
						$_POST['j2_oth'],
						$_POST['j2_tot_fare'],
						$_POST['j2_drv_fee'],
						$_POST['j2_oth_exp'],
						$_POST['j2_profit'],
						mysql_real_escape_string($_POST['j2_ext_notes']),
						mysql_real_escape_string($_POST['j2_int_notes']),
						mysql_real_escape_string($_POST['j2_driver_notes']),
						$j2_lat_long['lat'],
						$j2_lat_long['lang']
					);
				if($j2_id != '')
					{
						$return_message .= "<b>BOOKING ID -  ".$j2_id."</b><br/>";
					}
				$query99 = "SELECT * from user where role_id='1'";
				$result99 = $database->query($query99);
				while($row99 = mysql_fetch_array($result99))
				{
					if($row99['device_token']!='' || $row99['device_token']!=NULL)
					{
						$push_message = 'A new job (Job Id : '.$j2_id.') has been created.';
						$job->commonGooglePushNotification($row99['id'],$push_message);
					}
				}
				//Add in Job Log Table
				$job->addJobLog($j2_id, $_SESSION['USER_ID'], 'New job created', $j2_id, '');
				$job_ref->updateJobReferenceTable($job_reference_id, 'j2_id', $j2_id);
				//Add in Job Reference Log Table
				$job_ref->addJobReferenceLog($job_reference_id, $_SESSION['USER_ID'], 'Job 2 Added', $j2_id, 'NIL');
				
				//Allocate, Send of Accept Driver
				if($_POST['j2_driver_status'] == '1' && $_POST['j2_driver'] !='') //Allocate
					{
						$job->updateJob($j2_id, 'driver_status', '1'); // change the driver status to Allocate
						$job->updateJob($j2_id, 'driver_id', $_POST['j2_driver']); // Put this driver in job table
						$job->updateJob($j2_id, 'driver_price', $_POST['j2_driver_price']); // Put the driver price in the job table
						$job->addNewDriverToJob($j2_id, $_POST['j2_driver'], $_POST['j2_driver_price'], '', '', '', '', '', '', '', '', '',''); // Add this to job__driver table
						$driver_details = $user->getUserDetails($_POST['j2_driver']); // get details of this driver
						$job->addJobDriverLog($j2_id, $_SESSION['USER_ID'], 'Job Allocated', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$_POST['j2_driver_price'].'', '');
						$additional_message .= "Booking ID -  ".$j2_id ." allocated to - <b>".$driver_details['fname']." ".$driver_details['lname']." for $ ".$_POST['j2_driver_price']."</b><br/>";
					}
				if($_POST['j2_driver_status'] == '2' && !empty($_POST['j2_drivers']) && $_POST['j2_driver_price'] !='') //Send/Offer
					{
						$mailer = new mailer();
						$job->updateJob($j2_id, 'driver_status', '2'); // change the driver status to Sent
						foreach($_POST['j2_drivers'] as $key => $value)	
							{
								$user->getUserDetails($value); // get details of this driver
								$job->addNewDriverToJob($j2_id, '', '', $value, $_POST['j2_driver_price'], '', '', '', '', '', '', '',''); // Add this to job__driver table
								$driver_details = $user->getUserDetails($value); // get details of this driver
								$job->addJobDriverLog($j2_id, $_SESSION['USER_ID'], 'Job Offered', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$_POST['j2_driver_price'].'', '');
								$additional_message .= "Booking ID -  ".$j2_id ." offered to - <b>".$driver_details['fname']." ".$driver_details['lname']." for $ ".$_POST['j2_driver_price']."</b><br/>";
								$mailer->sendJobOfferToDriver($value, $j2_id, $_POST['j2_driver_price'],$_POST['j2_driver_notes']); // Send job offer to the driver
								if($driver_details['device_token']!='' || $driver_details['device_token']!=NULL)
								{
									$push_title = 'Job '.$j2_id;
									$push_message = 'Job Id : '.$j2_id.' has been offered to you.';
									$job->commonGooglePushNotification($value,$push_message,$push_title);
								}
							}
					}
				// For Group Drivers offer	
				if($_POST['j2_driver_status'] == '2' && !empty($_POST['j2_driver_group']) && $_POST['j2_driver_group'] !='') //Send
					{
						
						$mailer = new mailer();
						$job->updateJob($j2_id, 'driver_status', '2'); // change the driver status to Sent
						
						$query = "SELECT * from drivers__group_drivers where group_id='".$_POST['j2_driver_group']."'";
						$result = $database->query($query);
						while($row = mysql_fetch_array($result))
							{
								$user->getUserDetails($row['user_id']); // get details of this driver
								$job->addNewDriverToJob($j2_id, '', '', $row['user_id'], $_POST['j2_driver_price'], '', '', '', '', '', '', '',''); // Add this to job__driver table
								$driver_details = $user->getUserDetails($row['user_id']); // get details of this driver
								$job->addJobDriverLog($j2_id, $_SESSION['USER_ID'], 'Job Offered', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$_POST['j2_driver_price'].'', '');
								$additional_message .= "Booking ID -  ".$j2_id ." offered to - <b>".$driver_details['fname']." ".$driver_details['lname']." for $ ".$_POST['j2_driver_price']."</b><br/>";
								$mailer->sendJobOfferToDriver($row['user_id'], $j2_id, $_POST['j2_driver_price'],$_POST['j2_driver_notes']); // Send job offer to the driver
								if($driver_details['device_token']!='' || $driver_details['device_token']!=NULL)
								{
									$push_title = 'Job '.$j2_id;
									$push_message = 'Job Id : '.$j2_id.' has been offered to you.';
									$job->commonGooglePushNotification($row['user_id'],$push_message,$push_title);
								}
							}
					}
				if($_POST['j2_driver_status'] == '3' && !empty($_POST['j2_driver']) && $_POST['j2_driver_price'] !='') //Accepted
					{
						$mailer = new mailer();
						$job->updateJob($j2_id, 'driver_status', '3'); // change the driver status to Sent/Accepted
						$job->updateJob($j2_id, 'driver_id', $_POST['j2_driver']); // Put this driver in job table
						$job->updateJob($j2_id, 'driver_price', $_POST['j2_driver_price']); // Put the driver price in the job table
						$job->addNewDriverToJob($j2_id, '', '', '', '', $_POST['j2_driver'], $_POST['j2_driver_price'], '', '', '', '', '',''); // Add this to job__driver table
						$driver_details = $user->getUserDetails($_POST['j2_driver']); // get details of this driver
						$job->addJobDriverLog($j2_id, $_SESSION['USER_ID'], 'Job Accepted', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$_POST['j2_driver_price'].'', '');
						$additional_message .= "Booking ID -  ".$j2_id ." Accepted by - <b>".$driver_details['fname']." ".$driver_details['lname']." for $ ".$_POST['j2_driver_price']."</b><br/>";				
						$mailer->sendJobDetailsToDriver($j2_id, $_POST['j2_driver'], $_POST['j2_driver_price'],$_POST['j2_driver_notes']); // Send job acceptance email with job details to the driver
					}
					
				//Upadate Addresses
				$add_id_5 = $address->checkDuplicateOrEnter($pax_id, $_POST['j2_line_1'], $_POST['j2_line_2'], $_POST['j2_sub'], $_POST['j2_pc'], $_POST['j2_state']);
				if($add_id_5 != '')
					{
						$additional_message .= 'New Address Added - <b>'.$_POST['j2_line_1'].', '.$_POST['j2_line_2'].', '.$_POST['j2_sub'].', '.$_POST['j2_pc'].', '.$_POST['j2_state'].'</b><br/>';
					}
				$add_id_6 = $address->checkDuplicateOrEnter($pax_id, $_POST['j2_to_line_1'], $_POST['j2_to_line_2'], $_POST['j2_to_sub'], $_POST['j2_to_pc'], $_POST['j2_state']);
				if($add_id_6 != '')
					{
						$additional_message .= 'New Address Added - <b>'.$_POST['j2_to_line_1'].', '.$_POST['j2_to_line_2'].', '.$_POST['j2_to_sub'].', '.$_POST['j2_to_pc'].', '.$_POST['j2_state'].'</b><br/>';
					}
				$add_id_7 = $address->checkDuplicateOrEnter($pax_id, $_POST['j2_via_line_1'], $_POST['j2_via_line_2'], $_POST['j2_via_sub'], $_POST['j2_via_pc'], $_POST['j2_state']);
				if($add_id_7 != '')
					{
						$additional_message .= 'New Address Added - <b>'.$_POST['j2_via_line_1'].', '.$_POST['j2_via_line_2'].', '.$_POST['j2_via_sub'].', '.$_POST['j2_via_pc'].', '.$_POST['j2_state'].'</b><br/>';
					}
				$add_id_8 = $address->checkDuplicateOrEnter($pax_id, $_POST['j2_to_via_line_1'], $_POST['j2_to_via_line_2'], $_POST['j2_to_via_sub'], $_POST['j2_to_via_pc'], $_POST['j2_state']);
				if($add_id_8 != '')
					{
						$additional_message .= 'New Address Added - <b>'.$_POST['j2_to_via_line_1'].', '.$_POST['j2_to_via_line_2'].', '.$_POST['j2_to_via_sub'].', '.$_POST['j2_to_via_pc'].', '.$_POST['j2_state'].'</b><br/>';
					}
			}

		//send mail to bkg_by_id when bkg_src is website
		
		// if booking was made by admin staff, send emails to clients as BOOKING CONFIRMED
		if($_SESSION['ROLE_ID'] == '1') //Admin
			{
				$mailer = new Mailer();
				$job_ref_details = $job_ref->getJobReferenceDetails($job_reference_id);
				if($job_ref_details['acc_type'] == '3') //if this is booked by university or student
				{
					$mailer->sendUniversityMail($job_reference_id, '6');
				}
				else //booking made for other types of accounts
				{
					if($contact_id == $pax_id) // send this booking to 
					{
						if( $_POST['conf_to_pax'] == '1' || $_POST['conf_to_bkg_made_by'] == '1') // send this booking pax
						{
							$user = new User();
							$user_details = $user->getUserDetails($pax_id);
							if($user_details['email'] != '')
								{
									$mailer->sendMailToClientOnBooking($job_reference_id, '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
									$additional_message .= 'Booking confirmation sent to passenger.<br/>';
								}
							else
								{
									$additional_message .= 'Passenger Email - Not Found. Email not sent to passenger.<br/>';
								}
						}
					}
					else
					{
						if($_POST['conf_to_bkg_made_by'] == '1') // send this booking to booking made corporates only and not universities
						{
							$user = new User();
							$user_details = $user->getUserDetails($contact_id);
							if($user_details['email'] != '')
								{
									$mailer->sendMailToClientOnBooking($job_reference_id, '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
									$additional_message .= 'Booking confirmation sent to client.<br/>';
								}
							else
								{
									$additional_message .= 'Booking by Email - Not Found. Email not sent to booking made by.<br/>';
								}
						}
						if($_POST['conf_to_pax'] == '1') // send this booking pax
						{
							$user = new User();
							$user_details = $user->getUserDetails($pax_id);
							if($user_details['email'] != '')
								{
									$mailer->sendMailToClientOnBooking($job_reference_id, '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
									$additional_message .= 'Booking confirmation sent to passenger.<br/>';
								}
							else
								{
									$additional_message .= 'Passenger Email - Not Found. Email not sent to passenger.<br/>';
								}
						}
					}
					
					if($job_ref_details['agent_email'] != '') // Send an email to the agent
						{
							$mailer->sendMailToClientOnBooking($job_reference_id, '1', $job_ref_details['agent_email'], 'Agent', '','');
						}
				}
			}
		// if booking was made by customer send email as received and waiting for confirmation
		if($_SESSION['ROLE_ID'] == '2') //Driver
			{
				$mailer = new Mailer();
				if($_POST['conf_to_bkg_made_by'] == '1') // send this booking to booking made by
					{
						$user = new User();
						$user_details = $user->getUserDetails($contact_id);
						if($user_details['email'] != '')
							{
								$mailer->sendMailToClientOnBooking($job_reference_id, '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
								$additional_message .= 'Booking confirmation sent to client.<br/>';
							}
						else
							{
								$additional_message .= 'Booking by Email - Not Found. Email not sent to booking made by.<br/>';
							}
					}
				if($_POST['conf_to_pax'] == '1') // send this booking pax
					{
						$user = new User();
						$user_details = $user->getUserDetails($pax_id);
						if($user_details['email'] != '')
							{
								$mailer->sendMailToClientOnBooking($job_reference_id, '1', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
								$additional_message .= 'Booking confirmation sent to passenger.<br/>';
							}
						else
							{
								$additional_message .= 'Passenger Email - Not Found. Email not sent to passenger.<br/>';
							}
					}
				$mailer->sendMailToAdminOnBooking($job_reference_id);
			}
		if($_SESSION['ROLE_ID'] == '3') //Charge Account making the booking using BMS
			{
				$mailer = new Mailer();
				$user = new User();
				$user_details = $user->getUserDetails($contact_id);
				$job_ref_details = $job_ref->getJobReferenceDetails($job_reference_id);
				if($job_ref_details['acc_type'] == '3') //if this is booked by university or student
				{
					$mailer->sendUniversityMail($job_reference_id, '2');
					$additional_message .= 'Booking waiting to be confirmed by Allied.<br/>';
				}
				else
				{
					if($user_details['email'] != '')
						{
							$mailer->sendMailToClientOnBooking($job_reference_id, '2', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
							$additional_message .= 'Booking waiting to be confirmed by Allied.<br/>';
						}
					else
						{
							$additional_message .= 'We could not sent an email to booked by as no email address was found.<br/>However Allied has received your booking and will confirm as soon as possible.<br/>';
						}
					
					if($pax_id != $contact_id) // So that one email is not sent to same email id twice, incase pax and contact are same.
					{
						$user_details = $user->getUserDetails($pax_id); 
						if($user_details['email'] != '')
							{
								$mailer->sendMailToClientOnBooking($job_reference_id, '2', $user_details['email'], $user_details['title'], $user_details['fname'], $user_details['lname']);
								$additional_message .= 'Booking waiting to be confirmed by Allied.<br/>';
							}
						else
							{
								$additional_message .= 'We could not send an email to the passenger as no email address was found.<br/>However Allied has received your booking and will confirm as soon as possible.<br/>';
							}
					}
					$mailer->sendMailToAdminOnBooking($job_reference_id);
				}
			}
		if($_SESSION['ROLE_ID'] == '1')
			{
				$message = ''.$return_message.'<br/>'.$additional_message.'';
			}
		else if($_SESSION['ROLE_ID'] == '2' || $_SESSION['ROLE_ID'] == '3')
			{
				$booking_txt = ($j2_id != '')? $j1_id.' & '.$j2_id : $j1_id;  
				$html_message = "<div class='outer_box'>
					<div class='outer_inner_box'>
					Dear \"".$_SESSION['FNAME'].' '.$_SESSION['LNAME']."\".<br /><br />
					Thank you for your Online Booking.<br /><br />
					This is to let you know, that our Online Booking system has received your Booking.<br /><br /> 
					This booking ID ".$booking_txt." is ONLY VALID IF booked according to our terms and conditions as listed below:<br /><br /><br /> 
					TERMS & CONDITIONS FOR THIS ONLINE BOOKING<br /><br /> 
					1... During office hours, this booking MUST be booked with a 3 hour lead* time.<br /><br /> 
					2... Outside office hours, this booking MUST be booked with a 14 hour lead* time.<br /><br /> 
					3... Weekend and public holidays, this booking MUST be booked with a 36 hour lead* time.<br /><br />
					* \"Lead Time\" is the time before the booking time.<br /><br /> 
					New bookings or bookings cancellations are valid only on receipt of our confirmation.<br /><br /><br />
					Our service is 24 hours a day,<br />
					Our office hours are<br />
					Monday to Friday 8am to 7pm<br />
					Saturday 1pm to 5pm<br />
					Sunday   1pm to 8pm<br />
					Please also be aware that our office closes on public holidays<br /><br />
					Kind regards,<br />
					ALLIED CHAUFFEURED CARS AUSTRALIA<br />
					ABN: 38 076 977 136 <br />
					</div>
					<div class=\"footer_div\">
					Copyright &copy; 2011 Allied Chauffeured Cars. Ltd. All Rights Reserved. 
					</div>
					</div>";
				$message = $html_message;
			}
			
		
		$a = array('data1' => 'OK', 'data2' => ''.$message.'', 'data3' => ''.$j1_id.'');
		$json = json_encode($a); 
		echo $json;
		
	}
else
	{
		$a = array('data1' => 'ERROR', 'data2' => '<span class="error">ERRORS - Please Correct!<br/>'.$error_message.'</span>','data3' =>$timeout_msg);
		$json = json_encode($a); 
		echo $json;
	}
?>