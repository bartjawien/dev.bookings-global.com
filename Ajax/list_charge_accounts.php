<?php
ini_set("display_errors", 0);
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "chargeAccount.php");
require_once(CLASSES_PATH . "user.php");
require_once(INCLUDE_PATH . "functions.php");
session_start();

$database = new Database;
$charge_account = new chargeAccount();
$user = new user();
try
	{
/*-------------------SEARCH-----------------------------------------------------------------------------------------------------*/
		if($_POST['search_string'])
			{
				
				$database = new database;
				$query = "SELECT COUNT(*) AS RecordCount FROM charge_acc
							WHERE 
							id like '%".$_POST['search_string']."%'
							OR account_name like '%".$_POST['search_string']."%'
							OR myob_no like '%".$_POST['search_string']."%'";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];
				
				$query = "SELECT * FROM charge_acc
							WHERE 
							id like '%".$_POST['search_string']."%'
							OR account_name like '%".$_POST['search_string']."%'
							OR myob_no like '%".$_POST['search_string']."%'
							ORDER BY 
							".$_GET["jtSorting"]." LIMIT ".$_GET["jtStartIndex"].", ".$_GET["jtPageSize"]."";
				$result = $database->query($query);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['Records'] = $rows;
				$jTableResult['query'] = $query;
				print json_encode($jTableResult);
			}
/*-------------------LISTING-----------------------------------------------------------------------------------------------------*/
		else if($_GET["action"] == "list_charge_accounts")
			{
				if($_SESSION['ROLE_ID'] == '3')
					{
						$query1 = "SELECT * from charge_acc__contacts where user_id = '".$_SESSION['USER_ID']."'";
						$result1 = $database->query($query1);
						$row1 = mysql_fetch_array($result1);
						$charge_acc_id = $row1['charge_acc_id'];
						$where .= "WHERE 1 ";
						$where .= " AND charge_acc.id = '".$charge_acc_id."' ";
					}
				//Get record count
				$query = "SELECT COUNT(*) AS RecordCount FROM charge_acc";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];
				$query = "SELECT
							charge_acc.id as id,
							DATE_FORMAT(charge_acc.created_on,'%d/%m/%Y') AS created_on,
							CONCAT(user.title, ' ', user.fname, ' ', user.lname) as created_by,
							charge_acc.acc_type as acc_type,
							charge_acc.account_name as account_name,
							charge_acc.myob_no as myob_no
							FROM charge_acc
							LEFT JOIN user ON charge_acc.created_by = user.id
							".$where."
							ORDER BY
							".$_GET["jtSorting"]." LIMIT ".$_GET["jtStartIndex"].", ".$_GET["jtPageSize"]."";
				$result = $database->query($query);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				/////Export
				if(isset($_SESSION['LIST_CHARGE_ACCOUNTS']))
					{
						unset($_SESSION['LIST_CHARGE_ACCOUNTS']);
						$_SESSION['LIST_CHARGE_ACCOUNTS'] = array();
					}
				$_SESSION['LIST_CHARGE_ACCOUNTS'] = $query;
				////End
				
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['Records'] = $rows;
				print json_encode($jTableResult);
			}
		else if($_GET["action"] == "list_charge_accounts_contacts")
			{
				//Get record count
				$query = "SELECT COUNT(*) AS RecordCount FROM charge_acc__contacts";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];
				$query = "SELECT
							charge_acc__contacts.id as contact_id,
							charge_acc__contacts.type_id as type_id,
							user.id as user_id,
							user.title as title,
							user.fname as fname,
							user.lname as lname,
							user.password as password,
							user.email as email,
							user.mobile as mobile,
							user.phone as phone,
							user.preference as preference,
							user.hidden as hidden
							FROM charge_acc__contacts
							LEFT JOIN charge_acc ON charge_acc__contacts.id = charge_acc.id
							LEFT JOIN user ON charge_acc__contacts.user_id = user.id
							WHERE charge_acc__contacts.charge_acc_id = ".$_GET['id']."
							ORDER BY charge_acc__contacts.type_id";
				$result = $database->query($query);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['Records'] = $rows;
				print json_encode($jTableResult);
			}
		else if($_GET["action"] == "list_pois")
			{
				//Get record count
				$query = "SELECT COUNT(*) AS RecordCount FROM charge_acc__pois where charge_acc_id = '".$_GET['id']."'";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];

				//Get records from database
				$query = "SELECT * FROM charge_acc__pois where charge_acc_id = '".$_GET['id']."' order by sub ASC";
				$result = $database->query($query);
				//Add all records to an array
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}

				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['Records'] = $rows;
				print json_encode($jTableResult);
			}	
		else if($_GET["action"] == "list_addresses")
			{
				//Get record count
				$query = "SELECT COUNT(*) AS RecordCount FROM user__address where user_id = '".$_GET["user_id"]."'";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];

				//Get records from database
				$query = "SELECT *,
							DATE_FORMAT(created_on,'%d/%m/%Y') AS created_on
							FROM user__address
							where user_id = '".$_GET["id"]."'";
				$result = $database->query($query);
				//Add all records to an array
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}

				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['Records'] = $rows;
				print json_encode($jTableResult);
			}	
		
		
/*-------------------CREATE-----------------------------------------------------------------------------------------------------*/

		else if($_GET["action"] == "create_charge_accounts")
			{
				
				if(checkforDuplicates('charge_acc', 'account_name', ''.$_POST['account_name'].'')=='0')
					{
						foreach(array_keys($_POST) as $key)
							{
								$clean[$key] = mysql_real_escape_string($_POST[$key]);
							}
						$new_charge_acc_id = $charge_account->addChargeAccount($_SESSION['USER_ID'], $clean['acc_type'], $clean['account_name'], $clean['myob_no']);
						$row = $charge_account->getChargeAccountDetails($new_charge_acc_id);
						$jTableResult = array();
						$jTableResult['Result'] = "OK";
						$jTableResult['Record'] = $row;
						$jTableResult['Message'] = "New Charge Account Created Successfully";
					}
				else
					{
						$jTableResult = array();
						$jTableResult['Result'] = "ERROR";
						$jTableResult['Message'] = "Duplicate Charge Account Name";
					}
				print json_encode($jTableResult);
			}
		else if($_GET["action"] == "create_pois")
			{
				if( $_GET['id'] != '')
					{
						foreach(array_keys($_POST) as $key)
							{
								$clean[$key] = mysql_real_escape_string($_POST[$key]);
							}
						$query = "INSERT INTO charge_acc__pois (charge_acc_id, line1, line2, sub, postcode, state, country) VALUES
									('".$_GET['id']."','".$clean['line1']."','".$clean['line2']."','".$clean['sub']."','".$clean['postcode']."','".$clean['state']."','".$clean['country']."')";
						$result = $database->query($query);
						$id = mysql_insert_id();
						$q = "SELECT * from charge_acc__pois where id='$id'";
						$res = $database->query($q);
						$row = mysql_fetch_assoc($res);
						$jTableResult = array();
						$jTableResult['Result'] = "OK";
						$jTableResult['Record'] = $row;
						$jTableResult['Message'] = "New POI Created Successfully";
						print json_encode($jTableResult);
					}
				else
					{
						$jTableResult = array();
						$jTableResult['Result'] = "ERROR";
						$jTableResult['Message'] = "Charge Account ID Blank. Remedy - Please refresh the browser and try creating again.";
					}
				
			}
		else if($_GET["action"] == "create_charge_account_contact")
			{
				if( $_GET['id'] != '')
					{
						foreach(array_keys($_POST) as $key)
							{
								$clean[$key] = mysql_real_escape_string($_POST[$key]);
							}
						$new_contact_id = $charge_account->addChargeAccountContact($clean['title'], $clean['fname'], $clean['lname'], $clean['password'], $clean['email'], $clean['mobile'], $clean['phone'], $clean['preference'], $_GET['id'], $clean['type_id']);
						if($new_contact_id != '')
							{
								$row = $user->getUserDetails($new_contact_id);
								$jTableResult = array();
								$jTableResult['Result'] = "OK";
								$jTableResult['Record'] = $row;
								print json_encode($jTableResult);
							}
						else
							{
								$jTableResult = array();
								$jTableResult['Result'] = "ERROR";
								$jTableResult['Message'] = "There was a problem adding this contact. Remedy - Please refresh the browser and try creating again.";
							}
					}
				else
					{
						$jTableResult = array();
						$jTableResult['Result'] = "ERROR";
						$jTableResult['Message'] = "Charge Account ID Blank. Remedy - Please refresh the browser and try creating again.";
					}
				
			}
		else if($_GET["action"] == "create_addresses")
			{
				$new_address_id = $user->addUserAddress($_POST['user_id'], $_POST['type_id'], $_POST['line1'], $_POST['line2'], $_POST['sub'], $_POST['postcode'], $_POST['state']);
				$row = $user->getUserAddress($new_address_id);
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['Record'] = $row;
				print json_encode($jTableResult);
			}

/*-------------------UPDATE-----------------------------------------------------------------------------------------------------*/

		else if($_GET["action"] == "update_charge_accounts")
			{
				foreach(array_keys($_POST) as $key)
					{
						$clean[$key] = mysql_real_escape_string($_POST[$key]);
					}
				$charge_account->updateChargeAccount($clean['id'], 'acc_type', $clean["acc_type"]);
				$charge_account->updateChargeAccount($clean['id'], 'account_name', $clean["account_name"]);
				$charge_account->updateChargeAccount($clean['id'], 'myob_no', $clean["myob_no"]);
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}
			
		else if($_GET["action"] == "update_pois")
			{
				foreach(array_keys($_POST) as $key)
					{
						$clean[$key] = mysql_real_escape_string($_POST[$key]);
					}
				$query = "UPDATE charge_acc__pois SET
						line1 = '".$clean['line1']."',
						line2 = '".$clean['line2']."',
						sub = '".$clean['sub']."',
						postcode = '".$clean['postcode']."',
						state = '".$clean['state']."',
						country = '".$clean['country']."'
						WHERE id = '".$clean['id']."'";
				$result = $database->query($query);
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}
		else if($_GET["action"] == "update_charge_accounts_contact")
			{
				foreach(array_keys($_POST) as $key)
					{
						$clean[$key] = mysql_real_escape_string($_POST[$key]);
					}
				$user->updateChargeAccountContact($clean['user_id'], 'type_id', $clean["type_id"]);
				$user->updateUser($clean['user_id'], 'title', $clean["title"]);
				$user->updateUser($clean['user_id'], 'fname', $clean["fname"]);
				$user->updateUser($clean['user_id'], 'lname', $clean["lname"]);
				$user->updateUser($clean['user_id'], 'password', $clean["password"]);
				$user->updateUser($clean['user_id'], 'email', $clean["email"]);
				$user->updateUser($clean['user_id'], 'mobile', $clean["mobile"]);
				$user->updateUser($clean['user_id'], 'phone', $clean["phone"]);
				$user->updateUser($clean['user_id'], 'preference', $clean["preference"]);
				$user->updateUser($clean['user_id'], 'hidden', $clean["hidden"]);
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}
			
		else if($_GET["action"] == "update_addresses")
			{
				foreach(array_keys($_POST) as $key)
					{
						$clean[$key] = mysql_real_escape_string($_POST[$key]);
					}
				$user->updateUserAddress($clean['id'], 'type_id', $clean["type_id"]);
				$user->updateUserAddress($clean['id'], 'line1', $clean["line1"]);
				$user->updateUserAddress($clean['id'], 'line2', $clean["line2"]);
				$user->updateUserAddress($clean['id'], 'sub', $clean["sub"]);
				$user->updateUserAddress($clean['id'], 'postcode', $clean["postcode"]);
				$user->updateUserAddress($clean['id'], 'state', $clean["state"]);
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}
/*-------------------DELETE-----------------------------------------------------------------------------------------------------*/
		else if($_GET["action"] == "delete_charge_accounts")
			{
				$charge_account->deleteChargeAccount($_POST["id"]);
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}
		else if($_GET["action"] == "delete_pois")
			{
				$query = "DELETE FROM charge_acc__pois WHERE id = '".$_POST['id']."'";
				$result = $database->query($query);
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}
		else if($_GET["action"] == "delete_charge_accounts_contact")
			{
				$charge_account->deleteChargeAccountContact($_POST['user_id'], $_GET['id']);
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}
		else if($_GET["action"] == "delete_addresses")
			{
				$user->deleteAUserAddress($_POST['id']);
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}

/*-------------------EXPORT-----------------------------------------------------------------------------------------------------*/
			
		else if($_GET["action"] == "export_excel")
			{
				
				require_once '../Classes/PHPExcel.php';
				
				$database = new database;
				$objPHPExcel = new PHPExcel();
				
								// Create a first sheet
				$objPHPExcel->getProperties()->setCreator("".BUSINESS_NAME."")
							 ->setLastModifiedBy("".$_SESSION['TITLE']." ".$_SESSION['FNAME']." ".$_SESSION['LNAME']."")
							 ->setTitle("List of Users of ".BUSINESS_NAME."")
							 ->setSubject("List of Users of ".BUSINESS_NAME."")
							 ->setDescription("List of Users of ".BUSINESS_NAME."")
							 ->setKeywords("List of Users")
							 ->setCategory("List of Users");
							 
				$objPHPExcel->setActiveSheetIndex(0);
				$objPHPExcel->getActiveSheet()->setCellValue('A1', "ID");
				$objPHPExcel->getActiveSheet()->setCellValue('B1', "Account Type");
				$objPHPExcel->getActiveSheet()->setCellValue('C1', "Account Name");
				$objPHPExcel->getActiveSheet()->setCellValue('D1', "MYOB No");
				$objPHPExcel->getActiveSheet()->setCellValue('D1', "Created By");
				
				$styleArray = array( 'font' => array( 'bold' => true, ), 'alignment' => array( 'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, ), 'borders' => array( 'top' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN, ), ), 'fill' => array( 'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR, 'rotation' => 90, 'startcolor' => array( 'argb' => 'FFA0A0A0', ), 'endcolor' => array( 'argb' => 'FFFFFFFF', ), ), );
				$objPHPExcel->getActiveSheet()->getStyle('A1:AA1')->applyFromArray($styleArray);
				// Hide "ID" column
				//$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setVisible(false);
				
				$query = $_SESSION['LIST_CHARGE_ACCOUNTS'];
				$result = $database->query($query);
				$total_rows= mysql_num_rows($result);
				$rows = array();
				

				$i = 2;
				while($row = mysql_fetch_array($result))
					{
						$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A'.$i, $row['id'])
							->setCellValue('B'.$i, $row['account_name'])
							->setCellValue('C'.$i, $row['acc_type'])
							->setCellValue('D'.$i, $row['myob_no'])
							->setCellValue('D'.$i, $row['created_by']);
						$i++;
					}
				$todays_date = Date("d-m-Y-His");
				header('Content-Type: application/vnd.ms-excel');
				$user_report = "Users_List_".$todays_date.".xls";
				header('Content-Disposition: attachment;filename='.$user_report.'');
				header('Cache-Control: max-age=0');
				$objPHPExcel->getActiveSheet()->setTitle('Users_List');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$objWriter->save('php://output');
			}
			else if($_GET["action"] == "export_html")
			{
				require_once(CLASSES_PATH . "chargeAccount.php");
				$chargeaccount = new chargeAccount;
				
				$query = $_SESSION['LIST_CHARGE_ACCOUNTS'];
				$result = $database->query($query);
				$total_rows= mysql_num_rows($result);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				$htmlstr = $chargeaccount->getHTMLAccountList($rows);
				$todays_date = Date("d-m-Y-His"); 
				$file_name = "../html_report/USER_REPORT_".$todays_date.".html";
				
				$myfile = fopen($file_name, "w") or die("Unable to open file!");
				fwrite($myfile, $htmlstr);
				fclose($myfile);
				 
				header("Cache-Control: public");
				header("Content-Description: File Transfer");
				header("Content-Length: ". filesize("$file_name").";");
				header("Content-Disposition: attachment; filename=$file_name");
				header("Content-Type: application/octet-stream; "); 
				header("Content-Transfer-Encoding: binary");
				readfile($file_name);
			}
			
			else if($_GET["action"] == "export_pdf")
			{
				require_once('../html2pdf/html2pdf.class.php');
				require_once(CLASSES_PATH . "chargeAccount.php");
				
				$chargeaccount = new chargeAccount;
				
				$query = $_SESSION['LIST_CHARGE_ACCOUNTS'];
				$result = $database->query($query);
				$total_rows= mysql_num_rows($result);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				$htmlstr = $chargeaccount->getHTMLAccountList($rows);
				$todays_date = Date("d-m-Y-His");
				$file_name = "../html_report/BOOKINGS_REPORT_".$todays_date.".pdf";
				
					
				$html2pdf = new HTML2PDF('L', 'A4', 'en');
				$html2pdf->WriteHTML($htmlstr);
				
				$pdfdoc = $html2pdf->Output($file_name , 'F');
				
				header("Cache-Control: public");
				header("Content-Description: File Transfer");
				header("Content-Length: ". filesize("$file_name").";");
				header("Content-Disposition: attachment; filename=$file_name");
				header("Content-Type: application/pdf "); 
				 
				readfile($file_name);
				
			}
						
	}
catch(Exception $ex)
	{
		//Return error message
		$jTableResult = array();
		$jTableResult['Result'] = "ERROR";
		$jTableResult['Message'] = $ex->getMessage();
		print json_encode($jTableResult);
	}
?>