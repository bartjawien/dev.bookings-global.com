<?php
ini_set("display_errors", 0);
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "map.php");
session_start();


function round_5($in)
	{
		return round(($in*2)/10)*5;
	}
//For showing account balance on driver selection
if(	isset($_POST['pickup_address']) && isset($_POST['destination_address'])	)
	{
		
		$map = new map();
		$ret_val = $map->calculate_distance($_POST['pickup_address'], $_POST['destination_address']);
		
		$data1 = array(
						"from" 			=> $ret_val['from'],
						"to" 			=> $ret_val['to'],
						"distance" 		=> $ret_val['distance'],
						"time_hrs" 		=> $ret_val['time_hrs'],
						"time_min" 		=> $ret_val['time_min'],
						"time_sec" 		=> $ret_val['time_sec']
					);
		
		$database = new Database;
		$query = "SELECT * from cars";
		$result = $database->query($query);

		while($row = mysql_fetch_array($result))
			{
				//$fare = round_5($ret_val['distance'] * $row['rate_per_km']);
				$fare = number_format($ret_val['distance'] * $row['rate_per_km'],2);
				
				
				if($fare < $row['min_fare'])
					{
						if($row['driver_com_fixed'] > 0)
							{
								$driver_commission 	= $row['driver_com_fixed'];
								$driver_earning 	= $row['min_fare'] - $driver_commission;
								$amt_percentage 	= '- $'.number_format($row['driver_com_fixed'],2).'';
							}
						else
							{
								$driver_commission 	= $row['min_fare'] * ($row['driver_com_percentage']/100);
								$driver_earning 	= $row['min_fare'] - $driver_commission;
								$amt_percentage 	= '- '.$row['driver_com_percentage'].'%';
							}
						$data2[] = array(
									"id" 				=> $row['id'],
									"car_type"	 		=> $row['car_type'],
									"min_fare" 			=> $row['min_fare'],
									"rate_per_km" 		=> $row['rate_per_km'],
									"est_fare" 			=> $row['min_fare'],
									"driver_fare" 		=> number_format($driver_earning,2),
									"amt_percentage" 	=> $amt_percentage
								);
					}
				else
					{
						if($row['driver_com_fixed'] > 0)
							{
								$driver_commission 	= $row['driver_com_fixed'];
								$driver_earning 	= $fare - $driver_commission;
								$amt_percentage 	= '- $'.number_format($row['driver_com_fixed'],2).'';
							}
						else
							{
								$driver_commission 	= $fare * ($row['driver_com_percentage']/100) ;
								$driver_earning 	= $fare - $driver_commission;
								$amt_percentage 	= '- '.$row['driver_com_percentage'].'%';
							}
						$data2[] = array(
									"id" 				=> $row['id'],
									"car_type"	 		=> $row['car_type'],
									"min_fare" 			=> $row['min_fare'],
									"rate_per_km" 		=> $row['rate_per_km'],
									"est_fare" 			=> $fare,
									"driver_fare" 		=> number_format($driver_earning,2),
									"amt_percentage" 	=> $amt_percentage
									
								);
					}
				
			}
		$data = array('data1' => $data1, 'data2' => $data2);
		echo json_encode($data);
	}
	
if(	isset($_POST['pickup_from_area']) && isset($_POST['destination_suburb'])	)
	{
		$database = new Database;
		$query1 		= "SELECT * from cars__prices where id = '".$_POST['destination_suburb']."'";
		$result1 		= $database->query($query1);
		$row1 			= mysql_fetch_array($result1);
		$fare 			= number_format($row1[''.$_POST['pickup_from_area'].''],2);

		
		$query2 = "SELECT * from cars";
		$result2 = $database->query($query2);
		while($row2 = mysql_fetch_array($result2))
			{
				if($fare < $row2['min_fare'])
					{
						if($row2['driver_com_fixed'] > 0)
							{
								$driver_commission 	= $row2['driver_com_fixed'];
								$driver_earning 	= $row2['min_fare'] - $driver_commission;
								$amt_percentage 	= '- $'.number_format($row2['driver_com_fixed'],2).'';
							}
						else
							{
								$driver_commission 	= $row2['min_fare'] * ($row2['driver_com_percentage']/100);
								$driver_earning 	= $row2['min_fare'] - $driver_commission;
								$amt_percentage 	= '- '.$row2['driver_com_percentage'].'%';
							}
						$data[] = array(
									"id" 				=> $row2['id'],
									"car_type"	 		=> $row2['car_type'],
									"min_fare" 			=> $row2['min_fare'],
									"rate_per_km" 		=> $row2['rate_per_km'],
									"est_fare" 			=> $row2['min_fare'],
									"driver_fare" 		=> number_format($driver_earning,2),
									"amt_percentage" 	=> $amt_percentage
								);
					}
				else
					{
						if($row['driver_com_fixed'] > 0)
							{
								$driver_commission 	= $row2['driver_com_fixed'];
								$driver_earning 	= $fare - $driver_commission;
								$amt_percentage 	= '- $'.number_format($row2['driver_com_fixed'],2).'';
							}
						else
							{
								$driver_commission 	= $fare * ($row2['driver_com_percentage']/100) ;
								$driver_earning 	= $fare - $driver_commission;
								$amt_percentage 	= '- '.$row2['driver_com_percentage'].'%';
							}
						$data[] = array(
									"id" 				=> $row2['id'],
									"car_type"	 		=> $row2['car_type'],
									"min_fare" 			=> $row2['min_fare'],
									"rate_per_km" 		=> $row2['rate_per_km'],
									"est_fare" 			=> $fare,
									"driver_fare" 		=> number_format($driver_earning,2),
									"amt_percentage" 	=> $amt_percentage
									
								);
					}
				
			}
		echo json_encode($data);
	}
?>