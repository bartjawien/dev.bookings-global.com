<?php
ini_set("display_errors", 1);
require_once('../init.php');
require_once(INCLUDE_PATH . "settings.php");
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "fareEstimator.php");
require_once(CLASSES_PATH . "job.php");
// require_once(CLASSES_PATH . "jobReference.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "mailer.php");
require_once(CLASSES_PATH . "chargeAccount.php");
require_once(CLASSES_PATH . "invoice.php");
require_once(CLASSES_PATH . "vehicle.php");
// require_once(CLASSES_PATH . "driver.php");
require_once(INCLUDE_PATH . "functions_date_time.php");
session_start();
$database = new Database;
$user = new User();
$charge_account = new ChargeAccount();
$invoice = new invoice();
$vehicle = new vehicle();

if(isset($_GET['charge_account_id']) && isset($_GET['from_date']) && isset($_GET['to_date']))
	{
		$charge_account_details = $charge_account->getChargeAccountDetails($_GET['charge_account_id']);
		$query = "
			SELECT 
			jobs.*, 
			jobs.id as job_id,
			job__references.*
			from 
			job as jobs
			INNER JOIN job__reference as job__references ON jobs.job_reference_id = job__references.id
			AND (jobs.job_date BETWEEN '".$_GET['from_date']."' AND '".$_GET['to_date']."')
			WHERE
			jobs.cur_pay_status_id = '1' 
			AND
			jobs.job_status != '100' 
			AND
			job__references.charge_acc_id = '".$_GET['charge_account_id']."'
			order by job_date, job_time ASC";
		
		$data .= '<form method="post" name="make_invoice_form" id="make_invoice_form">
				<table width="100%" class="invoiceTable">
					<tr>
						<th colspan="16">
							Un-Invoiced Jobs for - '.$charge_account_details['account_name'].'
							<input type="hidden" name="charge_account_name" id="charge_account_name" value="'.$charge_account_details['account_name'].'">
						</th>
					</tr>
					<tr>
						<th><input type="checkbox" name="check_all_jobs" id="check_all_jobs"></th>
						<th>Job ID</th>
						<th>Date/Time</th>
						<th>Vehicle</th>
						<th>Pax Name</th>
						<th>From</th>
						<th>To</th>
						<th>Notes</th>
						<th>Fare</th>
						<th>International</th>
						<th>Extra Drop/Pickup</th>
						<th>Waiting Time</th>
						<th>Baby Seat</th>
						<th>After Hours</th>
						<th>Major Event</th>
						<th>Total</th>
					</tr>';
		$result = $database->query($query);
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
			{
				$extra_fare = $row['inter'] + $row['ed'] + $row['wait'] + $row['tolls'] + $row['bs'] + $row['park'] + $row['ah'] + $row['me'] + $row['alc'] + $row['fc'] + $row['oth'];
				
				$data .='<tr>
							<td valign-"top"><input type="checkbox" name="to_invoice_jobs[]" class="to_invoice_jobs" value="'.$row['job_id'].'"></td>
							<td valign="top"><a href="#" id="'.$row['job_id'].'" class="job_id_link">'.$row['job_id'].'</a></td>
							<td valign="top">
								'.calculateTimeInAmPmFormatWithoutSeconds($row['job_time']).'<br/>
								'.formatDate($row['job_date'], 1).'
							</td>
							<td valign="top">';
								$vehicle_details = $vehicle->getCarTypeType($row['car_id']);
								$data .=''.$vehicle_details.'';
							$data .='
							</td>';
							if($row['std_fname'] != '' && $row['std_lname'] != '')
								{
									$data .='<td valign-"top">'.$row['std_title'].' '.$row['std_fname'].' '.$row['std_lname'].'<br/>ID '.$row['std_id'].'</td>';
								}
							else
								{
									$pax_details = $user->getUserDetails($row['pax_id']); // get details of this pax
									$data .='<td valign="top">
												'.$pax_details['title'].' '.$pax_details['fname'].' '.$pax_details['lname'].'';
												if($row['order_ref'] != '')
													{	
														$data .='<br/><b>'.$row['order_ref'].'</b>';
													}
											$data .='	
											</td>';
								}
							$data .='
							<td valign="top">';
								if($row['frm_flight_no']!='')	{ $data.= '<b>Flight No - '.$row['frm_flight_no'].' </b>';}
								if($row['frm_line1']!='')		{ $data.= ''.$row['frm_line1'].'<br/>';}
								if($row['frm_line2']!='')		{ $data.= ''.$row['frm_line2'].'<br/>';}
								if($row['frm_sub']!='')			{ $data.= '<b>'.$row['frm_sub'].'</b>';}
								if($row['frm_pc']!='')			{ $data.= ' '.$row['frm_pc'].'';}
								if($row['frm_state']!='')		{ $data.= ' '.$row['frm_state'].'<br/>';}
								if($row['frm_via_sub'] != '')
									{
										$data.='<strong>2nd PICKUP-DROP OFF</strong><br/>';
										if($row['frm_via_line1']!='')	{ $data.= ''.$row['frm_via_line1'].'';}
										if($row['frm_via_line2']!='')	{ $data.= ''.$row['frm_via_line2'].'<br/>';}
										if($row['frm_via_sub']!='')		{ $data.= '<b>'.$row['frm_via_sub'].'</b> ';}
										if($row['frm_via_pc']!='')		{ $data.= ' '.$row['frm_via_pc'].'</b>';}
										if($row['frm_via_state']!='')	{ $data.= ' '.$row['frm_via_state'].'';}
									}
							$data .='
							</td>
							<td valign="top">';
								if($row['to_flight_no']!='')	{ $data.= 'Flight No - '.$row['to_flight_no'].'';}
								if($row['to_line1']!='')		{ $data.= ''.$row['to_line1'].'<br/>';}
								if($row['to_line2']!='')		{ $data.= ''.$row['to_line2'].'<br/>';}
								if($row['to_sub']!='')			{ $data.= '<b>'.$row['to_sub'].'</b>';}
								if($row['to_pc']!='')			{ $data.= ' '.$row['to_pc'].'';}
								if($row['to_state']!='')		{ $data.= ' '.$row['to_state'].'<br/>';}
								if($row['to_via_sub'] != '')
									{
										$data.='<strong>LAST DROP</strong><br/>';
										if($row['to_via_line1']!='')	{ $data.= ''.$row['to_via_line1'].'<br/>';}
										if($row['to_via_line2']!='')	{ $data.= ''.$row['to_via_line2'].'<br/>';}
										if($row['to_via_sub']!='')		{ $data.= '<b>'.$row['to_via_sub'].'</b>';}
										if($row['to_via_pc']!='')		{ $data.= ' '.$row['to_via_pc'].'';}
										if($row['to_via_state']!='')	{ $data.= ' '.$row['to_via_state'].'';}
									}
									
							$total_fare = $row['fare'] + $extra_fare;
							$data .='
							</td>
							<td style="text-align:right;" valign="bottom">'.$row['ext_notes'].'</td>
							<td style="text-align:right;" valign="bottom">$'.number_format($row['fare'],2).'</td>
							<td style="text-align:right;" valign="bottom">$'.number_format($row['inter'],2).'</td>
							<td style="text-align:right;" valign="bottom">$'.number_format($row['ed'],2).'</td>
							<td style="text-align:right;" valign="bottom">$'.number_format($row['wait'],2).'</td>
							<td style="text-align:right;" valign="bottom">$'.number_format($row['bs'],2).'</td>
							<td style="text-align:right;" valign="bottom">$'.number_format($row['ah'],2).'</td>
							<td style="text-align:right;" valign="bottom">$'.number_format($row['me'],2).'</td>
							<td style="text-align:right;" valign="bottom"><b>$'.number_format($total_fare,2).'</b></td>';
						$data .='
						</tr>';
			}
		$data .= '</table>
				<br/>
				<table width="60%" class="invoiceTable">
					<tr>	
						<th colspan="4">SELECT RECEIVERS</th>
					</tr>
					<tr>
						<th><input type="checkbox" name="check_all_receivers" id="check_all_receivers"></th>
						<th>Contact Type</th>
						<th>Name</th>
						<th>Email</th>
					</tr>
					';
					$query = "SELECT * 
								FROM 
								charge_acc__contacts, user
								WHERE
								charge_acc__contacts.user_id = user.id
								AND
								charge_acc__contacts.charge_acc_id = '".$_GET['charge_account_id']."'
								AND 
									(
										charge_acc__contacts.type_id = '1'
										OR
										charge_acc__contacts.type_id = '3'
										OR
										charge_acc__contacts.type_id = '4'
									)
								ORDER BY
								user.fname ASC";
					$result = $database->query($query);
					while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
						{
							if($row['type_id'] == '1') { $role = 'ALL'; }
							if($row['type_id'] == '3') { $role = 'BOOKING ONLY'; }
							if($row['type_id'] == '4') { $role = 'BILLING ONLY'; }
							$data.='
									<tr>
										<td valign-"top"><input type="checkbox" name="to_receivers[]" class="to_receivers" value="'.$row['user_id'].'"></td>
										<td style="border: 1px #336699 solid;" valign="bottom">
											'.$role.'
										</td>
										<td style="border: 1px #336699 solid;" valign="bottom">
											'.$row['title'].' '.$row['fname'].' '.$row['lname'].'
										</td>
										<td style="border: 1px #336699 solid;" valign="bottom">
											'.$row['email'].'
										</td>
									</tr>';	
						}
					$data.='
				</table>
				<table width="60%" class="invoiceTable">
					<tr>	
						<th colspan="3">ADDITIONAL RECEIVERS</th>
					</tr>
					<tr>
						<td></td>
						<td>FULL NAME WITH TITLE</td>
						<td>EMAIL ADDRESS</td>
					</tr>
					<tr>
						<td>1</td>
						<td><input type="text" name="add_receiver_1_name" id="add_receiver_1_name" size="34" palceholder="Full Name"></td>
						<td><input type="text" name="add_receiver_1_email" id="add_receiver_1_email" size="34" palceholder="Email"></td>
					</tr>
					<tr>
						<td>2</td>
						<td><input type="text" name="add_receiver_2_name" id="add_receiver_2_name" size="34" palceholder="Full Name"></td>
						<td><input type="text" name="add_receiver_2_email" id="add_receiver_2_email" size="34" palceholder="Email"></td>
					</tr>
				</table>
				<table width="60%" class="invoiceTable">
					<tr>	
						<th colspan="2">ADDITIONAL INFORMATION</th>
					</tr>
					<tr>
						<td>Status</td>
						<td>
							<select name="invoice_status" id="invoice_status">
								<option value="1">Payable</option>
								<option value="2">Paid</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Invoice Date</td>
						<td>
							<input type="checkbox" name="invoice_date_today" id="invoice_date_today" checked> TODAY<br/>
							OR add Manual Invoice Date<br/>
							<input type="text" name="manual_invoice_date" id="manual_invoice_date" palceholder="Invoice Date">
							<input type="hidden" name="hidden_manual_invoice_date" id="hidden_manual_invoice_date">
						</td>
					</tr>
					<tr>
						<td>Invoice Due Date<br/>(from invoice date)</td>
						<td>
							<select name="invoice_payable" id="invoice_payable">
								<option value="0">IMMIDIATE</option>
								<option value="7">7 Days</option>
								<option value="14" selected>14 Days</option>
								<option value="30">30 Days</option>
								<option value="">Not Applicable as Paid</option>
							</select><br/>
							OR Manual Due Date:<br/>
							<input type="text" name="manual_invoice_due_date" id="manual_invoice_due_date" palceholder="Due Date">
							<input type="hidden" name="hidden_manual_invoice_due_date" id="hidden_manual_invoice_due_date">
						</td>
					</tr>
					<tr>
						<td>Add Card Surcharge</td>
						<td>
							<input type="text" name="card_surcharge" id="card_surcharge"><b>%</b>
						</td>
					</tr>
					<tr>
						<td valign="top"><b>Embed Message in this Invoice</b><br/><i>This will be shown in color red at the top of invoice items.</i></td>
						<td>
							<textarea name="embed_message" id="embed_message" cols="30" rows="5"></textarea>	
						</td>
					</tr>
					<tr>
						<td valign="top"><b>Email Text</b><br/><i>Do not change text within *** *** as they are dynamic values.</i></td>
						<td>
							<textarea name="email_message" id="email_message" cols="30" rows="5">Dear ***RECEIVERS_NAME***,<br/>Please find the Invoice Number ***NEXT_INVOICE_ID*** enclosed for the work done by us.<br/>Thanks<br/>Allied Cars Admin</textarea>	
						</td>
					</tr>
					<tr>
						<td valign="top"><b>PURCHASE ORDER NUMBER</b></td>
						<td>
							<input type="text" name="po_number" id="po_number" size="34">
						</td>
					</tr>
				</table>
				<br/>
				<br/>				
				<input type="button" id="preview_invoice" name="preview_invoice" class="approve_button" value="PREVIEW INVOICE">
				</form>
				<script>
				$("#manual_invoice_date" ).datepicker({
							changeMonth: true,
							changeYear: true,
							dateFormat: "dd-mm-yy",
							altField: "#hidden_manual_invoice_date",
							altFormat: "yy-mm-dd"
						});
						
						$("#manual_invoice_due_date" ).datepicker({
							changeMonth: true,
							changeYear: true,
							dateFormat: "dd-mm-yy",
							altField: "#hidden_manual_invoice_due_date",
							altFormat: "yy-mm-dd"
						});
				</script>';
		$a = array('data' => ''.$data.'');
		$json = json_encode($a); 
		echo $json;
	}
	
if(isset($_GET['to_rec']) && isset($_GET['to_inv_jobs']))
	{
		$query = "select * from settings__business";
		$result = $database->query($query);
		$row = mysql_fetch_array($result);
		
		$total_fare 			= 	0;
		$surcharge				=	0;
		$embed_message 			= 	$_GET['embed_message'];
		$email_message 			= 	$_GET['email_message'];
		$add_receiver_1_email 	= 	$_GET['add_receiver_1_email'];
		$add_receiver_1_name 	= 	$_GET['add_receiver_1_name'];
		$add_receiver_2_email 	= 	$_GET['add_receiver_2_email'];
		$add_receiver_2_name 	= 	$_GET['add_receiver_2_name'];

		$myString 	= $_GET['to_inv_jobs'];
		$myArray 	= explode(',', $myString);

		foreach($myArray as $k=>$v)
			{
				$query1 	= 	"Select tot_fare from job where id=".$v."";
				$result1 	= 	$database->query($query1);
				$row1 = mysql_fetch_array($result1);
				$total_fare = $total_fare + $row1['tot_fare'];
			}
		if($_GET['card_surcharge'] == '0' || $_GET['card_surcharge'] == '')
			{
				$card_surcharge = FALSE;
				$fare_after_surcharge = $total_fare;
			}
		if($_GET['card_surcharge'] != '')
			{
				$card_surcharge = TRUE;
				$card_surcharge_text 	= 	'CARD SURCHARGE @'.number_format($_GET['card_surcharge'],2).'%';
				$surcharge 				= 	$total_fare * ($_GET['card_surcharge'] / 100);
				$fare_after_surcharge 	= 	$total_fare + $surcharge;
			}
			
		if($_GET['manual_invoice_date'] != '')
			{
				$invoice_date 		= 	$_GET['manual_invoice_date'];
				$invoice_db_date 	=	date('Y-m-d', strtotime($invoice_date));
			}
		else
			{
				$invoice_date = date("d-m-Y");
				$invoice_db_date 	=	date('Y-m-d', strtotime($invoice_date));
			}

		if($_GET['invoice_status'] == '1')//Due and payable
			{
				if($_GET['manual_invoice_due_date'] !='') //Use this date as due date for invoice
					{
						$due_date 		=	$_GET['manual_invoice_due_date'];
						$db_due_date 	=	date('Y-m-d', strtotime($_GET['manual_invoice_due_date']));
					
					}
				if($_GET['manual_invoice_date'] !='' && $_GET['manual_invoice_due_date'] =='') // calculate the due date based on 
					{
						if($_GET['invoice_payable'] == '0')
							{
								$due_date 		=	date('d-m-Y', strtotime("".$_GET['manual_invoice_date']."+0 days"));
								$db_due_date 	=	date('Y-m-d', strtotime("".$_GET['manual_invoice_date']."+0 days"));
							}
						if($_GET['invoice_payable'] == '7')
							{
								$due_date 		=	date('d-m-Y', strtotime("".$_GET['manual_invoice_date']."+7 days"));
								$db_due_date 	=	date('Y-m-d', strtotime("".$_GET['manual_invoice_date']."+7 days"));
							}
						if($_GET['invoice_payable'] == '14')
							{
								$due_date 		=	date('d-m-Y', strtotime("".$_GET['manual_invoice_date']."+14 days"));
								$db_due_date 	=	date('Y-m-d', strtotime("".$_GET['manual_invoice_date']."+14 days"));
							}
						if($_GET['invoice_payable'] == '30')
							{
								$due_date 		=	date('d-m-Y', strtotime("".$_GET['manual_invoice_date']."+30 days"));
								$db_due_date 	=	date('Y-m-d', strtotime("".$_GET['manual_invoice_date']."+30 days"));
							}
					}
				if($_GET['manual_invoice_date'] =='' && $_GET['manual_invoice_due_date'] =='') // if both manual dates are empty
					{
						if($_GET['invoice_payable'] == '0')
							{
								$due_date 		=	date('d-m-Y', strtotime("+0 days"));
								$db_due_date 	=	date('Y-m-d', strtotime("+0 days"));
							}
						if($_GET['invoice_payable'] == '7')
							{
								$due_date 		=	date('d-m-Y', strtotime("+7 days"));
								$db_due_date 	=	date('Y-m-d', strtotime("+7 days"));
							}
						if($_GET['invoice_payable'] == '14')
							{
								$due_date 		=	date('d-m-Y', strtotime("+14 days"));
								$db_due_date 	=	date('Y-m-d', strtotime("+14 days"));
							}
						if($_GET['invoice_payable'] == '30')
							{
								$due_date 		=	date('d-m-Y', strtotime("+30 days"));
								$db_due_date 	=	date('Y-m-d', strtotime("+30 days"));
							}
					}
			}
		if($_GET['invoice_status'] == '2')//Invoice already paid
			{
				$due_date .='PAID';
			}
		$data .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
					<title>Invoice Number ###NEXT_INVOICE_ID### </title>
					<meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
					<style type="text/css">
						.customer_name 		{color:#00557F; font-size:16px; font-weight:bold; text-transform:uppercase;}
						.invoice_message 	{color:red; font-size:13px; font-weight:bold;}
						.invoice_amount { padding: 5px; font-size: 13px; font-weight:bold; } 
						
						.box table { border-collapse: collapse; text-align: left; width: 100%; }
						.box {font: normal 12px Arial; background: #fff; }
						.box table td, .datagrid table th { padding: 4px 5px; }
						.box table thead th {background-color:#006699; color:#FFFFFF; font-size: 14px; font-weight: bold; border: 1px solid #0070A8; } 
						.box table tbody td { color: #00557F; border: 1px solid #E1EEF4;font-size: 11px;font-weight: normal; }
						.box table tbody .alt td { background: #E1EEf4; color: #00557F; }
						
						.box table tbody td:first-child { border-left: none; }
						.box table tbody tr:last-child td { border-bottom: none; }
						.box table tfoot td div { border-top: 1px solid #006699;background: #E1EEf4;} 
						.box table tfoot td { padding: 0; font-size: 12px } 
						.box table tfoot td div{ padding: 5px; }
						
						
						.datagrid table { border-collapse: collapse; text-align: left; width: 100%; }
						.datagrid {font: normal 12px Arial; background: #fff; }
						.datagrid table td, .datagrid table th { padding: 4px 5px; }
						.datagrid table thead th {background-color:#006699; color:#FFFFFF; font-size: 12px; font-weight: bold; border: 1px solid #0070A8; } 
						.datagrid table tbody td { color: #00557F; border: 1px solid #E1EEF4;font-size: 12px; font-weight: normal; }
						.datagrid table tbody .alt td { background: #E1EEf4; color: #00557F; }
						.datagrid table tbody .alt1 td { background: #ebfdfd; color: #00557F; }
						.datagrid table tbody td:first-child { border-left: none; }
						.datagrid table tbody tr:last-child td { border-bottom: none; }
						.datagrid table tfoot td div { border-top: 1px solid #006699;background: #E1EEf4;} 
						.datagrid table tfoot td { padding: 0; font-size: 12px } 
						.datagrid table tfoot td div{ padding: 5px; }
						#GST_TOTAL
								{
									font-size: 14px;
									text-align:center;
									font-weight:bold;
								}
						#THANK_YOU
							{
								font-size: 12px;
								font-weight:bold;
								text-align:center;
							}
						#DOTTED_LINE
							{
								border:1px dashed #006666; 
							}

					</style>
				</head>
				<body>
					<table style="width:190mm;">
						<tr>
							<td style="width:30%;" valign="top">
								<table><tr><td><img src="'.IMAGES_PATH.'logo_Allied.png" height="60px" width="140px" alt="logo"/></td></tr></table>
							</td>
							<td style="width:30%;">
								<table><tr><td></td></tr></table>
							</td>
							<td style="width:40%;">
								<div class="box">
									<table style="width:100%;">
										<thead>
											<tr><th colspan="2">Tax Invoice</th></tr>
										</thead>
										<tbody>
											<tr><td style="width:40%;">ABN:</td><td style="width:60%;">'.$row['abn'].'</td></tr>
											<tr class="alt"><td style="width:40%;">Address:</td><td style="width:60%;">'.$row['address'].'</td></tr>
											<tr><td  style="width:40%;">Phone No:</td><td style="width:60%;">'.$row['ph_1'].' '.$row['ph_2'].'</td></tr>
											<tr class="alt"><td style="width:40%;">Email:</td><td style="width:60%;">'.$row['email'].'</td></tr>
											<tr><td style="width:40%;">Website:</td><td style="width:60%;">'.$row['website'].'</td></tr>
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</table>
					<table style="width:190mm;">
						<tr>
							<td style="width:30%;" valign="top">
								<div class="box">
									<table style="width:100%;">
										<thead>
											<tr><th style="width:100%;">Invoice To:</th></tr>
										</thead>
										<tbody>
											<tr>
												<td style="width:100%;">
													<span class="customer_name">'.$_GET['charge_account_name'].'</span>';
													if($_GET['po_number'] != '')
														{
															$data .= '<br/>Purchase Order Number - <b>'.$_GET['po_number'].'</b>';
														}
												$data .= '
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</td>
							<td style="width:30%;">
								<table style="width:100%;"><tr><td></td></tr></table>
							</td>
							<td style="width:40%;">
								<div class="box">
									<table style="width:100%;">
										<tbody>
											<tr class="alt"><td style="width:40%;">Invoice No:</td><td style="width:60%;">###NEXT_INVOICE_ID###</td></tr>
											<tr><td style="width:40%;">Invoice Date:</td><td style="width:60%;">'.$invoice_date.'</td></tr>
											<tr class="alt"><td style="width:40%;">Due Date:</td><td style="width:60%;"><b>'.$due_date.'</b></td></tr>
											<tr><td style="width:40%;"><span class="invoice_amount">Invoice Amount:</span></td><td style="width:60%;"><span class="invoice_amount">$'.number_format($fare_after_surcharge,2).'</span></td></tr>
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</table>';
			if($embed_message != '')
				{
					$data .='
					<table style="width:190mm;">
						<tr>
							<td style="width:100%;" valign="top">
								<div class="box">
									<table style="width:100%;">
										<tbody>
											<tr><td style="width:100%;"><span class="invoice_message">'.$embed_message.'</span></td></tr>
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</table>';
				}
					$data .='
					<table style="width:190mm;">
						<tr>
							<td style="width:100%;" valign="top">
								<div class="datagrid">
									<table style="width:100%;">
										<thead>
											<tr>
												<th style="width:10%;">Job#</th>
												<th style="width:10%;">Reference</th>
												<th style="width:50%;">Description</th>
												<th style="width:10%;">Fare</th>
												<th style="width:10%;">Extras</th>
												<th style="width:10%;">Total</th>
											</tr>
										</thead>
										<tbody>';
									$total_grand_fare = 0;
									$alt = 0;
									foreach($myArray as $key=>$value)
										{
											$query2 = "
														SELECT 
														jobs.*, 
														jobs.id as job_id,
														job__references.*
														from 
														job as jobs
														INNER JOIN job__reference as job__references ON jobs.job_reference_id = job__references.id
														WHERE
														jobs.id = '".$value."'";
											$result2 	= 	$database->query($query2);
											$row2 = mysql_fetch_array($result2);
											
											$vehicle_details = $vehicle->getCarTypeType($row2['car_id']);
											$extra_fare = $row2['inter'] + $row2['ed'] + $row2['wait'] + $row2['tolls'] + $row2['bs'] + $row2['park'] + $row2['ah'] + $row2['me'] + $row2['alc'] + $row2['fc'] + $row2['oth'];
											$total_fare = $row2['fare'] + $extra_fare;
											$pax_details = $user->getUserDetails($row2['pax_id']); // get details of this pax
											if($alt % 2 == 0)
												{
													$data .='
													<tr>';
												}
											else
												{
													$data .='
													<tr class="alt1">';	
												}
											$data .='
												<td style="width:10%;">'.$row2['job_id'].'</td>
												<td style="width:10%;">'.$row2['order_ref'].'</td>
												<td style="width:50%;">
													<b>'.calculateTimeInAmPmFormatWithoutSeconds($row2['job_time']).' '.formatDate($row2['job_date'], 1).' ('.$vehicle_details.')</b>
													<i>'.$pax_details['title'].' '.$pax_details['fname'].' '.$pax_details['lname'].'</i><br/>';
													/*
													if($row2['frm_flight_no'] != '')
														{
															$data.='<b>'.$row2['frm_flight_no'].'</b><br/>';
														}
													*/
													
													if($row2['frm_sub']!='')		{ $data.= ' '.$row2['frm_sub'].'';}
													if($row2['frm_state']!='')		{ $data.= ' '.$row2['frm_state'].' &gt; ';}
													if($row2['frm_via_sub'] != '')
														{
															if($row2['frm_via_sub']!='')	{ $data.= ' '.$row2['frm_via_sub'].'';}
															if($row2['frm_via_state']!='')	{ $data.= ' '.$row2['frm_via_state'].' &gt; ';}
														}
													if($row2['to_sub']!='')			{ $data.= ''.$row2['to_sub'].'';}
													if($row2['to_state']!='')		{ $data.= ' '.$row2['to_state'].'';}
													if($row2['to_via_sub'] != '')
														{
															$data.=' &gt; ';
															if($row2['to_via_sub']!='')		{ $data.= ' '.$row2['to_via_sub'].'';}
															if($row2['to_via_state']!='')	{ $data.= ' '.$row2['to_via_state'].'';}
														}
													if($row2['ext_notes']!='')	{ $data.= '<br/><b>Notes: </b>'.$row2['ext_notes'].'';}
													if($extra_fare!='0.00')		{ $data.= '<br/><b>Extras:</b>';}
													if($row2['inter']!='0.00')	{ $data.= '<br/>International: $'.$row2['inter'].'';}
													if($row2['ed']!='0.00')		{ $data.= '<br/>Extra Drop/Pickup: $'.$row2['ed'].'';}
													if($row2['wait']!='0.00')	{ $data.= '<br/>Waiting Time: $'.$row2['wait'].'';}
													if($row2['park']!='0.00')	{ $data.= '<br/>Additional Parking: $'.$row2['park'].'';}
													if($row2['tolls']!='0.00')	{ $data.= '<br/>Tolls: $'.$row2['tolls'].'';}
													if($row2['bs']!='0.00')		{ $data.= '<br/>Baby Seats: $'.$row2['bs'].'';}
													if($row2['ah']!='0.00')		{ $data.= '<br/>After Hours: $'.$row2['ah'].'';}
													if($row2['me']!='0.00')		{ $data.= '<br/>Major Events: $'.$row2['me'].'';}
													if($row2['alc']!='0.00')	{ $data.= '<br/>Admin Fee: $'.$row2['alc'].'';}
													if($row2['fc']!='0.00')		{ $data.= '<br/>Other: $'.$row2['fc'].'';}
													if($row2['oth']!='0.00')	{ $data.= '<br/>Credit Card Surcharge: $'.$row2['oth'].'';}
												$data.= '	
												</td>
												<td style="width:10%;text-align:right;">$'.number_format($row2['fare'],2).'</td>
												<td style="width:10%;text-align:right;">$'.number_format($extra_fare,2).'</td>
												<td style="width:10%;text-align:right;">$'.number_format($total_fare,2).'</td>
											</tr>';
											$total_base_fare 	= 	$total_base_fare + $row2['fare'];
											$total_extras 		= 	$total_extras + $extra_fare;
											$total_grand_fare 	= 	$total_grand_fare + $total_fare;
											$alt++;
										}
											$data .='
											<tr class="alt">
												<td style="width:90%;text-align:right;" colspan="5"><b>TOTALS</b></td>
												<td style="width:10%;text-align:right;"><b>$'.number_format($total_grand_fare,2).'</b></td>
											</tr>';
									if($card_surcharge)
										{
											$data .='
											<tr class="alt">
												<td style="width:90%;text-align:right;" colspan="5"><b>'.$card_surcharge_text.'</b></td>
												<td style="width:10%;text-align:right;" text-align="right"><b>$'.number_format($surcharge,2).'</b></td>
											</tr>
											<tr class="alt">
												<td style="width:90%;text-align:right;" colspan="5"><b>TOTAL FOR THIS INVOICE</b></td>
												<td style="width:10%;text-align:right;"><b>$'.number_format($fare_after_surcharge,2).'</b></td>
											</tr>';
										}
									else
										{
											$fare_after_surcharge = $total_grand_fare;
										}
									$data .='
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</table>
					<table style="width:190mm;">
						<tr>
							<td style="width:100%;" valign="top">';
								$GST 		= $fare_after_surcharge / 11;
								$data .='
								<div id="GST_TOTAL">This invoice includes a total GST of $'.number_format($GST,2).'.</div>
								<div id="DOTTED_LINE"></div>
							</td>
						</tr>
					</table>
					<table style="width:190mm;">
						<tr>
							<td>
								<div class="box">
									<table style="width:100%;">';
								if($invoice_payable != 'PAID')
									{
										$data .= '
										<thead>
											<tr>
												<th style="width:33.3%;">Remittance Advice</th>
												<th style="width:33.3%;">Customer Name:<br/>'.$_GET['charge_account_name'].'</th>
												<th style="width:33.3%;">TAX INVOICE#  ###NEXT_INVOICE_ID### </th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td colspan="2" style="width:66.6%;">Total Invoice Amount</td>
												<td style="width:33.3%;">$'.number_format($fare_after_surcharge,2).'</td>
											</tr>
											<tr>
												<td colspan="3" style="width:100%;">PAYMENT METHODS</td>
											</tr>
											<tr class="alt">
												<td style="width:33.3%;">EFT</td>
												<td style="width:33.3%;">CREDIT CARD</td>
												<td style="width:33.3%;">CHEQUES</td>
											</tr>
											<tr>
												<td valign="top" style="width:33.3%;">'.$row['account_name'].'<br/>
													BSB:'.$row['bsb'].'<br/>
													ACC:'.$row['account_number'].'<br/>
													REF: ###NEXT_INVOICE_ID### 
												</td>
												<td valign="top" style="width:33.3%;">Please call us with your card details.</td>
												<td valign="top" style="width:33.3%;">Please make cheques payable to:<br/>
													'.$row['cheque_payable_to'].'<br/>
													'.$row['address'].'
												</td>
											</tr>';
										}
									else
										{
											$data.= '
											<tr><td><b>THIS INVOICE HAS BEEN PAID. THANKS</b></td></tr>';
										}
										$data.= '
										</tbody>
									</table>
								</div>
							</td>
						</tr>
					</table>
				</body>
				</html>';
		session_start();
		$_SESSION['html_data'] =  gzcompress($data);

		$a = array(
					'data' 					=> 	''.$data.'',
					'charge_account_id' 	=> 	''.$_GET['charge_account_id'].'',
					'invoice_status' 		=> 	''.$_GET['invoice_status'].'',
					'invoice_date' 			=> 	''.$invoice_db_date.'',
					'invoice_due_date' 		=> 	''.$db_due_date.'',
					'total_base_fare' 		=> 	''.$total_base_fare.'',
					'total_extras'			=> 	''.$total_extras.'',
					'total_before_surcharge'=> 	''.$total_grand_fare.'',
					'card_surcharge_percent'=> 	''.$_GET['card_surcharge'].'',
					'card_surcharge' 		=> 	''.$surcharge.'',
					'total_after_surcharge'	=> 	''.$fare_after_surcharge.'',
					'gst' 					=> 	''.$GST.'',
					'jobs' 					=> 	''.$_GET['to_inv_jobs'].'',
					'receivers' 			=> 	''.$_GET['to_rec'].'',
					'add_receiver_1_name' 	=> 	''.$_GET['add_receiver_1_name'].'',
					'add_receiver_1_email' 	=> 	''.$_GET['add_receiver_1_email'].'',
					'add_receiver_2_name' 	=> 	''.$_GET['add_receiver_2_name'].'',
					'add_receiver_2_email' 	=> 	''.$_GET['add_receiver_2_email'].'',
					'embed_message' 		=> 	''.$_GET['embed_message'].'',
					'email_message' 		=> 	''.$_GET['email_message'].'',
					'po_number' 			=> 	''.$_GET['po_number'].''
				);
		$json = json_encode($a); 
		echo $json;
	}
if(isset($_GET['send_now']) || isset($_GET['save_only']))
	{
		require_once('../html2pdf/html2pdf.class.php');
		
		if($_GET['save_only'] == '1')
			{
				$query = "INSERT INTO invoice 
							(id, created_on, invoice_date, due_on, charge_acc_id, status_id, sent_status_id, base_fare, extras, card_surcharge_percent, card_surcharge, total_amount, gst, embed_message, email_message, po_number) VALUES
							(NULL, CURRENT_TIMESTAMP, '".$_GET['invoice_date']."', '".$_GET['invoice_due_date']."', '".$_GET['charge_account_id']."', '".$_GET['invoice_status']."', '1', '".$_GET['total_base_fare']."', '".$_GET['total_extras']."', '".$_GET['card_surcharge_percent']."', '".$_GET['card_surcharge']."', '".$_GET['total_after_surcharge']."', '".$_GET['gst']."', '".$_GET['embed_message']."', '".$_GET['email_message']."', '".$_GET['po_number']."')";
				$result = $database->query($query);
				$this_invoice_id = mysql_insert_id();
				$invoice->addInvoiceLog($this_invoice_id, ''.$_SESSION['TITLE'].' '.$_SESSION['FNAME'].' '.$_SESSION['LNAME'].'', 'New Invoice created id '.$this_invoice_id.'', '', '');
				$invoice->addInvoiceLog($this_invoice_id, ''.$_SESSION['TITLE'].' '.$_SESSION['FNAME'].' '.$_SESSION['LNAME'].'', 'Invoice Saved', '', '');
			}
		if($_GET['send_now'] == '1')
			{
				$query = "INSERT INTO invoice 
							(id, created_on, invoice_date, due_on, charge_acc_id, status_id, sent_status_id, base_fare, extras, card_surcharge_percent, card_surcharge, total_amount, gst, embed_message, email_message, po_number) VALUES
							(NULL, CURRENT_TIMESTAMP, '".$_GET['invoice_date']."', '".$_GET['invoice_due_date']."', '".$_GET['charge_account_id']."', '".$_GET['invoice_status']."', '2', '".$_GET['total_base_fare']."', '".$_GET['total_extras']."', '".$_GET['card_surcharge_percent']."', '".$_GET['card_surcharge']."', '".$_GET['total_after_surcharge']."', '".$_GET['gst']."', '".$_GET['embed_message']."', '".$_GET['email_message']."', '".$_GET['po_number']."')";
				$result = $database->query($query);
				$this_invoice_id = mysql_insert_id();
				$invoice->addInvoiceLog($this_invoice_id, ''.$_SESSION['TITLE'].' '.$_SESSION['FNAME'].' '.$_SESSION['LNAME'].'', 'New Invoice created id '.$this_invoice_id.'', '', '');
			}

		//insert invoice items in invoice__items table
		$jobs_array 	= explode(',', $_GET['jobs']);
		foreach($jobs_array as $k=>$v)
			{
				$query1 = "INSERT INTO invoice__items
					(id, created_on, invoice_id, job_id) VALUES
					(NULL,CURRENT_TIMESTAMP, '".$this_invoice_id."', '".$v."')";
				$result1 = $database->query($query1);
				
				$query2 = "UPDATE job SET cur_pay_status_id = '2' WHERE id =  '".$v."'";
				$result2 = $database->query($query2);
			}

		$html= gzuncompress($_SESSION['html_data']); 
		$html= str_replace('###NEXT_INVOICE_ID###', $this_invoice_id, $html);
		
		$myFile = "../invoices/".$this_invoice_id.".html";
		$fh = fopen($myFile, 'w') or die("can't open file");
		fwrite($fh, $html);
		fclose($fh);

		ob_start();
		include('../invoices/'.$this_invoice_id.'.html');
		$content = ob_get_clean();
		
		$html2pdf = new HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array('10','5','5','10'));
		$html2pdf->setDefaultFont('arial');
		//$html2pdf->pdf->SetMargins(20,18);
		//$html2pdf->pdf->SetTopMargin(100);
		//$html2pdf->pdf->SetDisplayMode('fullpage');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$pdfdoc = $html2pdf->Output('../invoices/'.$this_invoice_id.'.pdf' , 'F');
		
		
		if($_GET['send_now'] == '1')
			{
				$mailer = new mailer();
				$email_body_with_invoice_id = str_replace('***NEXT_INVOICE_ID***', $this_invoice_id, $_GET['email_message']);
				
				$receivers_id 	= explode(',', $_GET['receivers']);
				if (!empty( $_GET['receivers']))
					{
						foreach($receivers_id as $k=>$v)
							{
								$query1 = "SELECT * from user where id = '".$v."'";
								$result1 = $database->query($query1);
								$row1 = mysql_fetch_assoc($result1);
								$full_name = "".$row1['title']." ".$row1['fname']." ".$row1['lname']."";
								
								$query2 = "INSERT INTO invoice__sent
									(id, created_on, invoice_id, sent_to, email) VALUES
									(NULL,CURRENT_TIMESTAMP, '".$this_invoice_id."', '".$full_name."', '".$row1['email']."')";
								
								$result2 = $database->query($query2);
								
								$email_body_with_name = str_replace('***RECEIVERS_NAME***', $full_name, $email_body_with_invoice_id);
								$mailer->sendInvoice($full_name, $row1['email'], $email_body_with_name, $this_invoice_id);
								
								$invoice->addInvoiceLog($this_invoice_id, ''.$_SESSION['TITLE'].' '.$_SESSION['FNAME'].' '.$_SESSION['LNAME'].'', 'Invoice Sent to '.$row1['email'].'', '', '');
								$message .= 'Invoice emailed to '.$full_name.' -  '.$row1['email'].'<br/>';
							}
					}
				if($_GET['add_receiver_1_email'] !='')
					{
						$query3 = "INSERT INTO invoice__sent
							(id, created_on, invoice_id, sent_to, email) VALUES
							(NULL,CURRENT_TIMESTAMP, '".$this_invoice_id."', '".$_GET['add_receiver_1_name']."', '".$_GET['add_receiver_1_email']."')";
							
						$result3 = $database->query($query3);
						$email_body_with_name = str_replace('***RECEIVERS_NAME***', $_GET['add_receiver_1_name'], $email_body_with_invoice_id);
						$mailer->sendInvoice($_GET['add_receiver_1_name'], $_GET['add_receiver_1_email'], $email_body_with_name, $this_invoice_id);
						
						$invoice->addInvoiceLog($this_invoice_id, ''.$_SESSION['TITLE'].' '.$_SESSION['FNAME'].' '.$_SESSION['LNAME'].'', 'Invoice Sent to '.$_GET['add_receiver_1_email'].'', '', '');
						
						$message .= 'Invoice emailed to '.$_GET['add_receiver_1_name'].' -  '.$_GET['add_receiver_1_email'].'<br/>';
					}
					
				if($_GET['add_receiver_2_email'] !='')
					{
						$query4 = "INSERT INTO invoice__sent
							(id, created_on, invoice_id, sent_to, email) VALUES
							(NULL,CURRENT_TIMESTAMP, '".$this_invoice_id."', '".$_GET['add_receiver_2_name']."', '".$_GET['add_receiver_2_email']."')";
							
						$result4 = $database->query($query4);
						$email_body_with_name = str_replace('***RECEIVERS_NAME***', $_GET['add_receiver_2_name'], $email_body_with_invoice_id);
						$mailer->sendInvoice($_GET['add_receiver_2_name'], $_GET['add_receiver_2_email'], $email_body_with_name, $this_invoice_id);
						
						$invoice->addInvoiceLog($this_invoice_id, ''.$_SESSION['TITLE'].' '.$_SESSION['FNAME'].' '.$_SESSION['LNAME'].'', 'Invoice Sent to '.$_GET['add_receiver_2_email'].'', '', '');
						
						$message .= 'Invoice emailed to '.$_GET['add_receiver_2_name'].' -  '.$_GET['add_receiver_2_email'].'<br/>';
					}
				$message .= "<b>INVOICE SAVED AND SENT</b><br/>THIS INVOICE ID - <b>".$this_invoice_id."</b>";
			}
		else
			{
				$message .= "<b>INVOICE SAVED</b><br/>THIS INVOICE ID - <b>".$this_invoice_id."</b>";
			}
		$a = array('data' => ''.$message.'');
		$json = json_encode($a); 
		echo $json;
	}
?>