<?php
ini_set("display_errors", 1);
require_once('../init.php');
require_once(INCLUDE_PATH . "settings.php");
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "fareEstimator.php");
require_once(CLASSES_PATH . "job.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "invoice.php");
require_once(CLASSES_PATH . "mailer.php");
require_once(CLASSES_PATH . "chargeAccount.php");
require_once(INCLUDE_PATH . "functions_date_time.php");
session_start();
$database = new Database;
$user = new User();
$charge_account = new ChargeAccount();
$invoice = new invoice();

if(isset($_GET['charge_account_id']) || (isset($_GET['from_date']) && isset($_GET['to_date'])))
	{
		$where .= 'WHERE ';
		if($_GET['charge_account_id'] !='')
			{
				if($where == 'WHERE '){}
				$where .= "charge_acc_id = '".$_GET['charge_account_id']."' ";
			}
			
		if($_GET['date_type'] == '1') //created on
			{
				if($_GET['from_date'] !='' && $_GET['to_date'] !='')
					{
						$from_date 	= 	"".$_GET['from_date']." 00:00:00";
						$to_date 	= 	"".$_GET['to_date']." 23:59:59";
						$where 		.= 	"AND (invoice.created_on BETWEEN '".$from_date."' AND '".$to_date."') ";
					}
			}
		if($_GET['date_type'] == '2') //due_on
			{
				if($_GET['from_date'] !='' && $_GET['to_date'] !='')
					{
						$where .= "AND (invoice.due_on BETWEEN '".$_GET['from_date']."' AND '".$_GET['to_date']."') ";
					}
			}
			
		if($_GET['invoice_status'] !='')
			{
				$where .= "AND invoice.status_id = '".$_GET['invoice_status']."' ";
			}
			
		if($_GET['invoice_sent_status'] !='')
			{
				$where .= "AND invoice.sent_status_id = '".$_GET['invoice_sent_status']."' ";
			}
		if($where == 'WHERE '){$where ='';}
		$charge_account_details = $charge_account->getChargeAccountDetails($_GET['charge_account_id']);
		$query = "SELECT 
				invoice.*, 
				invoice__sent_status.details as invoice_sent_status,
				invoice__status.details as invoice_status
				from invoice
				LEFT JOIN invoice__sent_status ON invoice.sent_status_id = invoice__sent_status.id
				LEFT JOIN invoice__status ON invoice.status_id = invoice__status.id
				".$where."
				ORDER BY id DESC";
		
		$data .= '<table width="100%" class="invoiceTable">
					<tr>
						<th colspan="18">INVOCIES FOR - '.$charge_account_details['account_name'].'</th>
					</tr>
					<tr>
						<th>Invoice ID</th>
						<th>Created on</th>
						<th>Invoice Date</th>
						<th>Due Date</th>
						<th>Status</th>
						<th>Sent Status</th>
						<th>Charge Account</th>
						<th>PO No.</th>
						<th>Base Fare</th>
						<th>Extras</th>
						<th>Card Surcharge</th>
						<th>GST</th>
						<th>Total Amount</th>
						<th>Total Paid</th>
						<th>Balance</th>
						<th>Emb. Message</th>
						<th>Invoice Actions</th>
					</tr>';
		$result = $database->query($query);
		$invoices_fare = 0;
		$invoices_extras = 0;
		$invoices_card_surcharge = 0;
		$invoices_total = 0;
		$invoices_total_gst = 0;
		$total_paid = 0;
		$total_balance = 0;
		
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
			{
				$query1 = "SELECT sum(amount_paid) as total_paid from invoice__payment where invoice_id = '".$row['id']."'";
				$result1 = $database->query($query1);
				$row1 = mysql_fetch_array($result1, MYSQL_ASSOC);
				$balance = $row['total_amount'] - $row1['total_paid'];
				
				$charge_account1 			= 	new ChargeAccount();
				$charge_account_details1 	= 	$charge_account1->getChargeAccountDetails($row['charge_acc_id']);
				$data .='
					<tr>
						<td valign-"top">'.$row['id'].'</td>
						<td valign="top">'.date('d-m-Y', strtotime($row['created_on'])).'</td>
						<td valign="top">';
						if($row['invoice_date'] == '0000-00-00')
							{
								$data .=''.date('d-m-Y', strtotime($row['created_on'])).'';
							}
						else
							{
								$data .=''.formatDate($row['invoice_date'],1).'';
							}
						$data .='
						</td>
						<td valign="top">'.formatDate($row['due_on'],1).'</td>
						<td valign="top">';
						if($row['invoice_status'] == "CANCELLED")
							{
								$data .='<font style="text-decoration: line-through; color:red;">'.$row['invoice_status'].'</font>';
							}
						else
							{
								$data .=''.$row['invoice_status'].'</td>';
							}
						$data .='
						<td valign="top">';
						if($row['invoice_sent_status'] == "SAVED ONLY")
							{
								$data .='<font style="font-weight: bold; color:red;">'.$row['invoice_sent_status'].'</font>';
							}
						else
							{
								$data .=''.$row['invoice_sent_status'].'</td>';
							}
						$data .='
						<td style="text-align:right;" valign="bottom">'.$charge_account_details1['account_name'].'</td>
						<td style="text-align:right;" valign="bottom">'.$row['po_number'].'</td>
						<td style="text-align:right;" valign="bottom">$'.number_format($row['base_fare'],2).'</td>
						<td style="text-align:right;" valign="bottom">$'.number_format($row['extras'],2).'</td>
						<td style="text-align:right;" valign="bottom">$'.number_format($row['card_surcharge'],2).'</td>
						<td style="text-align:right;" valign="bottom">$'.number_format($row['gst'],2).'</td>
						<td style="text-align:right;" valign="bottom">$'.number_format($row['total_amount'],2).'</td>
						
						<td style="text-align:right;" valign="bottom">$'.number_format($row1['total_paid'],2).'</td>
						<td style="text-align:right;" valign="bottom">$'.number_format($balance,2).'</td>
						<td valign="top">'.$row['embed_message'].'</td>
						<td valign="top">
							<a href="#" id="'.$row['id'].'" class="add_payment">Add Payment</a> | 
							<a href="#" id="'.$row['id'].'" class="send_invoice">Send</a> | 
							<a href="#" id="'.$row['id'].'" class="resend_invoice">Re-Send</a> | 
							<a href="#" id="'.$row['id'].'" class="cancel_invoice">Cancel</a> | 
							<a href="#" id="'.$row['id'].'" class="show_pdf">Pdf</a> | 
							<a href="#" id="'.$row['id'].'" class="show_html">Html</a> | 
							<a href="#" id="'.$row['id'].'" class="show_log">Log</a> | 
						</td>
					</tr>
					';
					$invoices_fare 				= $invoices_fare + $row['base_fare'];
					$invoices_extras 			= $invoices_extras + $row['extras'];
					$invoices_card_surcharge 	= $invoices_card_surcharge + $row['card_surcharge'];
					$invoices_total 			= $invoices_total + $row['total_amount'];
					$invoices_total_gst 		= $invoices_total_gst + $row['gst'];
					$total_paid 				= $total_paid + $row1['total_paid'];
					$total_balance 				= $total_balance + $balance;
			}
				$data .='
					<tr>
						<th colspan="8">TOTALS</th>
						<th style="text-align:right;">$'.number_format($invoices_fare,2).'</th>
						<th style="text-align:right;">$'.number_format($invoices_extras,2).'</th>
						<th style="text-align:right;">$'.number_format($invoices_card_surcharge,2).'</th>
						<th style="text-align:right;">$'.number_format($invoices_total_gst,2).'</th>
						<th style="text-align:right;">$'.number_format($invoices_total,2).'</th>
						
						<th style="text-align:right;">$'.number_format($total_paid,2).'</th>
						<th style="text-align:right;">$'.number_format($total_balance,2).'</th>
						<th></th>
						<th></th>
					</tr>';
		$data .= '</table>';
		$a = array('data' => ''.$data.'');
		$json = json_encode($a); 
		echo $json;
	}
if(isset($_GET['show_pdf']) && $_GET['show_pdf'] == '1')
	{
		$data = '<div><iframe src="http://dev.bookings-global.com.au/invoices/'.$_GET['invoice_id'].'.pdf" width="800" height="800"></iframe></div>';
		$a = array('data' => ''.$data.'');
		$json = json_encode($a); 
		echo $json;
	}
if(isset($_GET['show_html']) && $_GET['show_html'] == '1')
	{
		$data = '<div><iframe src="http://dev.bookings-global.com.au/invoices/'.$_GET['invoice_id'].'.html" width="800" height="800"></iframe></div>';
		$a = array('data' => ''.$data.'');
		$json = json_encode($a); 
		echo $json;
	}
if(isset($_GET['add_payment']) && $_GET['add_payment'] == '1')
	{
		$invoice_details = $invoice->getInvoiceDetails($_GET['invoice_id']);
		$charge_account_details = $charge_account->getChargeAccountDetails($invoice_details['charge_acc_id']);
		if($invoice_details['status_id'] == '1' || $invoice_details['status_id'] == '3') // Payable or partially paid
			{
				
				$data .= '
				<table>
					<tr>
						<td>Payment Status:</td>
						<td>
							<select name="invoice_new_status" id="invoice_new_status">
								<option value="2"'; if($invoice_details['status_id'] == '2') $data .='selected'; $data .='>Fully Paid</option>
								<option value="3"'; if($invoice_details['status_id'] == '3') $data .='selected'; $data .='>Partially paid</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Paid/Cancelled on:</td>
						<td>
							<input type="text" name="payment_date" id="payment_date">
							<input type="hidden" name="hidden_payment_date" id="hidden_payment_date">
						</td>
					</tr>
					<tr>
						<td>Amount Paid:<br/>N/A if Cancelled</td>
						<td>
							<input type="text" name="amount_paid" id="amount_paid">
						</td>
					</tr>
					<tr>
						<td>Payment Method:<br/>N/A if Cancelled</td>
						<td>
							<select name="payment_method" id="payment_method">
								<option value="1">EFT</option>
								<option value="2">CC Charge</option>
								<option value="3">Cash</option>
								<option value="3">Others</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Notes:</td>
						<td>
							<textarea name="payment_notes" id="payment_notes" cols="20" rows="3"></textarea>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="hidden" name="invoice_id" id="invoice_id" value="'.$_GET['invoice_id'].'">
							<input type="button" id="add_payment" name="add_payment" class="approve_button" value="ADD PAYMENT">
						</td>
					</tr>
				</table>';
			}
		if($invoice_details['status_id'] == '2') // Invoice paid
			{
				$data .= 'This invoice has already been paid. No further payment action is allowed.';	
			}
		if($invoice_details['status_id'] == '4') // Invoice cancelled
			{
				$data .= 'Sorry, this invoice has been cancelled';	
			}
		
		$a = array('data' => ''.$data.'');
		$json = json_encode($a); 
		echo $json;
	}
if(isset($_GET['add_payment_exec']) && $_GET['add_payment_exec'] == '1')
	{
		$invoice_details = $invoice->getInvoiceDetails($_GET['invoice_id']);
		$charge_account_details = $charge_account->getChargeAccountDetails($invoice_details['charge_acc_id']);
		
		if($_GET['invoice_new_status'] == '2') //Fully Paid, change the status of the individual jobs too
			{
				$query = "INSERT INTO invoice__payment
						(id, invoice_id, created_on, paid_on, amount_paid, payment_method, notes) VALUES
						(NULL, '".$_GET['invoice_id']."', CURRENT_TIMESTAMP, '".$_GET['payment_date']."', '".$_GET['amount_paid']."', '".$_GET['payment_method']."', '".$_GET['payment_notes']."')";
				$result = $database->query($query);
				
				$invoice->addInvoiceLog($_GET['invoice_id'], ''.$_SESSION['TITLE'].' '.$_SESSION['FNAME'].' '.$_SESSION['LNAME'].'', 'Payment Added - $'.$_GET['amount_paid'].'', '', '');
				
				$query1 = "UPDATE invoice SET status_id =  '".$_GET['invoice_new_status']."' WHERE id = '".$_GET['invoice_id']."'";
				$result1 = $database->query($query1);
				
				//Make the jobs cur_pay_status_id to PAID
				$query2 = "SELECT * from invoice__items where invoice_id = '".$_GET['invoice_id']."'";
				$result2 = $database->query($query2);
				while($row2 = mysql_fetch_assoc($result2))
					{
						$query3 = "UPDATE job SET cur_pay_status_id = '3' WHERE id =  '".$row2['job_id']."'";
						$result3 = $database->query($query3);
						
						$invoice->addInvoiceLog($_GET['invoice_id'], ''.$_SESSION['TITLE'].' '.$_SESSION['FNAME'].' '.$_SESSION['LNAME'].'', 'Payment status updated', ''.$invoice_details['status_id'].'', '3');
					}
				$data .= 'Invoice Updated';
			}
		if($_GET['invoice_new_status'] == '3') //Partially Paid, do not change the status of the individual job
			{
				$query = "INSERT INTO invoice__payment
						(id, invoice_id, created_on, paid_on, amount_paid, payment_method, notes) VALUES
						(NULL, '".$_GET['invoice_id']."', CURRENT_TIMESTAMP, '".$_GET['payment_date']."', '".$_GET['amount_paid']."', '".$_GET['payment_method']."', '".$_GET['payment_notes']."')";
				$result2 = $database->query($query);
				
				$invoice->addInvoiceLog($_GET['invoice_id'], ''.$_SESSION['TITLE'].' '.$_SESSION['FNAME'].' '.$_SESSION['LNAME'].'', 'Partial Payment Added - $'.$_GET['amount_paid'].'', '', '');
				
				$query1 = "UPDATE invoice SET status_id =  '".$_GET['invoice_new_status']."' WHERE id = '".$_GET['invoice_id']."'";
				$result1 = $database->query($query1);
				
				$invoice->addInvoiceLog($_GET['invoice_id'], ''.$_SESSION['TITLE'].' '.$_SESSION['FNAME'].' '.$_SESSION['LNAME'].'', 'Payment status updated', ''.$invoice_details['status_id'].'', ''.$_GET['invoice_new_status'].'');
				
				$data .= 'Patial Payment Entered';
			}
		
		$a = array('data' => ''.$data.'');
		$json = json_encode($a); 
		echo $json;
	}
	
if(isset($_GET['send_invoice']) && $_GET['send_invoice'] == '1')
	{
		$invoice_details = $invoice->getInvoiceDetails($_GET['invoice_id']);
		$charge_account_details = $charge_account->getChargeAccountDetails($invoice_details['charge_acc_id']);
		$data .= '
		<table width="100%" class="invoiceTable">
			<tr>	
				<th colspan="4">SELECT RECEIVERS</th>
			</tr>
			<tr>
				<th><input type="checkbox" name="check_all_receivers" id="check_all_receivers"></th>
				<th>Contact Type</th>
				<th>Name</th>
				<th>Email</th>
			</tr>
			';
			$query = "SELECT * 
						FROM 
						charge_acc__contacts, user
						WHERE
						charge_acc__contacts.user_id = user.id
						AND
						charge_acc__contacts.charge_acc_id = '".$invoice_details['charge_acc_id']."'
						AND 
							(
								charge_acc__contacts.type_id = '1'
								OR
								charge_acc__contacts.type_id = '3'
								OR
								charge_acc__contacts.type_id = '4'
							)
						ORDER BY
						user.fname ASC";
			$result = $database->query($query);
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
				{
					if($row['type_id'] == '1') { $role = 'ALL'; }
					if($row['type_id'] == '3') { $role = 'BOOKING ONLY'; }
					if($row['type_id'] == '4') { $role = 'BILLING ONLY'; }
					$data.='
							<tr>
								<td valign-"top"><input type="checkbox" name="to_receivers[]" class="to_receivers" value="'.$row['user_id'].'"></td>
								<td style="border: 1px #336699 solid; text-align:right;" valign="bottom">
									'.$role.'
								</td>
								<td style="border: 1px #336699 solid; text-align:right;" valign="bottom">
									'.$row['title'].' '.$row['fname'].' '.$row['lname'].'
								</td>
								<td style="border: 1px #336699 solid; text-align:right;" valign="bottom">
									'.$row['email'].'
								</td>
							</tr>';	
				}
			$data.='
		</table>
		<br/>
		<table width="100%" class="invoiceTable">
			<tr>	
				<th colspan="4">ADDITIONAL RECEIVERS</th>
			</tr>
			<tr>
				<td></td>
				<td>Email</td>
				<td>Name</td>
			</tr>
			<tr>
				<td>1</td>
				<td><input type="text" name="add_receiver_1_email" id="add_receiver_1_email" palceholder="Email"></td>
				<td><input type="text" name="add_receiver_1_name" id="add_receiver_1_name" palceholder="Full Name"></td>
			</tr>
			<tr>
				<td>2</td>
				<td><input type="text" name="add_receiver_2_email" id="add_receiver_2_email" palceholder="Email"></td>
				<td><input type="text" name="add_receiver_2_name" id="add_receiver_2_name" palceholder="Full Name"></td>
			</tr>
			<tr>
				<td colspan="3">
					<input type="hidden" name="invoice_id" id="invoice_id" value="'.$_GET['invoice_id'].'">
					<input type="button" id="send_invoice_button" name="send_invoice_button" class="approve_button" value="SEND INVOICE">
				</td>
			</tr>
		</table>';
		$a = array('data' => ''.$data.'');
		$json = json_encode($a); 
		echo $json;
	}
	
if(isset($_GET['send_invoice_exec']) && $_GET['send_invoice_exec'] == '1')
	{
		$mailer = new mailer();
		$receivers_id 	= explode(',', $_GET['receivers']);
		//change the status id of invoice to sent
		$invoice->updateInvoiceTable($_GET['invoice_id'], 'sent_status_id', '2');
		if (!empty( $_GET['receivers']))
			{
				foreach($receivers_id as $k=>$v)
					{
						$query1 = "SELECT * from user where id = '".$v."'";
						$result1 = $database->query($query1);
						$row1 = mysql_fetch_assoc($result1);
						$full_name = "".$row1['title']." ".$row1['fname']." ".$row1['lname']."";
						
						$query2 = "INSERT INTO invoice__sent
							(id, created_on, invoice_id, sent_to, email) VALUES
							(NULL,CURRENT_TIMESTAMP, '".$_GET['invoice_id']."', '".$full_name."', '".$row1['email']."')";
						
						$result2 = $database->query($query2);
						
						$email_body = 'Dear '.$full_name.',<br/>Please find the Invoice Number '.$_GET['invoice_id'].' enclosed for the work done by us.<br/>Thanks<br/>Allied Cars Admin';
						$mailer->sendInvoice($full_name, $row1['email'], $email_body, $_GET['invoice_id']);
						
						$invoice->addInvoiceLog($_GET['invoice_id'], ''.$_SESSION['TITLE'].' '.$_SESSION['FNAME'].' '.$_SESSION['LNAME'].'', 'Invoice Sent to '.$row1['email'].'', '', '');
						
						$message .= 'Invoice emailed to '.$full_name.' -  '.$row1['email'].'<br/>';
					}
			}
		if($_GET['add_receiver_1_email'] !='')
			{
				$query3 = "INSERT INTO invoice__sent
					(id, created_on, invoice_id, sent_to, email) VALUES
					(NULL,CURRENT_TIMESTAMP, '".$_GET['invoice_id']."', '".$_GET['add_receiver_1_name']."', '".$_GET['add_receiver_1_email']."')";
					
				$result3 = $database->query($query3);
				$email_body = 'Dear '.$_GET['add_receiver_1_name'].',<br/>Please find the Invoice Number '.$_GET['invoice_id'].' enclosed for the work done by us.<br/>Thanks<br/>Allied Cars Admin';
				$mailer->sendInvoice($_GET['add_receiver_1_name'], $_GET['add_receiver_1_email'], $email_body, $_GET['invoice_id']);
				
				$invoice->addInvoiceLog($_GET['invoice_id'], ''.$_SESSION['TITLE'].' '.$_SESSION['FNAME'].' '.$_SESSION['LNAME'].'', 'Invoice Sent to '.$_GET['add_receiver_1_email'].'', '', '');
				
				$message .= 'Invoice emailed to '.$_GET['add_receiver_1_name'].' -  '.$_GET['add_receiver_1_email'].'<br/>';
			}
			
		if($_GET['add_receiver_2_email'] !='')
			{
				$query4 = "INSERT INTO invoice__sent
					(id, created_on, invoice_id, sent_to, email) VALUES
					(NULL,CURRENT_TIMESTAMP, '".$_GET['invoice_id']."', '".$_GET['add_receiver_2_name']."', '".$_GET['add_receiver_2_email']."')";
					
				$result4 = $database->query($query4);
				$email_body = 'Dear '.$_GET['add_receiver_2_name'].',<br/>Please find the Invoice Number '.$_GET['invoice_id'].' enclosed for the work done by us.<br/>Thanks<br/>Allied Cars Admin';
				$mailer->sendInvoice($_GET['add_receiver_2_name'], $_GET['add_receiver_2_email'], $email_body, $_GET['invoice_id']);
				
				$invoice->addInvoiceLog($_GET['invoice_id'], ''.$_SESSION['TITLE'].' '.$_SESSION['FNAME'].' '.$_SESSION['LNAME'].'', 'Invoice Sent to '.$_GET['add_receiver_2_email'].'', '', '');
				
				$message .= 'Invoice emailed to '.$_GET['add_receiver_2_name'].' -  '.$_GET['add_receiver_2_email'].'<br/>';
			}
		$message .= "<b>INVOICE SENT FOR INVOICE ID - <b>".$_GET['invoice_id']."</b>";
		$a = array('data' => ''.$message.'');
		$json = json_encode($a); 
		echo $json;
	}
	
if(isset($_GET['resend_invoice']) && $_GET['resend_invoice'] == '1')
	{
		$invoice_details = $invoice->getInvoiceDetails($_GET['invoice_id']);
		$charge_account_details = $charge_account->getChargeAccountDetails($invoice_details['charge_acc_id']);
		$data .= '<br/>This action will resend Invoice Id '.$_GET['invoice_id'].' to the following email addresses.<br/><br/>';
		$query = "SELECT * from invoice__sent where invoice_id = '".$_GET['invoice_id']."'";
		$result = $database->query($query);
		$data .= '
		<table width="100%" class="invoiceTable">
			<tr>
				<th>Receiver</th>
				<th>Email</th>
			</tr>';
		while($row = mysql_fetch_assoc($result))
			{
				$data .= '
				<tr>
					<td>'.$row['sent_to'].'</td>
					<td>'.$row['email'].'</td>
				</tr>';	
			}
		$data .= '
		</table>
		<input type="hidden" name="invoice_id" id="invoice_id" value="'.$_GET['invoice_id'].'">
		<input type="button" id="resend_invoice_button" name="resend_invoice_button" class="approve_button" value="SEND INVOICE">
		<input type="button" id="cancel_resend_button" name="cancel_resend_button" class="cancel_button" value="CANCEL">
				</td>
			</tr>
		</table>';
		$a = array('data' => ''.$data.'');
		$json = json_encode($a); 
		echo $json;
	}
if(isset($_GET['resend_invoice_exec']) && $_GET['resend_invoice_exec'] == '1')
	{
		$mailer = new mailer();
		$query = "SELECT * from invoice__sent where invoice_id = '".$_GET['invoice_id']."'";
		$result = $database->query($query);
		while($row = mysql_fetch_assoc($result))
			{
				$email_body = 'Dear '.$row['sent_to'].',<br/>Please find the Invoice Number '.$_GET['invoice_id'].' enclosed for the work done by us.<br/>Thanks<br/>Allied Cars Admin';
				$mailer->sendInvoice($row['sent_to'], $row['email'], $email_body, $_GET['invoice_id']);
				
				$invoice->addInvoiceLog($_GET['invoice_id'], ''.$_SESSION['TITLE'].' '.$_SESSION['FNAME'].' '.$_SESSION['LNAME'].'', 'Invoice Re-Sent to '.$row['email'].'', '', '');
			}
		$message .= "<b>Invoices resent.</b>";
		$a = array('data' => ''.$message.'');
		$json = json_encode($a); 
		echo $json;
	}
	
if(isset($_GET['show_invoice_log']) && $_GET['show_invoice_log'] == '1')
	{
		$query = "SELECT * from invoice__log where invoice_id = '".$_GET['invoice_id']."'";
		$result = $database->query($query);
		$data .='
			<table width="100%" class="invoiceTable">
				<tr>	
					<th colspan="6">LOG FOR INVOCIE ID '.$_GET['invoice_id'].'</th>
				</tr>
				<tr>
					<td>Log Id</td>
					<td>Created On</td>
					<td>Created By</td>
					<td>Notes</td>
					<td>Old Value</td>
					<td>New Value</td>
				</tr>';
		while($row = mysql_fetch_assoc($result))
			{
				$data .='
				<tr>
					<td>'.$row['id'].'</td>
					<td>'.$row['created_on'].'</td>
					<td>'.$row['created_by'].'</td>
					<td>'.$row['notes'].'</td>
					<td>'.$row['old_value'].'</td>
					<td>'.$row['new_value'].'</td>
				</tr>';
			}
		$a = array('data' => ''.$data.'');
		$json = json_encode($a); 
		echo $json;
	}
	
if(isset($_GET['cancel_invoice']) && $_GET['cancel_invoice'] == '1')
	{
		$invoice_details = $invoice->getInvoiceDetails($_GET['invoice_id']);
		if($invoice_details['status_id'] == '4') // Invoice cancelled
			{
				$data .= 'This invoice has already been cancelled';	
			}
		else
			{
				$data .= '<br/>Are you sure, you want to cancel this invoice?<br/><br/>This will take off all the invoice items for this invoice.<br/>You will not be able to do anything further with this invoice.<br/><br/>
				<input type="hidden" name="invoice_id" id="invoice_id" value="'.$_GET['invoice_id'].'">
				<input type="button" id="cancel_invoice_button" name="cancel_invoice_button" class="approve_button" value="CONFIRM -CANCEL">
				<input type="button" id="do_not_cancel_invoice_button" name="do_not_cancel_invoice_button" class="cancel_button" value="DO NOT CANCEL">';	
			}
		$a = array('data' => ''.$data.'');
		$json = json_encode($a); 
		echo $json;
		
	}
if(isset($_GET['cancel_invoice_exec']) && $_GET['cancel_invoice_exec'] == '1')
	{
		$invoice_details = $invoice->getInvoiceDetails($_GET['invoice_id']);
		$charge_account_details = $charge_account->getChargeAccountDetails($invoice_details['charge_acc_id']);
		//change status of the invoice to cancelled
		$query1 = "UPDATE invoice SET status_id =  '4' WHERE id = '".$_GET['invoice_id']."'";
		$result1 = $database->query($query1);
		
		$invoice->addInvoiceLog($_GET['invoice_id'], ''.$_SESSION['TITLE'].' '.$_SESSION['FNAME'].' '.$_SESSION['LNAME'].'', 'Invoice Cancelled', '', '');
		
		//Change the status of the jobs back to Unpaid
		$query2 = "SELECT * from invoice__items where invoice_id = '".$_GET['invoice_id']."'";
		$result2 = $database->query($query2);
		while($row2 = mysql_fetch_assoc($result2))
			{
				$query3 = "UPDATE job SET cur_pay_status_id = '1' WHERE id =  '".$row2['job_id']."'";
				$result3 = $database->query($query3);
				
				$invoice->addInvoiceLog($_GET['invoice_id'], ''.$_SESSION['TITLE'].' '.$_SESSION['FNAME'].' '.$_SESSION['LNAME'].'', 'Job ID - '.$row2['job_id'].' Payment Status changed to Unpaid', '', '');
			}
		//delete these jobs from invoice__items table
		$query3 = "DELETE FROM invoice__items WHERE invoice_id = '".$_GET['invoice_id']."'";
		$result3 = $database->query($query3);
		$invoice->addInvoiceLog($_GET['invoice_id'], ''.$_SESSION['TITLE'].' '.$_SESSION['FNAME'].' '.$_SESSION['LNAME'].'', 'Invoice Id '.$_GET['invoice_id'].' - Items deleted', '', '');
		$data .= 'Invoice Cancelled. All jobs under this invoice has been reverted back to status UNPAID';
		$a = array('data' => ''.$data.'');
		$json = json_encode($a); 
		echo $json;
	}