<?php
ini_set("display_errors", 0);
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "fareEstimator.php");
session_start();
$database = new Database;
//For list of all points of interest
if(isset($_GET['all_pois']))
	{
		$query = "SELECT * from pois order by order_id ASC";
		$result = $database->query($query);
			while($row = mysql_fetch_assoc($result))	
					{
						$data .='<input type="radio" name="add_sel_add_1" value="'.$row['id'].'">'.$row['line1'].'<br/>';
					}
					//$data .="</div>";
		$a = array('data' => ''.$data.'');
		$json = json_encode($a); 
		echo $json;
	}
//Details of one of the points of interest
if(isset($_GET['a_pois']))
	{
		$query = "SELECT * from pois where id = '".$_GET['a_pois']."'";
		$result = $database->query($query);
		$row = mysql_fetch_assoc($result);
		$a = array('data1' => ''.$row['line1'].'', 'data2' => ''.$row['line2'].'', 'data3' => ''.$row['sub'].'', 'data4' => ''.$row['postcode'].'');
		$json = json_encode($a); 
		echo $json;
	}
//For list of all points of interest
if(isset($_GET['all_pax_add']))
	{
		$query = "SELECT *,
					user__address.id as address_id
					from 
					user__address, variable__booking_states
					where 
					user__address.state = variable__booking_states.id
					AND
					user__address.user_id='".$_GET['all_pax_add']."'";
		$result = $database->query($query);
		while($row = mysql_fetch_assoc($result))	
				{
					$data .='<input type="radio" name="add_sel_add_1" value="'.$row['address_id'].'">'.$row['line1'].', '.$row['line2'].', '.$row['sub'].', '.$row['details'].'<br/>';
				}
				$data .="</div>";
		$a = array('data' => ''.$data.'');
		$json = json_encode($a); 
		echo $json;
	}
//Details of one of the address
if(isset($_GET['an_add']))
	{
		$query = "SELECT * from user__address where id = '".$_GET['an_add']."'";
		$result = $database->query($query);
		$row = mysql_fetch_assoc($result);
		$a = array('data1' => ''.$row['line1'].'', 'data2' => ''.$row['line2'].'', 'data3' => ''.$row['sub'].'', 'data4' => ''.$row['postcode'].'');
		$json = json_encode($a); 
		echo $json;
	}
//Get the type of Account and return all the charge account types
if(isset($_GET['acc_type']))
	{
		$query = "SELECT * from charge_acc where acc_type='".$_GET['acc_type']."' order by account_name ASC";
		$result = $database->query($query);
		$data .= '<select name="charge_acc" id="charge_acc" data-placeholder="Select Charge Account..." class="chosen-select" style="width:350px;">
					<option value="">Please Select</option>';
		while($row = mysql_fetch_assoc($result))	
				{
					$data .='<option value="'.$row['id'].'">'.$row['account_name'].'</option>';
				}
		$data .="</select>";
		$a = array('data' => $data);
		$json = json_encode($a); 
		echo $json;
	}
//Get the type of Account and return all the charge account types
if(isset($_GET['acc_contacts']))
	{
		$query1 = "SELECT * from
					charge_acc__contacts, user
					where charge_acc__contacts.user_id = user.id
					AND charge_acc__contacts.charge_acc_id='".$_GET['acc_contacts']."' 
					AND (charge_acc__contacts.type_id = '1' OR charge_acc__contacts.type_id = '3')
					order by user.fname";
		$result1 = $database->query($query1);
		$data1 .= '<select name="acc_contact" id="acc_contact" data-placeholder="Select Booked By..." class="chosen-select" style="width:350px;">
					<option value="">Please Select</option>';
		while($row1 = mysql_fetch_assoc($result1))	
				{
					$data1 .='<option value="'.$row1['id'].'">'.$row1['fname'].' '.$row1['lname'].'</option>';
				}
		$data1 .="</select>";
		
		$query2 = "SELECT * from
					charge_acc__contacts, user
					where charge_acc__contacts.user_id = user.id
					AND charge_acc__contacts.charge_acc_id = '".$_GET['acc_contacts']."'
					AND (charge_acc__contacts.type_id = '1' OR charge_acc__contacts.type_id = '2')
					order by user.fname";
		$result2 = $database->query($query2);
		$data2 .= '<select name="acc_passenger" id="acc_passenger" data-placeholder="Select Passenger..." class="chosen-select" style="width:350px;">
					<option value="">Please Select</option>';
		while($row2 = mysql_fetch_assoc($result2))	
				{
					$data2 .='<option value="'.$row2['id'].'">'.$row2['fname'].' '.$row2['lname'].'</option>';
				}
		$data2 .="</select>";
		
		$a = array('data1' => $data1, 'data2' => $data2);
		$json = json_encode($a); 
		echo $json;
	}
if(isset($_POST['from']) && isset($_POST['to']))
	{	
		$fareEstimator 	= 	new fareEstimator();
		$google_value 	= 	$fareEstimator->getGoogleDistanceTime($_POST['from'], $_POST['to'], $raw = false);
		$google_ret_value 	= 	$fareEstimator->getGoogleDistanceTime($_POST['ret_from'], $_POST['ret_to'], $raw = false);
		$a = array('data1' => $google_value['distance'], 'data2' => $google_ret_value['distance']);
		$json = json_encode($a); 
		echo $json;
	}
?>