<?php
ini_set("display_errors", 0);
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(INCLUDE_PATH . "functions.php");
require_once(INCLUDE_PATH . "config.php");
session_start();

$database = new Database;
try
	{
/*-------------------SEARCH-----------------------------------------------------------------------------------------------------*/
		if($_POST['search_string'])
			{
				$database = new database;
				$query = "SELECT COUNT(*) AS RecordCount FROM user
							WHERE 
							id like '%".$_POST['search_string']."%'
							OR fname like '%".$_POST['search_string']."%'
							OR lname like '%".$_POST['search_string']."%'
							OR email like '%".$_POST['search_string']."%'
							AND role_id = '2'";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];
								
				$query = "SELECT * FROM user
							WHERE 
							id like '%".$_POST['search_string']."%'
							OR fname like '%".$_POST['search_string']."%'
							OR lname like '%".$_POST['search_string']."%'
							OR email like '%".$_POST['search_string']."%'
							ORDER BY 
							".$_GET["jtSorting"]." LIMIT ".$_GET["jtStartIndex"].", ".$_GET["jtPageSize"]."";
				$result = $database->query($query);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['Records'] = $rows;
				$jTableResult['query'] = $query;
				print json_encode($jTableResult);
			}
/*-------------------LISTING-----------------------------------------------------------------------------------------------------*/
		else if($_GET["action"] == "list_drivers")
			{
				//Get record count
				$query = "SELECT COUNT(*) AS RecordCount FROM user where role_id='2'";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];
				$query = "SELECT
							user.id as id,
							DATE_FORMAT(user.created_on,'%d/%m/%Y') AS created_on,
							user.role_id as role_id,
							user.title as title,
							user.fname as fname,
							user.lname as lname,
							user.password as password,
							user.email as email,
							user.mobile as mobile,
							user.phone as phone,
							user.hidden as hidden, 
							drivers.id as driver_id,
							drivers.user_id as user_id,
							drivers.bus_name as bus_name,
							drivers.dc as dc,
							drivers.dc_exp as dc_exp,
							drivers.lic as lic,
							drivers.lic_exp as lic_exp,
							drivers.abn as abn,
							drivers.notes as notes,
							drivers.acc_bal as acc_bal
							FROM user
							LEFT JOIN drivers ON user.id = drivers.user_id
							where user.role_id = '2'
							ORDER BY
							user.".$_GET["jtSorting"]." ";
				$result = $database->query($query);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
					
				if(isset($_SESSION['LIST_DRIVERS']))
				{
					unset($_SESSION['LIST_DRIVERS']);
					$_SESSION['LIST_DRIVERS'] = array();
				}
				$_SESSION['LIST_DRIVERS'] = $query;
				
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['Records'] = $rows;
				print json_encode($jTableResult);
			}
		else if($_GET["action"] == "list_addresses")
			{
				echo $_POST['user_id'];
				//Get record count
				$query = "SELECT COUNT(*) AS RecordCount FROM user__address where user_id = '".$_GET["user_id"]."'";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];

				//Get records from database
				$query = "SELECT *,
							DATE_FORMAT(created_on,'%d/%m/%Y') AS created_on
							FROM user__address
							where user_id = '".$_GET["id"]."'";
				$result = $database->query($query);
				//Add all records to an array
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}

				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['Records'] = $rows;
				print json_encode($jTableResult);
			}	
		else if($_GET["action"] == "list_cars")
			{
				//Get record count
				$query = "SELECT COUNT(*) AS RecordCount FROM drivers__cars where driver_id = '".$_GET["user_id"]."'";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];

				//Get records from database
				$query = "SELECT *
							FROM drivers__cars
							where driver_id = '".$_GET["id"]."'";
				$result = $database->query($query);
				//Add all records to an array
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}

				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['Records'] = $rows;
				print json_encode($jTableResult);
			}
		
		else if($_GET["action"] == "list_accounts")
			{
				//Get records from database
			 	$query = "SELECT * FROM drivers__accounts_".$_GET["id"]." order by id DESC" ;
				$result = $database->query($query);
				$recordCount = mysql_num_rows($result);
				//Add all records to an array
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}

				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['Records'] = $rows;
				print json_encode($jTableResult);
			}
			
		else if($_GET["action"] == "list_bank_accounts")
			{
				
				$query = "SELECT * FROM drivers__bank_accounts where user_id = '".$_GET["id"]."'";
				$result = $database->query($query);
				
				//Get record count
				$recordCount = mysql_num_rows($result);

				//Get records from database and Add all records to an array
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}

				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['Records'] = $rows;
				print json_encode($jTableResult);
				
			}
/*-------------------CREATE-----------------------------------------------------------------------------------------------------*/

		else if($_GET["action"] == "create_driver")
			{
				foreach(array_keys($_POST) as $key)
					{
						$clean[$key] = mysql_real_escape_string($_POST[$key]);
					}
				$query = "INSERT INTO user
							(
								id,
								created_on,
								role_id,
								title,
								fname,
								lname,
								password,
								email,
								mobile,
								phone
							) 
						VALUES
							(
								NULL,
								CURRENT_TIMESTAMP,
								'2',
								'".$clean['title']."',
								'".$clean['fname']."',
								'".$clean['lname']."',
								'".$clean['password']."',
								'".$clean['email']."',
								'".$clean['mobile']."',
								'".$clean['phone']."'
							)";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$id = mysql_insert_id();
				
				$query ="INSERT INTO drivers
							(id, user_id, bus_name, dc, dc_exp,	lic, lic_exp, abn, notes) 
						VALUES
							(
								NULL,
								'".$id."',
								'".$clean['bus_name']."',
								'".$clean['dc']."',
								'".$clean['dc_exp']."',
								'".$clean['lic']."',
								'".$clean['lic_exp']."',
								'".$clean['abn']."',
								'".$clean['abn']."'
							)";
				$result = $database->query($query);
				
				$query ="INSERT INTO drivers__accounts
							(
								id,
								user_id,
								acc_table_name,
								acc_name,
								created_on
							) 
						VALUES
							(
								NULL,
								'".$id."',
								'drivers__accounts_".$id."',
								'".$clean['title']." ".$clean['fname']." ".$clean['lname']."',
								'CURRENT_TIMESTAMP'
							)";
				$result = $database->query($query);
				$query ="	
						CREATE TABLE IF NOT EXISTS drivers__accounts_".$id." (
						id int(11) NOT NULL AUTO_INCREMENT,
						trx_id int(11) NOT NULL,
						trx_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
						entered_by varchar(25) NOT NULL DEFAULT 'SYSTEM',
						description varchar(200) NOT NULL,
						acc_dr varchar(50) NOT NULL,
						acc_cr varchar(50) NOT NULL,
						amt_dr decimal(10,2) NOT NULL,
						amt_cr decimal(10,2) NOT NULL,
						PRIMARY KEY (id)
						) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
				$result = $database->query($query);
				 
				/*----CREATE TRIGGER ON THE MEMBER ACCOUNT TABLE-----------------------------------------------------------------------------------------------------*/
				$db=new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
				$q  = "CREATE TRIGGER update_real_balance_".$id." AFTER INSERT ON drivers__accounts_".$id."
							FOR EACH ROW BEGIN
							declare total_debits decimal(10,2);
							declare total_credits decimal(10,2);
							SELECT sum(amt_dr) INTO total_debits FROM drivers__accounts_".$id.";
							SELECT sum(amt_cr) INTO total_credits FROM drivers__accounts_".$id.";
							SET @acc_balance = total_credits - total_debits;
							update drivers SET acc_bal = @acc_balance where user_id = ".$id.";
							END;
						";
				$db->multi_query($q); 
				
				//fetching the last inserted record
				$query = "SELECT
							user.id as id,
							DATE_FORMAT(user.created_on,'%d/%m/%Y') AS created_on,
							user.role_id as role_id,
							user.title as title,
							user.fname as fname,
							user.lname as lname,
							user.password as password,
							user.email as email,
							user.mobile as mobile,
							user.phone as phone,
							drivers.id as driver_id,
							drivers.user_id as user_id,
							drivers.bus_name as bus_name,
							drivers.dc as dc,
							drivers.dc_exp as dc_exp,
							drivers.lic as lic,
							drivers.lic_exp as lic_exp,
							drivers.abn as abn
							FROM user
							LEFT JOIN drivers ON user.id = drivers.user_id
							where user.id = '".$id."'";
							
				$result = $database->query($query);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['Record'] = $rows;
				print json_encode($jTableResult);
			}
		
		
		else if($_GET["action"] == "create_addresses")
			{
				foreach(array_keys($_POST) as $key)
					{
						$clean[$key] = mysql_real_escape_string($_POST[$key]);
					}
				$result = mysql_query("INSERT INTO user__address
										(
											id,
											created_on,
											user_id,
											type_id,
											line1,
											line2,
											sub,
											postcode,
											state													
										) 
									VALUES
										(
											NULL,
											CURRENT_TIMESTAMP,
											'".$clean['user_id']."',
											'".$clean['type_id']."',
											'".$clean['line1']."',
											'".$clean['line2']."',
											'".$clean['sub']."',
											'".$clean['postcode']."',
											'".$clean['state']."'
										)");

				$result = mysql_query("SELECT * FROM user__address WHERE id = LAST_INSERT_ID();");
				$row = mysql_fetch_array($result);
				
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['Record'] = $row;
				print json_encode($jTableResult);
			}

		else if($_GET["action"] == "create_cars")
			{
				foreach(array_keys($_POST) as $key)
					{
						$clean[$key] = mysql_real_escape_string($_POST[$key]);
					}
				$result = mysql_query("INSERT INTO drivers__cars
										(
											id,
											driver_id,
											plate_no,
											car_type,
											licence_type,
											shape_type,
											fuel_type,
											color_type,
											make_type,
											model_type
										) 
									VALUES
										(
											NULL,
											'".$clean['user_id']."',
											'".$clean['plate_no']."',
											'".$clean['car_type']."',
											'".$clean['licence_type']."',
											'".$clean['shape_type']."',
											'".$clean['fuel_type']."',
											'".$clean['color_type']."',
											'".$clean['make_type']."',
											'".$clean['model_type']."'
										)");

				$result = mysql_query("SELECT * FROM drivers__cars WHERE id = LAST_INSERT_ID();");
				$row = mysql_fetch_array($result);
				
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['Record'] = $row;
				print json_encode($jTableResult);
			}
			else if($_GET["action"] == "create_bank_accounts")
			{
				foreach(array_keys($_POST) as $key)
					{
						$clean[$key] = mysql_real_escape_string($_POST[$key]);
					}
				$result = mysql_query("INSERT INTO drivers__bank_accounts
										(
											id,
											account_name,
											account_number,
											bank_name,
											bsb_number,
											user_id											
										) 
									VALUES
										(
											NULL,
											'".$clean['account_name']."',
											'".$clean['account_number']."',
											'".$clean['bank_name']."',
											'".$clean['bsb_number']."',
											'".$clean['user_id']."'
											
										)");

				$result = mysql_query("SELECT * FROM drivers__bank_accounts WHERE id = LAST_INSERT_ID();");
				$row = mysql_fetch_array($result);
				
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['Record'] = $row;
				print json_encode($jTableResult);
			}
			
/*-------------------UPDATE-----------------------------------------------------------------------------------------------------*/
		
		else if($_GET["action"] == "update_driver")
			{
				foreach(array_keys($_POST) as $key)
					{
						$clean[$key] = mysql_real_escape_string($_POST[$key]);
					}
				$query = "UPDATE user SET
							title		= '".$clean["title"]."',
							fname		= '".$clean["fname"]."',
							lname		= '".$clean["lname"]."',
							password	= '".$clean["password"]."',
							email		= '".$clean["email"]."',
							mobile		= '".$clean["mobile"]."',
							phone		= '".$clean["phone"]."',
							hidden		= '".$clean["hidden"]."'
							WHERE id 	= ".$clean['id']."" ;
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$id = mysql_insert_id();
				
				$query ="UPDATE drivers SET
							bus_name	= '".$clean['bus_name']."',
							dc			= '".$clean['dc']."',
							dc_exp		= '".$clean['dc_exp']."',
							lic			= '".$clean['lic']."',
							lic_exp		= '".$clean['lic_exp']."',
							abn			= '".$clean['abn']."',
							notes		= '".$clean['notes']."'
							WHERE user_id 	= ".$clean['id']."" ;			
						
				$result = $database->query($query);
				
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}
			
		else if($_GET["action"] == "update_addresses")
			{
				foreach(array_keys($_POST) as $key)
					{
						$clean[$key] = mysql_real_escape_string($_POST[$key]);
					}
				$database = new database;
				//Update record into database
				$query = "UPDATE user__address SET 
							type_id		= '".$clean["type_id"]."',
							line1		= '".$clean["line1"]."',
							line2		= '".$clean["line2"]."',
							sub			= '".$clean["sub"]."',
							postcode	= '".$clean["postcode"]."',
							state		= '".$clean["state"]."'
							WHERE id 	= ".$clean['id']."";
				$result = $database->query($query);
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}
		
		else if($_GET["action"] == "update_cars")
			{
				$database = new database;
				//Update record into database
				foreach(array_keys($_POST) as $key)
					{
						$clean[$key] = mysql_real_escape_string($_POST[$key]);
					}
				$query = "UPDATE drivers__cars SET 
							plate_no		= '".$clean["plate_no"]."',
							car_type		= '".$clean["car_type"]."',
							licence_type	= '".$clean["licence_type"]."',
							shape_type		= '".$clean["shape_type"]."',
							fuel_type		= '".$clean["fuel_type"]."',
							color_type		= '".$clean["color_type"]."',
							make_type		= '".$clean["make_type"]."',
							model_type		= '".$clean["model_type"]."'
							where id 		= ".$clean['id']."";
				$result = $database->query($query);
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}
			
		else if($_GET["action"] == "update_bank_accounts")
			{
				foreach(array_keys($_POST) as $key)
					{
						$clean[$key] = mysql_real_escape_string($_POST[$key]);
					}
				$database = new database;
				//Update record into database
				$query = "UPDATE drivers__bank_accounts SET 
							bank_name		= '".$clean["bank_name"]."',
							account_name	= '".$clean["account_name"]."',
							bsb_number		= '".$clean["bsb_number"]."',
							account_number	= '".$clean["account_number"]."'
							where id 		= ".$clean['id']."";
				$result = $database->query($query);
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}
/*-------------------DELETE-----------------------------------------------------------------------------------------------------*/

		else if($_GET["action"] == "delete_driver")
			{
				$database = new database;
				//Delete from database
				$query = "DELETE FROM user WHERE id = ".$_POST['id']."";
				$result = $database->query($query);
				
				$query = "DELETE FROM drivers WHERE id = ".$_POST['driver_id']."";
				$result = $database->query($query);
				
				/*$query = "DELETE FROM drivers__accounts WHERE user_id = ".$_POST['id']."";
				$result = $database->query($query);
				
				$query = "DROP Table drivers__accounts_".$_POST['id'];
				$result = $database->query($query);*/
				
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}
		else if($_GET["action"] == "delete_addresses")
			{
				$database = new database;
				//Delete from database
				$query = "DELETE FROM user__address WHERE id = '".$_POST['id']."'";
				$result = $database->query($query);
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}

		else if($_GET["action"] == "delete_cars")
			{
				$database = new database;
				//Delete from database
				$query = "DELETE FROM drivers__cars WHERE id = '".$_POST['id']."'";
				$result = $database->query($query);
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}

			
		else if($_GET["action"] == "delete_bank_accounts")
			{
				$database = new database;
				//Delete from database
				$query = "DELETE FROM drivers__bank_accounts WHERE id = '".$_POST['id']."'";
				$result = $database->query($query);
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}
/*-------------------EXPORT-----------------------------------------------------------------------------------------------------*/
		
		else if($_GET["action"] == "export_excel")
			{
				
				require_once '../Classes/PHPExcel.php';
				
				$database = new database;
				$objPHPExcel = new PHPExcel();
				
								// Create a first sheet
				$objPHPExcel->getProperties()->setCreator("".BUSINESS_NAME."")
							 ->setLastModifiedBy("".$_SESSION['TITLE']." ".$_SESSION['FNAME']." ".$_SESSION['LNAME']."")
							 ->setTitle("List of Users of ".BUSINESS_NAME."")
							 ->setSubject("List of Users of ".BUSINESS_NAME."")
							 ->setDescription("List of Users of ".BUSINESS_NAME."")
							 ->setKeywords("List of Users")
							 ->setCategory("List of Users");
							 
				$objPHPExcel->setActiveSheetIndex(0);
				$objPHPExcel->getActiveSheet()->setCellValue('A1', "ID");
				$objPHPExcel->getActiveSheet()->setCellValue('B1', "FNAME");
				$objPHPExcel->getActiveSheet()->setCellValue('C1', "LNAME");
				$objPHPExcel->getActiveSheet()->setCellValue('D1', "PASSWORD");
				$objPHPExcel->getActiveSheet()->setCellValue('E1', "EMAIL");
				$objPHPExcel->getActiveSheet()->setCellValue('F1', "MOBILE");
				$objPHPExcel->getActiveSheet()->setCellValue('G1', "PHONE");
				$objPHPExcel->getActiveSheet()->setCellValue('H1', "NOTES");
				$objPHPExcel->getActiveSheet()->setCellValue('I1', "ACC BAL");
				$objPHPExcel->getActiveSheet()->setCellValue('J1', "VISIBILITY");
				
				
				$styleArray = array( 'font' => array( 'bold' => true, ), 'alignment' => array( 'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT, ), 'borders' => array( 'top' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN, ), ), 'fill' => array( 'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR, 'rotation' => 90, 'startcolor' => array( 'argb' => 'FFA0A0A0', ), 'endcolor' => array( 'argb' => 'FFFFFFFF', ), ), );
				$objPHPExcel->getActiveSheet()->getStyle('A1:AA1')->applyFromArray($styleArray);
				// Hide "ID" column
				//$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setVisible(false);
				
				$query = $_SESSION['LIST_DRIVERS'];
				$result = $database->query($query);
				$total_rows= mysql_num_rows($result);
			

				$i = 2;
				while($row = mysql_fetch_array($result))
					{
						$visibility_val=(($row['hidden'] == 0)?'Current':'Left');
						$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A'.$i, $row['id'])
							->setCellValue('B'.$i, $row['fname'])
							->setCellValue('C'.$i, $row['lname'])
							->setCellValue('D'.$i, $row['password'])
							->setCellValue('E'.$i, $row['email'])
							->setCellValue('F'.$i, $row['mobile'])
							->setCellValue('G'.$i, $row['phone'])
							->setCellValue('H'.$i, $row['notes'])
							->setCellValue('I'.$i, $row['acc_bal'])
							->setCellValue('J'.$i, $visibility_val);
						$i++; 
					}
				$todays_date = Date("d-m-Y-His");
				header('Content-Type: application/vnd.ms-excel');
				$user_report = "Users_List_".$todays_date.".xls";
				header('Content-Disposition: attachment;filename='.$user_report.'');
				header('Cache-Control: max-age=0');
				$objPHPExcel->getActiveSheet()->setTitle('Drivers_List');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$objWriter->save('php://output');
			}
			else if($_GET["action"] == "export_html")
			{
				require_once(CLASSES_PATH . "user.php");
				$user = new User;
				
				$query = $_SESSION['LIST_DRIVERS'];
				$result = $database->query($query);
				$total_rows= mysql_num_rows($result);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				$htmlstr = $user->getHTMLDriverList($rows);
				$todays_date = Date("d-m-Y-His"); 
				$file_name = "../html_report/USER_REPORT_".$todays_date.".html";
				
				$myfile = fopen($file_name, "w") or die("Unable to open file!");
				fwrite($myfile, $htmlstr);
				fclose($myfile);
				 
				header("Cache-Control: public");
				header("Content-Description: File Transfer");
				header("Content-Length: ". filesize("$file_name").";");
				header("Content-Disposition: attachment; filename=$file_name");
				header("Content-Type: application/octet-stream; "); 
				header("Content-Transfer-Encoding: binary");
				readfile($file_name);
			}
			
			else if($_GET["action"] == "export_pdf")
			{
				require_once('../html2pdf/html2pdf.class.php');
				require_once(CLASSES_PATH . "user.php");
				
				$user = new User;
				
				$query = $_SESSION['LIST_DRIVERS'];
				$result = $database->query($query);
				$total_rows= mysql_num_rows($result);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				$htmlstr = $user->getHTMLDriverList($rows);
				$todays_date = Date("d-m-Y-His");
				$file_name = "../html_report/BOOKINGS_REPORT_".$todays_date.".pdf";
				
					
				$html2pdf = new HTML2PDF('L', 'A4', 'en');
				$html2pdf->WriteHTML($htmlstr);
				
				$pdfdoc = $html2pdf->Output($file_name , 'F');
				
				header("Cache-Control: public");
				header("Content-Description: File Transfer");
				header("Content-Length: ". filesize("$file_name").";");
				header("Content-Disposition: attachment; filename=$file_name");
				header("Content-Type: application/pdf "); 
				 
				readfile($file_name);
				
			}
						
	}
catch(Exception $ex)
	{
		//Return error message
		$jTableResult = array();
		$jTableResult['Result'] = "ERROR";
		$jTableResult['Message'] = $ex->getMessage();
		print json_encode($jTableResult);
	}
?>