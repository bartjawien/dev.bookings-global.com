<?php
ini_set("display_errors", 0);
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "chargeAccount.php");
require_once(CLASSES_PATH . "job.php");
require_once(CLASSES_PATH . "jobReference.php");
require_once(CLASSES_PATH . "address.php");
require_once(CLASSES_PATH . "mailer.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "vehicle.php");
require_once(INCLUDE_PATH . "functions_date_time.php");
session_start();
$database = new database;
$charge_acc = new chargeAccount();
$user = new user();
$job = new Job();
$job_ref = new jobReference();
$address = new Address();


//FOR ALLOCATE BULK JOBS
if($_GET['bulk_allocate_job_ids'] != '' && $_GET['bulk_allocate_job_ids'] !== NULL)
	{
		
		$job_ID_string = $_GET['bulk_allocate_job_ids'];
		$job_ID_array = explode(',', $job_ID_string);
		$data .= '<div id="show_message"></div>';
		$data .= '<table style="border:1px solid #000; border-collapse:collapse; text-align:left;">
					<tr>
						<th style="border:1px solid #000; padding:8px;">Job ID</th>
						<th style="border:1px solid #000; padding:8px;">Time</th>
						<th style="border:1px solid #000; padding:8px;">Date</th>
						<th style="border:1px solid #000; padding:8px;">From</th>
						<th style="border:1px solid #000; padding:8px;">To</th>
						<th style="border:1px solid #000; padding:8px;">Job Price</th>
						<th style="border:1px solid #000; padding:8px;">Allocate Price</th>
					</tr>';
		
		foreach($job_ID_array as $job_id)
			{
				$this_job_details = $job->getJobDetails($job_id);
				$data .= '<tr>
							<td valign="center" style="border:1px solid #000; padding:5px;"><input type="text" size="10" name="this_job_id" class="allocate_job_ids" readonly value="'.$job_id.'"></td>
							<td valign="center" style="border:1px solid #000; padding:5px;"><b>'.calculateTimeInAmPmFormatWithoutSeconds($this_job_details['job_time']).'</b></td>
							<td valign="center" style="border:1px solid #000; padding:5px;">'.formatDate($this_job_details['job_date'], 1).'</td>
							<td valign="center" style="border:1px solid #000; padding:5px;"><b>'.$this_job_details['frm_flight_no'].'</b> '.$this_job_details['frm_sub'].'</td>
							<td valign="center" style="border:1px solid #000; padding:5px;">'.$this_job_details['to_sub'].'</td>
							<td valign="center" style="border:1px solid #000; padding:5px;"><b>$</b> '.$this_job_details['tot_fare'].'</td>
							<td valign="center" style="border:1px solid #000; padding:5px;"><b>$</b> <input type="text" size="8" name="allocate_bulk_job_price" class="allocate_bulk_job_price"></td>
						</tr>';
			}
		
		$data .= '<tr>
					<td style="border:1px solid #000; padding:5px;">Select Driver</td>
					<td  style="border:1px solid #000; padding:5px;" colspan="6">
					<select name="bulk_select_driver" id="bulk_select_driver"><option value="">Select Driver</option>';
						$query = "SELECT * FROM user where role_id='2' AND hidden='0' order by fname,lname ASC";
						$database = new database;
						$result = $database->query($query);
						while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
							{
								$data .= "<option value=$row[id]>$row[fname] $row[lname]</option>";
							}
		$data .='</select></td></tr>';
		$data .= '<table><br/>';
		$data .= '<button type="button" id="allocate_these_jobs">Bulk Allocate</button>';
		$a = array('data' => ''.$data.'');
		$json = json_encode($a); 
		echo $json;
	}
if($_GET['allocate_bulk_jobs'] == '1')
	{
		$driver_id			 	= 	$_GET['driver_id'];
		$job_ID_string 			= 	$_GET['job_ids'];
		$job_PRICE_string 		= 	$_GET['prices'];
		$job_ID_array 			= 	explode(',', $job_ID_string);
		$job_PRICE_array 		= 	explode(',', $job_PRICE_string);
		$i=0;
		foreach($job_ID_array as $job_id)
			{
				$job_details = $job->getJobDetails($job_id);
				if($job_details['driver_status'] == '' || $job_details['driver_status'] == '0' ||  $job_details['driver_status'] == '1') //Means job has not been allocated or has been allocated only earlier
					{
						$job->updateJob($job_id, 'driver_status', '1'); // change the driver status to Allocate
						$job->updateJob($job_id, 'driver_id', $driver_id); // Put this driver in job table
						$job->updateJob($job_id, 'driver_price', $job_PRICE_array[$i]); // Put the driver price in the job table

						$job->addNewDriverToJob($job_id, $driver_id, $job_PRICE_array[$i], '', '', '', '', '', '', '', '', '',''); // Add this to job__driver table
						$driver_details = $user->getUserDetails($driver_id); // get details of this driver
						$job->addJobDriverLog($job_id, $_SESSION['USER_ID'], 'Job Allocated', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$job_PRICE_array[$i].'', '');
						$data .= "Job ID -  ".$job_id ." allocated to - <b>".$driver_details['fname']." ".$driver_details['lname']." for $ ".$job_PRICE_array[$i]."</b><br/>";
					}
				else
					{
						$data .= "<span style='color:red; font-weight:bold;'>FAILED :Job ID -  ".$job_id ." NOT ALLOCATED due to its current status. To Allocate, please TEAR OFF this job first.</span><br/>";
					}
				$i++;
			}
			
		$a = array('data' => ''.$data.'');
		$json = json_encode($a); 
		echo $json;
	}

	
	
//FOR OFFER BULK JOBS
if($_GET['bulk_offer_job_ids'] != '' && $_GET['bulk_offer_job_ids'] !== NULL)
	{
		
		$job_ID_string = $_GET['bulk_offer_job_ids'];
		$job_ID_array = explode(',', $job_ID_string);
		$data .= '<div id="show_message"></div>';
		$data .= '<table style="border:1px solid #000; border-collapse:collapse; text-align:left;">
					<tr>
						<th style="border:1px solid #000; padding:8px;">Job ID</th>
						<th style="border:1px solid #000; padding:8px;">Time</th>
						<th style="border:1px solid #000; padding:8px;">Date</th>
						<th style="border:1px solid #000; padding:8px;">From</th>
						<th style="border:1px solid #000; padding:8px;">To</th>
						<th style="border:1px solid #000; padding:8px;">Job Price</th>
						<th style="border:1px solid #000; padding:8px;">Offer Price</th>
						<th style="border:1px solid #000; padding:8px;">Driver Notes</th>
					</tr>';
		
		foreach($job_ID_array as $job_id)
			{
				$this_job_details = $job->getJobDetails($job_id);
				$data .= '<tr>
							<td valign="center" style="border:1px solid #000; padding:5px;"><input type="text" size="10" name="this_job_id" class="offered_job_ids" readonly value="'.$job_id.'"></td>
							<td valign="center" style="border:1px solid #000; padding:5px;"><b>'.calculateTimeInAmPmFormatWithoutSeconds($this_job_details['job_time']).'</b></td>
							<td valign="center" style="border:1px solid #000; padding:5px;">'.formatDate($this_job_details['job_date'], 1).'</td>
							<td valign="center" style="border:1px solid #000; padding:5px;"><b>'.$this_job_details['frm_flight_no'].'</b> '.$this_job_details['frm_sub'].'</td>
							<td valign="center" style="border:1px solid #000; padding:5px;">'.$this_job_details['to_sub'].'</td>
							<td valign="center" style="border:1px solid #000; padding:5px;"><b>$</b>'.$this_job_details['tot_fare'].'</td>
							<td valign="center" style="border:1px solid #000; padding:5px;"><b>$</b><input type="text" size="8" name="offer_bulk_job_price" class="offer_bulk_job_price"></td>
							<td valign="center" style="border:1px solid #000; padding:5px;"><b></b><input type="text" size="25" name="offer_bulk_job_driver_notes" class="offer_bulk_job_driver_notes"></td>
						</tr>';
			}
		
		$data .= '<tr>
					<td style="border:1px solid #000; padding:5px;">Select Driver</td>
					<td  style="border:1px solid #000; padding:5px;" colspan="7">
					<select name="bulk_select_driver" id="bulk_select_driver"><option value="">Select Driver</option>';
						$query = "SELECT * FROM user where role_id='2' AND hidden='0' order by fname,lname ASC";
						$database = new database;
						$result = $database->query($query);
						while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
							{
								$data .= "<option value=$row[id]>$row[fname] $row[lname]</option>";
							}
		$data .='</select></td></tr>';
		$data .= '<table><br/>';
		$data .= '<button type="button" id="offer_these_jobs">Bulk Offer</button>';
		$a = array('data' => ''.$data.'');
		$json = json_encode($a); 
		echo $json;
	}

if($_GET['offer_bulk_jobs'] == '1')
	{
		$driver_id			 	= 	$_GET['driver_id'];
		$job_ID_string 			= 	$_GET['job_ids'];
		$job_PRICE_string 		= 	$_GET['prices'];
		$driver_NOTES_string 	= 	$_GET['driver_notes'];
		$job_ID_array 			= 	explode(',', $job_ID_string);
		$job_PRICE_array 		= 	explode(',', $job_PRICE_string);
		$driver_NOTES_array 	= 	explode(',', $driver_NOTES_string);
		$i=0;
		$JOBS_ARR 			= array();
		$DRIVER_PRICE_ARR 	= array();
		$DRIVER_NOTES_ARR 	= array();
		foreach($job_ID_array as $job_id)
			{
				$mailer = new mailer();
				$job_details 	= $job->getJobDetails($job_id);
				$driver_details = $user->getUserDetails($driver_id); // get details of this driver
				
				if($job_details['driver_status'] == '' || $job_details['driver_status'] == '0' || $job_details['driver_status'] == '1' || $job_details['driver_status'] == '2') //Nothing, Allocated, Offered
					{
						$job->updateJob($job_id, 'driver_status', '2'); // change the driver status to Offered

						$job->addNewDriverToJob($job_id, '', '', $driver_id, $job_PRICE_array[$i], '', '', '', '', '', '', '',''); // Add this to job__driver table
						$job->addJobDriverLog($job_id, $_SESSION['USER_ID'], 'Job Offered', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$job_PRICE_array[$i].'', '');
						$data .= "Job ID -  ".$job_id ." offered to - <b>".$driver_details['fname']." ".$driver_details['lname']." for $ ".$job_PRICE_array[$i]."</b><br/>";
						array_push($JOBS_ARR, array(
														"job_id" 		=> $job_id,
														"job_price" 	=> $job_PRICE_array[$i],
														"driver_notes" 	=> $driver_NOTES_array[$i]
													));

						
					}
				else
					{
						$data .= "<span style='color:red; font-weight:bold;'>FAILED :Job ID -  ".$job_id ." NOT OFFERED due to its current status. To Offer, please TEAR OFF this job first.</span><br/>";
					}
				$i++;
			}
		if( !empty($JOBS_ARR))
			{
				$mailer->sendBulkJobOfferToADriver($driver_id, $JOBS_ARR); // Send job offer to the driver
			}
		$a = array('data' => ''.$data.'');
		$json = json_encode($a); 
		echo $json;
	}	



//FOR ACCEPT BULK JOBS
if($_GET['bulk_accept_job_ids'] != '' && $_GET['bulk_accept_job_ids'] !== NULL)
	{
		
		$job_ID_string = $_GET['bulk_accept_job_ids'];
		$job_ID_array = explode(',', $job_ID_string);
		$data .= '<div id="show_message"></div>';
		$data .= '<table style="border:1px solid #000; border-collapse:collapse; text-align:left;">
					<tr>
						<th style="border:1px solid #000; padding:8px;">Job ID</th>
						<th style="border:1px solid #000; padding:8px;">Time</th>
						<th style="border:1px solid #000; padding:8px;">Date</th>
						<th style="border:1px solid #000; padding:8px;">From</th>
						<th style="border:1px solid #000; padding:8px;">To</th>
						<th style="border:1px solid #000; padding:8px;">Job Price</th>
						<th style="border:1px solid #000; padding:8px;">Accept Price</th>
						<th style="border:1px solid #000; padding:8px;">Driver Notes</th>
					</tr>';
		
		foreach($job_ID_array as $job_id)
			{
				$this_job_details = $job->getJobDetails($job_id);
				$data .= '<tr>
							<td valign="center" style="border:1px solid #000; padding:5px;"><input type="text" size="10" name="this_job_id" class="accept_job_ids" readonly value="'.$job_id.'"></td>
							<td valign="center" style="border:1px solid #000; padding:5px;"><b>'.calculateTimeInAmPmFormatWithoutSeconds($this_job_details['job_time']).'</b></td>
							<td valign="center" style="border:1px solid #000; padding:5px;">'.formatDate($this_job_details['job_date'], 1).'</td>
							<td valign="center" style="border:1px solid #000; padding:5px;"><b>'.$this_job_details['frm_flight_no'].'</b> '.$this_job_details['frm_sub'].'</td>
							<td valign="center" style="border:1px solid #000; padding:5px;">'.$this_job_details['to_sub'].'</td>
							<td valign="center" style="border:1px solid #000; padding:5px;"><b>$</b>'.$this_job_details['tot_fare'].'</td>
							<td valign="center" style="border:1px solid #000; padding:5px;"><b>$</b><input type="text" size="8" name="accept_bulk_job_price" class="accept_bulk_job_price"></td>
							<td valign="center" style="border:1px solid #000; padding:5px;"><b></b><input type="text" size="25" name="accept_bulk_job_driver_notes" class="accept_bulk_job_driver_notes"></td>
						</tr>';
			}
		
		$data .= '<tr>
					<td style="border:1px solid #000; padding:5px;">Select Driver</td>
					<td  style="border:1px solid #000; padding:5px;" colspan="7">
					<select name="bulk_select_driver" id="bulk_select_driver"><option value="">Select Driver</option>';
						$query = "SELECT * FROM user where role_id='2' AND hidden='0' order by fname,lname ASC";
						$database = new database;
						$result = $database->query($query);
						while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
							{
								$data .= "<option value=$row[id]>$row[fname] $row[lname]</option>";
							}
		$data .='</select></td></tr>';
		$data .= '<table><br/>';
		$data .= '<button type="button" id="accept_these_jobs">Bulk Accept</button>';
		$a = array('data' => ''.$data.'');
		$json = json_encode($a); 
		echo $json;
	}

if($_GET['accept_bulk_jobs'] == '1')
	{
		$driver_id			 	= 	$_GET['driver_id'];
		$job_ID_string 			= 	$_GET['job_ids'];
		$job_PRICE_string 		= 	$_GET['prices'];
		$driver_NOTES_string 	= 	$_GET['driver_notes'];
		$job_ID_array 			= 	explode(',', $job_ID_string);
		$job_PRICE_array 		= 	explode(',', $job_PRICE_string);
		$driver_NOTES_array 	= 	explode(',', $driver_NOTES_string);
		
		$JOBS_ARR 			= array();
		$DRIVER_PRICE_ARR 	= array();
		$DRIVER_NOTES_ARR 	= array();
		
		$i=0;
		foreach($job_ID_array as $job_id)
			{
				$job_details 	= $job->getJobDetails($job_id);
				$driver_details = $user->getUserDetails($driver_id); // get details of this driver
				

				if($job_details['driver_status'] == '' || $job_details['driver_status'] == '0' || $job_details['driver_status'] == '1' || $job_details['driver_status'] == '2') //Nothing, Allocated, Offered
					{
						
						$job->updateJob($job_id, 'driver_status', '3'); // change the driver status to Sent
						$job->updateJob($job_id, 'driver_id', $driver_id); // Put this driver in job table
						$job->updateJob($job_id, 'driver_price', $job_PRICE_array[$i]); // Put the driver price in the job table
						if($driver_NOTES_array[$i] !='')
							{
								$job->updateJob($job_id, 'driver_notes', $driver_NOTES_array[$i]);
							}
						
						$job->addNewDriverToJob($job_id, '', '', '', '', $driver_id, $job_PRICE_array[$i], '', '', '', '', '',''); // Add this to job__driver table
						$driver_details = $user->getUserDetails($driver_id); // get details of this driver
						$job->addJobDriverLog($job_id, $_SESSION['USER_ID'], 'Job Accepted', ''.$driver_details['fname'].' '.$driver_details['lname'].' for $ '.$job_PRICE_array[$i].'', '');
						//$mailer->sendJobDetailsToDriver($job_id, $driver_id, $job_PRICE_array[$i],$driver_NOTES_array[$i]); // Send job acceptance email with job details to the driver
						$data .= "Job ID -  ".$job_id ." Accepted - <b>".$driver_details['fname']." ".$driver_details['lname']." for $ ".$job_PRICE_array[$i]."</b><br/>";
						
						array_push($JOBS_ARR, array(
														"job_id" 		=> $job_id,
														"job_price" 	=> $job_PRICE_array[$i],
														"driver_notes" 	=> $driver_NOTES_array[$i]
													));
					}
				else
					{
						$data .= "<span style='color:red; font-weight:bold;'>FAILED :Job ID -  ".$job_id ." NOT ACCEPTED due to its current status. To Offer, please TEAR OFF this job first.</span><br/>";
					}
				$i++;
			}
		if( !empty($JOBS_ARR))
			{
				$mailer = new mailer();
				$mailer->sendBulkJobAcceptDetailsToADriver($driver_id, $JOBS_ARR); // Send job offer to the driver
			}
		$a = array('data' => ''.$data.'');
		$json = json_encode($a); 
		echo $json;
	}			
	
