<?php
ini_set("display_errors", 0);
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(INCLUDE_PATH . "functions.php");
session_start();

$database = new Database;
try
	{
/*-------------------LISTING-----------------------------------------------------------------------------------------------------*/
		if($_POST['search_string'])
			{
				$database = new database;
				$query = "SELECT COUNT(*) AS RecordCount FROM user
							WHERE 
							(id like '%".$_POST['search_string']."%'
							OR title like '%".$_POST['search_string']."%'
							OR fname like '%".$_POST['search_string']."%'
							OR lname like '%".$_POST['search_string']."%'
							OR password like '%".$_POST['search_string']."%'
							AND role_id = '1')";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];
				
				
				$query = "SELECT * FROM user
							WHERE 
							(id like '%".$_POST['search_string']."%'
							OR title like '%".$_POST['search_string']."%'
							OR fname like '%".$_POST['search_string']."%'
							OR lname like '%".$_POST['search_string']."%'
							OR password like '%".$_POST['search_string']."%'
							AND role_id = '1'
							ORDER BY 
							".$_GET["jtSorting"]." LIMIT ".$_GET["jtStartIndex"].", ".$_GET["jtPageSize"]."";
				$result = $database->query($query);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['Records'] = $rows;
				print json_encode($jTableResult);
			}
/*-------------------LISTING-----------------------------------------------------------------------------------------------------*/

		else if($_GET["action"] == "list_user")
			{
				//Get record count
				$query = "SELECT COUNT(*) AS RecordCount FROM user where role_id='1'";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];

				//Get records from database
				$query = "SELECT *, 
							DATE_FORMAT(created_on,'%d/%m/%Y') AS created_on
							FROM user
							where role_id='1'
							ORDER BY
							".$_GET["jtSorting"]." LIMIT ".$_GET["jtStartIndex"].", ".$_GET["jtPageSize"]."";
				$result = $database->query($query);
				//Add all records to an array
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}

				if(isset($_SESSION['LIST_ADMINS']))
				{
					unset($_SESSION['LIST_ADMINS']);
					$_SESSION['LIST_ADMINS'] = array();
				}
				$_SESSION['LIST_ADMINS'] = $query;
				
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['Records'] = $rows;
				print json_encode($jTableResult);
			}
		
		else if($_GET["action"] == "list_addresses")
			{
				//Get record count
				$query = "SELECT COUNT(*) AS RecordCount FROM user__address where user_id = '".$_GET["id"]."'";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];

				//Get records from database
				$query = "SELECT *,
							DATE_FORMAT(created_on,'%d/%m/%Y') AS created_on
							FROM user__address
							where user_id = '".$_GET["id"]."'";
				$result = $database->query($query);
				//Add all records to an array
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}

				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['Records'] = $rows;
				print json_encode($jTableResult);
			}
/*-------------------CREATE-----------------------------------------------------------------------------------------------------*/

		else if($_GET["action"] == "create_user")
			{
				foreach(array_keys($_POST) as $key)
					{
						$clean[$key] = mysql_real_escape_string($_POST[$key]);
					}
				$result = mysql_query("INSERT INTO user
										(
											id,
											created_on,
											role_id,
											title,
											fname,
											lname,
											password,
											email,
											mobile,
											phone
										) 
									VALUES
										(
											NULL,
											CURRENT_TIMESTAMP,
											'".$clean['role_id']."',
											'".$clean['title']."',
											'".$clean['fname']."',
											'".$clean['lname']."',
											'".$clean['password']."',
											'".$clean['email']."',
											'".$clean['mobile']."',
											'".$clean['phone']."'
										)");

				$result = mysql_query("SELECT * FROM user WHERE id = LAST_INSERT_ID();");
				$row = mysql_fetch_array($result);
				
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['Record'] = $row;
				print json_encode($jTableResult);
			}
		else if($_GET["action"] == "create_addresses")
			{
				foreach(array_keys($_POST) as $key)
					{
						$clean[$key] = mysql_real_escape_string($_POST[$key]);
					}
				$result = mysql_query("INSERT INTO user__address
										(
											id,
											created_on,
											user_id,
											type_id,
											line1,
											line2,
											sub,
											postcode,
											state													
										) 
									VALUES
										(
											NULL,
											CURRENT_TIMESTAMP,
											'".$_GET['id']."',
											'".$clean['type_id']."',
											'".$clean['line1']."',
											'".$clean['line2']."',
											'".$clean['sub']."',
											'".$clean['postcode']."',
											'".$clean['state']."'
										)");

				$result = mysql_query("SELECT * FROM user__address WHERE id = LAST_INSERT_ID();");
				$row = mysql_fetch_array($result);
				
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['Record'] = $row;
				print json_encode($jTableResult);
			}
	
/*-------------------UPDATE-----------------------------------------------------------------------------------------------------*/

		else if($_GET["action"] == "update_user")
			{
				$database = new database;
				//Update record into database
				foreach(array_keys($_POST) as $key)
					{
						$clean[$key] = mysql_real_escape_string($_POST[$key]);
					}
				$query = "UPDATE user SET 
							role_id		= '".$clean["role_id"]."',
							title		= '".$clean["title"]."',
							fname		= '".$clean["fname"]."',
							lname		= '".$clean["lname"]."',
							password	= '".$clean["password"]."',
							email		= '".$clean["email"]."',
							mobile		= '".$clean["mobile"]."',
							phone		= '".$clean["phone"]."',
							hidden		= '".$clean["hidden"]."'
							WHERE id 	= ".$clean["id"]."";
				$result = $database->query($query);
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}
		else if($_GET["action"] == "update_addresses")
			{
				$database = new database;
				//Update record into database
				foreach(array_keys($_POST) as $key)
					{
						$clean[$key] = mysql_real_escape_string($_POST[$key]);
					}
				$query = "UPDATE user__address SET 
							type_id		= '".$clean["type_id"]."',
							line1		= '".$clean["line1"]."',
							line2		= '".$clean["line2"]."',
							sub			= '".$clean["sub"]."',
							postcode	= '".$clean["postcode"]."',
							state		= '".$clean["state"]."'
							WHERE id 	= ".$clean['id']."";
				$result = $database->query($query);
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}
/*-------------------DELETE-----------------------------------------------------------------------------------------------------*/

		else if($_GET["action"] == "delete_user")
			{
				$database = new database;
				//Delete from database
				$query = "DELETE FROM user WHERE id = ".$_POST["id"]."";
				$result = $database->query($query);
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}
		else if($_GET["action"] == "delete_addresses")
			{
				$database = new database;
				//Delete from database
				$query = "DELETE FROM user__address WHERE id = '".$_POST['id']."'";
				$result = $database->query($query);
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}

/*-------------------EXPORT-----------------------------------------------------------------------------------------------------*/
			
		else if($_GET["action"] == "export_excel")
			{
				
				require_once '../Classes/PHPExcel.php';
				
				$database = new database;
				$objPHPExcel = new PHPExcel();
				
								// Create a first sheet
				$objPHPExcel->getProperties()->setCreator("".BUSINESS_NAME."")
							 ->setLastModifiedBy("".$_SESSION['TITLE']." ".$_SESSION['FNAME']." ".$_SESSION['LNAME']."")
							 ->setTitle("List of Users of ".BUSINESS_NAME."")
							 ->setSubject("List of Users of ".BUSINESS_NAME."")
							 ->setDescription("List of Users of ".BUSINESS_NAME."")
							 ->setKeywords("List of Users")
							 ->setCategory("List of Users");
							 
				$objPHPExcel->setActiveSheetIndex(0);
				$objPHPExcel->getActiveSheet()->setCellValue('A1', "ID");
				$objPHPExcel->getActiveSheet()->setCellValue('B1', "FNAME");
				$objPHPExcel->getActiveSheet()->setCellValue('C1', "LNAME");
				$objPHPExcel->getActiveSheet()->setCellValue('D1', "PASSWORD");
				$objPHPExcel->getActiveSheet()->setCellValue('E1', "EMAIL");
				$objPHPExcel->getActiveSheet()->setCellValue('F1', "MOBILE");
				$objPHPExcel->getActiveSheet()->setCellValue('G1', "PHONE");
				$objPHPExcel->getActiveSheet()->setCellValue('H1', "VISIBILITY");
				
				// Hide "ID" column
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setVisible(false);
				
				$query = $_SESSION['LIST_ADMINS'];
				$result = $database->query($query);
				$total_rows= mysql_num_rows($result);

				$i = 2;
				while($row = mysql_fetch_array($result))
					{
						$visibility_val=(($row['hidden'] == 0)?'Current':'Left');
						$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A'.$i, $row['id'])
							->setCellValue('B'.$i, $row['fname'])
							->setCellValue('C'.$i, $row['lname'])
							->setCellValue('D'.$i, $row['password'])
							->setCellValue('E'.$i, $row['email'])
							->setCellValue('F'.$i, $row['mobile'])
							->setCellValue('G'.$i, $row['phone'])
							->setCellValue('H'.$i, $visibility_val);
						$i++;
					}
				$todays_date = Date("d-m-Y-His");
				header('Content-Type: application/vnd.ms-excel');
				$user_report = "Admins_List_".$todays_date.".xls";
				header('Content-Disposition: attachment;filename='.$user_report.'');
				header('Cache-Control: max-age=0');
				$objPHPExcel->getActiveSheet()->setTitle('Users_List');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$objWriter->save('php://output');
			}
			else if($_GET["action"] == "export_html")
			{
				require_once(CLASSES_PATH . "user.php");
				$user = new User;
				
				$query = $_SESSION['LIST_ADMINS'];
				$result = $database->query($query);
				$total_rows= mysql_num_rows($result);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				$htmlstr = $user->getHTMLAdminList($rows);
				$todays_date = Date("d-m-Y-His"); 
				$file_name = "../html_report/ADMIN_REPORT_".$todays_date.".html";
				
				$myfile = fopen($file_name, "w") or die("Unable to open file!");
				fwrite($myfile, $htmlstr);
				fclose($myfile);
				 
				header("Cache-Control: public");
				header("Content-Description: File Transfer");
				header("Content-Length: ". filesize("$file_name").";");
				header("Content-Disposition: attachment; filename=$file_name");
				header("Content-Type: application/octet-stream; "); 
				header("Content-Transfer-Encoding: binary");
				readfile($file_name);
			}
			
			else if($_GET["action"] == "export_pdf")
			{
				require_once('../html2pdf/html2pdf.class.php');
				require_once(CLASSES_PATH . "user.php");
				
				$user = new User;
				
				$query = $_SESSION['LIST_ADMINS'];
				$result = $database->query($query);
				$total_rows= mysql_num_rows($result);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				$htmlstr = $user->getHTMLAdminList($rows);
				$todays_date = Date("d-m-Y-His");
				$file_name = "../html_report/ADMIN_REPORT_".$todays_date.".pdf";
				
					
				$html2pdf = new HTML2PDF('L', 'A4', 'en');
				$html2pdf->WriteHTML($htmlstr);
				
				$pdfdoc = $html2pdf->Output($file_name , 'F');
				
				header("Cache-Control: public");
				header("Content-Description: File Transfer");
				header("Content-Length: ". filesize("$file_name").";");
				header("Content-Disposition: attachment; filename=$file_name");
				header("Content-Type: application/pdf "); 
				 
				readfile($file_name);
				
			}
						
	}
catch(Exception $ex)
	{
		//Return error message
		$jTableResult = array();
		$jTableResult['Result'] = "ERROR";
		$jTableResult['Message'] = $ex->getMessage();
		print json_encode($jTableResult);
	}
?>