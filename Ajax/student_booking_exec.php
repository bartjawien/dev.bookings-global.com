<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "job.php");
require_once(CLASSES_PATH . "jobReference.php");
require_once(CLASSES_PATH . "address.php");
require_once(CLASSES_PATH . "mailer.php");
require_once(CLASSES_PATH . "chargeAccount.php");
session_start(); 

//Initialize Classes
$database 		= 	new database;
$user 			= 	new user();
$job 			= 	new Job();
$job_ref 		= 	new jobReference();
$address 		= 	new Address();
$chargeaccounts = 	new chargeAccount();
 
//First do some validation checks
$student_details_check 		= 	'';
$result_j1_dt_time_check 	= 	'';
$result_dest_add_check		=	'';
$result_flight_no 			=	'';

//Check if student has filled his/her personal details

	if($_POST['std_id'] != '' && $_POST['std_fname'] != '' && $_POST['std_lname'] != '' && $_POST['std_email'] != '' )
		{
			if(preg_match('/^[0-9 ]+$/i', $_POST['std_id']) && strlen($_POST['std_id'])>=7)
				{
					$student_details_check = 1; 
				}
			else
				{
					$error_message .= "Check - <b>Student Id should be numeric and at least 7 characters long. </b><br/>";
				}
		}
	else
		{
			if($_POST['std_id'] == '')
				{
					$error_message .= "Check - <b>No Student ID provided. </b><br/>";
				}
			if($_POST['std_fname'] == '')
				{
					$error_message .= "Check - <b>No Student first name provided. </b><br/>";
				}
			if($_POST['std_lname'] == '')
				{
					$error_message .= "Check - <b>No Student last name provided. </b><br/>";
				}
			if($_POST['std_email'] == '')
				{
					$error_message .= "Check - <b>No Student email provided. </b><br/>";
				}
		}

//j1 date and time provided

	if($_POST['j1_date_cap'] != '' && $_POST['j1_time_hrs'] != '' && $_POST['j1_time_mins'] != '')
		{
			$posted_date	=	strtotime($_POST['j1_date_cap']);
			$min_date		=	date('Y-m-d', strtotime("+3 days"));
			if($posted_date >= $min_date) //booking should be either today or a future date
				{
					$result_j1_dt_time_check = 1;
				}
			else
				{
					$error_message .= "Check - <b>Journey 1 Date or Time is not valid</b><br/>";
				}
		}
	else
		{
			$error_message .= "Check - <b>Journey 1 Date or Time - blank</b><br/>";
		}

//Check if destination address is posted

	if($_POST['j1_line_1'] != '' && $_POST['j1_sub'] != '' && $_POST['j1_pc'] != '')
		{
			$result_dest_add_check = 1;
		}	
	else
		{
			$error_message .= "Check - <b>Journey line 1 or suburb or postcode - blank</b><br/>";
		}

//check if flight number is supplied
	if($_POST['j1_flight_no'] !='')
		{
			$result_flight_no=1;
		}
	else
		{
			$error_message .= "Check - <b>Flight no. - blank</b><br/>";
		}

//If all the above checks are valid
	if($student_details_check == '1' && $result_j1_dt_time_check == '1' && $result_dest_add_check == '1' && $result_flight_no == '1')
		{
			if($_POST['below_7'])
				{
					$baby_capsule_ff = 0;
					$baby_capsule_rf = 0;
					$booster_seat = 0;
					if($_POST['child_age_1']!='')
						{
							switch($_POST['child_age_1'])
								{
									case '1':
										$baby_capsule_ff++;
										break;
									case '2':
										$baby_capsule_rf++;
										break;
									case '3':
										$booster_seat++;
										break;
								}
						}
					if($_POST['child_age_2']!='')
						{
							switch($_POST['child_age_2'])
								{
									case '1':
										$baby_capsule_ff++;
										break;
									case '2':
										$baby_capsule_rf++;
										break;
									case '3':
										$booster_seat++;
										break;
								}
						}
					if($_POST['child_age_3']!='')
						{
							switch($_POST['child_age_3'])
								{
									case '1':
										$baby_capsule_ff++;
										break;
									case '2':
										$baby_capsule_rf++;
										break;
									case '3':
										$booster_seat++;
										break;
								}
						}
					if($_POST['child_age_4']!='')
						{
							switch($_POST['child_age_4'])
								{
									case '1':
										$baby_capsule_ff++;
										break;
									case '2':
										$baby_capsule_rf++;
										break;
									case '3':
										$booster_seat++;
										break;
								}
						}
				}
			else
				{
					$baby_capsule_ff 	= 0;
					$baby_capsule_rf 	= 0;
					$booster_seat 		= 0;
				}
			//print_r($_POST);
			//VALUES FOR JOB REFERNECE TABLE
			
			$j1_time			= 	$_POST['j1_time_hrs'].':'.$_POST['j1_time_mins'].':00';
			$under_18 			= 	$_POST['under_18'];
			$on_behalf 			= 	($_POST['on_behalf'] != '')? $_POST['on_behalf'] : '0' ;
			$charge_acc_id 		= 	$_POST['charge_acc'];
			if($_POST['on_behalf'] != '1')
				{
					$contact_id = 	$_POST['acc_contact'];//UNIVERSITY SELF CONTACT
					$u_id		=	$_POST['acc_contact'];
				}
			else
				{
					$contact_id = 	$_POST['agent_contact'];//UNIVERSITY AGENT CONTACT
					$u_id		=	$_POST['agent_contact'];
				}
			$pax_id 			= 	$_POST['acc_passenger'];
			$job_status_id 		= 	'5';
			$from_state			= 	$_POST['j1_state'];
			$to_state 			= 	$from_state;
			$car_type = 1;
			//Remove empty spaces from flight numbers
			$flight_number = preg_replace('/\s+/', '', $_POST['j1_flight_no']);
			
			if($_POST['j1_airport'] == '1') // International
				{
					$j1_line_1 	= 'T2 Int';
					$j1_line_2 	= '';
					$j1_sub_1 	= 'MELBOURNE AIRPORT';
					$j1_pc_1 	= '3045';
				}
			if($_POST['j1_airport'] == '2') // Domestic
				{
					$j1_line_1 	= 'MEL Dom';
					$j1_line_2 	= '';
					$j1_sub_1 	= 'MELBOURNE AIRPORT';
					$j1_pc_1 	= '3045';
				}
			if($_POST['j1_airport'] == '3') // Avalon
				{
					$j1_line_1 	= 'Avalon Airport';
					$j1_line_2 	= '';
					$j1_sub_1 	= 'LARA';
					$j1_pc_1 	= '3212';
				}
			
			//Add values to job reference table
			$job_reference_id = $job_ref->addNewJobReference(
																$u_id,
																$_POST['job_src'],
																$_POST['order_ref'],
																$_POST['acc_type'],
																$_POST['std_id'],
																$_POST['std_title'],
																mysql_real_escape_string($_POST['std_fname']),
																mysql_real_escape_string($_POST['std_lname']),
																$_POST['std_ph'],
																$_POST['std_email'],
																$charge_acc_id,
																$contact_id,
																$pax_id,
																$_POST['job_type'],
																$_POST['charge_mode'],
																$under_18,
																$child_age_below_7,
																$on_behalf
															); 
			//Add job reference log
			$job_ref->addJobReferenceLog($job_reference_id, $u_id, 'New Job Reference Created', $job_reference_id, 'NIL');
			
			$driver_notes 	.= ''.$_POST['university_name'].' ';
			//If On behalf is there
			if($_POST['on_behalf'] == '1')
				{
					$job_ref->updateJobReferenceTable($job_reference_id, 'on_behalf', '1');
					$job_ref->updateJobReferenceTable($job_reference_id, 'on_behalf_title', $_POST['on_behalf_title']);
					$job_ref->updateJobReferenceTable($job_reference_id, 'on_behalf_fname', $_POST['on_behalf_fname']);
					$job_ref->updateJobReferenceTable($job_reference_id, 'on_behalf_lname', $_POST['on_behalf_lname']);
					$job_ref->updateJobReferenceTable($job_reference_id, 'on_behalf_phone', $_POST['on_behalf_phone']);
					$job_ref->updateJobReferenceTable($job_reference_id, 'on_behalf_email', $_POST['on_behalf_email']);
				}
				
			//If student guardian is there
			if($_POST['under_18'] == '1')
				{
					$query = "SELECT * from variable__university where charge_acc_id = '".$charge_acc_id."'";
					$database = new database;
					$result = $database->query($query);
					$row = mysql_fetch_assoc($result);
					
					$job_ref->updateJobReferenceTable($job_reference_id, 'under_18', '1');
					$job_ref->updateJobReferenceTable($job_reference_id, 'std_guar_title', $_POST['std_guar_title']);
					$job_ref->updateJobReferenceTable($job_reference_id, 'std_guar_fname', $_POST['std_guar_fname']);
					$job_ref->updateJobReferenceTable($job_reference_id, 'std_guar_lname', $_POST['std_guar_lname']);
					$job_ref->updateJobReferenceTable($job_reference_id, 'std_guar_email', $_POST['std_guar_email']);
					$job_ref->updateJobReferenceTable($job_reference_id, 'std_guar_phone', $_POST['std_guar_phone']);
					$job_ref->updateJobReferenceTable($job_reference_id, 'under_18_email', $row['under_18_email']);
					//$ext_notes 		.= ''.$_POST['university_name'].' Under 18<br/>';
					//$int_notes 		.= ''.$_POST['university_name'].' Under 18<br/>'; 
					$driver_notes 	.='"Under 18"';  // was .= '"Under 18"';
				}
				
			//If there is an agent
			if($_POST['has_agent'] == '1')
				{
					$job_ref->updateJobReferenceTable($job_reference_id, 'has_agent', '1');
					$job_ref->updateJobReferenceTable($job_reference_id, 'agents_name', $_POST['agents_name']);
					$job_ref->updateJobReferenceTable($job_reference_id, 'agents_email', $_POST['agents_email']);
				}
			if($_POST['has_destination_contact'] == '1')
				{
					$job_ref->updateJobReferenceTable($job_reference_id, 'has_destination_contact', '1');
					$job_ref->updateJobReferenceTable($job_reference_id, 'destination_contact_name', $_POST['destination_contact_name']);
					$job_ref->updateJobReferenceTable($job_reference_id, 'destination_contact_phone', $_POST['destination_contact_phone']);
					$job_ref->updateJobReferenceTable($job_reference_id, 'destination_contact_email', $_POST['destination_contact_email']);
				}	
				
			//If homestay add this value to homestay
			if($_POST['homestay'] == '1')
				{
					$query = "SELECT * from variable__university where charge_acc_id = '".$charge_acc_id."'";
					$database = new database;
					$result = $database->query($query);
					$row = mysql_fetch_assoc($result);
					$job_ref->updateJobReferenceTable($job_reference_id, 'is_homestay', '1');
					$job_ref->updateJobReferenceTable($job_reference_id, 'homestay_email', $row['homestay_email']);
				}
			//If homestay add this value to homestay
			if($_POST['oversize_lug_nos'] >=1 )
				{
					//$ext_notes 		.= 'Oversized Luggage/s - '.$_POST['oversize_lug_nos'].'<br/>';
					//$int_notes 		.= 'Oversized Luggage/s - '.$_POST['oversize_lug_nos'].'<br/>';
					$driver_notes 	.= 'Oversized Luggage/s - '.$_POST['oversize_lug_nos'].'<br/>';
				}
			

			//add the values to job table
			$j1_id = $job->addNewJob
				(
					$job_reference_id,
					$job_status_id,
					$_POST['j1_date_cap'],
					$j1_time,
					mysql_real_escape_string($flight_number),
					$j1_line_1,
					$j1_line_2,
					$j1_sub_1,
					$j1_pc_1,
					$_POST['j1_state'],
					'',
					mysql_real_escape_string($_POST['j1_line_1']),
					mysql_real_escape_string($_POST['j1_line_2']),
					mysql_real_escape_string($_POST['j1_sub']),
					mysql_real_escape_string($_POST['j1_pc']),
					$to_state,
					'',
					'', 
					'', 
					'', 
					'', 
					'', 
					'', 
					'', 
					'', 
					'',
					$car_type,
					$_POST['pax_nos'],
					$_POST['lug_nos'],
					$baby_capsule_rf,
					$booster_seat,
					$baby_capsule_ff,
					'',											//this is driver_status_field (leave blank)
					'0.0',
					'0.0',
					'0.0',
					'0.0',
					'0.0',
					'0.0',
					'0.0',
					'0.0',
					'0.0',
					'0.0',
					'0.0',
					'0.0',
					'0.0',
					'0.0',
					'0.0',
					'0.0',
					'0.0',
					'',
					'',
					$driver_notes,
					'',
					''
				);
			if($j1_id != '')
				{
					$return_message .= "<b>Your Booking has been successfully submitted</b><br/><br/>";
					$return_message .= "<b>BOOKING ID -  ".$j1_id."</b><br/>";
				}

			//Add in Job Log Table
			$job->addJobLog($j1_id,$u_id, 'New job created', $j1_id, '');
			
			//Add this job in job reference table
			$job_ref->updateJobReferenceTable($job_reference_id, 'j1_id', $j1_id);
			
			//Add in Job Reference Log Table
			$job_ref->addJobReferenceLog($job_reference_id, $u_id, 'Job 1 Added', $j1_id, 'NIL');

			
			//Sending mail to university, student, allied admin, informing about the booking details by student
			$mailer = new Mailer();
			$user = new User();

			
			//NEW PROCESS

			$mailer->sendUniversityMail($job_reference_id, '1', '0');
			
			
			$additional_message .= 'We have received your booking.<br/><br/>
			Allied will send you a confirmation, once approved by your institution. Please allow <strong>three (3) working days</strong> to receive your confirmation.<br/><br/>
			If you have any queries, please email us on <strong>alliedcars@alliedcars.com.au</strong><br/><br/>
			<strong>You have to include your University Student ID and Booking ID in the subject line.</strong>';
			
					
			////send mail done////
			
			$message = ''.$return_message.'<br/>'.$additional_message.'';
			$a = array('data1' => 'OK', 'data2' => ''.$message.'');
			$json = json_encode($a); 
			echo $json;
		}
	else
		{
			$a = array('data1' => 'ERROR', 'data2' => '<span class="error">ERRORS - Please Correct!<br/>'.$error_message.'</span>');
			$json = json_encode($a); 
			echo $json;
		}

?>
