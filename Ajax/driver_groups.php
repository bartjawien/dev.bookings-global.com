<?php
ini_set("display_errors", 0);
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
session_start();
$database = new database;
header("Content-Type: application/json;charset=utf-8");

if($_POST['new_group_name'] != '')
	{
		$query1 = "SELECT * from drivers__group_names where group_name = '".$_POST['new_group_name']."'";
		$result1 = $database->query($query1);	
		if(mysql_num_rows($result1)<=0)
			{
				$query2 = "INSERT INTO drivers__group_names (id, created_on, group_name) VALUES (NULL, CURRENT_TIMESTAMP, '".$_POST['new_group_name']."')";
				$result2 = $database->query($query2);
				//$message = "Group '".$_POST['group_name']."' CREATED";
				$a = array(
							'message' => 'Group <b>"'.$_POST['new_group_name'].'"</b> CREATED'
						);
			}
		else
			{
				$a = array(
							'message' => 'Error: Group name <b>"'.$_POST['new_group_name'].'"</b> already exists. Please select a different name.'
						);
			}
	}
if($_POST['group_id'] != '')
	{
		$myString = $_POST['group_id'];
		$myArray = explode(',', $myString);
		foreach($myArray as $k=>$v)
			{
				//Delete the group name from drivers_group_names
				$query = "DELETE from drivers__group_names where id =  '".$v."'";
				$result = $database->query($query);
				
				//Delete members from drivers__group__drivers table
				$query1 = "DELETE from drivers__group_drivers where group_id =  '".$v."'";
				$result1 = $database->query($query1);
				
			}
		$a = array('message' => 'Selected Group/s Deleted');
	}

//SHOW THE MEMBERS IN A GROUP AND THE MEMBERS WHO ARE NOT IN THE GROUP	
if($_POST['this_group_id'] != '')
	{
		//Get the group name to show
		$query = "SELECT * from drivers__group_names where id='".$_POST['this_group_id']."'";
		$result = $database->query($query);
		$row = mysql_fetch_array($result);
		
		//Get the who are in a group
		$result1 = $database->query($query1);
		$query1 = "SELECT
					drivers__group_drivers.group_id as group_id,
					drivers__group_drivers.user_id as driver_id,
					drivers__group_names.id as group_name_id,
					drivers__group_names.group_name as group_name,
					user.id as user_id,
					user.role_id as role_id,
					user.fname as fname,
					user.lname as lname
					from
					drivers__group_drivers
					LEFT JOIN user ON drivers__group_drivers.user_id = user.id
					LEFT JOIN drivers__group_names ON drivers__group_drivers.group_id = drivers__group_names.id
					where drivers__group_drivers.group_id = '".$_POST['this_group_id']."'
					order by fname ASC";
		$result1 = $database->query($query1);
		
		
		//Get the who are not in this group
		$query2 = "SELECT id, fname, lname from user where role_id='2' order by fname ASC";
		$result2 = $database->query($query2);
		
		
		$table .= 	'<table id="log_table">
						<tr>
							<th colspan="2"><b>Showing Group Details for "'.$row['group_name'].'"</b></th>
						</tr>
						<tr>
							<th><b>Group Members</b></th>
							<th><b>Non Members</b></th>
						</tr>
						<tr>
							<td colspan="2"><span id="show_message"></span></td>
						</tr>
						<tr>
							<td valign="top">';
								while($row1 = mysql_fetch_array($result1))
									{
										$table .= '<span class="in_group" id="'.$row1['driver_id'].'"><a href="#">[ Remove ]</a></span>&nbsp;&nbsp;&nbsp;&nbsp;'.$row1['fname'].' '.$row1['lname'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>';
									}
					$table .='</td>
							<td valign="top">';
								while($row2 = mysql_fetch_array($result2))
									{
										$table .= '<span class="out_group" id="'.$row2['id'].'"><a href="#">[ Add ]</a></span>&nbsp;&nbsp;&nbsp;&nbsp;'.$row2['fname'].' '.$row2['lname'].'<br/>';
									}
				$table .= 	'</td>
						</tr>
					</table>';
		$table .= 	'<input type="hidden" name="clicked_group_id" id="clicked_gr_id" value="'.$_POST['this_group_id'].'">';
		$a 	= 	array
					(
						'table' 	=> 	$table,
						'group_id' 	=> 	$_POST['this_group_id']
					);
	}
	
//DELETE A MEMBER FROM THE GROUP	
if($_POST['delete_from_group_id'] != '' && $_POST['delete_user_id'] != '')
	{

		$query = "DELETE from drivers__group_drivers where group_id =  '".$_POST['delete_from_group_id']."' AND user_id = '".$_POST['delete_user_id']."'";
		$result = $database->query($query);

		$a = array('message' => 'Selected Member Deleted from this Group');
	}
	
//ADD A MEMBER TO A GROUP
if($_POST['add_to_group_id'] != '' && $_POST['add_user_id'] != '')
	{
		//Get the group name to show
		$query = "SELECT * from drivers__group_drivers where group_id = '".$_POST['add_to_group_id']."' AND user_id = '".$_POST['add_user_id']."'";
		$result = $database->query($query);
		$total_rows= mysql_num_rows($result);
		
		if($total_rows <= 0) //No Members Found
			{
				$query1 = "INSERT INTO drivers__group_drivers 
								(
									id, 
									group_id, 
									created_on, 
									user_id
								) 
								VALUES 
								(
									NULL,
									'".$_POST['add_to_group_id']."',
									CURRENT_TIMESTAMP,
									'".$_POST['add_user_id']."'
								)";
				$result1 = $database->query($query1);
				$a = array('message' => 'Selected Member Added to this Group');
			}
		else
			{
				$a = array('message' => 'This member is already in the group');
			}
	}

$json = json_encode($a); 
echo $json;
?>