<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');

require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "job.php");
require_once(CLASSES_PATH . "jobReference.php");
require_once(CLASSES_PATH . "address.php");
require_once(CLASSES_PATH . "mailer.php");
require_once(CLASSES_PATH . "chargeAccount.php");
session_start(); 
$database = new database;
$user = new user();
$job = new Job();
$job_ref = new jobReference();
$address = new Address();
$chargeaccounts = new chargeAccount();
 
//First do some validation checks
$result_pax_check = '';
$result_j1_dt_time_check = '';
$result_age_check='';
$result_dest_add_check='';
$result_flight_no ='';
$result_pax_check ='';
$result_luggage_check ='';


//Check 2, see if contact or first name is provided
if($_POST['std_id'] != '' && $_POST['std_fname'] != '' && $_POST['std_lname'] != '' && $_POST['std_email'] != '' )
{
	if(preg_match('/^[0-9 ]+$/i', $_POST['std_id']) && strlen($_POST['std_id'])>=7)
	{
		$result_contact_check = 1; 
	}
	else
	{
		$error_message .= "Check - <b>Student Id should be numeric and atleast 7 characters long. </b><br/>";
	}
	
}
else{
		if($_POST['std_id'] == '')
			$error_message .= "Check - <b>No Student Id provided. </b><br/>";
		if($_POST['std_fname'] == '')
			$error_message .= "Check - <b>No Student first name provided. </b><br/>";
		if($_POST['std_lname'] == '')
			$error_message .= "Check - <b>No Student last name provided. </b><br/>";
		if($_POST['std_email'] == '')
			$error_message .= "Check - <b>No Student email provided. </b><br/>";
		
	}

//Check 4, j1 date and time provided
if($_POST['j1_date_cap'] != '' && $_POST['j_time_hrs'] != '' && $_POST['j_time_mins'] != '')
	{$result_j1_dt_time_check = 1; }
else{$error_message .= "Check - <b>Journey 1 Date or Time - blank</b><br/>";}

if($_POST['under_18']!='')
{
	$result_age_check = 1;
}
else{
	$error_message .= "Check - <b>Under 18 age check not checked</b><br/>";
}

if($_POST['j_line_1'] != '' && $_POST['j_sub'] != '' && $_POST['j_pc'] != '')
{
	$result_dest_add_check = 1;
}	
else
{
	$error_message .= "Check - <b>Journey line 1 or suburb or postcode - blank</b><br/>";
}

if($_POST['j_flight_no'] !='')
{
	$result_flight_no=1;
}
else{
	$error_message .= "Check - <b>Flight no. - blank</b><br/>";
}

if($_POST['pax_nos'] !='')
{
	$result_pax_check=1;
}
else{
	$error_message .= "Check - <b>Select Passenger (Including Yourself)</b><br/>";
}

if($_POST['lug_nos'] !='')
{
	$result_luggage_check=1;
}
else{
	$error_message .= "Check - <b>Select Luggage </b><br/>";
}

if($result_contact_check == '1' && $result_j1_dt_time_check == '1' && $result_age_check == '1' && $result_dest_add_check == '1' && $result_flight_no == '1' && $result_pax_check == '1' && $result_luggage_check == '1')
	{
		//print_r($_POST); 
		$j1_time= $_POST['j_time_hrs'].':'.$_POST['j_time_mins'].':00';
		$under_18 = $_POST['under_18'];
		$on_behalf = ($_POST['on_behalf'] != '')? $_POST['on_behalf'] : '0' ;
		$charge_acc_id = $_POST['charge_acc'];
		$contact_id = $_POST['acc_contact'];// bkg_made_by default Monash Abroad Arrival 294
		$pax_id = $_POST['acc_passenger'];
		$u_id = $_POST['entered_by']; // Default respective universities
		$job_status_id = '5';
		//$from_state = $address->getStateName($_POST['j1_state']);
		$from_state = $_POST['j_state'];
		$to_state = $from_state;
		
		$j_airport=($_POST['j_airport'] == '1')?'Melbourne International Airport':(($_POST['j_airport'] == '2')?'Melbourne Domestic Airport':(($_POST['j_airport'] == '3')?'Avalon Airport':''));
		$j1_line_1 = ($j_airport!='')? $j_airport:' ';
		
		if($_POST['below_7'])
		{
			$baby_capsule_ff = 0;
			$baby_capsule_rf = 0;
			$booster_seat = 0;
			if($_POST['child_age_1']!='')
			{
				switch($_POST['child_age_1'])
				{
					case '1':
						$baby_capsule_ff++;
						//$child_age_below_7= '0 - 6 mnths';
						break;
					
					case '2':
						$baby_capsule_rf++;
						//$child_age_below_7= '6mnths - 4 years';
						break;
					
					case '3':
						$booster_seat++;
						//$child_age_below_7= '4 - 7 years';
						break;
				}
			}
			if($_POST['child_age_2']!='')
			{
				switch($_POST['child_age_2'])
				{
					case '1':
						$baby_capsule_ff++;
						//$child_age_below_7= '0 - 6 mnths';
						break;
					
					case '2':
						$baby_capsule_rf++;
						//$child_age_below_7= '6mnths - 4 years';
						break;
					
					case '3':
						$booster_seat++;
						//$child_age_below_7= '4 - 7 years';
						break;
				}
			}
			if($_POST['child_age_3']!='')
			{
				switch($_POST['child_age_3'])
				{
					case '1':
						$baby_capsule_ff++;
						//$child_age_below_7= '0 - 6 mnths';
						break;
					
					case '2':
						$baby_capsule_rf++;
						//$child_age_below_7= '6mnths - 4 years';
						break;
					
					case '3':
						$booster_seat++;
						//$child_age_below_7= '4 - 7 years';
						break;
				}
			}
			if($_POST['child_age_4']!='')
			{
				switch($_POST['child_age_4'])
				{
					case '1':
						$baby_capsule_ff++;
						//$child_age_below_7= '0 - 6 mnths';
						break;
					
					case '2':
						$baby_capsule_rf++;
						//$child_age_below_7= '6mnths - 4 years';
						break;
					
					case '3':
						$booster_seat++;
						//$child_age_below_7= '4 - 7 years';
						break;
				}
			}
			
		}
		else
		{
			//$child_age_below_7= ' ';
			$baby_capsule_ff = 0;
			$baby_capsule_rf = 0;
			$booster_seat = 0;
		}
		$j1_to_line_1 = mysql_real_escape_string($_POST['j_line_1']);
		$car_type = 1;
		
		//Add values to job reference table
		$job_reference_id = $job_ref->addNewJobReference($u_id, $_POST['job_src'], $_POST['order_ref'], $_POST['acc_type'], $_POST['std_id'], $_POST['std_title'], mysql_real_escape_string($_POST['std_fname']), mysql_real_escape_string($_POST['std_lname']), $_POST['std_ph'], $_POST['std_email'], $charge_acc_id, $contact_id, $pax_id, $_POST['job_type'], $_POST['charge_mode'], $under_18, $child_age_below_7, $on_behalf); 
		//Add job reference log
		$job_ref->addJobReferenceLog($job_reference_id, $u_id, 'New Job Reference Created', $job_reference_id, 'NIL');
		
		if($_POST['charge_acc'] == '15')
		{
			$int_notes = ($under_18)?' Guardian info: '.$_POST['std_guar_title'].':'.$_POST['std_guar_fname'].':'.$_POST['std_guar_lname'].':'.$_POST['std_guar_phone'].':'.$_POST['std_guar_email']:' '; 
			$int_notes .= ($on_behalf)?': Booked by info: '.$_POST['bkg_by_title'].' '.$_POST['bkg_by_fname'].' '.$_POST['bkg_by_lname'].' '.$_POST['bkg_by_phone'].' '.$_POST['bkg_by_email']:' '; 
			$ext_notes = $_POST['ext_notes'];
			if($_POST['destination_contact'] == '1')
			{
				$ext_notes .= '<br /> Destination Contact Info '.$_POST['destination_contact_name'].' '.$_POST['destination_contact_phone'];
			}
		}
		else
		{
			$int_notes = ($under_18)?$_POST['std_guar_title'].':'.$_POST['std_guar_fname'].':'.$_POST['std_guar_lname'].':'.$_POST['std_guar_phone'].':'.$_POST['std_guar_email']:' '; 
			$ext_notes = $_POST['ext_notes'];
		}
		//add the values to job table
		$j1_id = $job->addNewJob
			(
				$job_reference_id,
				$job_status_id,
				$_POST['j1_date_cap'],
				$j1_time,
				mysql_real_escape_string($_POST['j_flight_no']),
				$j1_line_1,
				'',
				'',
				'',
				'',
				'',
				$j1_to_line_1,
				mysql_real_escape_string($_POST['j_line_2']),
				mysql_real_escape_string($_POST['j_sub']),
				mysql_real_escape_string($_POST['j_pc']),
				$to_state,
				'',
				'', 
				'', 
				'', 
				'', 
				'', 
				'', 
				'', 
				'', 
				'',
				$car_type,
				$_POST['pax_nos'],
				$_POST['lug_nos'],
				$baby_capsule_rf,
				$booster_seat,
				$baby_capsule_ff,
				'',											//this is driver_status_field (leave blank)
				$_POST['j1_kms'],
				$_POST['j1_fare'],
				$_POST['j1_inter'],
				$_POST['j1_ed'],
				$_POST['j1_wait'],
				$_POST['j1_tolls'],
				$_POST['j1_bs'],
				$_POST['j1_park'],
				$_POST['j1_ah'],
				$_POST['j1_me'],
				$_POST['j1_alc'],
				$_POST['j1_fc'],
				$_POST['j1_oth'],
				$_POST['j1_tot_fare'],
				$_POST['j1_drv_fee'],
				$_POST['j1_oth_exp'],
				$_POST['j1_profit'],
				$ext_notes,
				$int_notes
			);
		if($j1_id != '')
			{
				$return_message .= "<b>Your Booking has been successfully submitted</b><br/><br/>";
				$return_message .= "<b>BOOKING ID -  ".$j1_id."</b><br/>";
			}

		//Add in Job Log Table
		$job->addJobLog($j1_id,$u_id, 'New job created', $j1_id, '');
		//Add this job in job reference table
		$job_ref->updateJobReferenceTable($job_reference_id, 'j1_id', $j1_id);
		//Add in Job Reference Log Table
		$job_ref->addJobReferenceLog($job_reference_id, $u_id, 'Job 1 Added', $j1_id, 'NIL');
		
		
		////Sending mail to university, student, allied admin, informing about the booking details by student
		$mailer = new Mailer();
		$user = new User();
		$mailer->sendMailToChargeAccountsOnBooking($job_reference_id);
		$additional_message .= 'We have received your booking.<br/><br/>
		Once approved by the University, <strong>Please allow three (3) working days to receive your confirmation.</strong><br/><br/>
		If you have any queries, <strong>please email us on alliedcars@alliedcars.com.au</strong><br/><br/>
		<strong>You have to include your University Student ID and Booking ID in the subject line.</strong>';
		
				
		////send mail done////
		
		$message = ''.$return_message.'<br/>'.$additional_message.'';
		$a = array('data1' => 'OK', 'data2' => ''.$message.'');
		$json = json_encode($a); 
		echo $json;
	}
else
	{
		$a = array('data1' => 'ERROR', 'data2' => '<span class="error">ERRORS - Please Correct!<br/>'.$error_message.'</span>');
		$json = json_encode($a); 
		echo $json;
	}

?>
