<?php
ini_set("memory_limit","128M");
ini_set("display_errors", 0);
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(INCLUDE_PATH . "functions.php");
session_start();

$database = new Database;

/*--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/
if(isset($_POST['show_data']))
	{
		
		$query1 = "SELECT
				*,
				job.id as id,
				job.cur_pay_status_id as cur_pay_status_id,
				CONCAT(a.fname) as entry_by,
				CONCAT(b.fname,'<br/>',b.phone) as bkg_by,
				CONCAT(c.title, ' ', c.fname, ' ', c.lname) as pax_name,
				CONCAT(d.fname,'<br/>',d.mobile) as driver,
				CONCAT(job__reference.std_title, ' ', job__reference.std_fname, ' ', job__reference.std_lname) as student_name,
				CONCAT(job.frm_line1,' ',job.frm_line2,'<br/><b>',job.frm_sub,'</b>') as from_address,
				CONCAT(job.frm_via_line1,' ',job.frm_via_line2,'<br/><b>',job.frm_via_sub,'</b>') as from_via_address,
				CONCAT(job.to_flight_no,' ',job.to_line1,' ',job.to_line2,'<br/><b>',job.to_sub,'</b>') as to_address,
				CONCAT(job.to_via_line1,' ',job.to_via_line2,'<br/><b>',job.to_via_sub,'</b>') as to_via_address,
				CONCAT('<b>',TIME_FORMAT(job.job_time,'%H:%i'),'</b><br/>',DATE_FORMAT(job.job_date,'%d/%m/%Y')) as date_time,
				variable__car_type.details as vehicle_type,
				job__status_ids.details as current_booking_status,
				job__driver_status_ids.details as driver_status,
				job__reference.std_title as std_title,
				job__reference.std_fname as std_fname,
				job__reference.std_lname as std_lname,
				job__reference.under_18 as under_18,
				job__reference.acc_type as acc_type,
				job.driver_notes as driver_notes, 
				job__driver.accepted_amount as driver_price,
				job__driver_extra_pay.amount as extra_pay,
				job__payment_status.details as job_current_payment_status_id,
				job.fare as base_fare,
				job.tot_fare as total_fare
				from 
				job
				LEFT JOIN job__reference ON job.job_reference_id = job__reference.id
				LEFT JOIN job__status_ids ON job.job_status = job__status_ids.id
				LEFT JOIN variable__car_type ON job.car_id = variable__car_type.id
				LEFT JOIN job__driver_status_ids ON job.driver_status = job__driver_status_ids.id
				LEFT JOIN user AS a ON job__reference.entered_by = a.id
				LEFT JOIN user AS b ON job__reference.bkg_by_id = b.id
				LEFT JOIN user AS c ON job__reference.pax_id = c.id
				LEFT JOIN user AS d ON job.driver_id = d.id
				LEFT JOIN charge_acc ON job__reference.charge_acc_id = charge_acc.id
				LEFT JOIN job__driver ON job.id = job__driver.job_id AND job__driver.id=(SELECT MAX(id) FROM job__driver WHERE job_id = job.id)  
				LEFT JOIN job__driver_extra_pay ON job.id = job__driver_extra_pay.job_id
				LEFT JOIN job__payment_status ON job.cur_pay_status_id = job__payment_status.id ";
			
			$where = " WHERE 1 ";
			
			if($_POST['cancel_job'] != '1')
			{
				$where .= " AND job.job_status <> 100 ";
			}
			
			if($_POST['date_from']!='' && $_POST['date_to']!='')
			{
				$where .= " AND job.job_date BETWEEN '".$_POST['date_from']."' AND '".$_POST['date_to']."'";
			}
			elseif($_POST['all_booking_radio'] == '1')
			{
				$where .= " AND job.job_date between '1970-01-01' AND '2050-01-01'";
			}
			else if($_POST['preset_dates'] !='')
			{
				switch($_POST['preset_dates'])
				{
					case '1':
						$date_from = date("Y-n-j", strtotime("first day of previous month"));
						$date_to = date("Y-n-j", strtotime("last day of previous month"));
						$where .= " AND job.job_date BETWEEN '".$date_from."' AND '".$date_to."'";
						break;
						
					case '2':
						$date_to = date("Y-m-d");
						$date_from = date('Y-m-d', strtotime('-14 days'));
						$where .= " AND job.job_date BETWEEN '".$date_from."' AND '".$date_to."'";
						break;
						
					case '3':
						$date_to = date("Y-m-d");
						$date_from = date('Y-m-d', strtotime('-7 days'));
						$where .= " AND job.job_date BETWEEN '".$date_from."' AND '".$date_to."'";
						break;
						
					case '4':
						$current_dayname = date("l");
					
						if($current_dayname == 'Sunday') 
						{
							$date_from = Date("Y-m-d", strtotime("-7 days"));
							$date_to = date("Y-m-d");
							
						}
						else
						{
							$date_from = date("Y-m-d",strtotime('monday this week'));
							$date_to = date("Y-m-d",strtotime("sunday this week"));
						}
						$where .= " AND job.job_date BETWEEN '".$date_from."' AND '".$date_to."'";
						
						break;
						
					case '5':
						$date_from = date("Y-m-d");
						$date_to = date("Y-m-d");
						$where .= " AND job.job_date BETWEEN '".$date_from."' AND '".$date_to."'";
						
						break;
					
					case '6':
						$a_date = date('Y-m-d');
						$dt_from = date('Y-m').'-01';
						$dt_to = date("Y-m-t", strtotime($a_date));
						$where .= " AND job.job_date BETWEEN '".$dt_from."' AND '".$dt_to."'";
						break;
				}
			}
			
			if($_POST["entered_by"] != '' )
			{
				$where .= " AND job__reference.bkg_by_id = '".$_POST["entered_by"]."'";
			}
			if($_POST["pax_id"] != '' )
			{
				$where .= " AND job__reference.pax_id = '".$_POST["pax_id"]."'";
			}
			if($_POST["car_id"] != '' )
			{
				$where .= " AND job.car_id = '".$_POST["car_id"]."'";
			}
			if($_POST["charge_acc"] != '' )
			{
				$where .= " AND job__reference.charge_acc_id = '".$_POST["charge_acc"]."'";
			}
			
			//////////////////switched////////////////
			if($_POST["swt5"] != '0' )
			{
				if($_POST["swt5"] == 1)
				{
					$swt_where = " (job.job_status = '5' ";
				}
				else if($_POST["swt5"] == -1)
				{
					$swt_where = " (job.job_status <> '5' ";
				}
			}
			
			if($_POST["swt10"] != '0' )
			{
				if($_POST["swt10"] == 1)
				{
					$swt_where = ($swt_where != '')? $swt_where." or job.job_status = '10' " : " ( job.job_status = '10' ";
				}
				else if($_POST["swt10"] == -1)
				{
					$swt_where = ($swt_where != '')? $swt_where." or job.job_status <> '10' " : " ( job.job_status <> '10' ";
				}
			}
			
			if($_POST["swt20"] != '0' )
			{
				if($_POST["swt20"] == 1)
				{
					$swt_where = ($swt_where != '')? $swt_where." or job.job_status = '20' " : " ( job.job_status = '20' ";
				}
				else if($_POST["swt20"] == -1)
				{
					$swt_where = ($swt_where != '')? $swt_where." or job.job_status <> '20' " : " ( job.job_status <> '20' ";
				}
			}
			
			if($_POST["swt90"] != '0' )
			{
				if($_POST["swt90"] == 1)
				{
					$swt_where = ($swt_where != '')? $swt_where." or job.job_status = '90' " : " ( job.job_status = '90' ";
				}
				else if($_POST["swt90"] == -1)
				{
					$swt_where = ($swt_where != '')? $swt_where." or job.job_status <> '90' " : " ( job.job_status <> '90' ";
				}
			}
			
			if($_POST["swt80"] != '0' )
			{
				if($_POST["swt80"] == 1)
				{
					$swt_where = ($swt_where != '')? $swt_where." or job.job_status = '80' " : " ( job.job_status = '80' ";
				}
				else if($_POST["swt80"] == -1)
				{
					$swt_where = ($swt_where != '')? $swt_where." or job.job_status <> '80' " : " ( job.job_status <> '80' ";
				}
			}
			
			if($_POST["swt100"] != '0' )
			{
				if($_POST["swt100"] == 1)
				{
					$swt_where = ($swt_where != '')? $swt_where." or job.job_status = '100' " : " ( job.job_status = '100' ";
				}
				else if($_POST["swt100"] == -1)
				{
					$swt_where = ($swt_where != '')? $swt_where." or job.job_status <> '100' " : " ( job.job_status <> '100' ";
				}
			}
			
			$swt_where = ($swt_where !='')? $swt_where." ) ":'1';
			$where .= " AND ".$swt_where;
			
			$query1 = $query1.$where." order by job.job_date ASC ,job.job_time ASC, job.id ASC";
	
		$result1 = $database->query($query1);
		
		
				
		echo '<h2>STEP 2 - SELECT JOBS</h2><br>
				<table id="invoiceTable" width="100%">
				<tr>
					<td colspan="16"><input type="button" name="CheckAll" value="Select All"
					onClick="checkAll(document.make_invoice_form.jobID)">
					<input type="button" name="UnCheckAll" value="Un-Select All"
					onClick="uncheckAll(document.make_invoice_form.jobID)">
					</td>
				</tr>
				<tr>
					<th width="2%"></th>
					<th width="4%">Job ID</th>
					<th width="8%">Status</th>
					<th width="6%">Your Ref.</th>
					<th width="8%">Pax Name</th>
					<th width="8%">Job Time</th>
					<th width="8%">Pickup</th>
					<th width="8%">Destination</th>
					<th width="7%">Base Fare</th>
					<th width="10%">Extras</th>';
					if($surcharge_percentage != '0.00')
						{
							echo'<th width="4%">CC. Sur.</th>';
						}
					echo'
					<th width="8%">T. Ex. GST</th>
					<th width="5%">GST</th>
					<th width="5%">Total</th>
				</tr>';
		while ($row1 = mysql_fetch_array($result1))
			{
				if($surcharge_percentage !='0.00')
					{
						$base_fare 		= 	$row1['base_fare'];
						$extras 		= 	$row1['total_fare'] - $row1['base_fare'];
						$surcharge 		= 	($base_fare + $extras) * $surcharge_percentage;
						$total_ex_gst	=	(($base_fare + $extras + $surcharge)*10)/11;
						$gst			= 	$total_ex_gst * .10;
						$total_fare		=   $total_ex_gst + $gst;
					}
				else
					{
						$base_fare 		= 	$row1['base_fare'];
						$extras 		= 	$row1['total_fare'] - $row1['base_fare'];
						$total_ex_gst	=	(($base_fare + $extras)*10)/11;
						$gst			= 	$total_ex_gst * .10;
						$total_fare		=   $total_ex_gst + $gst;
					}
				echo '
					<tr>
						<td valign="top"><input type="checkbox" name="job_id[]" id="jobID" value="'.$row1['id'].'"></td>
						<td valign="top"><a href="http://dev.bookings-global.com.au/Forms/show_booking_details.php?job_id='.$row1['id'].'" target="_blank" >'.$row1['id'].'</a></td>
						<td valign="top"><b>'.$row1['job_current_payment_status_id'].'</b></td>
						<td valign="top">'.$row1['bkg_by'];
							
						echo'
						</td>
						<td valign="top">'.$row1['pax_name'].'</td>
						<td valign="top">'.$row1['date_time'].'</td>
						<td valign="top">';
							if($row1['pickup_flight_no'] != '')
								{
									echo ''.$row1['pickup_flight_no'].'<br>';
								}
							echo'
							'.$row1['from_address'].'
						</td>
						<td valign="top">
							'.$row1['to_address'].'
						</td>
						<td valign="bottom" style="text-align:right;"><span id="smaller_txt_bold">$'.$base_fare.'</span></td>
						<td valign="bottom" style="text-align:right;">';
							if($row1['booster_seats'] > 0)
								{
									echo 'Booster Seat = '.$row1['booster_seats'].'<br>';
								}
							if($row1['baby_seats'] > 0)
								{
									echo 'Baby Seat = '.$row1['baby_seats'].'<br>';
								}
							
							echo'Total Extras = '.number_format($extras,2).'
						</td>';
						if($surcharge_percentage != '0.00')
							{
								echo'<td valign="bottom" style="text-align:right;"><span id="smaller_txt_bold">$'.number_format($surcharge,2).'</span></td>';
							}
						echo'
						<td  valign="bottom" style="text-align:right;"><span id="smaller_txt_bold">$'.number_format($total_ex_gst,2).'</span></td>
						<td  valign="bottom" style="text-align:right;"><span id="smaller_txt_bold">$'.number_format($gst,2).'</span></td>
						<td  valign="bottom" style="text-align:right;"><span id="smaller_txt_bold">$'.number_format($total_fare,2).'</span></td>
					</tr>';
			}
		echo '
		</table><br>
		<h2>STEP 3 - SELECT RECEIVERS</h2><br>
		<table id="invoiceTable">
			<tr>
				<th></th>
				<th>Contact Type</th>
				<th>Send To</th>
				<th>Email</th>
			</tr>';
			$database = new database();
			$query2 = "SELECT 	user.id AS id,
								user.email AS email,
								user.fname AS fname,
								user.lname AS lname,
								user.title AS title,
								variable__conatct_type.details AS contact_type
					from
					charge_acc__contacts
					LEFT JOIN user ON charge_acc__contacts.user_id = user.id
					LEFT JOIN variable__conatct_type ON variable__conatct_type.id= charge_acc__contacts.type_id
					WHERE charge_acc__contacts.charge_acc_id = '".$_POST['charge_acc']."'
					AND (charge_acc__contacts.type_id = '1' OR charge_acc__contacts.type_id = '2' OR charge_acc__contacts.type_id = '3')
					order by user.fname";
			
			$result2 = $database->query($query2);
			while ($row2 = mysql_fetch_array($result2))
					{
						if($row2['email'] != '')
							{
								echo '
								<tr>
									<td><input type="checkbox" name="send_to[]" value="'.$row2['id'].'"</td>
									<td>'.$row2['contact_type'].'</td>
									<td>'.$row2['title'].' '.$row2['fname'].' '.$row2['lname'].'</td>
									<td>'.$row2['email'].'</td>
								</tr>';
							}
					}
		echo'
		</table>
		<br>
		<div style=" padding-top:50px;"><input type="submit" class="big_btn" name="preview_send" id="preview_send" value="Preview of the Invoice"></div>';
		
	}
	
	/*--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------*/	
if(isset($_POST['customer_id_for_listing']))
	{
		
		$query = "SELECT
					invoice.id as id,
					invoice.charge_acc_id as charge_acc_id,
					invoice.status_id as status_id,
					invoice.base_fare as base_fare,
					invoice.extras as extras,
					invoice.surcharge as surcharge,
					invoice.total_ex_gst as total_ex_gst,
					invoice.gst as gst,
					invoice.total_amount as total_amount,
					invoice.notes as notes,
					DATE_FORMAT(invoice.c_date,'%d-%m-%Y') as c_date,
					invoice__status.details as invoice_status_id,
					invoice__sent_status.details as invoice_sent_status
					from invoice
					LEFT JOIN invoice__status ON invoice.status_id = invoice__status.id
					LEFT JOIN invoice__sent_status ON invoice.sent_status_id = invoice__sent_status.id
					where charge_acc_id='".$_POST['customer_id_for_listing']."'
					ORDER BY id DESC";
		$result = $database->query($query);
		echo '<table id="invoiceTable" width="100%">
				<tr>
					<td colspan="14"><input type="button" name="CheckAll" value="Select All"
					onClick="checkAll(document.list_invoice_form.invoiceID)">
					<input type="button" name="UnCheckAll" value="Un-Select All"
					onClick="uncheckAll(document.list_invoice_form.invoiceID)">
					</td>
				</tr>
				<tr>
					<th>Send</th>
					<th>Edit</th>
					<th>Show</th>
					<th>Invoice ID</th>
					<th>Status</th>
					<th>Send Status</th>
					<th>Created On</th>
					<th>Base Fare</th>
					<th>Extras</th>
					<th>Surcharge</th>
					<th>Tot. Ex GST</th>
					<th>GST</th>
					<th>TOTAL</th>
					<th width="30%">Notes</th>
				</tr>';
		while ($row = mysql_fetch_array($result))
			{
			
				echo '
					<tr>
						<td><input type="checkbox" name="invoice_id[]" id="invoiceID" value="'.$row['id'].'"></td>
						<td>
							<a href="http://dev.bookings-global.com.au/Forms/edit_invoice.php?id='.$row['id'].'" ><img src="../Images/edit.png" border="0px" height"16px" width="16px"></a>
						</td>
						<td>
							<a href="http://dev.bookings-global.com.au/invoices/invoice_'.$row['id'].'.html" target="_blank">HTML</a>
							<a href="http://dev.bookings-global.com.au/invoices/invoice_'.$row['id'].'.pdf" target="_blank">PDF</a>
						</td>
						<td>'.$row['id'].'</td>	
						<td>'.$row['invoice_status_id'].'</td>
						<td>'.$row['invoice_sent_status'].'</td>
						<td>'.$row['c_date'].'</td>
						<td style="text-align:right;">'.$row['base_fare'].'</td>
						<td style="text-align:right;">'.$row['extras'].'</td>
						<td style="text-align:right;">'.$row['surcharge'].'</td>
						<td style="text-align:right;">'.$row['total_ex_gst'].'</td>
						<td style="text-align:right;">'.$row['gst'].'</td>
						<td style="text-align:right;"><b>'.$row['total_amount'].'</b></td>
						<td><b>'.$row['notes'].'</b></td>
					</tr>';
			}
		echo '
		</table><br>
		<h2>STEP 3 - SELECT RECEIVERS</h2><br>
		<table id="invoiceTable">
			<tr>
				<th></th>
				<th>Contact Type</th>
				<th>Send To</th>
				<th>Email</th>
			</tr>';
			$database = new database();
			$query2 = "SELECT 	user.id AS id,
								user.email AS email,
								user.fname AS fname,
								user.lname AS lname,
								user.title AS title,
								variable__conatct_type.details AS contact_type
					from
					charge_acc__contacts
					LEFT JOIN user ON charge_acc__contacts.user_id = user.id
					LEFT JOIN variable__conatct_type ON variable__conatct_type.id= charge_acc__contacts.type_id
					WHERE charge_acc__contacts.charge_acc_id = '".$_POST['customer_id_for_listing']."'
					AND (charge_acc__contacts.type_id = '1' OR charge_acc__contacts.type_id = '2' OR charge_acc__contacts.type_id = '3')
					order by user.fname";
			
			$result2 = $database->query($query2);
			while ($row2 = mysql_fetch_array($result2))
					{
						if($row2['email'] != '')
							{
								echo '
								<tr>
									<td><input type="checkbox" name="send_to[]" value="'.$row2['id'].'"</td>
									<td>'.$row2['contact_type'].'</td>
									<td>'.$row2['title'].' '.$row2['fname'].' '.$row2['lname'].'</td>
									<td>'.$row2['email'].'</td>
								</tr>';
							}
					}
		echo'
		</table>';
	}


?>