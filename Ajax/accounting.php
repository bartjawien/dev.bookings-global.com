<?php
ini_set("display_errors", 0);
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "account.php");
require_once(CLASSES_PATH . "transaction.php");
session_start();
$database = new Database;

//For showing account balance on driver selection
if(isset($_POST['acc_bal']))
	{
		$driver_id = $_POST['driver_id'];
		$query = "SELECT * from drivers where user_id='".$driver_id."'";
		$result = $database->query($query);
		$row = mysql_fetch_assoc($result);
			{
				$data =$row['acc_bal'];
			}
					
		echo $data;
	}
	
//When user Submit the form pressing save button of cash in page
if(isset($_POST['save_cash_in_entry']))
	{
		$driver_id = $_POST['driver_id'];
		$amt_cr = $_POST['cash_in_amt'];
		$amt_dr = $_POST['cash_in_amt'];
		$notes = $_POST['notes'];
		$acc_cr = 'drivers__accounts_'.$driver_id;
		$acc_dr = 'acc__cash';
		
		$transaction = new Transaction;
		$trx_id = $transaction->newTransaction($acc_dr,$acc_cr);
		
		$account = new Account();
		$account->creditEntry($trx_id, $_SESSION['FNAME'].' '.$_SESSION['LNAME'], $notes, $acc_cr, $acc_dr, $amt_cr);
		
		$account->debitEntry($trx_id, $_SESSION['FNAME'].' '.$_SESSION['LNAME'], $notes, $acc_dr,  $acc_cr, $amt_dr);
				
	}

	//When user Submit the form pressing save button of cash out page
if(isset($_POST['save_cash_out_entry']))
	{
		$driver_id = $_POST['driver_id'];
		$amt_dr = $_POST['cash_out_amt'];
		$amt_cr = $_POST['cash_out_amt'];
		$notes = $_POST['notes'];
		$acc_dr = 'drivers__accounts_'.$driver_id;
		$acc_cr = 'acc__cash';
		
		$transaction = new Transaction;
		$trx_id = $transaction->newTransaction($acc_dr,$acc_cr);
		
		$account = new Account();
		$account->creditEntry($trx_id, $_SESSION['FNAME'].' '.$_SESSION['LNAME'], $notes, $acc_cr, $acc_dr, $amt_cr);
		
		$account->debitEntry($trx_id, $_SESSION['FNAME'].' '.$_SESSION['LNAME'], $notes, $acc_dr,  $acc_cr, $amt_dr);
		
	}

	if(isset($_POST['save_credit_entry']))
	{
		$driver_id = $_POST['driver_id'];
		$amt_cr = $_POST['credit_amt'];
		$amt_dr = $_POST['credit_amt'];
		$notes = $_POST['notes'];
		$acc_cr = 'drivers__accounts_'.$driver_id;
		$acc_dr = 'acc__general';
		
		$transaction = new Transaction;
		$trx_id = $transaction->newTransaction($acc_dr,$acc_cr);
		
		$account = new Account();
		$account->creditEntry($trx_id, $_SESSION['FNAME'].' '.$_SESSION['LNAME'], $notes, $acc_cr, $acc_dr, $amt_cr);
		
		$account->debitEntry($trx_id, $_SESSION['FNAME'].' '.$_SESSION['LNAME'], $notes, $acc_dr,  $acc_cr, $amt_dr);
				
	}
	
	if(isset($_POST['save_debit_entry']))
	{
		$driver_id = $_POST['driver_id'];
		$amt_cr = $_POST['debit_amt'];
		$amt_dr = $_POST['debit_amt'];
		$notes = $_POST['notes'];
		$acc_dr = 'drivers__accounts_'.$driver_id;
		$acc_cr = 'acc__general';
		
		$transaction = new Transaction;
		$trx_id = $transaction->newTransaction($acc_dr,$acc_cr);
		
		$account = new Account();
		$account->creditEntry($trx_id, $_SESSION['FNAME'].' '.$_SESSION['LNAME'], $notes, $acc_cr, $acc_dr, $amt_cr);
		
		$account->debitEntry($trx_id, $_SESSION['FNAME'].' '.$_SESSION['LNAME'], $notes, $acc_dr,  $acc_cr, $amt_dr);
				
	}