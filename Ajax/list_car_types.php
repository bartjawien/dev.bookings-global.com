<?php
ini_set("display_errors", 0);
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(INCLUDE_PATH . "functions.php");

$database = new Database;
try
	{
/*-------------------LISTING-----------------------------------------------------------------------------------------------------*/
		if($_POST['search_string'])
			{
				$database = new database;
				$query = "SELECT COUNT(*) AS RecordCount FROM variable__car_type
							WHERE 
							(id like '%".$_POST['search_string']."%'
							OR details like '%".$_POST['search_string']."%')";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];
				
				
				$query = "SELECT * FROM variable__car_type
							WHERE 
							(id like '%".$_POST['search_string']."%'
							OR details like '%".$_POST['search_string']."%'
							ORDER BY 
							".$_GET["jtSorting"]." LIMIT ".$_GET["jtStartIndex"].", ".$_GET["jtPageSize"]."";
				$result = $database->query($query);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['Records'] = $rows;
				print json_encode($jTableResult);
			}
/*-------------------LISTING-----------------------------------------------------------------------------------------------------*/

		else if($_GET["action"] == "list_car_types")
			{
				//Get record count
				$query = "SELECT COUNT(*) AS RecordCount FROM variable__car_type";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];

				//Get records from database
				$query = "SELECT *
							FROM variable__car_type
							ORDER BY
							".$_GET["jtSorting"]." LIMIT ".$_GET["jtStartIndex"].", ".$_GET["jtPageSize"]."";
				$result = $database->query($query);
				//Add all records to an array
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}

				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['Records'] = $rows;
				print json_encode($jTableResult);
			}

/*-------------------CREATE-----------------------------------------------------------------------------------------------------*/

		else if($_GET["action"] == "create_car_types")
			{
				foreach(array_keys($_POST) as $key)
					{
						$clean[$key] = mysql_real_escape_string($_POST[$key]);
					}
				$result = mysql_query("INSERT INTO variable__car_type
										(
											id,
											order_id,
											details
										) 
									VALUES
										(
											NULL,
											'".$clean['order_id']."',
											'".$clean['details']."'
										)");

				$result = mysql_query("SELECT * FROM variable__car_type WHERE id = LAST_INSERT_ID();");
				$row = mysql_fetch_array($result);
				
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				$jTableResult['Record'] = $row;
				print json_encode($jTableResult);
			}
/*-------------------UPDATE-----------------------------------------------------------------------------------------------------*/

		else if($_GET["action"] == "update_car_types")
			{
				$database = new database;
				//Update record into database
				foreach(array_keys($_POST) as $key)
					{
						$clean[$key] = mysql_real_escape_string($_POST[$key]);
					}
				$query = "UPDATE variable__car_type SET 
							order_id	= '".$clean["order_id"]."',
							details		= '".$clean["details"]."'
							WHERE id 	= ".$clean['id']."";
				$result = $database->query($query);
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}

/*-------------------DELETE-----------------------------------------------------------------------------------------------------*/

		else if($_GET["action"] == "delete_car_types")
			{
				$database = new database;
				//Delete from database
				$query = "DELETE FROM variable__car_type WHERE id = ".$_POST["id"]."";
				$result = $database->query($query);
				//Return result to jTable
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				print json_encode($jTableResult);
			}
/*-------------------EXPORT-----------------------------------------------------------------------------------------------------*/
			
		else if($_GET["action"] == "export_excel")
			{
				
				require_once '../Classes/PHPExcel.php';
				
				$database = new database;
				$objPHPExcel = new PHPExcel();
				
								// Create a first sheet
				$objPHPExcel->getProperties()->setCreator("".BUSINESS_NAME."")
							 ->setLastModifiedBy("".$_SESSION['TITLE']." ".$_SESSION['FNAME']." ".$_SESSION['LNAME']."")
							 ->setTitle("List of Car Types of ".BUSINESS_NAME."")
							 ->setSubject("List of Car Types of ".BUSINESS_NAME."")
							 ->setDescription("List of Car Types of ".BUSINESS_NAME."")
							 ->setKeywords("List of Car Types ")
							 ->setCategory("List of Car Types ");
							 
				$objPHPExcel->setActiveSheetIndex(0);
				$objPHPExcel->getActiveSheet()->setCellValue('A1', "ID");
				$objPHPExcel->getActiveSheet()->setCellValue('B1', "ORDER ID");
				$objPHPExcel->getActiveSheet()->setCellValue('C1', "CAR TYPE");
				
				// Hide "ID" column
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setVisible(false);
				
				$query = "SELECT *
							FROM variable__car_type
							ORDER BY order_id ASC";
				$result = $database->query($query);

				$i = 2;
				while($row = mysql_fetch_array($result))
					{
						$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A'.$i, $row['id'])
							->setCellValue('B'.$i, $row['order_id'])
							->setCellValue('C'.$i, $row['details']);
						$i++;
					}
				$todays_date = Date("d-m-Y-His");
				header('Content-Type: application/vnd.ms-excel');
				$car_type_report = "POIS_List_".$todays_date.".xls";
				header('Content-Disposition: attachment;filename='.$car_type_report.'');
				header('Cache-Control: max-age=0');
				$objPHPExcel->getActiveSheet()->setTitle('POIS List');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$objWriter->save('php://output');
			}
						
	}
catch(Exception $ex)
	{
		//Return error message
		$jTableResult = array();
		$jTableResult['Result'] = "ERROR";
		$jTableResult['Message'] = $ex->getMessage();
		print json_encode($jTableResult);
	}
?>