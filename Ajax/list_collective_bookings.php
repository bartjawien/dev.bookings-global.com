<?php
ini_set("display_errors", 0);
require_once('../init.php');
require_once(CLASSES_PATH . "database.php");
require_once(INCLUDE_PATH . "functions.php");
session_start();

$database = new Database;


try
	{
	
/*-------------------LISTING-----------------------------------------------------------------------------------------------------*/
		if($_GET["action"] == "list_all_bookings")
			{
				
				$where .= "WHERE job__reference.acc_type='3' AND job.job_date > '2014-01-01' AND job.job_date < '2014-05-01' AND job.job_date<>'0000-00-00' ";
				/*if($_GET["dt_from"] != ''  && $_GET["dt_to"] != '')
					{
						$where .= " AND job.job_date between '".$_GET["dt_from"]."' AND '".$_GET["dt_to"]."' ";
					}
				else
					{
						$where .= " AND job.job_date between '1970-01-01' AND '2050-01-01' ";
					}*/
				if($_GET['search_field'] != '' && $_GET['search_text'] != '')
					{ 
						//if($_POST['search_field'] == 'id' || $_POST['search_field'] == 'bkg_state'  || $_POST['search_field'] == 'frm_flight_no'  || $_POST['search_field'] == 'frm_sub'  || $_POST['search_field'] == 'to_sub')
						if($_GET['search_field'] == 'id' || $_GET['search_field'] == 'frm_flight_no'  || $_GET['search_field'] == 'frm_sub'  || $_GET['search_field'] == 'to_sub')
							{
								$where .= " AND job.".$_GET['search_field']." = '".$_GET['search_text']."' ";
							}
						else if($_GET['search_field'] == 'bkg_state' )
							{
									//$where .= " AND job.frm_state =  ( SELECT id FROM variable__states WHERE details LIKE '%".$_GET['search_text']."%' ) ";
									$where .= " AND job.frm_state LIKE '%".$_GET['search_text']."%' ";
							}
						else if($_GET['search_field'] == 'charge_acc_id' )
							{
									$where .= " AND job__reference.charge_acc_id IN  ( SELECT id FROM charge_acc WHERE account_name LIKE '%".$_GET['search_text']."%' ) ";
							}
						else if($_GET['search_field'] == 'bkg_by_id' || $_GET['search_field'] == 'pax_id' )
							{
									$where .= " AND job__reference.".$_GET['search_field']." IN  ( SELECT id FROM user WHERE fname LIKE '%".$_GET['search_text']."%' OR lname LIKE '%".$_GET['search_text']."%' ) ";
							}
						else if($_GET['search_field'] == 'std_id' || $_GET['search_field'] == 'std_fname'  || $_GET['search_field'] == 'std_lname')
							{
									$where .= " AND job__reference.".$_GET['search_field']." = '".$_GET['search_text']."'  ";
							}
						else if($_GET['search_field'] == 'driver' )
							{
									$where .= " AND job.driver_id IN  ( SELECT id FROM user WHERE fname LIKE '%".$_GET['search_text']."%' OR lname LIKE '%".$_GET['search_text']."%' AND role_id ='2') ";
							}
						
						else if($_GET['search_field'] == 'phone' )
							{
								$where .= " AND ( b.phone LIKE '%".$_GET['search_text']."%' OR d.mobile LIKE '%".$_GET['search_text']."%' OR c.mobile LIKE '%".$_GET['search_text']."%') ";
							}
					}
				if($_GET['search_job_status'] !='')
					{
						$where .= " AND job.job_status = '".$_GET['search_job_status']."'  ";
						if($_GET["search_by_charge_account_id"] != '' )
						{
							$where .= " AND job__reference.charge_acc_id = '".$_GET["search_by_charge_account_id"]."'";
						}
					}
				if($_GET["under_18"] != '' )
					{
						$where .= " AND job__reference.under_18 = '".$_GET["under_18"]."'";
					}
				if($_SESSION['ROLE_ID'] == '3')
					{
						$query1 = "SELECT * from charge_acc__contacts where user_id = '".$_SESSION['USER_ID']."'";
						$result1 = $database->query($query1);
						$row1 = mysql_fetch_array($result1);
						$charge_acc_id = $row1['charge_acc_id'];
						$where .= " AND job__reference.charge_acc_id = '".$charge_acc_id."' ";
					}
				if($_SESSION['ROLE_ID'] == '2')
					{
						$where .= " AND job.driver_id = '".$_SESSION['USER_ID']."' AND job.driver_status in ('3','4')  ";
					}
				//Get record count
			/*	$query = "SELECT COUNT(*) AS RecordCount FROM job";
				$result = $database->query($query);
				$row = mysql_fetch_array($result);
				$recordCount = $row['RecordCount'];*/
				
				$query = "
				SELECT
				*,
				job.id as job_id,
				job__reference.entered_by as entered_by,
				CONCAT(a.fname) as entry_by,
				CONCAT(b.fname,'<br/>',b.phone) as bkg_by,";
				
				if($_SESSION['ROLE_ID']== '2' )
					{
						$query .= "
						CONCAT(c.title, ' ', c.fname, ' ', c.lname) as pax,"; 
					}
				else
					{
						$query .= "
						CONCAT(c.title, ' ', c.fname, ' ', c.lname,'<br/>', c.mobile) as pax,";
					}
				
				$query .="
				CONCAT(d.fname,'<br/>',d.mobile) as driver,
				CONCAT(job__reference.std_title, ' ', job__reference.std_fname, ' ', job__reference.std_lname) as student_name,
				CONCAT(job.frm_line1,' ',job.frm_line2,'<br/><b>',job.frm_sub,'</b>') as from_address,
				CONCAT(job.frm_via_line1,' ',job.frm_via_line2,'<br/><b>',job.frm_via_sub,'</b>') as from_via_address,
				CONCAT(job.to_flight_no,' ',job.to_line1,' ',job.to_line2,'<br/><b>',job.to_sub,'</b>') as to_address,
				CONCAT(job.to_via_line1,' ',job.to_via_line2,'<br/><b>',job.to_via_sub,'</b>') as to_via_address,
				CONCAT('<b>',TIME_FORMAT(job.job_time,'%H:%i'),'</b><br/>',DATE_FORMAT(job.job_date,'%d/%m/%Y')) as date_time,
				variable__car_type.details as vehicle_type,
				job__status_ids.details as current_booking_status,
				job__driver_status_ids.details as driver_status,
				job__reference.std_title as std_title,
				job__reference.std_fname as std_fname,
				job__reference.std_lname as std_lname,
				job__reference.under_18 as under_18,
				job__reference.acc_type as acc_type,
				job.driver_notes as driver_notes, 
				job__driver.accepted_amount as driver_price,
				job__driver_extra_pay.amount as extra_pay,
				(if(job__driver_extra_pay.amount!= null || job__driver_extra_pay.amount!= '' ,job__driver_extra_pay.amount, 0) + job__driver.accepted_amount) as driver_fee
				from 
				job
				LEFT JOIN job__reference ON job.job_reference_id = job__reference.id
				LEFT JOIN job__status_ids ON job.job_status = job__status_ids.id
				LEFT JOIN variable__car_type ON job.car_id = variable__car_type.id
				LEFT JOIN job__driver_status_ids ON job.driver_status = job__driver_status_ids.id
				LEFT JOIN user AS a ON job__reference.entered_by = a.id
				LEFT JOIN user AS b ON job__reference.bkg_by_id = b.id
				LEFT JOIN user AS c ON job__reference.pax_id = c.id
				LEFT JOIN user AS d ON job.driver_id = d.id
				LEFT JOIN charge_acc ON job__reference.charge_acc_id = charge_acc.id
				LEFT JOIN job__driver ON job.id = job__driver.job_id AND job__driver.id=(SELECT MAX(id) FROM job__driver WHERE job_id = job.id)  
				LEFT JOIN job__driver_extra_pay ON job.id = job__driver_extra_pay.job_id 
				".$where."
				ORDER BY ".$_GET["jtSorting"]."  LIMIT ".$_GET["jtStartIndex"].", ".$_GET["jtPageSize"]."";
			
			//echo $query; exit();
				$result = $database->query($query);
				$total_rows= mysql_num_rows($result);
				$rows = array();
				while($row = mysql_fetch_array($result))
					{
						$rows[] = $row;
					}
					
				
				$jTableResult = array();
				$jTableResult['Result'] = "OK";
				//$jTableResult['TotalRecordCount'] = $recordCount;
				$jTableResult['TotalRecordCount'] = $total_rows;
				$jTableResult['Records'] = $rows;
				
				
				print json_encode($jTableResult);
			}
		

	}
catch(Exception $ex)
	{
		//Return error message
		$jTableResult = array();
		$jTableResult['Result'] = "ERROR";
		$jTableResult['Message'] = $ex->getMessage();
		print json_encode($jTableResult);
	}
?>