<?php
ini_set("display_errors", 1);
require_once("../init.php");
include(''.INCLUDE_PATH.'config.php');
include(''.INCLUDE_PATH.'settings.php');
require_once(CLASSES_PATH . "mailer.php");
require_once(CLASSES_PATH . "jobReference.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "job.php");
require_once(CLASSES_PATH . "chargeAccount.php");

$database = new database;
$job = new job();
$job_reference = new JobReference();
$mailer = new Mailer();
$user = new User();
$chargeaccount = new chargeAccount();

$job_id= $_POST['job_id'];
$admin_id = $_SESSION['USER_ID'];
$admin_details = $user->getUserDetails($admin_id);
$j1_details = $job->getJobDetails($job_id);
$job_reference_id = $j1_details['job_reference_id'];
$job_reference_details = $job_reference->getJobReferenceDetails($job_reference_id);
//print_r($job_reference_details);


if($j1_details['job_status'] =='5') // Received and waiting for approval
	{
		
		$job->updateJob($j1_details['id'], 'job_status', '10'); //change status of the job to update
		$job->addJobLog($j1_details['id'], $admin_id, 'Job Approved by '.$admin_details['title'].' '.$admin_details['fname'].' '.$admin_details['lname'].'', '10', '5'); // add job log
		
		//$job_reference->updateJobReferenceTable($job_reference_id, 'bkg_by_id', $admin_id);
		//Add in Job Reference Log Table
		$job_reference->addJobReferenceLog($job_reference_id, $admin_id, 'job approved and sent to allied cars', $j1_details['id'], 'NIL');
		
		//send email to student and person who is booking on behalf of student
		$charge_acc_details = $chargeaccount->getChargeAccountDetails($job_reference_details['charge_acc_id']);
		
		$mailer->sendUniversityMail($job_reference_id, '5', '0');	
			
		echo "Booking ID - ".$j1_details['id']."";
		
		echo " has been approved by you
		Booking request sent to Allied Chauffeured Cars.";
	}
		
	

?>