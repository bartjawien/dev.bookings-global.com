<?php
ini_set("display_errors", 1);
require_once('../init.php');
require_once(INCLUDE_PATH . "settings.php");
require_once(CLASSES_PATH . "database.php");
require_once(CLASSES_PATH . "job.php");
require_once(CLASSES_PATH . "user.php");
require_once(CLASSES_PATH . "mailer.php");
require_once(CLASSES_PATH . "chargeAccount.php");
require_once(CLASSES_PATH . "transaction.php");
require_once(CLASSES_PATH . "account.php");
require_once(INCLUDE_PATH . "functions_date_time.php");
session_start();
$database = new Database;
$user = new User();
if(isset($_GET['driver_id']) && isset($_GET['from_date']) && isset($_GET['to_date']))
	{
		if($_GET['from_date'] != '' && $_GET['to_date'] != '')
			{
				$where .= 	" AND (jobs.job_date BETWEEN '".$_GET['from_date']."' AND '".$_GET['to_date']."')";
			}
		if($_GET['driver_status'] == '1') // Not paid
			{
				$where .= 	" AND jobs.driver_status != '5' ";
			}
		if($_GET['driver_status'] == '2')
			{
				$where .= 	" AND jobs.driver_status = '5' ";
			}
		$user_details = $user->getUserDetails($_GET['driver_id']);
		$query = "
			SELECT 
			jobs.*, 
			jobs.id as job_id,
			job__status_ids.details as job_status_id,
			job__driver_status_ids.details as driver_status_id,
			job__references.*
			from 
			job as jobs
			INNER JOIN job__status_ids as job__status_ids ON jobs.job_status = job__status_ids.id
			INNER JOIN job__driver_status_ids as job__driver_status_ids ON jobs.driver_status = job__driver_status_ids.id
			INNER JOIN job__reference as job__references ON jobs.job_reference_id = job__references.id
			WHERE
			jobs.driver_id = '".$_GET['driver_id']." '
			".$where."
			order by jobs.job_time, jobs.job_date ASC";
		
		$data .= '<form method="post" name="driver_payment_form" id="driver_payment_form">
				<table width="100%" class="invoiceTable">
					<tr>
						<th colspan="15">
							Jobs for - '.$user_details['title'].' '.$user_details['fname'].' '.$user_details['lname'].'
							<input type="hidden" name="driver_id" id="driver_id" value="'.$_GET['driver_id'].'">
						</th>
					</tr>
					<tr>
						<th><input type="checkbox" name="check_all_jobs" id="check_all_jobs"></th>
						<th>Job ID</th>
						<th>Date/Time</th>
						<th>Pax Name</th>
						<th>From</th>
						<th>To</th>
						<th>Driver Notes</th>
						<th>Job Status</th>
						<th>Driver Status</th>
						<th>Driver Price</th>
						<th>Extra Pay</th>
						<th>Driver Fee</th>
					</tr>';
		$result = $database->query($query);
		$total_base_fee = 0;
		$total_extras = 0;
		$total_driver_fee = 0;
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC))
			{
				$query1 = "Select sum(amount) as extras from job__driver_extra_pay where job_id = '".$row['job_id']."'";
				$result1 = $database->query($query1);
				$row1 = mysql_fetch_array($result1, MYSQL_ASSOC);
				
				$driver_fee = $row['driver_price'] + $row1['extras'];
				$data .='<tr>
							<td valign-"top">';
							if($row['driver_status'] == '1' || $row['driver_status'] == '5')
								{
									$data .='';
								}
							else
								{
									$data .='
									<input type="checkbox" name="to_pay_jobs[]" class="to_pay_jobs" value="'.$row['job_id'].'">';
								}
							$data .='
							</td>
							<td valign="top"><a href="#" id="'.$row['job_id'].'" class="job_id_link">'.$row['job_id'].'</a></td>
							<td valign="top">
								'.calculateTimeInAmPmFormatWithoutSeconds($row['job_time']).'<br/>
								'.formatDate($row['job_date'], 1).'
							</td>';
							if($row['std_fname'] != '' && $row['std_lname'] != '')
								{
									$data .='<td valign-"top">'.$row['std_title'].' '.$row['std_fname'].' '.$row['std_lname'].'<br/>ID '.$row['std_id'].'</td>';
								}
							else
								{
									$pax_details = $user->getUserDetails($row['pax_id']); // get details of this pax
									$data .='<td valign="top">'.$pax_details['title'].' '.$pax_details['fname'].' '.$pax_details['lname'].'</td>';
								}
							$data .='
							<td valign="top">';
								if($row['frm_flight_no']!='')	{ $data.= '<b>Flight No. -'.$row['frm_flight_no'].'</b>';}
								if($row['frm_line1']!='')		{ $data.= ''.$row['frm_line1'].'<br/>';}
								if($row['frm_line2']!='')		{ $data.= ''.$row['frm_line2'].'<br/>';}
								if($row['frm_sub']!='')			{ $data.= '<b>'.$row['frm_sub'].'</b>';}
								if($row['frm_pc']!='')			{ $data.= ' '.$row['frm_pc'].'';}
								if($row['frm_state']!='')		{ $data.= ' '.$row['frm_state'].'<br/>';}
								if($row['frm_via_sub'] != '')
									{
										$data.='<strong>2nd PICKUP-DROP OFF</strong><br/>';
										if($row['frm_via_line1']!='')	{ $data.= ''.$row['frm_via_line1'].'';}
										if($row['frm_via_line2']!='')	{ $data.= ''.$row['frm_via_line2'].'<br/>';}
										if($row['frm_via_sub']!='')		{ $data.= '<b>'.$row['frm_via_sub'].'</b> ';}
										if($row['frm_via_pc']!='')		{ $data.= ' '.$row['frm_via_pc'].'</b>';}
										if($row['frm_via_state']!='')	{ $data.= ' '.$row['frm_via_state'].'';}
									}
							$data .='
							</td>
							<td valign="top">';
								if($row['to_flight_no']!='')	{ $data.= 'Flight No. - '.$row['to_flight_no'].'';}
								if($row['to_line1']!='')		{ $data.= ''.$row['to_line1'].'<br/>';}
								if($row['to_line2']!='')		{ $data.= ''.$row['to_line2'].'<br/>';}
								if($row['to_sub']!='')			{ $data.= '<b>'.$row['to_sub'].'</b>';}
								if($row['to_pc']!='')			{ $data.= ' '.$row['to_pc'].'';}
								if($row['to_state']!='')		{ $data.= ' '.$row['to_state'].'<br/>';}
								if($row['to_via_sub'] != '')
									{
										$data.='<strong>LAST DROP</strong><br/>';
										if($row['to_via_line1']!='')	{ $data.= ''.$row['to_via_line1'].'<br/>';}
										if($row['to_via_line2']!='')	{ $data.= ''.$row['to_via_line2'].'<br/>';}
										if($row['to_via_sub']!='')		{ $data.= '<b>'.$row['to_via_sub'].'</b>';}
										if($row['to_via_pc']!='')		{ $data.= ' '.$row['to_via_pc'].'';}
										if($row['to_via_state']!='')	{ $data.= ' '.$row['to_via_state'].'';}
									}
									
							$data .='
							</td>
							<td style="text-align:left;">'.$row['driver_notes'].'</td>
							<td style="text-align:right;" valign="bottom">'.$row['job_status_id'].'</td>
							<td style="text-align:right;" valign="bottom">'.$row['driver_status_id'].'</td>
							<td style="text-align:right;" valign="bottom">$'.number_format($row['driver_price'],2).'</td>
							<td style="text-align:right;" valign="bottom">$'.number_format($row1['extras'],2).'</td>
							<td style="text-align:right;" valign="bottom"><b>$'.number_format($driver_fee,2).'</b></td>';
						$data .='
						</tr>';
						$total_base_fee 	= $total_base_fee + $row['driver_price'];
						$total_extras 		= $total_extras + $row1['extras'];
						$total_driver_fee 	= $total_driver_fee + $driver_fee;
			}
		$data .= '
					<tr>
						<th  style="text-align:right;" valign="bottom" colspan="9">TOTALS</th>
						<th style="text-align:right;" valign="bottom"><b>$'.number_format($total_base_fee,2).'</b></th>
						<th style="text-align:right;" valign="bottom"><b>$'.number_format($total_extras,2).'</b></th>
						<th style="text-align:right;" valign="bottom"><b>$'.number_format($total_driver_fee,2).'</b></th>
					</table>
				<input type="button" id="pay_now" name="pay_now" class="approve_button" value="Pay Now">
				</form>';
		$a = array('data' => ''.$data.'');
		$json = json_encode($a); 
		echo $json;
	}
if(isset($_GET['pay_driver']) && $_GET['pay_driver'] == '1')
	{
		$transaction 	= new Transaction();
		$account 		= new Account();
		$job 			= new job();
		
		$total_paid = 0;
		$jobs_array 	= explode(',', $_GET['pay_jobs']);
		$message .='<table width="100%" class="invoiceTable">';
		foreach($jobs_array as $k=>$v)
			{
				$job_details = $job->getJobDetails($v);
				$entered_by 		= 	$user->getUserDetails($_SESSION['USER_ID']);
				$entered_by_name 	= 	''.$entered_by['fname'].' '.$entered_by['lname'].'';
				
				//change the driver status of the job from whatever to PAID
				//Add to log first and then change the value to new value
				$job->addJobLog($v, $entered_by, 'Driver Paid', '5', $job_details['driver_status']);
				$job->updateJob($v, 'driver_status', '5');
				
				//make accounting entry
				$acc_cr_label 		= 	'drivers__accounts_'.$_GET['driver_id'].'';
				$acc_dr_label 		= 	'acc__general';
				
				$description_debit_entry = "Paid for Booking ID # ".$v."";
				$description_credit_entry = "Paid for Booking ID # ".$v."";

				//Add this in transaction table
				$this_trx_id = $transaction->newTransaction($acc_dr_label, $acc_cr_label);
				
				//calculate how much to pay
				
				$query = "Select sum(amount) as extras from job__driver_extra_pay where job_id = '".$v."'";
				$result = $database->query($query);
				$row = mysql_fetch_array($result, MYSQL_ASSOC);
				$total_amount = $job_details['driver_price'] + $row['extras'];

				//Account entry of debit in Allied General Account
				$acc_dr_entry_id = $account->debitEntry($this_trx_id, $entered_by_name, $description_debit_entry, $acc_dr_label,  $acc_cr_label, $total_amount);
				//Account entry of credit in drivers table	
				$acc_cr_entry_id = $account->creditEntry($this_trx_id, $entered_by_name, $description_credit_entry, $acc_cr_label, $acc_dr_label, $total_amount);
				$message .= "<tr><td>Paid for Job ID - ".$v."</td><td style='text-align:right;'>$".number_format($total_amount,2)."</td></tr>";	
				
				$total_paid = $total_paid + $total_amount;
			}
		$message .= "<tr><td style='text-align:right;'><b>TOTAL PAID</b></td><td style='text-align:right;'><b>$".number_format($total_paid,2)."</b></td></tr></table>";
		$a = array('data' => ''.$message.'');
		$json = json_encode($a);
		echo $json;
	}
?>